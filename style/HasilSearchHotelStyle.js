import Dimensions from 'react-native';

export default HasilSearchHotelStyle = {
    container: {
      backgroundColor:'#f5f6f7',
      flex:1,
    },
    navTop:{
      paddingHorizontal:20,
      backgroundColor:'#1a558f',
      shadowColor: '#000',
      paddingVertical:10,
      marginBottom:20,
      shadowOffset: { width: 0, height: 1 },
      shadowOpacity: 0.8,
      shadowRadius: 2,  
      elevation: 5
    },
    rowsBeetwen:{
      flexDirection:'row',
      paddingVertical:10,
      justifyContent:'space-between',
      alignSelf:'stretch'
    },
    titlePage:{
      fontSize:17,
      borderRadius:15,
      color:'#1a558f',
      marginLeft:20,
      fontWeight:'bold',
      paddingVertical:5,
      paddingHorizontal:10,
      backgroundColor:"#fff",
      textTransform:'uppercase'
    },
    tabsActive:{
      fontSize:17,
      borderRadius:15,
      color:'#fff',
      fontWeight:'bold',
      paddingVertical:5,
      paddingHorizontal:10,
      backgroundColor:"#fcdf00",
      textAlign:'center'
    },
    tabsMenu:{
      fontSize:17,
      borderRadius:15,
      color:'#fcdf00',
      marginLeft:20,
      fontWeight:'bold',
      paddingVertical:5,
      paddingHorizontal:10,
      backgroundColor:"transparent"
    },
    rows:{
      paddingHorizontal:20,
      paddingVertical:5,
    },
    boxShadow:{
      marginHorizontal:0,
      borderRadius:15,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 1,
      overflow:'hidden',
      marginBottom:25,
    },
    boxInner_bt:{
      backgroundColor:'#fff',
    },
    flexRows:{
      flexDirection:'row',
      marginBottom:10,
      alignItems:'center'
    },
    flexRowsBeetwen:{
      flexDirection:'row',
      marginBottom:10,
      justifyContent:'space-between',
      alignSelf:'stretch',
      position:'relative'
    },
    flexRowsLast:{
      flexDirection:'row',
      marginBottom:0,
      alignItems:'center'
    },
    flexRowsLastSpace:{
      flexDirection:'row',
      marginBottom:0,
      alignItems:'center',
      justifyContent:"space-between",
      alignSelf:"stretch"
    },
    rows2:{
      flex:1,
      marginBottom:10,
      position:'relative'
    },
    captionForm:{
      marginLeft:15
    },
    smallText:{
      fontSize:13,
      color:'#1a558f',
      marginBottom:5
    },
    VerySmallText:{
      fontSize:11,
      color:'#1a558f',
      marginBottom:5
    },
    BigText:{
      fontSize:20,
      fontWeight:'bold',
      color:'#1a558f',
      marginBottom:0
    },
    BigText2:{
      fontSize:20,
      textTransform:'uppercase',
      fontWeight:'bold',
      color:'#1a558f',
      marginBottom:0
    },
    button:{
      backgroundColor:'#1a558f',
      borderRadius:20,
      marginBottom:40,
      marginTop:5,
      width:300,
      paddingVertical:10,
    },
    alCenter:{
      alignItems:'center',
      marginTop:10,
    },
    buttonText:{
      fontSize:16,
      fontWeight:'500',
      color:'#fff',
      textAlign:'center',
    },
    containerSlider:{
      backgroundColor:"#fff",
      borderRadius:20,
      paddingVertical:20,
      paddingHorizontal:20,
    },
    panelContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  commandButton: {
    padding: 20,
    borderRadius: 10,
    backgroundColor: '#292929',
    alignItems: 'center',
    margin: 7,
  },
  panel: {
    height: 600,
    padding: 20,
    backgroundColor: '#fff',
    paddingTop: 10,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 5,
    shadowOpacity: 0.4,
  },
  header: {
    width: '100%',
    height: 50,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 8,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10,
  },
  panelTitle: {
    fontSize: 27,
    height: 35,
  },
  panelSubtitle: {
    fontSize: 14,
    color: 'gray',
    height: 30,
    marginBottom: 10,
  },
  panelButton: {
    padding: 13,
    borderRadius: 10,
    backgroundColor: '#292929',
    alignItems: 'center',
    marginVertical: 7,
  },
  rowPanel:{
    marginBottom:15,
    textAlign:"center",
    alignItems:"center"
  },
  rowPanelDirect:{
    marginBottom:15,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  flexRow:{
    flex:1,
    textAlign:"left"
  },
  flexRow2:{
    flex:1,
    textAlign:"center",
    flexDirection:"row",
    alignItems:"center"
  },
  greyBig:{
    fontSize:18,
    color:"#5d5d5d",
    fontWeight:'bold',
  },
  greySmall:{
    fontSize:12,
    color:"#5d5d5d",
  },
  plusMinusbt:{
    paddingHorizontal:20,
    paddingVertical:5,
    backgroundColor:"#5d5d5d",
    fontSize:18,
    borderRadius:20,
    marginHorizontal:5,
    color:"#fff"
  },
  buttonYellow:{
    backgroundColor:"#fcdf00",
    flex:1,
    textAlign:'center',
    borderRadius:20,
    color:"#fff",
    textTransform:"uppercase",
    paddingVertical:10,
    paddingHorizontal:10,
    marginHorizontal:5
  },
  buttonGreen:{
    backgroundColor:"#1a558f",
    flex:1,
    textAlign:'center',
    borderRadius:20,
    color:"#fff",
    textTransform:"uppercase",
    marginHorizontal:5,paddingVertical:10,
    paddingHorizontal:10,
  },
  bgGreenText:{
    backgroundColor:"#1a558f",
    textAlign:'center',
    borderRadius:20,
    color:"#fff",
    fontSize:13,
    paddingVertical:5,
    paddingHorizontal:10
  },
  bgGreenTextAbs:{
    backgroundColor:"#1a558f",
    textAlign:'center',
    borderRadius:20,
    color:"#fff",
    fontSize:13,
    paddingVertical:5,
    paddingHorizontal:20,
    position:'absolute',
    right:10,
    top:-30
  },
  whiteText:{
    fontSize:17,
    color:"#fff",
    textTransform:"uppercase",
    textAlign:"center"
  },
  searchRow:{
    paddingLeft:30,
    position:'relative'
  },
  searchInput:{
    alignSelf:'stretch',
    backgroundColor:'#fff',
    borderRadius:20,
    paddingRight:10,
    paddingLeft:55,
    paddingVertical:5,
    color:"#1a558f",
  },
  imgThumb:{
    flex:1,
    marginBottom:10,
  }
}