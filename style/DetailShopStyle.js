import Dimensions from 'react-native';

export default DetailShopStyle = {
    container: {
    },
    topcard: {backgroundColor: '#00b894',flexDirection: 'row', justifyContent: "space-between", justifyContent: 'center', paddingTop: 16, borderRadius: 12},
    topcard2: {flexDirection: 'row', justifyContent: "space-between"},
    carditem: {width: 100, height: 110, backgroundColor: 'white', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'},
    textwelcome: {
      fontSize: 14,
      marginBottom: 12,
      color: '#1a558f'
    },
    navTop:{
      paddingHorizontal:20,
      backgroundColor:'#1a558f',
      shadowColor: '#000',
      paddingVertical:10,
      shadowOffset: { width: 0, height: 1 },
      shadowOpacity: 0.8,
      shadowRadius: 2,  
      elevation: 5
    },
    rowsBeetwen:{
      flexDirection:'row',
      paddingVertical:10,
      justifyContent:'space-between',
      alignSelf:'stretch'
    },
    searchRow:{
      alignSelf:'stretch',
      position:'relative'
    },
    searchInput:{
      alignSelf:'stretch',
      backgroundColor:'#fff',
      borderRadius:20,
      paddingRight:10,
      paddingLeft:55,
      paddingVertical:5,
      color:"#01b369",
    },
    inputForm:{
      alignSelf:'stretch',
      backgroundColor:'#d1d1d1',
      borderRadius:10,
      paddingRight:10,
      paddingLeft:10,
      paddingVertical:3,
      color:"#000",
      marginTop:3
    },
    textSaldo: {
      fontSize: 18,
      marginBottom: 12,
      color: '#01b369',
      fontWeight:'bold'
    },
    textcard: {flex: 1,fontSize: 10, fontWeight: 'bold'},
    imagecard: {marginTop:12, marginBottom: 12, width: 56, height: 56},
    imagePromo: {
      width: 48, height: 48
    },
    bannerPromo: {
      borderRadius: 3,
      marginTop: 6,
      width: '100%', height: 175, resizeMode: 'cover'
    },
    listDate: {
      flexDirection: 'column',
      marginLeft: 16,
      width: 200
    },
    carditem2: {
      marginBottom: 6,
      backgroundColor: 'white',
      flexDirection: 'row'
    },
    infoTop: {
      flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'
    },
  footer:{
    height:70,
    backgroundColor:'#01b369',
  },
  container2: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  barContainer: {
    position: 'absolute',
    zIndex: 2,
    bottom: 10,
    flexDirection: 'row',
  },
  track: {
    backgroundColor: 'transparent',
    overflow: 'hidden',
    height: 2,
    borderRadius:50,
    width:10,
    height: 10,
    borderWidth: 1,
    borderColor: '#fff',
  },
  bar: {
    backgroundColor: '#fff',
    width:10,
    height: 10,
    position: 'absolute',
    left: 0,
    top: 0,
    borderRadius:50,
  },
  container:{
    flex:1,
  },
  topHome:{
    backgroundColor:"#01b369",
    flexDirection:'row',
    paddingVertical:20,
    paddingHorizontal:20,
    marginBottom:0,
    justifyContent:'space-between',
  },
  logoHome:{
    width:130,
    alignItems:'center',
    justifyContent:'center',
  },
  textName:{
    flex:1,
    marginLeft:15
  },
  userName:{
    color:'#6b6b6b',
    fontSize:16,
    fontWeight:'bold',
    marginBottom:5
  },
  date:{
    color:'#6b6b6b',
    fontSize:14,
  },
  boxShadow:{
    marginHorizontal:0,
    borderRadius:15,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    overflow:'hidden',
    marginBottom:25,
  },
  boxShadow2:{
    shadowColor: "#000",
    shadowOffset: {
    width: 0,
    height: 7,
    },
    shadowOpacity: 0.41,
    shadowRadius: 9.11,

    elevation: 14,
  },
  topBox:{
    flexDirection:'row',
    paddingHorizontal:10,
    justifyContent:'space-between',
    backgroundColor:'#fff',
    paddingVertical:10,
  },
  txtSaldo:{
    color:'#262626',
    fontSize:16,
    fontWeight:'bold',
  },
  txtSmall:{
    color:'#01b369',
    fontSize:14,
    textAlign:'right'
  },
  bottomBox:{
    flexDirection:'row',
    paddingHorizontal:10,
    backgroundColor:'#3f5398',
    paddingVertical:15,
  },
  rowButton:{
    flex:1,
    position:'relative',
    backgroundColor:"#fff",
    padding:20,
    marginHorizontal:15,
    alignItems:'center',
    justifyContent:'center',
    alignSelf:"stretch",
    borderRadius:10
  },
  rowSearch:{
    flexDirection:"row",
    marginBottom:25
  },
  rowButtonFoot:{
    marginHorizontal:5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowButtonTop:{
    marginHorizontal:0,
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
  },
  rowButtonTop2:{
    marginHorizontal:0,
  },
  rowButton2:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    position:'absolute',
    width:'100%',
    left:20,
    bottom:40,
  },
  textButton:{
    color:'#fff',
    fontSize:14,
    fontWeight:'bold',
  },
  boxInner:{
    flexDirection:'row',
    paddingHorizontal:15,
    backgroundColor:'#fff',
    paddingVertical:15,
    borderRadius:15,
  },
  boxInnerFooter:{
    flexDirection:'row',
    paddingHorizontal:15,
    paddingVertical:10,
    backgroundColor:'#01b369',
    justifyContent:'space-between',
  },
  boxRow:{
    flexDirection:'row',
    marginBottom:15,
  },
  boxInner_bt:{
    paddingHorizontal:15,
    backgroundColor:'#fff',
    paddingVertical:15,
    borderRadius:15,
  },
  textButton2:{
    color:'#01b369',
    fontSize:12,
    fontWeight:'bold',
    marginTop:5,
    textAlign:'center'
  },
  textButtonfoot:{
    color:'#fff',
    fontSize:12,
    fontWeight:'bold',
    marginTop:5,
    textAlign:'center'
  },
  leftText:{
    flex:1,
    justifyContent:'center',
  },
  textBold:{
    color:'#01b369',
    fontSize:16,
    fontWeight:'bold',
  },
  blueText:{
    color:'#01b369',
    fontSize:11,
  },
  btRight:{
    width:80,
    alignItems:'center',
    justifyContent:'center',
    marginLeft:2,
  },
  buttonGreen:{
    backgroundColor:'#3a4d87',
    borderRadius:5,
    width:80,
    height:25,
    paddingVertical:5 ,
  },
  buttonText:{
    fontSize:12,
    fontWeight:'500',
    color:'#fff',
    textAlign:'center',
  },
  innerSLider:{
    position:"relative",
    height:100,
    backgroundColor:"#fff",
  },
  textWlcome:{
    fontSize:15,
    color:'#000',
    fontWeight:'600'
  },
  rowRight:{
    flexDirection:'row',
  },
  buttonOrange:{
    backgroundColor:'#ff9602',
    borderRadius:25,
    width:80,
    paddingVertical:5,
  },
  buttonText:{
    fontSize:12,
    fontWeight:'500',
    color:'#fff',
    textAlign:'center',
  },
  roundRed:{
    width: 15,
    height: 15,
    position:'absolute',
    right:0,
    top:0,
    borderRadius: 100/2,
    backgroundColor: '#fb466d'
  },
  rows:{
    paddingHorizontal:20,
    paddingVertical:20,
  },
  bannerRow:{
    paddingHorizontal:20,
    paddingVertical:10,
    alignItems:"center"
  },
  rowsSmall:{
    paddingHorizontal:20,
    paddingVertical:0,
  },
  greyContent:{
    flex:1,
    backgroundColor:'#f5f6f7',
    paddingVertical:30,
    paddingHorizontal:20
  },
  blueText:{
    color:'#01b369',
    fontSize:16,
    fontWeight:'bold',
    marginRight:10,
  },
  strechRow:{
    flexDirection:'row',
    justifyContent:'space-between',
    marginBottom:15,
  },
  rowBanner:{
    flex: 1,
    backgroundColor: '#fff',
    marginBottom:20,
    justifyContent: 'center',
  },
  itemCont:{
    width:125,
    paddingVertical:10,
    paddingHorizontal:10,
    marginRight:10,
    borderRadius:15,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    overflow:'hidden',
    backgroundColor:'#fff',
    alignItems:"center"
  },
  infoShirt:{
    fontSize:18,
    fontWeight:"bold",
    color:"#000"
  },
  priceGreen:{
    fontSize:15,
    fontWeight:"bold",
    color:"#01b369"
  },
  textInfo:{
    textAlign:"left",
    alignSelf:"stretch"
  },
  rowFLex:{
    flexDirection:"row",
    marginBottom:5
  },
  pointRow:{
    marginVertical:3,
    textAlign:"left",
    alignItems:"flex-start"
  },
  infoUkuran:{
    flexDirection:"row",
    alignItems:"flex-end"
  },
  greyCity:{
    fontSize:14,
    color:"#9f9f9f"
  },
  whislistInfo:{
    alignItems:"center"
  },
  smallblacktxt:{
    fontSize:18,
    color:"#000"
  },
  smallGrey:{
    fontSize:12,
    color:"#9f9f9f"
  },
  circleActiveUk:{
    width:30,
    height:30,
    borderRadius:50,
    backgroundColor:"#01b369",
    color:"#fff",
    fontWeight:"bold",
    fontSize:17,
    lineHeight:27,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign:"center",
    marginLeft:10
  },
  circleUk:{
    width:30,
    height:30,
    borderRadius:50,
    backgroundColor:"#d1d1d1",
    color:"#fff",
    fontWeight:"bold",
    fontSize:17,
    lineHeight:27,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign:"center",
    marginLeft:10
  },
  underlineText:{
    width:30,
    marginHorizontal:5,
    fontSize:18,
    color:"#01b369",
    fontWeight:"bold",
    textAlign:"center",
    borderBottomWidth:1,
    borderColor:"#9f9f9f"
  },
  customTabs:{
    backgroundColor:"white",
    padding:0,
    margin:0
  },
  labelTabs:{
    fontSize:12,
    fontWeight:'bold',
    color:"#000"
  }


}