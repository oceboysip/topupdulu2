import Dimensions from 'react-native';

export default HasilSearchStyle = {
    container: {
      backgroundColor:'#f5f6f7',
      flex:1,
    },
    navTop:{
      backgroundColor:'#1a558f',
      shadowColor: '#000',
      paddingVertical:10,
    },
    rowsBeetwen:{
      paddingHorizontal: 20,
      flexDirection:'row',
      paddingVertical:10,
      justifyContent:'space-between',
      alignSelf:'stretch'
    },
    titlePage:{
      fontSize:17,
      borderRadius:15,
      color:'#1a558f',
      marginLeft:20,
      fontWeight:'bold',
      paddingVertical:5,
      paddingHorizontal:10,
      backgroundColor:"#fff",
      textTransform:'uppercase'
    },
    tabsActive:{
      fontSize:17,
      borderRadius:15,
      color:'#fff',
      fontWeight:'bold',
      paddingVertical:5,
      paddingHorizontal:10,
      backgroundColor:"#fcdf00",
      textAlign:'center'
    },
    tabsMenu:{
      fontSize:17,
      borderRadius:15,
      color:'#fcdf00',
      marginLeft:20,
      fontWeight:'bold',
      paddingVertical:5,
      paddingHorizontal:10,
      backgroundColor:"transparent"
    },
    rows:{
      paddingVertical:0,
    },
    boxShadow:{
      marginHorizontal:0,
      borderRadius:15,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 1,
      overflow:'hidden',
      marginBottom:12,
    },
    boxInner_bt:{
      paddingHorizontal:16,
      backgroundColor:'#fff',
      paddingVertical:8,
      borderRadius:8,
    },
    flexRows:{
      flexDirection:'row',
      marginBottom:10,
      alignItems:'center'
    },
    flexRowsBeetwen:{
      flexDirection:'row',
      marginTop: 0,
      marginBottom:8,
      justifyContent:'space-between',
      alignSelf:'stretch',
      alignItems: 'center',
    },
    flexRowsLast:{
      flexDirection:'row',
      alignItems:'center'
    },
    rowsLast:{
      alignItems:'center',
    },
    rows2:{
      flex:1,
      marginBottom:10,
      position:'relative'
    },
    captionForm:{
      marginLeft: 16,
      flexDirection: 'row',
      alignItems: 'flex-end'
    },
    smallText:{
      fontSize:15,
      color:'#1a558f',
      marginBottom:5
    },
    smallTextStrike:{
      fontSize:12,
      color:'#1a558f',
      marginBottom:3,
      textDecorationLine: 'line-through',
      textDecorationStyle: 'solid'
    },
    BigText:{
      fontSize:17,
      fontWeight:'bold',
      color:'#1a558f',
    },
    button:{
      backgroundColor:'#1a558f',
      borderRadius:20,
      marginBottom:40,
      marginTop:5,
      width:300,
      paddingVertical:10,
    },
    alCenter:{
      alignItems:'center',
      marginTop:10,
    },
    buttonText:{
      fontSize:16,
      fontWeight:'500',
      color:'#fff',
      textAlign:'center',
    },
    testTop:{
      alignItems:"center",
      width: '30%'
    },
    testTop2:{
      flex:1,
      textAlign:'center',
      alignItems:"center"
    },
    borderYellow:{
      fontSize:14,
      color:"#fff",
      backgroundColor:"#fcdf00",
      borderRadius: 12,
      paddingVertical:3,
      paddingHorizontal:10,
    },
    biggerText:{
      fontSize:13,
      fontWeight:"bold",
      color:"#fff",
      marginTop: 4
    },
    smallWhite:{
      fontSize:13,
      color:"#fff",
      textAlign: 'center'
    },
    buttonYellow:{
      fontSize:16,
      color:"#fff",
      backgroundColor:"#fcdf00",
      borderRadius:13,
      fontWeight:"normal",
      paddingVertical:4,
      paddingHorizontal:12,
      textTransform:"uppercase"
    },
    middleTextGreen:{
      fontSize:13,
      color:"#1a558f",
      textTransform:'uppercase'
    },
    bigTextGreen:{
      fontSize:17,
      color:"#1a558f",
      fontWeight:'normal',
      textTransform:'uppercase'

    }

}
