import Dimensions from 'react-native';

export default SearchAirportStyle = {
    container: {
      backgroundColor:'#f5f6f7',
      flex:1,
    },
    navTop:{
      paddingHorizontal:20,
      backgroundColor:'#1a558f',
      shadowColor: '#000',
      paddingVertical:10,
      marginBottom:20,
      shadowOffset: { width: 0, height: 1 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 5
    },
    rowsBeetwen:{
      flexDirection:'row',
      paddingBottom: 20,
      paddingHorizontal: 20,
      justifyContent:'space-between',
      alignSelf:'stretch'
    },
    titlePage:{
      fontSize:17,
      borderRadius:15,
      color:'#1a558f',
      marginLeft:20,
      fontWeight:'bold',
      paddingVertical:5,
      paddingHorizontal:10,
      backgroundColor:"#fff",
      textTransform:'uppercase'
    },
    tabsActive:{
      fontSize:17,
      borderRadius:15,
      color:'#fff',
      fontWeight:'bold',
      paddingVertical:5,
      paddingHorizontal:10,
      backgroundColor:"#fcdf00",
      textAlign:'center'
    },
    tabsMenu:{
      fontSize:17,
      borderRadius:15,
      color:'#fcdf00',
      marginLeft:20,
      fontWeight:'bold',
      paddingVertical:5,
      paddingHorizontal:10,
      backgroundColor:"transparent"
    },
    rows:{
      paddingHorizontal:16,
      paddingVertical:8,
    },
    boxShadow:{
      marginHorizontal:0,
      borderRadius:15,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.8,
      shadowRadius: 2,
      elevation: 1,
      overflow:'hidden',
    },
    boxInner_bt:{
      paddingHorizontal:15,
      backgroundColor:'#fff',
      paddingVertical:15,
      borderRadius:15,
    },
    flexRows:{
      flexDirection:'row',
      marginBottom:10,
      alignItems:'center'
    },
    flexRowsBeetwen:{
      flexDirection:'row',
      marginBottom:0,
      justifyContent:'space-between',
      alignSelf:'stretch'
    },
    flexRowsLast:{
      flexDirection:'row',
      marginBottom:0,
      alignItems:'center'
    },
    rows2:{
      flex:1,
      marginBottom:10,
      position:'relative'
    },
    captionForm:{
      marginLeft:15
    },
    smallText:{
      fontSize:15,
      color:'#1a558f',
      marginBottom:5
    },
    BigText:{
      fontSize:17,
      fontWeight:'bold',
      color:'#1a558f',
    },
    button:{
      backgroundColor:'#1a558f',
      borderRadius:20,
      marginBottom:40,
      marginTop:5,
      width:300,
      paddingVertical:10,
    },
    alCenter:{
      alignItems:'center',
      marginTop:10,
    },
    buttonText:{
      fontSize:16,
      fontWeight:'500',
      color:'#fff',
      textAlign:'center',
    },
    searchInput:{
      alignSelf:'stretch',
      backgroundColor:'#fff',
      borderRadius:20,
      paddingVertical:5,
      color:"#1a558f"
    }

}
