import Dimensions from 'react-native';

export default DashboardStyle = {
    container: {
    },
    topcard: {backgroundColor: '#00b894',flexDirection: 'row', justifyContent: "space-between", justifyContent: 'center', paddingTop: 16, borderRadius: 12},
    topcard2: {flexDirection: 'row', justifyContent: "space-between"},
    carditem: {width: 100, height: 110, backgroundColor: 'white', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'},
    textwelcome: {
      fontSize: 14,
      marginBottom: 12,
      color: '#1a558f'
    },
    textSaldo: {
      fontSize: 18,
      marginBottom: 12,
      color: '#1a558f',
      fontWeight:'bold'
    },
    fitImage: {
      borderRadius: 40,
    },
    fitImageWithSize: {
      height: 100,
      width: 30,
    },
    
    textcard: {flex: 1,fontSize: 10, fontWeight: 'bold'},
    imagecard: {marginTop:12, marginBottom: 12, width: 56, height: 56},
    imagePromo: {
      width: 48, height: 48
    },
    bannerPromo: {
      borderRadius: 3,
      marginTop: 6,
      width: '100%', height: 175, resizeMode: 'cover'
    },
    listDate: {
      flexDirection: 'column',
      marginLeft: 16,
      width: 200
    },
    carditem2: {
      marginBottom: 6,
      backgroundColor: 'white',
      flexDirection: 'row'
    },
    infoTop: {
      flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'
    },
  footer:{
    height:70,
    backgroundColor:'#1a558f',
  },
  container2: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  barContainer: {
    position: 'absolute',
    zIndex: 2,
    bottom: 10,
    flexDirection: 'row',
  },
  track: {
    backgroundColor: 'transparent',
    overflow: 'hidden',
    height: 2,
    borderRadius:50,
    width:10,
    height: 10,
    borderWidth: 1,
    borderColor: '#fff',
  },
  bar: {
    backgroundColor: '#fff',
    width:10,
    height: 10,
    position: 'absolute',
    left: 0,
    top: 0,
    borderRadius:50,
  },
  container:{
    flex:1,
  },
  topHome:{
    backgroundColor:"#1a558f",
    flexDirection:'row',
    height: 70,
    paddingHorizontal:20,
    marginBottom:0,
    justifyContent:'space-between',
    alignItems: 'center'
  },
  logoHome:{
    width:130,
    alignItems:'center',
    justifyContent:'center',
  },
  textName:{
    flex:1,
    marginLeft:15
  },
  userName:{
    color:'#6b6b6b',
    fontSize:16,
    fontWeight:'bold',
    marginBottom:5
  },
  date:{
    color:'#6b6b6b',
    fontSize:14,
  },
  boxShadow:{
    borderRadius: 16,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 1,
    overflow:'hidden',
    marginBottom:16,
    marginTop: 16,
  },
  boxShadow2:{
    shadowColor: "#000",
    shadowOffset: {
    width: 0,
    height: 7,
    },
    shadowOpacity: 0.41,
    shadowRadius: 9.11,

    elevation: 14,
  },
  topBox:{
    flexDirection:'row',
    paddingHorizontal:10,
    justifyContent:'space-between',
    backgroundColor:'#fff',
    paddingVertical:10,
  },
  txtSaldo:{
    color:'#262626',
    fontSize:16,
    fontWeight:'bold',
  },
  txtSmall:{
    color:'#1a558f',
    fontSize:14,
    textAlign:'right'
  },
  bottomBox:{
    flexDirection:'row',
    paddingHorizontal:10,
    backgroundColor:'#3f5398',
    paddingVertical:15,
  },
  rowButton:{
    flex:1,
    marginHorizontal:15,
    alignItems:'center',
    justifyContent:'center',
  },
  rowButtonFoot:{
    marginHorizontal:10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowButtonTop:{
    marginHorizontal:0,
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
  },
  rowButtonTop2:{
    marginHorizontal:0,
  },
  rowButton2:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    position:'absolute',
    width:'100%',
    left:20,
    bottom:40,
  },
  textButton:{
    color:'#fff',
    fontSize:14,
    fontWeight:'bold',
  },
  boxInner:{
    flexDirection:'row',
    paddingHorizontal:15,
    backgroundColor:'#fff',
    paddingVertical:15,
    borderRadius:15,
  },
  boxInnerFooter:{
    flexDirection:'row',
    paddingHorizontal:15,
    paddingVertical:10,
    backgroundColor:'#1a558f',
    justifyContent:'space-between',
  },
  boxRow:{
    flexDirection:'row',
    marginBottom:15,
  },
  boxInner_bt:{
    paddingHorizontal:15,
    backgroundColor:'#fff',
    paddingVertical:15,
    borderRadius:15,
    marginBottom:5,
    
  },
  textButton2:{
    color:'#1a558f',
    fontSize:12,
    fontWeight:'bold',
    textAlign:'center',
  },
  textButtonfoot:{
    color:'#fff',
    fontSize:12,
    fontWeight:'bold',
    marginTop:5,
    textAlign:'center'
  },
  leftText:{
    flex:1,
    justifyContent:'center',
  },
  textBold:{
    color:'#1a558f',
    fontSize:16,
    fontWeight:'bold',
  },
  blueText:{
    color:'#1a558f',
    fontSize:11,
  },
  btRight:{
    width:80,
    alignItems:'center',
    justifyContent:'center',
    marginLeft:2,
  },
  buttonGreen:{
    backgroundColor:'#3a4d87',
    borderRadius:5,
    width:80,
    height:25,
    paddingVertical:5 ,
  },
  buttonText:{
    fontSize:12,
    fontWeight:'500',
    color:'#fff',
    textAlign:'center',
  },
  innerSLider:{
    position:"relative",
    height:100,
    backgroundColor:"#fff",
  },
  textWlcome:{
    fontSize:15,
    color:'#000',
    fontWeight:'600'
  },
  rowRight:{
    flexDirection:'row',
  },
  buttonOrange:{
    backgroundColor:'#ff9602',
    borderRadius:25,
    width:80,
    paddingVertical:5,
  },
  buttonText:{
    fontSize:12,
    fontWeight:'500',
    color:'#fff',
    textAlign:'center',
  },
  roundRed:{
    width: 15,
    height: 15,
    position:'absolute',
    right:0,
    top:0,
    borderRadius: 100/2,
    backgroundColor: '#fb466d'
  },
  rows:{
    paddingHorizontal:20,
    paddingVertical:20,
  },
  greyContent:{
    flex:1,
    backgroundColor:'#f5f6f7',
    paddingVertical:20,
    paddingHorizontal:20
  },
  blueText:{
    color:'#1a558f',
    fontSize:16,
    fontWeight:'bold',
    marginRight:10,
  },
  strechRow:{
    flexDirection:'row',
    justifyContent:'space-between',
    marginBottom:15,
  },
  rowBanner:{
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  rowSection:{
    paddingHorizontal:15,
    position:'relative',
    backgroundColor:"#fff",
    paddingVertical:20,
  },
  rowNews:{
    borderBottomWidth:1,
    borderColor:"#eee",
  },
  rowNewsLast:{
    flexDirection:"row",
    marginBottom:7
  },
  bigGallery:{
    position:"relative",
    borderRadius:0,
    overflow:"hidden",
  },
  absOverflow:{
    position:"relative",
    left:0,
    top:0,
    right:0,
    bottom:0,
    margin:"auto",
    backgroundColor:"rgba(0,0,0,0.1)"
  },
  absTitle:{
      position:"relative",
      paddingVertical:10,
      paddingHorizontal:0,
      zIndex:2,
  },
  smallpara:{
    marginTop:10,
    fontSize:12,
    color:"#5a5a5a"
  },
  titleBig:{
    fontSize:18,
    fontWeight:"bold",
    color:"#000"
  }


  }