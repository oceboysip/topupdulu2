const colors = {
    theme: '#1A558F',
    primary: '#F7B800',
    lightGrey: '#EBECED',
    darkGrey: '#2A2A29',
    text: '#000000'
};

export default colors;
