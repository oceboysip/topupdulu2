import Color from '../screen/Color';

export default LoginStyle = {
    container:{
        flex:1,
        alignItems: 'center',
    },
    navTop:{
        paddingVertical:20,
        paddingHorizontal:20,
        flexDirection:'row',
        alignSelf:'stretch',
      },

    insideLogo:{
        height: 184,
        paddingTop:20,
        alignItems:'center',
        marginBottom:40,
    },

    innerLogin:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
    },

    inputBox:{
      width:300,
      borderColor:'transparent',
      borderRadius:0,
      borderBottomColor:'#b7b7b7',
      borderBottomWidth: 2,
      paddingVertical:5,
      fontSize:14,
      marginBottom:0,
      paddingLeft:0,
    },
    inputBoxPassword:{
        width:300,
        paddingTop:15,
        paddingHorizontal:15,
        fontSize:14,
        flexDirection:'row',
    },
    labelInput:{
      paddingLeft:0,
      fontSize:14
    },

    textInput:{
        textAlign:'left',width:300,fontSize:12,color:'#28368f',
    },

    button:{
        backgroundColor:Color.theme,
        borderRadius:20,
        marginBottom:25,
        marginTop:15,
        width:300,
        paddingVertical:10,
    },

    buttonText:{
        fontSize:18,
        fontWeight:'500',
        color:'#fff',
        textAlign:'center',
    },
    signupTextCont:{
        alignItems:'center',
        flexDirection:'row',
        marginBottom:5,
    },
    signupText:{
        color:'#949c9e',
        fontSize:16,
    },
    signupTextRef:{
        color:'#6BCF64',
        fontSize:16,
        fontWeight:'500',
    },
    versionText:{
        color:Color.theme,
        fontSize:15,
        marginTop:15,
    },
    boxShadow:{
        shadowColor: "#000",
        shadowOffset: {
        width: 0,
        height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
    },
    boxInner:{
      paddingHorizontal:0,
      paddingVertical:5,
      borderRadius:5,
    },
    groupForm:{
      position:'relative',
      marginBottom:15,
    }

  }