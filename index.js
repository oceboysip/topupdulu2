import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import Moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';

momentDurationFormatSetup(Moment);
console.disableYellowBox = true;

AppRegistry.registerComponent(appName, () => App);
