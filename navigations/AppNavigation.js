import { createStackNavigator, createAppContainer } from "react-navigation";
import LoginScreen from "../screen/LoginScreen";
import HomeDashboard from "../screen/HomeDashboard";

//USER
import RegisterScreen from '../screen/User/RegisterScreen';

//FLIGHT
import SearchFlightScreen from "../screen/Flight/SearchFlightScreen";
import HasilSearchScreen from "../screen/Flight/HasilSearchScreen";
import FlightClassDetail from "../screen/Flight/FlightClassDetail";
import FlightReviewBooking from "../screen/Flight/FlightReviewBooking";

//HOTEL
import HotelScreen from '../screen/Hotel/HotelScreen';
import HotelResultScreen from '../screen/Hotel/HotelResultScreen';
import DetailHotelScreen from '../screen/Hotel/DetailHotelScreen';
import SelectHotelRoom from '../screen/Hotel/SelectHotelRoom';
import DetailRoomHotel from '../screen/Hotel/DetailRoomHotel';
import HotelReviewBooking from '../screen/Hotel/HotelReviewBooking';

//REPORT
import OrderBooking from '../screen/Order/OrderBooking';
import BookingDetailFlight from '../screen/Order/BookingDetailFlight';
import BookingDetailHotel from '../screen/Order/BookingDetailHotel';
import BookingDetailPulsa from '../screen/Order/BookingDetailPulsa';
import BookingDetailBus from '../screen/Order/BookingDetailBus';
import BookingDetailTravel from '../screen/Order/BookingDetailTravel';
import BookingDetailAttraction from '../screen/Order/BookingDetailAttraction';
import BookingDetailPlnPrabayar from '../screen/Order/BookingDetailPlnPrabayar';
import BookingDetailPlnPascabayar from '../screen/Order/BookingDetailPlnPascabayar';
import BookingDetailPdamPascabayar from '../screen/Order/BookingDetailPdamPascabayar';
import BookingDetailTeleponPascabayar from '../screen/Order/BookingDetailTeleponPascabayar';
import BookingDetailPaketData from '../screen/Order/BookingDetailPaketData';
import BookingDetailVoucherGame from '../screen/Order/BookingDetailVoucherGame';

import SearchAirportScreen from "../screen/SearchAirportScreen";
import CalendarScreen from "../screen/CalendarScreen";
import CalendarPulangScreen from "../screen/CalendarPulangScreen";
import BookedTrainScreen from "../screen/BookedTrainScreen";
import SearchTerminalPulangScreen from "../screen/SearchTerminalPulangScreen";
import ReportKeretaScreen from "../screen/ReportKeretaScreen";
import BookedHotelScreen from "../screen/BookedHotelScreen";
import SearchHotelScreen from "../screen/SearchHotelScreen";
import CalendarCheckinScreen from "../screen/CalendarCheckinScreen";
import ReportHotelScreen from "../screen/ReportHotelScreen";
import HasilSearchHotelScreen from "../screen/HasilSearchHotelScreen";
import HotelDetailScreen from "../screen/HotelDetailScreen";
import PilihKamarScreen from "../screen/PilihKamarScreen";
import ReviewBookingScreen from "../screen/ReviewBookingScreen";

//PAYMENT
import PaymentScreen from '../screen/PaymentScreen';
import PaymentPerBank from '../screen/PaymentPerBank';
import PaymentDetail from '../screen/PaymentDetail';
import PaymentSucceed from '../screen/PaymentSucceed';

//BUS&PELNI
import BusScreen from '../screen/Bus&Pelni/BusScreen';
import BusResultScreen from '../screen/Bus&Pelni/BusResultScreen';
import SelectBusSeat from '../screen/Bus&Pelni/SelectBusSeat';
import SelectBusTerminal from '../screen/Bus&Pelni/SelectBusTerminal';
import BusReviewBooking from '../screen/Bus&Pelni/BusReviewBooking';

//UMROH&TRAVEL
import TravelScreen from '../screen/Travel/TravelScreen';
import RecommendedTravel from '../screen/Travel/RecommendedTravel';
import SelectTravelDeparture from '../screen/Travel/SelectTravelDeparture';
import ItineraryTravelScreen from '../screen/Travel/ItineraryTravelScreen';
import BookTravel from '../screen/Travel/BookTravel';
import BookRoom from '../screen/Travel/BookRoom';
import BookOverview from '../screen/Travel/BookOverview';
import TravelReviewBooking from '../screen/Travel/TravelReviewBooking';

//ATRACTION
import AttractionScreen from '../screen/Attraction/AttractionScreen';
import DetailAttraction from '../screen/Attraction/DetailAttraction';
import PopularAttraction from '../screen/Attraction/PopularAttraction';
import AllAttractionResult from '../screen/Attraction/AllAttractionResult';
import TicketAttractionScreen from '../screen/Attraction/TicketAttractionScreen';
import DetailAboutAttraction from '../screen/Attraction/DetailAboutAttraction';
import DateAttraction from '../screen/Attraction/DateAttraction';
import AttractionBookScreen from '../screen/Attraction/AttractionBookScreen';

//PPOB
import PPOBScreen from '../screen/PPOB/PPOBScreen';
import PulsaScreen from "../screen/PPOB/PulsaScreen";
import ReviewPulsaScreen from "../screen/PPOB/ReviewPulsaScreen";
import PlnScreen from '../screen/PPOB/PlnScreen';
import PdamScreen from '../screen/PPOB/PdamScreen';
import TeleponScreen from '../screen/PPOB/TeleponScreen';
import PaketDataScreen from '../screen/PPOB/PaketDataScreen';
import VoucherGameScreen from '../screen/PPOB/VoucherGameScreen';

//SHOP
import ShopScreen from '../screen/Shop/ShopDashboard';
import SearchShopScreen from "../screen/Shop/SearchShopScreen";
import DetailShopScreen from "../screen/Shop/DetailShopScreen";

//NEWS
import MainScreenNews from '../screen/News/MainScreenNews';
import NewsDetailScreen from "../screen/News/NewsDetailScreen";
import NewsListScreen from "../screen/News/NewsListScreen";
import NewsScreen from "../screen/News/NewsScreen";
import CaraPandangScreen from "../screen/News/CaraPandangScreen";

const AppNavigation = createStackNavigator(
  {
    Login: {
      screen: LoginScreen
    },
    RegisterScreen: {
      screen: RegisterScreen
    },
    ReviewBooking: {
      screen: ReviewBookingScreen
    },
    PilihKamar: {
      screen: PilihKamarScreen
    },
    HotelDetail: {
      screen: HotelDetailScreen
    },
    HasilSearchHotel: {
      screen: HasilSearchHotelScreen
    },
    ReportHotel: {
      screen: ReportHotelScreen
    },
    CalendarCheckin: {
      screen: CalendarCheckinScreen
    },
    SearchFlight: {
      screen: SearchFlightScreen
    },
    BookedTrain:{
      screen: BookedTrainScreen
    },
    ReportKereta:{
      screen: ReportKeretaScreen
    },
    SearchTerminalPulang:{
      screen: SearchTerminalPulangScreen
    },
    CalendarPulang: {
      screen: CalendarPulangScreen
    },
    Calendar: {
      screen: CalendarScreen
    },
    Dashboard: {
      screen: HomeDashboard
    },
    SearchAirport: {
      screen: SearchAirportScreen
    },
    HasilSearch:{
      screen: HasilSearchScreen
    },
    ShopScreen: { screen : ShopScreen },
    SearchShopScreen:{ screen: SearchShopScreen},
    DetailShopScreen:{ screen: DetailShopScreen},
    MainScreenNews: { screen: MainScreenNews },
    NewsDetailScreen:{ screen: NewsDetailScreen},
    NewsListScreen:{ screen: NewsListScreen},
    Berita:{screen:NewsScreen},
    CaraPandang:{screen:CaraPandangScreen},


    FlightClassDetail: { screen: FlightClassDetail },
    FlightReviewBooking: { screen: FlightReviewBooking },
    SearchHotel: {
      screen: SearchHotelScreen
    },
    BookedHotel: {
      screen: BookedHotelScreen
    },
    OrderBooking: { screen: OrderBooking },
    BookingDetailFlight: { screen: BookingDetailFlight },
    BookingDetailHotel: { screen: BookingDetailHotel },
    BookingDetailPulsa: { screen: BookingDetailPulsa },
    BookingDetailBus: { screen: BookingDetailBus },
    BookingDetailTravel: { screen: BookingDetailTravel },
    BookingDetailAttraction: { screen: BookingDetailAttraction },
    BookingDetailPlnPrabayar: { screen: BookingDetailPlnPrabayar },
    BookingDetailPlnPascabayar: { screen: BookingDetailPlnPascabayar },
    BookingDetailPdamPascabayar: { screen: BookingDetailPdamPascabayar },
    BookingDetailTeleponPascabayar: { screen: BookingDetailTeleponPascabayar },
    BookingDetailPaketData: { screen: BookingDetailPaketData },
    BookingDetailVoucherGame: { screen: BookingDetailVoucherGame },

    PaymentScreen: { screen: PaymentScreen },
    PaymentPerBank: { screen: PaymentPerBank },
    PaymentDetail: { screen: PaymentDetail },
    PaymentSucceed: { screen: PaymentSucceed },

    HotelScreen: { screen: HotelScreen },
    HotelResultScreen: { screen: HotelResultScreen },
    DetailHotelScreen: { screen: DetailHotelScreen },
    SelectHotelRoom: { screen: SelectHotelRoom },
    DetailRoomHotel: { screen: DetailRoomHotel },
    HotelReviewBooking: { screen: HotelReviewBooking },

    BusScreen: { screen: BusScreen },
    BusResultScreen: { screen: BusResultScreen },
    SelectBusSeat: { screen: SelectBusSeat },
    SelectBusTerminal: { screen: SelectBusTerminal },
    BusReviewBooking: { screen: BusReviewBooking },

    TravelScreen: { screen: TravelScreen },
    RecommendedTravel: { screen: RecommendedTravel },
    SelectTravelDeparture: { screen: SelectTravelDeparture },
    ItineraryTravelScreen: { screen: ItineraryTravelScreen },
    BookTravel: { screen: BookTravel },
    BookRoom: { screen: BookRoom },
    BookOverview: { screen: BookOverview },
    TravelReviewBooking: { screen: TravelReviewBooking },

    AttractionScreen: { screen: AttractionScreen },
    DetailAttraction: { screen: DetailAttraction },
    PopularAttraction: { screen: PopularAttraction },
    AllAttractionResult: { screen: AllAttractionResult },
    TicketAttractionScreen: { screen: TicketAttractionScreen },
    DetailAboutAttraction: { screen: DetailAboutAttraction },
    DateAttraction: { screen: DateAttraction },
    AttractionBookScreen: { screen: AttractionBookScreen },

    PPOBScreen: { screen: PPOBScreen },
    PulsaScreen: { screen: PulsaScreen },
    PaketDataScreen: { screen: PaketDataScreen },
    ReviewPulsa: { screen: ReviewPulsaScreen },
    PlnScreen: { screen: PlnScreen },
    PdamScreen: { screen: PdamScreen },
    TeleponScreen: { screen: TeleponScreen },
    VoucherGameScreen: { screen: VoucherGameScreen }
  },
  {
    navigationOptions: {
      headerVisible: false,
    },
  }
);

export default createAppContainer(AppNavigation);