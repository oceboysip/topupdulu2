import React from 'react';
import { Image, Platform, View } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import Styled from 'styled-components';

import Text from '../screen/Text';
import Color from '../screen/Color';

import NewsListScreen from '../screen/News/NewsListScreen';
import NewsScreen from '../screen/News/NewsScreen';
import CaraPandangScreen from '../screen/News/CaraPandangScreen';

const TabText = Styled(Text)`
    color: #FFFFFF;
    textAlign: center;
    fontSize: 17;
    paddingHorizontal: 4;
    color: ${props => props.color ? props.color : '#FFFFFF'};
`;

const NewsTabNavigator = createMaterialTopTabNavigator(
    {
        HOME: {
          screen: ({screenProps}) => <NewsListScreen {...screenProps} />
        },
        BERITA: {
          screen: ({screenProps}) => <NewsScreen {...screenProps} />
        },
        CARAPANDANG: {
          screen: ({screenProps}) => <CaraPandangScreen {...screenProps} />
        }
    },
    {
      defaultNavigationOptions: ({ navigation }) => ({
        tabBarLabel: ({ focused, horizontal, tintColor }) => <TabText color={tintColor} type='bold'>{navigation.state.routeName}</TabText>
      }),
        animationEnabled: true,
        swipeEnabled: true,
        tabBarPosition: 'top',
        tabBarOptions: {
            activeTintColor: Color.primary,
            inactiveTintColor: '#FFFFFF',
            upperCaseLabel: true,
            style: {
                height: 80,
                width: '88%',
                justifyContent: 'center',
                backgroundColor: Color.theme,
                paddingLeft: 16
            },
            indicatorStyle: {
                backgroundColor: 'transparent'
            },
            tabStyle: {
                width: null
            }
        },
        lazy: true
    }
)

export default createAppContainer(NewsTabNavigator);