import React, { Component } from 'react';
import { SafeAreaView, StyleSheet, ScrollView, View, Text, StatusBar, Platform } from 'react-native';
import { Header, LearnMoreLinks, Colors, DebugInstructions, ReloadInstructions } from 'react-native/Libraries/NewAppScreen';
import SplashScreen from 'react-native-splash-screen';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';

import AppNavigation from './navigations/AppNavigation';
import { persistor, store } from './state/redux';
import Color from './screen/Color';

export let navigation;

export default class App extends Component{
  constructor(props){
    super(props)
    this.state = {
      loading: false,
      bookid: null,
      Searchid: null,
      loading: false,
      user: null,
    }
  }
  
  componentDidMount() {
    SplashScreen.hide();
  }

  setUser = (user) => {
    this.setState({user: user})
  }

  setLoading = (loading) => {
    this.setState({loading: loading})
  }

  setBookid = (bookid) => {
    this.setState({bookid: bookid})
  }

  setSearchid = (Searchid) => {
    this.setState({Searchid: Searchid})
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <StatusBar backgroundColor={Color.theme} />
          <SafeAreaView style={{backgroundColor: Color.theme, flex: 1}}>
            <AppNavigation
              ref={nav => navigation = nav}
              screenProps={{
                ...this.state,
                setUser: this.setUser,
                setLoading: this.setLoading,
                setBookid: this.setBookid,
                setSearchid:this.setSearchid,
              }}
            />
          </SafeAreaView>
        </PersistGate>
      </Provider>
    )
  }
}
