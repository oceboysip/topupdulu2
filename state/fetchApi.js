import { constant } from '../constants';
import axios from 'axios';
import { store } from './redux';
import { NavigationActions, StackActions } from 'react-navigation';
import querystring from 'querystring';
import { navigation } from '../App';

let initialState = {
    data: null,
    error: null
}

const get = (params) => {
    const user_siago = store.getState()['user.auth'].user_siago;

    if (user_siago === null) logout(navigation);

    return axios(constant.api_url+'/'+params, {
      headers: { 'X-Auth-Token': user_siago.token }
    })
    .then(res => {
        console.log(res, 'response get data');

        if (res.status === 200) return { ...initialState, data: res.data };
        return { ...initialState, error: res };
    })
    .catch(err => {
        console.log(err.message, 'error get data');

        if (err.message === 'Request failed with status code 401') {
            logout(navigation);
        }
        
        return { ...initialState, error: err.message };
    })
  }

const post = (params, body, addedHeaders) => {
    const user_siago = store.getState()['user.auth'].user_siago;

    if (user_siago === null) logout(navigation);

    let headers = { 'X-Auth-Token': user_siago.token };

    if (addedHeaders) headers = { ...headers, ...addedHeaders };

    console.log(headers, 'pala fetchapi', body);

    return (
        axios.post(constant.api_url+'/'+params, querystring.stringify(body), {
            headers
        })
        .then(res => {
            console.log(res, 'response post data');
            
            if (res.status === 200) return { ...initialState, data: res.data };
            return { ...initialState, error: res };
        })
        .catch(err => {
            console.log(err.message, 'error post data');
            
            if (err.message === 'Request failed with status code 401') {
                logout(navigation);
            }
            
            return { ...initialState, error: err.message };
        })
    )
}

const logout = (navigation) => {
    store.dispatch({ type: 'USER.LOGOUT' });

    const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Login', params: { logout: true } })],
    });

    navigation.dispatch(resetAction);

    return;
}

export default { get, post };