import fetchApi from '../../fetchApi';

export const bookFlight = (contact, passengers, departureFlight, returnFlight) => {
    return (dispatch, getState) => {
        dispatch({ type: 'BOOKING.REQUEST_BOOKING' });

        const user = getState()['user.auth'].login.user;

        const departureSeatClass = getState()['selectedFlights'].departureSeatClass
        const returnSeatClass = getState()['selectedFlights'].returnSeatClass

        const passengersParams = passengers.map(pax => {
            return {
                type: pax.type,
                titleAbbr: pax.title,
                firstName: pax.firstName,
                lastName: pax.lastName,
                birthDate: pax.birthDate !== '' ? pax.birthDate : null,
                email: contact.email,
                countryCode: contact.countryCode,
                phone: contact.phone,
                idNumber: pax.idNumber !== '' ? pax.idNumber : '10000000',
                nationality: pax.nationality !== '' ? pax.nationality : 'ID',
                address: '',                      //add input adress
                postalCode: '',                    //add input postalCode
                passportExpire: "",                        //pax.passportExpire,
                passportNumber: "",                        //pax.passportNumber,
                passportOrigin: "IDN"                         //pax.passportOrigin,
            }
        });

        let flights = [];

        flights.push(buildFlightParams(departureFlight, departureSeatClass));

        if (returnFlight) {
            flights.push(buildFlightParams(returnFlight, returnSeatClass));
        }

        let body = {
            titleAbbr: contact.title,
            firstName: contact.firstName,
            lastName: contact.lastName,
            email: contact.email,
            countryCode: contact.countryCode,
            phone: contact.phone,
            address: '',  //                             added form
            postalCode: '',    //                             added form
            customerRemark: '',
            tripType: (returnFlight ? 'round_trip' : 'one_way'),
            originCode: departureFlight.segments[0].origin.code,
            destinationCode: departureFlight.segments[departureFlight.segments.length - 1].destination.code,
            finalAmount: (returnFlight ? (departureFlight.fare + returnFlight.fare) : departureFlight.fare)
        }

        let objKeysPassengers = Object.keys(passengersParams[0]);

        objKeysPassengers.forEach((name) => {
            body = {...body, ...intoObjString(passengersParams, 'passengers', name)};
        })

        let objKeysItems = Object.keys(flights[0]);

        objKeysItems.forEach((name) => {
            body = {...body, ...intoObjString(flights, 'items', name)};
        })

        console.log(body, 'body');
        
        return (
            fetchApi.post("transaksi/pesawat", body)
            .then(res => {
                if (res.data) {
                    dispatch({
                        type: 'BOOKING.ADD_BOOKING',
                        data: res.data.data.booking
                    });

                    if (user) {
                        dispatch({
                            type: 'USER.UPDATE_BOOKING_LOGIN',
                            data: res.data.data.booking
                        });
                    } else {
                        dispatch({
                            type: 'BOOKING.ADD_BOOKING_HISTORY',
                            data: res.data.data.booking
                        });
                    }
                } else {
                    dispatch({
                        type: 'BOOKING.ADD_BOOKING_ERROR',
                        data: res.error
                    });
                }
            })
            .catch(err => {
                console.log(err, 'err booking pesawat');
                dispatch({
                    type: 'BOOKING.ADD_BOOKING_ERROR',
                    data: err
                })
            })
        )
    }
}

const intoObjString = (arrayData, paramObj, nameObj) => {
    let newObj = {};
    arrayData.forEach((value, key) => {
        if (nameObj === 'segments') {
            let objKeysSegments = Object.keys(value[nameObj][0]);
            objKeysSegments.forEach((nameSegments) => {
                value[nameObj].forEach((valueSegments, keySegments) => {
                    newObj[`${paramObj}[${key}][${nameObj}][${keySegments}][${nameSegments}]`] = valueSegments[nameSegments];
                })
            })
        } else {
            newObj[`${paramObj}[${key}][${nameObj}]`] = value[nameObj];
        }
    });
    return newObj;
};

const buildFlightParams = (flight, classSelected) => {
    let segments = flight.segments.map((segment, index) => {
        return {
            no: index + 1,
            originCode: segment.origin.code,
            destinationCode: segment.destination.code,
            departsAt: segment.departsAt,
            arrivesAt: segment.arrivesAt,
            classCode: segment.classes[classSelected[index]].classCode,
            classType: segment.classes[classSelected[index]].classType, //                         aded for wahyu api's
            carrierCode: segment.carrierCode,
            flightNumber: segment.flightNumber,
            opSuffix: segment.opSuffix !== undefined ? segment.opSuffix : '',
        }
    });
    return {
        vendor: flight.vendor,
        segments: segments
    };
};