import gql from 'graphql-tag';
import graphClient from '../../apollo';

const getAirportsQuery = gql`{
  airports {
    code name cityName countryCode utcOffset
  }
}`;

export const getAirports = () => {
  return (dispatch, getState) => {
    dispatch({
      type: 'RESET_AIRPORTS'
    });
    dispatch({
      type: 'REQUEST_AIRPORTS'
    });

    graphClient
      .query({
        query: getAirportsQuery,
      })
      .then(res => {
        if (res.data.airports) {
          dispatch({
            type: 'ADD_AIRPORTS',
            data: res.data.airports,
          });
        }
      }).catch(reject => {
        dispatch({
          type: 'REQUEST_AIRPORTS_FAILED',
          data: reject,
        });
      });
  };
};
