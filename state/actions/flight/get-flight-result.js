import Moment from 'moment';
import gql from 'graphql-tag';
import graphClient from '../../apollo';

const getFlightResultQuery = gql`
  query(
    $departureDate: String!
    $returnDate: String
    $originCode: String!
    $destinationCode: String!
    $adult: Int!
    $child: Int!
    $infant: Int!
    $airlines: [FlightVendor!]!
    $class: String!
  ) {
    flightAvailability(
      departureDate: $departureDate
      returnDate: $returnDate
      originCode: $originCode
      destinationCode: $destinationCode
      adult: $adult
      child: $child
      infant: $infant
      airlines: $airlines
      class: $class
    ) {
      departures {
        flightId fare isMultiAirline vendor
        segments {
          departsAt arrivesAt airlineName airlineLogo carrierCode flightNumber opSuffix
          origin { code name cityName }
          destination { code name cityName }
          classes { classCode classType classFare baggage availableSeat }
        }
      }
      returns {
        flightId fare isMultiAirline vendor
        segments {
          departsAt arrivesAt airlineName airlineLogo carrierCode flightNumber opSuffix
          origin { code name cityName }
          destination { code name cityName }
          classes { classCode classType classFare baggage availableSeat }
        }
      }
    }
  }
`;

export function getFlightResult() {
  return (dispatch, getState) => {
    const { trip, departureDate, returnDate, origin, destination, adult, child, infant, flightClass, airlines } = getState().searchFlight;
    const milisecondsDelay = 300;
    let queue = 0;

    dispatch({ type: 'RESET_FLIGHT_RESULT' });

    dispatch({ type: 'REQUEST_FLIGHT_RESULT', totalAirlines: airlines.length });

    const variables = {
      departureDate: Moment(departureDate).format('YYYY-MM-DD'),
      originCode: origin.code,
      destinationCode: destination.code,
      adult,
      child,
      infant,
      class: flightClass,
    };
    
    if (trip !== 1) variables.returnDate = Moment(returnDate).format('YYYY-MM-DD');

    const updateFlightResult = (res, id) => {
      queue++;
      const timeout = setTimeout(async() => {
        if (id === getState().flightResult.requestId) await dispatch({ type: 'UPDATE_FLIGHT_RESULT', data: { departures: res.data.flightAvailability.departures, returns: res.data.flightAvailability.returns } });
        queue--;
        clearTimeout(timeout);
      }, queue * milisecondsDelay);
    };

    const updateFlightResultFailed = (airline, reject, id) => {
      queue++;
      const timeout = setTimeout(async() => {
        if (id === getState().flightResult.requestId) await dispatch({ type: 'REQUEST_FLIGHT_RESULT_FAILED', data: { airline, reject } });
        queue--;
        clearTimeout(timeout);
      }, queue * milisecondsDelay);
    };

    const flightResult = (airline, id) => {
      graphClient
        .query({
          query: getFlightResultQuery,
          variables: { ...variables, airlines: airline }
        })
        .then(res => {
          if (res.data.flightAvailability) updateFlightResult(res, id);
        }).catch(reject => {
          updateFlightResultFailed(airline, reject, id);
        });
    };

    for (const airline of airlines) flightResult(airline, getState().flightResult.requestId);
  };
}
