import fetchApi from '../../fetchApi';

export const teleponPascaBayar = (body) => {
    return (dispatch, getState) => {
        dispatch({ type: 'BOOKING.REQUEST_BOOKING' });

        const user = getState()['user.auth'].login.user;

        return (
          fetchApi.post("transaksi/telepon", body)
          .then(res => {
            if (res.data) {
              dispatch({
                type: 'BOOKING.ADD_BOOKING',
                data: res.data.data.Pay_Telepon_PascaBayar
              });
              
              if (user) {
                dispatch({
                  type: 'USER.UPDATE_BOOKING_LOGIN',
                  data: res.data.data.Pay_Telepon_PascaBayar
                });
              } else {
                dispatch({
                  type: 'BOOKING.ADD_BOOKING_HISTORY',
                  data: res.data.data.Pay_Telepon_PascaBayar
                })
              }
            } else {
              dispatch({
                type: 'BOOKING.ADD_BOOKING_ERROR',
                data: res.error
              });
            }
          })
          .catch(err => {
            console.log(err, 'err booking telepon pascabayar');
            dispatch({
              type: 'BOOKING.ADD_BOOKING_ERROR',
              data: err
            });
          })
        )
    }
}