import fetchApi from '../../fetchApi';

export const pdamPascaBayar = (body) => {
    return (dispatch, getState) => {
        dispatch({ type: 'BOOKING.REQUEST_BOOKING' });

        const user = getState()['user.auth'].login.user;

        return (
          fetchApi.post("transaksi/pdam", body)
          .then(res => {
            if (res.data) {
              dispatch({
                type: 'BOOKING.ADD_BOOKING',
                data: res.data.data.Pay_PDAM
              });
              
              if(user){
                dispatch({
                  type: 'USER.UPDATE_BOOKING_LOGIN',
                  data: res.data.data.Pay_PDAM
                });
              }else {
                dispatch({
                  type: 'BOOKING.ADD_BOOKING_HISTORY',
                  data: res.data.data.Pay_PDAM
                })
              }
            } else {
              dispatch({
                type: 'BOOKING.ADD_BOOKING_ERROR',
                data: res.error
              });
            }
          })
          .catch(err => {
              console.log(err, 'err booking pdam pascabayar');
              dispatch({
                type: 'BOOKING.ADD_BOOKING_ERROR',
                data: err
              });
          })
        )
    }
}