import fetchApi from '../../fetchApi';

export const plnPraBayar = (body) => {
  return (dispatch, getState) => {
    dispatch({ type: 'BOOKING.REQUEST_BOOKING' });

    const user = getState()['user.auth'].login.user;

    return (
      fetchApi.post("transaksi/popobprepaid", body)
      .then(res => {
        if (res.data) {
          dispatch({
            type: 'BOOKING.ADD_BOOKING',
            data: res.data.data.PrepaidTransaction
          });
          
          if(user){
            dispatch({
              type: 'USER.UPDATE_BOOKING_LOGIN',
              data: res.data.data.PrepaidTransaction
            });
          }else {
            dispatch({
              type: 'BOOKING.ADD_BOOKING_HISTORY',
              data: res.data.data.PrepaidTransaction
            })
          }
        } else {
          dispatch({
            type: 'BOOKING.ADD_BOOKING_ERROR',
            data: res.error
          });
        }
      })
      .catch(err => {
          console.log(err, 'err booking pln prabayar');
          dispatch({
            type: 'BOOKING.ADD_BOOKING_ERROR',
            data: err
          });
      })
    )
  }
}

export const plnPascaBayar = (body) => {
  return (dispatch, getState) => {
    dispatch({ type: 'BOOKING.REQUEST_BOOKING' });

    const user = getState()['user.auth'].login.user;

    return (
      fetchApi.post("transaksi/pln", body)
      .then(res => {
        if (res.data) {
          dispatch({
            type: 'BOOKING.ADD_BOOKING',
            data: res.data.data.Pay_PLN_PascaBayar
          });
          
          if(user){
            dispatch({
              type: 'USER.UPDATE_BOOKING_LOGIN',
              data: res.data.data.Pay_PLN_PascaBayar
            });
          }else {
            dispatch({
              type: 'BOOKING.ADD_BOOKING_HISTORY',
              data: res.data.data.Pay_PLN_PascaBayar
            })
          }
        } else {
          dispatch({
            type: 'BOOKING.ADD_BOOKING_ERROR',
            data: res.error
          });
        }
      })
      .catch(err => {
          console.log(err, 'err booking pln pascabayar');
          dispatch({
            type: 'BOOKING.ADD_BOOKING_ERROR',
            data: err
          });
      })
    )
  }
}