import fetchApi from '../../fetchApi';

export const paketData = (data) => {
  return (dispatch, getState) => {
    dispatch({ type: 'BOOKING.REQUEST_BOOKING'});

    const user = getState()['user.auth'].login.user

    let body = {
      finalAmount: data.price,
      phoneNumber: data.number,
      pulsaCode: data.pulsa_code,
      autoDetect: 'false',
      pulsaPrice: data.price.toString(),
      category: 'PAKET_DATA'
    }

    console.log(body, data);

    return (
      fetchApi.post('transaksi/popobprepaid', body)
      .then(res => {
        if (res.data) {
          dispatch({
            type: 'BOOKING.ADD_BOOKING',
            data: res.data.data.PrepaidTransaction
          });
          
          if(user){
            dispatch({
              type: 'USER.UPDATE_BOOKING_LOGIN',
              data: res.data.data.PrepaidTransaction
            });
          }else {
            dispatch({
              type: 'BOOKING.ADD_BOOKING_HISTORY',
              data: res.data.data.PrepaidTransaction
            })
          }
        } else {
          dispatch({
            type: 'BOOKING.ADD_BOOKING_ERROR',
            data: res.error
          });
        }
      })
      .catch(err => {
        console.log(err, 'err booking pulsa');
        dispatch({
          type: 'BOOKING.ADD_BOOKING_ERROR',
          data: err
        });
      })
    )
  }
}