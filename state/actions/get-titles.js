import gql from 'graphql-tag';
import graphClient from '../apollo';

export const getTitles = () => {
    return (dispatch, getState) => {
        dispatch({
            type: 'REQUEST_TITLES'
        });
        graphClient
            .query({
                query: searchQuery,
            })
            .then(res => {
                if (res.data.titles) {
                    dispatch({
                        type: 'ADD_TITLES',
                        data: res.data.titles
                    });
                }
            }).catch(reject => {
                throw reject;
            });
    };
};

const searchQuery = gql`
{
  titles{
    id
    name
    abbr
    isAdult
  }
}`;
