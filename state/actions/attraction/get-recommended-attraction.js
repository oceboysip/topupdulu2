import gql from 'graphql-tag';
import Client from '../../apollo';

const getRecommendedAttractionQuery = gql`
  query(
    $param: paramAttraction!
  ){
    attractions(
      param: $param
    ){
      AttractionProducts{
        uuid
        title
        AttractionsProductsDetail {
          typename
          AttractionConfig {
            name
            value
          }
          baseprice
          currency {
            code
          }
          locations {
            city
            country
          }
          photos {
            path
          }
        }
      }
      AttractionConfig {
        name
        value
      }
    }
  }
`;

export const getRecommendedAttraction = (data) => {
  return (dispatch, getState) => {
    dispatch({
      type: 'RESET_RECOMMENDED_ATTRACTION'
    });

    dispatch({
      type: 'REQUEST_RECOMMENDED_ATTRACTION'
    })

    const variables = {
      param: {
        itemPerPage: data,
        page: 1,
        sort: {
          popular: 'desc'
        },
      }
    }

    Client
    .query({
      query: getRecommendedAttractionQuery,
      variables
    })
    .then(res => {
      const data = res.data.attractions;
      dispatch({
        type: 'ADD_RECOMMENDED_ATTRACTION',
        data
      })
    })
    .catch(err => {
      dispatch({
        type: 'REQUEST_RECOMMENDED_ATTRACTION_FAILED',
        data: err
      })
    })
  }
}
