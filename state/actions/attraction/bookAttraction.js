import fetchApi from '../../fetchApi';

export const bookAttraction = (contact, attractions, finalAmount) => {
  return (dispatch, getState) => {
    dispatch({ type: 'BOOKING.REQUEST_BOOKING' });

    const user = getState()['user.auth'].login.user;

    let body = {
      titleAbbr: contact.title,
      firstName: contact.firstName,
      lastName: contact.lastName,
      email: contact.email,
      countryCode: contact.countryCode,
      phone: contact.phone,
      address:'',
      postalCode:'',
      finalAmount,
      enc: attractions.enc,
      timeslot: attractions.timeslots.length === 0 ? attractions.timeslots : attractions.timeslots.uuid,
      adult: attractions.adult,
      child: attractions.child,
      senior: attractions.old
    }

    console.log(body, 'body');
        
    return (
      fetchApi.post('transaksi/attraction', body)
      .then(res => {
        if (res.data) {
          dispatch({
            type: 'BOOKING.ADD_BOOKING',
            data: res.data.data.booking
          });

          if (user) {
            dispatch({
              type: 'USER.UPDATE_BOOKING_LOGIN',
              data: res.data.data.booking
            });
          } else {
            dispatch({
              type: 'BOOKING.ADD_BOOKING_HISTORY',
              data: res.data.data.booking
            });
          }
        } else {
          dispatch({
            type: 'BOOKING.ADD_BOOKING_ERROR',
            data: res.error
          });
        }
      })
      .catch(err => {
        console.log(err, 'err booking atraksi');
        dispatch({
          type: 'BOOKING.ADD_BOOKING_ERROR',
          data: err
        });
      })
    )
  }
}