import fetchApi from '../../fetchApi';

export const bookTour = (contact, tours, finalAmount) => {
  return (dispatch, getState) => {
    dispatch({ type: 'BOOKING.REQUEST_BOOKING' });

    const user = getState()['user.auth'].login.user;

    const rooms = tours.rooms.map(room => {
      const travellers = room.travellers.map(traveller => {
        return {
          type: traveller.type,
          titleAbbr: traveller.title,
          firstName: traveller.firstName,
          lastName: traveller.lastName,
          birthDate: traveller.birthDate,
          priceType: traveller.priceType
        }
      });

      return {
        ...room,
        travellers,
      }
    });

    let body = {
      titleAbbr: contact.title,
      firstName: contact.firstName,
      lastName: contact.lastName,
      countryCode: contact.countryCode,
      phone: contact.phone,
      email: contact.email,
      address: '',                      //add input adress
      postalCode: '',                    // add postal code input
      finalAmount,
      tourId: tours.tourId,
      departureId: tours.departureId,
      isDp: tours.isDp,
      isIncludeVisa: tours.isIncludeVisa
    }

    let objKeysRooms = Object.keys(rooms[0]);

    objKeysRooms.forEach((name) => {
      body = {...body, ...intoObjString(rooms, 'rooms', name)};
    })

    console.log(body, 'body');

    return (
      fetchApi.post("transaksi/tour", body)
      .then(res => {
        if (res.data) {
          dispatch({
            type: 'BOOKING.ADD_BOOKING',
            data: res.data.data.booking
          });
          if(user) {
            dispatch({
              type: 'USER.UPDATE_BOOKING_LOGIN',
              data: res.data.data.booking
            });
          } else {
            dispatch({
              type: 'BOOKING.ADD_BOOKING_HISTORY',
              data: res.data.data.booking
            });
          }
        } else {
          dispatch({
            type: 'BOOKING.ADD_BOOKING_ERROR',
            data: res.error
          });
        }
      })
      .catch(err => {
          console.log(err, 'err booking tour');
          dispatch({
              type: 'BOOKING.ADD_BOOKING_ERROR',
              data: err
          })
      })
    )
  }
}

const intoObjString = (arrayData, paramObj, nameObj) => {
  let newObj = {};
  arrayData.forEach((value, key) => {
    if (nameObj === 'travellers') {
      let objKeysSegments = Object.keys(value[nameObj][0]);
      objKeysSegments.forEach((nameSegments) => {
        value[nameObj].forEach((valueSegments, keySegments) => {
          newObj[`${paramObj}[${key}][${nameObj}][${keySegments}][${nameSegments}]`] = valueSegments[nameSegments].toString();
        })
      })
    } else {
      newObj[`${paramObj}[${key}][${nameObj}]`] = value[nameObj].toString();
    }
  });
  return newObj;
};