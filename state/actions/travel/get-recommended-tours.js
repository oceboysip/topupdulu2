import gql from 'graphql-tag';
import graphClient from '../../apollo';

const getRecommendedToursQuery = gql`{
   highlightedTours {
     id name slug
     startingPrice { price discount finalPrice }
     departures {
       id date duration quota reserved
       startingPrice { price discount finalPrice }
       airlines { logoUrl name }
     }
     pictures { thumbnailUrl }
     tags {
       id name
       items { id name }
     }
   }
}
`;

export const getRecommendedTours = () => {
  return (dispatch, getState) => {
    dispatch({
      type: 'RESET_RECOMMENDED_TOURS'
    });
    dispatch({
      type: 'REQUEST_RECOMMENDED_TOURS'
    });

    graphClient
      .query({
        query: getRecommendedToursQuery,
        // fetchPolicy: 'no-cache'
      })
      .then(res => {
        if (res.data.highlightedTours) {
          dispatch({
            type: 'ADD_RECOMMENDED_TOURS',
            data: res.data.highlightedTours,
          });
        }
      }).catch(reject => {
        dispatch({
          type: 'REQUEST_RECOMMENDED_TOURS_FAILED',
          data: reject,
        });
      });
  };
};
