import gql from 'graphql-tag';
import graphClient from '../../apollo';

const getPopularDestinationsQuery = gql`{
  tourGroups { id name slug pictureUrl }
}
`;

export const getPopularDestinations = () => {
  return (dispatch, getState) => {
    dispatch({
      type: 'RESET_POPULAR_DESTINATIONS'
    });
    dispatch({
      type: 'REQUEST_POPULAR_DESTINATIONS'
    });

    graphClient
      .query({
        query: getPopularDestinationsQuery,
        // fetchPolicy: 'no-cache'
      })
      .then(res => {
        if (res.data.tourGroups) {
          dispatch({
            type: 'ADD_POPULAR_DESTINATIONS',
            data: res.data.tourGroups,
          });
        }
      }).catch(reject => {
        dispatch({
          type: 'REQUEST_POPULAR_DESTINATIONS_FAILED',
          data: reject,
        });
      });
  };
};
