import Client from '../../state/apollo';
import gql from "graphql-tag";

export const getStatusBooking = (id) => {
    return async (dispatch, getState) => {
        dispatch({
            type: 'BOOKING.REQUEST_BOOKING_STATUS'
        });
        const login = getState()['user.auth'].login;
        Client.query({
                query: searchQuery,
                variables: {
                    bookingId: id
                }
            })
            .then(res => {
                if(res.data.bookingDetail) {
                    if (login.user ) {
                        dispatch({
                            type: 'USER.UPDATE_BOOKING_STATUS',
                            id: id,
                            data: res.data.bookingDetail.bookingStatus
                        });
                    } else {
                        dispatch({
                            type: 'BOOKING.UPDATE_BOOKING_HISTORY',
                            id: id,
                            data: res.data.bookingDetail.bookingStatus
                        });
                    }
                }
            }).catch(error => {
                dispatch({
                    type: 'BOOKING.ADD_BOOKING_ERROR',
                    error: error
                });
            });
    }
}

export const getDetailBooking = (id) => {
    return async (dispatch, getState) => {
        dispatch({
            type: 'BOOKING.REQUEST_BOOKING_STATUS'
        });
        const login = getState()['user.auth'].login;
        Client
            .query({
                query: detailQuery,
                variables: {
                    bookingId: id
                }
            })
            .then(res => {
                if(res.data.bookingDetail) {
                    dispatch({
                        type: 'BOOKING.ADD_BOOKING',
                        data: res.data.bookingDetail
                    })
                    if (login.user ) {
                        dispatch({
                            type: 'USER.UPDATE_BOOKING_STATUS',
                            id: id,
                            data: res.data.bookingDetail.bookingStatus
                        });
                    } else {
                        dispatch({
                            type: 'BOOKING.UPDATE_BOOKING_HISTORY',
                            id: id,
                            data: res.data.bookingDetail.bookingStatus
                        });
                    }
                }

            }).catch(error => {
                dispatch({
                    type: 'BOOKING.ADD_BOOKING_ERROR',
                    error: error
                });
            });
    }
}

const REQUEST_THRESHOLD = 3;
const CANCELED_STATUS = 6;
let numberOfRetry = 0;

export const bookingCancel = (id) => {
    return async (dispatch, getState) => {
        let response;
        dispatch({
            type: 'BOOKING.CLEAR_BOOKING_CANCEL'
        });
        dispatch({
            type: 'BOOKING.REQUEST_BOOKING_CANCEL'
        });

        try {
            const res = await Client.mutate({
                mutation: cancelQuery,
                fetchPolicy: 'no-cache',
                variables: {
                    bookingId: id
                }
            });
            const login = getState()['user.auth'].login;
            if(res.data.cancelBooking && res.data.cancelBooking.bookingStatus.id === CANCELED_STATUS) {
                restart();
                if (login.user ) {
                    dispatch({
                        type: 'USER.UPDATE_BOOKING_STATUS',
                        id: id,
                        data: res.data.cancelBooking.bookingStatus
                    });
                    dispatch({
                        type: 'BOOKING.CLEAR_BOOKING_CANCEL'
                    });
                } else {
                    dispatch({
                        type: 'BOOKING.UPDATE_BOOKING_HISTORY',
                        id: id,
                        data: res.data.cancelBooking.bookingStatus
                    });
                }

            } else {
                await dispatch(retry(id));
            }
        } catch (error) {
            await dispatch(retry(id));

        }
    }
}

const retry = (id) => {
    numberOfRetry++;

    if (numberOfRetry < REQUEST_THRESHOLD) return bookingCancel(id);
    restart();
    return {
        type: 'BOOKING.ADD_BOOKING_ERROR',
        error: 'Maaf, Pesanan anda gagal dibatalkan'
    };
};

const restart = () => {
    numberOfRetry = 0;
};

const detailQuery = gql`
query(
    $bookingId: Int!
) {
    bookingDetail(
        bookingId: $bookingId
    ){
        id
        userId
        invoiceNumber
        contact{
            title{
                name
                abbr
            }
            firstName
            lastName
            email
            countryCode
            phone
            address
            postalCode
        }
        bookingStatus{
            id
            name
        }
        amount
        discount
        vat
        finalAmount
        payments{
            type {
                id
                name
            }
            amount
            discount
            transactionFee
            finalAmount
            paidAt
            transferAccountNumber
        }
        expiresAt
        flights{
            departure{
                bookingCode
                fareDetail
                total
                journeys{
                    origin{
                        code
                        name
                        cityName
                        countryCode
                    }
                    destination{
                        code
                        name
                        cityName
                        countryCode
                    }
                    airline{
                        name
                        code
                        logoUrl
                    }
                    departsAt
                    arrivesAt
                    flightNumber
                    seatClass
                }
            }
            return{
                bookingCode
                fareDetail
                total
                journeys{
                    origin{
                        code
                        name
                        cityName
                        countryCode
                    }
                    destination{
                        code
                        name
                        cityName
                        countryCode
                    }
                    airline{
                        name
                        code
                        logoUrl
                    }
                    departsAt
                    arrivesAt
                    flightNumber
                    seatClass
                }
            }
            passengers{
                type
                title{
                    name
                    abbr
                }
                firstName
                lastName
                birthDate
                nationality
                idNumber
                phone
                passport{
                    number
                    expirationDate
                    issuingCountry
                }

            }
        }
        tours {
            rooms {
                extraBed
                travellers {
                    type
                    priceType
                    titleAbbr
                    firstName
                    lastName
                    birthDate
                }
            }
            tourSnapshot{
              id
              name
              slug
              departure{
                date
                duration
                airlines{
                  name
                  logoUrl
                  code
                }
              }
            }
            price {
                vat
                airportTaxAndFuel
                visa
                discount
                total
            }
            priceDetail{
                dp
                isDp
                isIncludeVisa
                vatPercent
                specs{
                    type
                    count
                    total
                }
            }
            paymentAmount
        }
        hotels {
            HotelConfig{
                name
                value
            }
            id
            bookingId
            bookingDate
            checkInDate
            checkOutDate
            hotelName
            hotelBoardName
            amount
            markup
            discount
            additionalFee
            transactionFee
            ppn
            finalAmount
            hotelCategory
            cancellationAmount
            rateClassName
            hotelImage
            hotelCustomerRemark
            hotelAdult
            hotelChild
            hotelInfant
            hotelRoomCodeName
            hotelAddress
        }
        attractions{
          paxPrice{
            adults
            children
            seniors
          }
          timeslottxt
          message
          adults
          children
          seniors
          arrivaldate
          attraction_name
          confirm_date
          status
          discount
          ppn
          final_amount
        }
        buses {
          booking_code
          booking_id
          order_id
          seats {
            busesCode
            busesName
            departureCode
            arrivalCode
            departureName
            arrivalName
            departureDate
            arrivalDate
            seatPosition
            rateCode
            date
            type
            boardingPoint{
              pointId
              pointName
              pointTime
              pointAddress
              pointLandmark
              pointContact
            }
            droppingPoint{
              pointId
              pointName
              pointTime
              pointAddress
              pointLandmark
              pointContact
            }
            passenger{
              title
              fullname
              name
              surname
              birthdate
              address
              city
              country
              postal_code
              phone_number
              age
              email
              id_card
            }
          }
        }
        prepaidTransaction {
          id
          orderId
          refId
          status
          code
          destinationNumber
          price
          message
          balance
          tr_id
          rc
          category
          order_status
          request_data
          response_data
          lastUpdated
        }
    }
  }`
;

const searchQuery = gql`
query(
    $bookingId: Int!
) {
    bookingDetail(
        bookingId: $bookingId
    ){
        id
        userId
        bookingStatus{
            id
            name
        }
    }
}`;

const cancelQuery = gql`
mutation cancelBooking(
  $bookingId: Int!
) {
 cancelBooking( bookingId: $bookingId ) { bookingStatus { id name } }
}
`;
