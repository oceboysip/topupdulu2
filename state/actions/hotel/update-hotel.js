export const updateHotels = (data) => {
    return (dispatch, getState) => {
        dispatch({
            type: 'LOADING'
        });
        dispatch({
            type: 'UPDATE_HOTEL_RESULT',
            data
        });
    };
};
