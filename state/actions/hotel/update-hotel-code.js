export const updateHotelCode = (data) => {
    return (dispatch, getState) => {
        dispatch({
            type: 'LOADING'
        });
        dispatch({
            type: 'UPDATE_HOTEL_CODE',
            data
        });
    };
};
