import fetchApi from '../../fetchApi';

export const bookHotel = (contact, hotels, finalAmount) => {
    return (dispatch, getState) => {
        dispatch({ type: 'BOOKING.REQUEST_BOOKING' });

        const user = getState()['user.auth'].login.user;

        let body = {
            titleAbbr: contact.title,
            firstName: contact.firstName,
            lastName: contact.lastName,
            email: contact.email,
            countryCode: contact.countryCode,
            phone: contact.phone,
            address: '',                    //add adress
            postalCode: '',                    //add postalCode
            rateEnc: hotels,
            customerRemark: '',                     //pesan pesan
            finalAmount
        }

        console.log(body, 'body');

        return (
            fetchApi.post("transaksi/hotel", body)
            .then(res => {
                if (res.data) {
                    dispatch({
                        type: 'BOOKING.ADD_BOOKING',
                        data: res.data.data.booking
                    });

                    if (user) {
                        dispatch({
                            type: 'USER.UPDATE_BOOKING_LOGIN',
                            data: res.data.data.booking
                        });
                    } else {
                        dispatch({
                            type: 'BOOKING.ADD_BOOKING_HISTORY',
                            data: res.data.data.booking
                        });
                    }
                } else {
                    dispatch({
                        type: 'BOOKING.ADD_BOOKING_ERROR',
                        data: res.error
                    });
                }
            })
            .catch(err => {
                console.log(err, 'err booking hotel');
                dispatch({
                    type: 'BOOKING.ADD_BOOKING_ERROR',
                    data: err
                })
            })
        )
    }
}