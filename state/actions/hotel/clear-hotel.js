export const clearHotels = () => {
    return (dispatch, getState) => {
        dispatch({
            type: 'LOADING'
        });
        dispatch({
            type: 'CLEAR_HOTEL_RESULT'
        });
    };
};
