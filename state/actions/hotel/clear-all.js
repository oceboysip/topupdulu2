export const clearAll = (data) => {
    return (dispatch, getState) => {
        dispatch({
            type: 'LOADING'
        });
        dispatch({
            type: 'CLEAR_HOTEL_ALL',
            data
        });
    };
};
