export const updateInputUser = (data) => {
    return (dispatch, getState) => {
        dispatch({
            type: 'LOADING'
        });
        dispatch({
            type: 'UPDATE_INPUT_USER',
            data
        });
    };
};
