import Moment from 'moment';
import gql from 'graphql-tag';
import graphClient from '../../apollo';

const getBusResultQuery = gql`
  query(
    $availDate: String
    $stationDeparture: String
    $stationArrival: String
  ){
    busAvail(
    availDate: $availDate
    stationDeparture: $stationDeparture
    stationArrival: $stationArrival
  ){
    data{
      base_final_price
      base_additional_price
      station_departure_code
      seat_avail
      base_ppn
      buses_code
      buses_name
      base_transaction_fee
      seat_code
      avail_date
      base_discount
      base_price
      station_arrival_code
      logo
      type
      row
      column
      departure_time
      arrival_time
    }
  }
}
`;

export function getBusResult(){
  return(dispatch, getState) => {
    const { trip, departureDate, returnDate, origin, destination } = getState().searchBus;
    const milisecondsDelay = 300;
    let queue = 0;
    let secondRes = [], secondErr = []

    dispatch({ type: 'RESET_BUS_RESULT' });

    dispatch({ type: 'REQUEST_BUS_RESULT' });

    const variables = {
      availDate: Moment(departureDate).format('YYYY-MM-DD'),
      stationDeparture: origin.code,
      stationArrival: destination.code
    };

    const updateBusResult = (response, id) => {
      queue++;
      const timeout = setTimeout(async() => {
        if (id === getState().busResult.requestId) {
          await dispatch({
            type: 'UPDATE_BUS_RESULT',
            data: {
              departures: response,
              returns: secondRes
          }})
        }
        queue--;
        clearTimeout(timeout)
      }, queue * milisecondsDelay)
    }

    const updateBusResultFailed = (err, id) => {
      queue++;
      const timeout = setTimeout(async() => {
        if (id === getState().busResult.requestId) {
          await dispatch({
            type: 'REQUEST_BUS_RESULT_FAILED',
            data: { err, secondErr}
          })
        }
        queue--;
        clearTimeout(timeout)
      }, queue * milisecondsDelay)
    }

    const busResult = (id) => {
      graphClient.query({
        query: getBusResultQuery,
        variables: { ...variables}
      }).then(res => {
        var data = res.data.busAvail.data;
        var response = []

        if (data) {
          for (var i = 0; i < data.length; i++) {
            if (data[i].seat_code !== 'null') response.push(data[i])
          }
        }
        updateBusResult(response, id)
      }).catch(err => {
        updateBusResultFailed(err, id)
      })


      if (trip !== 1) {
        const newVariables = {
          availDate: Moment(returnDate).format('YYYY-MM-DD'),
          stationDeparture: destination.code,
          stationArrival: origin.code
        }

        graphClient.query({
          query: getBusResultQuery,
          variables: { ...newVariables}
        }).then(res => {
          var data = res.data.busAvail.data;
          var response = []

          if (res.data.busAvail.data) {
            for (var i = 0; i < data.length; i++) {
              if (data[i].seat_code !== 'null') response.push(data[i])
            }
          }
          secondRes = response
        }).catch(err => {
          secondErr = err
        })
      }
    }

    busResult(getState().busResult.requestId);
  }
}
