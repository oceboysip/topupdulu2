import Moment from 'moment';
import fetchApi from '../../fetchApi';

export const bookBus = (contact, seats, departureBus, returnBus, finalAmount) => {
  return (dispatch, getState) => {

    dispatch({ type: 'BOOKING.REQUEST_BOOKING' });

    const user = getState()['user.auth'].login.user;

    let seatItem = [];
    let fullName = '';
    let age = '';

    for (var i = 0; i < seats.length; i++) {
      fullName = seats[i].passengers.firstName;
      if (seats[i].passengers.lastName !== '') fullName = fullName + ' ' + seats[i].passengers.lastName;
      if (seats[i].passengers.birthDate !== '') age = Moment(new Date()).diff(seats[i].passengers.birthDate, 'year').toString()

      seatItem.push({
        busesCode: seats[i].busesCode,
        busesName: seats[i].busesName,
        departureCode: seats[i].departureCode,
        arrivalCode: seats[i].arrivalCode,
        departureName: seats[i].departureName,
        arrivalName: seats[i].arrivalName,
        departureDate: seats[i].departureDate,
        arrivalDate: seats[i].arrivalDate,
        seatPosition: seats[i].seatPosition,
        rateCode: seats[i].rateCode,
        date: Moment(seats[i].date).format('YYYY-MM-DD'),
        type: seats[i].type,
        boardingPoint: {
          pointId: seats[i].boardingPoint.pointId,
          pointName: seats[i].boardingPoint.pointName,
          pointTime: seats[i].boardingPoint.pointTime,
          pointAddress: seats[i].boardingPoint.pointAddress,
          pointLandmark: seats[i].boardingPoint.pointLandmark,
          pointContact: seats[i].boardingPoint.pointContact,
        },
        droppingPoint: {
          pointId: seats[i].droppingPoint.pointId,
          pointName: seats[i].droppingPoint.pointName,
          pointTime: seats[i].droppingPoint.pointTime,
          pointAddress: seats[i].droppingPoint.pointAddress,
          pointLandmark: seats[i].droppingPoint.pointLandmark,
          pointContact: seats[i].droppingPoint.pointContact,
        },
        passenger: {
          title: seats[i].passengers.title,
          fullname: fullName,
          name: seats[i].passengers.firstName,
          surname: seats[i].passengers.lastName,
          birthdate: seats[i].passengers.birthDate,
          address: seats[i].passengers.address,
          city: seats[i].passengers.city,
          country: seats[i].passengers.country,
          postal_code: seats[i].passengers.postalCode,
          phone_number: seats[i].passengers.phoneNumber,
          age: age,
          email: seats[i].passengers.email,
          id_card: seats[i].passengers.idCard
        }
      })
    }

    let body = {
      titleAbbr: contact.title,
      firstName: contact.firstName,
      lastName: contact.lastName,
      countryCode: contact.countryCode,
      phone: contact.phone,
      email: contact.email,
      address: '',
      postalCode: '',
      finalAmount
    };

    let objKeysSeat = Object.keys(seatItem[0]);

    objKeysSeat.forEach((name) => {
      body = {...body, ...intoObjString(seatItem, 'seat', name)};
    })

    console.log(body, 'body');

    return (
      fetchApi.post("transaksi/bus", body)
      .then(res => {
        if (res.data) {
          dispatch({
            type: 'BOOKING.ADD_BOOKING',
            data: res.data.data.booking
          });
          if(user) {
            dispatch({
              type: 'USER.UPDATE_BOOKING_LOGIN',
              data: res.data.data.booking
            });
          } else {
            dispatch({
              type: 'BOOKING.ADD_BOOKING_HISTORY',
              data: res.data.data.booking
            });
          }
        } else {
          dispatch({
            type: 'BOOKING.ADD_BOOKING_ERROR',
            data: res.error
          });
        }
      })
      .catch(err => {
        console.log(err, 'err booking tour');
        dispatch({
          type: 'BOOKING.ADD_BOOKING_ERROR',
          data: err
        })
      })
    )
  }
}

const intoObjString = (arrayData, paramObj, nameObj) => {
  let newObj = {};
  arrayData.forEach((value, key) => {
    if (nameObj === 'boardingPoint' || nameObj === 'droppingPoint' || nameObj === 'passenger') {
      let objKeysSegments = Object.keys(value[nameObj]);
      objKeysSegments.forEach((nameSegments) => {
        newObj[`${paramObj}[${key}][${nameObj}][${nameSegments}]`] = value[nameObj][nameSegments];
      })
    } else {
      newObj[`${paramObj}[${key}][${nameObj}]`] = value[nameObj];
    }
  });
  return newObj;
};