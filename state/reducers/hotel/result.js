const initialState = {
    loading: true,
    itemPerPage: 20,
    type: null,
    hotel: null,
    hotelCode: null,
    checkInDate: null,
    checkOutDate: null,
    guest: 2,
    room: 1,
    hotels: []
};

const hotels = (state = initialState, action) => {
    switch (action.type) {
        case 'LOADING':
          return {
              ...state,
              loading: true
          };
        case 'UPDATE_HOTEL_RESULT':
            return {
                ...state,
                loading: false,
                hotels: action.data,
            };
        case 'UPDATE_HOTEL_CODE':
            return {
                ...state,
                loading: false,
                hotelCode: action.data,
            };
        case 'UPDATE_INPUT_USER':
            return {
                ...state,
                loading: false,
                type: action.data.type ? action.data.type : state.type,
                hotel: action.data.hotel ? action.data.hotel : state.hotel,
                checkInDate: action.data.checkIn ? action.data.checkIn : state.checkInDate,
                checkOutDate: action.data.checkOut ? action.data.checkOut : state.checkOutDate,
                guest: action.data.guest ? action.data.guest : state.guest,
                room: action.data.room ? action.data.room : state.room,
            };
        case 'CLEAR_HOTEL_ALL':
          return {
                ...state,
                loading: false,
                type: null,
                hotel: null,
                hotelCode: null,
                checkInDate: null,
                checkOutDate: null,
                guest: 1,
                room: 1,
                hotels: []
          };
        case 'CLEAR_HOTEL_RESULT':
          return {
                ...state,
                loading: false,
                hotels: []
          };
        default:
            return state;
    }
};

export default hotels;
