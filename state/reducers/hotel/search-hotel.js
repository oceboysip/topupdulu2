const initialState = {
    lastSearch: [],
    favoriteSearch: [
      {
        code: "BAI",
        name: "Bali",
        country: "Indonesia",
        city: "Bali"
      },
      {
        code: "JAV",
        name: "Jakarta",
        country: "Indonesia",
        city: "Jakarta"
      },
      {
        code: "BDO",
        name: "Bandung",
        country: "Indonesia",
        city: "Bandung"
      },
      {
        code: "JOG",
        name: "Yogyakarta",
        country: "Indonesia",
        city: "Yogyakarta"
      },
      {
        code: "MLG",
        name: "Malang",
        country: "Indonesia",
        city: "Malang"
      }
    ]
};

const searchHotel = (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE_LAST_SEARCH_HOTEL': {
            console.log(action.params, 'aksi')
          let lastSearch = [action.params].concat(state.lastSearch);
          if (lastSearch.length > 5) lastSearch.splice(lastSearch.length - 1, 1);

          return {
            ...state,
            lastSearch
          }
        }
        case 'RESET_LAST_SEARCH_HOTEL': {
          return {
            ...state,
            lastSearch: []
          }
        }
        default:
          return state;
    }
};

export default searchHotel;