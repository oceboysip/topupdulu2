import AsyncStorage from '@react-native-community/async-storage';
import {persistReducer} from 'redux-persist';
import {combineReducers} from 'redux';

import Auth from './user/auth';
import booking from './booking';
import Titles from './titles';
//flight
import searchFlight from './flight/search-flight';
import flightResult from './flight/flight-result';
import selectedFlights from './flight/selected-flights';
//hotel
import hotels from './hotel/result';
import searchHotel from './hotel/search-hotel';
//bus & pelni
import searchBus from './bus/search-bus';
import busResult from './bus/bus-result';
import selectedBus from './bus/selected-bus';
//travel
import recommendedTours from './travel/recommended-tours';
import popularDestinations from './travel/popular-destinations';
//attraction
import recommendedAttraction from './attraction/recommended-attraction';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const combinedReducers = combineReducers({
  'user.auth': Auth,
  booking,
  titles: Titles,
  searchFlight,
  flightResult,
  selectedFlights,
  hotels,
  searchHotel,
  searchBus,
  busResult,
  selectedBus,
  recommendedTours,
  popularDestinations,
  recommendedAttraction
});

const reducers = persistReducer(persistConfig, combinedReducers);

export default reducers;
