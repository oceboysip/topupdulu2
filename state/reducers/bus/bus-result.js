const initialState = {
  requestId: 0,
  loading: false,
  result: { departures: [], returns: []},
  failed: [],
  progress: 0
};

const busResult = (state = initialState, action) => {
  switch (action.type) {
    case 'REQUEST_BUS_RESULT':
      return {
        ...state,
        requestId: state.requestId + 1,
        loading: true,
      }
    case 'UPDATE_BUS_RESULT': {
      const progress = state.progress + 1;
      const totalData = action.data.departures.concat(action.data.returns);

      return {
        ...state,
        result: { ...state.result, departures: state.result.departures.concat(action.data.departures), returns: state.result.returns.concat(action.data.returns)},
        progress,
        loading: false
      }
    }
    case 'REQUEST_BUS_RESULT_FAILED': {
      const progress = state.progress + 1;

      return {
        ...state,
        failed: state.failed.concat(action.data),
        progress,
        loading: false
      }
    }
    case 'RESET_BUS_RESULT':
      return {
        ...state,
        loading: false,
        result: { departures: [], returns: []},
        failed: [],
        progress: 0
      }
    default:
      return state;
  }
}

export default busResult;
