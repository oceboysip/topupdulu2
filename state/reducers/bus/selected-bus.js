const initialState = {
  departureBus: null,
  departureSeatBus: null,
  departureBpBus: null,
  departureDpBus: null,
  returnBus: null,
  returnSeatBus: null,
  returnBpBus: null,
  returnDpBus: null
}

const selectedBus = (state = initialState, action) => {
  switch (action.type) {
    case 'SELECTED_DEPARTURE_BUS':
      return {
        ...state,
        departureBus: action.data
      };
    case 'SELECTED_DEPARTURE_SEAT_BUS':
      return {
        ...state,
        departureSeatBus: action.data
      };
    case 'SELECTED_RETURN_BUS':
      return {
        ...state,
        returnBus: action.data
      };
    case 'SELECTED_RETURN_SEAT_BUS':
      return {
        ...state,
        returnSeatBus: action.data
      };
    case 'SELECTED_BPDP_DEPARTURE_BUS':
      console.log(action.data, 'dep bpdp');
      return {
        ...state,
        departureBpBus: action.data,
        departureDpBus: action.data
      }
    case 'SELECTED_BPDP_RETURN_BUS':
      return {
        ...state,
        returnBpBus: action.data,
        returnDpBus: action.data
      }
    case 'NO_RETURN_BUS':
      return {
        ...state,
        returnBus: null,
        returnSeatBus: null,
        returnBpBus: null,
        returnBpBus: null
      }
    case 'RESET_SELECTED_BUS':
      return initialState;
    default:
      return state;
  }
};

export default selectedBus;
