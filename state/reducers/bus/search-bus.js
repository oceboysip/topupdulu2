import Moment from 'moment';

let now = Moment();

const initialState = {
  departureDate: Moment(now),
  returnDate: Moment(now).add(3, 'd'),
  origin: null,
  destination: null,
  trip: null
};

const searchBus = (state = initialState, action) => {
  switch (action.type) {
    case 'UPDATE_SEARCH_BUSES': {
      const { trip, departureDate, returnDate, origin, destination } = action.params;
      return { ...state, trip, departureDate: Moment(departureDate), returnDate: Moment(returnDate), origin, destination };
    }
    case 'UPDATE_DEPARTURE_DATE_BUS': {
      return { ...state, departureDate: Moment(action.params) };
    }
    case 'UPDATE_RETURN_DATE_BUS': {
      return { ...state, returnDate: Moment(action.params) };
    }
    case 'RESET_SEARCH_BUSES':
      now = Moment();
      return {
        ...initialState,
        departureDate: Moment(now),
        returnDate: Moment(now).add(3, 'd')
      };
    default:
      return state;
  }
};

export default searchBus;
