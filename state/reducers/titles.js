const initialState = {
    fetching: false,
    titles: []
};

const titles = (state = initialState, action) => {
    switch (action.type) {
          case 'REQUEST_TITLES':
            return {
                ...state,
                fetching: true
            };
        case 'ADD_TITLES':
            return {
                fetching: false,
                titles: action.data
            };
        default:
            return state;
    }
};

export default titles;
