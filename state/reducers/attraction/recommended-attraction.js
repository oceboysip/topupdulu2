const initialState = {
  loading: false,
  AttractionProducts: [],
  attractionConfig: [],
  error: null
};

const recommendedAttraction = (state = initialState, action) => {
  switch (action.type) {
    case 'REQUEST_RECOMMENDED_ATTRACTION':
      return {
        ...state,
        loading: true
      }
    case 'ADD_RECOMMENDED_ATTRACTION':
      return {
        ...state,
        loading: false,
        AttractionProducts: action.data.AttractionProducts,
        attractionConfig: action.data.AttractionConfig,
        error: null
      }
    case 'REQUEST_RECOMMENDED_ATTRACTION_FAILED':
      return {
        ...state,
        loading: false,
        AttractionProducts: [],
        AttractionConfig: [],
        error: action.data
      }
    case 'RESET_RECOMMENDED_ATTRACTION':
      return initialState;
    default:
      return state;
  }
};

export default recommendedAttraction;
