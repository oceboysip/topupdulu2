const initialState = {
    loading: false,
    airports: null,
    error: null,
};

const airports = (state = initialState, action) => {
    switch (action.type) {
        case 'REQUEST_AIRPORTS':
          return {
            ...state,
            loading: true,
          };
        case 'ADD_AIRPORTS':
          return {
            ...state,
            loading: false,
            airports: action.data,
            error: null,
          };
        case 'REQUEST_AIRPORTS_FAILED':
          return {
            ...state,
            loading: false,
            airports: null,
            error: action.data,
          };
        case 'RESET_AIRPORTS':
          return initialState;
        default:
          return state;
    }
};

export default airports;
