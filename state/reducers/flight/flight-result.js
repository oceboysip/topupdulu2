const initialState = {
    requestId: 0,
    loading: false,
    totalAirlines: 0,
    result: { departures: [], returns: [] },
    failed: [],
    progress: 0,
    percentage: 0,
};

const flightResult = (state = initialState, action) => {
    switch (action.type) {
        case 'REQUEST_FLIGHT_RESULT':
          return {
            ...state,
            requestId: state.requestId + 1,
            loading: true,
            totalAirlines: action.totalAirlines,
          };
        case 'UPDATE_FLIGHT_RESULT': {
          const progress = state.progress + 1;
          return {
            ...state,
            result: { ...state.result, departures: state.result.departures.concat(action.data.departures), returns: state.result.returns.concat(action.data.returns) },
            progress,
            percentage: (100 * progress) / state.totalAirlines,
            loading: progress < state.totalAirlines
          };
        }
        case 'REQUEST_FLIGHT_RESULT_FAILED': {
          const progress = state.progress + 1;
          return {
            ...state,
            failed: state.failed.concat(action.data),
            progress,
            percentage: (100 * progress) / state.totalAirlines,
            loading: progress < state.totalAirlines
          };
        }
        case 'RESET_FLIGHT_RESULT':
          return {
            ...state,
            loading: false,
            totalAirlines: 0,
            result: { departures: [], returns: [] },
            failed: [],
            progress: 0,
            percentage: 0,
          };
        default:
          return state;
    }
};

export default flightResult;
