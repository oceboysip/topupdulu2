const initialState = {
  departureFlight: null,
  departureSeatClass: null,
  returnFlight: null,
  returnSeatClass: null,
};

const selectedFlights = (state = initialState, action) => {
    switch (action.type) {
        case 'SELECTED_DEPARTURE_FLIGHT':
          return {
            ...state,
            departureFlight: action.data,
          };
        case 'SELECTED_DEPARTURE_SEAT_CLASS':
          return {
            ...state,
            departureSeatClass: action.data,
          };
        case 'SELECTED_RETURN_FLIGHT':
          return {
            ...state,
            returnFlight: action.data,
          };
        case 'SELECTED_RETURN_SEAT_CLASS':
          return {
            ...state,
            returnSeatClass: action.data,
          };
        case 'NO_RETURN_FLIGHT':
          return {
            ...state,
            returnFlight: null,
            returnSeatClass: null,
          };
        case 'RESET_SELECTED_FLIGHTS':
          return initialState;
        default:
          return state;
    }
};

export default selectedFlights;
