import Moment from 'moment';

let now = Moment();

const initialState = {
    departureDate: Moment(now),
    returnDate: Moment(now).add(3, 'd'),
    origin: null,
    destination: null,
    adult: null,
    child: null,
    infant: null,
    airlines: ['AIRASIA', 'GARUDA', 'CITILINK', 'GALILEO', 'MALINDO', 'LION_AIR', 'SRIWIJAYA'],
    flightClass: 'ALL',
    trip: null,
    lastSearch: [],
    favoriteSearch: [
      {
        header: 'Pencarian Favorit'
      },
      {
        name: "Soekarno Hatta",
        cityName: "Jakarta",
        code: "CGK"
      },
      {
        name: "Ngurah Rai (Bali)",
        cityName: "Denpasar-bali Island",
        code: "DPS"
      },
      {
        name: "Adi Sumarmo Airport",
        cityName: "Solo",
        code: "SOC"
      },
      {
        name: "Kuala Namu Airport",
        cityName: "Medan Kuala Namu",
        code: "KNO"
      },
      {
        name: "Juanda Airport",
        cityName: "Surabaya",
        code: "SUB"
      }
    ]
};

const searchFlight = (state = initialState, action) => {
    switch (action.type) {
        case 'UPDATE_SEARCH_PARAMS': {
          const { trip, departureDate, returnDate, origin, destination, adult, child, infant, flightClass } = action.params;
          return { ...state, trip, departureDate: Moment(departureDate), returnDate: Moment(returnDate), origin, destination, adult, child, infant, flightClass };
        }
        case 'UPDATE_DEPARTURE_DATE_PARAMS': {
          return { ...state, departureDate: Moment(action.params) };
        }
        case 'UPDATE_RETURN_DATE_PARAMS': {
          return { ...state, returnDate: Moment(action.params) };
        }
        case 'RESET_SEARCH_PARAMS':
          now = Moment();
          return {
            ...initialState,
            departureDate: Moment(now),
            returnDate: Moment(now).add(3, 'd'),
          };
        case 'UPDATE_LAST_SEARCH_FLIGHT': {
          let lastSearch = [action.params].concat(state.lastSearch);
          if (lastSearch.length > 5) lastSearch.splice(lastSearch.length - 1, 1);

          return {
            ...state,
            lastSearch
          }
        }
        case 'RESET_LAST_SEARCH_FLIGHT': {
          return {
            ...state,
            lastSearch: []
          }
        }
        default:
          return state;
    }
};

export default searchFlight;
