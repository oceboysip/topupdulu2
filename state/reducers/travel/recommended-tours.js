const initialState = {
    loading: false,
    result: null,
    error: null,
};

const recommendedTours = (state = initialState, action) => {
    switch (action.type) {
        case 'REQUEST_RECOMMENDED_TOURS':
          return {
            ...state,
            loading: true,
          };
        case 'ADD_RECOMMENDED_TOURS':
          return {
            ...state,
            loading: false,
            result: action.data,
            error: null,
          };
        case 'REQUEST_RECOMMENDED_TOURS_FAILED':
          return {
            ...state,
            loading: false,
            result: null,
            error: action.data,
          };
        case 'RESET_RECOMMENDED_TOURS':
          return initialState;
        default:
          return state;
    }
};

export default recommendedTours;
