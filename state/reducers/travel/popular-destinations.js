const initialState = {
    loading: false,
    result: null,
    error: null,
};

const popularDestinations = (state = initialState, action) => {
    switch (action.type) {
        case 'REQUEST_POPULAR_DESTINATIONS':
          return {
            ...state,
            loading: true,
          };
        case 'ADD_POPULAR_DESTINATIONS':
          return {
            ...state,
            loading: false,
            result: action.data,
            error: null,
          };
        case 'REQUEST_POPULAR_DESTINATIONS_FAILED':
          return {
            ...state,
            loading: false,
            result: null,
            error: action.data,
          };
        case 'RESET_POPULAR_DESTINATIONS':
          return initialState;
        default:
          return state;
    }
};

export default popularDestinations;
