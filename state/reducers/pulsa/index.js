const initialState = {
  order: {},
  error: null,
  fetching: false
};

const orderPulsa = (state = initialState, action) => {
  switch (action.type) {
    case 'ORDER.REQUEST_ORDER':
        return {
          ...state,
          fetching: true,
          error: null
        };
        break;
    case 'ORDER.ADD_ORDER':
      return {
        ...state,
        order: action.data,
        fetching: false,
        error: null
      };
      break;
    case 'ORDER.ADD_ORDER_ERROR':
      return {
        ...state,
        fetching: false,
        error: action.data
      }
      break;
    default:
      return state;
  }
}
