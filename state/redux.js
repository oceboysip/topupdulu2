import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore } from 'redux-persist';
import thunk from 'redux-thunk';

import reducers from './reducers';

const enhancer = compose(
    applyMiddleware(thunk)
);

export const store = createStore(reducers, enhancer);

export const persistor = persistStore(store);