import React, { Component } from 'react';
import { TextInput, View, Text, Platform, TouchableWithoutFeedback, Image } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import PropTypes from 'prop-types';
import Styled from 'styled-components';
import { Row, Col } from '../Grid';
import Label from '../Label';
import Color from '../Color';
// import Text from '../Text'

const InputWrapper = Styled(View)`
    marginTop: 6;
    minHeight: 50;
    marginBottom: 16;
`

const SectionView = Styled(View)`
    flexDirection: column;
`;

const LabelView = Styled(View)`
    justifyContent: flex-start;
    alignItems: flex-start
    flex: 1;
`;

const LabelViewRight = Styled(View)`
    alignItems: flex-end;
`

const TextView = Styled(View)`
    marginBottom: 5;
    justifyContent: flex-end;
`;

const StyledInput = Styled(TextInput)`
    color: black;
    fontSize: ${props => props.size ? props.size : 14};
    borderColor: ${props => props.error ? 'red' : '#DDDDDD'};
    borderBottomWidth: 0.5;
    paddingBottom: ${Platform.OS === 'ios' ? 5 : 0};
    marginTop: 0;
    paddingTop: 0;
    paddingLeft: 0;
`;

const StyledSelect= Styled(View)`
    borderBottomColor: ${props => props.error ? 'red' : props.borderColor ? props.borderColor : '#DDDDDD'};
    borderBottomWidth: ${props => props.borderBottomWidth ? props.borderBottomWidth : '0.5'};
`

const TextSelect = Styled(Text)`
    textAlign: left;
    color: ${props => props.color ? props.color : '#231F20'};
    fontWeight: ${props => props.fontWeight ? props.fontWeight : 'normal'};
`

const TextUnSelect = Styled(TextSelect)`
`

const ErrorWrapper = Styled(View)`
`

const ErrorMessage = Styled(Text)`
    color: red;
    font-size: 12px;
    text-align: left;
`

const Section = Styled(Row)`
`

const IconView = Styled(View)`
    justifyContent: flex-end;
    paddingBottom: 8;
    flex: 1;
`;

const IconImage = Styled(Image)`
    width: 21;
    height: 21;
`;

const defaultProps = {
    componentName: 'Input',
    color: '#231F20',
    size: 14,
    type: 'normal'
}

const propTypes = {
}

class Input extends Component {
    constructor() {
        super();

        this.onSelect = this.onSelect.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.onChangeText = this.onChangeText.bind(this);
        this.onFocus = this.onFocus.bind(this);
        this.onPressRightLabel = this.onPressRightLabel.bind(this);

    }

    componentWillMount() {

    }

    onChangeText(value) {
        const { name } = this.props;
        this.props.onChangeValue(name, value);
        if (this.props.id !== undefined) {
            this.props.onChangeValue(name, value, this.props.id);
        } else {
          this.props.onChangeValue(name, value);
        }
    }

    onBlur() {
        const { name } = this.props;
        if (this.props.onBlurValue !== undefined) {

            if (this.props.id !== undefined) {
                this.props.onBlurValue(name, this.props.id);
            } else {
              this.props.onBlurValue(name);
            }
        }

    }

    onSelect() {
        const { modalName } = this.props;
        this.props.onSelect(modalName);
    }

    onFocus(event) {
        let { onFocus, clearTextOnFocus } = this.props;

        if ('function' === typeof onFocus) {
          onFocus(event);
        }

        if (clearTextOnFocus) {
          this.clear();
        }

        this.setState({ focused: true, receivedFocus: true });
    }

    // focus() {
    //     console.log('here');
    //     // let { disabled, editable } = this.props;

    //     // if (!disabled && editable) {

    //     // }
    //     this.input.focus();
    // }

    onPressRightLabel() {
        if (this.props.onPressRightLabel !== undefined) {
            this.props.onPressRightLabel();
        }
    }


    render() {
        const { label, IconName, customIcon, componentName, error, labelRight, color, type } = this.props;
        return (
            <InputWrapper>
            { componentName !== 'select' &&
                <SectionView>
                <Row style={{height: 16, marginBottom: 18}}>
                    {Platform.OS === 'ios' && <View style={{height: 16}} />}
                    {(IconName || customIcon) && <Col size={1}>
                        <IconView>
                        </IconView>
                    </Col> }
                    <Col size={(IconName || customIcon) ? 11 : 12}>
                        <View style={{flex: 1, flexDirection: 'row'}}>
                            {label ? <LabelView>
                                <Label>{label}</Label>
                            </LabelView> : <LabelView />}
                            {labelRight && <LabelViewRight>
                                <Label color="text" onPress={this.onPressRightLabel}>{labelRight}</Label>
                            </LabelViewRight> }
                        </View>
                    </Col>
                </Row>
                <Row>
                    {(IconName || customIcon) && <Col size={1}>
                        <IconView>
                            {customIcon && <IconImage
                                resizeMode={'contain'}
                                source={customIcon}
                            />}
                            {IconName && <Ionicons name={IconName} size={21} />}
                        </IconView>
                    </Col> }
                    <Col size={(IconName || customIcon) ? 11 : 12}>
                        <TextView error={error}>
                            <StyledInput
                                placeholderTextColor="#919294"
                                underlineColorAndroid="transparent"
                                autoCapitalize="none"
                                onChangeText={this.onChangeText}
                                autoCorrect={false}
                                onBlur={this.onBlur}
                                ref={this.props.inputRef}
                                {...this.props}
                            />
                        </TextView>
                    </Col>
                </Row>
                <Row>
                    {(IconName || customIcon) && <Col size={1}>
                        <IconView>
                        </IconView>
                    </Col> }
                    <Col size={(IconName || customIcon) ? 11 : 12}>
                        <ErrorWrapper>
                            { this.props.error &&
                                <ErrorMessage>
                                {this.props.error}
                                </ErrorMessage>
                            }
                        </ErrorWrapper>
                    </Col>
                </Row>
                </SectionView>
            }
            { componentName === 'select' &&
                <TouchableWithoutFeedback onPress={this.onSelect}>
                    <SectionView>
                    <Row style={{height: 16, marginBottom: 23}}>
                        {Platform.OS === 'ios' && <View style={{height: 16}} />}
                        {(IconName || customIcon) && <Col size={1}>
                            <IconView>
                            </IconView>
                        </Col> }
                        <Col size={(IconName || customIcon) ? 11 : 12}>
                            <View style={{flex: 1, flexDirection: 'row'}}>
                                {label ? <LabelView>
                                    {color ? <TextSelect color={color}>{label}</TextSelect> : <Label>{label}</Label>}
                                </LabelView> : <LabelView />}
                                {labelRight && <LabelViewRight>
                                    <Label color="text" onPress={this.onPressRightLabel}>{labelRight}</Label>
                                </LabelViewRight> }
                            </View>
                        </Col>
                    </Row>
                    <StyledSelect {...this.props}>
                        <Row>
                            <Col size={11}>
                                <TextView>
                                { this.props.value !== "" ?
                                    <TextSelect color={color} fontWeight={type}>{this.props.value}</TextSelect> :
                                    <TextUnSelect color={color} fontWeight={type}>{this.props.placeholder}</TextUnSelect>
                                }
                                </TextView>
                            </Col>
                            <Col size={1} style={{alignItems: 'flex-end', justifyContent: 'flex-end'}}>
                                <Ionicons name="ios-arrow-forward" size={12} style={[color && {color}, { paddingBottom: 5 }]}/>
                            </Col>
                        </Row>

                    </StyledSelect>
                    <Row>
                        <Col size={12}>
                            <ErrorWrapper>
                            { this.props.error &&
                                <ErrorMessage>
                                {this.props.error}
                                </ErrorMessage>
                            }
                            </ErrorWrapper>
                        </Col>
                    </Row>
                    </SectionView>
                </TouchableWithoutFeedback>
            }
            </InputWrapper>
        );
    }
}

Input.defaultProps = defaultProps;
Input.propTypes = propTypes;

export default Input;
