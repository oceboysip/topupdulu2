/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {Alert,Platform, StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions, Button,Switch,TouchableWithoutFeedback} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Dialog, {SlideAnimation, DialogContent } from 'react-native-popup-dialog';
import BottomSheet from 'reanimated-bottom-sheet'
import SlidingUpPanel from 'rn-sliding-up-panel';
import HasilSearchHotelStyle from '../style/HasilSearchHotelStyle';



import ToggleSwitch from "toggle-switch-react-native";

export default class HasilSearchHotelScreen extends Component<{}> {

  constructor(props){
  super(props)
    this.state = {
      isOnDefaultToggleSwitch: false,
      isOnLargeToggleSwitch: false,
      isOnBlueToggleSwitch: false,
      isHidden: false
    };
  }

  onToggle(isOn) {
    console.log("Changed to " + isOn);
  }
  BookedHotel = () => {
    this.props.navigation.navigate('BookedHotel');
  }
  HotelDetail = () => {
    this.props.navigation.navigate('HotelDetail');
  }

  render() {
    return (
      <View style={styles.container}>
      

        <View style={styles.navTop}>
          <View style={styles.rowsBeetwen}>
            <View style={{flexDirection: 'row',alignItems:'center'}}>
              <TouchableOpacity onPress={this.BookedHotel}>
                <Image style={{width:30, height:30,resizeMode:'contain'}} source={require('../images/left-arrow.png')}/>
              </TouchableOpacity>
              <Text style={styles.titlePage}>Hasil Pencarian</Text>
            </View>

            <View style={{flexDirection:'row'}}>
              <TouchableOpacity onPress={this.ReportHotel}>
                <Image style={{width:66, height:25, resizeMode:'contain'}}  source={require('../images/icon_list.png')}/>
               </TouchableOpacity>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.searchRow}>
              <TextInput placeholder="Cari Hotel" style={styles.searchInput} />
              <Image style={{width:30, height:30,resizeMode:'contain',position:'absolute', left:40,top:4}} source={require('../images/icon_search.png')}/>
            </View>
          </View>

        </View>
   

        
        <ScrollView>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                <View style={styles.imgThumb}>
                  <Image style={{width:370, height:162, resizeMode:'contain'}}  source={require('../images/hotel_thumb.jpg')}/>
                </View>

                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row',alignSelf:'stretch',flex:1}}>
                    <View style={styles.captionForm}>
                        <Text style={styles.BigText}>Lorin Sentul</Text>

                        <View style={{flexDirection:'row',alignSelf:'stretch',flex:1,marginTop:5, alignItems:'center'}}>
                          <Image style={{width:15, height:21, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_place.png')}/>
                          <Text style={styles.smallText}>Sentul Circuit (Exit Toll KM. 32)</Text>
                        </View>
                        <View style={{flexDirection:'row',alignSelf:'stretch',flex:1,marginTop:5, alignItems:'center'}}>
                          <Image style={{width:127, height:20, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_stars.png')}/>
                          <Text style={styles.smallText}>4/5</Text>
                        </View>
                    </View>
                  </View>
                  <View style={{paddingRight:15,paddingTop:10}}>
                    <Text style={styles.BigText2}>Rp 600.000</Text>
                    <Text style={styles.smallText}>1 Malam</Text>
                  </View>
                  <TouchableOpacity style={styles.bgGreenTextAbs} onPress={this.HotelDetail}>
                    <Text style={{color:"#fff",fontSize:18,fontWeight:'bold',PaddingHorizontal:20}}>PILIH</Text>
                  </TouchableOpacity>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                <View style={styles.imgThumb}>
                  <Image style={{width:370, height:162, resizeMode:'contain'}}  source={require('../images/hotel_thumb.jpg')}/>
                </View>

                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row',alignSelf:'stretch',flex:1}}>
                    <View style={styles.captionForm}>
                        <Text style={styles.BigText}>Lorin Sentul</Text>

                        <View style={{flexDirection:'row',alignSelf:'stretch',flex:1,marginTop:5, alignItems:'center'}}>
                          <Image style={{width:15, height:21, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_place.png')}/>
                          <Text style={styles.smallText}>Sentul Circuit (Exit Toll KM. 32)</Text>
                        </View>
                        <View style={{flexDirection:'row',alignSelf:'stretch',flex:1,marginTop:5, alignItems:'center'}}>
                          <Image style={{width:127, height:20, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_stars.png')}/>
                          <Text style={styles.smallText}>4/5</Text>
                        </View>
                    </View>
                  </View>
                  <View style={{paddingRight:15,paddingTop:10}}>
                    <Text style={styles.BigText2}>Rp 600.000</Text>
                    <Text style={styles.smallText}>1 Malam</Text>
                  </View>
                  <TouchableOpacity style={styles.bgGreenTextAbs} onPress={this.HotelDetail}>
                    <Text style={{color:"#fff",fontSize:18,fontWeight:'bold',PaddingHorizontal:20}}>PILIH</Text>
                  </TouchableOpacity>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                <View style={styles.imgThumb}>
                  <Image style={{width:370, height:162, resizeMode:'contain'}}  source={require('../images/hotel_thumb.jpg')}/>
                </View>

                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row',alignSelf:'stretch',flex:1}}>
                    <View style={styles.captionForm}>
                        <Text style={styles.BigText}>Lorin Sentul</Text>

                        <View style={{flexDirection:'row',alignSelf:'stretch',flex:1,marginTop:5, alignItems:'center'}}>
                          <Image style={{width:15, height:21, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_place.png')}/>
                          <Text style={styles.smallText}>Sentul Circuit (Exit Toll KM. 32)</Text>
                        </View>
                        <View style={{flexDirection:'row',alignSelf:'stretch',flex:1,marginTop:5, alignItems:'center'}}>
                          <Image style={{width:127, height:20, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_stars.png')}/>
                          <Text style={styles.smallText}>4/5</Text>
                        </View>
                    </View>
                  </View>
                  <View style={{paddingRight:15,paddingTop:10}}>
                    <Text style={styles.BigText2}>Rp 600.000</Text>
                    <Text style={styles.smallText}>1 Malam</Text>
                  </View>
                  <TouchableOpacity style={styles.bgGreenTextAbs}>
                    <Text style={{color:"#fff",fontSize:18,fontWeight:'bold',PaddingHorizontal:20}}>PILIH</Text>
                  </TouchableOpacity>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                <View style={styles.imgThumb}>
                  <Image style={{width:370, height:162, resizeMode:'contain'}}  source={require('../images/hotel_thumb.jpg')}/>
                </View>

                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row',alignSelf:'stretch',flex:1}}>
                    <View style={styles.captionForm}>
                        <Text style={styles.BigText}>Lorin Sentul</Text>

                        <View style={{flexDirection:'row',alignSelf:'stretch',flex:1,marginTop:5, alignItems:'center'}}>
                          <Image style={{width:15, height:21, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_place.png')}/>
                          <Text style={styles.smallText}>Sentul Circuit (Exit Toll KM. 32)</Text>
                        </View>
                        <View style={{flexDirection:'row',alignSelf:'stretch',flex:1,marginTop:5, alignItems:'center'}}>
                          <Image style={{width:127, height:20, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_stars.png')}/>
                          <Text style={styles.smallText}>4/5</Text>
                        </View>
                    </View>
                  </View>
                  <View style={{paddingRight:15,paddingTop:10}}>
                    <Text style={styles.BigText2}>Rp 600.000</Text>
                    <Text style={styles.smallText}>1 Malam</Text>
                  </View>
                  <TouchableOpacity style={styles.bgGreenTextAbs}>
                    <Text style={{color:"#fff",fontSize:18,fontWeight:'bold',PaddingHorizontal:20}}>PILIH</Text>
                  </TouchableOpacity>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                <View style={styles.imgThumb}>
                  <Image style={{width:370, height:162, resizeMode:'contain'}}  source={require('../images/hotel_thumb.jpg')}/>
                </View>

                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row',alignSelf:'stretch',flex:1}}>
                    <View style={styles.captionForm}>
                        <Text style={styles.BigText}>Lorin Sentul</Text>

                        <View style={{flexDirection:'row',alignSelf:'stretch',flex:1,marginTop:5, alignItems:'center'}}>
                          <Image style={{width:15, height:21, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_place.png')}/>
                          <Text style={styles.smallText}>Sentul Circuit (Exit Toll KM. 32)</Text>
                        </View>
                        <View style={{flexDirection:'row',alignSelf:'stretch',flex:1,marginTop:5, alignItems:'center'}}>
                          <Image style={{width:127, height:20, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_stars.png')}/>
                          <Text style={styles.smallText}>4/5</Text>
                        </View>
                    </View>
                  </View>
                  <View style={{paddingRight:15,paddingTop:10}}>
                    <Text style={styles.BigText2}>Rp 600.000</Text>
                    <Text style={styles.smallText}>1 Malam</Text>
                  </View>
                  <TouchableOpacity style={styles.bgGreenTextAbs}>
                    <Text style={{color:"#fff",fontSize:18,fontWeight:'bold',PaddingHorizontal:20}}>PILIH</Text>
                  </TouchableOpacity>
                </View>

              </View>
            </View>
          </View>

        </ScrollView>
        
      </View>
    )
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  }
}




// STYLES
// ------------------------------------
const styles = StyleSheet.create(HasilSearchHotelStyle);