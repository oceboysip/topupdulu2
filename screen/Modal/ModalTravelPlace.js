import React, { Component } from 'react';
import { View, ScrollView, ActivityIndicator, TouchableOpacity as NativeTouchable , Platform } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import Styled from 'styled-components';
// import MapView, { Marker, Polyline, PROVIDER_GOOGLE } from 'react-native-maps';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import { Row, Col } from '../Grid';
import Text from '../Text';
import Color from '../Color';
import TouchableOpacity from '../Button/TouchableDebounce';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    flexDirection: column;
    backgroundColor: #FFFFFF;
`;

const RowView = Styled(Row)`
    height: 100%;
`;

const ColumnView = Styled(Col)`
    justifyContent: center;
    alignItems: center;
`;

const CloseView = Styled(View)`
    width: 100%;
    height: 60;
    position: absolute;
    top: 0;
    left: 0;
`;

const HeaderView = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: ${Color.theme};
    alignItems: center;
    justifyContent: flex-start;
    paddingVertical: 20;
`;

const SubHeaderView = Styled(View)`
    width: 100%;
    height: 100;
    alignItems: center;
    justifyContent: flex-start;
    backgroundColor: ${Color.theme};
`;

const ContentMainView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    padding: 16px 0px 16px 16px;
`;

const TwoRoundButtonsContainer = Styled(View)`
    width: 172;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
`;

const CircleButtonContainer = Styled(NativeTouchable)`
    minWidth: 1;
    minHeight: 1;
    alignItems: center;
    justifyContent: center;
    flexDirection: column;
`;

const CircleButton = Styled(View)`
    width: 50;
    height: 50;
    alignItems: center;
    justifyContent: center;
    backgroundColor: white;
    borderRadius: 25;
    marginBottom: 8;
`;

const YellowCircleView = Styled(View)`
    width: 9;
    height: 9;
    borderRadius: 50;
    backgroundColor: ${Color.theme};
    borderColor: #231F20;
    borderWidth: 0.5;
    marginVertical: 4;
`;

const LineView = Styled(View)`
    width: 100%;
    minHeight: 1
    paddingRight: 15;
    flexDirection: row;
`;

const LeftLineView = Styled(View)`
    width: 16;
    minHeight: 1;
    flexDirection: column;
    alignItems: center;
    marginRight: 16;
`;

const Limiter = Styled(View)`
    flex: 1;
    justifyContent: center;
`;

const LimiterCenter = Styled(Limiter)`
    justifyContent: center;
    alignItems: center;
`;

const VerticalLine = Styled(View)`
    flex: 1;
    width: 1;
    backgroundColor: #231F20;
`;

const UpperVerticalLine = Styled(VerticalLine)`
    flex: 0;
    height: 2;
`;

const RightLineView = Styled(LeftLineView)`
    flex: 1;
    minHeight: 1;
    alignItems: flex-start;
    justifyContent: flex-start;
    paddingBottom: 30;
`;

const IndicatorView = Styled(View)`
    flex: 1;
    width: 100%;
    backgroundColor: #FFFFFF;
    alignItems: center;
    justifyContent: center;
`;

const CloseTouch = Styled(TouchableOpacity)`
    flex: 1;
    width: 100%;
    alignItems: center;
    justifyContent: center;
`;

const FontAwesomeIcon = Styled(FontAwesome)`
    fontSize: 22;
    color: ${Color.text};
`;

const AntDesignIcon = Styled(AntDesign)`
    fontSize: 22;
    color: ${Color.text};
`;

const SideIcon = Styled(MaterialIcons)`
    fontSize: 23;
    color: #000000;
`;

const NormalText = Styled(Text)`
`;

const RoundLabelText = Styled(Text)`
    fontSize: 12;
`;

const RouteText = Styled(Text)`
    fontSize: 12;
    letterSpacing: 1;
    textAlign: left;
`;

export default class ModalTravelPlace extends Component {

  constructor(props) {
    super(props);
    this.state = {
      mode: true,
      ready: false,
    };
  }

  componentDidMount() {
    setTimeout(() => this.setState({ ready: true }), 500);
  }

  componentDidUpdate() {
    // =============================================================================================================================
    // if (this.state.mode) setTimeout(() => this.focusMap(this.props.data.coordinates), 1);
  }

  focusMap(coordinates) {
    if (coordinates.length > 1) this.refs.mapRef.fitToSuppliedMarkers([coordinates[0].name.toString(), coordinates[coordinates.length - 1].name.toString()], true);
  }

  renderAllRoutes(places) {
    const firstIndex = 0;
    const lastIndex = places.length - 1;

    return places.map((place, i) =>
      <LineView key={i}>
        <LeftLineView>
          <LimiterCenter>
            <UpperVerticalLine style={i === firstIndex && { backgroundColor: 'transparent' }} />
            <YellowCircleView />
            <VerticalLine style={i === lastIndex && { backgroundColor: 'transparent' }} />
          </LimiterCenter>
        </LeftLineView>
        <RightLineView style={i === lastIndex && { paddingBottom: 0 }}>
          <RouteText type='bold'>{place.name.toUpperCase()}</RouteText>
        </RightLineView>
      </LineView>
    );
  }

  renderRoute(places) {
    return (
      <ContentMainView>
        {this.renderAllRoutes(places)}
      </ContentMainView>
    );
  }

  renderMap(coordinates, ready) {
    // if (ready) return (
    //   <MapView
    //     ref='mapRef'
    //     style={{ flex: 1 }}
    //     provider={PROVIDER_GOOGLE}
    //     mapType='standard'
    //     showsScale
    //     showsCompass
    //     showsPointsOfInterest
    //     showsBuildings
    //     initialRegion={{ latitude: coordinates[0].latitude, longitude: coordinates[0].longitude, latitudeDelta: 0.0543, longitudeDelta: 0.0534 }}
    //   >
    //     {coordinates.map((coordinate, i) => <Marker key={i} title={coordinate.name} identifier={coordinate.name.toString()} coordinate={coordinate} />)}
    //     {coordinates.length > 1 && <Polyline coordinates={coordinates} strokeColor='hotpink' strokeWidth={2} />}
    //   </MapView>
    // );
    return (
      <IndicatorView>
        <ActivityIndicator size='large' color={Color.loading} />
      </IndicatorView>
    );
  }

  render() {
    const { mode, ready } = this.state;
    const { data, onClose } = this.props;
    const { coordinates, places, dayNumber } = data;

    return (
              <SafeAreaView style={{ backgroundColor: Color.theme, flex: 1 }} >
            { Platform.OS  === 'ios' && <View style={{ height: 30 }}></View> }
      <MainView>
        <HeaderView>
          <NormalText type='bold'>Destinasi Hari {dayNumber}</NormalText>
          <NormalText type='medium'>{places.length} Tempat Wisata</NormalText>
        </HeaderView>
        <CloseView>
          <RowView>
            <ColumnView size={2}>
              <CloseTouch onPress={onClose}><SideIcon name='close' /></CloseTouch>
            </ColumnView>
          </RowView>
        </CloseView>

        <ScrollView contentContainerStyle={{ flex: 1 }}>
          <SubHeaderView>
            <TwoRoundButtonsContainer>
              <CircleButtonContainer activeOpacity={1} onPress={() => this.setState({ mode: true })}>
                <CircleButton style={mode && { backgroundColor: Color.text }}>
                  <FontAwesomeIcon name='map-o' style={mode && { color: '#FFFFFF' }} />
                </CircleButton>
                <RoundLabelText type='medium' style={mode && { color: Color.text }}>Peta</RoundLabelText>
              </CircleButtonContainer>
              <CircleButtonContainer activeOpacity={1} onPress={() => this.setState({ mode: false })}>
                <CircleButton style={!mode && { backgroundColor: Color.text }}>
                  <AntDesignIcon name='picture' style={!mode && { color: '#FFFFFF' }} />
                </CircleButton>
                <RoundLabelText type='medium' style={!mode && { color: Color.text }}>Rute</RoundLabelText>
              </CircleButtonContainer>
            </TwoRoundButtonsContainer>
          </SubHeaderView>
          {/* mode ? this.renderMap(coordinates, ready) : this.renderRoute(places)*/}
          {mode === false && this.renderRoute(places)}
        </ScrollView>
      </MainView>
      </SafeAreaView>
    );
  }

}
