import React, { Component } from 'react';
import { StyleSheet, Image, View, TouchableOpacity, Text } from 'react-native';
import Styled from 'styled-components';

import BookedHotelStyle from '../../style/BookedHotelStyle';
import Color from '../Color';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    flexDirection: column;
    borderTopLeftRadius: 20;
    borderTopRightRadius: 20;
    backgroundColor: #FFFFFF;
`;

const DragContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    paddingVertical: 10;
    alignItems: center;
`;

const DragView = Styled(View)`
    width: 40;
    height: 6;
    borderRadius: 5;
    backgroundColor: #DDDDDD;
`;

const TitleView = Styled(View)`
    width: 100%;
    minHeight: 1;
    paddingVertical: 10;
    marginBottom: 20;
    alignItems: center;
`;

const PerLineView = Styled(View)`
    width: 100%;
    height: 80;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
    paddingHorizontal: 16;
    borderBottomWidth: 1;
    borderColor: #E8E3E3;
`;

const ButtonContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    padding: 10px 20px 10px 20px;
`;

const RectangleView = Styled(TouchableOpacity)`
    width: 35;
    height: 35;
    backgroundColor: ${Color.theme};
    alignItems: center;
    justifyContent: center;
    borderRadius: 3;
`;

const OperationalView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
`;

const PassengerNumberView = Styled(View)`
    width: 30;
    minHeight: 1;
    alignItems: center;
    justifyContent: center;
`;

const TitleText = Styled(Text)`
    letterSpacing: 1;
    fontSize: 18;
`;

const OptionText = Styled(Text)`
    fontSize: 16;
`;

const PassengerNumber = Styled(Text)`
    fontSize: 16;
`;

const CustomButton = Styled(TouchableOpacity)`
    width: 100%;
    height: 50;
    backgroundColor: ${Color.theme};
    justifyContent: center;
    alignItems: center;
    borderRadius: 25;
`;

const options = [
  { label: 'Dewasa', type: 'adult', desc: 'Diatas 12 tahun' },
  { label: 'Anak-anak', type: 'child', desc: '2-11 tahun' },
  { label: 'Bayi', type: 'infant', desc: 'Dibawah 2 tahun' }
];

export default class PassengerPicker extends Component {

  constructor(props) {
    super(props);
    this.state = {
      adult: props.passengers.adult,
      child: props.passengers.child,
      infant: props.passengers.infant,
      max: props.max || { adult: 6, child: 6, infant: 4 },
      min: props.min || { adult: 1, child: 0, infant: 0 },
    };
  }

  tryDecrement(type) {
    return this.state[type] > this.state.min[type];
  }

  tryIncrement(type) {
    return this.state[type] < this.state.max[type];
  }

  decrement(type) {
    this.setState({ [type]: this.state[type] - 1 });
  }

  increment(type) {
    this.setState({ [type]: this.state[type] + 1 });
  }

  // renderOp() {
  //   return options.map((opt, i) => {
  //     const canDecrement = this.tryDecrement(opt.type);
  //     const canIncrement = this.tryIncrement(opt.type);
  //     return (
  //       <PerLineView key={i}>
  //         <OptionText type='medium'>{opt.label}</OptionText>
  //         <OperationalView>
  //           <RectangleView activeOpacity={1} onPress={() => canDecrement && this.decrement(opt.type)} style={!canDecrement && { backgroundColor: '#94d1b8'}}>
  //             <Text style={[!canDecrement && {color: '#C9C6BE'}, {color: '#FFFFFF', fontSize: 24}]}>-</Text>
  //           </RectangleView>
  //           <PassengerNumberView>
  //             <PassengerNumber type='medium'>{this.state[opt.type]}</PassengerNumber>
  //           </PassengerNumberView>
  //           <RectangleView activeOpacity={1} onPress={() => canIncrement && this.increment(opt.type)} style={!canIncrement && { backgroundColor: '#94d1b8'}}>
  //             <Text style={[!canIncrement && {color: '#C9C6BE'}, {color: '#FFFFFF', fontSize: 24}]}>+</Text>
  //           </RectangleView>
  //         </OperationalView>
  //       </PerLineView>
  //     );
  //   });
  // }

  // render() {
  //   const { adult, child, infant } = this.state;
  //   return (
  //     <MainView>
  //       <DragContainer>
  //         <DragView />
  //       </DragContainer>
  //       <TitleView><TitleText type='bold'>Jumlah Penumpang</TitleText></TitleView>
  //       {this.renderOptions()}
  //       <ButtonContainer>
  //         <CustomButton onPress={() => this.props.onSave({ adult, child, infant })}>
  //           <TitleText style={{color: '#FFFFFF'}}>Selesai</TitleText>
  //         </CustomButton>
  //       </ButtonContainer>
  //     </MainView>
  //   );
  // }

  renderOptions() {
    return options.map((item, index) => {
      const canDecrement = this.tryDecrement(item.type);
      const canIncrement = this.tryIncrement(item.type);
      return (
        <View key={index}>
          <View style={styles.rowPanelDirect}>
            <View style={styles.flexRow}>
              <Text style={styles.greyBig}>{item.label}</Text>
              <Text style={styles.greySmall}>{item.desc}</Text>
            </View>
            <View style={styles.flexRow}>
              <Text style={styles.BigText2}>{this.state[item.type]}</Text>
            </View>
            <View style={styles.flexRow2}>
              <TouchableOpacity onPress={() => canDecrement && this.decrement(item.type)} activeOpacity={canDecrement ? 0.7 : 1}>
                <Text style={[styles.plusMinusbt, !canDecrement && { backgroundColor: '#DDDDDD'}]}>-</Text> 
              </TouchableOpacity>

              <TouchableOpacity onPress={() => canIncrement && this.increment(item.type)} activeOpacity={canIncrement ? 0.7 : 1}>
                <Text style={[styles.plusMinusbt, !canIncrement && { backgroundColor: '#DDDDDD'}]}>+</Text> 
              </TouchableOpacity>
            </View>
          </View>

          {(item.type == 'adult' || item.type == 'child') && <View style={styles.rowPanelDirect}>  
            <View style={{flex: 1, height: 2, backgroundColor:'#fcdf00'}}>
            </View>
          </View>}
        </View>
      )}
    )
  }

  render() {
    const { adult, child, infant } = this.state;
    return (
      <MainView>
        <View style={styles.panel}>
          <View style={styles.rowPanel}>
            <Image style={{width: 70, height: 12, resizeMode: 'contain', marginBottom:10}} source={require('../../images/block_panel.png')} />
            <Text style={styles.BigText2}>Tambah Penumpang</Text>
          </View>

          {this.renderOptions()}

          <View style={styles.rowPanelDirect}>
            <TouchableOpacity style={styles.buttonYellow} onPress={() => this.props.onClose()}>
            <Text style={styles.whiteText}>Batal</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonGreen} onPress={() => this.props.onSave({adult, child, infant})}>
            <Text style={styles.whiteText}>Selesai</Text>
            </TouchableOpacity>
          </View>
        </View>
      </MainView>
    )
  }
}

const styles = StyleSheet.create(BookedHotelStyle);