import React, { Component } from "react";
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, ScrollView, SafeAreaView, Modal, Dimensions, Platform, FlatList, PermissionsAndroid as PA } from 'react-native';
import Contacts from "react-native-contacts";

import Header from '../Header';
import Color from '../Color';

export default class ModalPhoneContact extends Component {
    constructor(props) {
        super(props);
        this.state = {
            contacts: [],
            searchText: '',
            contactPermission: false
        }
    }

    async componentWillMount() {
        if (Platform.OS === "android") {
          this.requestContactPermission();
        } else {
          this.loadContacts();
        }
    }

    async requestContactPermission() {
        try{
            const granted = await PA.requestMultiple([PA.PERMISSIONS.WRITE_CONTACTS, PA.PERMISSIONS.READ_CONTACTS]);

            if (Platform.Version >= 23) {
                if(granted['android.permission.WRITE_CONTACTS'] === PA.RESULTS.GRANTED && granted['android.permission.READ_CONTACTS'] === PA.RESULTS.GRANTED) {
                    this.setState({ contactPermission: true });
                    this.loadContacts();
                }
            }else {
                this.setState({ contactPermission: true });
                this.loadContacts();
            }
        }catch(err) {
            console.log(err);
        }
    }

    loadContacts() {
        Contacts.getAll((err, contacts) => {
            if (err === "denied") {
                console.warn("Permission to access contacts was denied");
            } else {
                this.setState({ contacts });
            }
        });

        // Contacts.getCount(count => {
        //     this.setState({ searchPlaceholder: `Search ${count} contacts` });
        // });
    }

    compare(a, b) {
        let nameA = '', nameB = '';

        if (Platform.OS === 'ios') {
            nameA = a.givenName.toUpperCase();
            nameB = b.givenName.toUpperCase();
        } else {
            nameA = a.displayName.toUpperCase();
            nameB = b.displayName.toUpperCase();
        }
      
        let comparison = 0;

        if (nameA > nameB) {
          comparison = 1;
        } else if (nameA < nameB) {
          comparison = -1;
        }
        return comparison;
    }

    getMobileNumber(phoneNumbers) {
        let mobileNumber = '';
        phoneNumbers.map((mobile) => {
            if (Platform.OS === 'ios') {
                if (mobile.label === 'home') mobileNumber = mobile.number.replace(/[^\d]/g, '');
            } else {
                if (mobile.label === 'mobile') mobileNumber = mobile.number.replace(/[^\d]/g, '');
            }
        })

        if (mobileNumber === '' || mobileNumber.length <= 9) return null;
        return mobileNumber;
    }

    keyExtractor = (item, index) => `${item.id} - ${index}`;

    renderCardContact(item) {
        if (item.phoneNumbers.length > 0 && this.getMobileNumber(item.phoneNumbers)) {
            return (
                <TouchableOpacity activeOpacity={0.7} onPress={() => this.props.selectedPhoneNumber(this.getMobileNumber(item.phoneNumbers))} style={{flexDirection: 'row', paddingHorizontal: 16, paddingVertical: 16, justifyContent: 'space-between', width: '100%'}}>
                    <View style={{width: '80%'}}>
                        <Text>{Platform.OS === 'ios' ? item.givenName + ' ' + item.familyName : item.displayName}</Text>
                        <Text>{this.getMobileNumber(item.phoneNumbers)}</Text>
                    </View>
                    <View style={{width: '20%', alignItems: 'flex-end'}}>
                        <Text>Ponsel</Text>
                    </View>
                </TouchableOpacity>
            )
        }
        
        return null;
    }

    render() {
        console.log(this.state, 'state contact');

        let contacts = null;
        if (this.state.contacts.length > 0) {
            contacts = this.state.contacts.sort(this.compare);
        }

        console.log(contacts, 'contacts');
        
        return (
            <SafeAreaView style={{backgroundColor: Color.theme, flex: 1}}>
                <Header title='Cari nomor telepon' onPressLeftButton={() => this.props.onClose()} />
                <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
                    <FlatList
                        keyExtractor={this.keyExtractor}
                        data={contacts}
                        renderItem={({ item }) => this.renderCardContact(item)}
                    />
                </View>
            </SafeAreaView>
        )
    }
}