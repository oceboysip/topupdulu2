import React, { Component } from 'react';
import { View, FlatList, TextInput } from 'react-native';
import Styled from 'styled-components';
import EvilIcons from 'react-native-vector-icons/EvilIcons';

import Text from '../Text';
import Color from '../Color';
import Countries from '../LocalData/Countries';
import TouchableOpacity from '../Button/TouchableDebounce';
import Header from '../Header';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    alignItems: center;
    flexDirection: column;
`;

const CustomHeader = Styled(Header)`
    height: 60;
`;

const BackgroundSearchBar = Styled(View)`
    width: 100%;
    minHeight: 1;
    alignItems: center;
    justifyContent: flex-start;
    backgroundColor: ${Color.theme};
    padding: 0px 16px 16px 16px;
`;

const SearchBar = Styled(View)`
    width: 100%;
    height: 50;
    flexDirection: row;
    backgroundColor: #FFFFFF;
`;

const CustomTextInput = Styled(TextInput)`
    width: 100%;
    height: 100%;
    color: ${Color.text};
    fontSize: 12;
`;

const CustomFlatList = Styled(FlatList)`
    width: 100%;
    paddingHorizontal: 16;
`;

const RowView = Styled(TouchableOpacity)`
    width: 100%;
    height: 50;
    alignItems: flex-start;
    justifyContent: center;
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
`;

const SearchIconContainer = Styled(View)`
    minWidth: 20;
    minHeight: 20;
    alignItems: center;
    justifyContent: center;
    margin: 10px 2px 10px 6px;
`;

const RowText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const SearchIcon = Styled(EvilIcons)`
    color: #A8A699;
    fontSize: 32;
`;

const sortedCountries = Countries.sort((a, b) => a.name.toLowerCase() < b.name.toLowerCase() ? -1 : 1);

export default class CountryPicker extends Component {

  constructor(props) {
    super(props);
    this.state = {
      countries: sortedCountries,
      searchText: '',
    };
  }

  onSelectedCountry({ item }) {
    this.props.onSelectedCountry(item);
    this.props.onClose();
  }

  searchFilter(searchText) {
    let countries = sortedCountries;
    const text = searchText.toLowerCase();
    countries = countries.filter((item) => item.name.toLowerCase().indexOf(text) > -1);
    this.setState({ countries });
  }

  onChangeText(searchText) {
    this.setState({ searchText });
    this.searchFilter(searchText);
  }

  renderRow(data) {
    return (
      <RowView onPress={() => this.onSelectedCountry(data)}>
        <RowText>{data.item.name}</RowText>
      </RowView>
    );
  }

  render() {
    const { onClose } = this.props;
    const { countries } = this.state;

    return (
      <MainView>
        <CustomHeader showLeftButton title='Pilih Negara' onPressLeftButton={onClose} />
        <BackgroundSearchBar>
          <SearchBar>
            <SearchIconContainer><SearchIcon name='search' /></SearchIconContainer>
            <CustomTextInput
              placeholder='Cari Negara'
              placeholderTextColor='#DDDDDD'
              underlineColorAndroid='transparent'
              autoCorrect={false}
              autoFocus
              onChangeText={(text) => this.onChangeText(text)}
              selectionColor={Color.text}
            />
          </SearchBar>
        </BackgroundSearchBar>
        <CustomFlatList
          keyboardShouldPersistTaps='always'
          data={countries}
          keyExtractor={(item, index) => `${item.id}.${index}`}
          renderItem={(rowData) => this.renderRow(rowData)}
        />
      </MainView>
    );
  }
}

// export default connect(mapStateToProps, mapDispatchToProps)(CountryPicker);
