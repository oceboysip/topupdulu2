import React, { Component } from 'react';
import { View, ScrollView , Platform } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import Accordion from 'react-native-collapsible/Accordion';
import Styled from 'styled-components';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Color from '../Color';
import Text from '../Text';
import Header from '../Header';
import TravelTermsConditions from '../LocalData/TravelTermsConditions';

const MainView = Styled(View)`
    width: 100%
    minHeight: 1;
    flexDirection: column;
    alignItems: center;
    backgroundColor: white;
`;

const CustomHeader = Styled(Header)`
    height: 60;
`;

const HeaderSectionView = Styled(View)`
    width: 100%;
    minHeight: 40;
    backgroundColor: white;
    flexDirection: row;
    borderBottomWidth: 1;
    borderColor: #F4F4F4;
    paddingTop: 8;
    paddingBottom: 8;
`;

const HeaderSectionLeft = Styled(View)`
    width: 90%;
    minHeight: 1;
    alignItems: flex-start;
    justifyContent: center;
`;

const HeaderSectionRight = Styled(HeaderSectionLeft)`
    width: 10%;
    alignItems: flex-end;
`;

const AccordionDescriptionView = Styled(View)`
    width: 100%;
    minHeight: 1;
    padding: 0px 0px 10px 0px;
`;

const AccordionContainerPerNumberView = Styled(View)`
    width: 94%;
    minHeight: 1;
    marginTop: 5;
    flexDirection: row;
`;

const AccordionNumberView = Styled(View)`
    width: 15;
    minHeight: 1;
    alignItems: center;
    paddingTop: 2;
`;

const AccordionInstructionView = Styled(View)`
    minWidth: 10;
    minHeight: 1;
    paddingLeft: 10;
    paddingRight: 3%;
`;

const BlueIconArrow = Styled(Ionicons)`
    fontSize: 14;
    color: #231F20;
`;

const FontAwesomeIcon = Styled(FontAwesome)`
    fontSize: 14;
    color: #231F20;
`;

const NormalText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
    lineHeight: 18;
`;

export default class ModalTravelTermsConditions extends Component {

  renderPerDescription(lbl, section) {
    return lbl.desc.map((dsc, j) =>
      <AccordionContainerPerNumberView key={j}>
        <AccordionNumberView>
          <FontAwesomeIcon name={section.type ? 'close' : 'check'} />
        </AccordionNumberView>
        <AccordionInstructionView>
          <NormalText type='medium'>{dsc}</NormalText>
        </AccordionInstructionView>
      </AccordionContainerPerNumberView>
    );
  }

  renderAccordionSection(section) {
     return section.label.map((lbl, i) =>
      <AccordionDescriptionView key={i}>
        {lbl.header && <AccordionDescriptionView>
          <NormalText type='medium'>{lbl.header}</NormalText>
        </AccordionDescriptionView>}
        {this.renderPerDescription(lbl, section)}
      </AccordionDescriptionView>
    );
  }

  renderAccordionHeader(section, index, isActive) {
    return (
      <HeaderSectionView>
        <HeaderSectionLeft><NormalText type='medium'>{section.title}</NormalText></HeaderSectionLeft>
        <HeaderSectionRight>
          <BlueIconArrow name={isActive ? 'ios-arrow-down' : 'ios-arrow-forward'} />
        </HeaderSectionRight>
      </HeaderSectionView>
    );
  }

  renderAccordions() {
    return TravelTermsConditions.map((detail, i) =>
      <Accordion
        key={i}
        touchableProps={{ activeOpacity: 1 }}
        sections={detail}
        renderHeader={(section, index, isActive) => this.renderAccordionHeader(section, index, isActive)}
        renderContent={(section) => this.renderAccordionSection(section)}
      />
    );
  }

  render() {
    return (
      <SafeAreaView style={{ height: '98%'}} >
      { Platform.OS  === 'ios' && <View style={{   backgroundColor: Color.theme, height: 30 }}></View> }
      <MainView>
        <CustomHeader showLeftButton iconLeftButton='close' title='Syarat & Ketentuan' onPressLeftButton={this.props.onClose} />
        <ScrollView contentContainerStyle={{ paddingHorizontal: 16 }}>
          {this.renderAccordions()}
        </ScrollView>
      </MainView>
      </SafeAreaView>
    );
  }
}
