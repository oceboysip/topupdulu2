import React, { Component } from 'react';
import { View, Image, ScrollView, Platform } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import Styled from 'styled-components';
import Moment from 'moment';

import Header from '../Header';
import Text from '../Text';
import Color from '../Color';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    flexDirection: column;
    backgroundColor: #FFFFFF;
`;

const CustomHeader = Styled(Header)`
    height: 60;
`;

const ContentContainer = Styled(View)`
    flex: 1;
    flexDirection: column;
`;

const FlightMainView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
`;

const FlightHeaderView = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: #F4F4F4;
    padding: 8px 16px;
    flexDirection: row;
    alignItems: center;
`;

const FlightInfoView = Styled(View)`
    width: 100%;
    minHeight: 20;
    paddingVertical: 8px
    flexDirection: column;
`;

const AirlineImageView = Styled(View)`
    width: 25;
    height: 25;
    marginRight: 9;
`;

const LineView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
`;

const LeftLineView = Styled(View)`
    width: 50;
    minHeight: 1;
    alignItems: center;
`;

const RightLineView = Styled(View)`
    flex: 1;
    minWidth: 1;
    minHeight: 1;
    alignItems: flex-start;
    marginHorizontal: 0;
`;

const VerticalLine = Styled(View)`
    flex: 1;
    width: 1;
    backgroundColor: #231F20;
`;

const Limiter = Styled(View)`
    flex: 1;
    justifyContent: center;
`;

const LimiterCenter = Styled(Limiter)`
    justifyContent: center;
    alignItems: center;
`;

const UpperVerticalLine = Styled(VerticalLine)`
    flex: 0;
    height: 4;
`;

const YellowCircleView = Styled(View)`
    width: 9;
    height: 9;
    borderRadius: 50;
    backgroundColor: ${Color.theme};
    borderColor: #231F20;
    borderWidth: 0.5;
    marginVertical: 2;
`;

const GreyBox = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    padding: 4px 8px 4px 8px;
    marginVertical: 4;
    backgroundColor: #F4F4F4;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const NormalText = Styled(Text)`
    textAlign: left;
`;

export default class ModalTravelFlight extends Component {

  renderOneFlight(flight, i) {
    const departsAt = flight.origin && flight.departsAt ? Moment.utc(flight.departsAt).utcOffset(flight.origin.utcOffset, true) : null;
    const arrivesAt = flight.destination && flight.arrivesAt ? Moment.utc(flight.arrivesAt).utcOffset(flight.destination.utcOffset, true) : null;
    const validDeparts = departsAt && Moment(departsAt, 'YYYY-MM-DD HH:mm:ss', true).isValid();
    const validArrives = arrivesAt && Moment(arrivesAt, 'YYYY-MM-DD HH:mm:ss', true).isValid();

    return (
      <FlightMainView key={i}>
        {flight.airline && <FlightHeaderView>
          <AirlineImageView><ImageProperty source={{ uri: flight.airline.logoUrl }} resizeMode='contain' /></AirlineImageView>
          <NormalText type='medium'>{flight.airline.name}</NormalText>
        </FlightHeaderView>}
        {flight.origin && flight.destination && <FlightInfoView>
          <LineView>
            <LeftLineView>
              <LimiterCenter>
                <UpperVerticalLine style={{ backgroundColor: 'transparent' }} />
                <YellowCircleView />
                <VerticalLine />
              </LimiterCenter>
            </LeftLineView>
            <RightLineView>
              <NormalText type='medium'>{`${flight.origin.cityName} (${flight.origin.code}) ${flight.origin.name}`}</NormalText>
              {validDeparts && <NormalText type='medium'>{`${departsAt.format('HH:mm')} `}{departsAt.format('dddd, DD MMM YYYY')}</NormalText>}
            </RightLineView>
          </LineView>
          <LineView style={{ minHeight: 10 }}>
            <LeftLineView>
              <LimiterCenter>
                <VerticalLine />
              </LimiterCenter>
            </LeftLineView>
            <RightLineView>
              {validDeparts && validArrives && <GreyBox><NormalText type='medium'>{Moment.duration(arrivesAt.diff(departsAt, 'seconds'), 'seconds').format('h[j] m[m]')}</NormalText></GreyBox>}
            </RightLineView>
          </LineView>
          <LineView>
            <LeftLineView>
              <LimiterCenter>
                <UpperVerticalLine />
                <YellowCircleView />
                <VerticalLine style={{ backgroundColor: 'transparent' }} />
              </LimiterCenter>
            </LeftLineView>
            <RightLineView>
              <NormalText type='medium'>{`${flight.destination.cityName} (${flight.destination.code}) ${flight.destination.name}`}</NormalText>
              {validArrives && <NormalText type='medium'>{`${arrivesAt.format('HH:mm')} `}{arrivesAt.format('dddd, DD MMM YYYY')}</NormalText>}
            </RightLineView>
          </LineView>
        </FlightInfoView>}
      </FlightMainView>
    );
  }

  renderFlights(flights) {
    return flights.map((flight, i) =>
      this.renderOneFlight(flight, i)
    );
  }

  render() {
    const { flights, onClose } = this.props;
    // const flights = [
    //   {
    //     "departsAt": Moment('2019-01-23T07:05:00'),
    //     "arrivesAt": Moment('2019-01-23T11:05:00'),
    //     "airline": {
    //       "logoUrl": "/static/img/airline/SQ.png",
    //       "code": "SQ",
    //       "name": "Singapore Airlines",
    //     },
    //     "origin": { name: 'Airport A', code: 'AAA', cityName: 'Jakarta', utcOffset: 7 },
    //     "destination": { name: 'Airport B', code: 'BBB', cityName: 'San Francisco', utcOffset: 7 },
    //   },
    //   {
    //     "departsAt": Moment('2019-01-23T07:05:00'),
    //     "arrivesAt": Moment('2019-01-23T11:05:00'),
    //     "airline": {
    //       "logoUrl": "/static/img/airline/SQ.png",
    //       "code": "SQ",
    //       "name": "Singapore Airlines",
    //     },
    //     "origin": { name: 'Airport A', code: 'AAA', cityName: 'Jakarta', utcOffset: 7 },
    //     "destination": { name: 'Airport B', code: 'BBB', cityName: 'San Francisco', utcOffset: 7 },
    //   }
    // ];

    return (
        <SafeAreaView style={{ height: '98%'}} >
      { Platform.OS  === 'ios' && <View style={{   backgroundColor: Color.theme, height: 30 }}></View> }
      <MainView>
        <CustomHeader showLeftButton title='Penerbangan' onPressLeftButton={onClose} />
        <ScrollView>
          <ContentContainer>
            {this.renderFlights(flights)}
          </ContentContainer>
        </ScrollView>
      </MainView>
      </SafeAreaView>
    );
  }
}
