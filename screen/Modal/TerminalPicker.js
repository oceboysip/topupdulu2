import React, { Component } from 'react';
import { View, FlatList, TextInput, ActivityIndicator, Platform } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import Styled from 'styled-components';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import gql from 'graphql-tag';

import Text from '../Text';
import Color from '../Color';
import TouchableOpacity from '../Button/TouchableDebounce';
import Header from '../Header';
import Client from '../../state/apollo';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    alignItems: center;
    flexDirection: column;
`;

const CustomHeader = Styled(Header)`
    height: 60;
`;

const BackgroundSearchBar = Styled(View)`
    width: 100%;
    minHeight: 1;
    alignItems: center;
    justifyContent: flex-start;
    backgroundColor: ${Color.theme};
    padding: 0px 16px 16px 16px;
`;

const SearchBar = Styled(View)`
    width: 100%;
    height: 50;
    flexDirection: row;
    backgroundColor: #FFFFFF;
`;

const CustomTextInput = Styled(TextInput)`
    width: 100%;
    height: 100%;
    color: ${Color.text};
    fontSize: 12;
`;

const CustomFlatList = Styled(FlatList)`
    width: 100%;
    paddingHorizontal: 16;
`;

const RowView = Styled(TouchableOpacity)`
    width: 100%;
    minHeight: 1;
    paddingVertical: 10;
    alignItems: flex-start;
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
`;

const SearchIconContainer = Styled(View)`
    minWidth: 20;
    minHeight: 20;
    alignItems: center;
    justifyContent: center;
    margin: 10px 2px 10px 6px;
`;

const RowText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const SmallText = Styled(Text)`
    fontSize: 10;
    textAlign: left;
    marginTop: 2;
`;

const SearchIcon = Styled(EvilIcons)`
    color: #A8A699;
    fontSize: 32;
`;

const WarningText = Styled(Text)`
`;

export default class TerminalPicker extends Component {

  constructor(props) {
    super(props);
    this.state = {
      busTerminal: [],
      searchText: '',
    };

    this.getBusTerminal(this.state.searchText)
  }

  getBusTerminal(searchText){
    const variables = {
      query: searchText
    }

    const getBusTerminal = gql`
      query(
        $query: String
      ){
        stations(
          query : $query
        ){
          data{
            code
            name
            city
            country
          }
        }
      }
    `;

    Client.query({
      query: getBusTerminal,
      variables
    }).then(res => {
      var data = res.data.stations.data;
      var response = []

      if (data) {
        for (var i = 0; i < data.length; i++) {
          if (data[i].code !== 'null') response.push(data[i])
        }

        this.setState({busTerminal: response})
      }
    }).catch(err => {
      console.log(err, 'error');
    })
  }

  onSelectTerminal({item}) {
    this.props.onSelectedTerminal(item);
    this.props.onClose();
  }

  onChangeText(searchText) {
    const query = searchText.toLowerCase();

    this.setState({
      searchText: query
    }, this.getBusTerminal(query));
  }

  renderRow(data) {
    console.log(data, 'data');
    return (
      <RowView onPress={() => this.onSelectTerminal(data)}>
        <RowText>{data.item.name}</RowText>
        <SmallText>{data.item.city !== 'null' && data.item.city}{data.item.country !== 'null' && ' - ' + data.item.country}</SmallText>
      </RowView>
    )
  }

  renderIsNilTerminal(){
    return (
      <View style={{marginTop: 6}}>
        <WarningText>Terminal yang Anda cari tidak tersedia.</WarningText>
      </View>
    )
  }

  render() {
    const { onClose } = this.props;
    const { busTerminal, searchText } = this.state;
    let isNilTerminal = (busTerminal.length === 0)

    return (
      <SafeAreaView style={{height: '98%'}}>
        {Platform.OS  === 'ios' && <View style={{backgroundColor: Color.theme, height: 30}}></View>}
        <MainView>
          <CustomHeader showLeftButton title='Pilih Terminal' onPressLeftButton={onClose} />
          <BackgroundSearchBar>
            <SearchBar>
              <SearchIconContainer><SearchIcon name='search' /></SearchIconContainer>
              <CustomTextInput
                placeholder='Cari terminal atau kota'
                placeholderTextColor='#DDDDDD'
                underlineColorAndroid='transparent'
                autoCorrect={false}
                autoFocus={false}
                onChangeText={(text) => this.onChangeText(text)}
                selectionColor={Color.text}
              />
            </SearchBar>
          </BackgroundSearchBar>
          {busTerminal.length > 0 && <CustomFlatList
            keyboardShouldPersistTaps='always'
            data={busTerminal}
            keyExtractor={(item, index) => `${item.id}.${index}`}
            renderItem={(rowData) => this.renderRow(rowData)}
          />}
          {isNilTerminal && searchText.length > 0 && this.renderIsNilTerminal()}
          {isNilTerminal && searchText.length === 0 && <View style={{height: '80%', justifyContent: 'center'}}><ActivityIndicator size='large' color={Color.loading} /></View>}
        </MainView>
      </SafeAreaView>
    );
  }
}

// export default connect(mapStateToProps, mapDispatchToProps)(AirportCityPicker);
