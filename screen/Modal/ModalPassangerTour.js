import React, { Component } from 'react';
import { View, ScrollView , Keyboard, Platform, Modal } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import Styled from 'styled-components';
import Ionicons from 'react-native-vector-icons/Ionicons'
import ModalTitlePicker from '../Modal/ModalTitlePicker';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Text from '../Text';
import Input from '../Input/Input';
import Color from '../Color';
import { TextInputMask } from 'react-native-masked-text'

const MainView = Styled(View)`
    width: 100%;
    backgroundColor: #FFFFFF;
    flexDirection: column;
    flex: 1;
`;

const SideIcon = Styled(Icon)`
    color: #000000;
`;

const TopView = Styled(View)`
    flexDirection: column
`;
const HeaderView = Styled(View)`
    flexDirection: row;
    backgroundColor: ${Color.theme};
    padding: 23px 16px 10px 17px;
    width: 100%;
`;
const LeftHeaderView = Styled(View)`
    width: 20%
    alignItems: flex-start
`;

const MidHeaderView = Styled(View)`
    width: 60%
`;

const RightHeaderView = Styled(View)`
    width: 20%
    alignItems: flex-end
`;

const AttentionView = Styled(View)`
    alignItems: flex-start;
    backgroundColor: ${Color.text};
    flexDirection: column;
    paddingTop: 16;
    paddingBottom: 14;
`;
const Container = Styled(View)`
    padding: 0px 16px 8px 16px;
    flexDirection: row;
`;
const LeftView = Styled(View)`
    paddingTop: 3px
    width: 5%
    alignItems: flex-start
`;
const RightView = Styled(View)`
    width: 85%
`;

const BaseText = Styled(Text)`
  textAlign: left;
  fontSize: 12;
  color: #FFFFFF;
  lineHeight: 18;
`;
const PassangerView = Styled(View)`
  margin: 8px 16px
`;

export default class ModalPassangerTour extends Component {

  constructor(props) {
    super(props);
    this.state = {
      refresh: false,
      visibleTitles: false,
      dt: props.pax.birthDate
    };
  }

  onPassangerChange = (name, value) => {
    const { pax } = this.props;
    this.props.onPassangerChange(pax.id, pax.roomId, name, value);
  }

  onTitleClose = () => {
    this.setState({
      visibleTitles: false
    })
  }

  onSelect = (key) => {
    this.setState(prevState => {
      return {
        ...prevState,
        [key]: true
      }
    })
  }

  render() {
    const { pax, tour, id } = this.props;
    const term = [
      { label: 'Masukkan nama penumpang seperti yang tertera pada ID' },
      { label: 'Pastikan ID Anda Valid setidaknya 6 bulan setelah tanggal keberangkatan' }
    ];

    return (
      <SafeAreaView style={{backgroundColor: Color.theme, flex: 1}}>
        {Platform.OS === 'ios' && <View style={{height: 30}} />}
        <MainView>
          <TopView>
            <HeaderView>
              <LeftHeaderView >
                <SideIcon name='close' size={23}  onPress={() => this.props.closeModal('modalPassangers', pax.id, 'cancel')}  />
              </LeftHeaderView>
              <MidHeaderView>
                <Text type='bold'>Data Wisatawan {id + 1}</Text>
              </MidHeaderView>
              <RightHeaderView  >
                <Text type='medium' onPress={() => this.props.closeModal('modalPassangers', pax.id, 'simpan')} >Simpan</Text>
              </RightHeaderView>
            </HeaderView>
          </TopView>
          <ScrollView>
            <AttentionView>
              {term.map((i, idx) =>
                <Container key={idx}>
                  <LeftView>
                    <BaseText><Ionicons name='ios-information-circle' /></BaseText>
                  </LeftView>
                  <RightView>
                    <BaseText type='medium'>{i.label}</BaseText>
                  </RightView>
                </Container>
              )}
            </AttentionView>
            <PassangerView>
              <Input
                  componentName="select"
                  placeholder="Pilih Gelar"
                  name="title"
                  label="Gelar"
                  error={pax.errors.title}
                  value={pax.title}
                  modalName="visibleTitles"
                  onSelect={this.onSelect}
              />
              <Input
                  name="firstName"
                  label='Nama Depan'
                  placeholder=""
                  keyboardType="default"
                  returnKeyType="next"
                  blurOnSubmit={false}
                  value={pax.firstName}
                  error={pax.errors.firstName}
                  onChangeValue={this.onPassangerChange}
                  onBlurValue={this.onValidatePassanger}
              />
              <Input
                  name="lastName"
                  label='Nama Belakang'
                  placeholder=""
                  keyboardType="default"
                  returnKeyType="next"
                  blurOnSubmit={false}
                  value={pax.lastName}
                  error={pax.errors.lastName}
                  onChangeValue={this.onPassangerChange}
                  onBlurValue={this.onValidatePassanger}
              />
              <Text
                style={{
                  color: '#A8A699',
                  textAlign: 'left',
                  fontSize: 12,
                  paddingTop: 7,
                  paddingBottom: 15,
                }}
              >Tanggal Lahir</Text>
              <TextInputMask
                  name="birthDate"
                  type={'datetime'}
                  label='Tanggal Lahir'
                  placeholder="YYYY-MM-DD"
                  keyboardType="numeric"
                  returnKeyType="next"
                  blurOnSubmit={false}
                  error={pax.errors.birthDate}
                  options={{ format: 'YYYY-MM-DD' }}
                  value={this.state.dt}
                  onChangeText={(text) => this.setState({ dt: text }, () => this.onPassangerChange('birthDate', text))}
                  onBlurValue={this.onValidatePassanger}
                  onSubmitEditing={Keyboard.dismiss}
                  style={{borderBottomWidth: 1, borderColor: '#DDDDDD', paddingBottom: 0}}
              />
            </PassangerView>
          </ScrollView>

          <Modal
            popup
            visible={this.state.visibleTitles}
            onClose={this.onTitleClose}
            animationType='slide-up'
            maskClosable
          >
            <ModalTitlePicker
              titles={this.props.titles}
              nameModal="visibleTitles"
              type="adult"
              onSave={this.onPassangerChange}
              onClose={this.onTitleClose}
              selected={pax.title}
            />
          </Modal>
        </MainView>
      </SafeAreaView>
    );
  }
}
