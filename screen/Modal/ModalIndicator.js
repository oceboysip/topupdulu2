import React from 'react';
import Styled from 'styled-components';
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator } from 'react-native-indicators';
import { View, ActivityIndicator, Modal } from 'react-native';

import Text from '../Text';
import Color from '../Color';

  const Wrapper = Styled(View)`
    background-color: ${props => props.backDropColor ? props.backDropColor : 'rgba(0, 0, 0, 0.4)'};
    flex: 1;
    justify-content: center;
    align-items: center;
    padding: 15px;
  `;

  const Body = Styled(View)`
    background-color: ${props => (props.transparent ? 'transparent' : '#FFF')};
    align-items: center;
    justify-content: center;
    padding: 15px;
    border-radius: 5px;
  `;

  const ContainerIndicator = Styled(View)`
    height: 40;
  `;

  const BodyText = Styled(View)`
    width: 100%
    margin-top: 15px;
    justify-content: center;
  `;

  const MessageText = Styled(Text)`
    textAlign: center;
  `;

  export default class ModalIndicator extends React.Component {
    getIndicators(indicators) {
      const { count, size, indicatorColor } = this.props;
      switch(indicators) {
        case 'ball': return <BallIndicator size={30} count={count} color={indicatorColor ? indicatorColor : Color.theme} />;
        case 'bar': return <BarIndicator count={count} color={indicatorColor ? indicatorColor : Color.theme} />;
        case 'dot': return <DotIndicator size={6} count={count} color={indicatorColor ? indicatorColor : Color.theme} />;
        case 'material': return <MaterialIndicator count={count} color={indicatorColor ? indicatorColor : Color.theme} />;
        case 'pacman': return <PacmanIndicator count={count} color={indicatorColor ? indicatorColor : Color.theme} />;
        case 'pulse': return <PulseIndicator count={count} color={indicatorColor ? indicatorColor : Color.theme} />;
        case 'skype': return <SkypeIndicator count={count} color={indicatorColor ? indicatorColor : Color.theme} />;
        case 'activity': return <UIActivityIndicator count={count} color={indicatorColor ? indicatorColor : Color.theme} />;
        case 'wave': return <WaveIndicator count={count} size={size && size} color={indicatorColor ? indicatorColor : Color.theme} />;
        default: return <ActivityIndicator size='large' count={count} color={indicatorColor ? indicatorColor : Color.theme} />;
      }
    }

    render() {
      const { onClose, visible, message, type, transparent, indicators } = this.props;
      let indicator = this.getIndicators(indicators);
      return (
        <Modal transparent animationType='fade' visible={visible} onRequestClose={!onClose ? () => {} : onClose}>
            <Wrapper {...this.props}>
              <Body transparent={transparent} style={type === 'small' ? {aspectRatio: 1} : {width: '100%'}}>
                <ContainerIndicator>
                  {indicator}
                </ContainerIndicator>
                {message && <BodyText>
                  <MessageText>{message}</MessageText>
                </BodyText>}
              </Body>
            </Wrapper>
        </Modal>
      );
    }
}
