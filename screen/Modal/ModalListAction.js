import React, { useState } from 'react';
import { SafeAreaView, View, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
import PropTypes from 'prop-types';

import Text from '../Text';
import { ScrollView } from 'react-native-gesture-handler';

const ModalListAction = ({ listLabel, visible, backdropOpacity, onSelectedAction, onClose, moreAction, onPressMoreAction }) => {
    return (
        <Modal
            isVisible={visible}
            onBackButtonPress={() => onClose()}
            onBackdropPress={() => onClose()}
            backdropOpacity={backdropOpacity}
            style={{width: '80%', alignSelf: 'center'}}
        >
            <SafeAreaView style={{justifyContent: 'center', alignItems: 'center'}}>
                <ScrollView style={{width: '100%'}}>
                    <View style={{width: '100%', backgroundColor: '#FFFFFF', paddingTop: 8}}>
                        {moreAction && <TouchableOpacity onPress={() => onSelectedAction()} style={{paddingHorizontal: 16, marginBottom: 8}}>
                            <Text>Semua</Text>
                        </TouchableOpacity>}
                        {listLabel.map((item, idx) =>
                            <TouchableOpacity key={idx} onPress={() => onSelectedAction(item)} style={{paddingHorizontal: 16, marginBottom: 8}}>
                                <Text>{item}</Text>
                            </TouchableOpacity>
                        )}
                        {moreAction && <TouchableOpacity onPress={() => onPressMoreAction()} style={{paddingHorizontal: 16, marginBottom: 8}}>
                            <Text>Lainnya..</Text>
                        </TouchableOpacity>}
                    </View>
                </ScrollView>
            </SafeAreaView>
        </Modal>
    )
}

ModalListAction.propTypes = {
    visible: PropTypes.bool,
    backdropOpacity: PropTypes.number,
    listLabel: PropTypes.array
}

ModalListAction.defaultProps = {
    visible: true,
    backdropOpacity: 0.7,
    listLabel: []
}

export default ModalListAction;