import React, { Component } from 'react';
import { View, Image, TouchableOpacity, ScrollView, Dimensions } from 'react-native';
import Styled from 'styled-components';
import Text from '../Text';
import Button from '../Button';

const { height, width } = Dimensions.get('window');

const ScrollItemView = Styled(ScrollView)`
   width: 100%;
   maxHeight: ${height * 0.6};
`;

const HeaderView = Styled(View)`
   width: 100%;
   height: 36;
   backgroundColor: #f0f0f0;
   justifyContent: center;
   alignItems: center;
`;

const HeaderText = Styled(Text)`
   fontSize: 15;
   color: #6C6E70;
   fontWeight: bold;
`;

const ItemView = Styled(TouchableOpacity)`
   

   width: 100%;
    height: 80;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    borderTopWidth: 1;
    borderColor: #DDDDDD;
    paddingHorizontal: 16;
`;

const ItemTextIconView = Styled(View)`
   width: 100%;
   height: 100%;
   justifyContent: center;
   alignItems: flex-start;
   flexDirection: row;
`;

const ItemText = Styled(Text)`
   fontSize: 16;
`;

const ItemSelectedText = Styled(ItemText)`
   color: #919294;
`;

const ItemTextView = Styled(View)`
    justifyContent: center;
    alignItems: flex-start;
    height: 100%;
    width: 74%;
    paddingLeft: 1%;
    paddingTop: 1;
    
`;

const IconCheckView = Styled(View)`
    justifyContent: center;
    alignItems: flex-start;
    height: 100%;
    width: 15%;
    paddingLeft: 10%;
    
`;

const IconImage = Styled(Image)`
    width: 10;
    height: 10;
`;

const SearchButton = Styled(Button)`
    padding: 16px
    marginVertical: 10px
`;

const ButtonView = Styled(View)`
    paddingHorizontal: 16;
`;
const MainView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    borderTopLeftRadius: 5;
    borderTopRightRadius: 5;
    backgroundColor: #FFFFFF;
`;
const OptionTouch = Styled(TouchableOpacity)`
    width: 100%;
    height: 80;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    borderTopWidth: 1;
    borderColor: #DDDDDD;
    paddingHorizontal: 16;
`;
const DragContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    paddingVertical: 10;
    alignItems: center;
`;

const DragView = Styled(View)`
    width: 40;
    height: 6;
    borderRadius: 5;
    backgroundColor: #DDDDDD;
`;
const TitleView = Styled(View)`
    width: 100%;
    minHeight: 1;
    paddingVertical: 10;
    marginBottom: 20;
    alignItems: center;
`;
const TitleText = Styled(Text)`
    letterSpacing: 1;
`;
const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const CheckIcon = Styled(View)`
    width: 20;
    height: 15;
`;

const NormalText = Styled(Text)`
`;

const check = require('../../images/check.png');
const checkMark = require('../../images/check-mark-2.png')

let arrayTitles = [];


class ModalTitlePicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: props.selected,
    };
    arrayTitles = [];
    if (this.props.type === 'adult' || this.props.type === 'ADULT') for (let i = 0; i < this.props.titles.length; i++)
        { if (this.props.titles[i].isAdult) arrayTitles.push(this.props.titles[i]); }

    else if (this.props.type === 'all') for (let i = 0; i < this.props.titles.length; i++)
      arrayTitles.push(this.props.titles[i]);

    else for (let i = 0; i < this.props.titles.length; i++)
        if (!this.props.titles[i].isAdult) arrayTitles.push(this.props.titles[i]);
  }

  onChange = (abbr) => {
    this.setState({ selected: abbr });
    this.props.onSave('title', abbr);
    this.props.onClose();
  }

  saveSelected = () => {
    this.props.onClose();
  }

  renderIcon = () => {
    return (
      <IconImage source={checkMark} />
    );
  }

  renderNormalText = (name) => {
    return (
      <Text>{name}</Text>
    );
  }

  renderSelectedText = (name) => {
    return (
      <Text color="text">{name}</Text>
    );
  }

  renderList = (itemList) => {
      console.log(this.props.selected, 'this.props.selected')
      return itemList.map((item, i) =>
        <OptionTouch key={i} onPress={() => this.onChange(item.abbr)}>
        <NormalText type={!(item.abbr === this.props.selected) ? 'medium' : 'bold'}>{item.name}</NormalText>
        {item.abbr === this.props.selected && <CheckIcon><ImageProperty resizeMode='stretch' source={check} /></CheckIcon>}
      </OptionTouch>
    );
  }

  render() {
    return (
      

      <MainView>
        <DragContainer>
          <DragView />
        </DragContainer>
        <TitleView><TitleText type='bold'>GELAR</TitleText></TitleView>
         { this.renderList(arrayTitles)}
      </MainView>

    );
  }
}

export default ModalTitlePicker;
