import React, { Component } from 'react';
import { View, FlatList, TextInput, ScrollView, ActivityIndicator, Keyboard, Platform, SafeAreaView } from 'react-native';
import Styled from 'styled-components';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import gql from 'graphql-tag';

import Text from '../Text';
import graphClient from '../../state/apollo';
import Color from '../Color';
import CardItemTravel from '../Travel/CardItemTravel';
import Header from '../Header';
import TouchableOpacity from '../Button/TouchableDebounce';

const MainView = Styled(View)`
    width: 100%;
    alignItems: center;
    flexDirection: column;
    backgroundColor: #FFFFFF;
    flex: 1;
`;

const BackgroundSearchBar = Styled(View)`
    width: 100%;
    minHeight: 1;
    alignItems: center;
    justifyContent: flex-start;
    backgroundColor: ${Color.theme};
    padding: 0px 16px 16px 16px;
`;

const SearchBar = Styled(View)`
    width: 100%;
    height: 50;
    flexDirection: row;
    backgroundColor: #FFFFFF;
`;

const CustomTextInput = Styled(TextInput)`
    width: 100%;
    height: 100%;
    color: ${Color.text};
    fontSize: 14;
`;

const CustomScrollView = Styled(ScrollView)`
    width: 100%;
    height: 100%;
    paddingRight: 16;
`;

const MiniLoadingIndicator = Styled(View)`
    flex: 1;
    justifyContent: center;
    alignItems: center;
`;

const CustomFlatList = Styled(FlatList)`
    width: 100%;
    padding: 8px 8px 0px 8px;
    flex: 1;
`;

const SearchIconContainer = Styled(View)`
    minWidth: 20;
    minHeight: 20;
    alignItems: center;
    justifyContent: center;
    margin: 10px 2px 10px 6px;
`;

const DefaultTourView = Styled(View)`
    width: 100%;
    flex: 1;
    alignItems: center;
    flexDirection: column;
    paddingLeft: 16;
`;

const TitleDefaultTourView = Styled(View)`
    width: 100%;
    height: 40;
    alignItems: flex-start;
    justifyContent: center;
`;

const PerDefaultTourView = Styled(TouchableOpacity)`
    width: 100%;
    height: 60;
    alignItems: flex-start;
    justifyContent: center;
    borderBottomWidth: 1;
    borderColor: #DDDDDD;
`;

const SearchIcon = Styled(EvilIcons)`
    color: #A8A699;
    fontSize: 32;
`;

const DefaultTourGroupText = Styled(Text)`
    fontSize: 12;
`;

const TitleDefaultTourGroupText = Styled(Text)`
    letterSpacing: 1;
`;

const CardTravel = Styled(CardItemTravel)`
    marginBottom: 12;
`;

const getTourAvailabilityQuery = gql`
  query(
    $query: String
  ) {
   tourAvailability(
    query: $query
   ) {
      id name slug
      startingPrice { price discount finalPrice }
      departures {
        id date duration quota reserved
        startingPrice { price discount finalPrice }
        airlines { logoUrl name }
      }
      pictures { url thumbnailUrl }
      countries{
        code
        name
      }
      tags {
        id name
        items { id name }
      }
   }
 }
`;

class TravelSearch extends Component {

  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      idRequest: 0,
      result: props.tours || null,
      loading: false,
    };
  }

  searchKeyword(searchText) {
    if (searchText.length <= 0) this.setState({ searchText, result: null, loading: false });
    else {
      let { idRequest } = this.state;
      idRequest++;
      this.setState({ idRequest, searchText, result: null, loading: true },
        () => this.searchTour(idRequest, searchText)
      );
    }
  }

  searchFilter(searchText) {
    let { tours } = this.props;
    if (searchText.length <= 0) this.setState({ searchText, result: tours });
    else {
      const text = searchText.toLowerCase();
      tours = tours.filter((item) => item.name.toLowerCase().indexOf(text) > -1);
      this.setState({ searchText, result: tours });
    }
  }

  searchTour(idRequest, query) {
    const variables = { query };

    graphClient
      .query({
        query: getTourAvailabilityQuery,
        variables
      })
      .then(res => {
        if (idRequest === this.state.idRequest) {
          let result = null;
          if (res.data.tourAvailability.length > 0) result = res.data.tourAvailability;
          this.setState({ result, loading: false });
        }
      }).catch(reject => {
        if (idRequest === this.state.idRequest) {
          this.setState({ result: null, loading: false });
        }
      });
  }

  onSelectedDefaultTourGroup(destination) {
    this.props.onClose();
    this.props.navigation.navigate('RecommendedTours', { data: destination });
  }

  renderMiniLoadingIndicator() {
    return (
      <MiniLoadingIndicator>
        <ActivityIndicator size='large' color={Color.loading} />
      </MiniLoadingIndicator>
    );
  }

  renderRow(data) {
    return (
      <CardTravel
        data={data.item}
        onPress={() => {
          this.props.onClose();
          this.props.navigation.navigate('SelectTravelDeparture', { data: data.item });
        }}
      />
    );
  }

  renderPerDefaultTourGroup() {
    return this.props.popularDestinations.result.map((destination, i) =>
      <PerDefaultTourView key={i} onPress={() => this.onSelectedDefaultTourGroup(destination)}>
        <DefaultTourGroupText type='semibold'>{destination.name}</DefaultTourGroupText>
      </PerDefaultTourView>
    );
  }

  renderDefaultTourGroups() {
    return (
      <DefaultTourView>
        <TitleDefaultTourView>
          <TitleDefaultTourGroupText type='bold'>DESTINASI POPULER</TitleDefaultTourGroupText>
        </TitleDefaultTourView>
        <CustomScrollView keyboardShouldPersistTaps='always' onScrollBeginDrag={() => Keyboard.dismiss()}>
          {this.renderPerDefaultTourGroup()}
        </CustomScrollView>
      </DefaultTourView>
    );
  }

  switchRender() {
    const { tours, popularDestinations } = this.props;
    const { loading, result, searchText } = this.state;
    if (searchText.length > 0) {
      if (loading) return this.renderMiniLoadingIndicator();
    }
    // else if (!tours && popularDestinations.result) return this.renderDefaultTourGroups();
    if (result) return (
      <CustomFlatList
        keyboardShouldPersistTaps='always'
        onScrollBeginDrag={() => Keyboard.dismiss()}
        data={result}
        keyExtractor={(item, index) => `${item.id}.${index}`}
        renderItem={(rowData) => this.renderRow(rowData)}
      />
    );
  }

  render() {
    const { onClose, tours } = this.props;
    return (
        <SafeAreaView style={{backgroundColor: Color.theme, flex: 1}}>
          <MainView>
            <Header showLeftButton iconLeftButton='close' title='Umroh & Travel' color={Color.theme} onPressLeftButton={onClose} />
            <BackgroundSearchBar>
              <SearchBar>
                <SearchIconContainer><SearchIcon name='search' /></SearchIconContainer>
                <CustomTextInput
                  placeholder='Cari Umroh atau Travel'
                  placeholderTextColor={Color.placeholder}
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  autoFocus
                  onChangeText={(text) => { if (tours) this.searchFilter(text); else this.searchKeyword(text); }}
                  selectionColor={Color.text}
                />
              </SearchBar>
            </BackgroundSearchBar>
            {this.switchRender()}
          </MainView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => ({
  popularDestinations: state.popularDestinations,
});

export default connect(mapStateToProps)(withNavigation(TravelSearch));
