import React, { Component } from 'react';
import { View, Image } from 'react-native';
import Styled from 'styled-components';

import Text from '../Text';
import TouchableOpacity from '../Button/TouchableDebounce';

const MainView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    borderTopLeftRadius: 5;
    borderTopRightRadius: 5;
    backgroundColor: #FFFFFF;
`;

const OptionTouch = Styled(TouchableOpacity)`
    width: 100%;
    height: 80;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    borderTopWidth: 1;
    borderColor: #DDDDDD;
    paddingHorizontal: 16;
`;

const DragContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    paddingVertical: 10;
    alignItems: center;
`;

const DragView = Styled(View)`
    width: 40;
    height: 6;
    borderRadius: 5;
    backgroundColor: #DDDDDD;
`;

const TitleView = Styled(View)`
    width: 100%;
    minHeight: 1;
    paddingVertical: 10;
    marginBottom: 20;
    alignItems: center;
`;

const CheckIcon = Styled(View)`
    width: 20;
    height: 15;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const TitleText = Styled(Text)`
    letterSpacing: 1;
`;

const NormalText = Styled(Text)`
`;

const check = require('../../images/check.png');

export default class FlightSort extends Component {
  onSelected(selected) {
    this.props.onSelectedSort(selected);
  }

  renderSortOptions() {
    return this.props.sortOptions.map((option, i) => (
      <OptionTouch key={i} onPress={() => this.onSelected(option.name)}>
        <NormalText type={!(option.name === this.props.selected) ? 'medium' : 'bold'}>{option.label}</NormalText>
        {option.name === this.props.selected && <CheckIcon><ImageProperty resizeMode='stretch' source={check} /></CheckIcon>}
      </OptionTouch>
    ));
  }

  render() {
    return (
      <MainView>
        <DragContainer>
          <DragView />
        </DragContainer>

        <TitleView>
          <TitleText type='bold'>URUTKAN BERDASARKAN</TitleText>
        </TitleView>
        {this.renderSortOptions()}
      </MainView>
    );
  }
}
