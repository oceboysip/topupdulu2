import React, { Component } from 'react';
import { View, TouchableOpacity as NativeTouchable, SafeAreaView, Modal } from 'react-native';
import Styled from 'styled-components';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Color from '../Color';
import Text from '../Text';

const ViewNotification = Styled(NativeTouchable)`
    width: 100%;
    flexDirection: row;
    backgroundColor: ${props => (props.type === 'success' ? Color.success :
                      props.type === 'error' ? Color.error :
                      props.type === 'warning' ? Color.warning : Color.info)};
    alignItems: center;
    justifyContent: flex-start;
    padding: 16px;
`;

const DescriptionView = Styled(View)`
    flex: 1;
    alignItems: flex-start;
`;

const WhiteText = Styled(Text)`
    textAlign: left;
    color: #FFFFFF;
`;

export default class ModalBanner extends Component {
  render() {
    const { label, visible, onClose } = this.props;
    return (
      <Modal
        animationType='fade'
        transparent
        visible={visible}
        onRequestClose={() => onClose()}
      >
        <SafeAreaView>
          <ViewNotification {...this.props}>
            <DescriptionView>
              <WhiteText type='medium'>{label}</WhiteText>
            </DescriptionView>

            <NativeTouchable onPress={() => onClose()}>
              <Ionicons name='md-close' size={20} style={{color: '#FFFFFF'}} />
            </NativeTouchable>
          </ViewNotification>
        </SafeAreaView>
      </Modal>
    );
  }
}
