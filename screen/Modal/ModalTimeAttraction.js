import React, { Component } from 'react';
import { View, ScrollView, Image, SafeAreaView } from 'react-native';
import Styled from 'styled-components';

import Text from '../Text';
import Header from '../Header';
// import SafeAreaView from '../SafeAreaView';
// import { Checkbox } from 'antd-mobile-rn';

const MainView = Styled(View)`
    height: 100%;
   flexDirection: column;
`;
const HeaderView = Styled(View)`
    height: 10%
`;

const BaseText = Styled(Text)`
    textAlign: left
`;

const CheckboxImage = Styled(Image)`
    width: 20;
    height: 20;
    marginRight: 4;
`;

// const CheckboxItem = Checkbox.CheckboxItem;
const checked = require('../../images/checkbox-selected.png');
const checkoff = require('../../images/checkbox-unselected.png');

export default class ModalTimeAttraction extends Component {
  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTitle: <Header title='Review & Book Attraction' showLeftButton {...screenProps} />,
    headerLeft: null,
    headerRight: null
  })

    constructor(props) {
        super(props);
        this.state = {

        };
        // console.log(this.props)
    }

    onClose = () => {
        this.props.onClose('modalTime')
    }

    selectFIlter = (e, value) => {
        console.log('select', e, value)
        this.props.onSelectFilter(value, this.props.index);
        this.onClose()
    }

    render() {
        return (
            <SafeAreaView>
                <MainView>
                  <HeaderView>
                    <Header title="Pilih Jam" showLeftButton={true} onPressLeftButton={() => this.onClose()} style={{borderBottomWidth: 1, borderBottomColor: '#DDDDDD'}} />
                  </HeaderView>

                    <ScrollView>
                    {this.props.tiket.timeslots.map((value, idx) => (
                        false && <CheckboxItem key={idx} onChange={(e) => this.selectFIlter(e, value)}>
                            <BaseText color="darkGrey">{value.startTime.substr(0, 5)} - {value.endTime.substr(0, 5)}</BaseText>
                        </CheckboxItem>
                    ))}
                    <CheckboxImage resizeMode='contain' source={true ? checked : checkoff} />
                    </ScrollView>
                </MainView>
            </SafeAreaView>
        );
    }
}
