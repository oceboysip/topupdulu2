import React, { Component } from 'react';
import { View, Platform, SafeAreaView } from 'react-native';
import Styled from 'styled-components';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Tabs, Tab, TabHeading } from 'native-base';

import Text from '../Text';
import Color from '../Color';
import TouchableOpacity from '../Button/TouchableDebounce';
import { Row, Col } from '../Grid';

import FlightFilterTransit from '../Flight/FlightFilterTransit';
import FlightFilterTime from '../Flight/FlightFilterTime';
import FlightFilterAirline from '../Flight/FlightFilterAirline';
import FlightFilterOthers from '../Flight/FlightFilterOthers';

const MainView = Styled(SafeAreaView)`
  backgroundColor: ${Color.theme};
  flex: 1;
`;

const HeaderView = Styled(View)`
    width: 100%;
    height: 60;
    backgroundColor: ${Color.theme};
`;

const RowView = Styled(Row)`
    height: 100%;
`;

const ColumnView = Styled(Col)`
    justifyContent: center;
    alignItems: center;
`;

const BackView = Styled(TouchableOpacity)`
    width: 100%;
    height: 100%;
    alignItems: center;
    justifyContent: center;
`;

const BackIcon = Styled(Icon)`
    fontSize: 23;
`;

const TitleText = Styled(Text)`
  color: #FFFFFF;
`;

const HeaderTabsText = Styled(Text)`
    fontSize: 12;
`;

export default class FlightFilter extends Component {
  constructor(props) {
    super(props);
    const { airportTransitFilter, stopFilter, departureFilter, arrivalFilter, airlineFilter, priceFilter } = props.selectedFilters;
    this.state = {
      activeTab: 0,
      airportTransitFilter,
      stopFilter,
      departureFilter,
      arrivalFilter,
      airlineFilter,
      priceFilter
    };
  }

  resetFilter() {
    this.setState({ airportTransitFilter: [], stopFilter: [], departureFilter: [], arrivalFilter: [], airlineFilter: [], priceFilter: this.props.optionsFilter.optionsPriceFilter });
  }

  onChangeFilter(value, check, type) {
    const tempArray = [].concat(this.state[type]);
    if (check) tempArray.push(value);
    else {
      const index = tempArray.indexOf(value);
      if (index !== -1) tempArray.splice(index, 1);
    }
    this.setState({ [type]: tempArray });
  }

  isNoFilter() {
    const { airportTransitFilter, stopFilter, departureFilter, arrivalFilter, airlineFilter, priceFilter } = this.state;
    const { optionsPriceFilter } = this.props.optionsFilter;
    if (airportTransitFilter.length > 0) return false;
    if (stopFilter.length > 0) return false;
    if (departureFilter.length > 0) return false;
    if (arrivalFilter.length > 0) return false;
    if (airlineFilter.length > 0) return false;
    if (priceFilter[0] !== optionsPriceFilter[0]) return false;
    if (priceFilter[1] !== optionsPriceFilter[1]) return false;
    return true;
  }

  renderAllTabs() {
    const { activeTab, airportTransitFilter, stopFilter, departureFilter, arrivalFilter, airlineFilter, priceFilter } = this.state;
    const { optionsAirportTransitFilter, optionsAirlineFilter, optionsPriceFilter } = this.props.optionsFilter;
    const tabs = [
      { title: 'Transit', children: <FlightFilterTransit optionsAirportTransitFilter={optionsAirportTransitFilter} selectedFilters={{ airportTransitFilter, stopFilter }} onChangeFilter={(value, check, type) => this.onChangeFilter(value, check, type)} /> },
      { title: 'Time', children: <FlightFilterTime selectedFilters={{ departureFilter, arrivalFilter }} onChangeFilter={(value, check, type) => this.onChangeFilter(value, check, type)} /> },
      { title: 'Airline', children: <FlightFilterAirline optionsAirlineFilter={optionsAirlineFilter} selectedFilters={{ airlineFilter }} onChangeFilter={(value, check, type) => this.onChangeFilter(value, check, type)} /> },
      { title: 'Others', children: <FlightFilterOthers optionsPriceFilter={optionsPriceFilter} selectedFilters={{ priceFilter }} onChangeFilter={(value) => this.setState({ priceFilter: value })} /> },
    ];
    return tabs.map((tab, i) =>
      <Tab
        key={i}
        heading={
          <TabHeading style={{ backgroundColor: '#F4F4F4' }}>
            <HeaderTabsText type={activeTab !== i ? 'medium' : 'bold'}>{tab.title}</HeaderTabsText>
          </TabHeading>
        }
        tabStyle={{ backgroundColor: '#F4F4F4', elevation: 0 }}
        activeTabStyle={{ backgroundColor: '#F4F4F4' }}
      >
        {i === activeTab && tab.children}
      </Tab>
    );
  }

  renderMainTabs() {
    return (
      <Tabs
        prerenderingSiblingsNumber={Infinity}
        tabStyle={{ backgroundColor: '#F4F4F4' }}
        tabContainerStyle={{ elevation: 0 }}
        tabBarUnderlineStyle={{ backgroundColor: '#000000', height: 2 }}
        onChangeTab={({ i }) => this.setState({ activeTab: i })}
        locked={Platform.OS === 'android'}
      >
        {this.renderAllTabs()}
      </Tabs>
    );
  }

  renderHeader() {
    const { airportTransitFilter, stopFilter, departureFilter, arrivalFilter, airlineFilter, priceFilter } = this.state;
    const isNoFilter = this.isNoFilter();
    return (
      <HeaderView>
        <RowView>
          <ColumnView size={2}>
            <BackView onPress={() => this.props.onClose({ airportTransitFilter, stopFilter, departureFilter, arrivalFilter, airlineFilter, priceFilter })}>
              <BackIcon name='clear' color={Color.white} />
            </BackView>
          </ColumnView>

          <ColumnView size={8}>
            <TitleText type='bold'>Filter</TitleText>
          </ColumnView>

          <ColumnView size={2}>
            <BackView onPress={isNoFilter ? () => {} : () => this.resetFilter()}>
              <TitleText type='medium' style={isNoFilter && { color: Color.disabled }}>Reset</TitleText>
            </BackView>
          </ColumnView>

        </RowView>
      </HeaderView>
    );
  }

  render() {
    return (
      <MainView>
        {this.renderHeader()}
        {this.renderMainTabs()}
      </MainView>
    );
  }

}
