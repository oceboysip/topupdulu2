import React, { Component } from 'react';
import { View, TouchableOpacity, Dimensions, Image, ScrollView, Animated } from 'react-native';
import Styled from 'styled-components';
// import GestureRecognizer from 'react-native-swipe-gestures';

import Header from '../Header';

const { width } = Dimensions.get('window');

const MainView = Styled(View)`
  flexDirection: column;
  width: 100%;
  height: 100%;
  backgroundColor: black;
`;

const CustomHeader = Styled(Header)`
  height: 60;
`;

const BodyView = Styled(View)`
  width: 100%;
  flex: 1;
  justifyContent: flex-end;
`;

const BigImagesView = Styled(View)`
  minWidth: 1;
  height: 70%;
  alignItems: center;
  justifyContent: center;
`;

const BigImagesContainer = Styled(Animated.View)`
  minWidth: 1;
  flex: 2;
  left: 0;
  flexDirection: row;
  alignItems: center;
`;

// const CustomGesture = Styled(GestureRecognizer)`
//   width: 100%;
//   height: 100%;
//   position: absolute;
//   backgroundColor: transparent;
// `;

const SelectImagesView = Styled(View)`
  minWidth: 1;
  flex: 1;
  flexDirection: row;
  padding: 0px 0px 10px 10px;
`;

const PerSelectImagesView = Styled(TouchableOpacity)`
  width: 90;
  height: 90;
  marginRight: 10;
`;

const OrangePerSelectImagesView = Styled(PerSelectImagesView)`
  width: 100;
  height: 100;
  borderWidth: 3;
  borderColor: #E78723;
`;

const Photos = Styled(Image)`
  width: 100%;
  height: 100%;
`;

const BigPhotos = Styled(Image)`
  width: ${width};
  height: 100%;
`;

const config = {
  velocityThreshold: 0.3,
  directionalOffsetThreshold: 80
};

// const photos = [
//   { url: 'https://dbijapkm3o6fj.cloudfront.net/resources/12089,1004,1,6,4,0,600,450/-4601-/20170213203551/hotel-ciputra-jakarta.jpeg', thumbnailUrl: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg' },
//   { url: 'https://t-ec.bstatic.com/images/hotel/max1024x768/891/89154892.jpg', thumbnailUrl: 'https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg' },
//   { url: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg', thumbnailUrl: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg' },
//   { url: 'https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg', thumbnailUrl: 'https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg' },
//   { url: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg', thumbnailUrl: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg' },
//   { url: 'https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg', thumbnailUrl: 'https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg' },
//   { url: 'https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg', thumbnailUrl: 'https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg' },
//   { url: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg', thumbnailUrl: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg' },
//   { url: 'https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg', thumbnailUrl: 'https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg' },
//   { url: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg', thumbnailUrl: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg' },
//   { url: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg', thumbnailUrl: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg' },
//   { url: 'https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg', thumbnailUrl: 'https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg' },
//   { url: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg', thumbnailUrl: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg' },
//   { url: 'https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg', thumbnailUrl: 'https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg' },
//   { url: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg', thumbnailUrl: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg' },
// ];

class ModalPhotoViewer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selected: props.selected || 0,
      animated: new Animated.Value(0)
    };
  }

  onSwipe(direction) {
    const { photos } = this.props;
    let { selected } = this.state;
    switch (direction) {
      case 'SWIPE_LEFT':
        selected++;
        if (selected < photos.length) this.slide(selected);
        break;
      case 'SWIPE_RIGHT':
        selected--;
        if (selected >= 0) this.slide(selected);
        break;
    }
  }

  slide(selected) {
    this.setState({ selected }, () =>
      Animated.timing(this.state.animated, {
        toValue: selected,
        duration: 300,
      }).start()
    );
  }

  renderAllPhotos(photos) {
    return photos.map((photo, i) => (
      i !== this.state.selected ? <PerSelectImagesView key={i} onPress={() => this.slide(i)}>
        <Photos source={{ uri: photo.thumbnailUrl }} />
      </PerSelectImagesView>
      : <OrangePerSelectImagesView key={i}>
        <Photos source={{ uri: photo.thumbnailUrl }} />
      </OrangePerSelectImagesView>
    ));
  }

  renderAllBigPhotos(photos) {
    return photos.map((photo, i) =>
      <BigImagesView key={i}>
        <BigPhotos resizeMode='contain' source={{ uri: photo.url }} />
      </BigImagesView>
    );
  }

  render() {
    const { photos, onClose } = this.props;
    const { animated } = this.state;
    const maxRange = photos.length - 1;

    return (
      <MainView>
        <CustomHeader transparentMode showLeftButton onPressLeftButton={onClose} iconLeftButton='close' colorIconLeft='white' />
        <BigImagesContainer style={{ transform: [{ translateX: animated.interpolate({ inputRange: [0, maxRange], outputRange: [0, -width * maxRange] }) }] }}>
          {this.renderAllBigPhotos(photos)}
        </BigImagesContainer>
        {/*<CustomGesture config={config} onSwipe={(direction) => this.onSwipe(direction)} />*/}
        <BodyView>
          <SelectImagesView>
          <ScrollView horizontal showsHorizontalScrollIndicator={false} contentContainerStyle={{ alignItems: 'center' }}>
            {this.renderAllPhotos(photos)}
          </ScrollView>
          </SelectImagesView>
        </BodyView>
      </MainView>
    );
  }
}

export default ModalPhotoViewer;
