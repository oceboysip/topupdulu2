import React, { Component } from 'react';
import { View, FlatList, TextInput, ScrollView, ActivityIndicator, Keyboard, Platform, SafeAreaView } from 'react-native';
import Styled from 'styled-components';
import { withNavigation } from 'react-navigation';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import gql from 'graphql-tag';

import Text from '../Text';
import graphClient from '../../state/apollo';
import Color from '../Color';
import PopularAttractions from '../Attraction/CardPopularAttractions';
import Header from '../Header';
import TouchableOpacity from '../Button/TouchableDebounce';

const MainView = Styled(View)`
  width: 100%;
  height: 100%;
  alignItems: center;
  flexDirection: column;
  backgroundColor: #FAFAFA;
`;

const HeaderScreen = Styled(Header)`
  height: 60;
`;

const MainSearchBar = Styled(View)`
  width: 100%;
  minHeight; 1;
  alignItems: center;
  justifyContent: flex-start;
  backgroundColor: ${Color.theme};
  padding: 0px 16px 16px 16px;
`;

const SearchBar = Styled(View)`
  width: 100%;
  height: 50;
  flexDirection: row;
  backgroundColor: #FFFFFF;
`;

const SearchIconContainer = Styled(View)`
  minWidth: 20;
  minHeight: 20;
  alignItems: center;
  justifyContent: center;
  margin: 10px 2px 10px 6px;
`;

const SearchIcon = Styled(EvilIcons)`
  color: #A8A699;
  fontSize: 32;
`;

const CustomTextInput = Styled(TextInput)`
  width: 100%;
  height: 100%;
  color: ${Color.text};
  fontSize: 12;
`;

const DefaultView = Styled(View)`
  flex: 1;
  width: 100%;
  alignItems: center;
  justifyContent: center;
  paddingLeft: 16;
`;

const TitleDefaultView = Styled(View)`
  height: 40;
  width: 100%;
  alignItems: flex-start;
  justifyContent: center;
`;

const TitleDefaultText = Styled(Text)`
  letterSpacing: 1;
`;

const CustomScrollView = Styled(ScrollView)`
    width: 100%;
    height: 100%;
    paddingRight: 16;
`;

const MiniLoadingIndicator = Styled(View)`
    flex: 1;
    justifyContent: center;
    alignItems: center;
`;

const PerDefaultAttraction = Styled(TouchableOpacity)`
    width: 100%;
    height: 60;
    alignItems: flex-start;
    justifyContent: center;
    borderBottomWidth: 1;
    borderColor: #DDDDDD;
`;

const PerDefaultText = Styled(Text)`
    fontSize: 12;
`;

const CustomFlatList = Styled(FlatList)`
    width: 100%;
    padding: 8px 8px 0px 8px;
    flex: 1
`;

const CustomAttractions = Styled(PopularAttractions)`
  marginBottom: 12;
`;

// =============================================================================

// const getAttractionAvailability = gql`
//   query(
//     $query: String
//   ){
//     attractionsAvailability
//   }
// `;

const getAttractionAvailability = gql`
  query(
    $param: paramAttraction!
  ){
    attractions(
      param: $param
    ){
      AttractionProducts{
        uuid
        title
        AttractionsProductsDetail {
          typename
          AttractionConfig {
            name
            value
          }
          baseprice
          currency{
            code
          }
          locations{
            city
            country
          }
          photos {
            path
          }
        }
      }
    }
  }
`;

class AttractionSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      result: [],
      destination: [],
      readyDest: false
    }

    this.getRelatedAttraction();
  }

  getShorterArray(array){
    return array.slice(0, 6);
  }

  searchKeyword(searchText){
    const self = this;
    const query = searchText.toLowerCase();

    if (query.length > 0) {
      self.setState({
        readyDest: true,
        searchText: query
      }, self.getAttractionByQuery())
    }else {
      self.setState({
        readyDest: false,
        result: [],
        searchText: query
      })
    }
  }

  getAttractionByQuery(){
    const { searchText } = this.state;
    const { value } = this.props;
    const variables = {
      param: {
        query: searchText,
        destination: value ? value : '',
        itemPerPage: 10,
        page: 1,
        sort:{
          popular: 'desc'
        },
      }
    }

    graphClient.query({
      query: getAttractionAvailability,
      variables
    }).then(res => {
      if (res.data.attractions.AttractionProducts) {
        this.setState({
          result: res.data.attractions.AttractionProducts,
          readyDest: false
        })
      }
    }).catch(err => {
      console.log(err, 'err');
    })
  }

  getRelatedAttraction(){
    const getAttractionConfig = gql`
      query{
        attractionConfig{
          filterDestinations{
            name
            value
          }
        }
      }
    `;
    graphClient.query({
      query: getAttractionConfig,

    }).then(res => {
      if(res.data.attractionConfig){
        this.setState({
          destination: res.data.attractionConfig.filterDestinations,
          readyDest: true
        })
      }
    }).catch(reject =>{
        console.log(reject)
    })
  }

  renderMiniLoadingIndicator() {
    return (
      <MiniLoadingIndicator>
        <ActivityIndicator size='large' color={Color.loading} />
      </MiniLoadingIndicator>
    );
  }

  onSelectDefaultAttraction(dest){
    this.props.onClose();
    this.props.navigation.navigate('PopularAttraction', { data: dest });
  }

  renderPerDefaultAttraction(){
    return this.getShorterArray(this.state.destination).map((dest, idx) =>
      <PerDefaultAttraction key={idx} onPress={() => this.onSelectDefaultAttraction(dest)}>
        <PerDefaultText type='semibold'>{dest.name}</PerDefaultText>
      </PerDefaultAttraction>
    )
  }

  renderAttractionPerCountry(data){
    return data.map((item, index) =>
      <CustomAttractions key={index} data={item} onPress={() => { this.props.onClose(); this.props.navigation.navigate('DetailAttraction', { data: item }); }} />
    )
  }

  renderDefaultAttraction(){
    const { data } = this.props;

    return(
      <DefaultView>
        <TitleDefaultView>
          <TitleDefaultText type='bold'>{data ? data.title.toUpperCase() :'ATRAKSI NEGARA FAVORIT'}</TitleDefaultText>
        </TitleDefaultView>
        <CustomScrollView keyboardShouldPersistTaps='always' onScrollBeginDrag={() => Keyboard.dismiss()}>
          {data ? this.renderAttractionPerCountry(data.attractions) : this.renderPerDefaultAttraction()}
        </CustomScrollView>
      </DefaultView>
    )
  }

  switchRender(){
    const { result, searchText, destination } = this.state;

    if (searchText.length > 0 && result.length === 0) {
      return this.renderMiniLoadingIndicator();
    }else if (destination.length > 0 && result.length === 0) {
      return this.renderDefaultAttraction();
    }

    if (result.length > 0) {
      return(
        <CustomFlatList
          keyboardShouldPersistTaps='always'
          onScrollBeginDrag={() => Keyboard.dismiss()}
          data={result}
          keyExtractor={(item, index) => `${item.id}.${index}`}
          renderItem={(rowData) => this.renderRow(rowData)}
        />
      )
    }
  }

  renderRow(data) {
    const { onClose, navigation } = this.props;
    return (
      <CustomAttractions data={data.item} onPress={() => { onClose(); navigation.navigate('DetailAttraction', { data: data.item }); }} />
    );
  }

  render(){
    // console.log(this.props, 'propsASearch');
    // console.log(this.state, 'stateASearch');

    const { onClose } = this.props;

    return(
      <SafeAreaView style={{backgroundColor: Color.theme, flex: 1}}>
        <MainView>
          <HeaderScreen showLeftButton iconLeftButton='close' title='Cari' onPressLeftButton={onClose} />
          <MainSearchBar>
            <SearchBar>
              <SearchIconContainer><SearchIcon name='search' /></SearchIconContainer>
              <CustomTextInput
                placeholder='Ketik nama atraksi atau aktivitas'
                placeholderTextColor='#DDDDDD'
                underlineColorAndroid='transparent'
                autoCorrect={false}
                autoFocus
                selectionColor={Color.text}
                onChangeText={(text) =>  this.searchKeyword(text)}
              >
              </CustomTextInput>
            </SearchBar>
          </MainSearchBar>
          {this.switchRender()}
        </MainView>
      </SafeAreaView>
    )
  }
}

export default withNavigation(AttractionSearch);
