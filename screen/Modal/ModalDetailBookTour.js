import React, { Component } from 'react';
import { View, ScrollView, Platform } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import Styled from 'styled-components';

import Header from '../Header';
import FormatMoney from '../FormatMoney';
import Color from '../Color';
import Text from '../Text';

const MainView = Styled(View)`
    width: 100%;
    flexDirection: column;
    backgroundColor: #FAFAFA;
    flex: 1
`;

const CustomHeader = Styled(Header)`
    height: 60;
`;

const NormalText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
    lineHeight: 28;
`;

const LineView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
`;

const DetailInfoMainView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    padding: 12px 16px 12px 16px;
    backgroundColor: #FFFFFF;
`;

const LowerDetailInfoView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    padding: 8px 0px 0px 0px;
`;

const UpperDetailInfoView = Styled(LowerDetailInfoView)`
    borderBottomWidth: 0.5;
    borderColor: #231F20;
    padding: 0px 0px 8px 0px;
`;

const typeBeds = [
  { code: 'adultSingle', label: 'Adult (Single)' },
  { code: 'adultTwinSharing', label: 'Adult (Twin Sharing)' },
  { code: 'childTwinSharing', label: 'Child (Twin Sharing)' },
  { code: 'childExtraBed', label: 'Child (Extra Bed)' },
  { code: 'childNoBed', label: 'Child (No Bed)' }
];

export default class ModalDetailBookTour extends Component {

  getSumAllProps(obj) {
    return Object.values(obj).reduce((a, b) => a + b);
  }

  renderUpperDetails(allBedsPrice, price, totalBeds) {
    return typeBeds.map((type, i) => {
      if (allBedsPrice[type.code] >= 0) {
        return (
          <View key={i}>
            <LineView>
              <NormalText type='medium'>{type.label}:</NormalText>
              <NormalText type='medium'>{FormatMoney.getFormattedMoney(allBedsPrice[type.code])}</NormalText>
            </LineView>
            {totalBeds[type.code] > 1 && <LineView>
              <NormalText type='medium'>{totalBeds[type.code]} @ {FormatMoney.getFormattedMoney(price[`${type.code}Price`].finalPrice)}</NormalText>
            </LineView>}
          </View>
        );
      }
      return null;
    });
  }

  renderLowerDetail(subtotal, totalGuests, totalVat, totalTaxFuel, vatPercent, airportTaxAndFuelPrice, includeVisa, totalVisaPrice) {
    const subDetails = [
      { label: 'Subtotal', value: FormatMoney.getFormattedMoney(subtotal) },
      { label: `Ppn${vatPercent !== 0 ? ` ${vatPercent} %` : ''}`, value: vatPercent !== 0 ? FormatMoney.getFormattedMoney(totalVat) : 'Sudah Termasuk' },
      { label: `Airport Tax & Fuel x ${totalGuests}`, value: airportTaxAndFuelPrice !== 0 ? FormatMoney.getFormattedMoney(totalTaxFuel) : 'Sudah Termasuk' },
    ];
    if (includeVisa) subDetails.push({ label: `Termasuk Visa x ${totalGuests}`, value: FormatMoney.getFormattedMoney(totalVisaPrice) });

    return subDetails.map((detail, i) =>
      <LineView key={i}>
        <NormalText type='medium'>{detail.label}:</NormalText>
        <NormalText type='medium'>{detail.value}</NormalText>
      </LineView>
    );
  }

  render() {
    const { data, onClose } = this.props;
    const { tourDetail, allBedsPrice, totalVat, totalTaxFuel, totalVisaPrice, totalBeds, guests, includeVisa } = data;
    const { price } = tourDetail.departure;
    const { vatPercent, airportTaxAndFuelPrice } = price;
    const subtotal = this.getSumAllProps(allBedsPrice);
    const totalGuests = this.getSumAllProps(guests);

    return (
        <SafeAreaView style={{ backgroundColor: Color.theme, height: '100%' }} >
            { Platform.OS  === 'ios' && <View style={{ height: 30 }}></View> }
            <MainView>
              <CustomHeader showLeftButton iconLeftButton='close' title='Rincian Bayar' onPressLeftButton={onClose} />
                <ScrollView>
                  <DetailInfoMainView>
                    <UpperDetailInfoView>
                      {this.renderUpperDetails(allBedsPrice, price, totalBeds)}
                    </UpperDetailInfoView>
                    <LowerDetailInfoView>
                      {this.renderLowerDetail(subtotal, totalGuests, totalVat, totalTaxFuel, vatPercent, airportTaxAndFuelPrice, includeVisa, totalVisaPrice)}
                    </LowerDetailInfoView>
                  </DetailInfoMainView>
                </ScrollView>
            </MainView>
      </SafeAreaView>
    );
  }
}
