import React, { Component } from 'react';
import { View, SafeAreaView, Modal, FlatList, TextInput } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { TextInputMask } from 'react-native-masked-text';
import gql from 'graphql-tag';
import Styled from 'styled-components';
import EvilIcons from 'react-native-vector-icons/EvilIcons';

import Text from '../Text';
import Button from '../Button';
import Color from '../Color';
import Header from '../Header';
import TouchableOpacity from '../Button/TouchableDebounce';
import Client from '../../state/apollo';
import FormatMoney from '../FormatMoney';

const BackgroundSearchBar = Styled(View)`
    width: 100%;
    alignItems: center;
    justifyContent: flex-start;
    backgroundColor: ${Color.theme};
    paddingHorizontal: 16;
`;

const SearchBar = Styled(View)`
    width: 100%;
    height: 45;
    flexDirection: row;
    backgroundColor: #FFFFFF;
    borderRadius: 22.5;
    alignItems: center;
`;

const SearchIconContainer = Styled(View)`
    width: 32;
    height: 32;
    borderRadius: 16;
    alignItems: center;
    justifyContent: center;
    marginHorizontal: 8;
    backgroundColor: ${Color.primary};
`;

const SearchIcon = Styled(EvilIcons)`
    color: #FFFFFF;
    fontSize: 32;
`;

const CustomTextInput = Styled(TextInput)`
    width: 100%;
    height: 100%;
    color: ${Color.theme};
    fontSize: 16;
    letterSpacing: 0.5
`;

export default class PdamScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listPdamArea: [],
            searchText: ''
        }
    }

    searchFilter(searchText) {
        const text = searchText.toLowerCase();
        let data = this.props.listPdamArea;
    
        listPdamArea = data.filter((item) => (
          item.toLowerCase().indexOf(text) > -1
        ));
    
        this.setState({ listPdamArea });
      }
    
      onChangeText(searchText) {
        this.setState({ searchText });
        this.searchFilter(searchText);
      }
    render() {
        const { listPdamArea, searchText } = this.state;

        return (
          <SafeAreaView style={{backgroundColor: Color.theme, flex: 1}}>
            <Header style={{height: 70}} onPressLeftButton={() => this.props.closeModal()}>
              <BackgroundSearchBar>
                <SearchBar>
                  <SearchIconContainer><SearchIcon name='search' /></SearchIconContainer>
                  <CustomTextInput
                    ref={(input) => this.searchBarHotel = input}
                    placeholder='Cari Area PDAM'
                    placeholderTextColor='#DDDDDD'
                    underlineColorAndroid='transparent'
                    autoCorrect={false}
                    autoFocus
                    maxLength={25}
                    onChangeText={(text) => this.onChangeText(text)}
                    selectionColor={Color.text}
                  />
                </SearchBar>
              </BackgroundSearchBar>
            </Header>
            <View style={{paddingHorizontal: 16, backgroundColor: '#FFFFFF', marginBottom: 60}}>
              <FlatList
                data={searchText !== '' ? listPdamArea : this.props.listPdamArea}
                style={{height: '100%'}}
                keyExtractor={( item, idx ) => `${item}.${idx}`}
                renderItem={({ item, idx }) => <TouchableOpacity onPress={() => this.props.onSelectedArea(item)}>
                    <View style={{paddingVertical: 12, borderBottomWidth: 0.5, borderColor: '#DDDDDD', alignItems: 'flex-start'}}>
                    <Text style={{paddingLeft: 8}}>{item}</Text>
                    </View>
                </TouchableOpacity>}
                ListEmptyComponent={() => <View style={{marginTop: 20}}><Text>DATA TIDAK TERSEDIA</Text></View>}
              />
            </View>
          </SafeAreaView>
        )
      }
}