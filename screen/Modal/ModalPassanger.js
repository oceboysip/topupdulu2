import React, { Component } from 'react';
import { View, Modal, ScrollView, Keyboard, Platform, Modal as AntdModal, SafeAreaView } from 'react-native';
import Styled from 'styled-components';
import Icon from 'react-native-vector-icons/MaterialIcons';
import TouchableOpacity from '../Button/TouchableDebounce';
import ModalTitlePicker from './ModalTitlePicker';
import Text from '../Text';
import Input from '../Input/Input';
import Color from '../Color';
import { TextInputMask } from 'react-native-masked-text'

const MainView = Styled(View)`
  flex: 1;
  width: 100%;
  backgroundColor: #FFFFFF;
  flexDirection: column;
`;

const TopView = Styled(View)`
  flexDirection: column;
`;
const HeaderView = Styled(View)`
  flexDirection: row
  backgroundColor: ${Color.theme}
  padding: 23px 16px 10px 17px
  width: 100%
`;
const LeftHeaderView = Styled(TouchableOpacity)`
  width: 20%
  alignItems: flex-start
`;

const MidHeaderView = Styled(View)`
    width: 60%
`;

const RightHeaderView = Styled(View)`
    width: 20%
    alignItems: flex-end
`;

const AttentionView = Styled(View)`
    alignItems: flex-start;
    backgroundColor: #231F20;
    flexDirection: column;
    padding: 10px 16px;
`;
const Container = Styled(View)`
    padding: 0px;
    flexDirection: row;
`;
const LeftView = Styled(View)`
    paddingTop: 3px;
    width: 4%;
    alignItems: flex-start;
`;
const RightView = Styled(View)`
    width: 96%;
    paddingTop: 3px;
    alignItems: flex-start
`;

const BaseText = Styled(Text)`
  textAlign: left;
  fontSize:12;

`;
const PassangerView = Styled(View)`
  margin: 8px 16px
`;
const WhiteText = Styled(BaseText)`
    color: white;
`;
const BulletText = Styled(Text)`
    fontSize: 30;
    color: white;
    lineHeight:24px;
`;
const BackIcon = Styled(Icon)`
    fontSize: 23;
    color: #000000;
`;

export default class ModalPassanger extends Component {

  constructor(props) {
    super(props);
    this.state = {
      refresh: false,
      visibleTitles: false,
      dt: props.pax.birthDate
    };
  }

  onPassangerChange = (name, value) => {
    this.props.onPassangerChange(name, this.props.pax.id, value)
  }

  onTitleClose = () => {
    this.setState({
      visibleTitles: false
    })
  }

  onSelect = (key) => {
    this.setState(prevState => {
      return {
        ...prevState,
        [key]: true
      }
    })
  }

  render() {
    const { pax, tour } = this.props;
    const term = [
      { label: 'Masukkan nama penumpang seperti yang tertera pada ID' },
      { label: 'Pastikan ID Anda Valid setidaknya 6 bulan setelah tanggal keberangkatan' }
    ];

    return (
      <SafeAreaView style={{backgroundColor: Color.theme, flex: 1}}>
        <MainView>
          <TopView>
            <HeaderView>
              <LeftHeaderView onPress={() => this.props.closeModal('modalPassangers')}>
                <BackIcon name='clear' />
              </LeftHeaderView>
              <MidHeaderView>
                <Text type='bold'>Tambah Penumpang</Text>
              </MidHeaderView>
              <RightHeaderView>
                <Text type='medium' onPress={() => this.props.closeModal('modalPassangers', pax.id, 'simpan')}>Simpan</Text>
              </RightHeaderView>
            </HeaderView>
          </TopView>
          <ScrollView>
            <AttentionView>
              <Container style={{marginBottom: 10}}>
                <LeftView>
                  <BulletText>{'\u2022'}</BulletText>
                </LeftView>
                <RightView>
                  <WhiteText type='medium'>{term[0].label}</WhiteText>
                </RightView>
              </Container>
              <Container style={{paddingTop: 0}}>
                <LeftView>
                  <BulletText>{'\u2022'}</BulletText>
                </LeftView>
                <RightView>
                  <WhiteText type='medium'>{term[1].label}</WhiteText>
                </RightView>
              </Container>
            </AttentionView>
            <PassangerView>
              <Input
                  componentName="select"
                  placeholder="Pilih Gelar"
                  name="title"
                  label="Gelar"
                  error={pax.errors.title}
                  value={pax.title}
                  modalName="visibleTitles"
                  onSelect={this.onSelect}
              />
              <Input
                  name="firstName"
                  label='Nama Depan'
                  placeholder=""
                  keyboardType="default"
                  returnKeyType="next"
                  blurOnSubmit={false}
                  value={pax.firstName}
                  error={pax.errors.firstName}
                  onChangeValue={this.onPassangerChange}
                  onBlurValue={this.onValidatePassanger}
              />
              <Input
                  name="lastName"
                  label='Nama Belakang'
                  placeholder=""
                  keyboardType="default"
                  returnKeyType="next"
                  blurOnSubmit={false}
                  value={pax.lastName}
                  error={pax.errors.lastName}
                  onChangeValue={this.onPassangerChange}
                  onBlurValue={this.onValidatePassanger}
              />
               <Text
                style={{
                  color: '#A8A699',
                  textAlign: 'left',
                  fontSize: 12,
                  paddingTop: 7,
                  paddingBottom: 15,
                }}
              >Tanggal Lahir</Text>
              <TextInputMask
                  name="birthDate"
                  type={'datetime'}
                  label='Tanggal Lahir'
                  placeholder="YYYY-MM-DD"
                  keyboardType="numeric"
                  returnKeyType="done"
                  blurOnSubmit={false}
                  error={pax.errors.birthDate}
                  options={{format: 'YYYY-MM-DD'}}
                  value={this.state.dt}
                  onChangeText={(text) => this.setState({ dt: text }, () => this.onPassangerChange('birthDate', text))}
                  onSubmitEditing={Keyboard.dismiss}
                  onBlurValue={this.onValidatePassanger}
                  style={{borderBottomWidth: 1, borderColor: '#DDDDDD', paddingBottom: 0}}
              />
            </PassangerView>
          </ScrollView>

          <AntdModal
            popup
            visible={this.state.visibleTitles}
            onClose={this.onTitleClose}
            animationType='slide-up'
            maskClosable
          >
            <ModalTitlePicker
              titles={this.props.titles}
              nameModal="visibleTitles"
              type="adult"
              onSave={this.onPassangerChange}
              onClose={this.onTitleClose}
              selected={pax.title}
            />
          </AntdModal>
        </MainView>
      </SafeAreaView>
    );
  }
}
