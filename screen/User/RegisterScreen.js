import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Image, TouchableOpacity, ImageBackground, Modal, ScrollView, Text as ReactText } from 'react-native';
import FloatingLabel from 'react-native-floating-labels';
import RadioGroup from 'react-native-radio-buttons-group';
import axios from 'axios';
import querystring from 'querystring';
import Styled from 'styled-components';

import Text from '../Text';
import LoginStyle from '../../style/LoginStyle';
import Header from '../Header';
import Color from '../Color';
import { constant } from '../../constants';
import ModalIndicator from '../Modal/ModalIndicator';
import ModalInformation from '../Modal/ModalInformation';

const styles = StyleSheet.create(LoginStyle);

const MainViewFixMode = Styled(View)`
    paddingHorizontal: 16;
    shadowColor: #000;
    paddingVertical: 10;
    width: 100%;
`;

const ContainerOptionView = Styled(View)`
    flexDirection: row;
    paddingVertical: 10;
    justifyContent: space-between;
    alignSelf: stretch;
    alignItems: center;
`;

const bgScreen = require('../../images/bg_login.jpg');
const logoSiago = require('../../images/logotopup.png');

const genderData = [
    { label: 'Laki-laki', value: 'laki-laki', color: Color.theme },
    { label: 'Perempuan', value: 'perempuan', color: Color.theme },
];

const RegisterScreen = (props) => {
    const [userData, setUserData] = useState({
        name: '',
        gender: 'laki-laki',
        phone: '',
        email: '',
        password: '',
        password2: ''
    });
    const [loadingSubmit, setLoadingSubmit] = useState(false);
    const [loadingWarning, setLoadingWarning] = useState(false);
    const [loadingError, setLoadingError] = useState(false);
    const [loadingSuccess, setLoadingSuccess] = useState(false);
    const [reponseMessage, setResponseMessage] = useState('');

    const onChangeText = (type, value) => {
        const newUserData = userData;
        newUserData[type] = value;
        setUserData(newUserData);
    }

    const validateState = (type) => {
        if (type === '') {
            return false;
        }

        return true;
    }

    const onSubmit = async() => {
        const body = userData;
        let valid = true;

        const arrBody = [
            { message: 'Nama Lengkap harus diisi', value: body.name },
            { message: 'Nomor Telepon harus diisi', value: body.phone },
            { message: 'E-mail harus diisi', value: body.email },
            { message: 'Kata Sandi harus diisi', value: body.password },
            { message: 'Ulangi Kata Sandi harus diisi', value: body.password2 },
        ];

        for (var i = 0; i < arrBody.length; i++) {
            if (validateState(arrBody[i].value) === false) {
                valid = false;
                setLoadingWarning(true);
                setResponseMessage(arrBody[i].message);
                setTimeout(() => {
                    setLoadingWarning(false);
                    setResponseMessage('');
                }, 2000);
                return;
            }
        }

        if (body.password !== body.password2) {
            valid = false;
            setLoadingWarning(true);
            setResponseMessage('Kata Sandi yang diinput tidak sama');
            setTimeout(() => {
                setLoadingWarning(false);
                setResponseMessage('');
            }, 2000);
            return;
        }

        if (valid) {
            await setLoadingSubmit(true);

            await axios.post(constant.api_url+'/user/signup', querystring.stringify(body), {
                headers: {'X-Auth-Token': constant.api_token}
            })
            .then(async res => {
                console.log(res, 'res regis');
                if (res.data.status !== 'error') {
                    await setLoadingSuccess(true);
                    await setResponseMessage('Register berhasil');
                    await setTimeout(() => {
                        setLoadingSuccess(false);
                        setResponseMessage('');
                        props.navigation.goBack();
                    }, 1500);
                } else {
                    await setLoadingWarning(true);
                    await setResponseMessage(res.data.message.toString());
                    await setTimeout(() => {
                        setLoadingWarning(false);
                        setResponseMessage('');
                    }, 2000);
                }

                await setLoadingSubmit(false);
            })
            .catch(async err => {
                console.log(JSON.stringify(err), 'err regis');
                await setLoadingSubmit(false);

                await setLoadingError(true);
                await setResponseMessage(err.message);
                await setTimeout(() => {
                    setLoadingError(false);
                    setResponseMessage('');
                }, 2000);
            })
        }
    }

    return (
        <ImageBackground source={bgScreen} style={{width: '100%', height: '100%'}}>
            <MainViewFixMode>
                <ContainerOptionView>
                    <View style={{alignItems: 'flex-start', width: '10%'}}>
                        <TouchableOpacity onPress={() => props.navigation.pop()} style={{width: '100%', aspectRatio: 1, backgroundColor: Color.theme, borderRadius: 20, justifyContent: 'center', alignItems: 'center'}}>
                            <Image style={{width: 30, height: 30, resizeMode: 'contain'}} source={require('../../images/left-arrow.png')}/>
                        </TouchableOpacity>
                    </View>
                </ContainerOptionView>
            </MainViewFixMode>

            <ScrollView>
                <View style={styles.container}>
                    <View style={styles.innerLogin}>
                        <View style={styles.insideLogo}>
                            <Image style={{width:250,height:184,resizeMode:'contain'}} source={logoSiago} />
                        </View>
                        <View style={styles.boxInner}>
                            <View style={styles.groupForm}>
                                <FloatingLabel
                                    clearButtonMode='always'
                                    returnKeyType="next"
                                    value={userData.name}
                                    onChangeText={(value) => onChangeText('name', value)}
                                    labelStyle={styles.labelInput}
                                    inputStyle={styles.inputBox}
                                >
                                    Nama Lengkap
                                </FloatingLabel>
                            </View>

                            <View style={{alignItems: 'flex-start', paddingBottom: 8, marginBottom: 4, borderBottomWidth: 2, borderColor: '#B7B7B7', borderRadius: 4}}>
                                <ReactText style={{color: Color.placeholder, marginBottom: 8}}>Jenis Kelamin</ReactText>
                                <RadioGroup
                                    radioButtons={genderData}
                                    onPress={() => {
                                        let selectedGender = genderData.find(e => e.selected === true);
                                        onChangeText('gender', selectedGender.value)
                                    }}
                                    flexDirection='row'
                                />
                            </View>

                            <View style={styles.groupForm}>
                                <FloatingLabel
                                    clearButtonMode='always'
                                    returnKeyType="next"
                                    value={userData.phone}
                                    onChangeText={(value) => onChangeText('phone', value)}
                                    labelStyle={styles.labelInput}
                                    inputStyle={styles.inputBox}
                                    keyboardType='numeric'
                                >
                                    Nomor Telepon
                                </FloatingLabel>
                            </View>

                            <View style={styles.groupForm}>
                                <FloatingLabel
                                    clearButtonMode='always'
                                    returnKeyType="next"
                                    value={userData.email}
                                    onChangeText={(value) => onChangeText('email', value)}
                                    labelStyle={styles.labelInput}
                                    inputStyle={styles.inputBox}
                                    keyboardType='email-address'
                                >
                                    E-mail
                                </FloatingLabel>
                            </View>

                            <View style={styles.groupForm}>
                                <FloatingLabel
                                    clearButtonMode='always'
                                    returnKeyType="next"
                                    value={userData.password}
                                    onChangeText={(value) => onChangeText('password', value)}
                                    labelStyle={styles.labelInput}
                                    inputStyle={styles.inputBox}
                                    password
                                >
                                    Kata Sandi
                                </FloatingLabel>
                            </View>

                            <View style={styles.groupForm}>
                                <FloatingLabel
                                    clearButtonMode='always'
                                    returnKeyType="next"
                                    value={userData.password2}
                                    onChangeText={(value) => onChangeText('password2', value)}
                                    labelStyle={styles.labelInput}
                                    inputStyle={styles.inputBox}
                                    password
                                >
                                    Ulangi Kata Sandi
                                </FloatingLabel>
                            </View>
                        </View>

                        <TouchableOpacity style={styles.button} onPress={() => onSubmit()}>
                            <Text style={styles.buttonText}>Daftar</Text>
                        </TouchableOpacity>
                    
                        {/* {this.props.screenProps.loading && <Spinner color={Color.theme} />} */}
                    </View>
                </View>
            </ScrollView>

            {/* <ModalBanner
                visible={modalInformation}
                type={type}
                label={messageInformation}
                onClose={() => this.setState({ modalInformation: false })}
            /> */}

            <ModalIndicator
                visible={loadingSubmit}
                type="large"
                indicators='skype'
                message="Harap tunggu, kami sedang memproses permintaan Anda"
            />

            <Modal
                animationType="fade"
                transparent
                visible={loadingSuccess}
            >
                <ModalInformation
                    label={reponseMessage}
                />
            </Modal>

            <Modal
                animationType="fade"
                transparent
                visible={loadingWarning}
            >
                <ModalInformation
                    label={reponseMessage}
                    warning
                />
            </Modal>

            <Modal
                animationType="fade"
                transparent
                visible={loadingError}
            >
                <ModalInformation
                    label={reponseMessage}
                    error
                />
            </Modal>
        </ImageBackground>
    )
}

export default RegisterScreen;

RegisterScreen.navigationOptions = {
    header: null
};