/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {Alert,Platform, StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions, Button,Switch,TouchableWithoutFeedback} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Dialog, {SlideAnimation, DialogContent } from 'react-native-popup-dialog';
import BottomSheet from 'reanimated-bottom-sheet'
import SlidingUpPanel from 'rn-sliding-up-panel';
import ReviewBookingStyle from '../style/ReviewBookingStyle';
import Carousel from 'react-native-banner-carousel';

const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 200;
 
const images = [
     "http://mfebriansyah.com/images/img_hotel1.jpg",
    "http://mfebriansyah.com/images/img_hotel2.jpg"
];


export default class ReviewBookingScreen extends Component<{}> {
  renderPage(image, index) {
      return (
          <View key={index}>
              <Image style={{ width: BannerWidth, height: BannerHeight, resizeMode:'contain' }} source={{ uri: image }} />
          </View>
      );
  }

  constructor(props){
  super(props)
    this.state = {
      isOnDefaultToggleSwitch: false,
      isOnLargeToggleSwitch: false,
      isOnBlueToggleSwitch: false,
      isHidden: false
    };
  }

  onToggle(isOn) {
    console.log("Changed to " + isOn);
  }
  HasilSearchHotel = () => {
    this.props.navigation.navigate('HasilSearchHotel');
  }
  PilihKamar = () => {
    this.props.navigation.navigate('PilihKamar');
  }

  render() {
    return (
      <View style={styles.container}>
      

        <View style={styles.navTop}>
          <View style={styles.rowsBeetwen}>
            <View style={{flexDirection: 'row',alignItems:'center'}}>
              <TouchableOpacity onPress={this.HasilSearchHotel}>
                <Image style={{width:30, height:30,resizeMode:'contain'}} source={require('../images/left-arrow.png')}/>
              </TouchableOpacity>
              <Text style={styles.titlePage}>Booking</Text>
            </View>
          </View>

        </View>
   

        
        <ScrollView>
          <View style={styles.rowBanner}>
              <Carousel
                  autoplay
                  autoplayTimeout={5000}
                  loop
                  index={0}
                  pageSize={BannerWidth}
              >
                  {images.map((image, index) => this.renderPage(image, index))}
              </Carousel>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row',alignSelf:'stretch',flex:1}}>
                    <View style={styles.captionForm}>

                        <View style={{flexDirection:'row',alignSelf:'stretch',flex:1,marginTop:5, alignItems:'center',marginBottom:10}}>
                          <Image style={{width:127, height:20, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_stars.png')}/>
                        </View>
                        <Text style={styles.BigText}>Lorin Sentul</Text>

                        <View style={{flexDirection:'row',alignSelf:'stretch',flex:1,marginTop:5, marginBottom:15, alignItems:'center'}}>
                          <Image style={{width:15, height:21, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_place.png')}/>
                          <Text style={styles.smallText}>Sentul Circuit (Exit Toll KM. 32)</Text>
                        </View>
                        <Text style={styles.smallText}>Checkin : Fri, 27 Sep 2019</Text>
                        <Text style={styles.smallText}>Checkout : Sat, 28 Sep 2019</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <Text style={styles.BigText2}>Tamu</Text>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt2}>
                <Text style={styles.smallText}>1 Dewasa</Text>
                <View style={{flexDirection:'row'}}>
                  <View style={{width:80}}>
                    <Text style={styles.underlineText}>Tuan</Text>
                  </View>
                  <View style={{flex:1}}>
                    <TextInput
                      placeholder="Nama"
                      style={styles.inputText}
                    />
                  </View>
                </View>

                <View>
                  <TextInput
                    placeholder="No Telepon"
                    style={styles.inputText}
                  />
                </View>
              </View>
            </View>
          </View>

        </ScrollView>

        <View style={styles.footer}>
            <View style={styles.boxShadow2}>
                <View style={styles.boxInnerFooter}>
                  <View style={{ flex:1, textAlign:"center" }}>
                    <Text style={styles.smallTextYellow}>Total Perkiraan</Text>
                    <Text style={styles.BigText}>IDR 750.000</Text>
                    <Text style={styles.smallText}>untuk 1 malam</Text>
                  </View>

                  <View style={{textAlign:"center",paddingRight:15 }}>
                    <TouchableOpacity style={styles.bgGreenTextAbs} onPress={this.HotelDetail}>
                      <Text style={{color:"#fff",fontSize:18,fontWeight:'bold',PaddingHorizontal:20}}>ISSUED</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
          </View>
        
      </View>
    )
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  }
}




// STYLES
// ------------------------------------
const styles = StyleSheet.create(ReviewBookingStyle);