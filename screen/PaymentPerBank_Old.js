import React, { Component } from 'react';
import { Image, View, TextInput, ScrollView, BackHandler } from 'react-native';
import Styled from 'styled-components';
import gql from 'graphql-tag';
import Icon from 'react-native-vector-icons/Ionicons';
import Moment from 'moment';

import TouchableOpacity from './Button/TouchableDebounce';
import Button from './Button';
import Text from './Text';
import FormatMoney from './FormatMoney';
import Color from './Color';
import Header from './Header';
import graphClient from '../state/apollo';

const MainView = Styled(View)`
  height: 100%;
  width: 100%;
  backgroundColor: #FAFAFA;
`;

const LeftHeaderView = Styled(View)`
  alignItems: flex-start;
  width: 70%;
`;

const RightHeaderView = Styled(View)`
  alignItems: flex-end;
  width: 30%;
`;

const HeaderView = Styled(View)`
  backgroundColor: ${Color.theme};
  width: 100%;
`;

const SubHeaderView = Styled(View)`
  flexDirection: row;
`;

const Container = Styled(View)`
  padding: 20px 15px 15px 16px;
`;

const BaseText = Styled(Text)`
  color: ${Color.text};
  lineHeight: 18px;
`;

const SmallText = Styled(BaseText)`
  fontSize: 12px;
  textAlign: left;
`;

const SectionView = Styled(TouchableOpacity)`
  minHeight: 1px;
  backgroundColor: #FFFFFF;
  marginBottom: 3px;
`;

const ImageDetail = Styled(Image)`
  width: 92px;
  height: 30px;
`;

const TextFieldOVOPhone = Styled(View)`
  width: 70%;
  flexDirection: row;
  paddingHorizontal: 10;
  borderColor: #999999;
  borderWidth: 0.5;
  height: 45;
`;

const ContainerOvo = Styled(View)`
  width: 100%;
  alignItems: flex-end
`;

const OVOTextInput = Styled(TextInput)`
  fontSize: 12;
  flexBasis: 90%;
  color: #484848;
`;

const ButtonView = Styled(View)`
  width: 100%
  alignItems: center
  justifyContent: center
  flexDirection: row
  backgroundColor: #FFFFFF
  padding: 10px 0px 10px 0px
  minHeight: 1px
`;

const ButtonRadius = Styled(Button)`
  width: 80%;
  height: 50px;
`;

const getAmountVesta = gql`
  query{
    vestaBalance{
      amount
    }
  }
`;

export default class PaymentPerBank extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    const { booking, bookingDetail, bookingType } = props.navigation.state.params;
    let type = bookingType;

    if (booking['prepaidTransaction']) type = bookingDetail.category;
    if (booking['postpaidTransaction']) type = bookingDetail.productCode;

    super(props);
    this.state = {
      activeSections: [],
      textOVOPhone: null,
      vestaAmount: 0,
      selected: null,
      expiresAt: booking.expiresAt,
      bookingDetail,
      type
    };

    this.getVesta();
  }

  startCountdown() {
    const countdown = setInterval(() => {
      this.isBookingExpired(this.state.expiresAt)
    }, 1000);
    
    this.setState({ countdown });
  }

  isBookingExpired = (expiresAt) => {
    if (Moment(expiresAt).diff(Moment(), 'seconds') <= 0) {
      this.setState({ kdVoucher: '', codeVoucherApplied: null, messageVoucherCode: null, expiresAt });
      this.clearCountdown();
      this.props.navigation.popToTop();
      return true;
    }
    this.setState({ expiresAt });
    return false;
  }

  clearCountdown() {
    if (this.state.countdown !== null) clearInterval(this.state.countdown);
  }

  getVesta(){
    graphClient.query({
      query: getAmountVesta
    }).then(res => {
      this.setState({ vestaAmount: res.data.vestaBalance.amount })
    }).catch(reject => { console.log(reject) })
  }

  componentDidMount() {
    this.startCountdown();
    // if (this.props.navigation.state.params === undefined) {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    // }
  }

  componentWillUnmount() {
    // if (this.props.navigation.state.params === undefined) {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    // }
  }

  handleBackPress = () => {
    this.props.navigation.popToTop();
    return true;
  }

  submit(pay){
    this.props.navigation.navigate('PaymentDetail', { payment: pay, ovoPhone: this.state.textOVOPhone })
  }

  renderBank(items) {
    console.log(this.state, 'sate payment per bank');
    console.log(this.props, 'prop payment per bank');

    const { vestaAmount } = this.state;
    const { paymentMethods, booking } = this.props.navigation.state.params;
    let sisaVestaPoint = parseInt(vestaAmount) - parseInt(booking.amount);

    return items.map((pay, id) =>
      <View key={id}>
        {pay.class == 'vestaPoint' ? <View>
          <View style={{backgroundColor: '#ffffff'}}>
            <View>
              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 16, marginTop: 16}}>
                <Text>Vesta Balance</Text>
                <Text type='semibold'>{FormatMoney.getFormattedMoney(vestaAmount)}</Text>
              </View>

              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 16, marginTop: 16}}>
                <Text>Tagihan</Text>
                <Text>{FormatMoney.getFormattedMoney(booking.amount)}</Text>
              </View>

              <View style={{marginVertical: 16, borderBottomWidth: 0.5 }} />

              <View style={{flexDirection: 'row', justifyContent: 'space-between', marginRight: 15, marginLeft: 15,  marginBottom: 15}}>
                <Text>Sisa Vesta Point</Text>
                <Text type='semibold' style={{color: '#FF425E'}}>{FormatMoney.getFormattedMoney(sisaVestaPoint)}</Text>
              </View>
            </View>
          </View>
        </View>
        :
        <SectionView activeOpacity={0.6} onPress={() => this.submit(pay)}>
          <Container>
            <SubHeaderView>
              <LeftHeaderView>
                <ImageDetail resizeMode={'contain'} source={{ uri: pay.logo }} />
              </LeftHeaderView>
              <RightHeaderView>
                <BaseText><Icon name='ios-arrow-forward' /></BaseText>
              </RightHeaderView>
            </SubHeaderView>
          </Container>
        </SectionView>}
        {pay.class === 'ovoPayment' && <ContainerOvo>
          <TextFieldOVOPhone>
            <OVOTextInput
              numberOfLines={1}
              value={this.state.textOVOPhone}
              underlineColorAndroid="transparent"
              autoCorrect={false}
              placeholder='masukkan no telepon OVO / OVO ID'
              onChangeText={(code) => { this.setState({ textOVOPhone: code }, () => console.log(this.state.textOVOPhone)) }}
            />
          </TextFieldOVOPhone>
        </ContainerOvo>}
      </View>
    )
  }

  renderPulsaPrabayar(booking, bookingDetail) {
    return (
      <View style={{marginTop: 8, paddingHorizontal: 16}}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 16}}>
          <Text>Pemesan</Text><Text>{booking.contact.firstName} {booking.contact.lastName}</Text>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 16}}>
          <Text>Nomor Tujuan</Text><Text>{bookingDetail.destinationNumber}</Text>
        </View>
      </View>
    );
  }

  renderSwitch(type, booking, bookingDetail) {
    switch (type) {
      // case 'flights': return this.renderFlight();
      // case 'tours': return this.renderTour();
      // case 'attractions': return this.renderAttraction();
      // case 'buses': return this.renderBuses();
      // case 'hotels': return this.renderHotel();
      // case 'vestabalance': return this.renderTopUp();
      case 'PULSA_HP': return this.renderPulsaPrabayar(booking, bookingDetail);
      // case 'unknown': return this.renderPlnPrabayar();
      default: return <View />
    }
  }

  render() {
    const { paymentMethods, pickedTitle, booking } = this.props.navigation.state.params;
    const { expiresAt, type, bookingDetail } = this.state;

    let durationNumber = Moment(expiresAt).diff(Moment(), 'seconds');
        hours = Moment.duration(durationNumber, 'seconds').hours();
        minutes = Moment.duration(durationNumber, 'seconds').minutes();
        seconds = Moment.duration(durationNumber, 'seconds').seconds();

        console.log(durationNumber);
        

    return (
      <MainView>
        <Header title={pickedTitle} showLeftButton onPressLeftButton={() => this.props.navigation.popToTop()} />
        <ScrollView>
          <HeaderView>
            <Container>
              <BaseText style={{color: '#FFFFFF'}} type='bold' align='left'>ORDER ID : {booking.invoiceNumber}</BaseText>
            </Container>
          </HeaderView>
          
          <HeaderView style={{backgroundColor: '#FFFFFF'}}>
            <Container>
              <SmallText align='left'>Pembayaran dapat dilakukan melalui {pickedTitle}.</SmallText>
            </Container>
          </HeaderView>

          <Text style={{marginBottom: 8}}>Sisa waktu pembayaran</Text>
          <Text>{hours > 0 ? hours + ' Jam : ' : ''}{minutes > 0 ? minutes + ' Menit : ' : ''}{seconds + ' Detik'}</Text>
          
          {this.renderSwitch(type, booking, bookingDetail)}

          {paymentMethods.length > 0 && this.renderBank(paymentMethods)}
        </ScrollView>
        {paymentMethods.map((pay, idx) =>
          pay.class == 'vestaPoint' && <ButtonView key={idx}>
            <ButtonRadius onPress={() => this.submit(pay)}>Bayar Dengan Vesta Point</ButtonRadius>
          </ButtonView>
        )}
      </MainView>
    );
  }
}
