import React, { Component } from "react";
import { View, Platform, ActivityIndicator } from 'react-native';
import { Tabs, Tab, TabHeading } from 'native-base';
import gql from 'graphql-tag';
import Moment from 'moment';
import { connect } from 'react-redux';
import ModalBox from 'react-native-modalbox';
import Styled from 'styled-components';

import Text from '../Text';
import Color from '../Color';
import Header from '../Header';
import TabScreen from './TabScreen';
import graphClient from '../../state/apollo';
import ModalIndicator from '../Modal/ModalIndicator';
import TouchableOpacity from '../Button/TouchableDebounce';
import fetchApi from '../../state/fetchApi';
import BookingDetailVoucherGame from "./BookingDetailVoucherGame";

const CustomModalBox = Styled(ModalBox)`
  justifyContent: center;
  alignItems: center;
  width: 75%;
  backgroundColor: transparent;
`;

const ModalView = Styled(View)`
    width: 100%;
    minHeight 1;
    alignItems: center;
`;

const OptionsContainerModalView = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: #FFFFFF;
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
`;

const OptionModalView = Styled(TouchableOpacity)`
    width: 100%;
    paddingVertical: 10;
    justifyContent: center;
    alignItems: flex-start;
    paddingLeft: 10;
`;

const NormalText = Styled(Text)`
    fontSize: 15;
`;

const cancelMutation = gql`
  mutation(
    $bookingId: Int!
  ) {
   cancelBooking( bookingId: $bookingId ) { bookingStatus { id name } }
  }
`;

const getUserBookingsQuery = gql`
  query(
    $page: Int
    $itemPerPage: Int
    $type: BookingStatusType
  )
  {
    currentUserBookings(
      page: $page
      itemPerPage: $itemPerPage
      type: $type
    ){
      id
      userId
      invoiceNumber
      createdAt
      expiresAt
      amount
      discount
      vat
      finalAmount
      contact {
        title { name abbr }
        firstName
        lastName
        email
        countryCode
        phone
        address
        postalCode
      }
      bookingStatus { id name }
      payments {
        type { id name }
        amount
      }
      flights {
        departure {
          bookingCode
          fareDetail
          total
          journeys {
            origin { code name cityName countryCode }
            destination { code name cityName countryCode }
            airline { name code logoUrl }
            departsAt
            arrivesAt
            flightNumber
            seatClass
          }
        }
        return {
          bookingCode
          fareDetail
          total
          journeys {
            origin { code name cityName countryCode }
            destination { code name cityName countryCode }
            airline { name code logoUrl }
            departsAt
            arrivesAt
            flightNumber
            seatClass
          }
        }
        passengers {
          type
          title { name abbr }
          firstName
          lastName
          birthDate
          nationality
          idNumber
          phone
          passport { number expirationDate issuingCountry }
        }
      }
      tours {
        price { subTotal vat airportTaxAndFuel visa discount total }
        paymentAmount
        priceDetail {
          specs { type price count total }
          dp
          isDp
          isIncludeVisa
        }
        tourSnapshot {
          id
          name
          slug
          departure {
            date
            duration
            airlines { name logoUrl code }
          }
        }
        rooms {
          travellers { type titleAbbr priceType firstName lastName birthDate }
        }
      }
      vestabalance {
        id amount
      }
      attractions {
        id
        paxPrice { adults children seniors }
        timeslottxt
        message
        adults
        children
        seniors
        arrivaldate
        attraction_name
        confirm_date
        status
        discount
        ppn
        final_amount
      }
      buses {
        booking_code
        booking_id
        order_id
        seats {
          busesCode
          busesName
          departureCode
          arrivalCode
          departureName
          arrivalName
          departureDate
          arrivalDate
          seatPosition
          rateCode
          date
          type
          boardingPoint{
            pointId
            pointName
            pointTime
            pointAddress
            pointLandmark
            pointContact
          }
          droppingPoint{
            pointId
            pointName
            pointTime
            pointAddress
            pointLandmark
            pointContact
          }
          passenger{
            title
            fullname
            name
            surname
            birthdate
            address
            city
            country
            postal_code
            phone_number
            age
            email
            id_card
          }
        }
      }
      hotels {
        HotelConfig { name value }
        id
        bookingId
        bookingDate
        checkInDate
        checkOutDate
        hotelName
        hotelBoardName
        hotelCategory
        cancellationAmount
        rateClass
        rateClassName
        hotelImage
        hotelCustomerRemark
        hotelAdult
        hotelChild
        hotelInfant
        hotelRoomCodeName
        hotelAddress
        amount
        markup
        discount
        additionalFee
        transactionFee
        ppn
        finalAmount
      }
      prepaidTransaction {
        id
        orderId
        refId
        status
        code
        destinationNumber
        price
        message
        balance
        tr_id
        rc
        category
        order_status
        request_data
        response_data
        lastUpdated
      }
      postpaidTransaction{
        id
        orderId
        paymentRequestData
        paymentResponseData
        transactionId
        productCode
        customerSubscribeNumber
        customerName
        billPeriod
        nominal
        adminFee
        inquiryResponseCode
        inquiryResponseMessage
        price
        sellingPrice
        refId
        paymentResponseCode
        paymentResponseMessage
        paymentResponseDescription
        lastUpdated
      }
    }
  }`
;

let pureState = {
  requestId: 0,
  page: 1,
  itemPerPage: 15,
  type: 'ALL',
  currentUserBookings: [],
  lengthData: 0,
  loading: true
};

class OrderBooking extends Component {
  constructor(props){
    super(props);

    const { title } = props.navigation.state.params;
    let product = '';
    switch(title) {
      case 'Pesawat': product = 'pesawat'; break;
      case 'Travel': product = 'tour'; break;
      case 'Hotel': product = 'hotel'; break;
      case 'Pulsa': product = 'pulsa'; break;
      case 'Atraksi': product = 'attraction'; break;
      case 'Bus': product = 'bus'; break;
      case 'PlnPrabayar': product = 'prepaid/TOKEN_LISTRIK'; break;
      case 'PlnPascabayar': product = 'postpaid/pln'; break;
      case 'PdamPascabayar': product = 'postpaid/pdam'; break;
      case 'TeleponPascabayar': product = 'postpaid/telepon'; break;
      case 'PaketData': product = 'prepaid/PAKET_DATA'; break;
      case 'VoucherGame': product = 'prepaid/online_game'; break;
    }

    this.state = {
      activeTab: 0,
      modal: false,
      selectedBooking: null,
      tabs: [
        { title: 'AKTIF', children: <View /> },
        { title: 'SELESAI', children: <View /> },
        { title: 'DIBATALKAN', children: <View /> },
      ],
      activeBookings: {...pureState, type: 'ACTIVE'},
      succeedBookings: {...pureState, type: 'PAID'},
      canceledBookings: {...pureState, type: 'CANCEL'},
      heightModalBox: 0,

      activeBookingSiago: [],
      loadingActiveBooking: true,
      succeedBookingSiago: [],
      loadingSucceedBooking: true,
      canceledBookingSiago: [],
      loadingCanceledBooking: true,
      product
    };
  }

  async componentDidMount() {
    const { product } = this.state;
    await this.getHistoryBooking(product, '/draft');
    await this.getHistoryBooking(product, '/paid');
    await this.getHistoryBooking(product, '/canceled');
  }

  getHistoryBooking(product, type) {
    fetchApi.get('history/'+product+type)
    .then((res) => {
      if (type === '/draft') this.setState({ activeBookingSiago: res.data.data, loadingActiveBooking: false });
      else if (type === '/paid') this.setState({ succeedBookingSiago: res.data.data, loadingSucceedBooking: false });
      else if (type === '/canceled') this.setState({ canceledBookingSiago: res.data.data, loadingCanceledBooking: false });

      this.refresh();
    })
    .catch((err) => {
      console.log(err, 'err history booking');

      if (type === '/draft') this.setState({ activeBookingSiago: [], loadingActiveBooking: false });
      else if (type === '/paid') this.setState({ succeedBookingSiago: [], loadingSucceedBooking: false });
      else if (type === '/canceled') this.setState({ canceledBookingSiago: [], loadingCanceledBooking: false });

      this.refresh();
    });
  }

  getUserBookings(param) {
    const { currentUserBookings, requestId, page, itemPerPage, type } = this.state[param];
    const reqId = requestId + 1;
    const variables = {
      page,
      itemPerPage,
      type
    };

    let newData = {
      ...this.state[param],
      requestId: reqId
    };

    this.setState({ [param]: newData }, () => {
      graphClient
      .query({
        query: getUserBookingsQuery,
        variables
      })
      .then(res => {
        let response = res.data.currentUserBookings;
        let tempArray = currentUserBookings;

        if (response.length > 0 && reqId == this.state[param].requestId) {
          tempArray = currentUserBookings.concat(response);
        }

        newData = {
          ...this.state[param],
          lengthData: response.length,
          currentUserBookings: tempArray,
          loading: false
        }

        this.setState({ [param]: newData }, () => this.refresh());
      })
      .catch((reject) => {
        this.setState({ [param]: this.state[param] }, () => this.refresh());
      });
    });
  }

  pullRefresh(product) {
    this.getHistoryBooking(product, '/draft');
    this.getHistoryBooking(product, '/paid');
    this.getHistoryBooking(product, '/canceled');
  }

  refresh() {
    const { activeBookingSiago, succeedBookingSiago, canceledBookingSiago, tabs, product } = this.state;
    const { title } = this.props.navigation.state.params;

    let newActiveBookings = [];
    let newCanceledBookings = canceledBookingSiago;

    if (activeBookingSiago.length > 0) {
      for(const resp of activeBookingSiago) {
        if (Moment(resp.expiresAt).diff(Moment(), 'seconds') > 0) newActiveBookings.push(resp);
        else newCanceledBookings.push(resp);
      }
    }

    const onRefresh = () => this.pullRefresh(product);
    const onExpired = () => this.refresh();

    this.setState({
      activeBookingSiago: newActiveBookings,
      canceledBookingSiago: newCanceledBookings,
      tabs: [
        {
          title: 'AKTIF',
          children: <TabScreen type={title} bookings={newActiveBookings} status={1} onRefresh={onRefresh} onExpired={onExpired} openModal={(booking) => this.openModal(1, booking)} onSelectOrder={(selectedBooking) => this.setState({ selectedBooking }, () => this.detailBooking())} />
        },
        {
          title: 'SELESAI',
          children: <TabScreen type={title} bookings={succeedBookingSiago} status={2} onRefresh={onRefresh} onExpired={onExpired} openModal={(booking) => this.openModal(2, booking)} onSelectOrder={(selectedBooking) => this.setState({ selectedBooking }, () => this.detailBooking())} />
        },
        {
          title: 'DIBATALKAN',
          children: <TabScreen type={title} bookings={newCanceledBookings} status={3} onRefresh={onRefresh} onExpired={onExpired} openModal={(booking) => this.openModal(3, booking)} onSelectOrder={(selectedBooking) => this.setState({ selectedBooking }, () => this.detailBooking())} />
        },
      ]
    });
  }

  payBooking() {
    const booking = this.state.selectedBooking;
    this.closeModal();
    this.props.addBooking(booking);
    this.props.navigation.navigate('PaymentScreen', 'order_screen');
  }

  cancelBooking() {
    const bookingId = this.state.selectedBooking.id;
    this.closeModal();
    
    graphClient
    .mutate({
      mutation: cancelMutation,
      fetchPolicy: 'no-cache',
      variables: { bookingId },
    })
    .then(() => {
      this.pullRefresh(this.state.product);
    })
    .catch(() => {
      this.pullRefresh(this.state.product);
    });
  }

  detailBooking() {
    this.closeModal();

    const { title } = this.props.navigation.state.params;
    let navigateScreen = null;

    switch (title) {
      case 'Pesawat': navigateScreen = 'BookingDetailFlight'; break;
      case 'Travel': navigateScreen = 'BookingDetailTravel'; break;
      case 'Hotel': navigateScreen = 'BookingDetailHotel'; break;
      case 'Pulsa': navigateScreen = 'BookingDetailPulsa'; break;
      case 'Atraksi': navigateScreen = 'BookingDetailAttraction'; break;
      case 'Bus': navigateScreen = 'BookingDetailBus'; break;
      case 'PlnPrabayar': navigateScreen = 'BookingDetailPlnPrabayar'; break;
      case 'PlnPascabayar': navigateScreen = 'BookingDetailPlnPascabayar'; break;
      case 'PdamPascabayar': navigateScreen = 'BookingDetailPdamPascabayar'; break;
      case 'TeleponPascabayar': navigateScreen = 'BookingDetailTeleponPascabayar'; break;
      case 'PaketData': navigateScreen = 'BookingDetailPaketData'; break;
      case 'VoucherGame': navigateScreen = 'BookingDetailVoucherGame'; break;
    }

    if (navigateScreen) this.props.navigation.navigate(navigateScreen, { booking: this.state.selectedBooking });
  }

  openModal(status, selectedBooking) {
    this.setState({ modal: status, selectedBooking });
  }

  closeModal() {
    this.setState({ modal: 0, selectedBooking: null });
  }

  renderMainTabs() {
    return (
      <Tabs
        prerenderingSiblingsNumber={Infinity}
        tabStyle={{backgroundColor: '#F4F4F4'}}
        tabContainerStyle={{elevation: 0, borderBottomWidth: 1, borderBottomColor: '#DDDDDD'}}
        tabBarUnderlineStyle={{backgroundColor: '#FFFFFF', height: 3}}
        onChangeTab={({ i }) => this.setState({ activeTab: i })}
        locked={Platform.OS === 'android'}
      >
        {this.renderAllTabs()}
      </Tabs>
    );
  }

  renderAllTabs() {
    const { activeTab, tabs } = this.state;
    return tabs.map((tab, i) =>
      <Tab
        key={i}
        heading={this.renderTabHeading(tab, i, activeTab)}
        tabStyle={{ backgroundColor: '#F4F4F4', elevation: 0 }}
        activeTabStyle={{ backgroundColor: '#F4F4F4' }}
      >
        {i === activeTab && tab.children}
      </Tab>
    );
  }

  renderTabHeading(tab, i, activeTab) {
    return (
      <TabHeading style={{ backgroundColor: Color.theme }}>
        <Text type={activeTab === i && 'bold'} style={activeTab === i ? {color: '#FFFFFF'} : {color: '#DDDDDD'}}>{tab.title}</Text>
      </TabHeading>
    );
  }

  renderLoading() {
    const { activeBookings, succeedBookings, canceledBookings } = this.state;
    if ((activeBookings.currentUserBookings.length > 4 && activeBookings.loading) || (succeedBookings.currentUserBookings.length > 4 && succeedBookings.loading) || (canceledBookings.currentUserBookings.length > 4 && canceledBookings.loading)) {
      return <ActivityIndicator color={Color.text} style={{paddingBottom: 4}} />
    }
  }

  renderModal() {
    const { modal } = this.state;
    return (
      <ModalView onLayout={(event) => this.setState({ heightModalBox: event.nativeEvent.layout.height })}>
        {modal === 1 && <OptionsContainerModalView>
          <OptionModalView onPress={() => this.payBooking()}>
            <NormalText>Lanjutkan Pembayaran</NormalText>
          </OptionModalView>
        </OptionsContainerModalView>}
        <OptionsContainerModalView>
          <OptionModalView onPress={() => this.detailBooking()}>
            <NormalText>Detail Pesanan</NormalText>
          </OptionModalView>
        </OptionsContainerModalView>
        {modal === 1 && <OptionsContainerModalView>
          <OptionModalView onPress={() => this.cancelBooking()}>
            <NormalText>Batalkan Pesanan</NormalText>
          </OptionModalView>
        </OptionsContainerModalView>}
        <OptionsContainerModalView>
          <OptionModalView onPress={() => this.closeModal()}>
            <NormalText>Cancel</NormalText>
          </OptionModalView>
        </OptionsContainerModalView>
      </ModalView>
    );
  }
  
  render() {
    const { loadingActiveBooking, loadingSucceedBooking, loadingCanceledBooking, modal } = this.state;

    let title = this.props.navigation.state.params.title;
    switch(title) {
      case 'Pesawat': title = 'Laporan Booking Pesawat'; break;
      case 'Travel': title = 'Laporan Booking Travel'; break;
      case 'Hotel': title = 'Laporan Booking Hotel'; break;
      case 'Pulsa': title = 'Laporan Order Pulsa'; break;
      case 'Atraksi': title = 'Laporan Booking Atraksi'; break;
      case 'Bus': title = 'Laporan Booking Bus'; break;
      case 'PlnPrabayar': title = 'Laporan Order PLN Prabayar'; break;
      case 'PlnPascabayar': title = 'Laporan Order PLN Pascabayar'; break;
      case 'PdamPascabayar': title = 'Laporan Order PDAM'; break;
      case 'TeleponPascabayar': title = 'Laporan Order Telepon'; break;
      case 'PaketData': title = 'Laporan Order Paket Data'; break;
      case 'VoucherGame': title = 'Laporan Order Voucher Game'; break;
    }

    return (
      <View style={{backgroundColor: '#F5F6F7', flex: 1}}>
        <Header title={title} />
        {this.renderMainTabs()}

        <CustomModalBox
          isOpen={modal == 1 || modal == 2 || modal == 3}
          onClosed={() => this.closeModal()}
          coverScreen
          backdropOpacity={0.5}
          style={{height: this.state.heightModalBox + 1}}
        >
          {this.renderModal()}
        </CustomModalBox>

        <ModalIndicator
          type='large'
          indicators='skype'
          message='Memuat Riwayat Booking'
          visible={loadingActiveBooking && loadingSucceedBooking && loadingCanceledBooking}
        />
        {this.renderLoading()}
      </View>
    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}

const mapStateToProps = (state) => {
  return {
    user_siago: state['user.auth'].user_siago,
  }
}

const mapDispatchToProps = (dispatch, props) => ({
  addBooking: (data) => dispatch({ type: 'BOOKING.ADD_BOOKING', data }),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderBooking);