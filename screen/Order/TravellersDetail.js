import React, { Component } from 'react';
import { View, Text as ReactText } from 'react-native';
import Moment from 'moment';
import Styled from 'styled-components';

import Text from '../Text';
import Color from '../Color';
import Countries from '../LocalData/Countries';
import Titles from '../LocalData/Titles';

const MainView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    flexDirection: column;
    backgroundColor: #FFFFFF;
`;

const HeaderContainer = Styled(View)`
    width: 100%;
    height: 50;
    alignItems: flex-start;
    justifyContent: center;
    paddingHorizontal: 16;
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
    backgroundColor: #FFFFFF;
`;

const TravellersView = Styled(View)`
    width: 100%;
    minHeight: 1;
    paddingLeft: 16;
    paddingRight: 12;
    borderBottomWidth: 8;
    borderColor: #DDDDDD;
`;

const OneTravellerView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    paddingVertical: 10;
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
`;

const TravellerNumberView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    paddingTop: 2;
`;

const TravellerInfoView = Styled(View)`
    flex: 1;
    flexDirection: column;
`;

const TravellerHeaderInfoView = Styled(View)`
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    marginBottom: 6;
`;

const TravellerLeftHeaderInfoView = Styled(View)`
    flex: 1;
    alignItems: flex-start;
    paddingTop: 2;
`;

const TravellerRightHeaderInfoView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
`;

const TravellerSubInfoView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    paddingVertical: 2;
`;

const TravellerHalfSubInfoView = Styled(View)`
    flex: 1;
    alignItems: flex-start;
`;

const NormalText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const TitleText = Styled(Text)`
    letterSpacing:1
`;

const TimeCustomText = Styled(ReactText)`
  fontSize: 12;
  backgroundColor: ${Color.theme};
  borderRadius: 8;
  paddingHorizontal: 4;
  paddingVertical: 2;
  color: #FFFFFF;
`;

export default class TravellersDetail extends Component {

  static navigationOptions = { header: null };

  getCountry(countryCode) {
    for (const country of Countries) if (country.isoCode2 === countryCode) return country.name;
  }

  getCategoryAndLongTitle(abbr) {
    const travellerInfo = {};
    for (const title of Titles) if (title.abbr === abbr) {
      travellerInfo.longName = ` ${title.longName}`;
      if (title.isAdult) travellerInfo.category = 'Dewasa';
      else travellerInfo.category = 'Anak';
    }
    return travellerInfo;
  }

  renderAllTravellers(travellers) {
    return travellers.map((traveller, i) => {
      const { longName, category } = this.getCategoryAndLongTitle(traveller.titleAbbr);
      return (
        <OneTravellerView key={i}>
          <TravellerNumberView><NormalText type='medium'>{i + 1}.</NormalText></TravellerNumberView>
          <TravellerInfoView>
            <TravellerHeaderInfoView>
              <TravellerLeftHeaderInfoView><NormalText type='medium'>{longName} {traveller.firstName} {traveller.lastName}</NormalText></TravellerLeftHeaderInfoView>
              {category && <TravellerRightHeaderInfoView><TimeCustomText>{category}</TimeCustomText></TravellerRightHeaderInfoView>}
            </TravellerHeaderInfoView>
            <TravellerSubInfoView>
              <TravellerHalfSubInfoView><NormalText type='medium'>Tanggal Lahir</NormalText></TravellerHalfSubInfoView>
              <TravellerHalfSubInfoView><NormalText type='medium'>: {Moment(traveller.birthDate).format('DD MMMM YYYY')}</NormalText></TravellerHalfSubInfoView>
            </TravellerSubInfoView>
            {traveller.nationality && <TravellerSubInfoView>
              <TravellerHalfSubInfoView><NormalText type='medium'>Kewarganegaraan</NormalText></TravellerHalfSubInfoView>
              <TravellerHalfSubInfoView><NormalText type='medium'>: {this.getCountry(traveller.nationality)}</NormalText></TravellerHalfSubInfoView>
            </TravellerSubInfoView>}
          </TravellerInfoView>
        </OneTravellerView>
      );
    });
  }

  render() {
    return (
      <MainView>
        <HeaderContainer>
          <TitleText type='bold'>{this.props.title}</TitleText>
        </HeaderContainer>
        <TravellersView>
          {this.renderAllTravellers(this.props.travellers)}
        </TravellersView>
      </MainView>
    );
  }

}
