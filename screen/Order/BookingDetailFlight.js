import React, { Component } from 'react';
import { Image, View, ScrollView, Text as ReactText, SafeAreaView, ActivityIndicator } from 'react-native';
import Moment from 'moment';
import Styled from 'styled-components';
import Accordion from 'react-native-collapsible/Accordion';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Text from '../Text';
import Header from '../Header';
import Color from '../Color';
import TravellersDetail from './TravellersDetail';
import PriceDetail from './PriceDetail';
import BookingInfo from './BookingInfo';
import Client from '../../state/apollo';
import gql from "graphql-tag";

const queryBookingDetail = gql`
query(
    $bookingId: Int!
  ) {
    bookingDetail(
        bookingId: $bookingId
    ){
        id
        userId
        invoiceNumber
        contact{
            title{
                name
                abbr
            }
            firstName
            lastName
            email
            countryCode
            phone
            address
            postalCode
        }
        bookingStatus{
            id
            name
        }
        amount
        discount
        vat
        finalAmount
        payments{
            type {
                id
                name
            }
            amount
            discount
            transactionFee
            finalAmount
            paidAt
            transferAccountNumber
        }
        expiresAt
        flights{
            departure{
                bookingCode
                fareDetail
                total
                journeys{
                    origin{
                        code
                        name
                        cityName
                        countryCode
                    }
                    destination{
                        code
                        name
                        cityName
                        countryCode
                    }
                    airline{
                        name
                        code
                        logoUrl
                    }
                    departsAt
                    arrivesAt
                    flightNumber
                    seatClass
                }
            }
            return{
                bookingCode
                fareDetail
                total
                journeys{
                    origin{
                        code
                        name
                        cityName
                        countryCode
                    }
                    destination{
                        code
                        name
                        cityName
                        countryCode
                    }
                    airline{
                        name
                        code
                        logoUrl
                    }
                    departsAt
                    arrivesAt
                    flightNumber
                    seatClass
                }
            }
            passengers{
                type
                title{
                    name
                    abbr
                }
                firstName
                lastName
                birthDate
                nationality
                idNumber
                phone
                passport{
                    number
                    expirationDate
                    issuingCountry
                }

            }
        }
    }
  }`
;

const MainView = Styled(View)`
    width: 100%;
    alignItems: center;
    backgroundColor: #FFFFFF;
`;

const HeaderContainer = Styled(View)`
    width: 100%;
    height: 50;
    alignItems: center;
    flexDirection: row;
    paddingHorizontal: 16;
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
    backgroundColor: #FFFFFF;
`;

const HeaderSectionLeft = Styled(View)`
    width: 90%;
    height: 100%;
    alignItems: flex-start;
    justifyContent: center;
`;

const HeaderSectionRight = Styled(View)`
    width: 10%;
    height: 100%;
    justifyContent: center;
    alignItems: flex-end;
`;

const AccordionSectionContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
`;

const FlightDetailView = Styled(View)`
    width: 100%;
    minHeight: 100;
    borderBottomWidth: 8;
    borderColor: #DDDDDD;
    flexDirection: column;
    paddingVertical: 10;
    paddingHorizontal: 10;
`;

const OneFlightContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
`;

const LineView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    paddingLeft: 8;
`;

const TimeView = Styled(View)`
    width: 50;
    minHeight: 1;
    alignItems: center;
`;

const CodeNameAirportView = Styled(View)`
    flex: 1;
    alignItems: center;
    justifyContent: flex-start;
    flexDirection: row;
    paddingRight: 10;
`;

const CircleView = Styled(View)`
    width: 15;
    height: 22;
    flexDirection: column;
    marginRight: 8;
`;

const DottedView = Styled(View)`
    width: 8;
    height: 50;
    alignItems: flex-end;
    justifyContent: center;
`;

const DottedImageView = Styled(View)`
    width: 1;
    height: 100%;
`;

const GreyTransitContainer = Styled(View)`
    flex: 1;
    justifyContent: center;
    paddingLeft: 2;
`;

const GreyTransitView = Styled(View)`
    height: 25;
    alignItems: flex-start;
    justifyContent: center;
    backgroundColor: #F4F4F4;
    paddingLeft: 14;
`;

const DepartureTopCircleView = Styled(View)`
    width: 100%;
    height: 57.5%;
    alignItems: center;
    justifyContent: flex-end;
`;

const DepartureBottomCircleView = Styled(DepartureTopCircleView)`
    height: 42.5%;
    justifyContent: flex-start;
`;

const DestinationTopCircleView = Styled(DepartureBottomCircleView)`
    justifyContent: flex-end;
`;

const DestinationBottomCircleView = Styled(DepartureTopCircleView)`
    justifyContent: flex-start;
`;

const FullVerticalLine = Styled(CircleView)`
    height: 100%;
    alignItems: center;
`;

const VerticalLineView = Styled(DepartureTopCircleView)`
    width: 0.5;
    height: 100%;
    backgroundColor: #000000;
`;

const MainAirlineInfo = Styled(View)`
    flex: 1;
    height: 100%;
    alignItems: flex-start;
    justifyContent: center;
    flexDirection: column;
`;

const AirlineInfo = Styled(View)`
    width: 100%;
    minHeight: 1;
    alignItems: center;
    justifyContent: flex-start;
    flexDirection: row;
`;

const BaggageIcon = Styled(View)`
    width: 13;
    height: 10;
`;

const AirlineLogo = Styled(View)`
    width: 40;
    height: 20;
    alignItems: flex-start;
    justifyContent: center;
    margin: 4px 10px 2px 0px;
`;

const MainDurationView = Styled(LineView)`
    height: 60;
`;

const DurationView = Styled(View)`
    width: 50;
    height: 100%;
    justifyContent: center;
    alignItems: center;
`;

const BlackCircle = Styled(View)`
    width: 5;
    height: 5;
    borderRadius: 5;
    backgroundColor: #000000;
`;

const WhiteCircle = Styled(BlackCircle)`
    backgroundColor: #FFFFFF;
    borderWidth: 1;
    borderColor: #000000;
`;

const AccordionTitle = Styled(Text)`
    letterSpacing:1
`;

const FlightNumberText = Styled(Text)`
    fontSize: 11;
    textAlign: left;
`;

const TransitText = Styled(Text)`
    fontSize: 11;
    color: #A8A699;
    textAlign: left;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const IconArrow = Styled(Ionicons)`
    color: #333333;
`;

const TimeCustomText = Styled(ReactText)`
  fontSize: 12;
  backgroundColor: ${Color.theme};
  borderRadius: 8;
  paddingHorizontal: 4;
  paddingVertical: 2;
  color: #FFFFFF;
`;

const AirportCustomText = Styled(ReactText)`
  fontSize: 15;
  backgroundColor: #DDDDDD;
  borderRadius: 5;
  paddingHorizontal: 3;
  paddingBottom: 1;
  color: #FFFFFF;
  fontWeight: bold;
`;

const baggage = require('../../images/baggage.png');
const dotted = require('../../images/dotted-black.png');
const flying = require('../../images/icon_flying.png');
const depart = require('../../images/icon_depart.png');

export default class BookingDetailFlight extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);

    let booking = null;
    if (props.isComponent) booking = props.booking;

    const activeSection = [];
    if (booking) for (const flight of booking.flights) activeSection.push({ departure: 0, return: 0 });

    this.state = {
      booking,
      activeSection
    };
  }

  componentDidMount() {
    if (!this.props.isComponent) this.getBookingDetail(this.props.navigation.state.params.booking.id);
  }

  getBookingDetail(bookingId) {
    let activeSection = [];

    Client
    .query({
        query: queryBookingDetail,
        variables: { bookingId }
    })
    .then(res => {
        if(res.data.bookingDetail) {
          for (const flight of res.data.bookingDetail.flights) activeSection.push({ departure: 0, return: 0 });
          this.setState({ booking: res.data.bookingDetail, activeSection });
        }
    }).catch(error => {
        console.log(error, 'error detail booking');
        
    });
  }

  getAllTravellers(passengers) {
    const travellers = [];
    for (const passenger of passengers) {
      const { firstName, lastName, birthDate, title } = passenger;
      travellers.push({ firstName, lastName, birthDate, titleAbbr: title.abbr });
    }
    return travellers;
  }

  onChangeActiveAccordion(i, index, type) {
    const activeSection = this.state.activeSection;
    activeSection[i] = { ...activeSection[i], [type]: index };
    this.setState({ activeSection });
  }

  renderFlights(i, segments) {
    const segment = segments[i];
    const departsAt = Moment(segment.departsAt);
    const arrivesAt = Moment(segment.arrivesAt);
    let duration = arrivesAt.diff(departsAt, 'seconds');
    const durationHours = Moment.duration(duration, 'seconds').hours();
    const durationMinutes = Moment.duration(duration, 'seconds').minutes();

    return (
      <OneFlightContainer>
        <LineView>
          <CircleView>
            {false && <DepartureTopCircleView>{i === 0 ? <BlackCircle /> : <WhiteCircle />}</DepartureTopCircleView>}
            <Image style={{width: 17, height: 17}} resizeMode='contain' source={flying} />
            <DepartureBottomCircleView><VerticalLineView /></DepartureBottomCircleView>
          </CircleView>
          <CodeNameAirportView>
            <AirportCustomText>{segment.origin.code}</AirportCustomText>
            <Text> {segment.origin.name}</Text>
          </CodeNameAirportView>
          <TimeView>
            <ReactText>{Moment.parseZone(departsAt).format('HH:mm')}</ReactText>
            <ReactText style={{fontSize: 12}}>{Moment.parseZone(departsAt).format('DD MMM')}</ReactText>
          </TimeView>
        </LineView>
        <MainDurationView>
          <FullVerticalLine><VerticalLineView /></FullVerticalLine>
          <MainAirlineInfo>
            <AirlineInfo>
              <AirlineLogo><ImageProperty resizeMode='contain' source={{ uri: segment.airline.logoUrl }} /></AirlineLogo>
              <FlightNumberText type='bold'>{segment.airline.code} {segment.flightNumber}</FlightNumberText>
            </AirlineInfo>
            {false && segment.classes.length > 0 && <AirlineInfo>
              <Text>{segment.classes[i].classType === 'E' ? 'Economy' : 'Business'} - </Text>
              <BaggageIcon><ImageProperty resizeMode='stretch' source={baggage} /></BaggageIcon>
              <Text> {segment.classes[i].baggage}</Text>
            </AirlineInfo>}
          </MainAirlineInfo>
          <DurationView>
            <TimeCustomText>{durationHours > 0 && durationHours + 'j '}{durationMinutes + 'm'}</TimeCustomText>
          </DurationView>
        </MainDurationView>
        <LineView>
          <CircleView style={{top: -6}}>
            <DestinationTopCircleView><VerticalLineView /></DestinationTopCircleView>
            {false && <DestinationBottomCircleView>{i === segments.length - 1 ? <BlackCircle /> : <WhiteCircle />}</DestinationBottomCircleView>}
            <Image style={{width: 17, height: 17}} resizeMode='contain' source={depart} />
          </CircleView>
          <CodeNameAirportView>
            <AirportCustomText>{segment.destination.code}</AirportCustomText>
            <Text> {segment.destination.name}</Text>
          </CodeNameAirportView>
          <TimeView>
            <ReactText>{Moment.parseZone(arrivesAt).format('HH:mm')}</ReactText>
            <ReactText style={{fontSize: 12}}>{Moment.parseZone(arrivesAt).format('DD MMM')}</ReactText>
          </TimeView>
        </LineView>
      </OneFlightContainer>
    );
  }

  renderTransit(i, segments) {
    const segment = segments[i];
    const nextSegment = segments[i + 1];
    let transitDuration = Moment(nextSegment.departsAt).diff(Moment(segment.arrivesAt), 'seconds');
    const durationHours = Moment.duration(transitDuration, 'seconds').hours();
    const durationMinutes = Moment.duration(transitDuration, 'seconds').minutes();

    return (
      <LineView>
        <DottedView>
          <DottedImageView>
            <ImageProperty resizeMode='stretch' source={dotted} />
          </DottedImageView>
        </DottedView>
        <GreyTransitContainer>
          <GreyTransitView>
            <TransitText type='medium'>Transit di {nextSegment.origin.cityName}</TransitText>
          </GreyTransitView>
        </GreyTransitContainer>
        <TimeView style={{backgroundColor: '#F4F4F4', height: 25, justifyContent: 'center'}}>
          <Text>{durationHours > 0 && durationHours + 'j '} {durationMinutes + 'm'}</Text>
        </TimeView>
      </LineView>
    );
  }

  renderAllDetail(flight) {
    return flight.journeys.map((segment, i) =>
      <View key={i}>
        {this.renderFlights(i, flight.journeys)}
        {i < flight.journeys.length - 1 && this.renderTransit(i, flight.journeys)}
      </View>
    );
  }

  renderAccordionHeader = (method, index, isActive) => {
    return (
      <HeaderContainer>
        <HeaderSectionLeft><AccordionTitle type='bold'>{method.name}</AccordionTitle></HeaderSectionLeft>
        <HeaderSectionRight><IconArrow name={isActive ? 'ios-arrow-down' : 'ios-arrow-forward'} /></HeaderSectionRight>
      </HeaderContainer>
    );
  }

  renderAccordionSection = (flight) => {
    return (
      <AccordionSectionContainer>
        <FlightDetailView>
          <Text type='bold' style={{marginBottom: 8}}>{flight.journeys.length > 1 ? `Transit ${flight.journeys.length - 1} kali` : 'Direct'}</Text>
          {this.renderAllDetail(flight)}
        </FlightDetailView>
      </AccordionSectionContainer>
    );
  }

  renderAccordion(flight, type, i) {
    return (
      <Accordion
        touchableProps={{ activeOpacity: 1 }}
        sections={[{ name: `${type.toUpperCase()} FLIGHT` }]}
        renderHeader={this.renderAccordionHeader}
        renderContent={() => this.renderAccordionSection(flight)}
        activeSection={this.state.activeSection[i][type]}
        onChange={(index) => this.onChangeActiveAccordion(i, index, type)}
      />
    );
  }

  renderAllDetailFlights() {
    return this.state.booking.flights.map((flight, i) =>
      <View key={i}>
        <BookingInfo
          accordion
          title='INFORMATION BOOKING'
          booking={this.state.booking}
        />
        
        {this.renderAccordion(flight.departure, 'departure', i)}
        {flight.return && this.renderAccordion(flight.return, 'return', i)}

        <TravellersDetail
          title='PENUMPANG'
          travellers={this.getAllTravellers(flight.passengers)}
        />

        <PriceDetail
          title='RINCIAN HARGA'
          booking={this.state.booking}
        />
      </View>
    );
  }

  render() {
    const { isComponent } = this.props;
    // console.log(this.state, 'state booking fligth');
    // console.log(this.props, 'props booking flight');
    
    return (
      <MainView style={!isComponent && {height: '100%'}}>
        {!isComponent && <Header title='Detail Penerbangan' showLeftButton />}
        <ScrollView contentContainerStyle={{ maxWidth: '100%' }}>
          {this.state.booking ? this.renderAllDetailFlights() : <ActivityIndicator />}

          {/* {!isComponent && <View style={{height: 45, paddingHorizontal: 16, marginBottom: 16}}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('PaymentScreen', 'order_screen')}
              style={{backgroundColor: Color.text, height: '100%', justifyContent: 'center', borderRadius: 20}}
            >
              <Text style={{color: '#FFFFFF'}}>Lanjutkan Pembayaran</Text>
            </TouchableOpacity>
          </View>} */}
        </ScrollView>
      </MainView>
    );
  }
}
