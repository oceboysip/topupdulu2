import React, { Component } from 'react';
import { Image, View, ScrollView } from 'react-native';
import Moment from 'moment';
import Styled from 'styled-components';
import Accordion from 'react-native-collapsible/Accordion';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Text from '../Text';
import Header from '../Header';
import TravellersDetail from './TravellersDetail';
import PriceDetail from './PriceDetail';
import BookingInfo from './BookingInfo';
import Client from '../../state/apollo';
import gql from "graphql-tag";

const MainView = Styled(View)`
    width: 100%;
    alignItems: center;
    backgroundColor: #FFFFFF;
`;

const HeaderContainer = Styled(View)`
    width: 100%;
    height: 50;
    alignItems: center;
    flexDirection: row;
    paddingHorizontal: 16;
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
    backgroundColor: #FFFFFF;
`;

const HeaderSectionLeft = Styled(View)`
    width: 90%;
    height: 100%;
    alignItems: flex-start;
    justifyContent: center;
`;

const HeaderSectionRight = Styled(View)`
    width: 10%;
    height: 100%;
    justifyContent: center;
    alignItems: flex-end;
`;

const MainInfoView = Styled(View)`
    width: 100%;
    minHeight: 1;
    padding: 10px 16px 10px 16px;
    borderBottomWidth: 8;
    borderColor: #DDDDDD;
`;

const AirlineView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    marginVertical: 6;
`;

const AirlineImageView = Styled(View)`
    width: 20;
    height: 20;
    marginRight: 6;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const AccordionTitle = Styled(Text)`
    letterSpacing:1
`;

const IconArrow = Styled(Ionicons)`
    color: #333333;
`;

const NormalText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const TitleInfoText = Styled(Text)`
    textAlign: left;
    marginBottom: 6;
`;

const DetailInfoText = Styled(NormalText)`
    textAlign: left;
    marginBottom: 6;
`;

const queryBookingDetail = gql`
  query(
    $bookingId: Int!
  ) {
    bookingDetail(
      bookingId: $bookingId
    ){
      id
      userId
      invoiceNumber
      createdAt
      expiresAt
      amount
      discount
      vat
      finalAmount
      contact {
        title { name abbr }
        firstName
        lastName
        email
        countryCode
        phone
        address
        postalCode
      }
      bookingStatus { id name }
      payments {
        type { id name }
        amount
      }
      tours {
        price { subTotal vat airportTaxAndFuel visa discount total }
        paymentAmount
        priceDetail {
          specs { type price count total }
          dp
          isDp
          isIncludeVisa
        }
        tourSnapshot {
          id
          name
          slug
          departure {
            date
            duration
            airlines { name logoUrl code }
          }
        }
        rooms {
          travellers { type titleAbbr priceType firstName lastName birthDate }
        }
      }
    }
  }`
;

export default class BookingDetailTravel extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    let booking = null;
    let activeSection = [];
    
    if (props.isComponent) {
      booking = props.booking;
    }

    this.state = {
      booking,
      activeSection
    };
  }

  componentDidMount() {
    if (!this.props.isComponent) this.getBookingDetail(this.props.navigation.state.params.booking.id);
  }

  getBookingDetail(bookingId) {
    const variables = { bookingId };
    
    Client
    .query({
        query: queryBookingDetail,
        variables
    })
    .then(res => {
      console.log(res, 'res detail travel');
      let activeSection = [];
      if(res.data.bookingDetail) {
        for (const tour of res.data.bookingDetail.tours) activeSection.push(0);
        this.setState({ booking: res.data.bookingDetail, activeSection });
      }
    }).catch(error => {
        console.log(error, 'error detail booking');
    });
  }

  onChangeActiveAccordion(i, index) {
    const activeSection = this.state.activeSection;
    activeSection[i] = index;
    this.setState({ activeSection });
  }

  getAllTravellers(rooms) {
    let travellers = [];
    for (const room of rooms) travellers = travellers.concat(room.travellers);
    return travellers;
  }

  renderAirlines(airlines) {
    return airlines.map((airline, i) =>
      <AirlineView key={i}>
        <AirlineImageView><ImageProperty resizeMode='contain' source={{ uri: airline.logoUrl }} /></AirlineImageView>
        <DetailInfoText type='medium'>{airline.name}</DetailInfoText>
      </AirlineView>
    );
  }

  renderAccordionSection = (tour) => {
    return (
      <MainInfoView>
        <TitleInfoText type='bold'>{tour.tourSnapshot.departure.duration}D {tour.tourSnapshot.name}</TitleInfoText>
        <DetailInfoText type='medium'>Tanggal berangkat : {Moment(tour.tourSnapshot.departure.date).format('DD MMM YYYY')} - {Moment(tour.tourSnapshot.departure.date).add(tour.tourSnapshot.departure.duration - 1, 'days').format('DD MMM YYYY')} </DetailInfoText>
        <DetailInfoText type='medium'>Durasi : {tour.tourSnapshot.departure.duration} Hari</DetailInfoText>
        {this.renderAirlines(tour.tourSnapshot.departure.airlines)}
      </MainInfoView>
    );
  }

  renderAccordionHeader = (method, index, isActive) => {
    return (
      <HeaderContainer>
        <HeaderSectionLeft><AccordionTitle type='bold'>{method.name}</AccordionTitle></HeaderSectionLeft>
        <HeaderSectionRight><IconArrow name={isActive ? 'ios-arrow-down' : 'ios-arrow-forward'} /></HeaderSectionRight>
      </HeaderContainer>
    );
  }

  renderAccordion(tour, i) {
    return (
      <Accordion
        touchableProps={{ activeOpacity: 1 }}
        sections={[{ name: 'RINCIAN TRAVEL' }]}
        renderHeader={this.renderAccordionHeader}
        renderContent={() => this.renderAccordionSection(tour)}
        activeSection={this.state.activeSection[i]}
        onChange={(index) => this.onChangeActiveAccordion(i, index)}
      />
    );
  }

  renderAllDetailTours(booking) {
    return booking.tours.map((tour, i) =>
      <View key={i}>
        <BookingInfo
          accordion
          title='INFORMATION BOOKING'
          booking={booking}
        />
        {this.renderAccordion(tour, i)}
        <TravellersDetail title='PESERTA' travellers={this.getAllTravellers(tour.rooms)} />
        <PriceDetail title='RINCIAN HARGA' booking={booking} />
      </View>
    );
  }

  render() {
    const { booking } = this.state;
    const { isComponent } = this.props;

    return (
      <MainView style={!isComponent && {height: '100%'}}>
        {!isComponent && <Header title='Detail TRAVEL' />}
        <ScrollView contentContainerStyle={{ maxWidth: '100%' }}>
          {booking && this.renderAllDetailTours(booking)}
        </ScrollView>
      </MainView>
    );
  }

}
