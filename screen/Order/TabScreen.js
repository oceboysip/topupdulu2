import React, { Component } from 'react';
import { Image, View, FlatList } from 'react-native';
import Styled from 'styled-components';
import Text from '../Text';
import CardBooking from './CardBooking';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    flexDirection: column;
    backgroundColor: #FAFAFA;
`;

const ContentView = Styled(View)`
    width: 100%;
    height: 100%;
    justifyContent: center;
    alignItems: center;
`;

const ImageProperty = Styled(Image)`
    width: 100;
    height: 150;
    marginBottom: 20;
`;

const NoBookingText = Styled(Text)`
`;

// const noBookingImage = require('../../../assets/images/daftar-kosong.png');

export default class TabScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
    };
  }

  keyExtractor = (booking, index) => `${booking.id} - ${index}`;

  renderAllCards(booking) {
    return <CardBooking booking={booking} type={this.props.type} status={this.props.status} openModal={(selectedBooking) => this.props.openModal(selectedBooking)} onExpired={() => this.props.onExpired()} onSelectOrder={(selectedBooking) => this.props.onSelectOrder(selectedBooking)} />;
  }

  renderNoOrder() {
    return (
      <ContentView>
        {false && <ImageProperty resizeMode='contain' source={noBookingImage} />}
        <NoBookingText type='bold'>Riwayat Pemesanan Anda Kosong</NoBookingText>
        <NoBookingText>Anda tidak memiliki riwayat pemesanan,</NoBookingText>
        <NoBookingText>Silahkan melakukan transaksi</NoBookingText>
      </ContentView>
    );
  }

  renderResult(currentData) {
    return (
      <FlatList
        contentContainerStyle={{padding: 12, width: '100%', minHeight: '100%'}}
        keyExtractor={this.keyExtractor}
        data={currentData}
        onRefresh={this.props.onRefresh}
        refreshing={false}
        ListEmptyComponent={() => this.renderNoOrder()}
        renderItem={({ item }) => this.renderAllCards(item)}
        onEndReached={this.props.getMoreResult}
        onEndReachedThreshold={0.1}
      />
    );
  }

  getCurrentData(type) {
    let newBooking = [];
    // let prepaidCategory = '';
    // let postpaidCategory = '';
    
    this.props.bookings.map((item, idx) => {
      
      // if (item.prepaidTransaction) prepaidCategory = item.prepaidTransaction.category;
      // if (item.postpaidTransaction) if (item.postpaidTransaction.productCode) postpaidCategory = item.postpaidTransaction.productCode;
    
      // if (type === 'Pesawat' && item.flights.length > 0) newBooking.push(item);
      // if (type === 'Travel' && item.tours.length > 0) newBooking.push(item);
      // if (type === 'Hotel' && item.hotels) newBooking.push(item);
      // if (type === 'Bus' && item.buses) newBooking.push(item);
      // if (type === 'Atraksi' && item.attractions) newBooking.push(item);
      // if (type === 'Pulsa' && item.prepaidTransaction && prepaidCategory === 'PULSA_HP') newBooking.push(item);
      // if (type === 'PlnPrabayar' && item.prepaidTransaction && prepaidCategory === 'TOKEN_LISTRIK') newBooking.push(item);

      // if (type === 'PlnPascabayar' && item.postpaidTransaction && postpaidCategory === 'PLNPOSTPAID') newBooking.push(item);
      // if (type === 'PdamPascabayar' && item.postpaidTransaction && (postpaidCategory.split('.')[0] === 'PDAMKOTA' || postpaidCategory.split('.')[0] === 'PDAMKAB' || postpaidCategory === 'AETRA_JAKARTA' || postpaidCategory === 'PALYJA_JAKARTA')) newBooking.push(item);
    })
    // console.log(type, prepaidCategory, postpaidCategory && postpaidCategory.split('.')[0], 'hideung', this.props.bookings);

    return newBooking;
  }

  render() {
    const currentData = this.getCurrentData(this.props.type);
    // console.log(currentData, this.props, 'vurrdata');
    
    return (
      <MainView>
        {this.renderResult(this.props.bookings)}
      </MainView>
    );
  }
}
