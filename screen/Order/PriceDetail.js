import React, { Component } from 'react';
import { View } from 'react-native';
import Styled from 'styled-components';
import Accordion from 'react-native-collapsible/Accordion';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Text from '../Text';
import FormatMoney from '../FormatMoney';

const MainView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    flexDirection: column;
    backgroundColor: #FFFFFF;
`;

const HeaderContainer = Styled(View)`
    width: 100%;
    height: 50;
    alignItems: center;
    flexDirection: row;
    paddingHorizontal: 16;
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
    backgroundColor: #FFFFFF;
`;

const HeaderSectionLeft = Styled(View)`
    width: 90%;
    height: 100%;
    alignItems: flex-start;
    justifyContent: center;
`;

const HeaderSectionRight = Styled(View)`
    width: 10%;
    height: 100%;
    justifyContent: center;
    alignItems: flex-end;
`;

const OneDetailPriceView = Styled(View)`
    width: 100%;
    height: 40;
    paddingHorizontal: 16;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
`;

const TotalPriceView = Styled(OneDetailPriceView)`
    borderTopWidth: 0.5;
    borderColor: #DDDDDD;
    marginBottom: 10;
`;

const HalfPriceView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
`;

const NormalText = Styled(Text)`
    fontSize: 14;
    textAlign: left;
`;

const PriceText = Styled(Text)`
    color: #FF425E;
`;

const TitleText = Styled(Text)`
    letterSpacing:1
`;

const IconArrow = Styled(Ionicons)`
    color: #333333;
`;

export default class PriceDetail extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    this.state = {
      activeSection: [0]
    };
  }

  onChangeActiveAccordion(index) {
    const activeSection = this.state.activeSection;
    activeSection[0] = index;
    this.setState({ activeSection });
  }

  renderDetailPrice() {
    let paymentMethod = '-';
    if (this.props.booking.payments.length > 0) {
      paymentMethod = this.props.booking.payments[0].type.name === 'Vesta Point' ? 'Siago Pay' : this.props.booking.payments[0].type.name;
    }

    const detailsPrice = [
      { label: 'Metode Pembayaran', value: paymentMethod },
      { label: 'Sub Total', value: FormatMoney.getFormattedMoney(this.props.booking.amount) },
      { label: 'Diskon', value: FormatMoney.getFormattedMoney(-Math.abs(this.props.booking.discount)) },
      { label: 'Ppn', value: FormatMoney.getFormattedMoney(this.props.booking.vat) },
    ];
    return detailsPrice.map((detailPrice, i) =>
      <OneDetailPriceView key={i}>
        <HalfPriceView><NormalText type='medium'>{detailPrice.label}</NormalText></HalfPriceView>
        <HalfPriceView><NormalText type='medium'>{detailPrice.value}</NormalText></HalfPriceView>
      </OneDetailPriceView>
    );
  }

  renderContentPriceDetail = () => (
    <MainView>
      {this.renderDetailPrice()}
      <TotalPriceView>
        <HalfPriceView><NormalText type='bold'>Total</NormalText></HalfPriceView>
        <HalfPriceView><PriceText type='bold'>{FormatMoney.getFormattedMoney(this.props.booking.finalAmount)}</PriceText></HalfPriceView>
      </TotalPriceView>
    </MainView>
  )

  renderHeader = (method, index, isActive) => (
    <HeaderContainer>
      <HeaderSectionLeft><TitleText type='bold'>{this.props.title}</TitleText></HeaderSectionLeft>
      {this.props.accordion && <HeaderSectionRight><IconArrow name={isActive ? 'ios-arrow-down' : 'ios-arrow-forward'} /></HeaderSectionRight>}
    </HeaderContainer>
  )

  render() {
    console.log(this.props);
    
    if (this.props.accordion) return (
      <Accordion
        touchableProps={{ activeOpacity: 1 }}
        sections={[{ name: this.props.title }]}
        renderHeader={this.renderHeader}
        renderContent={() => this.renderContentPriceDetail()}
        activeSection={this.state.activeSection[0]}
        onChange={(index) => this.onChangeActiveAccordion(index)}
      />
    );
    return (
      <MainView>
        {this.renderHeader()}
        {this.renderContentPriceDetail()}
      </MainView>
    );
  }
}
