import React, { Component } from 'react';
import { View, ScrollView, Platform, Text as ReactText } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';
import Accordion from 'react-native-collapsible/Accordion';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Text from '../Text';
import Color from '../Color';
import Header from '../Header';
import PriceDetail from './PriceDetail';
import BookingInfo from './BookingInfo';
import Titles from '../LocalData/Titles';
import TouchableOpacity from '../Button/TouchableDebounce';
// import QRCode from 'react-native-qrcode-svg';
import ModalBox from 'react-native-modalbox';
import { Row, Col } from '../Grid';
import Client from '../../state/apollo';
import gql from "graphql-tag";

const MainView = Styled(View)`
    width: 100%;
    alignItems: center;
    backgroundColor: #FFFFFF;
`;

const HeaderContainer = Styled(View)`
    width: 100%;
    height: 50;
    alignItems: flex-start;
    justifyContent: center;
    paddingHorizontal: 16;
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
    backgroundColor: #FFFFFF;
`;

const HeaderSectionLeft = Styled(View)`
    width: 90%;
    height: 100%;
    alignItems: flex-start;
    justifyContent: center;
`;

const HeaderSectionRight = Styled(View)`
    width: 10%;
    height: 100%;
    justifyContent: center;
    alignItems: flex-end;
`;

const AccordionTitle = Styled(Text)`
    letterSpacing:1
`;

const IconArrow = Styled(Ionicons)`
    color: #333333;
`;

const AccordionSectionContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    borderBottomWidth: 8;
    borderBottomColor: #DDDDDD;
`;

const BusTripDetail = Styled(View)`
  width: 100%;
`;

const TravellersView = Styled(View)`
    width: 100%;
    minHeight: 1;
    paddingHorizontal: 16;
    borderBottomWidth: 8;
    borderColor: #DDDDDD;
`;

const TitleText = Styled(Text)`
    letterSpacing:1
`;

const OneTravellerView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    paddingVertical: 10;
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
`;

const TravellerNumberView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    paddingTop: 2;
`;

const NormalText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const SmallerNormalText = Styled(NormalText)`
    fontSize: 10;
`;

const GreyText = Styled(Text)`
  fontSize: 12;
  marginTop: 2;
  color: #A8A699;
`;

const SmallerGreyText = Styled(Text)`
  fontSize: 8;
  color: #A8A699;
`;

const WhiteText = Styled(Text)`
  color: #FFFFFF;
`;

const TravellerInfoView = Styled(View)`
    flex: 1;
    flexDirection: column;
`;

const TravellerHeaderInfoView = Styled(View)`
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    marginBottom: 8;
`;

const TravellerLeftHeaderInfoView = Styled(View)`
    flex: 1;
    alignItems: flex-start;
    paddingTop: 2;
`;

const TravellerRightHeaderInfoView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
`;

const TravellerSubInfoView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
`;

const TravellerHalfSubInfoView = Styled(View)`
    flex: 1;
    alignItems: flex-start;
`;

const CustomModalBox = Styled(ModalBox)`
    width: 100%;
    height: 440;
    backgroundColor: transparent;
    alignItems: center;
`;

const QRCodeView = Styled(View)`
  width: 215;
  height: 215;
  alignItems: center;
  justifyContent: center;
  backgroundColor: #FFFFFF;
  borderRadius: 3;
`;

const InfoView = Styled(View)`
  width: 100%;
  flexDirection: row;
  paddingHorizontal: 16;
  paddingTop: 16;
  marginBottom: 16;
`;

const DepartureView = Styled(View)`
  width: 100%;
  flexDirection: column;
  paddingHorizontal: 16;
  paddingBottom: 16;
`;

const ReturnView = Styled(DepartureView)`
`;

const InfoSeatView = Styled(View)`
  borderWidth: 0.5;
  borderColor: #000000;
  width: 65;
  aspectRatio: 1;
  justifyContent: flex-start;
`;

const BottomView = Styled(View)`
  width: 100%;
  backgroundColor: #FFFFFF;
  alignItems: center;
  justifyContent: center;
  flexDirection: row;
`;

const MainBookingView = Styled(View)`
  width: 100%;
  height: 63;
  flexDirection: row;
  justifyContent: center;
  alignItems: center;
  backgroundColor: #FFFFFF;
  padding: 8px 16px 8px 16px;
  elevation: 20;
`;

const HalfBookingView = Styled(View)`
  width: 60%;
  height: 100%;
  flexDirection: column;
  alignItems: flex-start;
  justifyContent: center;
`;

const ButtonRadius = Styled(TouchableOpacity)`
  borderRadius: 30px;
  width: 100%;
  backgroundColor: #231F20;
  height: 100%;
  justifyContent: center;
`;

const SeatText = Styled(ReactText)`
  fontSize: 28;
`;

const queryBookingDetail = gql`
  query(
    $bookingId: Int!
  ) {
    bookingDetail(
      bookingId: $bookingId
    ){
      id
      userId
      invoiceNumber
      createdAt
      expiresAt
      amount
      discount
      vat
      finalAmount
      contact {
        title { name abbr }
        firstName
        lastName
        email
        countryCode
        phone
        address
        postalCode
      }
      bookingStatus { id name }
      payments {
        type { id name }
        amount
      }
      buses {
        booking_code
        booking_id
        order_id
        seats {
          busesCode
          busesName
          departureCode
          arrivalCode
          departureName
          arrivalName
          departureDate
          arrivalDate
          seatPosition
          rateCode
          date
          type
          boardingPoint{
            pointId
            pointName
            pointTime
            pointAddress
            pointLandmark
            pointContact
          }
          droppingPoint{
            pointId
            pointName
            pointTime
            pointAddress
            pointLandmark
            pointContact
          }
          passenger{
            title
            fullname
            name
            surname
            birthdate
            address
            city
            country
            postal_code
            phone_number
            age
            email
            id_card
          }
        }
      }
    }
  }`
;

// const logoVesta = require('../../../../assets/images/vesta-logo.jpg');

class BookingDetailBus extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);

    let booking = null;
    if (props.isComponent) booking = props.booking;

    this.state = {
      booking,
      modalQRCode: false,
      activeSection: [{
        departure: 0,
        return: 0
      }]
    }
  }

  componentDidMount() {
    if (!this.props.isComponent) this.getBookingDetail(this.props.navigation.state.params.booking.id);
  }

  getBookingDetail(bookingId) {
    const variables = { bookingId };
    
    Client
    .query({
        query: queryBookingDetail,
        variables
    })
    .then(res => {
      console.log(res, 'res detail bus');
      if(res.data.bookingDetail) {
        this.setState({ booking: res.data.bookingDetail });
      }
    }).catch(error => {
        console.log(error, 'error detail booking');
    });
  }

  openModal = (modal, type) => {
    this.setState({ [modal]: true });
  }

  closeModal = (modal) => {
    this.setState({ [modal]: false });
  }

  getCategoryAndLongTitle(abbr) {
    const travellerInfo = {};
    for (const title of Titles) if (title.abbr === abbr) {
      travellerInfo.longName = ` ${title.longName}`;
      if (title.isAdult) travellerInfo.category = 'Dewasa';
      else travellerInfo.category = 'Anak';
    }
    return travellerInfo;
  }

  locale(string) {
    let day = '';
    switch (string) {
      case 'Sun':
        return day = 'Minggu';
      case 'Mon':
        return day = 'Senin';
      case 'Tue':
        return day = 'Selasa';
      case 'Wed':
        return day = 'Rabu';
      case 'Thu':
        return day = 'Kamis';
      case 'Fri':
        return day = 'Jumat';
      case 'Sat':
        return day = 'Sabtu';
      default:
        return day;
    }
  }

  renderAccordionHeader = (method, index, isActive) => {
    return (
      <HeaderContainer style={{flexDirection: 'row'}}>
        <HeaderSectionLeft><AccordionTitle type='bold'>{method.name}</AccordionTitle></HeaderSectionLeft>
        <HeaderSectionRight><IconArrow name={isActive ? 'ios-arrow-down' : 'ios-arrow-forward'} /></HeaderSectionRight>
      </HeaderContainer>
    );
  }

  renderAccordionSection = (buses, diffDay) => (
    <AccordionSectionContainer>
      <InfoView>
        <Row>
          <Col size={10} style={{alignItems: 'flex-start'}}>
            <NormalText type='bold'>{buses.busesName}</NormalText>
            <NormalText type='medium' style={{marginTop: 2}}>{buses.departureName} - {buses.arrivalName}</NormalText>
          </Col>
          <Col size={2} style={{alignItems: 'flex-end'}}>
            <NormalText type='medium'>{buses.departureDate && Moment(buses.departureDate).format('hh:mm')}</NormalText>
            <GreyText type='medium'>{buses.arrivalDate && Moment(buses.arrivalDate).format('hh:mm')}</GreyText>
            {diffDay && <SmallerGreyText type='medium'>{diffDay > 0 && `(+${diffDay} hari)`}</SmallerGreyText>}
          </Col>
        </Row>
      </InfoView>
      <DepartureView>
        <NormalText type='bold' style={{marginBottom: 2}}>Titik Berangkat</NormalText>
        {buses.departureDate && <NormalText type='medium'>{this.locale(Moment(buses.departureDate).format('ddd'))}, {Moment(buses.departureDate).format('DD-MM-YYYY')}</NormalText>}
        <NormalText type='medium'>{buses.boardingPoint.pointName !== "" ? buses.boardingPoint.pointName : buses.departureName}</NormalText>
        {buses.droppingPoint.pointId !== "" && <View>
          {false && <NormalText type='medium'>{buses.boardingPoint.pointLandmark}</NormalText>}
          <NormalText type='medium'>{buses.boardingPoint.pointAddress}</NormalText>
        </View>}
      </DepartureView>
      <ReturnView>
        <NormalText type='bold' style={{marginBottom: 2}}>Titik Tiba</NormalText>
        {buses.arrivalDate && <NormalText type='medium'>{this.locale(Moment(buses.arrivalDate).format('ddd'))}, {Moment(buses.arrivalDate).format('DD-MM-YYYY')}</NormalText>}
        <NormalText type='medium'>{buses.droppingPoint.pointName !== "" ? buses.droppingPoint.pointName : buses.arrivalName}</NormalText>
        {buses.droppingPoint.pointId !== "" && <View>
          {false && <NormalText type='medium'>{buses.droppingPoint.pointLandmark}</NormalText>}
          <NormalText type='medium'>{buses.droppingPoint.pointAddress}</NormalText>
        </View>}
      </ReturnView>
    </AccordionSectionContainer>
  )

  onChangeActiveAccordion(i, index, type) {
    const activeSection = this.state.activeSection;
    activeSection[i] = { ...activeSection[i], [type]: index };
    this.setState({ activeSection });
  }

  renderAccordion(buses, type, i) {
    console.log(buses, type, i);
    const depTime = buses.departureDate ? Moment(buses.departureDate) : null;
    const arrTime = buses.arrivalDateDate ? Moment(buses.arrivalDate) : null;
    let diffDay = null;

    if (depTime && arrTime) diffDay = arrTime.diff(depTime, 'days');

    return (
      <Accordion
        touchableProps={{activeOpacity: 1}}
        sections={[{name: `${type.toUpperCase()} BUS`}]}
        renderHeader={this.renderAccordionHeader}
        renderContent={() => this.renderAccordionSection(buses, diffDay)}
        activeSection={this.state.activeSection[i][type]}
        onChange={(index) => this.onChangeActiveAccordion(i, index, type)}
      />
    )
  }

  renderBusesDetail(buses) {
    let orderSeats = {
      departure: [],
      return: []
    };

    for (const seat of buses.seats) {
      if (seat.type === 'TRIP_AWAY') orderSeats.departure.push(seat);
      else if (seat.type === 'RETURN_TRIP') orderSeats.return.push(seat);
    }

    console.log(orderSeats, 'orderseats', buses);

    return (
      <View>
        {orderSeats.departure.length > 0 && this.renderAccordion(orderSeats.departure[0], 'departure', 0)}
        {orderSeats.departure.length > 0 && <HeaderContainer><TitleText type='bold'>PENUMPANG</TitleText></HeaderContainer>}
        {orderSeats.departure.length > 0 && <TravellersView>{this.renderTravellers(orderSeats.departure)}</TravellersView>}

        {orderSeats.return.length > 0 && this.renderAccordion(orderSeats.return[0], 'return', 0)}
        {orderSeats.return.length > 0 && <HeaderContainer><TitleText type='bold'>PENUMPANG</TitleText></HeaderContainer>}
        {orderSeats.return.length > 0 && <TravellersView>{this.renderTravellers(orderSeats.return)}</TravellersView>}
      </View>
    )
  }

  renderTravellersDetail(booking) {
    return (
      <View>
        <PriceDetail title='RINCIAN HARGA' booking={booking} />
      </View>
    )
  }

  renderTravellers(buses) {
    const { booking } = this.state;

    return buses.map((seat, idx) => {
      const { longName } = this.getCategoryAndLongTitle(seat.passenger.title);
      return (
        <OneTravellerView key={idx}>
          <TravellerNumberView>
            <NormalText type='medium'>{idx + 1}.</NormalText>
          </TravellerNumberView>
          <TravellerInfoView>
            <TravellerHeaderInfoView>
              <TravellerLeftHeaderInfoView><NormalText type='medium'>{longName} {seat.passenger.fullname}</NormalText></TravellerLeftHeaderInfoView>
            </TravellerHeaderInfoView>
            <TravellerSubInfoView>
              <TravellerHalfSubInfoView><NormalText type='medium'>{booking.buses.booking_id}</NormalText></TravellerHalfSubInfoView>
            </TravellerSubInfoView>
            {false && <TravellerSubInfoView>
              <TravellerHalfSubInfoView><NormalText type='medium'>Kategori</NormalText></TravellerHalfSubInfoView>
              <TravellerHalfSubInfoView><NormalText type='medium'>: {seat.passenger.age}</NormalText></TravellerHalfSubInfoView>
            </TravellerSubInfoView>}
          </TravellerInfoView>
          <InfoSeatView>
            <View style={{width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{fontSize: 9, backgroundColor: Color.theme, color: Color.white, width: '100%'}}>Nomor Kursi</Text>
              <SeatText>{seat.seatPosition}</SeatText>
            </View>
          </InfoSeatView>
        </OneTravellerView>
      )
    })
  }

  render() {
    const { booking } = this.state;
    const { isComponent } = this.props;
    // const data = `BUS~${booking.buses.booking_id}~${booking.id}`;

    console.log(this.props, 'props BO detail');
    console.log(this.state, 'state BO detail');

    return (
      <MainView style={!isComponent && {height: '100%'}}>
        {!isComponent && <Header title='Detail Bus' />}
        <ScrollView contentContainerStyle={{maxWidth: '100%'}}>
          {booking && <BookingInfo
            accordion
            title='INFORMATION BOOKING'
            booking={booking}
          />}
          {booking && this.renderBusesDetail(booking.buses)}
          {booking && this.renderTravellersDetail(booking)}
        </ScrollView>

        {/*booking.bookingStatus.id === 4 && <BottomView>
          <MainBookingView>
            <HalfBookingView>
              <ButtonRadius onPress={() => this.openModal('modalQRCode')}>
                <WhiteText>Tampilkan QR Code</WhiteText>
              </ButtonRadius>
            </HalfBookingView>
          </MainBookingView>
        </BottomView>*/}

        {/*<CustomModalBox
          coverScreen
          position='bottom'
          isOpen={this.state.modalQRCode}
          onClosed={() => this.closeModal('modalQRCode')}
        >
          <QRCodeView>
            <QRCode value={data} logo={logoVesta} logoSize={45} size={200} />
          </QRCodeView>
        </CustomModalBox>*/}
      </MainView>
    );
  }
}

export default BookingDetailBus;
