import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import Styled from 'styled-components';

import Header from '../Header';
import PriceDetail from './PriceDetail';
import Client from '../../state/apollo';
import gql from "graphql-tag";

import DetailOrderPostpaid from './DetailOrderPostpaid';

const MainView = Styled(View)`
    width: 100%;
    alignItems: center;
    backgroundColor: #FFFFFF;
`;

const queryBookingDetail = gql`
  query(
    $bookingId: Int!
  ) {
    bookingDetail(
      bookingId: $bookingId
    ){
      id
      userId
      invoiceNumber
      createdAt
      expiresAt
      amount
      discount
      vat
      finalAmount
      contact {
        title { name abbr }
        firstName
        lastName
        email
        countryCode
        phone
        address
        postalCode
      }
      bookingStatus { id name }
      payments {
        type { id name }
        amount
      }
      postpaidTransaction{
        id
        orderId
        paymentRequestData
        paymentResponseData
        transactionId
        productCode
        customerSubscribeNumber
        customerName
        billPeriod
        nominal
        adminFee
        inquiryResponseCode
        inquiryResponseMessage
        price
        sellingPrice
        refId
        paymentResponseCode
        paymentResponseMessage
        paymentResponseDescription
        lastUpdated
      }
    }
  }`
;

export default class BookingDetailPdamPascabayar extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);

    let booking = null;
    if (props.isComponent) booking = props.booking;

    this.state = {
      booking
    }
  }

  componentDidMount() {
    if (!this.props.isComponent) this.getBookingDetail(this.props.navigation.state.params.booking.id);
  }

  getBookingDetail(bookingId) {
    const variables = { bookingId };
    
    Client
    .query({
        query: queryBookingDetail,
        variables
    })
    .then(res => {
      console.log(res, 'res detail pdam pascabayar');
      if(res.data.bookingDetail) {
        this.setState({ booking: res.data.bookingDetail });
      }
    }).catch(error => {
        console.log(error, 'error detail pdam pascabayar');
    });
  }

  render() {
    const { booking } = this.state;
    const { isComponent } = this.props;
    return (
      <MainView style={!isComponent && {height: '100%'}}>
        {!isComponent && <Header title='Detail Order Pdam Pascabayar' />}
        <ScrollView contentContainerStyle={{ maxWidth: '100%' }}>
          {booking && <DetailOrderPostpaid booking={booking} />}
          {booking && <PriceDetail title='RINCIAN HARGA' booking={booking} />}
        </ScrollView>
      </MainView>
    );
  }
}
