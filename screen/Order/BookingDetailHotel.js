import React, { Component } from 'react';
import { Image, View, ScrollView } from 'react-native';
import Moment from 'moment';
import Styled from 'styled-components';
import Accordion from 'react-native-collapsible/Accordion';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Text from '../Text';
import Header from '../Header';
import TravellersDetail from './TravellersDetail';
import PriceDetail from './PriceDetail';
import Client from '../../state/apollo';
import gql from "graphql-tag";

const MainView = Styled(View)`
    width: 100%;
    backgroundColor: #FFFFFF;
`;

const HeaderContainer = Styled(View)`
    width: 100%;
    height: 50;
    alignItems: center;
    flexDirection: row;
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
    backgroundColor: #FFFFFF;
`;
const HeaderContainer2 = Styled(View)`
    width: 100%;
    paddingHorizontal: 16;
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
    backgroundColor: #FFFFFF;
`;

const HeaderSectionLeft = Styled(View)`
    width: 90%;
    height: 100%;
    alignItems: flex-start;
    justifyContent: center;
    paddingHorizontal: 16;
`;

const HeaderSectionRight = Styled(View)`
    width: 10%;
    height: 100%;
    justifyContent: center;
    alignItems: flex-end;
`;

const MainInfoView = Styled(View)`
    width: 100%;
    minHeight: 1;
    padding: 10px 0px 10px 0px;
    borderBottomWidth: 8;
    borderColor: #DDDDDD;
`;

const AirlineView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    marginVertical: 6;
`;

const AirlineImageView = Styled(View)`
    width: 20;
    height: 20;
    marginRight: 6;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const AccordionTitle = Styled(Text)`
    letterSpacing:1
`;

const IconArrow = Styled(Ionicons)`
    color: #333333;
`;

const NormalText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const TitleInfoText = Styled(Text)`
    textAlign: left;
    marginBottom: 6;
`;

const DetailInfoText = Styled(NormalText)`
    textAlign: left;
    marginBottom: 6;
`;

const ContentView = Styled(View)`
    width: 100%;
    minHeight: 1;
    paddingHorizontal: 16;
    borderBottomWidth: 8;
    borderColor: #DDDDDD;
`;

const ListDetailView = Styled(View)`
    width: 100%;
    height: 40;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
`;

const queryBookingDetail = gql`
  query(
    $bookingId: Int!
  ) {
    bookingDetail(
      bookingId: $bookingId
    ){
      id
      userId
      invoiceNumber
      createdAt
      expiresAt
      amount
      discount
      vat
      finalAmount
      contact {
        title { name abbr }
        firstName
        lastName
        email
        countryCode
        phone
        address
        postalCode
      }
      bookingStatus { id name }
      payments {
        type { id name }
        amount
      }
      hotels {
        HotelConfig { name value }
        id
        bookingId
        bookingDate
        checkInDate
        checkOutDate
        hotelName
        hotelBoardName
        hotelCategory
        cancellationAmount
        rateClass
        rateClassName
        hotelImage
        hotelCustomerRemark
        hotelAdult
        hotelChild
        hotelInfant
        hotelRoomCodeName
        hotelAddress
        amount
        markup
        discount
        additionalFee
        transactionFee
        ppn
        finalAmount
      }
    }
  }`
;

export default class HotelOrderDetail extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);

    let booking = null;
    if (props.isComponent) booking = props.booking;

    const activeSection = [];
    // for (const hotel of this.props.navigation.state.params.booking.hotels) activeSection.push(0);

    this.state = {
      booking,
      activeSection
    };
  }

  componentDidMount() {
    if (!this.props.isComponent) this.getBookingDetail(this.props.navigation.state.params.booking.id);
  }

  getBookingDetail(bookingId) {
    const variables = { bookingId };
    
    Client
    .query({
        query: queryBookingDetail,
        variables
    })
    .then(res => {
      console.log(res, 'res detail travel');
      if(res.data.bookingDetail) {
        this.setState({ booking: res.data.bookingDetail });
      }
    }).catch(error => {
        console.log(error, 'error detail booking');
    });
  }

  onChangeActiveAccordion(i, index) {
    const activeSection = this.state.activeSection;
    activeSection[i] = index;
    this.setState({ activeSection });
  }

  getAllTravellers(c) {
    let con = [];
    for (const contact of c) con = con.concat(contact);
    return con;
  }

//   getGuestInfo() {
//       return (
//         <Text>{this.state.booking.contact.title + '. ' + this.state.booking.contact.firstName + ' ' + this.state.booking.contact.lastName}</Text>
//       );
//   }

  renderAccordionSection = (hotel) => {
    //   const night = Moment(hotel.checkOutDate).diff(Moment(hotel.checkInDate), 'days');
    return (
      <MainInfoView>
        <TitleInfoText type='bold'>{hotel.hotelName}</TitleInfoText>
        <DetailInfoText type='medium'>
        {Moment(hotel.checkInDate).format('DD MMM ')} - {Moment(hotel.checkOutDate).format('DD MMM YYYY')}, {Moment(hotel.checkOutDate).diff(Moment(hotel.checkInDate), 'days')} Malam, {hotel.hotelAdult} Tamu
        </DetailInfoText>
        <DetailInfoText type='medium'>
        {hotel.hotelRoomCodeName} - {hotels.hotelBoardName}
        </DetailInfoText>
      </MainInfoView>
    );
  }

  renderAccordionHeader = (method, index, isActive) => {
    return (
      <HeaderContainer>
        <HeaderSectionLeft><AccordionTitle type='bold'>{method.name}</AccordionTitle></HeaderSectionLeft>
        <HeaderSectionRight><IconArrow name={isActive ? 'ios-arrow-down' : 'ios-arrow-forward'} /></HeaderSectionRight>
      </HeaderContainer>
    );
  }

  renderAccordion(hotel, i) {
    return (
      <Accordion
        touchableProps={{ activeOpacity: 1 }}
        sections={[{ name: 'RINCIAN HOTEL' }]}
        renderHeader={this.renderAccordionHeader}
        renderContent={() => this.renderAccordionSection(hotel)}
        activeSection={this.state.activeSection[i]}
        onChange={(index) => this.onChangeActiveAccordion(i, index)}
      />
    );
  }

  renderAllDetailHotel() {
    return this.state.booking.hotels.map(( hotel, i ) => 
      <View>
        {this.renderAccordion(hotel, i)}
        <TravellersDetail title='TAMU' travellers={this.getAllTravellers(this.state.booking.contact)}>
        </TravellersDetail>
        <PriceDetail title='RINCIAN HARGA' booking={this.state.booking} />
      </View>
    )
  }

  renderHotel(booking) {
    const { hotels } = booking;
    return (
      <ContentView>
          <View style={{width: '100%', alignItems: 'center', justifyContent: 'space-between', paddingVertical: 16}}>
            <Text type='bold' style={{marginBottom: 8}}>{hotels.hotelName}</Text>
            <Text>{hotels.hotelRoomCodeName} - {hotels.hotelBoardName}</Text>
          </View>
          {/* <ListDetailView>
            <Text>ID Booking</Text><Text>{booking.id}</Text>
          </ListDetailView> */}
          <ListDetailView>
            <Text>Nomor Invoice</Text><Text>{booking.invoiceNumber}</Text>
          </ListDetailView>
          <ListDetailView>
            <Text>Check in/Check out</Text><Text>{Moment(hotels.checkInDate).format('DD MMM')} - {Moment(hotels.checkOutDate).format('DD MMM YYYY')}</Text>
          </ListDetailView>
          <ListDetailView>
            <Text>{Moment(hotels.checkOutDate).diff(Moment(hotels.checkInDate), 'days')} Malam, {hotels.hotelAdult} Tamu</Text>
          </ListDetailView>
      </ContentView>
    )
  }

  renderContact(booking) {
    const { contact } = booking;
    return (
      <MainInfoView style={[this.props.isComponent && {borderBottomWidth: 0.5, borderColor: '#000000'}, {alignItems: 'flex-start'}]}>
        <View style={{paddingHorizontal: 16}}>
          <ListDetailView>
            <Text>{contact.title.name + ' ' + contact.firstName + ' ' + contact.lastName}</Text>
          </ListDetailView>
          <ListDetailView>
            <Text>Telepon</Text><Text>{contact.phone}</Text>
          </ListDetailView>
          <ListDetailView>
            <Text>E-mail</Text><Text>{contact.email}</Text>
          </ListDetailView>
          <ListDetailView>
            <Text>Kode Pos</Text><Text>{contact.postalCode}</Text>
          </ListDetailView>
        </View>
      </MainInfoView>
    )
  }

  render() {
    const { isComponent } = this.props;
    const { booking } = this.state;

    // console.log(this.state, 'state');
    

    return (
      <MainView style={!isComponent && {height: '100%'}}>
        {!isComponent && <Header title='Detail Hotel' />}
        <ScrollView contentContainerStyle={{maxWidth: '100%'}}>
          <HeaderContainer>
            <HeaderSectionLeft>
              <AccordionTitle type='bold'>INFORMATION BOOKING</AccordionTitle>
            </HeaderSectionLeft>
          </HeaderContainer>

          {booking && this.renderHotel(booking)}
       
          <HeaderContainer>
            <HeaderSectionLeft><AccordionTitle type='bold'>RINCIAN PEMESAN</AccordionTitle></HeaderSectionLeft>
          </HeaderContainer>
          {booking && this.renderContact(booking)}

          {booking && <PriceDetail title='RINCIAN HARGA' booking={booking} />}
        </ScrollView>
      </MainView>
    );
  }

}
