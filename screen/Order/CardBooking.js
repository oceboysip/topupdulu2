import React, { Component } from 'react';
import { Image, View, StyleSheet } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Text from '../Text';
import Color from '../Color';
import TouchableOpacity from '../Button/TouchableDebounce';
import FormatMoney from '../FormatMoney';

const baseMargin = 12;

const CardView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    backgroundColor: #FFFFFF;
    borderWidth: 0.5;
    borderColor: #DDDDDD;
    borderRadius: 15;
    elevation: 5;
    marginBottom: 12;
`;

const MainView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
`;

const BottomLineView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    justifyContent: space-between;
    paddingHorizontal: ${baseMargin};
    paddingBottom: 16;
`;

const LeftBottomView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    flexDirection: column;
`;

const LineView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    paddingHorizontal: ${baseMargin};
    paddingVertical: 10;
`;

const HeaderView = Styled(LineView)`
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
`;

const SubLineView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    flexDirection: row;
    justifyContent: flex-start;
    alignItems: center;
`;

const ContentView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    justifyContent: flex-start;
    alignItems: center;
`;

const LeftContentView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
`;

const RightContentView = Styled(View)`
    flex: 1;
    flexDirection: row;
    justifyContent: flex-start;
    alignItems: center;
    flexWrap: wrap;
`;

const ThreeDotsView = Styled(TouchableOpacity)`
    alignItems: flex-end;
    justifyContent: center;
    width: 10%;
    paddingRight: 10;
`;

const OneDot = Styled(View)`
    width: 5;
    height: 5;
    borderRadius: 3;
    backgroundColor: ${Color.text};
`;

const MiddleDot = Styled(OneDot)`
    marginVertical: 2;
`;

const DateTimeView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    paddingTop: 3;
`;

const DateTimeText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
    color: #A8A699;
`;

const NormalText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const SmallerText = Styled(Text)`
    fontSize: 10;
    textAlign: left;
`;

const PriceText = Styled(NormalText)`
    color: #FF425E;
`;

const StatusText = Styled(NormalText)`
  paddingHorizontal: 6;
  paddingVertical: 2;
  color: #FFFFFF;
  borderRadius: 2;
`;

const BlueText = Styled(StatusText)`
    backgroundColor: #5887FB;
`;

const GreenText = Styled(StatusText)`
    backgroundColor: #2D8B57;
`;

const RedText = Styled(StatusText)`
    backgroundColor: #FF0000;
`;

const Icons = Styled(Ionicons)`
    fontSize: 17;
    marginHorizontal: 4;
`;

const ImageProperty = Styled(Image)`
    width: 18;
    height: 18;
    marginRight: 10;
`;

const SmallerImageProperty = Styled(ImageProperty)`
    width: 25;
    height: 25;
`;

const topUp = require('../../images/logotopup.png');
const flight = require('../../images/logotopup.png');
const tour = require('../../images/logotopup.png');
const attraction = require('../../images/logotopup.png');
const busIcon = require('../../images/logotopup.png');
const bedIcon = require('../../images/logotopup.png');
const pulsa = require('../../images/logotopup.png');

export default class CardBooking extends Component {
  countdownId;

  constructor(props) {
    super(props);
    this.state = {
      timeLeft: Math.max(Moment(this.props.booking.expiresAt).diff(Moment(), 'seconds'), 0)
    };
  }

  componentDidMount() {
    if (this.props.status === 1) this.countdownId = setInterval(() => this.countdown(), 1000);
  }

  componentWillUnmount() {
    if (this.countdownId) clearInterval(this.countdownId);
  }

  countdown() {
    const timeLeft = Moment(this.props.booking.expiresAt).diff(Moment(), 'seconds');
    if (timeLeft > 0) this.setState({ timeLeft });
    else {
      clearInterval(this.countdownId);
      this.setState({ timeLeft: 0 }, () => this.props.onExpired());
    }
  }

  splitSentence(sentence) {
    return sentence.split(' ').map((word, i) =>
      <NormalText type='bold' key={i}>{word} </NormalText>
    );
  }

  renderDateTimeCreated() {
    return <DateTimeView><DateTimeText>{Moment.parseZone(this.props.booking.createdAt).format('DD-MM-YYYY   HH:mm')}</DateTimeText></DateTimeView>;
  }

  renderSubContentFlight(flights, status) {
    return flights.map((flg, i) =>
      <View key={i}>
        <LineView>
          <ContentView style={{flexDirection: 'column', alignItems: 'flex-start'}}>
            <RightContentView>
              {this.splitSentence(flg.departure.journeys[0].origin.cityName)}
              <NormalText type='bold'>({flg.departure.journeys[0].origin.code})</NormalText>
              <Icons name='ios-arrow-round-forward' />
              {this.splitSentence(flg.departure.journeys[flg.departure.journeys.length - 1].destination.cityName)}
              <NormalText type='bold'>({flg.departure.journeys[flg.departure.journeys.length - 1].destination.code})</NormalText>
            </RightContentView>
    
            {status === 2 && <DateTimeView><DateTimeText>
              {Moment.parseZone(flg.departure.journeys[0].departsAt).format('ddd, DD MMM YYYY[.] HH:mm[ - ]')}
              {Moment.parseZone(flg.departure.journeys[flg.departure.journeys.length - 1].arrivesAt).format('HH:mm')}
            </DateTimeText></DateTimeView>}
          </ContentView>
        </LineView>
        {flg.return && <LineView>
          <ContentView>
            <RightContentView>
              {this.splitSentence(flg.return.journeys[0].origin.cityName)}
              <NormalText type='bold'>({flg.return.journeys[0].origin.code})</NormalText>
              <Icons name='ios-arrow-round-forward' />
              {this.splitSentence(flg.return.journeys[flg.return.journeys.length - 1].destination.cityName)}
              <NormalText type='bold'>({flg.return.journeys[flg.return.journeys.length - 1].destination.code})</NormalText>
            </RightContentView>

            {status === 2 && <DateTimeView><DateTimeText>
              {Moment.parseZone(flg.return.journeys[0].departsAt).format('ddd, DD MMM YYYY[.] HH:mm[ - ]')}
              {Moment.parseZone(flg.return.journeys[flg.return.journeys.length - 1].arrivesAt).format('HH:mm')}
            </DateTimeText></DateTimeView>}
          </ContentView>
        </LineView>}
      </View>
    );
  }

  renderSubContentTour(tours) {
    return tours.map((tr, i) =>
      <LineView key={i}>
        <ContentView>
          <LeftContentView><SmallerImageProperty resizeMode='contain' source={tour} /></LeftContentView>
          <RightContentView>{this.splitSentence(tr.tourSnapshot.name)}</RightContentView>
        </ContentView>
      </LineView>
    );
  }

  renderSubContentAttraction(attr){
    // return attr.map((tr, i) =>
    return(
      <LineView>
        <ContentView>
          <LeftContentView><SmallerImageProperty resizeMode='contain' source={attraction} /></LeftContentView>
          <RightContentView>{this.splitSentence(attr.attraction_name)}</RightContentView>
        </ContentView>
      </LineView>
    )
    // );
  }

  renderSubContentHotel(hotel){
    return(
      <LineView>
        <ContentView>
          <LeftContentView><SmallerImageProperty resizeMode='contain' source={bedIcon} /></LeftContentView>
          <RightContentView>{this.splitSentence(hotel.hotelName)}</RightContentView>
        </ContentView>
      </LineView>
    )
  }

  renderSubContentBuses(buses){
    let orderSeats = {
      departure: [],
      return: []
    }

    for (var i = 0; i < buses.seats.length; i++) {
      if (buses.seats[i].type === 'TRIP_AWAY') orderSeats.departure.push(buses.seats[i]);
      else orderSeats.return.push(buses.seats[i]);
    }

    return (
      <View>
        <LineView>
          <ContentView>
            <LeftContentView><SmallerImageProperty resizeMode='contain' source={busIcon} /></LeftContentView>
            <RightContentView style={{flexDirection: 'column', alignItems: 'flex-start'}}>
              <NormalText type='bold'>{orderSeats.departure[0].busesName}</NormalText>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <SmallerText type='medium'>{orderSeats.departure[0].departureName}</SmallerText>
                <Icons style={{fontSize: 12}} name='ios-arrow-round-forward' />
                <SmallerText type='medium'>{orderSeats.departure[0].arrivalName}</SmallerText>
              </View>
            </RightContentView>
          </ContentView>
        </LineView>
        {orderSeats.return.length > 0 && <LineView>
          <ContentView>
            <LeftContentView><SmallerImageProperty resizeMode='contain' source={busIcon} /></LeftContentView>
            <RightContentView style={{flexDirection: 'column', alignItems: 'flex-start'}}>
              <NormalText type='bold'>{orderSeats.return[0].busesName}</NormalText>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <SmallerText type='medium'>{orderSeats.return[0].departureName}</SmallerText>
                <Icons style={{fontSize: 12}} name='ios-arrow-round-forward' />
                <SmallerText type='medium'>{orderSeats.return[0].arrivalName}</SmallerText>
              </View>
            </RightContentView>
          </ContentView>
        </LineView>}
      </View>
    )
  }

  renderThreeDots() {
    return (
      <ThreeDotsView onPress={() => this.props.openModal(this.props.booking)}>
        <OneDot />
        <MiddleDot />
        <OneDot />
      </ThreeDotsView>
    );
  }

  renderDefault() {
    const { booking, status } = this.props;
    let statusText;
    const hours = Moment.duration(this.state.timeLeft, 'seconds').hours({ trim: false });
    const minutes = Moment.duration(this.state.timeLeft, 'seconds').minutes({ trim: false });
    const seconds = Moment.duration(this.state.timeLeft, 'seconds').seconds({ trim: false });

    switch (status) {
      case 1 : statusText = <GreenText type='bold'>MENUNGGU PEMBAYARAN - {hours > 0 ? hours + ' jam ' : ''}{minutes > 0 ? minutes + ' menit ' : ''}{seconds + ' detik'}</GreenText>; break;
      case 2 : statusText = <BlueText type='bold'>ORDER BERHASIL</BlueText>; break;
      default: statusText = <RedText type='bold'>ORDER TIDAK BERHASIL</RedText>; break;
    }
    return (
      <MainView>
        <HeaderView>
          <SubLineView>
            <Ionicons style={{fontSize: 22, marginRight: 10}} name='ios-information-circle' />
            <NormalText type='bold'>{booking.invoiceNumber}</NormalText>
          </SubLineView>
          <SubLineView>
            {/* <BlueText type='bold'>{FormatMoney.getFormattedMoney(booking.finalAmount)}</BlueText> */}
          </SubLineView>
        </HeaderView>
        <BottomLineView>
          <LeftBottomView>
            {statusText}
            {/* {this.renderDateTimeCreated()} */}
          </LeftBottomView>
          {this.renderThreeDots()}
        </BottomLineView>
      </MainView>
    );
  }

  renderVoucherGame() {
    const { booking, status } = this.props;
    let statusText;
    const hours = Moment.duration(this.state.timeLeft, 'seconds').hours({ trim: false });
    const minutes = Moment.duration(this.state.timeLeft, 'seconds').minutes({ trim: false });
    const seconds = Moment.duration(this.state.timeLeft, 'seconds').seconds({ trim: false });

    switch (status) {
      case 1 : statusText = <GreenText type='bold'>MENUNGGU PEMBAYARAN - {hours > 0 ? hours + ' jam ' : ''}{minutes > 0 ? minutes + ' menit ' : ''}{seconds + ' detik'}</GreenText>; break;
      case 2 : statusText = <BlueText type='bold'>ORDER BERHASIL</BlueText>; break;
      default: statusText = <RedText type='bold'>ORDER TIDAK BERHASIL</RedText>; break;
    }
    return (
      <MainView>
        <HeaderView>
          <SubLineView>
            <Ionicons style={{fontSize: 22, marginRight: 10}} name='ios-information-circle' />
            <NormalText type='bold'>{booking.invoiceNumber}</NormalText>
          </SubLineView>
          <SubLineView>
            {/* <BlueText type='bold'>{FormatMoney.getFormattedMoney(booking.finalAmount)}</BlueText> */}
          </SubLineView>
        </HeaderView>
        <BottomLineView>
          <LeftBottomView>
            {statusText}
            {/* {this.renderDateTimeCreated()} */}
          </LeftBottomView>
          {this.renderThreeDots()}
        </BottomLineView>
      </MainView>
    );
  }

  renderPaketData() {
    const { booking, status } = this.props;
    let statusText;
    const hours = Moment.duration(this.state.timeLeft, 'seconds').hours({ trim: false });
    const minutes = Moment.duration(this.state.timeLeft, 'seconds').minutes({ trim: false });
    const seconds = Moment.duration(this.state.timeLeft, 'seconds').seconds({ trim: false });

    switch (status) {
      case 1 : statusText = <GreenText type='bold'>MENUNGGU PEMBAYARAN - {hours > 0 ? hours + ' jam ' : ''}{minutes > 0 ? minutes + ' menit ' : ''}{seconds + ' detik'}</GreenText>; break;
      case 2 : statusText = <BlueText type='bold'>ORDER BERHASIL</BlueText>; break;
      default: statusText = <RedText type='bold'>ORDER TIDAK BERHASIL</RedText>; break;
    }
    return (
      <MainView>
        <HeaderView>
          <SubLineView>
            <Ionicons style={{fontSize: 22, marginRight: 10}} name='ios-information-circle' />
            <NormalText type='bold'>{booking.invoiceNumber}</NormalText>
          </SubLineView>
          <SubLineView>
            {/* <BlueText type='bold'>{FormatMoney.getFormattedMoney(booking.finalAmount)}</BlueText> */}
          </SubLineView>
        </HeaderView>
        <BottomLineView>
          <LeftBottomView>
            {statusText}
            {/* {this.renderDateTimeCreated()} */}
          </LeftBottomView>
          {this.renderThreeDots()}
        </BottomLineView>
      </MainView>
    );
  }

  renderPulsaPrabayar() {
    const { booking, status } = this.props;
    let statusText;
    const hours = Moment.duration(this.state.timeLeft, 'seconds').hours({ trim: false });
    const minutes = Moment.duration(this.state.timeLeft, 'seconds').minutes({ trim: false });
    const seconds = Moment.duration(this.state.timeLeft, 'seconds').seconds({ trim: false });

    switch (status) {
      case 1 : statusText = <GreenText type='bold'>MENUNGGU PEMBAYARAN - {hours > 0 ? hours + ' jam ' : ''}{minutes > 0 ? minutes + ' menit ' : ''}{seconds + ' detik'}</GreenText>; break;
      case 2 : statusText = <BlueText type='bold'>PEMBAYARAN PULSA PRABAYAR BERHASIL</BlueText>; break;
      default: statusText = <RedText type='bold'>PEMBAYARAN PULSA PRABAYAR KADALUARSA</RedText>; break;
    }
    return (
      <MainView>
        <HeaderView>
          <SubLineView><NormalText type='bold'>{booking.invoiceNumber}</NormalText></SubLineView>
          {/* <SubLineView><PriceText type='bold'>{FormatMoney.getFormattedMoney(booking.finalAmount)}</PriceText></SubLineView> */}
        </HeaderView>
        <LineView>
          <ContentView>
            <LeftContentView><SmallerImageProperty resizeMode='contain' source={pulsa} /></LeftContentView>
            {/* <RightContentView><NormalText type='bold'>PULSA PRABAYAR {booking.prepaidTransaction.destinationNumber}</NormalText></RightContentView> */}
          </ContentView>
        </LineView>
        <BottomLineView>
          <LeftBottomView>
            {statusText}
            {/* {this.renderDateTimeCreated()} */}
          </LeftBottomView>
          {this.renderThreeDots()}
        </BottomLineView>
      </MainView>
    );
  }

  renderPlnPrabayar() {
    const { booking, status } = this.props;
    let statusText;
    const hours = Moment.duration(this.state.timeLeft, 'seconds').hours({ trim: false });
    const minutes = Moment.duration(this.state.timeLeft, 'seconds').minutes({ trim: false });
    const seconds = Moment.duration(this.state.timeLeft, 'seconds').seconds({ trim: false });

    switch (status) {
      case 1 : statusText = <GreenText type='bold'>MENUNGGU PEMBAYARAN - {hours > 0 ? hours + ' jam ' : ''}{minutes > 0 ? minutes + ' menit ' : ''}{seconds + ' detik'}</GreenText>; break;
      case 2 : statusText = <BlueText type='bold'>PEMBAYARAN PLN PRABAYAR BERHASIL</BlueText>; break;
      default: statusText = <RedText type='bold'>PEMBAYARAN PLN PRABAYAR KADALUARSA</RedText>; break;
    }
    return (
      <MainView>
        <HeaderView>
          <SubLineView><NormalText type='bold'>{booking.invoiceNumber}</NormalText></SubLineView>
          {/* <SubLineView><PriceText type='bold'>{FormatMoney.getFormattedMoney(booking.finalAmount)}</PriceText></SubLineView> */}
        </HeaderView>
        <LineView>
          <ContentView>
            <LeftContentView><SmallerImageProperty resizeMode='contain' source={pulsa} /></LeftContentView>
            {/* <RightContentView><NormalText type='bold'>PLN PRABAYAR {booking.prepaidTransaction.destinationNumber}</NormalText></RightContentView> */}
          </ContentView>
        </LineView>
        <BottomLineView>
          <LeftBottomView>
            {statusText}
            {/* {this.renderDateTimeCreated()} */}
          </LeftBottomView>
          {this.renderThreeDots()}
        </BottomLineView>
      </MainView>
    );
  }

  renderPlnPascabayar() {
    const { booking, status } = this.props;
    let statusText;
    const hours = Moment.duration(this.state.timeLeft, 'seconds').hours({ trim: false });
    const minutes = Moment.duration(this.state.timeLeft, 'seconds').minutes({ trim: false });
    const seconds = Moment.duration(this.state.timeLeft, 'seconds').seconds({ trim: false });

    switch (status) {
      case 1 : statusText = <GreenText type='bold'>MENUNGGU PEMBAYARAN - {hours > 0 ? hours + ' jam ' : ''}{minutes > 0 ? minutes + ' menit ' : ''}{seconds + ' detik'}</GreenText>; break;
      case 2 : statusText = <BlueText type='bold'>PEMBAYARAN PLN PASCABAYAR BERHASIL</BlueText>; break;
      default: statusText = <RedText type='bold'>PEMBAYARAN PLN PASCABAYAR KADALUARSA</RedText>; break;
    }
    return (
      <MainView>
        <HeaderView>
          <SubLineView><NormalText type='bold'>{booking.invoiceNumber}</NormalText></SubLineView>
          {/* <SubLineView><PriceText type='bold'>{FormatMoney.getFormattedMoney(booking.finalAmount)}</PriceText></SubLineView> */}
        </HeaderView>
        <LineView>
          <ContentView>
            <LeftContentView><SmallerImageProperty resizeMode='contain' source={pulsa} /></LeftContentView>
            {/* <RightContentView><NormalText type='bold'>PLN PASCABAYAR {booking.postpaidTransaction.customerSubscribeNumber}</NormalText></RightContentView> */}
          </ContentView>
        </LineView>
        <BottomLineView>
          <LeftBottomView>
            {statusText}
            {/* {this.renderDateTimeCreated()} */}
          </LeftBottomView>
          {this.renderThreeDots()}
        </BottomLineView>
      </MainView>
    );
  }

  renderPdamPascabayar() {
    const { booking, status } = this.props;
    let statusText;
    const hours = Moment.duration(this.state.timeLeft, 'seconds').hours({ trim: false });
    const minutes = Moment.duration(this.state.timeLeft, 'seconds').minutes({ trim: false });
    const seconds = Moment.duration(this.state.timeLeft, 'seconds').seconds({ trim: false });

    switch (status) {
      case 1 : statusText = <GreenText type='bold'>MENUNGGU PEMBAYARAN - {hours > 0 ? hours + ' jam ' : ''}{minutes > 0 ? minutes + ' menit ' : ''}{seconds + ' detik'}</GreenText>; break;
      case 2 : statusText = <BlueText type='bold'>PEMBAYARAN PDAM PASCABAYAR BERHASIL</BlueText>; break;
      default: statusText = <RedText type='bold'>PEMBAYARAN PDAM PASCABAYAR KADALUARSA</RedText>; break;
    }
    return (
      <MainView>
        <HeaderView>
          <SubLineView><NormalText type='bold'>{booking.invoiceNumber}</NormalText></SubLineView>
          {/* <SubLineView><PriceText type='bold'>{FormatMoney.getFormattedMoney(booking.finalAmount)}</PriceText></SubLineView> */}
        </HeaderView>
        <LineView>
          <ContentView>
            <LeftContentView><SmallerImageProperty resizeMode='contain' source={pulsa} /></LeftContentView>
            {/* <RightContentView><NormalText type='bold'>PLN PASCABAYAR {booking.postpaidTransaction.customerSubscribeNumber}</NormalText></RightContentView> */}
          </ContentView>
        </LineView>
        <BottomLineView>
          <LeftBottomView>
            {statusText}
            {/* {this.renderDateTimeCreated()} */}
          </LeftBottomView>
          {this.renderThreeDots()}
        </BottomLineView>
      </MainView>
    );
  }

  renderTour() {
    const { booking, status } = this.props;
    let statusText;
    const hours = Moment.duration(this.state.timeLeft, 'seconds').hours({ trim: false });
    const minutes = Moment.duration(this.state.timeLeft, 'seconds').minutes({ trim: false });
    const seconds = Moment.duration(this.state.timeLeft, 'seconds').seconds({ trim: false });

    switch (status) {
      case 1 : statusText = <GreenText type='bold'>MENUNGGU PEMBAYARAN - {hours > 0 ? hours + ' jam ' : ''}{minutes > 0 ? minutes + ' menit ' : ''}{seconds + ' detik'}</GreenText>; break;
      case 2 : statusText = <BlueText type='bold'>PEMBAYARAN TRAVEL BERHASIL</BlueText>; break;
      default: statusText = <RedText type='bold'>PESANAN KADALUARSA</RedText>; break;
    }
    return (
      <MainView>
        <HeaderView>
          <SubLineView><NormalText type='bold'>{booking.invoiceNumber}</NormalText></SubLineView>
          {/* <SubLineView><PriceText type='bold'>{FormatMoney.getFormattedMoney(booking.finalAmount)}</PriceText></SubLineView> */}
        </HeaderView>
        {/* {this.renderSubContentTour(booking.tours)} */}
        <BottomLineView>
          <LeftBottomView>
            {statusText}
            {/* {this.renderDateTimeCreated()} */}
          </LeftBottomView>
          {this.renderThreeDots()}
        </BottomLineView>
      </MainView>
    );
  }

  renderFlight() {
    const { booking, status } = this.props;
    let statusText;
    const hours = Moment.duration(this.state.timeLeft, 'seconds').hours({ trim: false });
    const minutes = Moment.duration(this.state.timeLeft, 'seconds').minutes({ trim: false });
    const seconds = Moment.duration(this.state.timeLeft, 'seconds').seconds({ trim: false });

    switch (status) {
      case 1 : statusText = <GreenText type='bold'>MENUNGGU PEMBAYARAN - {hours > 0 ? hours + ':' : ''}{minutes > 0 ? minutes + ':' : ''}{seconds}</GreenText>; break;
      case 2 : statusText = <BlueText type='bold'>PEMBAYARAN SUKSES</BlueText>; break;
      default: statusText = <RedText type='bold'>PEMBAYARAN KADALUARSA</RedText>; break;
    }
    return (
      <MainView>
        <HeaderView>
          <SubLineView><NormalText style={{color: Color.theme}} type='bold'>{booking.invoiceNumber}</NormalText></SubLineView>
          {/* <SubLineView><PriceText type='bold'>{FormatMoney.getFormattedMoney(booking.finalAmount)}</PriceText></SubLineView> */}
        </HeaderView>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity style={{width: '90%'}} onPress={() => this.props.onSelectOrder(booking)}>
            {/* {this.renderSubContentFlight(booking.flights, status)} */}
            <BottomLineView style={{paddingBottom: 8}}>
              {statusText}
            </BottomLineView>
            {status === 2 && <View style={{paddingLeft: 10, paddingBottom: 10}}><DateTimeText>
              {Moment.parseZone(booking.departsAt).format('ddd, DD MMM YYYY[ ] HH:mm[-]')}
              {Moment.parseZone(booking.arrivesAt).format('HH:mm')}
            </DateTimeText></View>}
          </TouchableOpacity>
          {this.renderThreeDots()}
        </View>
      </MainView>
    );
  }

  renderAttraction() {
    const { booking, status } = this.props;
    let statusText;
    const hours = Moment.duration(this.state.timeLeft, 'seconds').hours({ trim: false });
    const minutes = Moment.duration(this.state.timeLeft, 'seconds').minutes({ trim: false });
    const seconds = Moment.duration(this.state.timeLeft, 'seconds').seconds({ trim: false });

    switch (status) {
      case 1 : statusText = <GreenText type='bold'>MENUNGGU PEMBAYARAN - {hours > 0 ? hours + ' jam ' : ''}{minutes > 0 ? minutes + ' menit ' : ''}{seconds + ' detik'}</GreenText>; break;
      case 2 : statusText = <BlueText type='bold'>PEMBAYARAN ATRAKSI BERHASIL</BlueText>; break;
      default: statusText = <RedText type='bold'>PESANAN KADALUARSA</RedText>; break;
    }
    return (
      <MainView>
        <HeaderView>
          <SubLineView><NormalText type='bold'>{booking.invoiceNumber}</NormalText></SubLineView>
          {/* <SubLineView><PriceText type='bold'>{FormatMoney.getFormattedMoney(booking.finalAmount)}</PriceText></SubLineView> */}
        </HeaderView>
        {/* {this.renderSubContentAttraction(booking.attractions)} */}
        <BottomLineView>
          <LeftBottomView>
            {statusText}
            {/* {this.renderDateTimeCreated()} */}
          </LeftBottomView>
          {this.renderThreeDots()}
        </BottomLineView>
      </MainView>
    );
  }

  renderBuses() {
    const { booking, status } = this.props;
    let statusText;    
    const hours = Moment.duration(this.state.timeLeft, 'seconds').hours({ trim: false });
    const minutes = Moment.duration(this.state.timeLeft, 'seconds').minutes({ trim: false });
    const seconds = Moment.duration(this.state.timeLeft, 'seconds').seconds({ trim: false });

    switch (status) {
      case 1 : statusText = <GreenText type='bold'>MENUNGGU PEMBAYARAN - {hours > 0 ? hours + ' jam ' : ''}{minutes > 0 ? minutes + ' menit ' : ''}{seconds + ' detik'}</GreenText>; break;
      case 2 : statusText = <BlueText type='bold'>PEMBAYARAN TIKET BUS BERHASIL</BlueText>; break;
      default: statusText = <RedText type='bold'>PESANAN KADALUARSA</RedText>; break;
    }
    return (
      <MainView>
        <HeaderView>
          <SubLineView><NormalText type='bold'>{booking.invoiceNumber}</NormalText></SubLineView>
          {/* <SubLineView><PriceText type='bold'>{FormatMoney.getFormattedMoney(booking.finalAmount)}</PriceText></SubLineView> */}
        </HeaderView>
        {/* {this.renderSubContentBuses(booking.buses)} */}
        <BottomLineView>
          <LeftBottomView>
            {statusText}
            {/* {this.renderDateTimeCreated()} */}
          </LeftBottomView>
          {this.renderThreeDots()}
        </BottomLineView>
      </MainView>
    );
  }

  renderHotel() {
    const { booking, status } = this.props;
    let statusText;
    const hours = Moment.duration(this.state.timeLeft, 'seconds').hours({ trim: false });
    const minutes = Moment.duration(this.state.timeLeft, 'seconds').minutes({ trim: false });
    const seconds = Moment.duration(this.state.timeLeft, 'seconds').seconds({ trim: false });

    switch (status) {
      case 1 : statusText = <GreenText type='bold'>MENUNGGU PEMBAYARAN - {hours > 0 ? hours + ' jam ' : ''}{minutes > 0 ? minutes + ' menit ' : ''}{seconds + ' detik'}</GreenText>; break;
      case 2 : statusText = <BlueText type='bold'>PEMBAYARAN HOTEL BERHASIL</BlueText>; break;
      default: statusText = <RedText type='bold'>PESANAN KADALUARSA</RedText>; break;
    }
    return (
      <MainView>
        <HeaderView>
          <SubLineView><NormalText type='bold'>{booking.invoiceNumber}</NormalText></SubLineView>
          {/* <SubLineView><PriceText type='bold'>{FormatMoney.getFormattedMoney(booking.finalAmount)}</PriceText></SubLineView> */}
        </HeaderView>
        {/* {this.renderSubContentHotel(booking.hotels)} */}
        <BottomLineView>
          <LeftBottomView>
            {statusText}
            {/* {status === 2 && <DateTimeView><DateTimeText>
              {Moment.parseZone(booking.hotels.checkInDate).format('DD MMM')} -&nbsp;
              {Moment.parseZone(booking.hotels.checkOutDate).format('DD MMM')}
            </DateTimeText></DateTimeView>} */}
          </LeftBottomView>
          {this.renderThreeDots()}
        </BottomLineView>
      </MainView>
    )
  }

  renderSwitch(type) {
    switch (type) {
      case 'Pesawat': return this.renderFlight();
      case 'Travel': return this.renderTour();
      case 'Hotel': return this.renderHotel();
      case 'Atraksi': return this.renderAttraction();
      case 'Bus': return this.renderBuses();
      case 'Pulsa': return this.renderPulsaPrabayar();
      case 'PlnPrabayar': return this.renderPlnPrabayar();
      case 'PlnPascabayar': return this.renderPlnPascabayar();
      case 'PdamPascabayar': return this.renderPdamPascabayar();
      case 'PaketData': return this.renderPaketData();
      case 'VoucherGame': return this.renderVoucherGame();
      default: return this.renderDefault();
    }
  }

  render() {
    const { type } = this.props;
    // console.log(this.props, 'props card booking');
    
    return (
      <CardView>
        {this.renderSwitch(type)}
      </CardView>
    );
  }
}
