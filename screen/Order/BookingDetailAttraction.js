import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';

import Text from '../Text';
import Header from '../Header';
import PriceDetail from './PriceDetail';
import Client from '../../state/apollo';
import gql from "graphql-tag";

const MainView = Styled(View)`
    width: 100%;
    alignItems: center;
    backgroundColor: #FFFFFF;
`;

const HeaderContainer = Styled(View)`
    width: 100%;
    height: 50;
    alignItems: flex-start;
    justifyContent: center;
    paddingHorizontal: 16;
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
    backgroundColor: #FFFFFF;
`;

const TitleText = Styled(Text)`
    letterSpacing:1
`;

const ContentView = Styled(View)`
    width: 100%;
    minHeight: 1;
    paddingHorizontal: 16;
    borderBottomWidth: 8;
    borderColor: #DDDDDD;
`;

const ListDetailView = Styled(View)`
    width: 100%;
    height: 40;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
`;

const queryBookingDetail = gql`
  query(
    $bookingId: Int!
  ) {
    bookingDetail(
      bookingId: $bookingId
    ){
      id
      userId
      invoiceNumber
      createdAt
      expiresAt
      amount
      discount
      vat
      finalAmount
      contact {
        title { name abbr }
        firstName
        lastName
        email
        countryCode
        phone
        address
        postalCode
      }
      bookingStatus { id name }
      payments {
        type { id name }
        amount
      }
      attractions {
        id
        paxPrice { adults children seniors }
        timeslottxt
        message
        adults
        children
        seniors
        arrivaldate
        attraction_name
        confirm_date
        status
        discount
        ppn
        final_amount
      }
    }
  }`
;

export default class BookingDetailAttraction extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);

    let booking = null;
    if (props.isComponent) booking = props.booking;

    this.state = {
      booking
    }
  }

  componentDidMount() {
    if (!this.props.isComponent) this.getBookingDetail(this.props.navigation.state.params.booking.id);
  }

  getBookingDetail(bookingId) {
    const variables = { bookingId };
    
    Client
    .query({
        query: queryBookingDetail,
        variables
    })
    .then(res => {
      console.log(res, 'res detail attr');
      if(res.data.bookingDetail) {
        this.setState({ booking: res.data.bookingDetail });
      }
    }).catch(error => {
        console.log(error, 'error detail booking');
    });
  }

  renderDetail(booking) {
    return (
      <View>
        <HeaderContainer>
          <TitleText type='bold'>INFORMATION BOOKING</TitleText>
        </HeaderContainer>
        <ContentView>
          <ListDetailView style={{justifyContent: 'center'}}>
            <Text type='bold'>{booking.attractions.attraction_name}</Text>
          </ListDetailView>
          {/* <ListDetailView>
            <Text>ID Booking</Text><Text>{booking.id}</Text>
          </ListDetailView> */}
          <ListDetailView>
            <Text>Nomor Invoice</Text><Text>{booking.invoiceNumber}</Text>
          </ListDetailView>
          <ListDetailView>
            <Text>Tanggal Kedatangan</Text><Text>{Moment(booking.attractions.arrivaldate).format('ddd, DD MMM YYYY')}</Text>
          </ListDetailView>
          <ListDetailView>
            <Text>Jumlah Tamu</Text><Text>{booking.attractions.adults} Dewasa</Text>
          </ListDetailView>
          {booking.attractions.children > 0 && <ListDetailView>
            <Text></Text><Text>{booking.attractions.children} Anak</Text>
          </ListDetailView>}
          {booking.attractions.seniors > 0 && <ListDetailView>
            <Text></Text><Text>{booking.attractions.seniors} Lanjut Usia</Text>
          </ListDetailView>}
        </ContentView>
      </View>
    )
  }

  render() {
    const { booking } = this.state;
    const { isComponent } = this.props;
    return (
      <MainView style={!isComponent && {height: '100%'}}>
        {!isComponent && <Header title='Detail Atraksi' />}
        <ScrollView contentContainerStyle={{ maxWidth: '100%' }}>
          {booking && this.renderDetail(booking)}
          {booking && <PriceDetail accordion title='RINCIAN HARGA' booking={booking} />}
        </ScrollView>
      </MainView>
    );
  }
}
