import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import Styled from 'styled-components';

import Header from '../Header';
import PriceDetail from './PriceDetail';
import Client from '../../state/apollo';
import gql from "graphql-tag";

import DetailOrderPrepaid from './DetailOrderPrepaid';

const MainView = Styled(View)`
    width: 100%;
    alignItems: center;
    backgroundColor: #FFFFFF;
`;

const queryBookingDetail = gql`
  query(
    $bookingId: Int!
  ) {
    bookingDetail(
      bookingId: $bookingId
    ){
      id
      userId
      invoiceNumber
      createdAt
      expiresAt
      amount
      discount
      vat
      finalAmount
      contact {
        title { name abbr }
        firstName
        lastName
        email
        countryCode
        phone
        address
        postalCode
      }
      bookingStatus { id name }
      payments {
        type { id name }
        amount
      }
      prepaidTransaction {
        id
        orderId
        refId
        status
        code
        destinationNumber
        price
        message
        balance
        tr_id
        rc
        category
        order_status
        request_data
        response_data
        lastUpdated
      }
    }
  }`
;

export default class BookingDetailPlnPrabayar extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);

    let booking = null;
    if (props.isComponent) booking = props.booking;

    this.state = {
      booking
    }
  }

  componentDidMount() {
    if (!this.props.isComponent) this.getBookingDetail(this.props.navigation.state.params.booking.id);
  }

  getBookingDetail(bookingId) {
    const variables = { bookingId };
    
    Client
    .query({
        query: queryBookingDetail,
        variables
    })
    .then(res => {
      console.log(res, 'res detail pln prabayar');
      if(res.data.bookingDetail) {
        this.setState({ booking: res.data.bookingDetail });
      }
    }).catch(error => {
        console.log(error, 'error detail pln prabayar');
    });
  }

  render() {
    const { booking } = this.state;
    const { isComponent } = this.props;
    return (
      <MainView style={!isComponent && {height: '100%'}}>
        {!isComponent && <Header title='Detail Order Pln Prabayar' />}
        <ScrollView contentContainerStyle={{ maxWidth: '100%' }}>
          {booking && <DetailOrderPrepaid booking={booking} />}
          {booking && <PriceDetail title='RINCIAN HARGA' booking={booking} />}
        </ScrollView>
      </MainView>
    );
  }
}
