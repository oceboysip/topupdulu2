import React, { Component } from 'react';
import { View } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';

import Text from '../Text';

const HeaderContainer = Styled(View)`
    width: 100%;
    height: 50;
    alignItems: flex-start;
    justifyContent: center;
    paddingHorizontal: 16;
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
    backgroundColor: #FFFFFF;
`;

const TitleText = Styled(Text)`
    letterSpacing:1
`;

const ContentView = Styled(View)`
    width: 100%;
    minHeight: 1;
    paddingHorizontal: 16;
    borderBottomWidth: 8;
    borderColor: #DDDDDD;
`;

const ListDetailView = Styled(View)`
    width: 100%;
    height: 40;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
`;

export default class DetailOrderPostpaid extends Component {
    render() {
        const { booking } = this.props;
        return (
            <View>
                <HeaderContainer>
                    <TitleText type='bold'>INFORMATION ORDER</TitleText>
                </HeaderContainer>
                <ContentView>
                    {/* <ListDetailView>
                        <Text>ID Booking</Text><Text>{booking.id}</Text>
                    </ListDetailView> */}
                    <ListDetailView>
                        <Text>Nomor Invoice</Text><Text>{booking.invoiceNumber}</Text>
                    </ListDetailView>
                    <ListDetailView>
                        <Text>Tanggal Order</Text><Text>{Moment(booking.createdAt).format('ddd, DD MMM YYYY')}</Text>
                    </ListDetailView>
                    <ListDetailView>
                        <Text>ID Pelanggan</Text><Text>{booking.postpaidTransaction.customerSubscribeNumber}</Text>
                    </ListDetailView>
                </ContentView>
            </View>
        )
    }
}