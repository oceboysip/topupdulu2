import React, { Component } from "react";
import { StyleSheet, View, Modal, Image, TextInput, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native';

import NewsListStyle from '../../style/NewsListStyle';
import Color from '../../style/Color';
import Text from '../Text';

import NewsTabNavigator from '../../navigations/NewsTabNavigator';

export default class MainScreenNews extends Component {
  constructor(props){
    super(props);
    this.state = {
      dataSearch: [],
      loadingSearch: true,
      modalSearch: false,
      valueSearch: '',
      searchMessage: '',
      resultSearch: []
    };
  }

  openModal(modal) {
    this.setState({ [modal]: true });
  }

  closeModal(modal) {
    this.setState({ [modal]: false, valueSearch: '' });
  }

  renderCardSearch(dataItem) {
    return <TouchableOpacity >
        <Text></Text>
      </TouchableOpacity>
  }

renderModalSearch() {
    const { loadingSearch, dataSearch, valueSearch, searchMessage, resultSearch } = this.state;

    return (
      <SafeAreaView style={{flex: 1}}>
      <View style={{backgroundColor: 'rgba(255, 255, 255, 0.8)', height: '100%'}}>
        <View style={{
          paddingHorizontal:20,
          backgroundColor:'#145c94',
          shadowColor: '#000',
          paddingVertical:10,
          marginBottom:20,
          shadowOffset: { width: 0, height: 1 },
          shadowOpacity: 0.8,
          shadowRadius: 2,  
          elevation: 5,
          marginHorizontl:17,
          flexDirection:'row'}}>

          <View style={{width: '80%'}}>
            <TextInput
              value={this.state.valueSearch}
              placeholder='Ketikan berita'
              // onChangeText={(search) => this.onChangeTextSearch(search)}
              style={{backgroundColor: '#FFFFFF', paddingLeft: 10, height: 48}}  
            />
          </View>
          <TouchableOpacity disabled style={{width: '12%', backgroundColor: '#FFFFFF', height: 48, justifyContent: 'center'}}>
          <Image style={{ width: 35, height: 35,resizeMode:'contain' }} source={require('../../images/icon_search_yellow.png')}/>
          </TouchableOpacity>
        </View>

        <ScrollView>
          {valueSearch === '' && this.renderCardSearch(dataSearch)}
          {!loadingSearch && resultSearch.length > 0 && valueSearch !== '' && this.renderCardSearch(resultSearch)}
          {loadingSearch && valueSearch !== '' && <View style={{alignItems: 'center', justifyContent: 'center', height: 120}}><ActivityIndicator /><Text>Memuat</Text></View>}
          {!loadingSearch && resultSearch.length === 0 && valueSearch !== '' && <View style={{alignItems: 'center', justifyContent: 'center', height: 120}}><Text>{searchMessage}</Text></View>}
          <TouchableOpacity onPress={() => this.closeModal('modalSearch')} activeOpacity={0.7} style={{paddingVertical: 16, paddingHorizontal: 16, borderBottomWidth: 0.5, borderColor: '#DDDDDD', backgroundColor: '#FFFFFF', alignItems: 'center'}}>
            <View style={{height: 37, backgroundColor: Color.theme, width: '100%', justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{color: '#FFFFFF'}}>Kembali</Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
      </SafeAreaView>
    )
  }
  
  render() {
    const { modalSearch}  = this.state;

    return (
        <View style={{flex: 1}}>
            <NewsTabNavigator screenProps={{screenProps: this.props.screenProps, navigation: this.props.navigation}} />

            <TouchableOpacity activeOpacity={1} style={{position: 'absolute', top: 0, width: '14%', height: 80, backgroundColor: Color.theme, right: 0}} onPress={() => this.openModal('modalSearch')}>
                <View style={{flexDirection: 'row',alignItems:'center', width: '100%', height: '100%', paddingRight: 16}}>
                    <Image style={{width: '100%', aspectRatio: 1,resizeMode:'contain' }} source={require('../../images/icon_search_yellow.png')} />
                </View>
            </TouchableOpacity>
        
            <TouchableOpacity style={{position:'absolute', left:20,bottom:20 }} onPress={() => this.props.navigation.pop()}>
                <Image style={{ height: 40, width: 40,resizeMode:'contain'}} source={require('../../images/icon_back.png')}/>
            </TouchableOpacity>

            <Modal
                visible={modalSearch}
                onRequestClose={() => this.closeModal('modalSearch')}
                animationType='fade'
                transparent
            >
                {this.renderModalSearch()}
            </Modal>
        </View>
    );
  }

  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}