import React, { Component } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, Dimensions } from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import HTMLView from 'react-native-htmlview';
import FitImage from 'react-native-fit-image';
import { Spinner } from 'native-base';

import NewsDetailStyle from '../../style/NewsDetailStyle';
import Header from '../Header';

export default class NewsDetailScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      idnya: this.props.navigation.state.params.idnya,
      berita: this.props.navigation.state.params.berita,
      tempContentBerita: [],
      title: "",
    };
  }

  componentDidMount() {
    this._loadInitialState().done();
  }

  setLoading = (loading) => {
		this.setState({loading: loading});
  }
    
  _loadInitialState = async () => {
    var value = await AsyncStorage.getItem('user');
    if(value == null) {
      // this.props.navigation.navigate('Login');
    }else{
        this.setState({user: JSON.parse(value)})
        this.props.screenProps.setUser(this.state.user)
        this.props.screenProps.setLoading(true);   
        this._loadBerita();
    }
  }

  _loadBerita = () => {
    var url="";
    var tokenberita="";
    var paramtoken="";
    if(this.state.berita==='CARPAN')
    {
      url="http://ataprumah.com/api/detail/";
      tokenberita="6?spik&6es@g5Cr-p1od";
      paramtoken='CARPAN-TOKEN-KEY';
      axios.get(url+this.state.idnya, {headers: {'CARPAN-TOKEN-KEY': tokenberita}})
      .then(response => {
        res = response.data;
     
        this.props.screenProps.setLoading(false); 
        this.setState({tempContentBerita: res.data.detail[0]});
        this.setState({title: "CARAPANDANG"});
       })
      .catch(function (error,ye) {
        console.log(error);
      });
    }else if (this.state.berita==='SEMANGAT'){
      url="https://zulisk.com/apisemangat/detail/";
      tokenberita="h5WoME1u$oJofOwi99F1";
      paramtoken="SEMANGAT-Token-Key";
   
      axios.get(url+this.state.idnya, {headers: {'SEMANGAT-Token-Key': tokenberita}})
      .then(response => {
        res = response.data;
     
        this.props.screenProps.setLoading(false); 
        this.setState({tempContentBerita:res.data.detail[0]});
        this.setState({title:"Semangat 45"});
       })
      .catch(function (error,ye) {
        console.log(error);
      });
    }
  }
  
  render() {
    let dimensions = Dimensions.get("window");
    let imageHeight = Math.round((dimensions.width * 9) / 16);
    let imageWidth = dimensions.width;  
   // var detailGambar=this.state.tempContentBerita.gambar_detail;
    const { title } = this.state;
    
    return (
      <View style={styles.container}>
        <Header title={title !== '' ? title : 'Memuat...'} />
        <ScrollView>
          <View style={styles.rows}>
            <Text style={styles.bigTitle}>{this.state.tempContentBerita.judul}</Text>
            <View style={{flexDirection:"row",marginBottom:10}}>
              <View>
                {this.state.tempContentBerita.datetime &&  <Text style={styles.smallGrey}>{this.state.tempContentBerita.datetime}</Text> }
              </View>
            </View>
          </View>

          <View style={styles.rowSectionNopad}>
            <View>
              {this.state.tempContentBerita.gambar_detail && <FitImage source={{uri: this.state.tempContentBerita.gambar_detail}}/>}
            </View>
          </View>

          <View>
            <Text style={styles.smallGrey2}>{this.state.tempContentBerita.tag}</Text>
          </View>

          <View>
            <View style={styles.rows}>
              {this.props.screenProps.loading && <Spinner color="green" />}
              {this.state.tempContentBerita.isi && <HTMLView addLineBreaks={false} value={`<div>${this.state.tempContentBerita.isi}</div>`}/>}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}

// STYLES
// ------------------------------------
const styles = StyleSheet.create(NewsDetailStyle);