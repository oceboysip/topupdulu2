import React, { Component } from "react";
import { Alert, StyleSheet, FlatList, View, Image, TextInput, TouchableOpacity, SafeAreaView, ScrollView, Dimensions } from 'react-native';
import NewsListStyle from '../../style/NewsListStyle';
import FitImage from 'react-native-fit-image';
import {Spinner} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import Color from '../../style/Color';
import Text from '../Text';

let NeoSansStdBoldTR = 'NeoSansStd Bold TR';
let NeoSansStdBoldItalicTR = 'NeoSansStd BoldItalic TR';
let NeoSansStdTR = 'NeoSansStd TR';

if (Platform.OS === 'ios') {
  NeoSansStdBoldTR = 'NeoSansStdBoldTR';
  NeoSansStdBoldItalicTR = 'NeoSansStdBoldItalicTR';
  NeoSansStdTR = 'NeoSansStdTR';
}

export default class NewsListScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isOnDefaultToggleSwitch: false,
      isOnLargeToggleSwitch: false,
      isOnBlueToggleSwitch: false,
      isHidden: false,
      tempContentBerita:[],
      tempContentBerita2:[],
      dataSearch: [],
      loadingSearch: true,
      modalSearch: false,
      valueSearch: '',
      searchMessage: '',
      activeMenu: 'dashboard',
      resultSearch: []

    };
  }

  componentDidMount(){
    this._loadInitialState().done();
  }

  _loadInitialState = async () => {
    var value = await AsyncStorage.getItem('user');
    if(value == null) {
      // this.props.navigation.navigate('Login');
    }else{
        this.setState({user: JSON.parse(value)})
        this.props.screenProps.setUser(this.state.user)
        this.props.screenProps.setLoading(false);   
        this.props.screenProps.setLoading(true);   
        this._loadBerita();
        this._loadBerita2();
    }
  }
  setLoading = (loading) => {
    this.setState({loading: loading})
	  }
  onToggle(isOn) {
    console.log("Changed to " + isOn);
  }
  Dashboard = () => {
    this.props.navigation.navigate('Dashboard');
  }
  Berita = () => {
    this.props.navigation.navigate('Berita');
  }
  Carpan= () => {
    this.props.navigation.navigate('CaraPandang');
  }

  _loadBerita = () => {
    axios.get("http://ataprumah.com/api/headline", {headers: {'CARPAN-TOKEN-KEY': '6?spik&6es@g5Cr-p1od'}})
    .then(response => {
      res = response.data;
      //console.log(res.data);
      if(res.status){
        this.setState({tempContentBerita:res.data})
        this.props.screenProps.setLoading(false);
      }
    })
    .catch(function (error,ye) {
      console.log(error);
      console.log(ye);
        Alert.alert("Siago", "Terjadi kesalahan: "+error)
    });
  }

  _loadBerita2 = () => {
    axios.get("https://zulisk.com/apisemangat//headline", {headers: {'SEMANGAT-Token-key': 'h5WoME1u$oJofOwi99F1'}})
    .then(response => {
      res = response.data;
      //console.log(res.data);
      if(res.status){
        this.setState({tempContentBerita2:res.data})
        this.props.screenProps.setLoading(false);
      }
    })
    .catch(function (error,ye) {
      console.log(error);
      console.log(ye);
        Alert.alert("Siago", "Terjadi kesalahan: "+error)
    });
  }

  GetImage(content){
    var myRegexp = new RegExp(/<img.*?src="(.*?)"/);
    var match = myRegexp.exec(content);
        if (match){
            return match[1];
         }
  }

  ContentSnippet(content){
    return content.split(/\s+/).slice(0, 30).join(" ")+"...";
  }

  openModal(modal) {
    this.setState({ [modal]: true });
  }

  closeModal(modal) {
    this.setState({ [modal]: false, valueSearch: '' });
  }
  navigateToDetail(item, time, header) {
    this.props.navigation.navigate('NewsDetailScreen', {item, time, header, dataDirektorat: this.state.dataDirektorat});
    this.closeModal('modalSearch')
  }

  _RenderBeritaHeadline(idnya, judul, gambar_detail, status) {
    return (
      <TouchableOpacity activeOpacity={0.7} onPress={() => this.props.navigation.navigate('NewsDetailScreen',{ idnya: idnya,berita: status})} style={{justifyContent:'flex-start', height: 200}}>
        <View style={styles.rowNews}>
          <View style={styles.bigGallery}>
            <FitImage source={{uri: gambar_detail}}/>
              <View style={styles.absOverflow}></View>
            </View>

            <View style={styles.absTitle}>
              <Text style={{fontFamily: NeoSansStdTR}}>{judul}</Text>
            </View>
        </View> 
      </TouchableOpacity>
    );
  }

  Dashboard = () => {
    this.props.navigation.navigate('Dashboard');
  }

  renderCardSearch(dataItem) {
    return <TouchableOpacity >
        <Text></Text>
      </TouchableOpacity>
  }

  renderModalSearch() {
    const { loadingSearch, dataSearch, valueSearch, searchMessage, resultSearch } = this.state;

    return (
      <SafeAreaView style={{flex: 1}}>
      <View style={{backgroundColor: 'rgba(255, 255, 255, 0.8)', height: '100%'}}>
        <View style={{
          paddingHorizontal:20,
          backgroundColor:'#145c94',
          shadowColor: '#000',
          paddingVertical:10,
          marginBottom:20,
          shadowOffset: { width: 0, height: 1 },
          shadowOpacity: 0.8,
          shadowRadius: 2,  
          elevation: 5,
          marginHorizontl:17,
          flexDirection:'row'}}>

          <View style={{width: '80%'}}>
            <TextInput
              value={this.state.valueSearch}
              placeholder='Ketikan berita'
              // onChangeText={(search) => this.onChangeTextSearch(search)}
              style={{backgroundColor: '#FFFFFF', paddingLeft: 10, height: 48}}  
            />
          </View>
          <TouchableOpacity disabled style={{width: '12%', backgroundColor: '#FFFFFF', height: 48, justifyContent: 'center'}}>
          <Image style={{ width: 35, height: 35,resizeMode:'contain' }} source={require('../../images/icon_search_yellow.png')}/>
          </TouchableOpacity>
        </View>

        <ScrollView>
          {valueSearch === '' && this.renderCardSearch(dataSearch)}
          {!loadingSearch && resultSearch.length > 0 && valueSearch !== '' && this.renderCardSearch(resultSearch)}
          {loadingSearch && valueSearch !== '' && <View style={{alignItems: 'center', justifyContent: 'center', height: 120}}><ActivityIndicator /><Text>Memuat</Text></View>}
          {!loadingSearch && resultSearch.length === 0 && valueSearch !== '' && <View style={{alignItems: 'center', justifyContent: 'center', height: 120}}><Text>{searchMessage}</Text></View>}
          <TouchableOpacity onPress={() => this.closeModal('modalSearch')} activeOpacity={0.7} style={{paddingVertical: 16, paddingHorizontal: 16, borderBottomWidth: 0.5, borderColor: '#DDDDDD', backgroundColor: '#FFFFFF', alignItems: 'center'}}>
            <View style={{height: 37, backgroundColor: Color.theme, width: '100%', justifyContent: 'center', alignItems: 'center'}}>
              <Text style={{color: '#FFFFFF'}}>Kembali</Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
      </SafeAreaView>
    )
  }

  render() {
    let dimensions = Dimensions.get("window");
    let imageHeight = Math.round((dimensions.width * 9) / 16);
    let imageWidth = dimensions.width / 2;  
    const { activeMenu, activeSubMenu, modalSearch}  = this.state;

    return (
      <View style={styles.container}>        
        <ScrollView>
          <View style={styles.boxRow}>
            <View style={styles.rowNewsnya}>
              <FlatList
                keyExtractor={(row, index) => `${index.toString() + row.id_berita}`}
                style={{marginBottom: 32}}
                data={this.state.tempContentBerita}
                renderItem={({ item, index }) => this._RenderBeritaHeadline(item.slug,item.judul,item.gambar_detail,'CARPAN')}
              />
            </View>

            {this.props.screenProps.loading && <Spinner color="green" />}

            <View style={styles.rowNewsnya}>
              <FlatList
                keyExtractor={(row, index) => `${index.toString() + row.id_berita}`}
                style={{marginBottom: 32}}
                data={this.state.tempContentBerita2}
                renderItem={({ item, index }) => this._RenderBeritaHeadline(item.slug,item.judul,item.gambar_detail,'SEMANGAT')}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}

// STYLES
// ------------------------------------
const styles = StyleSheet.create(NewsListStyle);