import React, { Component } from 'react';
import { View, ScrollView, ActivityIndicator, BackHandler } from 'react-native';
import Styled from 'styled-components';
import Accordion from 'react-native-collapsible/Accordion';
import gql from 'graphql-tag';
import Ionicons from 'react-native-vector-icons/Ionicons';

import graphClient from '../state/apollo';
import Text from './Text';
import Header from './Header';
import Color from './Color';
import FormatMoney from './FormatMoney';
import Button from './Button';
import fetchApi from '../state/fetchApi';

const BaseHorizontalPadding = 16;

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    flexDirection: column;
    backgroundColor: #F4F4F4;
`;

const LoadingActivityView = Styled(View)`
    width: 100%;
    height: 100%;
    justifyContent: center;
    alignItems: center;
    backgroundColor: #FFFFFF;
`;

const BlackBoxContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    paddingHorizontal: ${BaseHorizontalPadding};
    marginVertical: 20;
`;

const BlackBoxView = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: #231F20;
    padding: 10px 10px 10px 10px;
    borderRadius: 3;
`;

const InformationView = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: #FFFFFF;
    paddingHorizontal: ${BaseHorizontalPadding};
    paddingVertical: 10;
    marginBottom: 10;
    flexDirection: column;
`;

const OneLineInformationView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    paddingVertical: 5;
    justifyContent: space-between;
`;

const OneInformation = Styled(View)`
    minWidth: 1;
    minHeight: 1;
`;

const HeaderSectionView = Styled(View)`
    width: 100%;
    height: 50;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
    backgroundColor: white;
    paddingHorizontal: ${BaseHorizontalPadding};
    borderBottomWidth: 2;
    borderColor: #F4F4F4;
`;

const HeaderSectionLeft = Styled(View)`
    minWidth: 1;
    height: 100%;
    justifyContent: center;
`;

const HeaderSectionRight = Styled(HeaderSectionLeft)`
    alignItems: center;
    justifyContent: space-between;
    flexDirection: row;
`;

const NormalText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const WhiteText = Styled(Text)`
    fontSize: 12;
    color: #FFFFFF;
    lineHeight: 18;
`;

const PriceText = Styled(Text)`
    fontSize: 12;
    color: #FF425E;
`;

const IconArrow = Styled(Ionicons)`
    color: #231F20;
    fontSize: 10;
    marginLeft: 8;
`;

const getPaymentMethodsQuery = gql`
  query(
    $bookingId: Int!
  ) {
   paymentMethods(
     bookingId: $bookingId
   ) {
     items { id name }
   }
 }`;

const queryGetBookingDetailPrice = gql`
  query(
    $bookingId: Int!
  ) {
   bookingDetail(
     bookingId: $bookingId
   ) {
      id amount discount vat invoiceNumber
      bookingStatus { id }
      payments { discount transactionFee finalAmount type { id } }
      contact { firstName lastName email phone countryCode }
   }
 }`;

const ButtonView = Styled(View)`
    width: 100%
    alignItems: center
    justifyContent: center
    flexDirection: row
    backgroundColor: #FFFFFF
    padding: 48px 0px
    minHeight: 1px
`;

const ButtonRadius = Styled(Button)`
    width: 80%;
    height: 50px;
`;

const ButtonText = Styled(Text)`
    fontSize: 14px
    color: #FFFFFF
    lineHeight: 34px
`;
export default class PaymentSucceed extends Component {

  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    this.state = {
      informations: null,
      paymentMethods: null,
      bookingDetail: null,
      detailPrice: null,
    };
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    this.getPaymentMethods();
    this.paymentTransaction(this.props.navigation.state.params.bookingId);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
  }

  paymentTransaction(booking_id) {
    const body = { booking_id };
    const headers = { 'Content-Type': 'application/x-www-form-urlencoded' };

    fetchApi.post('transaksi/payment', body, headers)
    .then(res => {
      console.log(res, 'res payment transaction');
    })
    .catch(err => {
      console.log(err, 'err payment transaction');
    })
  }

  handleBackPress = () => {
    this.props.navigation.popToTop();
    return true;
  }

  getPaymentMethodName(bookingDetail) {
    for (const paymentMethod of this.state.paymentMethods) {
      for (const item of paymentMethod.items) if (item.id === bookingDetail.payments[0].type.id) return item.name;
    }
  }

  getDetailPrice() {
    const { amount, vat, payments } = this.state.bookingDetail;
    let { discount } = this.state.bookingDetail;
    if (discount > 0) discount = -discount;
    if (payments[0].discount > 0) discount -= payments[0].discount;
    else discount += payments[0].discount;

    const detailPrice = [];
    detailPrice.push({
      title: 'Rincian Bayar',
      informations: [
        { label: 'Sub Total', value: FormatMoney.getFormattedMoney(amount) },
        { label: 'Transaction Fee', value: payments[0].transactionFee === 0 ? 'Gratis' : FormatMoney.getFormattedMoney(payments[0].transactionFee) },
        { label: 'Discount', value: FormatMoney.getFormattedMoney(discount) },
        { label: 'Ppn', value: FormatMoney.getFormattedMoney(vat) },
        { label: 'Total', value: FormatMoney.getFormattedMoney(payments[0].finalAmount) }
      ]
    });
    this.setState({ detailPrice });
  }

  getPaymentMethods() {
    const variables = { bookingId: this.props.navigation.state.params.bookingId };
    graphClient
      .query({ query: getPaymentMethodsQuery, variables })
      .then(res => {
        if (res.data.paymentMethods) {
          this.setState({ paymentMethods: res.data.paymentMethods }, () => this.getBookingDetail());
        }
      }).catch(reject => {
          // console.log(reject);
      });
  }

  getBookingDetail() {
    const variables = { bookingId: this.props.navigation.state.params.bookingId };
    graphClient
      .query({ query: queryGetBookingDetailPrice, variables })
      .then(res => {
        if (res.data.bookingDetail) {
          const informations = [];
          informations.push({ label: 'Metode Pembayaran', value: this.getPaymentMethodName(res.data.bookingDetail) === 'Vesta Point' ? 'Siago Pay' : this.getPaymentMethodName(res.data.bookingDetail)});
          informations.push({ label: 'Order ID', value: res.data.bookingDetail.invoiceNumber });
          informations.push({ label: 'Total Tagihan', value: FormatMoney.getFormattedMoney(res.data.bookingDetail.payments[0].finalAmount) });
          informations.push({ label: 'Nama Pemesan', value: `${res.data.bookingDetail.contact.firstName} ${res.data.bookingDetail.contact.lastName}` });
          informations.push({ label: 'E-mail', value: res.data.bookingDetail.contact.email });
          informations.push({ label: 'Nomor Telepon', value: `+${res.data.bookingDetail.contact.countryCode}${res.data.bookingDetail.contact.phone}` });
          this.setState({ informations, bookingDetail: res.data.bookingDetail }, () => this.getDetailPrice());
        }
      }).catch(reject => {
          // console.log(reject);
    });
  }

  renderAccordionDetailPriceHeader = (section, index, isActive) => {
    const icon = isActive ? 'ios-arrow-down' : 'ios-arrow-forward';
    return (
      <HeaderSectionView>
        <HeaderSectionLeft><NormalText type='bold'>{section.title}</NormalText></HeaderSectionLeft>
        <HeaderSectionRight>
          <PriceText type='bold'>{FormatMoney.getFormattedMoney(this.state.bookingDetail.payments[0].finalAmount)}</PriceText>
          <IconArrow name={icon} />
        </HeaderSectionRight>
      </HeaderSectionView>
    );
  }

  renderAccordionDetailPriceSection = (section) => (
    <InformationView>
      {this.renderInformations(section.informations)}
    </InformationView>
  )

  renderInformations(informations) {
    return informations.map((info, i) =>
      <OneLineInformationView key={i}>
        <OneInformation><NormalText type='medium'>{info.label}</NormalText></OneInformation>
        <OneInformation><NormalText type='medium'>{info.value}</NormalText></OneInformation>
      </OneLineInformationView>
    );
  }

  renderLoadingIndicator() {
    return (
      <LoadingActivityView>
        <ActivityIndicator size='large' color={Color.loading} />
      </LoadingActivityView>
    );
  }

  render() {
    console.log(this.props, 'payment succedd');
    
    const { detailPrice, informations } = this.state;
    if (!detailPrice) return this.renderLoadingIndicator();
    return (
      <MainView>
        <Header title='Pembayaran Berhasil' showLeftButton onPressLeftButton={() => this.props.navigation.popToTop()} />
        <ScrollView>
          <BlackBoxContainer>
            <BlackBoxView>
              <WhiteText type='bold'>Pembayaran Anda berhasil.{'\n'}Terima kasih telah melakukan pembayaran melalui SIAGO.</WhiteText>
            </BlackBoxView>
          </BlackBoxContainer>
          <InformationView>
            {this.renderInformations(informations)}
          </InformationView>
          <Accordion
            touchableProps={{ activeOpacity: 1 }}
            sections={this.state.detailPrice}
            renderHeader={this.renderAccordionDetailPriceHeader}
            renderContent={this.renderAccordionDetailPriceSection}
          />
          <ButtonView>
            <ButtonRadius onPress={() => this.props.navigation.popToTop()}>Kembali Ke Beranda</ButtonRadius>
          </ButtonView>
        </ScrollView>
      </MainView>
    );
  }

}
