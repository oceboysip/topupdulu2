/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {Alert,Platform, StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions, Button} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import SearchHotelStyle from '../style/SearchHotelStyle';


export default class SearchHotelScreen extends Component<{}> {
  BookedHotel = () => {
    this.props.navigation.navigate('BookedHotel');
  }
  render() {
    return (
      <View style={styles.container}>

        <View style={styles.navTop}>
          <View style={styles.rowsBeetwen}>
            <View style={{flexDirection: 'row',alignItems:'center'}}>
              <TouchableOpacity onPress={this.BookedHotel}>
                <Image style={{width:30, height:30,resizeMode:'contain'}} source={require('../images/left-arrow.png')}/>
              </TouchableOpacity>
              <Text style={styles.titlePage}>Cari Hotel</Text>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.searchRow}>
              <TextInput placeholder="Cari Hotel" style={styles.searchInput} />
              <Image style={{width:30, height:30,resizeMode:'contain',position:'absolute', left:40,top:4}} source={require('../images/icon_search.png')}/>
            </View>
          </View>

        </View>

        
        <ScrollView>
          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  
                  <View style={styles.captionForm}>
                    <Text style={styles.smallText}>Ambarawa</Text>
                  </View>

                  <View style={styles.iconForm}>
                    <Image style={{width:25, height:34, resizeMode:'contain'}}  source={require('../images/icon_place.png')}/>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  
                  <View style={styles.captionForm}>
                    <Text style={styles.smallText}>Padang Sidempuan</Text>
                  </View>

                  <View style={styles.iconForm}>
                    <Image style={{width:25, height:34, resizeMode:'contain'}}  source={require('../images/icon_place.png')}/>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  
                  <View style={styles.captionForm}>
                    <Text style={styles.smallText}>Ambon</Text>
                  </View>

                  <View style={styles.iconForm}>
                    <Image style={{width:25, height:34, resizeMode:'contain'}}  source={require('../images/icon_place.png')}/>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  
                  <View style={styles.captionForm}>
                    <Text style={styles.smallText}>BANDUNG</Text>
                  </View>

                  <View style={styles.iconForm}>
                    <Image style={{width:25, height:34, resizeMode:'contain'}}  source={require('../images/icon_place.png')}/>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  
                  <View style={styles.captionForm}>
                    <Text style={styles.smallText}>Anyer</Text>
                  </View>

                  <View style={styles.iconForm}>
                    <Image style={{width:25, height:34, resizeMode:'contain'}}  source={require('../images/icon_place.png')}/>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  
                  <View style={styles.captionForm}>
                    <Text style={styles.smallText}>CIREBON</Text>
                  </View>

                  <View style={styles.iconForm}>
                    <Image style={{width:25, height:34, resizeMode:'contain'}}  source={require('../images/icon_place.png')}/>
                  </View>
                </View>

              </View>
            </View>
          </View>

          
          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  
                  <View style={styles.captionForm}>
                    <Text style={styles.smallText}>CIREBON</Text>
                  </View>

                  <View style={styles.iconForm}>
                    <Image style={{width:25, height:34, resizeMode:'contain'}}  source={require('../images/icon_place.png')}/>
                  </View>
                </View>

              </View>
            </View>
          </View>

        </ScrollView>
      </View>
    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}




// STYLES
// ------------------------------------
const styles = StyleSheet.create(SearchHotelStyle);