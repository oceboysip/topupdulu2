import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import Accordion from 'react-native-collapsible/Accordion';
import Styled from 'styled-components';
import Icon from 'react-native-vector-icons/Ionicons';

import Text from '../Text';
import Header from '../Header';
import { Row, Col } from '../Grid';

const MainView = Styled(View)`
  width: 100%
  height: 100%
  alignItems: center;
  backgroundColor: #FFFFFF;
`;

const CustomHeader = Styled(Header)`
  height: 60;
`

const ContentView = Styled(ScrollView)`
  height: 100%;
  paddingHorizontal: 16;
  paddingVertical: 12;
  borderBottomColor: #FAF9F9;
  borderBottomWidth: 8;
`;

const HeaderContent = Styled(View)`
  width: 100%;
  minHeight: 50;
  backgroundColor: #FFFFFF;
  flexDirection: row;
  padding: 8px 16px 8px 16px;
  alignItems: center;
`;

const HeaderContentLeft = Styled(View)`
   width: 90%;
   minHeight: 1;
   alignItems: flex-start;
`;

const HeaderContentRight = Styled(HeaderContentLeft)`
   width: 10%;
   alignItems: flex-end;
`;

const IconArrow = Styled(Icon)`
    fontSize: 14;
    color: #231F20;
`;

const AccordionDescriptionView = Styled(View)`
   width: 100%;
   minHeight: 1;
   paddingHorizontal: 16
`;

const TitleText = Styled(Text)`
  fontSize: 12px;
  lineHeight: 16;
  textAlign: left;
`;

const ContentText = Styled(Text)`
  fontSize: 12px;
  lineHeight: 20;
  textAlign: left
`

class DetailAboutAttraction extends Component {
  static navigationOptions = { header: null };

  renderAccordionContent = (item) => {
    const { data } = this.props.navigation.state.params;

    return(
      <AccordionDescriptionView>
        <View style={{borderTopWidth: 1, borderColor: '#E8E3E3'}}>
          {item.desc === data.AttractionsProductTypes.cancelationpolicies ?
            item.desc.map((itemDesc, idx) =>
              <Row key={idx} style={{paddingVertical: 12}}>
                <Col size={5}>
                  <ContentText type='medium'>{itemDesc.numberOfDays} hari sebelumnya</ContentText>
                </Col>
                <Col size={7} style={{alignItems: 'flex-end'}}>
                  <ContentText type='medium'>Dana Pengembalian : {itemDesc.refundPercentage}%</ContentText>
                </Col>
              </Row>
            )
            :
            <ContentText type='medium' style={{paddingVertical: 12}}>{item.desc}</ContentText>
          }
        </View>
      </AccordionDescriptionView>
    )
  }

  renderAccordionHeader = (item, index, isActive) => {
    let iconArrow = 'ios-arrow-forward';
    if (isActive) iconArrow = 'ios-arrow-down';

    return(
      <HeaderContent>
        <HeaderContentLeft>
          <TitleText type='semibold'>{item.title}</TitleText>
        </HeaderContentLeft>
        <HeaderContentRight>
          <IconArrow name={iconArrow} />
        </HeaderContentRight>
      </HeaderContent>
    )
  }

  render(){
    const { data } = this.props.navigation.state.params;

    dataDetail = [
      [{
        title: 'Voucher',
        desc: data.AttractionsProductTypes.voucheruse
      }],
      [{
        title: 'Lokasi Pengambilan Voucher',
        desc: data.AttractionsProductTypes.voucherredemtionaddress
      }],
      [{
        title: 'Pengaturan Pembatalan',
        desc: data.AttractionsProductTypes.cancelationpolicies
      }]
    ]

    return(
      <MainView>
        <CustomHeader title={data.AttractionsProductTypes.title} showLeftButton showIconLeftButton />
        <ScrollView>
          <ContentView>
            <ContentText type='medium'>See details Make your own way to the Chinatown Visitor Centre. For the complimentary transfer, please wait at the designated lobby of the selected hotel at least 15 minutes before the stated pick-up time.</ContentText>
          </ContentView>
          {dataDetail.map((item, index) =>
            <View key={index}>
              {item[0].desc.length === 0 || item[0].desc === 'null' ?
              <View />
              :
              <View style={{borderBottomColor: '#FAF9F9', borderBottomWidth: 8}}>
                <Accordion
                  key={index}
                  touchableProps={{activeOpacity: 1}}
                  sections={item}
                  renderHeader={this.renderAccordionHeader}
                  renderContent={this.renderAccordionContent}
                />
              </View>}
            </View>
          )}
        </ScrollView>
      </MainView>
    )
  }
}

export default DetailAboutAttraction;
