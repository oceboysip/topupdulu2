import React, { Component } from 'react';
import { View, Modal } from 'react-native';
import Styled from 'styled-components';
import { connect } from 'react-redux';

import Text from '../Text';
import Color from '../Color';
import Header from '../Header';
import { Row, Col } from '../Grid';
import FormatMoney from '../FormatMoney';
import TouchableOpacity from '../Button/TouchableDebounce';
import ModalAttractionTime from '../Modal/ModalTimeAttraction';

const CardTicket = Styled(TouchableOpacity)`
    flex; 1;
    margin: 22px 0px 8px
    width: 100%;
    flexDirection: column;
    alignItems: center;
`;

const MenuText = Styled(Text)`
    textAlign: left
`;

const RedText = Styled(MenuText)`
    color: #FF425E
`;

const BaseText = Styled(MenuText)`
    fontSize: 12px
`;

const WhiteText = Styled(BaseText)`
    color: #FFFFFF
    padding: 5px 21px
`;

const SmallText = Styled(MenuText)`
    fontSize: 10px
`;

const MenuView = Styled(TouchableOpacity)`
    backgroundColor: #FFFFFF
    margin: 0px 8px
    borderRadius: 3px
    shadow-color: #000;
    shadow-radius: 2;
    elevation: 2;
`;

const ContentContainer = Styled(View)`
    margin: 9px 8px 13px
`;

const ButtonText = Styled(Text)`
    textAlign: left
    fontSize: 12px
    color: #231F20
    padding: 5px 10px
`;

const IconView = Styled(TouchableOpacity)`
    marginTop: 4px
    flexDirection: row
    alignItems: flex-start
    justifyContent: flex-start
`;

const CardBorderView = Styled(View)`
    borderBottomColor: #707070
    borderBottomWidth: 1px
    paddingBottom: 11px
    marginBottom: 5px
`;

const HeaderRowRight = Styled(Col)`
    alignItems: flex-end
`;

const HeaderRowLeft = Styled(Col)`
    alignItems: flex-start
    justifyContent: center
`;

const ButtonHeaderLeft = Styled(TouchableOpacity)`
    borderRadius: 30px
    minWidth: 1px
    backgroundColor: ${Color.theme}
`;
const ButtonHeaderRight = Styled(ButtonHeaderLeft)`
    backgroundColor: #231F20
`;

class CardTicketAttraction extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
      headerTitle: <Header title='Cari Tiket' showLeftButton {...screenProps} />,
      headerLeft: null,
      headerRight: null
  });

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      modalTime: false,
      render: false
    };

  }

  closeModal = (name) => {
    this.setState({ [name]: false })
  }

  openModal = (name) => {
    this.setState({ [name]: true })
  }

  onSelectFilter = (time, index) => {
    this.props.onSelectFilter(time, index)
    this.setState({ render: true })
  }

  submit(item){
    this.props.submit(item);
  }

  render() {
    const { item } = this.props;
    const { minpax, maxpax, minchildren, maxchildren } = item.AttractionsProductTypes;
    const { navigate } = this.props.navigation;

    return (
      <CardTicket>
        <MenuView>
          <ContentContainer>
            <CardBorderView>
              <BaseText type='bold'>{item.AttractionsProductTypes.title}</BaseText>
              <IconView>
                <HeaderRowLeft size={5}>
                  <RedText type='bold'>{FormatMoney.getFormattedMoney(item.amount)}</RedText>
                  <SmallText type='medium'>Harga {minpax} Dewasa {minchildren === 0 ? '' : minchildren + ' Anak'}</SmallText>
                </HeaderRowLeft>
                <HeaderRowRight size={7}>
                  <ButtonHeaderRight activeOpacity={0.5} onPress={() => this.submit(item)}><WhiteText type='medium'>Pesan</WhiteText></ButtonHeaderRight>
                </HeaderRowRight>
              </IconView>
            </CardBorderView>
              <IconView>
                <HeaderRowLeft size={5}>
                  <SmallText type='medium' style={{ paddingTop: 5 }}>Waktu Aktifitas</SmallText>
                </HeaderRowLeft>
                <HeaderRowRight size={7}>
                  {item.timeslots.length === 0 && <ButtonHeaderLeft><ButtonText type='medium'>Waktu berlaku seharian</ButtonText></ButtonHeaderLeft>}
                  {item.timeslots.length === 1 && <ButtonHeaderLeft><ButtonText type='medium'>{item.timeslots[0].startTime.substring(0, 5)} - {item.timeslots[0].endTime.substring(0, 5)}</ButtonText></ButtonHeaderLeft>}
                  {(item.timeslots.length > 1 && this.props.time !== null && this.props.index === this.props.idx) && <ButtonHeaderLeft activeOpacity={0.8} onPress={() => this.openModal('modalTime')}><ButtonText type='medium'>{this.props.time.startTime.substring(0, 5)} - {this.props.time.endTime.substring(0, 5)}</ButtonText></ButtonHeaderLeft>}
                {(item.timeslots.length > 1 && this.props.time === null && this.props.index === this.props.idx) && <ButtonHeaderLeft activeOpacity={0.8} onPress={() => this.openModal('modalTime')}><ButtonText type='medium'>Pilih Waktu</ButtonText></ButtonHeaderLeft>}
              </HeaderRowRight>
              </IconView>
              <TouchableOpacity onPress={() => navigate('DetailAboutAttraction', { data: item })}>
              <BaseText type='medium' style={{textDecorationLine: 'underline'}}>Detail</BaseText>
              </TouchableOpacity>
          </ContentContainer>
        </MenuView>
        <Modal
            visible={this.state.modalTime}
            animationType="slide"
            onRequestClose={() => this.closeModal('modalTime')}
        >
          <ModalAttractionTime
              tiket={item}
              index={this.props.index}
              onSelectFilter={this.onSelectFilter}
              onClose={this.closeModal}
          />
        </Modal>
      </CardTicket>
    );
  }
}


const mapStateToProps = state => {
  return {
    user: state['user.auth'].login.user,
    error: state['user.auth'].error,
  };
};
  export default connect(mapStateToProps, null)(CardTicketAttraction);
