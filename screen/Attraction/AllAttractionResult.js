import React, { Component } from 'react';
import { Image, ActivityIndicator, View, FlatList, ScrollView, Modal, Dimensions } from 'react-native';
import Styled from 'styled-components';
import { connect } from 'react-redux';
import Ionicons from 'react-native-vector-icons/Ionicons'
import gql from 'graphql-tag';

import Client from '../../state/apollo';
import TouchableOpacity from '../Button/TouchableDebounce';
import Button from '../Button';
import CardAttraction from './CardPopularAttractions';
import Text from '../Text';
import Color from '../Color';
import Header from '../Header';

const { width, height } = Dimensions.get('window');

const MainView = Styled(View)`
  width: 100%;
  height: 100%;
  flexDirection: column
  backgroundColor: #FFFFFF;
`;

const CustomHeader = Styled(Header)`
  height: 60;
`;

const ButtonText = Styled(Text)`
  textAlign: left
  fontSize: 12px
  color: #231F20
`;

const ContentView = Styled(ScrollView)`
  backgroundColor: #FFFFFF
  padding: 16px 0px
`;

const CategoryView = Styled(View)`
  flexDirection: row
  justifyContent: space-between
  marginHorizontal: 10px
`;

const TabView = Styled(TouchableOpacity)`
  height: 24px
  borderRadius: 30px
  paddingHorizontal: 9px
  justifyContent: center
  alignItems: center
  marginHorizontal: 6
`;

const WarningView = Styled(View)`
  alignItems: center;
  justifyContent: center;
  paddingHorizontal: 20;
  borderTopWidth: 8;
  borderTopColor: #FAF9F9;
`;

const WarningImage = Styled(Image)`
  width: 85.33;
  height: 96;
  marginTop: 60;
`;

const TitleWarningText = Styled(Text)`
  fontSize: 18;
  lineHeight: 24;
  marginTop: 40;
`;

const InfoWarningText = Styled(Text)`
  lineHeight: 20;
  marginTop: 10;
`;

const WarningButton = Styled(Button)`
  height: 40;
  width: 150;
  marginTop: 15;
`;

const iconNoResult = require('../../images/warning-atraksi.png')

class AllAttractionResult extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;

    this.state = {
      activeTab: params.tab,
      itemAtt: [],
      page: 1,
      ready: false
    }

    this.getAttraction(params.category, params.destination)
  }

  getAttraction = (category, destination) => {
    let variables = {
      param: {
        destination: destination,
        itemPerPage: 10,
        page: this.state.page,
        filter:{
          category: category
        },
        sort: {
          popular: 'desc'
        },
      }
    }

    const getAttraction = gql`
      query(
        $param: paramAttraction!
      ){
        attractions(
          param: $param
        ){
          AttractionProducts{
            uuid
            title
            AttractionsProductsDetail {
              typename
              AttractionConfig {
                name
                value
              }
              baseprice
              currency{
                code
              }
              locations{
                city
                country
              }
              photos {
                path
              }
            }
          }
        }
      }
    `;

    Client.query({
      query: getAttraction,
      variables
    }).then(res => {
      if (res.data.attractions.AttractionProducts) {
        // data.push(res.data.attractions.AttractionProduct)
        this.setState({
          itemAtt: res.data.attractions.AttractionProducts,
          ready: true
        });
      }
    }).catch(reject =>{
        console.log(reject, 'error')
    })
  }

  getAttractionAgain = (name, data, tab) => {
    this.setState({
      page: this.state.page + 1
    },() => this.getAttraction(name, data))
  }

  selectedTabs(name, data, tab){
    if (name === 'All') name = ''

    this.setState({ready: false, activeTab: tab, page: 1},
    () => this.getAttraction(name, data, tab))
  }

  submit = (item) => {
    this.props.navigation.navigate('DetailAttraction', { data: item })
  }

  renderItem = ({ item, index }) => (
    <CardAttraction data={item}/>
  )

  renderTabs(itemAtt, tabs, categories) {
    if (itemAtt.length === 0) {
      return(
        <WarningView>
          <WarningImage source={iconNoResult} />
          <TitleWarningText type='bold'>Atraksi Tidak Tersedia</TitleWarningText>
          <InfoWarningText>Reset filter untuk menampilkan semua atraksi yang tersedia.</InfoWarningText>
          <WarningButton onPress={() => this.props.navigation.pop()}>Kembali</WarningButton>
        </WarningView>
      )
    }else {
      return (
        <View style={{paddingHorizontal: 8, marginBottom: 8}}>
          <FlatList
            ref="flatList"
            extraData={this.state}
            data={itemAtt}
            renderItem={this.renderItem}
            onEndReachedThreshold={0.1}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      );
    }
  }
  // onEndReached={() => this.getAttractionAgain(params.category, params.destination, this.state.activeTab)}

  render() {
    const { activeTab, itemAtt } = this.state;
    const { params } = this.props.navigation.state;

    return (
      <MainView>
        <CustomHeader showLeftButton showIconLeftButton title={params.title} />
        <ScrollView>
          <ContentView horizontal={true} showsHorizontalScrollIndicator={false}>
            <CategoryView>
            {params.categories.map((itemCategories, idx) =>
              <TabView
                key={idx} activeOpacity={0.5}
                onPress={() => this.selectedTabs(itemCategories.name, params.destination, idx)}
                style={[activeTab===idx ? {backgroundColor: '#231F20'} : {backgroundColor: Color.theme}]}
              >
                <ButtonText style={[activeTab===idx ? {color: '#FFFFFF'} : {color:'#000000'}]} type='medium'>{itemCategories.label}</ButtonText>
              </TabView>
            )}
            </CategoryView>
          </ContentView>
          {this.state.ready ? this.renderTabs(itemAtt, activeTab, params.categories)
            :
            <View style={{ height: height, flexDirection: 'column', alignItems: 'center', justifyContent: 'flex-start', paddingHorizontal: 16}}>
              <ActivityIndicator size="large" color={Color.loading} />
          </View>}
        </ScrollView>
      </MainView>
    );
  }
}

export default AllAttractionResult;
