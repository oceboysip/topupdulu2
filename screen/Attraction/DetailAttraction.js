import React, { Component } from 'react';
import { View, ScrollView, ActivityIndicator, Animated, Modal, Dimensions, Platform, Image, TouchableOpacity as Touchable } from 'react-native';
import Styled from 'styled-components';
import Icon from 'react-native-vector-icons/MaterialIcons';
import gql from 'graphql-tag'
import Moment from 'moment';
import { Tab, TabHeading, Tabs } from 'native-base';
import Carousel from 'react-native-banner-carousel';

import TouchableOpacity from '../Button/TouchableDebounce';
import Client from '../../state/apollo'
import ModalDatePicker from '../CalendarScreen';
import FormatMoney from '../FormatMoney';
import Color from '../Color';
import Description from './TabDetail/Description';
import Information from './TabDetail/Information';
import Activity from './TabDetail/Activity';
import Header from '../Header';
import Text from '../Text';
import { Row, Col } from '../Grid';

const MainView = Styled(View)`
  width: 100%;
  height: 100%;
`;

const UpperMainView = Styled(View)`
  flex: 1;
  width: 100%;
`;

const CorouselView = Styled(TouchableOpacity)`
  width: 100%;
  height: 260;
`;
const CorouselImage = Styled(Image)`
  width: 100%;
  height: 100%;
`;

const SubMainView = Styled(View)`
  width: 100%;
  height: 100%;
  top: -50;
  flexDirection: column;
`;

const MainInfoContainer = Styled(View)`
  width: 100%;
  minHeight: 1;
  backgroundColor: transparent;
  paddingHorizontal: 16;
  marginBottom: 16
`;

const MainInfoView = Styled(View)`
  width: 100%;
  minHeight: 1;
  backgroundColor: #FFFFFF;
  padding: 12px 12px 16px 12px;
  borderRadius: 3;
  elevation: 5;
  flexDirection: column;
  borderWidth: ${Platform.OS === 'ios' ? 1 : 0 };
  borderBottomWidth: ${Platform.OS === 'ios' ? 2 : 0 };
  borderColor: #DDDDDD;
`;

const HeaderTitle = Styled(View)`
  borderBottomColor: #707070
  borderBottomWidth: 1px
  paddingBottom: 10px
  marginBottom: 10px
`;

const NormalText = Styled(Text)`
  textAlign: left
  color: #000000
`;

const BaseText = Styled(NormalText)`
  fontSize: 12
`;

const SmallText = Styled(NormalText)`
  fontSize: 10px;
`;

const LeftHeaderView = Styled(View)`
  width: 30%
  justifyContent: flex-start
  alignItems: flex-start
`;

const LeftView = Styled(View)`
  width: 70%
  justifyContent: flex-start
  alignItems: flex-end
`;

const ButtonText = Styled(Text)`
  fontSize: 14px
  color: #FFFFFF
  lineHeight: 34px
`;

const PriceText = Styled(Text)`
  color: #FF425E
`;

const ButtonRadius = Styled(Touchable)`
  borderRadius: 30px;
  width: 130
  backgroundColor: #231F20;
  height: 45
  alignItems: center
  justifyContent: center
`;

const ButtomView = Styled(View)`
  width: 100%
  backgroundColor: #FFFFFF
  alignItems: center
  justifyContent: center
  flexDirection: row
  elevation: 2
`;

const SubtotalView = Styled(View)`
  width: 100%
  height: 63
  flexDirection: row
  justifyContent: space-between
  alignItems: center
  backgroundColor: #FFFFFF
  padding: 8px 16px 8px
  elevation: 20
`;

const CustomHeader = Styled(Header)`
  position: absolute
`;

const AbsoluteCustomHeader = Styled(Animated.View)`
  width: 100%;
  alignItems: center;
  zIndex: 1;
  position: absolute;
  paddingHorizontal: 16;
  paddingVertical: 10;
`;

const ContainerOptionView = Styled(Row)`
  flexDirection: row;
  paddingVertical: 10;
  justifyContent: space-between;
  alignSelf: stretch;
  alignItems: center;
`;

const ColumnView = Styled(Col)`
  justifyContent: center;
  alignItems: center;
`;

const SideButton = Styled(TouchableOpacity)`
  flex: 1;
  width: 100%;
  alignItems: center;
  justifyContent: center;
`;

const SideIcon = Styled(Icon)`
  fontSize: 23;
  color: ${Color.text};
`;

const AnimatedTitleText = Styled(Animated.Text)`
  textShadowRadius: 0;
  fontSize: 17;
  borderRadius: 15;
  fontWeight: bold;
  paddingVertical: 5;
  paddingLeft: 16;
  paddingHorizontal: 10;
  textTransform: uppercase;
`;

const TextHeading = Styled(Text)`
  fontSize: 12px;
`;

const RowCol = Styled(Col)`
  justifyContent: center;
`;


const { width, height } = Dimensions.get('window');

let photoTemp = [];
let obj = {};
const imgLocation = require('../../images/location.png');
const imgWatch = require('../../images/watch.png');
const imgUserPax = require('../../images/users-group.png');
const imgHourGlass = require('../../images/hour-glass.png');
const imgInformation = require('../../images/information.png');

export default class DetailAttraction extends Component {

  static navigationOptions = { header: null };
  _animatedValue;

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      attraction: [],
      activeTab: 0,
      modalPhotoViewer: false,
      selected: 0,
      headerHeight: 0,
      modalDatePicker: false,
      departureDate: Moment(),
      dateAttraction: '',
    };

    this.getDetail();
    this._animatedValue = new Animated.Value(0);
  }

  componentWillMount() {
    this.props.navigation.setParams({
      animatedValue: this._animatedValue.interpolate({
        inputRange: [0, 75],
        outputRange: ['transparent', Color.theme],
        extrapolate: 'clamp'
      }),
      animatedText: this._animatedValue.interpolate({
        inputRange: [0, 75],
        outputRange: ['transparent', Color.text],
        extrapolate: 'clamp'
      }),
      animatedWhite: this._animatedValue.interpolate({
        inputRange: [0, 75],
        outputRange: ['transparent', '#FFFFFF'],
        extrapolate: 'clamp'
      }),
    });
  }

  getDetail(){
    let variables = {
      param:{
        uuid: this.props.navigation.state.params.data.uuid,
      }
    }
    const getAttraction = gql`
      query(
        $param: paramAttraction!
      ){
        attractionDetail(
          param: $param
        ){
          uuid
          title
          bussinesshoursto
          bussinesshoursfrom
          minpax
          maxpax
          highlights
          longitude
          latitude
          description
          additionalinfo
          priceexcludes
          AttractionsProductTypes{
            durationhours
            title
          }
          AttractionConfig {
            name
            value
          }
          baseprice
          currency{
            code
          }
          locations{
            city
            country
          }
          photos {
            path
          }
        }
      }
    `;
    Client.query({
      query: getAttraction,
      variables
    }).then(res => {
      if(res.data.attractionDetail){
        let imageConfigsmall = null;
        const imageConfig = this.searchImageConfig(res.data.attractionDetail);

        for(let i = 0; i < res.data.attractionDetail.photos.length ; i++){
          let thumbNail = imageConfig + res.data.attractionDetail.photos[i].path;
          let url = imageConfig + res.data.attractionDetail.photos[i].path;
          obj['thumbnailUrl'] = thumbNail;
          obj['url'] = url
          photoTemp.push(obj);
          obj = {};
        }

        this.setState({
          attraction: res.data.attractionDetail,
          loading: false
        })
        // console.log(this.state.attraction)
      }
    }).catch(reject => {
        console.log(reject)
        this.setState({loading: true})
    })
  }

  searchImageConfig(att) {
    for (const config of att.AttractionConfig)
      if (config.name === 'image-url-big') return config.value;
  }

  closeModalPhotoViewer = () => {
    this.setState({ modalPhotoViewer: false });
  }

  openModalPhotoViewer = (i) => {
    this.setState({ selected: i, modalPhotoViewer: true });
  }

  tooglePhotoGallery = () => {
    this.setState({ photoGallery: !this.state.photoGallery });
  }

  onSelectedDate = (date) => {
    this.setState({ departureDate: date, dateAttraction: date.format('DD MMM YYYY'), modalDatePicker: false },
      () => this.props.navigation.navigate('TicketAttractionScreen', { data: this.state.attraction, date: this.state.departureDate } ));
  }

  openModal(modal){
    this.setState({
      [modal]: true
    })
  }

  closeModal(modal){
    this.setState({
      [modal]: false
    })
  }


  renderBackgroundImage(photos, attraction){
    return(
      <Carousel
        autoplay
        autoplayTimeout={5000}
        loop
        index={0}
        pageSize={width}
      >
        {photos.map((item, idx) =>
          <CorouselView key={idx} activeOpacity={0.7} onPress={() => this.openModalPhotoViewer(item)}>
            <CorouselImage resizeMode={'stretch'} source={{ uri: this.searchImageConfig(attraction) + item.path }} />
          </CorouselView>
        )}
      </Carousel>
    )
  }

  renderAbsoluteCustomHeader(attraction, animatedValue, animatedText, animatedWhite) {
    return(
      <AbsoluteCustomHeader style={{ backgroundColor: animatedValue || 'transparent'}}>
        <ContainerOptionView>
          <View style={{alignItems: 'flex-start', width: '10%'}}>
            <TouchableOpacity onPress={() => this.props.navigation.pop()}>
              <Image style={{width: 30, height: 30, resizeMode: 'contain'}} source={require('../../images/left-arrow.png')}/>
            </TouchableOpacity>
          </View>

          <View style={{paddingLeft: 8, width: '90%', alignItems: 'flex-start', justifyContent: 'center'}}>
            <AnimatedTitleText numberOfLines={2} type='bold' style={[animatedText && { color: animatedText }, animatedWhite && { backgroundColor: animatedWhite }]}>{attraction.title}</AnimatedTitleText>
          </View>
        </ContainerOptionView>
      </AbsoluteCustomHeader>
    )
  }

  render() {
    const { attraction, departureDate, dateAttraction, valuePassengers, activeTab, modalDatePicker, loading } = this.state;
    const { data, animatedValue, animatedText, animatedWhite } = this.props.navigation.state.params;

    // console.log(this.props.navigation.state.params, 'paramsDetail');
    // console.log(this._animatedValue, 'props')
    // console.log(this.state, 'state');

    if (loading) {
      return (
        <View style={{ height: '100%', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
          <ActivityIndicator size="large" color={Color.loading} />
        </View>
      );
    }

    return (
      <MainView>
        {attraction && this.renderAbsoluteCustomHeader(attraction, animatedValue, animatedText, animatedWhite)}
        
        <UpperMainView>
        <ScrollView contentContainerStyle={{width: '100%', minHeight: '100%'}} onScroll={Animated.event([{nativeEvent: {contentOffset: {y: this._animatedValue} } }])}>
          {attraction && this.renderBackgroundImage(attraction.photos, attraction)}
          <CustomHeader transparentMode showLeftButton showIconLeftButton />
          <SubMainView>
            <MainInfoContainer>
              <MainInfoView>
                <HeaderTitle>
                  <NormalText lineHeight={16} type='bold'>{attraction.title}</NormalText>
                  <SmallText lineHeight={24}><Image source={imgLocation} style={{width: 5.75, height: 7}}/> {attraction.locations[0].city}, {attraction.locations[0].country}</SmallText>
                </HeaderTitle>
                <Row style={{marginBottom: 5}}>
                  <RowCol size={0.9}>
                    <Image source={imgWatch} style={{width: 7, height: 10}}/>
                  </RowCol>
                  <Col size={11.1}>
                    <BaseText type='medium'>{attraction.bussinesshoursfrom.substring(0, 5)} - {attraction.bussinesshoursto.substring(0, 5)}</BaseText>
                  </Col>
                </Row>
                <Row style={{marginBottom: 5}}>
                  <RowCol size={0.9}>
                    <Image source={imgUserPax} style={{width: 9.1, height: 10}}/>
                  </RowCol>
                  <Col size={11.1}>
                    <BaseText type='medium'>Min {attraction.minpax} pax | Maks {attraction.maxpax} pax</BaseText>
                  </Col>
                </Row>
                <Row style={{marginBottom: 5}}>
                  <RowCol size={0.9}>
                    <Image source={imgHourGlass} style={{width: 9, height: 10}}/>
                  </RowCol>
                  <Col size={11.1}>
                    <BaseText type='medium'>{attraction.AttractionsProductTypes[0].durationhours === 0 ? 'Fullday' : attraction.AttractionsProductTypes[0].durationhours + ' hours'}</BaseText>
                  </Col>
                </Row>
                <Row>
                  <RowCol size={0.9}>
                    <Image source={imgInformation} style={{width: 10, height: 10}}/>
                  </RowCol>
                  <Col size={11.1}>
                    <BaseText type='medium'>Personal expenses Tips Transfer services</BaseText>
                  </Col>
                </Row>
              </MainInfoView>
            </MainInfoContainer>
            <Tabs
              prerenderingSiblingsNumber={Infinity}
              tabStyle={{backgroundColor: 'F4F4F4'}}
              tabContainerStyle={{elevation: 0}}
              tabBarUnderlineStyle={{backgroundColor: '#000000', height: 2}}
              onChangeTab={({i}) => this.setState({ activeTab: i})}
            >
              <Tab
                heading={<TabHeading style={{backgroundColor: '#F4F4F4', justifyContent: 'flex-start', paddingLeft: 16}}><TextHeading lineHeight={24} type={activeTab === 0? 'bold' : 'medium'}>Ringkasan</TextHeading></TabHeading>}>
                {activeTab === 0 && <Information highlights={attraction} />}
              </Tab>
              <Tab
                heading={<TabHeading style={{backgroundColor: '#F4F4F4', justifyContent: 'center'}}><TextHeading lineHeight={24} type={activeTab === 1? 'bold' : 'medium'}>Keterangan</TextHeading></TabHeading>}>
                {activeTab === 1 && <Description description={attraction} />}
              </Tab>
              <Tab
                heading={<TabHeading style={{backgroundColor: '#F4F4F4', justifyContent: 'flex-end', paddingRight: 16}}><TextHeading lineHeight={24} type={activeTab === 2? 'bold' : 'medium'}>Detail Aktivitas</TextHeading></TabHeading>}>
                {activeTab === 2 && <Activity activity={attraction} />}
              </Tab>
            </Tabs>
          </SubMainView>
        </ScrollView>
        </UpperMainView>
        <ButtomView>
          <SubtotalView>
            <LeftHeaderView>
              <Text type='medium'>Mulai dari</Text>
              <PriceText type='bold' color='button'>{FormatMoney.getFormattedMoney(this.state.attraction.baseprice)}</PriceText>
            </LeftHeaderView>
            <LeftView>
              <ButtonRadius activeOpacity={0.5} onPress={() => this.openModal('modalDatePicker')}>
                <ButtonText>Cari Tiket</ButtonText>
              </ButtonRadius>
            </LeftView>
          </SubtotalView>
        </ButtomView>
        <Modal visible={modalDatePicker} animationType='slide' onRequestClose={() => this.closeModal('modalDatePicker')}>
          <ModalDatePicker
            onSelectedDate={this.onSelectedDate}
            onClose={() => this.closeModal('modalDatePicker')}
            selectedDate={departureDate}
            label='Pilih'
            minDate={Moment()}
            maxDate={Moment().add(1, 'Y')}
          />
        </Modal>
      </MainView>
    );
  }
}
