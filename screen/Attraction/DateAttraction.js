import React, { Component } from 'react';
import { View, TouchableOpacity as Touchable } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';
import { connect } from 'react-redux';
import AntDesign from 'react-native-vector-icons/AntDesign';

import TouchableOpacity from '../Button/TouchableDebounce';
import FormatMoney from '../FormatMoney';
import Text from '../Text';
import Color from '../Color';
import Header from '../Header';

const MainView = Styled(View)`
  width: 100%;
  height: 100%;
  flexDirection: column;
  backgroundColor: #FFFFFF;
`;

const BackgroundView = Styled(View)`
  width: 100%;
  height: 66;
  backgroundColor: ${Color.theme};
  position: absolute;
  top: 0;
  left: 0;
`;

const ChildContainer = Styled(View)`
  flex; 1;
  margin: 22px 0px;
  width: 100%;
  flexDirection: column;
  alignItems: center;
`;

const MenuView = Styled(View)`
  backgroundColor: #FFFFFF;
  margin: 0px 16px;
  width: 94%;
  borderRadius: 3px;
  shadow-color: #000;
  shadow-radius: 2;
  elevation: 2;
`;

const MenuText = Styled(Text)`
  textAlign: left;
`;

const BaseText = Styled(MenuText)`
  fontSize: 12px;
`;

const Container = Styled(View)`
  margin: 9px 15px 12px 11px;
`;

const ButtonText = Styled(Text)`
  textAlign: left;
  fontSize: 12px;
  color: #231F20;
  padding: 5px 10px;
`;

const IconView = Styled(TouchableOpacity)`
  marginTop: 8px;
  flexDirection: row;
  alignItems: flex-start;
  justifyContent: flex-start;
`;

const HeaderTicket = Styled(View)`
  borderRadius: 30px;
  minWidth: 1px;
  backgroundColor: ${Color.theme};
  marginRight: 5;
`;

const ContentView = Styled(View)`
  backgroundColor: #FFFFFF;
`;

const PassangerView = Styled(View)`
  width: 100%;
  flexDirection: row;
  justifyContent: space-between;
  paddingVertical: 12;
  paddingHorizontal: 16;
  borderBottomWidth: 1;
  borderBottomColor: #DDDDDD;
`;

const InfoView = Styled(View)`
  width: 70%;
`;

const OperateView = Styled(View)`
  width: 30%;
  flexDirection: row;
  justifyContent: space-between;
`;

const MinView = Styled(Touchable)`
  height: 35px;
  width: 35px;
  backgroundColor: ${Color.theme};
  borderRadius: 3px;
  borderWidth: 1px;
  borderColor: #231F20;
  justifyContent: center;
  alignItems: center;
`;

const PlusView = Styled(MinView)`
`;

const NumberView = Styled(View)`
  justifyContent: center;
`;

const BigText = Styled(MenuText)`
  fontSize: 16px;
`;

const ButtomView = Styled(View)`
  width: 100%;
  backgroundColor: #FFFFFF;
  alignItems: center;
  justifyContent: center;
  flexDirection: row;
  elevation: 2;
  bottom: 0;
  position: absolute;
`;

const SubtotalView = Styled(View)`
  width: 100%;
  height: 63;
  flexDirection: row;
  justifyContent: space-between;
  alignItems: center;
  backgroundColor: #FFFFFF;
  padding: 8px 16px 8px;
  elevation: 20;
`;

const LeftHeaderView = Styled(View)`
  width: 50%;
  height: 100%;
  flexDirection: column;
  alignItems: flex-start;
  justifyContent: center;
`;

const PriceText = Styled(Text)`
  color: #FF425E;
`;

const ButtonRadius = Styled(Touchable)`
  borderRadius: 30px;
  width: 100%;
  backgroundColor: #231F20;
  height: 100%;
  justifyContent: center;
`;

const OperationalIcon = Styled(AntDesign)`
  fontSize: 20;
  color: #231F20;
  alignItems: center;
  justifyContent: center;
`;

class DateAttraction extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    const { minpax, minchildren, minseniors } = this.props.navigation.state.params.data.AttractionsProductTypes;

    this.state = {
      modalDatePicker: false,
      departureDate: Moment(),
      dateAttraction: '',
      valuePassengers: {
        adult: minpax,
        child: minchildren,
        old: minseniors === null ? 0 : minseniors
      }
    };
  }

  closeModal = (name) => {
    this.setState({ [name]: false })
  }

  openModal = (name) => {
    this.setState({ [name]: true })
  }

  onBeforeReviewBooking(subTotal){
    const { user, navigation } = this.props;
    if (user.guest) navigation.navigate('LoginScreen', { loginFrom: 'attr', afterLogin: () => this.submit(subTotal) });
    else this.submit(subTotal);
  }

  submit(subTotal) {
    const { adult, child, old } = this.state.valuePassengers;
    const { navigate, state } = this.props.navigation;
    const { data, title, timeslots, date } = state.params;
    const { enc, AttractionsProductTypes } = data;
    const { options } = AttractionsProductTypes;

    navigate('AttractionBookScreen', {
      attractions: {
        enc,
        date,
        title,
        subTotal,
        timeslots,
        options,
        AttractionsProductTypes,
        adult,
        child,
        old
      }
    });
  }

  addValuePassengers = (name, value, type, minpax, maxpax) => {
    if(type === 'plus') {
      this.setState(prevState => {
      return {
        valuePassengers: {
          ...prevState.valuePassengers,
            [name]: value >= maxpax ? maxpax : value + 1
          }
        };
      });
    } else {
      // this.setState(prevState => {
      // return {
      //   valuePassengers: {
      //     ...prevState.valuePassengers,
      //       [name]: (value === min && name === 'adult') ? min : (value === minpax && (name === 'child' || name === 'old')) ? minpax : Math.abs(value - 1)
      //     }
      //   };
      // });
      this.setState(prevState => {
      return {
        valuePassengers: {
          ...prevState.valuePassengers,
            [name]: value <= minpax ? minpax : value - 1
          }
        };
      });
    }
  }

  render() {
    const { valuePassengers } = this.state;
    const { title, data, date } = this.props.navigation.state.params;
    const { prices } = data;
    const { minpax, maxpax, minadultage, maxadultage, maxchildage, maxchildren, minchildage, minchildren, maxseniorage, maxseniors, allowinfant, allowseniors, allowchildren, minseniorage, minseniors } = data.AttractionsProductTypes

    let isAllowChildren = (allowchildren === 'true')
    let isAllowSeniors = (allowseniors === 'true')
    let disableBtn = { backgroundColor: Color.disabled, borderColor: '#C9C6BE' }
    let minimalAdult = (valuePassengers.adult === minpax)
    let maximalAdult = (valuePassengers.adult === maxpax)
    let minimalChild = (valuePassengers.child === minchildren)
    let maximalChild = (valuePassengers.child === maxchildren)
    let minimalSenior = (valuePassengers.old === minseniors)
    let maximalSenior = (valuePassengers.old === maxseniors)
    let validateOnButton = (valuePassengers.adult === 0)

    const subTotal = (prices.adults * valuePassengers.adult) + (prices.children * valuePassengers.child) + (prices.seniors * valuePassengers.old);

    return (
      <MainView>
        <Header title='Cari Tiket' />
        <ChildContainer>
          <MenuView>
            <Container>
              <MenuText type='bold'>{title}</MenuText>
              <IconView>
                <HeaderTicket>
                  <ButtonText type='medium'>{date.format('DD MMM YYYY')}</ButtonText>
                </HeaderTicket>
                <HeaderTicket>
                  <ButtonText type='medium'>{valuePassengers.adult} Dewasa {valuePassengers.child !== 0 && valuePassengers.child + ' Anak'} {valuePassengers.old !== 0 && valuePassengers.old + ' Lansia'}</ButtonText>
                </HeaderTicket>
              </IconView>
            </Container>
          </MenuView>
        </ChildContainer>
        <ContentView>
          <BaseText type='bold' style={{marginLeft: 16, marginBottom: 12}}>JUMLAH PENGUNJUNG</BaseText>

          <PassangerView>
            <InfoView>
              <BaseText type='medium'>Dewasa ({minadultage}-{maxadultage} thn)</BaseText>
              <BaseText type='medium'>{FormatMoney.getFormattedMoney(prices.adults)}</BaseText>
            </InfoView>
            <OperateView>
              <MinView
                disabled={minimalAdult ? true : false}
                style={minimalAdult && disableBtn}
                activeOpacity={0.5}
                onPress={() => this.addValuePassengers('adult', valuePassengers.adult, 'min', minpax, maxpax)}>
                <OperationalIcon name='minus' style={minimalAdult && {color: '#C9C6BE'}} />
              </MinView>
              <NumberView>
                <BigText type='bold'>{valuePassengers.adult}</BigText>
              </NumberView>
              <PlusView
                disabled={maximalAdult ? true : false}
                style={maximalAdult && disableBtn}
                activeOpacity={0.5}
                onPress={() => this.addValuePassengers('adult', valuePassengers.adult, 'plus', minpax, maxpax)}>
                <OperationalIcon name='plus' style={maximalAdult && {color: '#C9C6BE'}} />
              </PlusView>
            </OperateView>
          </PassangerView>

          {isAllowChildren && <PassangerView>
            <InfoView>
              <BaseText type='medium'>Anak ({minchildage}-{maxchildage} thn)</BaseText>
              <BaseText type='medium'>{FormatMoney.getFormattedMoney(prices.children)}</BaseText>
            </InfoView>
            <OperateView>
              <MinView
                disabled={minimalChild ? true : false}
                style={minimalChild && disableBtn}
                activeOpacity={0.5}
                onPress={() => this.addValuePassengers('child', valuePassengers.child, 'min', minchildren, maxchildren)}>
                <OperationalIcon name='minus' style={minimalChild && {color: '#C9C6BE'}} />
              </MinView>
              <NumberView>
                <BigText type='bold'>{valuePassengers.child}</BigText>
              </NumberView>
              <PlusView
                disabled={maximalChild ? true : false}
                style={maximalChild && disableBtn}
                activeOpacity={0.5}
                onPress={() => this.addValuePassengers('child', valuePassengers.child, 'plus', minchildren, maxchildren)}>
                <OperationalIcon name='plus' style={maximalChild && {color: '#C9C6BE'}}/>
              </PlusView>
            </OperateView>
          </PassangerView>}

        {isAllowSeniors && <PassangerView>
          <InfoView>
            <BaseText type='medium'>Lansia</BaseText>
            <BaseText type='medium'>{FormatMoney.getFormattedMoney(prices.seniors)}</BaseText>
          </InfoView>
          <OperateView>
            <MinView
              disabled={minimalSenior ? true : false}
              style={minimalSenior && disableBtn}
              activeOpacity={0.5}
              onPress={() => this.addValuePassengers('old', valuePassengers.old, 'min', minseniors, maxseniors)}>
              <OperationalIcon name='minus' style={minimalSenior && {color: '#C9C6BE'}} />
            </MinView>
            <NumberView>
              <BigText type='bold'>{valuePassengers.old}</BigText>
            </NumberView>
            <PlusView
              disabled={maximalSenior ? true : false}
              style={maximalSenior && disableBtn}
              activeOpacity={0.5}
              onPress={() => this.addValuePassengers('old', valuePassengers.old, 'plus', minseniors, maxseniors)}>
              <OperationalIcon name='plus' style={maximalSenior && {color: '#C9C6BE'}} />
            </PlusView>
          </OperateView>
        </PassangerView>}

        </ContentView>
        <ButtomView>
          <SubtotalView>
            <LeftHeaderView>
              <Text type='medium'>Subtotal</Text>
              <PriceText type='bold' color='button'>{FormatMoney.getFormattedMoney(subTotal)}</PriceText>
            </LeftHeaderView>
            <LeftHeaderView>
              <ButtonRadius activeOpacity={0.7} disabled={validateOnButton} style={validateOnButton && {backgroundColor: '#DDDDDD'}} onPress={() => this.onBeforeReviewBooking(subTotal)}>
                <Text lineHeight={34} style={{color: '#FFFFFF'}}>Selanjutnya</Text>
              </ButtonRadius>
            </LeftHeaderView>
          </SubtotalView>
        </ButtomView>
      </MainView>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state['user.auth'].login.user,
    error: state['user.auth'].error,
  };
};
//
// const mapDispatchToProps = (dispatch) => (
//   {
//     onRegister: (user) => {
//       dispatch(register(user));
//     }
//   }
// );

  export default connect(mapStateToProps, null)(DateAttraction);
