import React, { Component } from 'react';
import { View, Image } from 'react-native';
import Styled from 'styled-components';
import { withNavigation } from 'react-navigation';

import Text from '../Text';
import FormatMoney from '../FormatMoney';
import TouchableOpacity from '../Button/TouchableDebounce';
import Color from '../Color';

const MainView = Styled(TouchableOpacity)`
    width: 100%;
    minHeight: 1;
    maxHeight: 170;
    flexDirection: row;
    borderColor: #000000;
    elevation: 0.5;
    marginBottom: 12;
`;

const ImageContainer = Styled(View)`
    width: 120;
    height: 100%;
`;

const ImageLimiterContainer = Styled(View)`
    flex: 1;
`;

const LabelContainer = Styled(View)`
    flex: 1;
    minHeight: 1;
    flexDirection: column;
    paddingHorizontal: 10;
    paddingVertical: 14;
`;

const TitleView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
`;

const LocationView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    marginTop: 8;
`;

const PriceContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    justifyContent: flex-end;
    marginBottom: 20;
`;

const LocationImageView = Styled(View)`
    width: 8.18;
    height: 10;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const TitleText = Styled(Text)`
    fontSize: 12;
    color: #333333;
    textAlign: left;
`;

const LocationText = Styled(Text)`
    fontSize: 10;
    color: #333333;
    textAlign: left;
`;

const GreyText = Styled(Text)`
    fontSize: 10;
    color: #231F20;
    textAlign: left;
`;

const RedText = Styled(Text)`
    fontSize: 12;
    color: #FF0000;
    textAlign: left;
`;

const AttractionTypeView = Styled(View)`
  height: 15;
  width: 60;
  borderRadius: 15;
  backgroundColor: ${Color.theme};
  marginTop: 8;
`;

const AttractionType = Styled(Text)`
  fontSize: 10;
  color: #333333;
  textAlign: center;
  marginRight: 2;
`;

const Dotted = Styled(View)`
  minHeight: 1;
  marginVertical: 14;
`;

const DottedLimiter = Styled(View)`
  flex: 1;
`;

const dotted = require('../../images/line-dotted.png');
const pinLocation = require('../../images/location.png');

class CardPopularAttractions extends Component {

  render() {
    // const { image, price, label, location } = this.props.data;

    const { data, navigation, onPress, ...style } = this.props;
    const { title, typename, locations, baseprice } = data.AttractionsProductsDetail;
    // console.log(this.props, 'propsAttr');

    return (
      <MainView activeOpacity={1} {...style} onPress={() => { if(onPress) onPress(); else navigation.navigate('DetailAttraction', { data }); }}>
        <ImageContainer>
          <ImageLimiterContainer>
            {/*<ImageProperty source={{ uri: image }} />*/}
            <ImageProperty source={{ uri: data.AttractionsProductsDetail.AttractionConfig[1].value + data.AttractionsProductsDetail.photos[0].path }} />
          </ImageLimiterContainer>
        </ImageContainer>

        <LabelContainer>
          <TitleView>
            <TitleText numberOfLines={2} type='semibold'>{data.title}</TitleText>
          </TitleView>

          <LocationView>
            <LocationImageView>
              <ImageProperty resizeMode='stretch' source={pinLocation} />
            </LocationImageView>
            <LocationText> {locations[0].city}, {locations[0].country}</LocationText>
          </LocationView>

          <AttractionTypeView>
            <AttractionType>{typename}</AttractionType>
          </AttractionTypeView>

          <Dotted>
            <DottedLimiter>
              <ImageProperty source={dotted} />
            </DottedLimiter>
          </Dotted>

          <PriceContainer>
            <GreyText type='medium'>mulai dari</GreyText>
            <GreyText><RedText type='medium'>{FormatMoney.getFormattedMoney(baseprice)}</RedText> per org</GreyText>
          </PriceContainer>
        </LabelContainer>
      </MainView>
    );
  }
}

export default withNavigation(CardPopularAttractions)
