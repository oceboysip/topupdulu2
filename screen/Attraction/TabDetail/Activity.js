import React, { Component } from 'react';
import { View, TouchableOpacity, Dimensions, Image, ScrollView, Platform, StyleSheet, Modal as BaseModal } from 'react-native';
import Styled from 'styled-components';
// import Text from '../../Fonts/OpenSans';
import Text from '../../Text'
import { Row, Col } from '../../Grid';

const { width, height } = Dimensions.get('window');

const MainView = Styled(View)`
  flex: 1;
  backgroundColor: #FFFFFF
`;

const InfoView = Styled(View)`
`;

const InfoText = Styled(Text)`
  textAlign: left
  fontSize: 12px
  padding: 13px 0px 13px 16px
`;

class Activity extends Component {

  render() {
    return (
      <MainView>
        <InfoView>
          <InfoText lineHeight={20}>{this.props.activity.additionalinfo}</InfoText>
        </InfoView>
      </MainView>
    );
  }
}

export default Activity;
