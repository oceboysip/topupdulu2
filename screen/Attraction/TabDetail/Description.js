import React, { Component } from 'react';
import { View, TouchableOpacity, Dimensions, Image, ScrollView, Platform, StyleSheet, Modal as BaseModal } from 'react-native';
import Styled from 'styled-components';
// import Text from '../../Fonts/OpenSans';
import Text from '../../Text'
import { Row, Col } from '../../Grid';

const { width, height } = Dimensions.get('window');

const MainView = Styled(View)`
  flex: 1;
  backgroundColor: #FFFFFF
`;

const InfoText = Styled(Text)`
  fontSize: 12px
  textAlign: left
  padding: 13px 0px 13px 16px
`;

const InfoView = Styled(View)`
`;

class Description extends Component {

  render() {
    return (
      <MainView>
        <InfoView>
          <InfoText lineHeight={20}>{this.props.description.description}</InfoText>
        </InfoView>
      </MainView>
    );
  }
}

export default Description;
