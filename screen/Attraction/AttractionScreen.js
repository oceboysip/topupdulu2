import React, { Component } from 'react';
import { View, ScrollView, ActivityIndicator, Modal } from 'react-native';
import Styled from 'styled-components';
import gql from "graphql-tag";
import { connect } from 'react-redux';
import EvilIcons from 'react-native-vector-icons/EvilIcons';

import TouchableOpacity from '../Button/TouchableDebounce';
import Client from '../../state/apollo';
import ModalIndicator from '../Modal/ModalIndicator';
import ModalAttractionSearch from '../Modal/AttractionSearch';
import Text from '../Text';
import Color from '../Color';
import FirstHighlightedCard from '../Travel/FirstHighlighted';
import SecondHighlightedCard from '../Travel/SecondHighlighted';
import Header from '../Header';

const MiniLoadingActivityView = Styled(View)`
    width: 100%;
    height: 100%;
    justifyContent: center;
    alignItems: center;
`;
const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    backgroundColor: #FFFFFF
`;

const HeaderContent = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: ${Color.theme};
    paddingHorizontal: 16;
`;

const DestinationSearchView = Styled(TouchableOpacity)`
    width: 100%;
    height: 50;
    flexDirection: row;
    alignItems: center;
    backgroundColor: #FFFFFF;
    paddingHorizontal: 6;
    marginBottom: 16;
`;

const SearchIconContainer = Styled(View)`
    minWidth: 20;
    minHeight: 20;
    alignItems: center;
    justifyContent: center;
    margin: 10px 6px 10px 0px;
`;

const SearchIcon = Styled(EvilIcons)`
    color: #A8A699;
    fontSize: 32;
`;

const SearchText = Styled(Text)`
    fontSize: 12;
    color: #A8A699;
    textAlign: left;
`;

const SubMenuContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    marginTop: 16;
    alignItems: center;
`;

const SubHeaderMenuContainer = Styled(View)`
    width: 50%;
    minHeight: 1;
    flexDirection: row;
    justifyContent: center;
    paddingHorizontal: 16;
    marginBottom: 16;
    paddingVertical: 4;
    backgroundColor: ${Color.primary};
`;

const LargerBlackText = Styled(Text)`
    fontSize: 14;
    letterSpacing: 1;
`;

const SubMenu = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    flexWrap: wrap;
    paddingLeft: 4;
`;

const pesanan = require('../../images/icon_paperWhite.png');

class AttractionScreen extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    this.state = {
      destination: null,
      readyDest: false,
      modalSearch: false
    };
  }

  componentDidMount() {
    this.getRelatedAttraction();
  }

  getRelatedAttraction(){
    const getAttractionConfig = gql`
      query{
        attractionConfig{
          filterDestinations{
            name
            value
          }
        }
      }
    `;
    Client.query({
      query: getAttractionConfig
    }).then(res => {
      if(res.data.attractionConfig){
        this.setState({
          destination: res.data.attractionConfig.filterDestinations,
          readyDest: true
        })
      }
    }).catch(reject => {
        console.log(reject)
    })
  }

  openModal(modal) {
    this.setState({ [modal]: true });
  }

  closeModal(modal) {
    this.setState({ [modal]: false });
  }

  openOrderBooking() {
    this.props.navigation.navigate('OrderBooking', {
      title: 'Atraksi'
    })
  }

  renderMiniLoadingIndicator() {
    return (
      <MiniLoadingActivityView>
        <ActivityIndicator size='large' color={Color.loading} />
      </MiniLoadingActivityView>
    );
  }

  renderFirstHighlighted(firstHighlightedData) {
    return firstHighlightedData.map((data, i) =>
      <FirstHighlightedCard
        key={i}
        data={data}
        style={{marginBottom: 16}}
        onPress={() => this.props.navigation.navigate('DetailAttraction', { data })}
      />
    );
  }

  renderSecondHighlighted(secondHighlightedData) {
    return secondHighlightedData.map((data, i) =>
      <SecondHighlightedCard
        key={i}
        data={data}
        onPress={() => this.props.navigation.navigate('PopularAttraction', { data, 'user': this.props.user })}
      />
    );
  }

  render() {
    const { destination, modalSearch } = this.state;
    const { recommendedAttraction } = this.props;

    console.log(destination, 'destination');
    console.log(recommendedAttraction, 'recommendedAttraction');
    
    // if(!recommendedAttraction.loading && this.state.readyDest) {
      return (
        <MainView>
          <Header title='Atraksi' imageRightButton={pesanan} onPressRightButton={() => this.openOrderBooking()} />
          <ScrollView>
            <HeaderContent>
              <DestinationSearchView onPress={() => this.openModal('modalSearch')}>
                <SearchIconContainer><SearchIcon name='search' /></SearchIconContainer>
                <SearchText>Cari Atraksi</SearchText>
              </DestinationSearchView>
            </HeaderContent>
            
            <SubMenuContainer>
              <SubHeaderMenuContainer>
                <LargerBlackText type='bold'>REKOMENDASI ATRAKSI</LargerBlackText>
              </SubHeaderMenuContainer>

              <SubMenu>
                {!recommendedAttraction.loading && recommendedAttraction.AttractionProducts.length > 0 ? this.renderFirstHighlighted(recommendedAttraction.AttractionProducts) : this.renderMiniLoadingIndicator()}
              </SubMenu>
            </SubMenuContainer>

            <SubMenuContainer>
              <SubHeaderMenuContainer style={{marginTop: 32}}>
                <LargerBlackText type='bold'>DESTINASI ATRAKSI</LargerBlackText>
              </SubHeaderMenuContainer>

              <SubMenu style={{flexDirection: 'row', flex: 1}}>
                {destination ? this.renderSecondHighlighted(destination) : this.renderMiniLoadingIndicator()}
              </SubMenu>
            </SubMenuContainer>
          </ScrollView>

          <Modal
            visible={modalSearch}
            transparent
            onRequestClose={() => this.closeModal('modalSearch')}
          >
            <ModalAttractionSearch onClose={() => this.closeModal('modalSearch')} />
          </Modal>
        </MainView>
      )
      // return (
      //   <HighlightedScreen
      //     title='Jenuh dengan rutinitas? Refreshing yuk!'
      //     textInputPlaceholder='Ketik nama atraksi atau aktivitas'
      //     firstHighlightedLabel='ATRAKSI POPULER'
      //     firstHighlightedNavigate='PopularAttraction'
      //     firstHighlightedData={recommendedAttraction.AttractionProducts}
      //     firstHighlightedOnPress='DetailAttraction'
      //     secondHighlightedLabel='ATRAKSI NEGARA FAVORIT'
      //     secondHighlightedNavigate='FavoriteAttraction'
      //     secondHighlightedData={destination}
      //     secondHighlightedOnPress='PopularAttraction'
      //     FirstHighlightedCard={FirstHighlightedCard}
      //     SecondHighlightedCard={SecondHighlightedCard}
      //     CustomModal={ModalAttractionSearch}
      //   />
      // );
    // }else {
    //   return this.renderMiniLoadingIndicator();
    // }
  }
}


const mapStateToProps = state => {
  return {
    user: state['user.auth'].login.user,
    error: state['user.auth'].error,
    recommendedAttraction: state.recommendedAttraction
  };
};

const mapDispatchToProps = null;

export default connect(mapStateToProps, mapDispatchToProps)(AttractionScreen);
