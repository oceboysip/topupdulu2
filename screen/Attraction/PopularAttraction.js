import React, { Component } from 'react';
import { View, ScrollView, ActivityIndicator, TouchableOpacity, Image, Modal } from 'react-native';
import Styled from 'styled-components';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import gql from 'graphql-tag';
import { connect } from 'react-redux';

import Color from '../Color';
import Header from '../Header';
import PopularAttractions from './CardPopularAttractions';
import ModalAttractionSearch from '../Modal/AttractionSearch';
import graphClient from '../../state/apollo';
import Text from '../Text';
import { Row, Col } from '../Grid';
import Button from '../Button';

const MainView = Styled(View)`
  width: 100%;
  height: 100%;
  flexDirection: column;
  backgroundColor: #FFFFFF;
`;

const CustomScrollView = Styled(ScrollView)`
  width: 100%;
  height: 100%;
`;

const BackgroundView = Styled(View)`
  width: 100%;
  height: 60;
  backgroundColor: ${Color.theme};
  position: absolute;
  top: 0;
  left: 0;
`;

const ChildContainer = Styled(View)`
  flex: 1;
  width: 100%;
  flexDirection: column;
  paddingHorizontal: 16;
`;

const TitleText = Styled(Text)`
  fontSize: 16px
  lineHeight: 19px
`;

const SearchView = Styled(TouchableOpacity)`
    width: 100%;
    height: 45;
    flexDirection: row;
    alignItems: center;
    backgroundColor: #FFFFFF;
    paddingHorizontal: 14;
    borderRadius: 22.5;
`;

const SearchImage = Styled(Image)`
    height: 20px
    width: 20px
    marginRight: 8
`;

const SearchText = Styled(Text)`
    fontSize: 12;
    color: #A8A699;
    textAlign: left;
`;

const MenuView = Styled(View)`
  width: 100%;
  borderRadius: 3px
  paddingHorizontal: 14;
  backgroundColor: #FFFFFF
  elevation: 2;
  borderWidth: 0.5;
  borderColor: #DDDDDD;
`;

const IconView = Styled(TouchableOpacity)`
  flexDirection: row
  padding: 14px 0px
  alignItems: center
  justifyContent: flex-start
`;

const IconImage = Styled(Image)`
    height: 15px
    width: 15px
`;

const MenuText = Styled(Text)`
    textAlign: left
`;

const SeeAll = Styled(Button)`
    width: 100%
    height:50px
    alignSelf: center
    marginVertical: 16px
`;

const ResultView = Styled(View)`
  width: 100%;
  minHeight: 1;
  padding: 14px 10px 0px 10px;
  flexDirection: column;
`;

const Attraction = Styled(PopularAttractions)`
  marginBottom: 12;
`;

const TitleContent = Styled(View)`
    minWidth: 1
    minHeight: 1
    paddingHorizontal: 10
    justifyContent: center
`;

const TitleContentText = Styled(Text)`
    margin: 35px 0px 13px
    textAlign: left
    letterSpacing: 1;
`;

const SearchIcon = Styled(EvilIcons)`
  color: ${Color.text};
  fontSize: 32;
`;

const iconSearch = require('../../images/search-icon.png');
const iconAttraction = require('../../images/atraksi.png');

const getAttractionAvailabilityQuery = gql`
  query(
    $param: paramAttraction!
  ){
    attractions(
      param: $param
    ){
      AttractionConfig{
        name value
      }
      AttractionProducts{
        uuid
        title
        AttractionsProductsDetail {
          typename
          AttractionConfig {
            name
            value
          }
          baseprice
          currency{
            code
          }
          locations{
            city
            country
          }
          photos {
            path
          }
        }
      }
    }
  }
`;

class PopularAttraction extends Component {

  static navigationOptions = { header: null };

  constructor() {
    super();
    this.state = {
      title: null,
      modalAttractionSearch: false,
      loading: false,
      attractions: [],
      page: 1,
      valueConfig: '',
      attractionConfig: []
    }
  }

  openModal(modal) {
    this.setState({ [modal]: true });
  }

  closeModal(modal) {
    this.setState({ [modal]: false });
  }

  renderPopularAttraction(attr){
    return attr.map((item, index) =>
      <Attraction key={index} data={item} />
    )
  }

  getAttractionAvailability(data){
    this.setState({
      title: 'Atraksi di ' + data.name,
      loading: true,
      valueConfig: data.value
    }, () => {

      const variables = {
        param: {
          destination: this.state.valueConfig,
          itemPerPage: 10,
          page: this.state.page
        }
      }

      graphClient
      .query({
        query: getAttractionAvailabilityQuery,
        variables
      })
      .then(res => {
        console.log(res, 'resss');
        
        if (res.data.attractions.AttractionProducts) {
          this.setState({
            attractions: res.data.attractions.AttractionProducts,
            attractionConfig: res.data.attractions.AttractionConfig,
            loading: false
          })
        }
      })
      .catch(err => {
        this.setState({ attractions: [], loading: false });
      })
    })
  }

  getShorterArray(array){
    return array.slice(0, 5);
  }

  //passing data in this
  componentDidMount(){
    const { data } = this.props.navigation.state.params;

    if (Object.prototype.toString.call(data) === '[object Array]') {
      this.setState({
        attractions: data,
        title: 'Atraksi Populer'
      })
    }else {
      this.getAttractionAvailability(data);
    }
  }

  componentWillMount(){
    // this.setState({
    //   page: this.state.page + 1
    // }, () => {
    //   this.getAttractionAvailability(this.props.navigation.state.params.data);
    // })
  }

  renderMenuView(data, categories){
    const { navigate, state } = this.props.navigation

    return(
      <MenuView>
        {this.getShorterArray(categories).map((item, index) =>
          <View key={index}>
          {item.filter===1 && <IconView onPress={() => { console.log(item.name), navigate('AllAttractionResult', {category: item.name, tab: index, title: this.state.title, categories: categories, destination: state.params.data.value})}}>
            <Col size={2}>
              <IconImage source={iconAttraction} resizeMode={'stretch'} />
            </Col>
            <Col size={10}>
              <MenuText type='semibold'>{item.label}</MenuText>
            </Col>
          </IconView>}
          </View>
        )}
        {this.getShorterArray(categories).map((item, index) =>
          <View key={index}>
            {item.filter===0 && <SeeAll onPress={() => navigate('AllAttractionResult', {category: '', tab: index, title: this.state.title, categories: categories, destination: state.params.data.value})}>
              Lihat Semua
            </SeeAll>}
          </View>
        )}
      </MenuView>
    )
  }

  render(){
    const { data } = this.props.navigation.state.params;
    const { user } = this.props;
    const { title, modalAttractionSearch, loading, attractions, attractionConfig } = this.state;
    const isGuest = !user || user.guest;
    let favoriteCountry = (Object.prototype.toString.call(data) === '[object Object]')
    let categories = [];

    if (loading) {
      return (
        <View style={{ height: '100%', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
          <ActivityIndicator size="large" color={Color.loading} />
        </View>
      );
    }

    for (const config of attractionConfig) {
      if (config.name === 'categories') {
        categories = JSON.parse(config.value);
      }
    }

    return(
      <MainView>
        <Header>
          <SearchView activeOpacity={1} onPress={() => this.openModal('modalAttractionSearch')}>
            <SearchImage source={iconSearch} />
            <SearchText>Ketik nama atraksi atau aktivitas di {data.name}</SearchText>
          </SearchView>
        </Header>
        <ScrollView>
          {favoriteCountry && <BackgroundView />}
          {favoriteCountry && <ChildContainer>
            {this.renderMenuView(attractions, categories)}
          </ChildContainer>}
          {favoriteCountry && <TitleContent><TitleContentText type='bold' lineHeight={19}>ATRAKSI POPULER DI {data.name.toUpperCase()}</TitleContentText></TitleContent>}
          {attractions.length > 0 &&
            <ResultView>{this.renderPopularAttraction(attractions)}</ResultView>
          }
        </ScrollView>

        <Modal visible={modalAttractionSearch} animationType='slide' onRequestClose={() => this.closeModal('modalAttractionSearch')}>
          <ModalAttractionSearch onClose={() => this.closeModal('modalAttractionSearch')} data={this.state} value={data.value} />
        </Modal>
      </MainView>
    )
  }
}

const mapStateToProps = state => {
  return{
    user: state['user.auth'].login.user,
    error: state['user.auth'].error,
  };
}

export default connect(mapStateToProps, null)(PopularAttraction);
