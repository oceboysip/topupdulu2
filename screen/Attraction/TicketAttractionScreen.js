import React, { Component } from 'react';
import { View, FlatList, Modal, ActivityIndicator } from 'react-native';
import Styled from 'styled-components';
import gql from 'graphql-tag'
import Moment from 'moment';
import { connect } from 'react-redux';

import TouchableOpacity from '../Button/TouchableDebounce';
import Client from '../../state/apollo';
import CardTicketAttraction from './CardTicketAttraction';
import Text from '../Text';
import ModalDatePicker from '../CalendarScreen';
import Color from '../Color';
import Header from '../Header';
import { Row, Col } from '../Grid';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    flexDirection: column;
    backgroundColor: #FFFFFF
`;

const BackgroundView = Styled(View)`
    width: 100%;
    height: 66;
    backgroundColor: ${Color.theme};
    position: absolute;
    top: 0;
    left: 0;
`;

const ChildContainer = Styled(View)`
    flex; 1;
    margin: 22px 0px 8px
    width: 100%;
    flexDirection: column;
    alignItems: center;
`;

const MenuText = Styled(Text)`
    textAlign: left
`;

const MenuView = Styled(View)`
    backgroundColor: #FFFFFF
    width: 94%
    margin: 0px 16px
    borderRadius: 3px
    shadow-color: #000;
    shadow-radius: 2;
    elevation: 2;
`;

const Container = Styled(View)`
    margin: 9px 15px 12px 11px
`;
const ButtonText = Styled(Text)`
    textAlign: left
    fontSize: 12px
    color: #231F20
    padding: 5px 10px
`;

const IconView = Styled(TouchableOpacity)`
    marginTop: 4px
    flexDirection: row
    alignItems: flex-start
    justifyContent: flex-start
`;
const HeaderRowRight = Styled(Col)`
    alignItems: flex-end
`;

const HeaderRowLeft = Styled(Col)`
    alignItems: flex-start
    justifyContent: center
`;

const ButtonHeaderLeft = Styled(TouchableOpacity)`
    borderRadius: 30px
    minWidth: 1px
    backgroundColor: ${Color.theme}
`;

const ButtonHeaderRight = Styled(ButtonHeaderLeft)`
    backgroundColor: #231F20
`;

let available = false

class TicketAttractionScreen extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      attraction: [],
      valuePassengers: {
          adult: 1,
          child: 0,
          old: 0
      },
      passengers: {
          adult: {
              subName: 'Umur 12-60',
              options: [1, 2, 3, 4, 5, 6],
          },
          child: {
              subName: 'Umur 2-12',
              options: [0, 1, 2, 3, 4, 5, 6],
          },
          old: {
              subName: 'Umur 60+',
              options: [0, 1, 2, 3, 4],
          },
      },
      departureDate: props.navigation.state.params.date,
      dateAttraction: '',
      modalDatePicker: false,
      openAlert: false,
      typeAlert: '',
      DateHolder: null,
      message: '',
      modalVisible: false,
      time: null,
      enable: 0,
      idx: null,
      activeButton: null,
      modalTime: false,
      modalPax: false
    };
    this.getAttractionAvailability();
  }

  getAttractionAvailability(){

      let variables = {
        param:{
          uuid: this.props.navigation.state.params.data.uuid,
          date: this.state.departureDate.format('YYYY-MM-DD'),
          adult: 1,
          child: 0,
          senior: 0

        }
      }
      // console.log(variables)
      const getAttraction = gql`
        query(
          $param: paramAttraction!
        ){
          attractionAvailability(
            param: $param
          ){

            enc
            uuid
            available
            amount
            final_amount
            AttractionsProductTypes{
              options{
                perBooking{
                  uuid
                  required
                  name
                  formatRegex
                  description
                  inputType
                  validFrom
                  validTo
                  items{
                    label
                    value
                  }
                }
                perPax{
                  uuid
                  name
                  description
                  required
                  formatRegex
                  inputType
                  validFrom
                  validTo
                  price
                  items{
                    label
                    value
                  }
                }
              }
              title
              minpax
              maxpax
              minadultage
              maxadultage
              allowinfant
              allowseniors
              allowchildren
              minseniorage
              minseniors
              maxseniors
              maxseniorage
              minchildren
              maxchildren
              minchildage
              maxchildage
              isnonrefundable
              cancelationpolicies{
                numberOfDays
                refundPercentage
              }
              voucheruse
              voucherredemtionaddress
              noninstantvoucher
            }
            options{
              uuid
              required
            }
            timeslots{
              uuid
              startTime
              endTime
            }
            currency
            prices{
              adults
              children
              seniors
              cancellationPolicy
            }
          }
        }
      `;
      Client.query({
        query: getAttraction,
        variables
      }).then(res => {
        if(res.data.attractionAvailability) {
           available = this.searchAvability(res.data.attractionAvailability)
           console.log(available, 'available')
           const sortByLowPrice = [].concat(res.data.attractionAvailability)
                            .sort((a, b) => parseInt(a.prices.adults, 10) - parseInt(b.prices.adults, 10));
          this.setState({
            attraction: sortByLowPrice, enable: true
          })
          if(this.state.attraction[0].timeslots.length === 0) {
            this.setState({ activeButton: false })
          }
           console.log('avaibility', res.data.attractionAvailability)
        }
      }).catch(reject =>{
          console.log(reject, 'reject')
      })
    }

    searchAvability(attraction) {
      for (const att of attraction)
        if (att.available === 'true') return true;
    }


  closeModal = (name) => {
    this.setState({ [name]: false })
  }

  openModal = (name) => {
    this.setState({ [name]: true })
  }

  onSelectFilter = (time, idx) => {
    this.setState({ time, idx },
    () => console.log(time, idx, 'time'))
  }


  submit = (item) => {
    if(item.timeslots.length === 0){
      this.props.navigation.navigate('DateAttraction', { data: item, date: this.state.departureDate, title: this.props.navigation.state.params.data.title, timeslots: '' })
    } else if (item.timeslots.length === 1) {
      this.props.navigation.navigate('DateAttraction', { data: item, date: this.state.departureDate, title: this.props.navigation.state.params.data.title, timeslots: item.timeslots[0] })
    } else {
      this.props.navigation.navigate('DateAttraction', { data: item, date: this.state.departureDate, title: this.props.navigation.state.params.data.title, timeslots: this.props.time })
    }

  }

  onSelectedDate = (date) => {
    this.setState({
      departureDate: date, modalDatePicker: false, enable: false
    }, () => this.getAttractionAvailability());
  }

  renderItem = ({ item, index }) => (
    <CardTicketAttraction
      enable={this.state.enable}
      item={item}
      index={index}
      idx={this.state.idx}
      submit={this.submit}
      date={this.state.departureDate}
      time={this.state.time}
      openModal={this.openModal}
      onSelectFilter={this.onSelectFilter}
      navigation={this.props.navigation}
      available={available}
    />
  )

  render() {
    // console.log(this.state, 'stateTicket');
    // console.log(this.props, 'propsTicket');

    const { attraction, departureDate, dateAttraction, modalDatePicker, enable } = this.state;
    const { data } = this.props.navigation.state.params;

    return (
      <MainView>
        <Header title='Cari Tiket' />
        <ChildContainer>
          <MenuView>
            <Container>
              <MenuText type='bold'>{data.title}</MenuText>
              <IconView>
                <HeaderRowLeft size={6}>
                  <ButtonHeaderLeft activeOpacity={0.5}><ButtonText type='medium'>{departureDate.format('DD MMM YYYY')}</ButtonText></ButtonHeaderLeft>
                </HeaderRowLeft>
                <HeaderRowRight size={6}>
                  <ButtonHeaderRight activeOpacity={0.5} onPress={() => this.openModal('modalDatePicker')}><ButtonText type='medium' style={{ color: '#FFFFFF' }}>Ubah</ButtonText></ButtonHeaderRight>
                </HeaderRowRight>
              </IconView>
            </Container>
          </MenuView>
        </ChildContainer>
        {enable ? <FlatList
          ref="flatList"
          extraData={this.state}
          data={attraction}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index.toString()}
        /> :
        <View style={{ height: '100%', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
          <ActivityIndicator size="large" color={Color.loading} />
        </View> }
        <Modal visible={modalDatePicker} animationType='slide' onRequestClose={() => this.closeModal('modalDatePicker')}>
          <ModalDatePicker
            onSelectedDate={this.onSelectedDate}
            onClose={() => this.closeModal('modalDatePicker')}
            selectedDate={departureDate}
            label={'Pilih'}
            minDate={Moment()}
            maxDate={Moment().add(1, 'Y')}
          />
        </Modal>
      </MainView>
    );
  }
}


const mapStateToProps = state => {
  return {
    user: state['user.auth'].login.user,
    error: state['user.auth'].error,
  };
};
  export default connect(mapStateToProps, null)(TicketAttractionScreen);
