import React, { Component } from 'react';
import { View, ScrollView, Modal } from 'react-native';
import Styled from 'styled-components';
import { connect } from 'react-redux';

import TouchableOpacity from '../Button/TouchableDebounce';
import ModalIndicator from '../Modal/ModalIndicator';
import ContactBooking from '../ContactBooking';
import ModalInformation from '../Modal/ModalInformation';
// import ModalFlightDetail from '../Modal/ModalFlightDetail';
import PassangersItem from '../Flight/PassangersItem';
import Text from '../Text';
import Color from '../Color';
import Header from '../Header';
import { Row, Col } from '../Grid';
import FormatMoney from '../FormatMoney';

import { bookAttraction } from '../../state/actions/attraction/bookAttraction';
import { getTitles } from '../../state/actions/get-titles';

const MainView = Styled(View)`
  width: 100%;
  height: 100%;
  alignItems: center;
  backgroundColor: #FFFFFF;
`;

const BackgroundView = Styled(View)`
  width: 100%;
  height: 76;
  backgroundColor: ${Color.theme};
`;

const AbsoluteView = Styled(View)`
  width: 100%;
  top: 30;
  position: absolute;
`;

const HeaderView = Styled(TouchableOpacity)`
  minHeight: 1;
  backgroundColor: #FFFFFF
  margin: 0px 15px
  elevation: 5px;
  justifyContent: center;
  alignItems: flex-start;
`;

const Container = Styled(View)`
  margin: 10px 15px 10px 11px
`;

const MenuText = Styled(Text)`
  textAlign: left
`;

const IconViewInfo = Styled(TouchableOpacity)`
  marginTop: 8px
  flexDirection: column
  alignItems: flex-start
  justifyContent: flex-start
`

const HeaderRowLeft = Styled(Col)`
  alignItems: flex-start
`;

const ButtonTextInfo = Styled(Text)`
  textAlign: left
  fontSize: 12px
  color: #231F20
`;

const BaseText = Styled(Text)`
  fontSize: 12px
`;

const WhiteText = Styled(Text)`
  color: #FFFFFF
  lineHeight: 12px
`;

const PriceView = Styled(View)`
  backgroundColor: red
  flexDirection: row
  marginTop: 60px;
  paddingHorizontal: 16;
  width: 100%
`;

const LeftHeaderView = Styled(View)`
  width: 50%
  height: 100%
  flexDirection: column
  alignItems: flex-start
  justifyContent: center
`;

const MidSubHeader = Styled(View)`
`;

const RightHeaderView = Styled(View)`
  width: 100%
  height: 100%
  justifyContent: center
  alignItems: flex-end
`;

const BlackHeaderView = Styled(View)`
  backgroundColor: black
  padding: 3px 10px
`;

const ContentView = Styled(View)`
  margin: 10px 15px 10px 11px
  flexDirection: row
`;

const WrapContentView = Styled(View)`
  width: 90%
`;
const IconView = Styled(View)`
  width: 10%
`;

const PerContent = Styled(View)`
  flexDirection: row
`;

const ContactView = Styled(View)`
  width: 100%
  backgroundColor: #FFFFFF
  borderBottomColor: #FAF9F9
  borderBottomWidth: 8px
`;

const SubContactView = Styled(View)`
  margin: 22px 15px
  alignItems: flex-start
`;
const SubtotalView = Styled(View)`
  width: 100%
  height: 63
  flexDirection: row
  justifyContent: space-between
  alignItems: center
  backgroundColor: #FFFFFF
  padding: 8px 16px 8px
  elevation: 20
`;

const ButtonText = Styled(Text)`
  fontSize: 14px
  color: #FFFFFF
  lineHeight: 34px
`;

const PriceText = Styled(Text)`
  color: #FF425E
`;

const ButtonRadius = Styled(TouchableOpacity)`
  borderRadius: 30px;
  width: 100%;
  backgroundColor: #231F20;
  height: 100%;
  justifyContent: center;
`;
const ButtomView = Styled(View)`
  width: 100%
  backgroundColor: #FFFFFF
  alignItems: center
  justifyContent: center
  flexDirection: row
  elevation: 2
`;

const titles = [
  {abbr: 'MR', name: 'Mr', isAdult: true},
  {abbr: 'MRS', name: 'Mrs', isAdult: true},
  {abbr: 'MS', name: 'Ms', isAdult: true}
];

const penumpang = { adult: 1, child: 0, infant: 0 };

  class AttractionBookScreen extends Component {
    static navigationOptions = { header: null };

    constructor(props){
      super(props);
      this.props.getTitles();

      let TempObjPax = []
      let TempObjBooking = []

      // if (props.navigation.state.params.attractions.options.perPax.length !== 0){
      //   let objValue
      //   for(let i = 0; i < (props.navigation.state.params.attractions.adult); i++){
      //    objValue = props.navigation.state.params.attractions.options.perPax.map((pax, idx) => {
      //       return {
      //         ...pax,
      //         id: idx,
      //         typePax: 'adult',
      //         error: null,
      //         value: '',
      //       }
      //     })
      //
      //     TempObjPax.push(objValue)
      //   }
      //
      //   for (let i = 0; i < (props.navigation.state.params.attractions.child); i++){
      //    objValue = props.navigation.state.params.attractions.options.perPax.map((pax, idx) => {
      //       return {
      //         ...pax,
      //         id: idx,
      //         typePax: 'child',
      //         error: null,
      //         value: '',
      //       }
      //     })
      //
      //     TempObjPax.push(objValue)
      //   }
      //
      //   for (let i = 0; i < (props.navigation.state.params.attractions.senior); i++){
      //      objValue = props.navigation.state.params.attractions.options.perPax.map((pax, idx) => {
      //       return {
      //         ...pax,
      //         id: idx,
      //         typePax: 'senior',
      //         error: null,
      //         value: '',
      //       }
      //     })
      //
      //     TempObjPax.push(objValue)
      //   }
      // }
      //
      // if (props.navigation.state.params.attractions.options.perBooking.length !== 0){
      //   let objValue = props.navigation.state.params.attractions.options.perBooking.map((pax, idx) => {
      //     return {
      //       ...pax,
      //       id: idx,
      //       typePax: 'adult',
      //       error: null,
      //       value: '',
      //     }
      //   })
      //
      //   TempObjBooking.push(objValue)
      // }

      const { title, firstName, lastName, countryCode, phoneNumber, email } = this.props.user_siago;

      this.state = {
        ModalPassanger: false,
        modalDetail: false,
        modalFail: false,
        loading: false,
        TempObjPax,
        loadingBooking: false,
        checked: true,
        sameAsContact: false,
        checkBox: false,
        valid: false,
        renderTitle: false,
        TempObjBooking,
        contact: {
          title: title || '',
          firstName: firstName || '',
          lastName: lastName || '',
          countryCode: countryCode || '62',
          phone: phoneNumber || '',
          email: email || ''
        },
        errors: {
          title: null,
          firstName: null,
          lastName: null,
          countryCode: null,
          phone: null,
          email: null,
        }
      }
    }

    openModal = (modal) => {
      this.setState({
        [modal]: true
      })
    }

    closeModal = (modal) => {
      this.setState({
        [modal]: false
      })
    }

    deletePassanger = (id) => {
      this.setState(prevState => {
        return {
          passengers: prevState.passengers.map(pax => {
            if (pax.id === id) {
              return  {
                ...pax,
                title: '',
                firstName: '',
                lastName: '',
                dateOfBirth: '',
              };
            }
            return pax;
          })
        }
      });
    }

    componentWillReceiveProps(nextProps){
      if (!this.props.fetchingBook && nextProps.fetchingBook) {
        this.openModal('loading');
      } else if (this.props.fetchingBook && !nextProps.fetchingBook) {
        this.closeModal('loading');
        if (nextProps.error === null) {
          this.props.navigation.navigate('PaymentScreen');
        } else {
          this.setState({ modalFail: true }, () => setTimeout(() => {
            this.setState({ modalFail: false,
            })
          }, 1000));
          console.log(nextProps.error, 'error');
        }
      }
    }

    onContactChange = (name, value) => {
      this.setState(prevState => {
        return {
          contact: {
            ...prevState.contact,
            [name]: value
          }
        }
      }, () => {console.log(this.state.contact, 'contact')})
    }

    onPassangerChange = (name, id, value) => {
      this.setState(prevState => {
        return {
          ...prevState,
          passengers: prevState.passengers.map((pax) => {
            if(pax.id === id){
              return {
                ...pax,
                [name]: value
              }
            } else {
              return pax
            }
          })
        }
      });
    }

    getTitleLabel = (abbr) => {
      const idx = titles.findIndex(title => title.abbr === abbr);
      if(idx !== -1) {
        return titles[idx].name;
      } else {
        return ''
      }
    }

    submit(contact, attractions, finalAmount) {
      this.props.onBookAttraction(contact, attractions, finalAmount);
    }

    renderPax(){
      return this.state.TempObjPax.map((pax, id) => {
        return (
          <PassangersItem
            titles={titles}
            pax={pax}
            key={id}
            deletePassanger={this.deletePassanger}
            onPassangerChange={this.onPassangerChange}
          />
        )
      })
    }

    renderBooking(){
      return this.state.TempObjBooking.map((pax, id) => {
        return (
          <PassangersItem
            titles={titles}
            pax={pax}
            key={id}
            deletePassanger={this.deletePassanger}
            onPassangerChange={this.onPassangerChange}
          />
        )
      })
    }

    render() {
      const { attractions } = this.props.navigation.state.params;
      const { contact, errors } = this.state;

      console.log(this.props);

      return (
        <MainView>
          <Header title='Review & Pesan Atraksi' />
          <ScrollView>
            <BackgroundView />
            <AbsoluteView>
              <HeaderView>
                <Container>
                  <MenuText type='bold'>{attractions.title}</MenuText>
                  <IconViewInfo>
                    <ButtonTextInfo type='medium'>Tanggal tiba : {attractions.date.format('DD MMM YYYY')}</ButtonTextInfo>
                    <ButtonTextInfo type='medium'>Jumlah pengunjung : {attractions.adult} Dewasa{attractions.child === 0 ? '' : ', ' + attractions.child + ' Anak'} {attractions.senior === 0 ? '' : ', ' + attractions.senior + ' Lansia'}</ButtonTextInfo>
                  </IconViewInfo>
                </Container>
              </HeaderView>
            </AbsoluteView>
            <PriceView />

            <ContactView>
              <ContactBooking
                titles={titles}
                contact={contact}
                errors={errors}
                onContactChange={this.onContactChange}
              />
            </ContactView>
            {false && (attractions.options.perPax.length !== 0 || attractions.options.perBooking.length !== 0) && <ContactView>
              <SubContactView>
                <Text type='bold'>DATA PENUMPANG</Text>
              </SubContactView>
            </ContactView>}
            {/*attractions.options.perPax.length !== 0 && this.renderPax()*/}
            {/*attractions.options.perBooking.length !== 0 && this.renderBooking()*/}
          </ScrollView>
          <ButtomView>
            <SubtotalView>
              <LeftHeaderView>
                <Text type='medium'>Subtotal</Text>
                <PriceText type='bold' color='button'>{FormatMoney.getFormattedMoney(attractions.subTotal)}</PriceText>
              </LeftHeaderView>
              <LeftHeaderView>
                <ButtonRadius onPress={() => this.submit(contact, attractions, attractions.subTotal)}>
                  <ButtonText>Bayar Sekarang</ButtonText>
                </ButtonRadius>
              </LeftHeaderView>
            </SubtotalView>
          </ButtomView>

          {/* <Modal
            onRequestClose={() => this.closeModal('modalDetail')}
            animationType="fade"
            transparent={true}
            visible={this.state.modalDetail}
          >
            <ModalFlightDetail
              closeModal={this.closeModal}
            />
          </Modal> */}

          <ModalIndicator
            visible={this.state.loading}
            type="large"
            message="Harap tunggu, kami sedang memproses pesanan Anda"
          />

          <Modal
            onRequestClose={() => {}}
            animationType="fade"
            transparent={true}
            visible={this.state.modalFail}
          >
            <ModalInformation
              label='Booking Anda Gagal'
              error
            />
          </Modal>
        </MainView>
      );
    }
  }


const mapStateToProps = state => {
  return {
    user: state['user.auth'].login.user,
    fetchingBook: state['booking'].fetching,
    error: state['booking'].error,
    titles: state['titles'].titles,
    user_siago: state['user.auth'].user_siago
  };
};

const mapDispatchToProps = (dispatch) => (
  {
    getTitles: () => {
        dispatch(getTitles());
    },
    onBookAttraction: (contact, attractions, finalAmount) => {
        dispatch(bookAttraction(contact, attractions, finalAmount));
    },
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(AttractionBookScreen);
