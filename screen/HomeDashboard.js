import React, { Component } from 'react';
import { Alert, StyleSheet, View, Image, Text as ReactText, TouchableOpacity, ScrollView, Platform, Dimensions, ActivityIndicator, RefreshControl } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import axios from 'axios';
import { NavigationActions, StackActions } from 'react-navigation';
import fetchApi from '../state/fetchApi';

import Text from './Text';
import { login } from '../state/actions/user/auth';
import BannerSlider from './BannerSlider';
import DashboardStyle from '../style/DashboardStyle';
import Color from './Color';
import FormatMoney from './FormatMoney';
import HTMLView from 'react-native-htmlview';
import FitImage from 'react-native-fit-image';
import Carousel from 'react-native-snap-carousel';
import { sliderWidth, itemWidth } from '../style/SliderEntry.style';

import { getRecommendedTours } from '../state/actions/travel/get-recommended-tours';
import { getPopularDestinations } from '../state/actions/travel/get-popular-destinations';
import { getRecommendedAttraction } from '../state/actions/attraction/get-recommended-attraction';

const MenuHome = [
  { label: 'Pesawat', nav: 'SearchFlight', icon: require('../images/icon_pesawat.png') },
  { label: 'Umroh & Travel', nav: 'TravelScreen', icon: require('../images/icon_umroh.png') },
  { label: 'Hotel', nav: 'HotelScreen', icon: require('../images/icon_hotel.png') },

  { label: 'Atraksi', nav: 'AttractionScreen', icon: require('../images/icon_atraksi.png') },
  { label: 'PPOB', nav: 'PPOBScreen', icon: require('../images/icon_ppob.png') },
  { label: 'Bus & Pelni', nav: 'BusScreen', icon: require('../images/icon_buspelni.png') },

  { label: 'News & Inspirasi', nav: 'MainScreenNews', icon: require('../images/icon_news.png') },
  { label: 'Kereta', nav: 'BookedTrain', icon: require('../images/icon_kereta.png'), comingsoon: true },
  { label: 'Shop', nav: 'ShopScreen', icon: require('../images/icon_shop.png'), comingsoon: true }
];

const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 150;

let NeoSansStdBoldTR = 'NeoSansStd Bold TR';
let NeoSansStdBoldItalicTR = 'NeoSansStd BoldItalic TR';
let NeoSansStdTR = 'NeoSansStd TR';

if (Platform.OS === 'ios') {
  NeoSansStdBoldTR = 'NeoSansStdBoldTR';
  NeoSansStdBoldItalicTR = 'NeoSansStdBoldItalicTR';
  NeoSansStdTR = 'NeoSansStdTR';
}

class HomeDashboard extends Component {
  focusListener;

  constructor(props) {
    super(props)
    this.state = {
      refreshing: false,
      siagoAmount: 0,
      loadingAmount: true,
      username: '', 
      password: '',
      showPassword: true,
      tempContentBanner:[],
      tempContentBerita:[],
      inputValue: '',
      fieldActive: false,
      dirty: false
    };

    this.focusListener = props.navigation.addListener('willFocus', this.componentWillFocus);
  }

  componentWillFocus = () => {
    this.getSaldo();
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  componentDidMount(){
    this.initialComponent();
  }

  initialComponent() {
    this.setState({ refreshing: true }, () => {
      this._loadInitialState().done();
      this._loadBanner();
      this._loadBerita();
      // this.props.getRecommendedTours();
      this.props.getPopularDestinations();
      this.props.getRecommendedAttraction(10);
      this.setState({ refreshing: false });
    });
  }

  getSaldo() {
    this.setState({ loadingAmount: true });

    fetchApi.get('user/check-saldo')
    .then(res => {
      this.setState({ siagoAmount: res.data.data.saldo, loadingAmount: false })
    })
    .catch(err => {
      console.log(err, 'error get saldo');
    });
  }

  _loadInitialState = async () => {
    var value = await AsyncStorage.getItem('user');
    if(value == null) {
      // this.props.navigation.navigate('Login');
    }else{
        this.setState({user: JSON.parse(value)})
        this.props.screenProps.setUser(this.state.user)
        this.props.screenProps.setLoading(false);   
        this._loadBerita();
    }
  }

  _loadBanner = () => {
    fetchApi.get("statis/banner")
    .then(response => {
      res = response.data;
      if(res.status){
        this.setState({tempContentBanner:res.data.banner})
        this.props.screenProps.setLoading(false);
      }
    })
    .catch(error => {
      console.log(error);
    });
  }

  

  _loadBerita = () => {
    axios.get("http://ataprumah.com/api/headline", {headers: {'CARPAN-TOKEN-KEY': '6?spik&6es@g5Cr-p1od'}})
    .then(response => {
      res = response.data;
      //console.log(res.data);
      if(res.status){
       
        this.setState({tempContentBerita:res.data})
        this.props.screenProps.setLoading(false);
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  }
  

renderNews() {
    const { tempContentBerita, loading } = this.state;

    if (tempContentBerita.length > 0 && !loading) {
        return tempContentBerita.map((item, key) =>
        <View>
        <View style={styles.boxInner_bt}>
                  <View style={styles.rowNews}>
                  <View style={styles.bigGallery}>
                    <FitImage 
                    source={{uri: item.gambar_detail}}
                    resizeMode="contain"
                    style={styles.fitImage}/>
                    <View style={styles.absOverflow}></View>
                  </View>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('NewsDetailScreen',{idnya: item.slug,berita:'CARPAN'})} style={{alignItems:'center',justifyContent:'center'}} >
                    <View style={styles.absTitle}>
                      <View style={{paddingTop:16,paddingBottom:10}}>
                      <Text style={{fontSize:16,position: 'absolute',paddingBottom:10, left: 0,color: '#1C1C1C', fontFamily: NeoSansStdBoldItalicTR }}>SIAGO-NEWS</Text>
                      <HTMLView value={this.ContentSnippet(item.judul)} style={{fontFamily: NeoSansStdTR, paddingTop: 0, paddingBottom:5}} /> 
                      <View style={{backgroundColor:'#1a558f',paddingHorizontal:12,paddingVertical:11,alignSelf:'flex-end',borderRadius:4}}>
                      <Text style={styles.smallpara} style={{fontSize: 13, color: 'white', fontWeight: 'bold',textAlign:'center'}}>Selengkapnya..</Text>
                      </View>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
                </View>
                </View>
        )
    } else {
        return (
            <View style={{height: 155,  backgroundColor: '#DDDDDD', justifyContent: 'center'}}>
                <ActivityIndicator />
            </View>
        )
    }
  }

  renderSearch(){
    if(this.state.tempContentBanner.length > 0 ){
      
      return   this.state.tempContentBanner.map((item, key) =>
                  this.rendertempContentBanner(item.file, key, item.tipe, item.value)
                )
    }else{
      return false;
    }
  }

  rendertempContentBanner(image, index, tipe, val){
      return   ( <View key={index}>
      <TouchableOpacity >
        <Image style={{ width: BannerWidth, height: BannerHeight }} source={{ uri: image }} />
        </TouchableOpacity>
    </View>);
  }

  renderPage(image, index) {
    return (
        <View key={index}>
            <Image style={{ width: BannerWidth, height: BannerHeight, resizeMode:'contain' }} source={{ uri: image }} />
        </View>
    );
  }

  logout = async () => {
    try{
      await AsyncStorage.removeItem('user');
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Login' })],
      });

      this.props.navigation.dispatch(resetAction);
      this.props.onLogout();
    }
    catch(e)
    {
      console.log("Error", "Terjadi Kesalahan: " + e);
    }
  }

  _keluar=()=>{
    Alert.alert(
      'Logout',
      'Apakah Anda yakin akan mengakhiri sesi ini?',
      [
        {text: 'Cancel', onPress: () => console.log('Ask me later pressed')},
        {text: 'OK', onPress: () => this.logout()},
      ],
      
    );
  }

  navigate(screen) {
    this.props.navigation.navigate(screen);
  }

  GetImage(content){
    var myRegexp = new RegExp(/<img.*?src="(.*?)"/);
    var match = myRegexp.exec(content);
      if (match){
          return match[1];
        }
  }

  renderMenuIOS(menu, idx) {
    if (!menu.comingsoon) return this.renderMenuAndroid(menu, idx);
  }

  renderMenuAndroid(menu, idx) {
    return (
      <TouchableOpacity
        key={idx}
        activeOpacity={0.7}
        style={{width: '33.33%', height: 100, alignItems: 'center'}}
        onPress={() => this.navigate(menu.nav)}
        disabled={menu.comingsoon}
      >
        <View style={{width: '100%', height: '50%', resizeMode:'contain', alignItems: 'center', justifyContent: 'center'}}>
          <Image style={[{width: '100%', height: '100%', resizeMode:'contain'}, menu.comingsoon && {opacity: 0.5}]} source={menu.icon} />
          <View style={{position: 'absolute'}}>
            {menu.comingsoon && <Text size={8} style={{backgroundColor: '#000000', color: '#FFFFFF', borderColor: '#FFFFFF', borderWidth: 0.5, paddingHorizontal: 8}}>COMING SOON</Text>}
          </View>
        </View>

        <View style={{height: '5%'}} />
        
        <View style={{height: '45%', width: '100%'}}>
          <Text style={[styles.textButton2, {height: '45%', width: '100%'}, menu.comingsoon && {opacity: 0.5}]}>{menu.label}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  ContentSnippet(content){
    return content.split(/\s+/).slice(0, 40).join(" ")+"...";
  }

  _renderItem = ({item, index}) => {
    return (
      <View style={styles.rowNews}>
        <View style={styles.bigGallery}>
          <FitImage source={{uri: item.gambar_detail}} />
          <View style={styles.absOverflow}></View>
        </View>
        <TouchableOpacity onPress={() => this.props.navigation.navigate('NewsDetailScreen',{idnya: item.slug,berita:'CARPAN'})} style={{alignItems:'center',justifyContent:'center'}} >
          <View style={styles.absTitle}>
            <HTMLView value={this.ContentSnippet(item.judul)} />
            <Text style={styles.smallpara} style={{fontSize: 12, color: '#1a558f', fontWeight: 'bold'}}>Selengkapnya..</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    console.log(this.props, 'das', this.state);
    const { user_siago } = this.props;
    const { siagoAmount, loadingAmount, refreshing } = this.state;

    if (refreshing) return <View />

    return (
      <View style={styles.container}>
        <View style={styles.topHome}>
          <ReactText style={{fontSize: 28, color: '#FFFFFF', fontWeight: 'bold'}}>SIAGO</ReactText>
          {/* <View style={{flexDirection:'row',justifyContent:'flex-end',position:'relative',flex:1,textAlign:'right'}}>
            <View style={{position:'relative'}}>
              <Image style={{width: 20, height: 20, resizeMode:'contain'}}  source={require('../images/icon_bell.png')}/>
              <View style={styles.roundRed} />
            </View>

            <View style={{position:'relative',marginLeft:10}}>
              <Image style={{width: 20, height: 20, resizeMode:'contain'}}  source={require('../images/icon_setting.png')}/>
            </View>
          </View> */}
        </View>

        <ScrollView refreshControl={<RefreshControl refreshing={refreshing} onRefresh={() => { this.initialComponent(); this.getSaldo(); }} />}>
          <View style={[styles.rows, {alignItems: 'flex-start'}]}>
            <Text style={styles.textWlcome}>Selamat Datang,</Text>
            {user_siago && <Text style={styles.textBold}>{user_siago.name}</Text>}
          </View>
          
          <BannerSlider {...this.props} />

          <View style={styles.greyContent}>
            <View style={styles.strechRow}>
              <Text style={styles.textBold}>Saldo Anda</Text>
              <View style={{flexDirection:'row'}}>
                <View style={{marginRight:10,textAlign:'right',justifyContent:'flex-end'}}>
                  {user_siago && user_siago.role === 'member' ? <View>
                    {loadingAmount ? <ActivityIndicator color={Color.theme} /> : <Text style={styles.textBold}>{FormatMoney.getFormattedMoney(siagoAmount)},-</Text>}
                  </View>
                  :
                    <Text style={styles.textBold}>Rp 0,-</Text>}
                  {user_siago && <Text style={styles.txtSmall}>{user_siago.phone}</Text>}
                </View>
                <TouchableOpacity onPress={() => this.getSaldo()}>
                  <Image style={{width:30, height:30, resizeMode:'contain'}} source={require('../images/icon_checkmark.png')} />
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                  <View style={[{width: '100%', flexDirection: 'row', flexWrap: 'wrap', marginTop: 24, paddingBottom: 8}, Platform.OS === 'ios' && {paddingBottom: 0}]}>
                    {MenuHome.map((menu, idx) =>
                      Platform.OS === 'ios' ? this.renderMenuIOS(menu, idx) : this.renderMenuAndroid(menu, idx)
                    )}
                  </View>
              </View>
            </View>

            <View style={{marginBottom:10}}>
              <View style={{flexDirection: 'row'}}>
                <ReactText style={{fontSize: 18, color: '#1a558f', fontFamily: NeoSansStdBoldTR}}>Ada Topik Baru nih ?</ReactText>
              </View>
            </View>
           
               
               {this.renderNews()}
            
            {/* <View style={[ {paddingVertical: 20, width: '100%'}]}>
              <Carousel
                layout={'default'}
                ref={(c) => { this._carousel = c; }}
                data={this.state.tempContentBerita}
                renderItem={this._renderItem}
                sliderWidth={sliderWidth}
                itemWidth={itemWidth}
              />
            </View> */}
          </View>
        </ScrollView>
   
        <View style={styles.footer}>
          <View style={styles.boxShadow2}>
            <View style={styles.boxInnerFooter}>
              <View style={styles.rowButtonTop}>
                {/* <View style={styles.rowButtonFoot}>
                <TouchableOpacity style={{alignItems:'center',justifyContent:'center'}} >
                    <Image style={{width:30, height:30, resizeMode:'contain'}} source={require('../images/icon_home.png')}/>
                    <Text style={styles.textButtonfoot}>Home</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.rowButtonFoot}>
                    <Image style={{width:30, height:30, resizeMode:'contain'}}   source={require('../images/icon_administrasi.png')}/>
                    <Text style={styles.textButtonfoot}>Administrasi</Text>
                </View> */}
              </View> 
              <View style={styles.rowButtonTop2}>
                <TouchableOpacity onPress={this._keluar} style={{alignItems:'center',justifyContent:'center'}} >
                <Image style={{width:30, height:30, resizeMode:'contain'}}   source={require('../images/icon_keluar.png')}/>
                <Text style={styles.textButtonfoot}>Keluar</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }
  
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}

// STYLES
// ------------------------------------
const styles = StyleSheet.create(DashboardStyle);

const mapStateToProps = (state) => ({
  allState: state,
  user: state['user.auth'].login.user,
  user_siago: state['user.auth'].user_siago
});

const mapDispatchToProps = (dispatch, props) => ({
  onLogin: (email, password) => dispatch(login(email, password)),
  onLogout: () => dispatch({ type: 'USER.LOGOUT' }),
  getRecommendedTours: () => {
    dispatch(getRecommendedTours());
  },
  getPopularDestinations: () => {
    dispatch(getPopularDestinations());
  },
  getRecommendedAttraction: (data) => {
    dispatch(getRecommendedAttraction(data))
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeDashboard);