import React, { Component } from 'react';
import { Image, Text, View, TextInput, TouchableOpacity } from 'react-native';
import ModalBox from 'react-native-modalbox';;
import Styled from 'styled-components';
// import TouchableOpacity from './Button/TouchableDebounce';
import ModalTitlePicker from './Modal/ModalTitlePicker';
import FormatMoney from './FormatMoney';
import Input from './Input/Input';
import { Row, Col } from './Grid';
// import Text from './Text';

const SubContactView = Styled(View)`
  margin: 22px 15px
  alignItems: flex-start
`;

const CustomModalBox = Styled(ModalBox)`
  height: 325;
`;
export default class CardBooking extends Component {

  constructor(props) {
    super(props);
    this.state = {
      visibleTitles: false
    };
  }

  openModal = (modal) => {
    this.setState({
      [modal]: true
    })
  }

  closeModal = (modal) => {
    this.setState({
      [modal]: false
    })
  }

  onTitleClose = () => {
    this.setState({
      visibleTitles: false
    })
  }

  onSelect = (key) => {
    this.setState(prevState => {
        return {
            ...prevState,
            [key]: true
        }
    })
  }

  render() {
    return (
      <View>
        <SubContactView>
          <View style={{ marginBottom: 15 }}>
            <Text type='bold' style={{letterSpacing:1 }}>DATA PEMESAN</Text>
          </View>
          <Input
                componentName="select"
                name="title"
                placeholder="Pilih Gelar"
                label="Gelar"
                error={this.props.errors.title}
                value={this.props.contact.title}
                modalName="visibleTitles"
                onSelect={this.onSelect}
          />

          <Input
                name="firstName"
                label='Nama Depan'
                placeholder=""
                keyboardType="default"
                returnKeyType="next"
                blurOnSubmit={false}
                value={this.props.contact.firstName}
                error={this.props.errors.firstName}
                onChangeValue={this.props.onContactChange}
                onBlurValue={this.onValidateContact}
          />
          <Input
                name="lastName"
                label='Nama Belakang'
                placeholder=""
                keyboardType="default"
                returnKeyType="next"
                blurOnSubmit={false}
                value={this.props.contact.lastName}
                error={this.props.errors.lastName}
                onChangeValue={this.props.onContactChange}
                onBlurValue={this.onValidateContact}
          />
          <Row>
              <Col size={1}>
                  <Input
                      name="countryCode"
                      keyboardType="numeric"
                      returnKeyType="next"
                      value={this.props.contact.countryCode}
                      error={this.props.errors.countryCode}
                      underlineColorAndroid='rgba(0,0,0,0)'
                      onChangeValue={this.props.onContactChange}
                      onBlurValue={this.onValidateContact}
                  />
              </Col>
              <Col size={11} style={{ paddingLeft: 8 }}>
                      <Input
                          name="phone"
                          IconName='ios-arrow-forward'
                          placeholder="No Telepon"
                          keyboardType="phone-pad"
                          returnKeyType="done"
                          maxLength={12}
                          value={this.props.contact.phone}
                          underlineColorAndroid='rgba(0,0,0,0)'
                          error={this.props.errors.phone}
                          onChangeValue={this.props.onContactChange}
                          onBlurValue={this.onValidateContact}
                      />
              </Col>
          </Row>
          <Input
                name="email"
                label='Email'
                placeholder=""
                keyboardType="email-address"
                returnKeyType="next"
                blurOnSubmit={false}
                value={this.props.contact.email}
                error={this.props.errors.email}
                onChangeValue={this.props.onContactChange}
                onBlurValue={this.onValidateContact}
          />
        </SubContactView>
        <CustomModalBox
          position='bottom' 
          isOpen={this.state.visibleTitles} 
          onClosed={this.onTitleClose}
          coverScreen
          backdropOpacity={0}
        >
          <ModalTitlePicker
              titles={this.props.titles}
              nameModal="visibleTitles"
              type="adult"
              onSave={this.props.onContactChange}
              onClose={this.onTitleClose}
              selected={this.props.contact.title}
          />
        </CustomModalBox>
      </View>
    );
  }
}