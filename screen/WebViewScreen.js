import React, { Component } from 'react';
import { View, ActivityIndicator, Platform } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import Styled from 'styled-components';
import { WebView } from 'react-native-webview';

import Color from './Color';
import Header from './Header';

const URL = require('url-parse');

const MainView = Styled(View)`
  flexDirection: column;
  width: 100%;
  backgroundColor: white;
  flex: 1
`;

const CustomActivityIndicator = Styled(ActivityIndicator)`
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    alignItems: center;
    justifyContent: center;
`;

// const Container = Styled(View)`
//     maxHeight: 35px;
//     justifyContent: center;
//     flex: 1;
// `;
//
// const BackIcon = Styled(Icon)`
//     fontSize: 24;
//     color: red;
// `;
//
// const BackView = Styled(TouchableOpacity)`
//     alignItems: flex-start;
//     justifyContent: center;
//     backgroundColor: transparent;
//     marginHorizontal: 20px
//     minHeight: 1px;
// `;

const baseDomain = 'vesta.id';

export default class WebViewScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      loading: true
    };
  }

  isPaid(url, host) {
    // const hostnamePaymentRouter = host.indexOf(`${baseDomain}/payment-router/callback`);
    const hostnamePaymentRouter = host.indexOf(`${baseDomain}/static-pages/payment-finish`);
    const paymentPaid = url.query.indexOf('status-payment=PAID');
    if (hostnamePaymentRouter >= 0 && paymentPaid >= 0) this.props.onClose('paymentPaid');
  }

  isTourPage(url, host) {
    const hostnameTour = host.indexOf(`${baseDomain}/tour/detail`);
    if (hostnameTour >= 0) {
      const arrayOfPathname = url.pathname.split('/');
      this.props.onClose('tourPage', arrayOfPathname[3]);
    }
  }

  checkURL(web) {
    const url = new URL(web.url);
    const host = url.hostname + url.pathname;
    this.isPaid(url, host);
    this.isTourPage(url, host);
  }

  onNavigationStateChange = (webViewState) => {
    // ini close modal otomatis dari web view
    this.checkURL(webViewState);
    if (!webViewState.loading) this.setState({ title: webViewState.title, loading: false });
    else this.setState({ title: webViewState.title });
  }

  renderActivityIndicatorLoadingView() {
    return (
      <CustomActivityIndicator color={Color.loading} size='large' />
    );
  }

  onPressLeftButton = () => {
    this.props.onClose();
  }

  render() {
    const { onClose, url } = this.props;
    return (
        <SafeAreaView style={{ backgroundColor: Color.theme, flex: 1 }} >
          <MainView>
            <Header title="Siago App Content" showLeftButton onPressLeftButton={onClose} iconLeftButton='close' />
            <WebView
              source={{ uri: url }}
              onNavigationStateChange={this.onNavigationStateChange}
              renderLoading={this.renderActivityIndicatorLoadingView}
              javaScriptEnabled
              domStorageEnabled
              startInLoadingState
            />
          </MainView>
            { Platform.OS  === 'ios' && <View style={{ height: 30 }}></View> }
      </SafeAreaView>
    );
  }
}