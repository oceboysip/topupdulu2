import React, { Component } from 'react';
import { Alert, StyleSheet, View, Image, TouchableOpacity, Dimensions, ActivityIndicator } from 'react-native';
import Carousel from 'react-native-banner-carousel';
import axios from 'axios';

import { constant } from '../constants';
import DashboardStyle from '../style/DashboardStyle';

const BannerWidth = Dimensions.get('window').width;

export default class BannerSlider extends Component {
    onMounting = true;

    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            tempContentBanner: [],
            fitHeight: []
        };
    }

    componentDidMount() {
        this._loadBanner();
    }

    componentWillUnmount() {
        this.onMounting = false;
    }

    _loadBanner = () => {
        axios.get(constant.api_url+'/statis/banner', { headers: {'X-Auth-Token': constant.api_token }})
        .then(response => {
          const res = response.data;
          let fitHeight = [], tempContentBanner = [];

          if (res.data) {
              tempContentBanner = res.data.banner;

              res.data.banner.map((item) => {
                Image.getSize(item.file, (width, height) => {
                    fitHeight.push(height * (BannerWidth / width));
                });
              })
          }

          if (this.onMounting) this.setState({ tempContentBanner, loading: false, fitHeight });
        })
        .catch(function (error) {
          console.log(error);
          this.setState({ loading: false });
        });
    }

    rendertempContentBanner(image, index, tipe, val){
        return (
            <View key={index}>
                <TouchableOpacity activeOpacity={0.7}>
                    <Image style={{width: BannerWidth, height: this.state.fitHeight[index]}} resizeMode='contain' source={{ uri: image}} />
                </TouchableOpacity>
            </View>
        );
    }

    renderBanner() {
        const { tempContentBanner, fitHeight, loading } = this.state;

        if (tempContentBanner.length > 0 && fitHeight.length > 0 && !loading) {
            return tempContentBanner.map((item, key) =>
                this.rendertempContentBanner(item.file, key, item.tipe, item.value)
            )
        } else {
            return (
                <View style={{height: 155, width: BannerWidth, backgroundColor: '#DDDDDD', justifyContent: 'center'}}>
                    <ActivityIndicator />
                </View>
            )
        }
      }
    
    render() {
        return (
            <View style={styles.rowBanner}>
                <Carousel
                    autoplay
                    autoplayTimeout={5000}
                    loop
                    index={0}
                    pageSize={BannerWidth}
                >
                    {this.renderBanner()}
                </Carousel>
            </View>
        )
    }
}

const styles = StyleSheet.create(DashboardStyle);