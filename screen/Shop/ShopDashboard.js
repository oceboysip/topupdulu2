import React, {Component} from 'react';
import {Alert,Platform, StyleSheet, Text , View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import DashboardStyle from '../../style/ShopDashboardStyle';
import Carousel from 'react-native-banner-carousel';

export default class ShopDashboard extends Component {


  SearchShop = () => {
    this.props.navigation.navigate('SearchShopScreen');
  }
  BookedTrain = () => {
    this.props.navigation.navigate('BookedTrain');
  }
  BookedHotel = () => {
    this.props.navigation.navigate('BookedHotel');
  }
  Pulsa = () => {
    this.props.navigation.navigate('Pulsa');
  }
  render() {
    return (
      <View style={styles.container}>
        
        <View style={styles.navTop}>

          <View style={styles.rowsBeetwen}>
            <View style={{flexDirection: 'row'}}>
            <Text style={{fontSize: 28, color: '#FFFFFF', fontWeight: 'bold'}}>SIAGO</Text>
            </View>

            <View style={{flexDirection:'row',justifyContent:'flex-end',position:'relative',flex:1,textAlign:'right'}}>
              <View style={{position:'relative'}}>
                <Image style={{width:25, height:29, resizeMode:'contain'}}  source={require('../../images/icon_bell.png')}/>
                <View style={styles.roundRed} />
              </View>

              <View style={{position:'relative',marginLeft:10}}>
                <Image style={{width:25, height:29, resizeMode:'contain'}}  source={require('../../images/icon_message.png')}/>
              </View>
              <View style={{position:'relative',marginLeft:10}}>
                <Image style={{width:25, height:29, resizeMode:'contain'}}  source={require('../../images/icon_chart.png')}/>
              </View>
            </View>
          </View>


          <View style={styles.rowsSmall}>
            <View style={styles.searchRow}>
              <TextInput placeholder="Cari Shop" style={styles.searchInput} />
              <Image style={{width:30, height:30,resizeMode:'contain',position:'absolute', left:10,top:4}} source={require('../../images/icon_search.png')}/>
            </View>
          </View>

        </View>

        <ScrollView>

        <View style={styles.greyContent}>
          <View style={styles.boxShadow}>
            <View style={styles.boxInner_bt}>
                <View style={styles.rows}>
                  <Text style={styles.textBold}>
                    Kategori Produk
                  </Text>
                </View>
                <View style={styles.boxRow}>
                      <View style={styles.rowButton}>
                        <TouchableOpacity style={{alignItems:'center',justifyContent:'center'}}  onPress={this.SearchShop}>
                          <Image style={{width:100, height:51, resizeMode:'contain'}} source={require('../../images/icon_tshirt.png')}/>
                          <Text style={styles.textButton2}>T-Shirt</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.rowButton}>
                        <TouchableOpacity style={{alignItems:'center',justifyContent:'center'}} onPress={this.BookedTrain}>
                          <Image style={{width:100, height:51, resizeMode:'contain'}} source={require('../../images/icon_hoodie.png')}/>
                          <Text style={styles.textButton2}>Hoodie</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.rowButton}>
                        <TouchableOpacity  style={{alignItems:'center',justifyContent:'center'}} onPress={this.BookedHotel}>
                          <Image style={{width:100, height:51, resizeMode:'contain'}} source={require('../../images/icon_jakcet.png')}/>
                          <Text style={styles.textButton2}>Jacket</Text>
                        </TouchableOpacity>
                      </View>
                      </View>

                      <View style={styles.boxRow}>
                      <View style={styles.rowButton}>
                        <TouchableOpacity style={{alignItems:'center',justifyContent:'center'}} onPress={this.Pulsa}>
                          <Image style={{width:100, height:51, resizeMode:'contain'}} source={require('../../images/icon_fashionanak.png')}/>
                          <Text style={styles.textButton2}>Fashion Anak</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.rowButton}>
                        <TouchableOpacity style={{alignItems:'center',justifyContent:'center'}} >
                          <Image style={{width:100, height:51, resizeMode:'contain'}} source={require('../../images/icon_elektronik.png')}/>
                          <Text style={styles.textButton2}>Elektronik</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.rowButton}>
                        <TouchableOpacity  style={{alignItems:'center',justifyContent:'center'}} >
                          <Image style={{width:100, height:51, resizeMode:'contain'}} source={require('../../images/icon_automotive.png')}/>
                          <Text style={styles.textButton2}>Automotive</Text>
                        </TouchableOpacity>
                      </View>
                      </View>

                      <View style={styles.boxRow}>
                      <View style={styles.rowButton}>
                        <TouchableOpacity style={{alignItems:'center',justifyContent:'center'}} >
                          <Image style={{width:100, height:51, resizeMode:'contain'}} source={require('../../images/icon_buku.png')}/>
                          <Text style={styles.textButton2}>Buku</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.rowButton}>
                        <TouchableOpacity style={{alignItems:'center',justifyContent:'center'}} >
                          <Image style={{width:100, height:51, resizeMode:'contain'}} source={require('../../images/icon_camera.png')}/>
                          <Text style={styles.textButton2}>Kamera</Text>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.rowButton}>
                        <TouchableOpacity  style={{alignItems:'center',justifyContent:'center'}} >
                          <Image style={{width:100, height:51, resizeMode:'contain'}} source={require('../../images/icon_mainan.png')}/>
                          <Text style={styles.textButton2}>Mainan</Text>
                        </TouchableOpacity>
                      </View>
                      </View>
            </View>
          </View>
          <View style={styles.bannerRow}>
            <Image style={{width:350, height:124, resizeMode:'contain'}} source={require('../../images/banner_shop.jpg')}/>
          </View>
          <View style={styles.rows}>
            <ScrollView 
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                >
              <View style={styles.itemCont}>
                <TouchableOpacity  style={{alignItems:'center',justifyContent:'center'}} >
                  <Image style={{width:100, height:100, resizeMode:'contain'}} source={require('../../images/thumb_baju.jpg')}/>
                  <Text style={styles.infoShirt}>T-Shirt Gundam</Text>
                  <Text style={styles.priceGreen}>Rp 130.000</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.itemCont}>
                <TouchableOpacity  style={{alignItems:'center',justifyContent:'center'}} >
                  <Image style={{width:100, height:100, resizeMode:'contain'}} source={require('../../images/thumb_baju.jpg')}/>
                  <Text style={styles.infoShirt}>T-Shirt Gundam</Text>
                  <Text style={styles.priceGreen}>Rp 130.000</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.itemCont}>
                <TouchableOpacity  style={{alignItems:'center',justifyContent:'center'}} >
                  <Image style={{width:100, height:100, resizeMode:'contain'}} source={require('../../images/thumb_baju.jpg')}/>
                  <Text style={styles.infoShirt}>T-Shirt Gundam</Text>
                  <Text style={styles.priceGreen}>Rp 130.000</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.itemCont}>
                <TouchableOpacity  style={{alignItems:'center',justifyContent:'center'}} >
                  <Image style={{width:100, height:100, resizeMode:'contain'}} source={require('../../images/thumb_baju.jpg')}/>
                  <Text style={styles.infoShirt}>T-Shirt Gundam</Text>
                  <Text style={styles.priceGreen}>Rp 130.000</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.itemCont}>
                <TouchableOpacity  style={{alignItems:'center',justifyContent:'center'}} >
                  <Image style={{width:100, height:100, resizeMode:'contain'}} source={require('../../images/thumb_baju.jpg')}/>
                  <Text style={styles.infoShirt}>T-Shirt Gundam</Text>
                  <Text style={styles.priceGreen}>Rp 130.000</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.itemCont}>
                <TouchableOpacity  style={{alignItems:'center',justifyContent:'center'}} >
                  <Image style={{width:100, height:100, resizeMode:'contain'}} source={require('../../images/thumb_baju.jpg')}/>
                  <Text style={styles.infoShirt}>T-Shirt Gundam</Text>
                  <Text style={styles.priceGreen}>Rp 130.000</Text>
                </TouchableOpacity>
              </View>

            </ScrollView>
          </View>
        </View>
        </ScrollView>
      </View>



    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null
  };
}

// STYLES
// ------------------------------------
const styles = StyleSheet.create(ShopDashboardStyle);