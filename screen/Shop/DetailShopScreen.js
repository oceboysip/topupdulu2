import React, {Component} from 'react';
import {Alert,Platform, StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import DashboardStyle from '../../style/DetailShopStyle';
import Carousel from 'react-native-banner-carousel';
import { TabView, SceneMap } from 'react-native-tab-view';
import { TabBar } from 'react-native-tab-view';

const FirstRoute = () => (
  <View style={[styles.container, { backgroundColor: '#fff',padding:10 }]} >
  <Text>safsafafag </Text>
  </View>
);

const SecondRoute = () => (
  <View style={[styles.container, { backgroundColor: '#fff',padding:10 }]} >
  <Text>safsafafag </Text>
  </View>
);

const ThirdRoute = () => (
  <View style={[styles.container, { backgroundColor: '#fff',padding:10 }]} >
  <Text>asnfkjas </Text>
  </View>
);

const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 216;
 
const images = [
     "http://bogorkuliner.com/shirt_banner.jpg",
     "http://bogorkuliner.com/shirt_banner.jpg"
];

export default class DetailShopScreen extends Component {
    state = {
      index: 0,
      routes: [
        { key: 'first', title: 'Informasi Produk' },
        { key: 'second', title: 'Spesifikasi' },
        { key: 'aa', title: 'Ulasan' }
      ],
    };
  renderPage(image, index) {
      return (
          <View key={index}>
              <Image style={{ width: BannerWidth, height: BannerHeight, resizeMode:'contain' }} source={{ uri: image }} />
          </View>
      );
  }

  SearchFlight = () => {
    this.props.navigation.navigate('SearchFlight');
  }
  BookedTrain = () => {
    this.props.navigation.navigate('BookedTrain');
  }
  BookedHotel = () => {
    this.props.navigation.navigate('BookedHotel');
  }
  Pulsa = () => {
    this.props.navigation.navigate('Pulsa');
  }
  render() {
    return (
      <View style={styles.container}>
        
        <View style={styles.navTop}>

          <View style={styles.rowsBeetwen}>
          <View style={{flexDirection: 'row'}}>
            <Text style={{fontSize: 28, color: '#FFFFFF', fontWeight: 'bold'}}>SIAGO</Text>
            </View>

            <View style={{flexDirection:'row',justifyContent:'flex-end',position:'relative',flex:1,textAlign:'right'}}>
              <View style={{position:'relative'}}>
                <Image style={{width:25, height:29, resizeMode:'contain'}}  source={require('../../images/icon_bell.png')}/>
                <View style={styles.roundRed} />
              </View>

              <View style={{position:'relative',marginLeft:10}}>
                <Image style={{width:25, height:29, resizeMode:'contain'}}  source={require('../../images/icon_message.png')}/>
              </View>
              <View style={{position:'relative',marginLeft:10}}>
                <Image style={{width:25, height:29, resizeMode:'contain'}}  source={require('../../images/icon_chart.png')}/>
              </View>
            </View>
          </View>


          <View style={styles.rowsSmall}>
            <View style={styles.searchRow}>
              <TextInput placeholder="Cari Airport" style={styles.searchInput} />
              <Image style={{width:30, height:30,resizeMode:'contain',position:'absolute', left:10,top:4}} source={require('../../images/icon_search.png')}/>
            </View>
          </View>

        </View>

        <ScrollView>
          <View style={styles.greyContent}>
            <View style={styles.rowBanner}>
              <Carousel
                  autoplay
                  autoplayTimeout={5000}
                  loop
                  index={0}
                  pageSize={BannerWidth}
              >
                  {images.map((image, index) => this.renderPage(image, index))}
              </Carousel>
            </View>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                <View style={styles.rowsBeetwen}>
                  <View style={styles.leftInfo}>
                    <Text style={styles.infoShirt}>T-Shirt Gundam</Text>
                    <Text style={styles.priceGreen}>Rp 130.000</Text>
                  </View>
                  <View style={styles.whislistInfo}>
                    <Image style={{width:30, height:30,resizeMode:'contain'}} source={require('../../images/love_unactive.png')}/>
                    <Text style={styles.greyCity}>Add to Wishlist</Text>
                  </View>
                </View>
              </View>
            </View>

            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                <View style={styles.rowsBeetwen}>
                  <View style={styles.leftInfo}>
                    <Text style={styles.smallblacktxt}>Ukuran</Text>
                  </View>

                  <View style={styles.infoUkuran}>
                    <Text style={styles.circleActiveUk}>S</Text>
                    <Text style={styles.circleUk}>M</Text>
                    <Text style={styles.circleUk}>L</Text>
                    <Text style={styles.circleUk}>XL</Text>
                  </View>
                </View>
              </View>
            </View>

            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                <View style={styles.rowRight}>
                  <View style={{width:120}}>
                    <Text style={styles.smallblacktxt}>Jumlah</Text>
                    <View style={{flexDirection:'row', marginTop:10}}>
                      <Image style={{width:20, height:20,resizeMode:'contain'}} source={require('../../images/minus_circle.png')}/>
                      <Text style={styles.underlineText}>2</Text>
                      <Image style={{width:20, height:20,resizeMode:'contain'}} source={require('../../images/plus_circle.png')}/>
                    </View>
                  </View>
                  <View style={{flex:1}}>
                    <Text style={styles.greyCity}>Catatan untuk Penjual (Opsional)</Text>
                    <TextInput
                        placeholder="Contoh : Warna Hitam, Ukuran L"
                        style={styles.inputForm}
                      />
                  </View>
                </View>
              </View>
            </View>

            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                 <TabView
                  navigationState={this.state}
                  renderScene={SceneMap({
                    first: FirstRoute,
                    second: SecondRoute,
                    aa: ThirdRoute
                  })}
                  onIndexChange={index => this.setState({ index })}
                  initialLayout={{ width: Dimensions.get('window').width }}
                  renderTabBar={props =>
                      <TabBar
                          {...props}
                          indicatorStyle={{ backgroundColor: 'red' }}
                          style={{ backgroundColor: '#fff' }}
                          tabStyle={styles.customTabs}
                          labelStyle={styles.labelTabs}
                      />
                  }

                />
              </View>
            </View>

          </View>
        </ScrollView>
      </View>



    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}

// STYLES
// ------------------------------------
const styles = StyleSheet.create(DetailShopStyle);