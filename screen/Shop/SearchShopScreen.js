import React, {Component} from 'react';
import {Alert,Platform, StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import DashboardStyle from '../../style/SearchShopStyle';
import Carousel from 'react-native-banner-carousel';

export default class SearchShopScreen extends Component {

  DetailShop = () => {
    this.props.navigation.navigate('DetailShopScreen');
  }
  BookedTrain = () => {
    this.props.navigation.navigate('BookedTrain');
  }
  BookedHotel = () => {
    this.props.navigation.navigate('BookedHotel');
  }
  Pulsa = () => {
    this.props.navigation.navigate('Pulsa');
  }
  render() {
    return (
      <View style={styles.container}>
        
        <View style={styles.navTop}>

          <View style={styles.rowsBeetwen}>
            <View style={{flexDirection: 'row'}}>
            <Text style={{fontSize: 28, color: '#FFFFFF', fontWeight: 'bold'}}>SIAGO</Text>
            </View>

            <View style={{flexDirection:'row',justifyContent:'flex-end',position:'relative',flex:1,textAlign:'right'}}>
              <View style={{position:'relative'}}>
                <Image style={{width:25, height:29, resizeMode:'contain'}}  source={require('../../images/icon_bell.png')}/>
                <View style={styles.roundRed} />
              </View>

              <View style={{position:'relative',marginLeft:10}}>
                <Image style={{width:25, height:29, resizeMode:'contain'}}  source={require('../../images/icon_message.png')}/>
              </View>
              <View style={{position:'relative',marginLeft:10}}>
                <Image style={{width:25, height:29, resizeMode:'contain'}}  source={require('../../images/icon_chart.png')}/>
              </View>
            </View>
          </View>


          <View style={styles.rowsSmall}>
            <View style={styles.searchRow}>
              <TextInput placeholder="Cari Airport" style={styles.searchInput} />
              <Image style={{width:30, height:30,resizeMode:'contain',position:'absolute', left:10,top:4}} source={require('../../images/icon_search.png')}/>
            </View>
          </View>

        </View>

        <ScrollView>
          <View style={styles.greyContent}>
            <View style={styles.rowSearch}>
              <View style={styles.rowButton}>

                  <Image style={{width:30, height:30,resizeMode:'contain', position:'absolute',right:10, top:10}} source={require('../../images/love_unactive.png')}/>
                  <TouchableOpacity  style={{marginTop:25}} onPress ={this.DetailShop}>
                    <Image style={{width:150, height:150,resizeMode:'contain'}} source={require('../../images/shirt_big.jpg')}/>
                  </TouchableOpacity>
                  <View style={styles.textInfo}>
                    <Text style={styles.infoShirt}>T-Shirt Gundam</Text>
                    <Text style={styles.priceGreen}>Rp 130.000</Text>
                  </View>
                  <View style={styles.pointRow}>
                    <Text style={styles.greyCity}>Jakarta Selatan</Text>
                    <View style={styles.rowFLex}>
                      <Image style={{width:79, height:13,resizeMode:'contain',marginRight:3}} source={require('../../images/star_point.png')}/>
                      <Text style={styles.smallGrey}>(150)</Text>
                    </View>
                  </View>
              </View>

              <View style={styles.rowButton}>

                  <Image style={{width:30, height:30,resizeMode:'contain', position:'absolute',right:10, top:10}} source={require('../../images/love_active.png')}/>
                  <TouchableOpacity  style={{marginTop:25}} >
                    <Image style={{width:150, height:150,resizeMode:'contain'}} source={require('../../images/shirt_big.jpg')}/>
                  </TouchableOpacity>
                  <View style={styles.textInfo}>
                    <Text style={styles.infoShirt}>T-Shirt Gundam</Text>
                    <Text style={styles.priceGreen}>Rp 130.000</Text>
                  </View>
                  <View style={styles.pointRow}>
                    <Text style={styles.greyCity}>Jakarta Selatan</Text>
                    <View style={styles.rowFLex}>
                      <Image style={{width:79, height:13,resizeMode:'contain',marginRight:3}} source={require('../../images/star_point.png')}/>
                      <Text style={styles.smallGrey}>(150)</Text>
                    </View>
                  </View>
              </View>


            </View>
          </View>
        </ScrollView>
      </View>



    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}

// STYLES
// ------------------------------------
const styles = StyleSheet.create(SearchShopStyle);