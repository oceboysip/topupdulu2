import React, {Component} from 'react';
import {Alert,Platform, StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ImageBackground} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import LoginStyle from '../style/LoginStyle';
import FloatingLabel from 'react-native-floating-labels';
import passShow from '../images/eye_open.png';
import passHide from '../images/eye_close.png';


export default class LoginScreen extends Component {
	constructor(props){
	super(props)
	this.state = {
	  	showPassword: true,
		};
	}

	dashboard = () => {
		this.props.navigation.navigate('dashboard');
	}
  render() {
  	var imgSource = this.state.showPassword? passHide :  passShow;

    return (
    	<ImageBackground source={require('../images/bg_login.jpg')} style={{width: '100%', height: '100%'}}>
	    	<View style={styles.container}>
	    	<View style={styles.navTop}>
	            <TouchableOpacity >
	              <Image style={{width:40, height:40,resizeMode:'contain'}} source={require('../images/anchor.png')}/>
	            </TouchableOpacity>
	          </View>
	          <View style={styles.innerLogin}>
	            <View style={styles.insideLogo}>
	              <Image style={{width:250,height:184,resizeMode:'contain'}} 
	              source={require('../images/logotopup.png')}/>
	            </View>
	              <View style={styles.boxInner}>
	                <View style={styles.groupForm}>
	                  <FloatingLabel
	                    returnKeyType="next"
	                    autoCapitalize="none"
	                    labelStyle={styles.labelInput}
	                    inputStyle={styles.inputBox}
	                    onBlur={this.onBlur}
	                  >USER ID</FloatingLabel>
	                </View>

	                <View style={styles.groupForm}>

	                <FloatingLabel
	                    returnKeyType="next"
	                    autoCapitalize="none"
	                    labelStyle={styles.labelInput}
	                    inputStyle={styles.inputBox}
	                    onBlur={this.onBlur}
	                    secureTextEntry = { this.state.showPassword }
	                  >
	                 PASSWORD</FloatingLabel>
	                    <TouchableOpacity style={{position:'absolute', right:0,bottom:5,width:20,height:20}} onPress={ () => this.setState({ showPassword: !this.state.showPassword }) }>
	                      <Image style={{width:20,height:20,resizeMode:'contain'}}
	                      source={ imgSource }/>
	                    </TouchableOpacity>
	                </View>

	            </View>
	            <TouchableOpacity style={styles.button} onPress={this.dashboard}>
	              <Text style={styles.buttonText} onPress={this.onHome}>Login</Text>
	            </TouchableOpacity>

	            <Text style={styles.versionText}>Lupa Paswword</Text>

	          </View>
	        </View>
        </ImageBackground>
    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}

// STYLES
// ------------------------------------
const styles = StyleSheet.create(LoginStyle);