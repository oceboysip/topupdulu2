import React, { Component } from 'react';
import { View, ScrollView , Keyboard, Platform, Modal } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import Styled from 'styled-components';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { TextInputMask } from 'react-native-masked-text';
import Accordion from 'react-native-collapsible/Accordion';

import ModalTitlePicker from '../Modal/ModalTitlePicker';
import ModalCountryPicker from '../Modal/CountryPicker';
import Text from '../Text';
import Input from '../Input/Input';
import Color from '../Color';

const MainView = Styled(View)`
  width: 100%;
  backgroundColor: #FFFFFF;
  flexDirection: column;
  flex: 1;
`;

const SideIcon = Styled(Icon)`
  color: #000000;
`;


const TopView = Styled(View)`
  flexDirection: column;
`;
const HeaderView = Styled(View)`
  flexDirection: row;
  backgroundColor: ${Color.theme};
  padding: 23px 16px 10px 17px;
  width: 100%;
`;
const LeftHeaderView = Styled(View)`
  width: 20%;
  alignItems: flex-start;
`;

const MidHeaderView = Styled(View)`
  width: 60%;
`;

const RightHeaderView = Styled(View)`
  width: 20%;
  alignItems: flex-end;
`;

const AttentionView = Styled(View)`
  alignItems: flex-start;
  backgroundColor: ${Color.text};
  flexDirection: column;
`;
const Container = Styled(View)`
  padding: 16px;
  flexDirection: row;
`;
const LeftView = Styled(View)`
  paddingTop: 3px
  width: 5%;
  alignItems: flex-start;
`;
const RightView = Styled(View)`
  width: 85%;
`;

const BaseText = Styled(Text)`
  fontSize: 12;
  textAlign: left;
  color: #FFFFFF;
`;
const PassangerView = Styled(View)`
  margin: 8px 16px;
`;

const HeaderContainer = Styled(View)`
  width: 100%;
  height: 50;
  alignItems: center;
  flexDirection: row;
  borderBottomWidth: 0.5;
  borderColor: #DDDDDD;
`;

const HeaderSectionLeft = Styled(View)`
  width: 95%;
  height: 100%;
  alignItems: flex-end;
  justifyContent: center;
`;

const HeaderSectionRight = Styled(View)`
  width: 5%;
  height: 100%;
  justifyContent: center;
  alignItems: flex-end;
`;

const IconArrow = Styled(Ionicons)`
  color: #333333;
`;

export default class ModalPassangerBus extends Component {

  constructor(props) {
    super(props);
    this.state = {
      activeSection: [1],
      refresh: false,
      modalTitle: false,
      modalCountryPicker: false,
      country: 'Indonesia',
      dt: props.pax.birthDate
    };
  }

  onPassangerChange = (name, value) => {
    this.props.onPassangerChange(name, this.props.id, value);
  }

  onSelect = (key) => {
    console.log(key, 'keeeee');
    this.setState(prevState => {
      return {
        ...prevState,
        [key]: true
      }
    })
  }

  closeModal = (modal) => {
    this.setState({ [modal]: false });
  }

  // openModal = (modal) => {
  //   this.setState({ [modal]: true });
  // }

  renderHeader = (method, index, isActive) => (
    <View style={{paddingHorizontal: 16, backgroundColor: '#FFFFFF'}}>
      <HeaderContainer>
        <HeaderSectionLeft><Text>(Opsional)</Text></HeaderSectionLeft>
        <HeaderSectionRight><IconArrow name={isActive ? 'md-remove' : 'md-add'} /></HeaderSectionRight>
      </HeaderContainer>
    </View>
  )

  renderContentPriceDetail = (pax) => (
    <PassangerView>
      <Text style={{
        color: '#A8A699',
        textAlign: 'left',
        fontSize: 12,
        paddingTop: 7,
        paddingBottom: 15,
      }}>Tanggal Lahir</Text>
      <TextInputMask
        name="birthDate"
        type={'datetime'}
        label='Tanggal Lahir'
        placeholder="YYYY-MM-DD"
        keyboardType="numeric"
        returnKeyType="done"
        blurOnSubmit={false}
        error={pax.errors.birthDate}
        options={{ format: 'YYYY-MM-DD' }}
        value={this.state.dt}
        onChangeText={(text) => this.setState({ dt: text }, () => this.onPassangerChange('birthDate', text))}
        onSubmitEditing={Keyboard.dismiss}
        onBlurValue={this.onValidatePassanger}
        style={{borderBottomWidth: 1, borderColor: '#DDDDDD', paddingBottom: 0}}
      />
      <Input
        name="address"
        label='Alamat'
        placeholder=""
        keyboardType="default"
        returnKeyType="next"
        blurOnSubmit={false}
        value={pax.address}
        error={pax.errors.address}
        onChangeValue={this.onPassangerChange}
        onBlurValue={this.onValidatePassanger}
        maxLength={100}
      />
      <Input
        name="city"
        label='Kota/Kabupaten'
        placeholder=""
        keyboardType="default"
        returnKeyType="next"
        blurOnSubmit={false}
        value={pax.city}
        error={pax.errors.city}
        onChangeValue={this.onPassangerChange}
        onBlurValue={this.onValidatePassanger}
        maxLength={100}
      />
      <Input
        componentName="select"
        name="country"
        label='Negara'
        placeholder="Pilih Negara"
        modalName='modalCountryPicker'
        value={pax.country}
        error={pax.errors.country}
        onSelect={this.onSelect}
      />
      <Input
        name="postalCode"
        label='Kode Pos'
        placeholder=""
        keyboardType="numeric"
        returnKeyType="next"
        blurOnSubmit={false}
        value={pax.postalCode}
        error={pax.errors.postalCode}
        onChangeValue={this.onPassangerChange}
        onBlurValue={this.onValidatePassanger}
        maxLength={5}
      />
      <Input
        name="email"
        label='Email'
        placeholder=""
        keyboardType="email-address"
        returnKeyType="next"
        blurOnSubmit={false}
        value={pax.email}
        error={pax.errors.email}
        onChangeValue={this.onPassangerChange}
        onBlurValue={this.onValidatePassanger}
        maxLength={100}
      />
      <Input
        name="idCard"
        label='Nomor KTP'
        placeholder=""
        keyboardType="numeric"
        returnKeyType="next"
        blurOnSubmit={false}
        value={pax.idCard}
        error={pax.errors.idCard}
        onChangeValue={this.onPassangerChange}
        onBlurValue={this.onValidatePassanger}
        maxLength={100}
      />
    </PassangerView>
  )

  onChangeActiveAccordion(index) {
    const activeSection = this.state.activeSection;
    activeSection[0] = index;
    this.setState({ activeSection });
  }

  render() {
    const { pax, tour } = this.props;

    return (
      <SafeAreaView style={{backgroundColor: Color.theme, flex: 1}}>
        {Platform.OS  === 'ios' && <View style={{ height: 30 }}></View>}
        <MainView>
          <TopView>
            <HeaderView>
              <LeftHeaderView >
                <SideIcon name='close' size={23}  onPress={() => this.props.closeModal('modalPassangers', pax.id, 'cancel')}  />
              </LeftHeaderView>
              <MidHeaderView>
                <Text type='bold'>Tambah Penumpang</Text>
              </MidHeaderView>
              <RightHeaderView  >
                <Text type='medium' onPress={() => this.props.closeModal('modalPassangers', pax.id, 'simpan')} >Simpan</Text>
              </RightHeaderView>
            </HeaderView>
          </TopView>
          <ScrollView>
            <AttentionView>
              <Container>
                <LeftView>
                  <BaseText><Ionicons name='ios-information-circle' /></BaseText>
                </LeftView>
                <RightView>
                  <BaseText>Masukkan nama penumpang seperti yang tertera pada ID.</BaseText>
                </RightView>
              </Container>
            </AttentionView>
            <PassangerView>
              <Input
                componentName="select"
                placeholder="Pilih Gelar"
                name="title"
                label="Gelar"
                error={pax.errors.title}
                value={pax.title}
                modalName="modalTitle"
                onSelect={this.onSelect}
              />
              <Input
                name="firstName"
                label='Nama Depan'
                placeholder=""
                keyboardType="default"
                returnKeyType="next"
                blurOnSubmit={false}
                value={pax.firstName}
                error={pax.errors.firstName}
                onChangeValue={this.onPassangerChange}
                onBlurValue={this.onValidatePassanger}
                maxLength={100}
              />
              <Input
                name="lastName"
                label='Nama Belakang'
                placeholder=""
                keyboardType="default"
                returnKeyType="next"
                blurOnSubmit={false}
                value={pax.lastName}
                error={pax.errors.lastName}
                onChangeValue={this.onPassangerChange}
                onBlurValue={this.onValidatePassanger}
                maxLength={100}
              />
              <Input
                name="phoneNumber"
                label='Nomor Telepon'
                placeholder=""
                keyboardType="phone-pad"
                returnKeyType="next"
                blurOnSubmit={false}
                value={pax.phoneNumber}
                error={pax.errors.phoneNumber}
                onChangeValue={this.onPassangerChange}
                onBlurValue={this.onValidatePassanger}
                maxLength={15}
              />
            </PassangerView>
            <Accordion
              touchableProps={{ activeOpacity: 1 }}
              sections={[{ name: 'this.props.title' }]}
              renderHeader={this.renderHeader}
              renderContent={() => this.renderContentPriceDetail(this.props.pax)}
              activeSection={this.state.activeSection[0]}
              onChange={(index) => this.onChangeActiveAccordion(index)}
            />
          </ScrollView>

          <Modal
            popup
            visible={this.state.modalTitle}
            onClose={() => this.closeModal('modalTitle')}
            animationType='slide-up'
            maskClosable
          >
            <ModalTitlePicker
              titles={this.props.titles}
              nameModal="modalTitle"
              type="adult"
              onSave={this.onPassangerChange}
              onClose={() => this.closeModal('modalTitle')}
              selected={pax.title}
            />
          </Modal>
          <Modal
            visible={this.state.modalCountryPicker}
            onClose={() => this.closeModal('modalCountryPicker')}
            animationType='slide'
          >
            <ModalCountryPicker
              onSelectedCountry={(val) => this.onPassangerChange('country', val.name)}
              onClose={() => this.closeModal('modalCountryPicker')}
            />
          </Modal>
        </MainView>
      </SafeAreaView>
    );
  }
}
