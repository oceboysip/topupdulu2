import React, { Component } from 'react';
import { Image, View, Modal, Switch } from 'react-native';
import Styled from 'styled-components';

import Color from '../Color';
import TouchableOpacity from '../Button/TouchableDebounce';
import ModalPassangerBus from './ModalPassangerBus';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Text from '../Text';

const MainView = Styled(View)`
  minHeight: 1;
  width: 100%;
  flexDirection: column;
`;

const SubtotalView = Styled(View)`
  margin: 22px 15px 10px
  alignItems: flex-start
  flexDirection: row
  elevation: 5px
`;

const TextView = Styled(SubtotalView)`
  margin: 0px 10px 0px 0px
  alignItems: flex-start
  flexDirection: row
`;
const MidHeaderView = Styled(View)`
  minWidth: 1;
  minHeight: 1;
  alignItems: flex-start;
`;

const LeftView = Styled(View)`
  width: 70%
  justifyContent: flex-start
  alignItems: flex-end
`;

const SwitchTouch = Styled(TouchableOpacity)`
  minWidth: 1;
  minHeight: 1;
`;

const LeftHeaderView = Styled(View)`
  width: 30%
  justifyContent: flex-start
  alignItems: flex-start
`;

const PassangerView = Styled(TouchableOpacity)`
  alignItems: flex-start
  backgroundColor: #FFFFFF
  padding: 16px
  width: 100%
  borderBottomColor: #DDDDDD
  borderBottomWidth: 1px
`;
const SamaPemesanView = Styled(View)`
  width: 100%;
  minHeight: 1;
  padding: 0px 16px 0px 16px;
  justifyContent: space-between;
  alignItems: center;
  flexDirection: row;
`;

const add = require('../../images/add.png');

export default class BusPassangers extends Component {

  constructor(props) {
    super(props);
    this.state = {
      switchOn: false,
      modalPassangers: false
    };
  }

  openModal = (modal) => {
    this.setState({
      [modal]: true
    })
  }

  closeModal = (modal, id, name) => {
    if(name !== 'simpan') this.props.deletePassanger(id)
    this.setState({
      [modal]: false
    })

    const { contact, ordering } = this.props;
    const sameTitle = (contact.title === ordering.title);
    const sameFirstName = (contact.firstName === ordering.firstName);
    const sameLastName = (contact.lastName === ordering.lastName);
    let phoneNumber = contact.countryCode + contact.phone;
    const samePhone = (phoneNumber === ordering.phoneNumber);

    if (sameTitle && sameFirstName && sameLastName && samePhone) {
      this.setState({ switchOn: true });
    }else {
      this.setState({ switchOn: false });
    }
  }

  validateForm() {
    const { title, firstName, lastName, countryCode, phone, email } = this.props.contact;
    let result;

    if (title !== '' && firstName !== '' && lastName !== '' && countryCode !== '' && phone !== '' && email !== '') result = true;
    else result = false;

    return result;
  }

  onSwitch(result) {
    if (result) {
      this.setState({ switchOn: !this.state.switchOn }, () => {
        this.props.onSameContact(this.state.switchOn);
        this.openModal('modalPassangers');
      })
    }else {
      this.props.modalFailSameContact(true);
      let timeout = setTimeout(() => {
        clearTimeout(timeout);
        this.props.modalFailSameContact(false);
      }, 3000);
    }
  }

  onPressPassenger(result) {
    if (result) {
      this.openModal('modalPassangers')
    }else {
      this.props.modalFailSameContact(true);
      let timeout = setTimeout(() => {
        clearTimeout(timeout);
        this.props.modalFailSameContact(false);
      }, 3000);
    }
  }

  render() {
    const { pax, id, index } = this.props;
    const result = this.validateForm();

    return (
      <MainView>
        {index === 0 && <SamaPemesanView>
          <MidHeaderView>
            <Text type='medium'>Sama dengan Pemesan</Text>
          </MidHeaderView>
          <Switch
            onValueChange={() => this.onSwitch(result)}
            value={this.state.switchOn}
            thumbColor={this.state.switchOn ? Color.theme : '#DDDDDD'}
          />
        </SamaPemesanView>}
        <PassangerView onPress={() => this.onPressPassenger(result)} key={pax.id} >
          {pax.passengers.firstName === '' &&
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <Image source={add} style={{ width: 15, height: 15, marginTop: 2, marginRight: 10 }} />
            <Text type='medium'>Nama Penumpang {id + 1}</Text>
            </View>
          }
          {pax.passengers.firstName !== '' && <TextView>
            <LeftHeaderView style={{ width: '90%' }}>
              <Text type='medium'>{pax.passengers.firstName} {pax.passengers.lastName}</Text>
            </LeftHeaderView>
            <LeftView style={{ width: '10%' }}>
              <Text type='bold' align='right'><Ionicons name='md-create' style={{fontSize:18}} /></Text>
            </LeftView>
          </TextView>}
          <Modal
            onRequestClose={() => this.closeModal('modalPassangers') }
            animationType="fade"
            transparent
            visible={this.state.modalPassangers}
          >
            <ModalPassangerBus
              closeModal={this.closeModal}
              titles={this.props.titles}
              onPassangerChange={this.props.onPassangerChange}
              pax={pax.passengers}
              id={id}
            />
          </Modal>
        </PassangerView>
      </MainView>
    );
  }
}
