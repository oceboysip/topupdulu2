import React, { Component } from 'react';
import { View } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { connect } from 'react-redux';

import FormatMoney from '../FormatMoney';
import Text from '../Text';
import Color from '../Color';
import Header from '../Header';
import TouchableOpacity from '../Button/TouchableDebounce';
import { Tabs, Tab, TabHeading, Radio } from 'native-base';
import { Row, Col } from '../Grid';

const MainView = Styled(View)`
  width: 100%;
  height: 100%;
  backgroundColor: #FAFAFA;
  flexDirection: column;
`

const CustomHeader = Styled(Header)`
  height: 69;
`

const TitleHeader = Styled(View)`
  width: 100%;
  height: 100%;
  flexDirection: row;
  alignItems: center;
  justifyContent: center;
  position: absolute;
`;

const ChildHeader = Styled(View)`
  width: 100%;
  alignItems: center;
  top: 20;
`

const TitleText = Styled(Text)`
  lineHeight: 16;
  letterSpacing: 0;
  color: #FFFFFF;
`

const ContentText = Styled(Text)`
  fontSize: 12;
  lineHeight: 16;
`

const ArrowRight = Styled(FontAwesome5)`
  fontSize: 10;
  color: #FFFFFF;
  paddingHorizontal: 8;
`

const BackgroundView = Styled(View)`
  width: 100%;
  height: 11;
  backgroundColor: ${Color.theme}
`

const ResultView = Styled(View)`
  flex: 1;
`;

const BottomView = Styled(View)`
  width: 100%
  backgroundColor: #FFFFFF
  alignItems: center
  justifyContent: center
  flexDirection: row
  elevation: 2
  bottom: 0
  position: absolute
`;

const BottomContentView = Styled(View)`
  width: 100%
  height: 63
  flexDirection: row
  justifyContent: space-between
  alignItems: center
  backgroundColor: #FFFFFF
  padding: 8px 16px 8px
  elevation: 20
`;

const MidContentView = Styled(View)`
  width: 50%
  height: 100%
  flexDirection: column
  alignItems: flex-start
  justifyContent: center
`;

const ButtonRadius = Styled(TouchableOpacity)`
  borderRadius: 30px;
  width: 100%
  backgroundColor: #DDDDDD;
  height: 100%
  justifyContent: center
`;

const PriceText = Styled(Text)`
  color: #FF425E
`

const SubmitText = Styled(Text)`
  color: #FFFFFF;
`

const RadioView = Styled(TouchableOpacity)`
  width: 100%;
  minHeight: 1;
  paddingVertical: 16;
  borderColor: #DDDDDD;
  borderBottomWidth: 0.5;
  alignItems: center;
  flexDirection: row;
`

const LeftRadio = Styled(View)`
  paddingHorizontal: 16;
`

const RightRadio = Styled(View)`
  paddingHorizontal: 16;
  position: absolute;
  right: 0;
`

const CenterCol = Styled(Col)`
  width: 100%;
  flexDirection: column;
  alignItems: flex-start;
`

const CenterRadio = Styled(View)`
  alignItems: flex-start;
`;

class SelectBusTerminal extends Component {
  static navigationOptions = { header: null };
  constructor(props) {
    super(props);
    this.state = {
      type: props.navigation.state.params.type === 'departure',
      initialPage: 0,
      activeTab: 0,
      boarding_point: {
        point_id: null,
        point_name: null,
        point_time: null,
        point_address: null,
        point_landmark: null,
        point_contact: null
      },
      dropping_point: {
        point_id: null,
        point_name: null,
        point_time: null,
        point_address: null,
        point_landmark: null,
        point_contact: null
      }
    }
  }

  onSelectedBoardingPoint(boarding_point) {
    this.setState({ boarding_point });
    if (!this.state.dropping_point.point_id) this.setState({ activeTab: 1});
  }

  onSelectedDroppingPoint(dropping_point) {
    this.setState({ dropping_point });
    if (!this.state.boarding_point.point_id) this.setState({ activeTab: 0 });
  }

  renderDeparture(boarding_point){
    return boarding_point.map((bp, idx) =>
      <RadioView
        key={idx}
        onPress={() => this.onSelectedBoardingPoint(bp)}
        style={this.state.boarding_point.point_id === bp.point_id && {backgroundColor: '#DDDDDD'}}
      >
        <LeftRadio>
          <Radio disabled selected={this.state.boarding_point.point_id === bp.point_id} selectedColor={'#000000'}/>
        </LeftRadio>
        <CenterRadio>
          <ContentText type='medium'>{bp.point_name}</ContentText>
          <ContentText type='medium'>{bp.point_address}</ContentText>
        </CenterRadio>
        <RightRadio>
          <ContentText type='medium'>{Moment(bp.point_time).format('hh:MM')}</ContentText>
        </RightRadio>
      </RadioView>
    )
  }

  renderArrived(dropping_point){
    return dropping_point.map((dp, idx) =>
      <RadioView
        key={idx}
        onPress={() => this.onSelectedDroppingPoint(dp)}
        style={this.state.dropping_point.point_id === dp.point_id && {backgroundColor: '#DDDDDD'}}
      >
        <LeftRadio>
          <Radio disabled selected={this.state.dropping_point.point_id === dp.point_id} selectedColor={'#000000'}/>
        </LeftRadio>
        <CenterRadio>
          <ContentText type='medium'>{dp.point_name}</ContentText>
          <ContentText type='medium'>{dp.point_address}</ContentText>
        </CenterRadio>
        <RightRadio>
          <ContentText type='medium'>{Moment(dp.point_time).format('hh:MM')}</ContentText>
        </RightRadio>
      </RadioView>
    )
  }

  renderAllTabs(){
    const { activeTab } = this.state;
    const { boarding_point, dropping_point } = this.props.navigation.state.params.busBpDp;
    const tabs = [
      {title: 'Pilih Titik Berangkat', children: this.renderDeparture(boarding_point)},
      {title: 'Pilih Titik Tiba', children: this.renderArrived(dropping_point)}
    ];

    return tabs.map((tab, idx) =>
      <Tab
        key={idx}
        heading={
          <TabHeading style={{backgroundColor: Color.theme}}>
            <Text style={{color: '#FFFFFF'}} type={activeTab !== idx ? 'medium' : 'bold'}>{tab.title}</Text>
          </TabHeading>
        }
        tabStyle={{backgroundColor: Color.theme, elevation: 0}}
        activeTabStyle={{backgroundColor: Color.theme}}
      >
        {tab.children}
      </Tab>
    );
  }

  renderMainTabs(){
    return(
      <Tabs
        initialPage={this.state.initialPage}
        page={this.state.activeTab}
        prerenderingSiblingsNumber={Infinity}
        tabContainerStyle={{elevation: 0}}
        tabBarUnderlineStyle={{backgroundColor: '#000000', height: 2}}
        onChangeTab={({ i }) => this.setState({activeTab: i})}
      >
        {this.renderAllTabs()}
      </Tabs>
    )
  }

  onBeforeReviewBooking(){
    const { user, navigation } = this.props;

    if (user.guest) {
      navigation.navigate('LoginScreen', { loginFrom: 'bus', afterLogin: () => navigation.navigate('BusReviewBooking') });
    }else {
      navigation.navigate('BusReviewBooking');
    }
  }

  onContinueBooking(){
    if (this.state.type) {
      console.log(this.state, 'departure');
      this.props.selectedBpDpDepartureBus(this.state);
      if(this.props.searchBus.trip === 1){
        this.props.noReturnBus();
        this.onBeforeReviewBooking();
      }else {
        this.props.navigation.navigate({
          routeName: 'BusResultScreen',
          params: { type: 'return'},
          key: 'BusResultScreenReturn'
        })
      }
    }else {
      console.log(this.state, 'return');
      this.props.selectedBpDpReturnBus(this.state);
      this.onBeforeReviewBooking();
    }
  }

  render(){
    console.log(this.state, 'state SelectedTerminal');
    console.log(this.props, 'props SelectedTerminal');

    const { busBpDp, subTotal } = this.props.navigation.state.params;
    const { origin, destination } = this.props.searchBus;
    const { type, boarding_point, dropping_point } = this.state;
    const { departureBus, returnBus } = this.props.selectedBus;

    let onSelectedTrue = (boarding_point.point_id !== null && dropping_point.point_id !== null)

    return(
      <MainView>
        <CustomHeader>
          <TitleHeader>
            <TitleText type='bold'>{type ? origin.name : destination.name} </TitleText>
            <ArrowRight name='arrow-right'/>
            <TitleText type='bold'>  {type ? destination.name : origin.name}</TitleText>
          </TitleHeader>
          <ChildHeader>
            <ContentText style={{color: '#FFFFFF'}} type='medium'>{type ? departureBus.buses_name : returnBus.buses_name}</ContentText>
          </ChildHeader>
        </CustomHeader>

        <BackgroundView />

        <ResultView>
          {this.renderMainTabs()}
        </ResultView>

        <BottomView>
          <BottomContentView>
            <MidContentView>
              <Text type='medium'>Subtotal</Text>
              <PriceText type='bold'>{FormatMoney.getFormattedMoney(subTotal)}</PriceText>
            </MidContentView>
            <MidContentView>
              <ButtonRadius
                style={onSelectedTrue && {backgroundColor: '#231F20'}}
                disabled={onSelectedTrue ? false : true}
                onPress={() => this.onContinueBooking()}
              >
                <SubmitText>Selanjutnya</SubmitText>
              </ButtonRadius>
            </MidContentView>
          </BottomContentView>
        </BottomView>

      </MainView>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state['user.auth'].login.user,
  selectedBus: state.selectedBus,
  searchBus: state.searchBus
});

const mapDispatchToProps = (dispatch, props) => ({
  selectedBpDpDepartureBus: (data) => dispatch({ type: 'SELECTED_BPDP_DEPARTURE_BUS', data }),
  selectedBpDpReturnBus: (data) => dispatch({ type: 'SELECTED_BPDP_RETURN_BUS', data }),
  noReturnBus: () => dispatch({ type: 'NO_RETURN_BUS' })
});

export default connect(mapStateToProps, mapDispatchToProps)(SelectBusTerminal);
