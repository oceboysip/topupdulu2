import React, { Component } from 'react';
import { View, Image } from 'react-native';
import Styled from 'styled-components';
import gql from 'graphql-tag';

import Text from '../Text';
import Color from '../Color';
import Button from '../Button';
import TouchableOpacity from '../Button/TouchableDebounce';
import Client from '../../state/apollo';
import ModalIndicator from '../Modal/ModalIndicator';

const SeatLabel = Styled(Text)`
  fontSize: 12;
`;

const ContentText = Styled(Text)`
  fontSize: 12;
  lineHeight: 16;
`;

const EmptySeatInfo = Styled(View)`
  height: 35;
  width: 31.9%;
  justifyContent: center;
  borderWidth: 1;
  borderColor: #707070;
  marginBottom: 24;
`;

const FilledSeatInfo = Styled(EmptySeatInfo)`
  backgroundColor: #DDDDDD;
  marginHorizontal: 0;
`;

const SelectedSeatInfo = Styled(EmptySeatInfo)`
  backgroundColor: ${Color.theme};
  marginHorizontal: 0;
`;

const MainSeatRow = Styled(View)`
  width: 100%;
  height: 50;
  justifyContent: center;
  marginBottom: 4;
  flexDirection: row;
  paddingHorizontal: 2;
`;

const ViewSelectSeat = Styled(View)`
  width: 100%;
  flexDirection: row;
  justifyContent: space-between;
  marginBottom: 2%;
`;

const MainSelectSeat = Styled(TouchableOpacity)`
  width: 15%;
  aspectRatio: 1;
  borderColor: #707070;
  borderWidth: 1;
  justifyContent: center;
`;

const MiniSubtitleView = Styled(View)`
  width: 100%;
  alignItems: center;
  paddingHorizontal: 12;
  marginVertical: 24;
`;

const WarningImage = Styled(Image)`
  width: 95.99;
  height: 96;
`;

const WarningButton = Styled(Button)`
  height: 40;
  width: 150;
  marginTop: 16;
`;

const BigText = Styled(Text)`
  fontSize: 18;
  marginTop: 40;
  marginBottom: 11;
`;

const warningImage = require('../../images/warning-flight.png');

export default class TypeSeat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedSeat: [],
      busAvailSeat: [],
      loading: true
    }
  }

  componentDidMount() {
    this.getBusAvailSeat();
  }

  getBusAvailSeat(){
    let variables = {
      seatCode: this.props.data.seat_code
    }

    const getBusAvailSeat = gql`
      query(
        $seatCode: String
      ){
        busAvailSeat(
          seatCode: $seatCode
        ){
          data{
            code
            seat_position
            booked
            type
            row
            column
            category
            rate_code
            base_final_price
            base_additional_price
            base_ppn
            base_transaction_fee
            base_discount
            base_price
          }
        }
      }
    `;

    Client.query({
      query: getBusAvailSeat,
      variables
    }).then(res => {
      const data = res.data.busAvailSeat.data;
      let busAvailSeat = [];

      if (data) {
        for (var i = 0; i < data.length; i++) {
          if (data[i].code !== 'null') busAvailSeat.push(data[i]);
        }
      }

      this.setState({ busAvailSeat, loading: false })
    }).catch(err => {
      console.log(err, 'err');
    })
  }

  onSelectedSeat(seatItem){
    let selectedSeat = this.state.selectedSeat;
    let idx = selectedSeat.findIndex(item => item.rate_code === seatItem.rate_code);

    if (idx === -1 ) {
      if (selectedSeat.length > 4) {
        return false;
      }else {
        selectedSeat.push(seatItem);
      }
    }else {
      selectedSeat.splice(idx, 1);
    }

    this.setState({ selectedSeat });

    this.props.onSelectedSeat(this.state.selectedSeat);
  }

  seatsAvailable(props, busAvailSeat){
    // const { row, column } = props.data;
    // let notFill = [], tempArraySeat = [], allSeatData = [], newAllSeatData = [];

    // const item = {
    //   notFill: props.notFill || [2],
    //   sideDoor: props.sideDoor || [0, 1, 2],
    //   lastSeat: props.lastSeat || [],
    // }

    let seatBus = [];

    if (busAvailSeat.length > 0) {
      //-------------------------------------seat with row & col
      // if (row !== 'null' && row > 0 && column !== 'null' && column > 0) {
      // for (var i = 0; i < props.data.row; i++) {
      //   notFill.push(item.notFill);
      // }
      // if (this.props.sideDoorIndex) notFill.splice(props.data.row - 2, 1, item.sideDoor);
      // if (this.props.lastSeatIndex) notFill.splice(props.data.row, 1, item.lastSeat);

      //   for (var i = 0; i < props.data.row; i++) {
      //     for (var j = 0; j < props.data.column; j++) {
      //       allSeatData.push({ row: i+1, column: j+1 });
      //     }
      //   }
      //
      //   for (var i = 0; i < allSeatData.length; i++) {
      //     if (busAvailSeat.filter(bas => bas.row === allSeatData[i].row && bas.column === allSeatData[i].column && bas.category !== 'EMPTY').length > 0) {
      //       let fieldBus = (busAvailSeat.filter(bas => bas.row === allSeatData[i].row && bas.column === allSeatData[i].column));
      //       newAllSeatData.push({
      //         row: allSeatData[i].row,
      //         column: allSeatData[i].column,
      //         fill: true,
      //         seat_position: fieldBus[0].seat_position,
      //         booked: fieldBus[0].booked,
      //         category: fieldBus[0].category,
      //         rate_code: fieldBus[0].rate_code,
      //         base_final_price: fieldBus[0].base_final_price
      //       })
      //     }else {
      //       newAllSeatData.push({
      //         row: allSeatData[i].row,
      //         column: allSeatData[i].column,
      //         fill: false,
      //         seat_position: 'EMPTY',
      //         booked: true,
      //         category: 'EMPTY',
      //         rate_code: null,
      //         base_final_price: 0
      //       })
      //     }
      //   }
      //
      //   for (const data of newAllSeatData) {
      //     tempArraySeat.push(data)
      //     if (tempArraySeat.length === props.data.column) {
      //       seatBus.push(tempArraySeat)
      //       tempArraySeat = []
      //     }
      //   }
      //
      //   if (tempArraySeat.length > 0) seatBus.push(tempArraySeat)
      // }
      //-------------------------------------end of seat with row & col

      //-------------------------------------seat default
      // else {=================================================================
        let newSeatData = [];

        busAvailSeat.map((val, i) => {
          newSeatData[val.row] = [];
        });

        busAvailSeat.map((val, i) => {
          newSeatData[val.row][val.column] = {};
        });

        busAvailSeat.map((val, i) => {
          if(val.category === 'EMPTY'){
            newSeatData[val.row][val.column] = {
              ...val,
              fill: false
            };
          }else if (val.category === 'DRIVER SEAT') {
            newSeatData[val.row][val.column] = {
              ...val,
              fill: true,
              booked: true,
              seat_position: 'Supir'
            };
          }else{
            newSeatData[val.row][val.column] = {
              ...val,
              fill: true
            };
          }

          seatBus[val.row] = newSeatData[val.row];
        });
      // }
    }

    return seatBus;
  }

  renderEmptySeat(){
    return(
      <MiniSubtitleView>
        <WarningImage source={warningImage} />
        <BigText type='bold'>Kursi Tidak Tersedia</BigText>
        <WarningButton onPress={() => this.props.onBackPress()}>Kembali</WarningButton>
      </MiniSubtitleView>
    )
  }

  renderBusSeat(seatBus, selectedSeat){
    const bg = {
      theme: { backgroundColor: Color.theme },
      grey: { backgroundColor: '#DDDDDD' },
      transparent: { backgroundColor: 'transparent' }
    }

    return seatBus.map((seatBusMap, indexSeat) =>
      <ViewSelectSeat key={indexSeat}>
        {seatBusMap.map((item, index) =>
          <MainSelectSeat
            key={index}
            onPress={() => this.onSelectedSeat(item)}
            disabled={item.booked || item.category === 'DRIVER SEAT' || item.category === 'EMPTY' ? true : false}
            style={[ !item.fill ? {borderWidth: 0} : item.booked ? bg.grey : selectedSeat.filter(ss => ss.rate_code === item.rate_code).length > 0 ? bg.theme : bg.transparent ]}
          >
            <SeatLabel type='medium'>{item.fill && item.seat_position}</SeatLabel>
          </MainSelectSeat>
        )}
      </ViewSelectSeat>
    )
  }

  resultSeats(seatsAvailable, selectedSeat){
    if (this.state.busAvailSeat[0].rate_code === "null") return this.renderEmptySeat();
    return this.renderBusSeat(seatsAvailable, selectedSeat);
  }

  render(){
    const { selectedSeat, busAvailSeat, loading } = this.state;
    let seatsAvailable = this.seatsAvailable(this.props, busAvailSeat);
    console.log(busAvailSeat, 'busAvailSeat');

    return(
      <View style={{width: '100%'}}>
        <View style={{width: '100%', flexDirection: 'row', justifyContent: 'space-between'}}>
          <FilledSeatInfo>
            <ContentText type='medium'>Terisi</ContentText>
          </FilledSeatInfo>
          <EmptySeatInfo>
            <ContentText type='medium'>Kosong</ContentText>
          </EmptySeatInfo>
          <SelectedSeatInfo>
            <ContentText type='medium'>Dipilih</ContentText>
          </SelectedSeatInfo>
        </View>
        {busAvailSeat.length > 0 && this.resultSeats(seatsAvailable, selectedSeat)}
        {busAvailSeat.length === 0 && !loading && this.renderEmptySeat()}
        <ModalIndicator
          visible={busAvailSeat.length === 0 && loading}
          type="large"
          message="Memuat ketersediaan kursi"
        />
      </View>
    );
  }
}
