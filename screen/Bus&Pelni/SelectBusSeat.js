import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';
import { connect } from 'react-redux';
import gql from 'graphql-tag';

import FormatMoney from '../FormatMoney';
import Text from '../Text';
import Color from '../Color';
import Header from '../Header';
import TypeSeat from './TypeSeat';
import TouchableOpacity from '../Button/TouchableDebounce';
import { Row, Col } from '../Grid';
import Client from '../../state/apollo';

const MainView = Styled(View)`
  width: 100%;
  height: 100%;
  backgroundColor: #FAFAFA;
  flexDirection: column;
`;

const CustomHeader = Styled(Header)`
  height: 69;
`;

const TitleHeader = Styled(View)`
  width: 100%;
  height: 100%;
  flexDirection: row;
  alignItems: center;
  justifyContent: center;
  position: absolute;
`;

const ChildHeader = Styled(View)`
  top: 20;
`

const PriceText = Styled(Text)`
  color: #FF425E
`

const SubmitText = Styled(Text)`
  color: #FFFFFF;
`

const TitleText = Styled(Text)`
  fontSize: 14;
  lineHeight: 16;
  letterSpacing: 0;
  color: ${Color.white};
`

const ContentText = Styled(Text)`
  fontSize: 12;
  lineHeight: 16;
  color: ${Color.white};
`

const MainInformationView = Styled(View)`
  width: 100%;
  height: 60;
  backgroundColor: ${Color.theme};
  paddingBottom: 16;
  justifyContent: space-between;
  alignItems: center;
`

const InformationDeepView = Styled(View)`
  width: 100%;
  minHeight: 1;
  padding: 8px 16px 8px 16px;
`

const StyleRow = Styled(Row)`
  marginBottom: 8
`

const LeftCol = Styled(Col)`
  alignItems: flex-start;
`

const RightCol = Styled(Col)`
  alignItems: flex-end;
`

const MainContentView = Styled(View)`
  width: 100%;
  minHeight: 1;
  marginBottom: 45;
`

const DriverView = Styled(View)`
  width: 100%;
  height: 45;
  justifyContent: center;
  borderBottomWidth: 0.5;
  borderColor: #DDDDDD;
`

const DetailView = Styled(View)`
  width: 100%;
  minHeight: 1;
  padding: 16px;
`

const BottomView = Styled(View)`
  width: 100%
  backgroundColor: #FFFFFF
  alignItems: center
  justifyContent: center
  flexDirection: row
  elevation: 2
  bottom: 0
  position: absolute
`;

const BottomContentView = Styled(View)`
  width: 100%
  height: 63
  flexDirection: row
  justifyContent: space-between
  alignItems: center
  backgroundColor: #FFFFFF
  padding: 8px 16px 8px
  elevation: 20
`;

const MidContentView = Styled(View)`
  width: 50%
  height: 100%
  flexDirection: column
  alignItems: flex-start
  justifyContent: center
`;

const ButtonRadius = Styled(TouchableOpacity)`
  borderRadius: 30px;
  width: 100%
  backgroundColor: #DDDDDD;
  height: 100%
  justifyContent: center
`;


class SelectBusSeat extends Component {
  static navigationOptions = { header: null };
  constructor(props) {
    super(props);
    this.state = {
      type: props.navigation.state.params.type === 'departure',
      selectedSeat: [],
      busBpDp: null
    }

    this.getbusBpDpPointAvail();
  }

  getbusBpDpPointAvail(){
    const { type } = this.state;
    const { departureDate, origin, destination, returnDate } = this.props.searchBus;

    const busBpDpPointAvail = gql`
      query(
        $availDate: String
        $stationDeparture: String
        $stationArrival: String
      ){
        busBpDpPointAvail(
          availDate: $availDate
          stationDeparture: $stationDeparture
          stationArrival: $stationArrival
        ){
          data{
            boarding_point{
              point_id
              point_name
              point_time
              point_address
              point_landmark
              point_contact
            }
            dropping_point{
              point_id
              point_name
              point_time
              point_address
              point_landmark
              point_contact
            }
          }
        }
      }
    `;

    let variables = {
      availDate: type ? Moment(departureDate) : Moment(returnDate),
      stationDeparture: type ? origin.code : destination.code,
      stationArrival: type ? destination.code : origin.code
    }

    Client.query({
      query: busBpDpPointAvail,
      variables
    }).then(res =>{
      let data = res.data.busBpDpPointAvail.data;
      if (data) {
        if (data.boarding_point.filter(val => val.point_id !== 'null').length > 0 && data.dropping_point.filter(val => val.point_id !== 'null').length > 0) {
          this.setState({busBpDp: data})
        }
      }
    }).catch(err => {
      console.log(err, 'error');
    })
  }

  getShortMonth(month) {
    return Moment(month, 'M').format('MMMM').substring(0, 3);
  }

  capitalizeWords(str){
    let newStr = str.toLowerCase();
    return newStr.charAt(0).toUpperCase() + newStr.slice(1);
  }

  splitSentence(sentence) {
    let newSentence = '';
    sentence.split(' ').map((word, i) => newSentence += ' ' + this.capitalizeWords(word) );
    return newSentence;
  }

  onBeforeReviewBooking(){
    const { user, navigation } = this.props;

    if (user.guest) {
      navigation.navigate('LoginScreen', { loginFrom: 'bus', afterLogin: () => navigation.navigate('BusReviewBooking') });
    }else {
      navigation.navigate('BusReviewBooking');
    }
  }

  onContinueBooking(){
    if (this.props.navigation.state.params.type === 'departure') {
      if(this.props.searchBus.trip === 1){
        this.props.noReturnBus();
        this.onBeforeReviewBooking();
      }else {
        this.props.navigation.navigate({
          routeName: 'BusResultScreen',
          params: { type: 'return'},
          key: 'BusResultScreenReturn'
        })
      }
    }else {
      this.onBeforeReviewBooking();
    }
  }

  onSubmit(subTotal){
    const { navigate, state } = this.props.navigation;
    const { type, selectedSeat } = this.state;

    type ? this.props.updateSelectedDepartureSeatBus(selectedSeat) : this.props.updateSelectedReturnSeatBus(selectedSeat)

    if (this.state.busBpDp !== null) {
      navigate({
        routeName: 'SelectBusTerminal',
        params: {
          selectedSeat: this.state.selectedSeat,
          data: state.params.data,
          busBpDp: this.state.busBpDp,
          subTotal: subTotal,
          type: state.params.type,
        },
        key: `SelectBusTerminal${state.params.type}`
      })
    }else {
      this.onContinueBooking();
    }
  }

  render(){
    const { data } = this.props.navigation.state.params;
    const { origin, destination, departureDate, returnDate } = this.props.searchBus;
    const { departureBus, returnBus, departureSeatBus } = this.props.selectedBus;
    const { type, selectedSeat } = this.state;
    let sameWithDeparture = false;

    if (type) {
      if (selectedSeat.length > 0) sameWithDeparture = true;
      else sameWithDeparture = false;
    }else {
      if (selectedSeat.length === departureSeatBus.length) sameWithDeparture = true;
      else sameWithDeparture = false;
    }

    let subTotal = 0;
    if (selectedSeat.length > 0) for(const ss of selectedSeat) subTotal += ss.base_final_price
    else subTotal = 0;

    console.log(this.state, 'state SelectBusSeat');
    console.log(this.props, 'props SelectBusSeat');

    return(
      <MainView>
        <CustomHeader title={type ? 'Pilih Kursi Pergi' : 'Pilih Kursi Pulang'} showLeftButton />
        <ScrollView>
          <MainInformationView>
              <TitleText align='right' type='medium'>{type ? this.splitSentence(departureBus.buses_name) : this.splitSentence(returnBus.buses_name)}</TitleText>
              <View>
                {type ? <TitleText align='left' type='medium'>{this.getShortMonth(departureDate)} {departureDate.format('D')}<ContentText type='medium'> {departureDate.format('ddd YYYY')} - </ContentText>{Moment(departureBus.departure_time).format('hh:mm')}</TitleText>
                : <TitleText align='left' type='medium'>{this.getShortMonth(returnDate)} {returnDate.format('D')}<ContentText type='medium'> {returnDate.format('ddd YYYY')} - </ContentText>{Moment(returnBus.departure_time).format('hh:mm')}</TitleText>}
              </View>
          </MainInformationView>

          <MainContentView>
            <DriverView>
              <Text type='medium'>Depan</Text>
            </DriverView>
            <DetailView>
              <TypeSeat
                data={data}
                onSelectedSeat={(selectedSeat) => this.setState({selectedSeat})}
                onBackPress={() => this.props.navigation.pop()}
                notFill={[2]}
                sideDoor={[0, 1, 2]}
                lastSeat={[]}
              />
            </DetailView>
          </MainContentView>
        </ScrollView>

        <BottomView>
          <BottomContentView>
            <MidContentView>
              <Text type='medium'>Subtotal</Text>
              <PriceText type='bold'>{FormatMoney.getFormattedMoney(subTotal)}</PriceText>
            </MidContentView>
            <MidContentView>
              <ButtonRadius
                style={sameWithDeparture && {backgroundColor: '#231F20'}}
                disabled={sameWithDeparture ? false : true}
                onPress={() => this.onSubmit(subTotal)}
              >
                <SubmitText>Selanjutnya</SubmitText>
              </ButtonRadius>
            </MidContentView>
          </BottomContentView>
        </BottomView>
      </MainView>
    )
  }
}

const mapStateToProps = (state) => ({
  user: state['user.auth'].login.user,
  searchBus: state.searchBus,
  busResult: state.busResult,
  selectedBus: state.selectedBus
});

const mapDispatchToProps = (dispatch, props) => ({
  updateSelectedDepartureSeatBus: (data) => dispatch({ type: 'SELECTED_DEPARTURE_SEAT_BUS', data }),
  updateSelectedReturnSeatBus: (data) => dispatch({ type: 'SELECTED_RETURN_SEAT_BUS', data }),
  noReturnBus: () => dispatch({ type: 'NO_RETURN_BUS' })
});

export default connect(mapStateToProps, mapDispatchToProps)(SelectBusSeat);
