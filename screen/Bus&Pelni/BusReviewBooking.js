import React, { Component } from 'react';
import { Image, View, TextInput, ScrollView, Modal, Platform } from 'react-native';
import Styled from 'styled-components';
import { connect } from 'react-redux';
import Moment from 'moment';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import TouchableOpacity from '../Button/TouchableDebounce';
import Button from '../Button';
import ModalIndicator from '../Modal/ModalIndicator';
import ContactBooking from '../ContactBooking';
import ModalInformation from '../Modal/ModalInformation';
// import ModalBusesDetail from '../Modal/ModalBusesDetail';
import BusPassangers from './BusPassangers';
import { getTitles } from '../../state/actions/get-titles';
import { bookBus } from '../../state/actions/bus/bookBus';
import Text from '../Text';
import Color from '../Color';
import Header from '../Header';
import FormatMoney from '../FormatMoney';

const ArrowRightIcon = Styled(FontAwesome)`
  fontSize: 16;
  color: ${Color.text};
`;

const MainView = Styled(View)`
  width: 100%;
  height: 100%;
  alignItems: center;
  backgroundColor: #FFFFFF;
`;

const BaseText = Styled(Text)`
  fontSize: 12px
`;

const WhiteText = Styled(Text)`
  color: #FFFFFF
  lineHeight: 12px;
  fontSize: 11px;
`;

const AbsoluteView = Styled(View)`
  position: absolute;
  top: 20;
  zIndex:5;
  width: 100%;
`;

const HeaderView = Styled(TouchableOpacity)`
  minHeight: 1px;
  backgroundColor: #FFFFFF;
  padding: 10px;
  elevation: 5px;
  margin: 0 16px;
  flexDirection: row;
  borderWidth: ${Platform.OS === 'ios' ? 1 : 0 };
  borderBottomWidth: ${Platform.OS === 'ios' ? 2 : 0 };
  borderColor: #DDDDDD;
`;

const LeftHeaderView = Styled(View)`
  width: 30%;
  justifyContent: flex-start;
  alignItems: flex-start;
`;

const MidHeaderView = Styled(View)`
  width: 70%;
  justifyContent: flex-start;
  alignItems: flex-start;
`;

const MidSubHeader = Styled(View)`
`;

const MidSubHeaderText = Styled(Text)`
  fontSize:11px;
`;

const RightHeaderView = Styled(View)`
  width: 100%
  height: 100%
  justifyContent: center
  alignItems: flex-end
`;

const BlackHeaderView = Styled(View)`
  backgroundColor: #000000;
  width: 70px;
  height: 20px;
  justifyContent: center;
`;

const ContentView = Styled(View)`
`;

// add in WrapContentView
// justifyContent: center;
// alignItems: center;
const WrapContentView = Styled(View)`
  flex: 1;
  height: 65px;
`;

const IconView = Styled(View)`
  justifyContent: center
`;

const PerContent = Styled(View)`
  flexDirection: row;
`;

const ContactView = Styled(View)`
  width: 100%
  backgroundColor: #FFFFFF
  borderBottomColor: #DDDDDD
  borderBottomWidth: 1px
`;

const ContactViewPemesan = Styled(View)`
  width: 100%
  backgroundColor: #FFFFFF
  borderBottomColor: #FAF9F9
  borderBottomWidth: 8px
`;

const SubContactView = Styled(View)`
  margin: 22px 15px
  alignItems: flex-start
`;

const SubtotalView = Styled(View)`
  margin: 22px 15px 10px
  alignItems: flex-start
  flexDirection: row
  elevation: 5px
`;

const TextView = Styled(SubtotalView)`
  margin: 0px 10px 0px 0px
  alignItems: flex-start
  flexDirection: row
`;

const LeftView = Styled(MidHeaderView)`
  alignItems: flex-end
`;

const ButtonText = Styled(Text)`
  fontSize: 14px
  color: #FFFFFF
  lineHeight: 34px
`;

const PriceText = Styled(Text)`
  color: #FF425E
`;

const ButtonRadius = Styled(TouchableOpacity)`
  borderRadius: 30px;
  width: 100%;
  backgroundColor: #231F20;
  height: 100%;
  justifyContent: center;
`;

const NormalText = Styled(Text)`
`;

const CustomButton = Styled(Button)`
    width: 100%;
    height: 100%;
`;

const ButtomView = Styled(View)`
  width: 100%
  backgroundColor: #FFFFFF
  alignItems: center
  justifyContent: center
  flexDirection: row
`;

const PassangerView = Styled(TouchableOpacity)`
  alignItems: flex-start
  backgroundColor: #FFFFFF
  padding: 16px
  width: 100%
  borderBottomColor: #DDDDDD
  borderBottomWidth: 1px
`;

const MainBookingView = Styled(View)`
    width: 100%;
    height: 63;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    backgroundColor: #FFFFFF;
    padding: 8px 16px 8px 16px;
    elevation: 20;
`;

const HalfBookingView = Styled(View)`
    width: 50%;
    height: 100%;
    flexDirection: column;
    alignItems: flex-start;
    justifyContent: center;
`;

const titles = [
  { abbr: 'MR', name: 'Mr', isAdult: true },
  { abbr: 'MRS', name: 'Mrs', isAdult: true },
  { abbr: 'MS', name: 'Ms', isAdult: true }
];

  class BusReviewBooking extends Component {
    static navigationOptions = { header: null };

    constructor(props){
      super(props);
      const { departureSeatBus, departureBus, departureBpBus, departureDpBus,
      returnSeatBus, returnBus, returnBpBus, returnDpBus } = props.selectedBus;

      let seats = [], j = 0, k = 0;
      let passengers = {};

      passengers = {
        title: '',
        fullName: '',
        firstName: '',
        lastName: '',
        birthDate: '',
        address: '',
        city: '',
        country: 'Indonesia',
        postalCode: '',
        phoneNumber: '+62',
        age: '',
        email: '',
        idCard: '',
        errors: {
          title: null,
          firstName: null,
          lastName: null,
          phoneNumber: null
        }
      };

      for (let i = 0; i < departureSeatBus.length; i++) {
        seats.push({
          no: i,
          id: j++,
          busesCode: departureBus.buses_code,
          busesName: departureBus.buses_name,
          departureCode: props.searchBus.origin.code,
          arrivalCode: props.searchBus.destination.code,
          departureName: props.searchBus.origin.name,
          arrivalName: props.searchBus.destination.name,
          departureDate: departureBus.departure_time,
          arrivalDate: departureBus.arrival_time,
          seatPosition: departureSeatBus[i].seat_position,
          rateCode: departureSeatBus[i].rate_code,
          date: props.searchBus.departureDate,
          type: 'TRIP_AWAY',
          boardingPoint: {
            pointId: departureBpBus ? departureBpBus.boarding_point.point_id : null,
            pointName: departureBpBus ? departureBpBus.boarding_point.point_name : null,
            pointTime: departureBpBus ? departureBpBus.boarding_point.point_time : null,
            pointAddress: departureBpBus ? departureBpBus.boarding_point.point_address : null,
            pointLandmark: departureBpBus ? departureBpBus.boarding_point.point_landmark : null,
            pointContact: departureBpBus ? departureBpBus.boarding_point.point_contact : null
          },
          droppingPoint: {
            pointId: departureDpBus ? departureDpBus.dropping_point.point_id : null,
            pointName: departureDpBus ? departureDpBus.dropping_point.point_name : null,
            pointTime: departureDpBus ? departureDpBus.dropping_point.point_time : null,
            pointAddress: departureDpBus ? departureDpBus.dropping_point.point_address : null,
            pointLandmark: departureDpBus ? departureDpBus.dropping_point.point_landmark : null,
            pointContact: departureDpBus ? departureDpBus.dropping_point.point_contact : null
          },
          passengers
        });
      }

      if (returnSeatBus) {
        for (var i = 0; i < returnSeatBus.length; i++) {
          seats.push({
            no: i,
            id: k++,
            busesCode: returnBus.buses_code,
            busesName: returnBus.buses_name,
            departureCode: props.searchBus.destination.code,
            arrivalCode: props.searchBus.origin.code,
            departureName: props.searchBus.destination.name,
            arrivalName: props.searchBus.origin.name,
            departureDate: returnBus.departure_time,
            arrivalDate: returnBus.arrival_time,
            seatPosition: returnSeatBus[i].seat_position,
            rateCode: returnSeatBus[i].rate_code,
            date: props.searchBus.returnDate,
            type: 'RETURN_TRIP',
            boardingPoint: {
              pointId: returnBpBus ? returnBpBus.boarding_point.point_id : null,
              pointName: returnBpBus ? returnBpBus.boarding_point.point_name : null,
              pointTime: returnBpBus ? returnBpBus.boarding_point.point_time : null,
              pointAddress: returnBpBus ? returnBpBus.boarding_point.point_address : null,
              pointLandmark: returnBpBus ? returnBpBus.boarding_point.point_landmark : null,
              pointContact: returnBpBus ? returnBpBus.boarding_point.point_contact : null
            },
            droppingPoint: {
              pointId: returnDpBus ? returnDpBus.dropping_point.point_id : null,
              pointName: returnDpBus ? returnDpBus.dropping_point.point_name : null,
              pointTime: returnDpBus ? returnDpBus.dropping_point.point_time : null,
              pointAddress: returnDpBus ? returnDpBus.dropping_point.point_address : null,
              pointLandmark: returnDpBus ? returnDpBus.dropping_point.point_landmark : null,
              pointContact: returnDpBus ? returnDpBus.dropping_point.point_contact : null
            },
            passengers
          });
        }
      }

      const { title, firstName, lastName, countryCode, phoneNumber, email } = this.props.user_siago;

      this.state = {
        ModalPassanger: false,
        modalDetail: false,
        modalFail: false,
        modalFailSameContact: false,
        loading: false,
        seats,
        messageRes: '',
        contact: {
          title: title || '',
          firstName: firstName || '',
          lastName: lastName || '',
          countryCode: countryCode || '62',
          phone: phoneNumber || '',
          email: email || ''
        },
        errors: {
          title: null,
          firstName: null,
          lastName: null,
          countryCode: null,
          phone: null,
          email: null,
        }
      }
    }

    openModal = (modal) => {
      this.setState({
        [modal]: true
      })
    }

    closeModal = (modal) => {
      this.setState({
        [modal]: false
      })
    }

    componentWillReceiveProps(nextProps) {
      if (!this.props.fetchingBook && nextProps.fetchingBook) {
        this.openModal('loading');
      }else if (this.props.fetchingBook && !nextProps.fetchingBook) {
        this.closeModal('loading');
        if (nextProps.error === null) {
          this.props.navigation.navigate('PaymentScreen');
        }else {
          this.setState({ modalFail: true, messageRes: nextProps.error }, () => setTimeout(() => {
            this.setState({ modalFail: false, messageRes: '' })
          }, 3000));
          console.log('Error:', nextProps.error);
        }
      }
    }

    onContactChange = (name, value) => {
      this.setState(prevState => {
        return {
          contact: {
            ...prevState.contact,
            [name]: value
          }
        }
        // alert(e.data)
      }, () => {console.log(this.state.contact)})
    }

    onSameContact = (switchOn) => {
      const { title, firstName, lastName, countryCode, phone, email } = this.state.contact;
      let passengers = {};

      if (switchOn) {
        passengers = {
          ...this.state.seats[0].passengers,
          title,
          firstName,
          lastName,
          phoneNumber: countryCode + phone,
          email,
        }
      }else {
        passengers = {
          ...this.state.seats[0].passengers,
          title: '',
          firstName: '',
          lastName: '',
          phoneNumber: '',
          email: '',
        }
      }

      this.setState(prevState => {
        return {
          ...prevState,
          seats: prevState.seats.map((pax) => {
            // var passengers = pax.passengers;
            // passengers = {
            //   ...passengers,
            //   [name]: value
            // }

            if(pax.id === 0){
              return {
                ...pax,
                passengers
              }
            } else {
              return pax
            }
          })
        }
      });
    }

    modalFailSameContact = (modalFailSameContact) => {
      this.setState({ modalFailSameContact })
    }

    onPassangerChange = (name, id, value) => {
      this.setState(prevState => {
        return {
          ...prevState,
          seats: prevState.seats.map((pax) => {
            var passengers = pax.passengers;
            passengers = {
              ...passengers,
              [name]: value
            }

            if(pax.id === id){
              return {
                ...pax,
                passengers
              }
            } else {
              return pax
            }
          })
        }
      });
    }

    deletePassanger = (id) => {
      this.setState(prevState => {
        return {
          ...prevState,
          seats: prevState.seats.map(pax => {
            var passengers = pax.passengers;
            passengers = {
              ...passengers,
              title: '',
              fullName: '',
              firstName: '',
              lastName: '',
              birthDate: '',
              address: '',
              city: '',
              country: '',
              postalCode: '',
              phoneNumber: '',
              age: '',
              email: '',
              idCard: ''
            }

            if (pax.id === id) {
              return {
                ...pax,
                passengers
              };
            }
            return pax;
          })
        }
      });
    }

    getTitleLabel = (abbr) => {
      const idx = titles.findIndex(title => title.abbr === abbr);
      if (idx !== -1) return titles[idx].name;
      return '';
    }

    getShortMonth(date) {
      return Moment.parseZone(date).format('MMMM').substring(0, 3);
    }

    getShorterArray(array) {
      return array.slice(0, array.length / 2);
    }

    submit(finalAmount) {
      const { contact, seats } = this.state;
      const { selectedBus } = this.props;

      this.props.onBookBus(
        contact,
        seats,
        selectedBus.departureBus,
        selectedBus.returnBus,
        finalAmount
      );
    }

    calculateSubtotal() {
      const { selectedBus } = this.props;
      let subTotal = 0;
      // const { departureFlight, departureSeatClass, returnFlight, returnSeatClass } = this.props.selectedFlights;
      // for (let i = 0; i < departureFlight.segments.length; i++) if (departureFlight.segments[i].classes.length > 0) subtotal += departureFlight.segments[i].classes[departureSeatClass[i]].classFare;
      // if (returnFlight) for (let i = 0; i < returnFlight.segments.length; i++) if (returnFlight.segments[i].classes.length > 0) subtotal += returnFlight.segments[i].classes[returnSeatClass[i]].classFare;
      for(const ss of selectedBus.departureSeatBus) subTotal += ss.base_final_price;
      if (selectedBus.returnSeatBus) for(const ss of selectedBus.returnSeatBus) subTotal += ss.base_final_price;

      return subTotal;
    }

    renderDepartureInfo(departureBus) {
      // const firstSegment = flight.segments[0];
      // const lastSegment = flight.segments[flight.segments.length - 1];
      // const firstSegmentDepartsAt = Moment.parseZone(firstSegment.departsAt);
      // const lastSegmentArrivesAt = Moment.parseZone(lastSegment.arrivesAt);
      // const duration = Moment.duration(lastSegmentArrivesAt.diff(firstSegmentDepartsAt, 'seconds'), 'seconds');
      const { departureDate, origin, destination } = this.props.searchBus;
      // const duration = {duration.format('h[j] m[m]')}
      return (
        <PerContent>
          <LeftHeaderView>
            <BlackHeaderView>
              <WhiteText>Pergi</WhiteText>
            </BlackHeaderView>
          </LeftHeaderView>
          <MidHeaderView>
            <MidSubHeader>
              <MidSubHeaderText align='left'>{this.getShortMonth(departureDate)} {departureDate.format('DD ddd YYYY')}</MidSubHeaderText>
              <MidSubHeaderText align='left'>{origin.name} - {destination.name}</MidSubHeaderText>
            </MidSubHeader>
          </MidHeaderView>
        </PerContent>
      );
    }

    renderReturnInfo(returnBus) {
      // const firstSegment = flight.segments[0];
      // const lastSegment = flight.segments[flight.segments.length - 1];
      // const firstSegmentDepartsAt = Moment.parseZone(firstSegment.departsAt);
      // const lastSegmentArrivesAt = Moment.parseZone(lastSegment.arrivesAt);
      // const duration = Moment.duration(lastSegmentArrivesAt.diff(firstSegmentDepartsAt, 'seconds'), 'seconds');
      const { returnDate, origin, destination } = this.props.searchBus;
      return (
        <PerContent style={{marginTop: 8}}>
          <LeftHeaderView>
            <BlackHeaderView>
              <WhiteText>Pulang</WhiteText>
            </BlackHeaderView>
          </LeftHeaderView>
          <MidHeaderView>
            <MidSubHeader>
              <MidSubHeaderText align='left'>{this.getShortMonth(returnDate)} {returnDate.format('DD ddd YYYY')}</MidSubHeaderText>
              <MidSubHeaderText align='left'>{destination.name} - {origin.name}</MidSubHeaderText>
            </MidSubHeader>
          </MidHeaderView>
        </PerContent>
      );
    }

    renderPassanger(Passangers) {
      const { trip } = this.props.searchBus;
      let itemPassangers = trip === 1 ? Passangers : this.getShorterArray(Passangers);

      return itemPassangers.map((pax, i) => {
        return (
          <BusPassangers
            key={i}
            index={i}
            titles={titles}
            pax={pax}
            id={pax.id}
            deletePassanger={this.deletePassanger}
            onPassangerChange={this.onPassangerChange}
            onSameContact={this.onSameContact}
            contact={this.state.contact}
            ordering={this.state.seats[0].passengers}
            modalFailSameContact={this.modalFailSameContact}
          />
        )
      })
    }

    modalInformation(visible, label) {
      return (
        <Modal
          onRequestClose={() => {}}
          animationType="fade"
          transparent
          visible={visible}
        >
          <ModalInformation
            label={label}
            error
          />
        </Modal>
      )
    }

    validateForm() {
      const { title, firstName, lastName, countryCode, phone, email } = this.state.contact;
      const contact = [ title, firstName, lastName, countryCode, phone, email ];
      let passenger = [];
      let idxFalse = 0;

      for(const c of contact) if (c === '') idxFalse = idxFalse + 1;

      for(const seat of this.state.seats) {
        passenger = [ seat.passengers.title, seat.passengers.firstName, seat.passengers.lastName, seat.passengers.phoneNumber ];
        for(const p of passenger) if (p === '') idxFalse = idxFalse = 1;
      }


      if (idxFalse > 0) return false;
      else return true;
    }

    render() {
      const { contact, errors, seats, modalFail, modalFailSameContact } = this.state;
      const { selectedBus } = this.props;
      const result = this.validateForm();

      const subTotal = this.calculateSubtotal();

      // console.log(this.state, 'state Book BUS');
      // console.log(this.props, 'props Book BUS');

      return (
        <MainView>
          <Header title='Review & Pesan' />
          <ScrollView style={{ minHeight: 1, width: '100%' }}>
            <View style={{ height: 57, backgroundColor: Color.theme }} />
            <AbsoluteView>
              <HeaderView onPress={() => {false && this.openModal('modalDetail')} }>
                <WrapContentView>
                  {this.renderDepartureInfo(selectedBus.departureBus)}
                  {selectedBus.returnBus && this.renderReturnInfo(selectedBus.returnBus)}
                </WrapContentView>
                {false && <IconView>
                  <Text><ArrowRightIcon name='chevron-right' /></Text>
                </IconView>}
              </HeaderView>
            </AbsoluteView>
            <View style={{ height: 60 }} />
            <ContactViewPemesan>
              <ContactBooking
                titles={titles}
                contact={contact}
                errors={errors}
                onContactChange={this.onContactChange}
              />
            </ContactViewPemesan>
            <ContactView>
              <SubContactView>
                <Text type='bold' style={{ letterSpacing: 1 }}>DATA PENUMPANG</Text>
              </SubContactView>
              {this.renderPassanger(seats)}
            </ContactView>
          </ScrollView>
          <ButtomView>
            <MainBookingView>
              <HalfBookingView>
                <NormalText type='medium'>Subtotal</NormalText>
                <PriceText type='bold'>{FormatMoney.getFormattedMoney(subTotal)}</PriceText>
              </HalfBookingView>
              <HalfBookingView>
                <ButtonRadius disabled={!result} style={!result && {backgroundColor: '#DDDDDD'}} onPress={() => this.submit(subTotal)} delay={1000}>
                  <ButtonText>Bayar Sekarang</ButtonText>
                </ButtonRadius>
              </HalfBookingView>
            </MainBookingView>
          </ButtomView>

          {/*<Modal
            onRequestClose={() => this.closeModal('modalDetail')}
            animationType='slide'
            transparent
            visible={this.state.modalDetail}
          >
            <ModalBusesDetail
              onClose={() => this.closeModal('modalDetail')}
            />
          </Modal>*/}

          <ModalIndicator
            visible={this.state.loading}
            type="large"
            message="Harap tunggu, kami sedang memproses pesanan Anda"
          />

          {this.modalInformation(modalFailSameContact, 'Lengkapi Data Pemesan terlebih dahulu')}
          {this.modalInformation(modalFail, this.state.messageRes)}
        </MainView>
      );
    }
  }


const mapStateToProps = state => {
  return {
    searchBus: state.searchBus,
    selectedBus: state.selectedBus,
    user: state['user.auth'].login.user,
    fetchingBook: state.booking.fetching,
    error: state.booking.error,
    user_siago: state['user.auth'].user_siago
  };
};

const mapDispatchToProps = (dispatch) => ({
  onBookBus: (contact, seats, departureBus, returnBus, finalAmount) => {
    dispatch(bookBus(contact, seats, departureBus, returnBus, finalAmount));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(BusReviewBooking);
