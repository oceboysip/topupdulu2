import React, { Component } from 'react';
import { View, ScrollView, Image, Platform, Modal } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';
import { connect } from 'react-redux';

import Text from '../Text';
import Color from '../Color';
import Button from '../Button';
import Header from '../Header';
import TouchableOpacity from '../Button/TouchableDebounce';
import ModalTerminalPicker from '../Modal/TerminalPicker';
import CalendarScreen from "../CalendarScreen";
import { getBusResult } from '../../state/actions/bus/get-bus-result';

const MainView = Styled(View)`
  width: 100%;
  height: 100%;
  backgroundColor: #F4F4F4;
`;

const ChildContainer = Styled(View)`
  flex: 1;
  width: 100%;
  alignItems: center;
  flexDirection: column;
`;

const NormalText = Styled(Text)`
  fontSize: 12;
  color: ${Color.theme};
`;

const CustomScrollView = Styled(ScrollView)`
  width: 100%;
  padding: 0px 16px 100px 16px;
`;

const MainBookingView = Styled(View)`
  width: 100%;
  minHeight: 1;
  backgroundColor: #FFFFFF;
  flexDirection: column;
  alignItems: center;
  marginBottom: 15;
  borderRadius: 20
`

const DestinationMainView = Styled(View)`
  width: 100%;
  minHeight: 1;
  paddingTop: 20;
  paddingBottom: 5;
  paddingHorizontal: 16;
  margin 23px 0px 25px 0px;
`;

const DestinationContainer = Styled(View)`
  width: 100%;
  minHeight: 50;
  flexDirection: column;
  marginBottom: 20;
`

const HalfDestinationView = Styled(View)`
  width: 100%;
  minHeight: 1;
  padding: 0px 2px 0px 2px;
  zIndex: 1;
  alignItems: flex-start;
`

const AbsoluteDestinationContainer = Styled(View)`
  alignItems: flex-end;
  top: -4;
`

const LabelText = Styled(Text)`
  color: ${Color.theme};
  fontSize: 12;
  lineHeight: 15;
`

const TouchOption = Styled(TouchableOpacity)`
  marginTop: 10;
`

const BigOptionText = Styled(Text)`
  fontSize: 16;
  lineHeight: 40;
  color: ${Color.theme};
`

const OptionText = Styled(Text)`
  lineHeight: 20;
  color: ${Color.theme};
  fontWeight: bold;
`

const SmallOptionText = Styled(Text)`
  fontSize: 12;
  lineHeight: 40;
  color: ${Color.theme};
`

const SwapButton = Styled(TouchableOpacity)`
  width: 30;
  height: 30;
  alignItems: center;
  justifyContent: center;
  borderRadius: 15;
`

const SwapIcon = Styled(Image)`
  width: 100%;
  height: 100%;
`

const OneWayOptionView = Styled(View)`
  width: 100%;
  minHeight: 1;
  marginTop: 20;
  marginBottom: 20;
  alignItems: center;
`

const OneWayOptionContainer = Styled(View)`
  width: 250;
  minHeight: 1;
  flexDirection: row;
  justifyContent: center;
  borderWidth: 2;
  borderColor: ${Color.theme};
  borderRadius: 30;
`

const OneWayView = Styled(TouchableOpacity)`
  flex: 1;
  minHeight: 1;
  padding: 5px 15px;
  borderBottomLeftRadius: 30;
  borderTopLeftRadius: 30;
  backgroundColor: ${Color.disabled}
`

const RoundTripView = Styled(OneWayView)`
  borderBottomLeftRadius: 0;
  borderTopLeftRadius: 0;
  borderBottomRightRadius: 30;
  borderTopRightRadius: 30;
`

const DetailedInformation = Styled(View)`
  width: 100%;
  minHeight: 1;
  flexDirection: column;
  padding: 0px 16px;
`

const PerDetailedInformation = Styled(View)`
  width: 100%;
  minHeight: 1;
  paddingBottom: 8;
  alignItems: flex-start;
  justifyContent: flex-start;
`

const LeftDetailedInformation = Styled(View)`
  width: 100%;
  minHeight; 1;
  alignItems: flex-start;
  justifyContent: flex-end;
  borderBottomWidth: 2;
  borderColor: ${Color.primary};
`

const RightDetailedInformation = Styled(LeftDetailedInformation)`
  marginTop: 16;
  borderBottomWidth: 0;
`

const LeftTouch = Styled(TouchableOpacity)`
  width; 100%;
  minHeight: 1;
  alignItems: flex-start;
  justifyContent: flex-end;
  marginBottom: 4;
`

const RightTouch = Styled(LeftTouch)`
  alignItems: flex-end;
`

const SearchButton = Styled(Button)`
  height: 50;
  width: 90%;
  backgroundColor: ${Color.theme};
`

const swap = require('../../images/icon_refresh_orange.png');
const pesanan = require('../../images/icon_paperWhite.png');

class BusScreen extends Component {
  static navigationOptions = {
    header: null
  }

  focusListener;

  constructor(props) {
    super(props);
    this.state = {
      trip: 1,
      type: null,
      origin: {code: '112', name: 'TERMINAL JOMBOR', city: 'KOTA YOGYAKARTA'},
      destination: {code: '36', name: 'PONDOK UNGU', city: 'KOTA BEKASI'},
      modalTerminalPicker: false,
      modalDatePicker: false
    }
  }

  componentWillFocus = () => {
    const { departureDate, returnDate } = this.props.searchBus;

    if (Moment(Moment(departureDate).format('YYYY-MM-DD')).isBefore(Moment().format('YYYY-MM-DD'))) {
      this.props.updateDepartureDateBus(Moment());
    }

    if (Moment(Moment(returnDate).format('YYYY-MM-DD')).isBefore(Moment().format('YYYY-MM-DD'))) {
      this.props.updateReturnDateBus(Moment().add(3, 'd'));
    }
  }

  componentWillUnmount(){
    this.focusListener.remove();
  }

  componentDidMount(){
    this.focusListener = this.props.navigation.addListener('willFocus', this.componentWillFocus);
  }

  getShortMonth(month) {
    return Moment(month, 'M').format('MMMM').substring(0, 3);
  }

  searchBus(){
    const { trip, origin, destination } = this.state;

    if (origin.code === destination.code) {
      return;
    }

    const { departureDate, returnDate } = this.props.searchBus;

    this.props.resetSearchBuses();
    this.props.resetSelectedBus();
    this.props.updateSearchBuses({ trip, departureDate, returnDate, origin, destination });
    this.props.getBusResult()
    this.props.navigation.navigate({
      routeName: 'BusResultScreen',
      params: { type: 'departure' },
      key: 'BusResultScreenDeparture'
    });
  }

  openModal = (modal, type) => {
    this.setState({ [modal]: true, type });
  }

  closeModal = (modal) => {
    this.setState({ [modal]: false });
  }

  onSelectedDate = (date) => {
    if (this.state.type) {
      this.props.updateDepartureDateBus(date);
      if (Moment(Moment(date).format('YYYY-MM-DD')).isAfter(Moment(this.props.searchBus.returnDate).format('YYYY-MM-DD'))) {
        this.props.updateReturnDateBus(Moment(date).add(3, 'd'))
      }
    }else {
      this.props.updateReturnDateBus(date)
    }

    this.setState({
      modalDatePicker: false
    })
  }

  onSelectedTerminal = (item) => {
    if (this.state.type) {
      this.setState({origin: item})
    }else {
      this.setState({destination: item})
    }
  }

  openOrderBooking() {
    this.props.navigation.navigate('OrderBooking', {
      title: 'Bus'
    })
  }

  render(){
    const { type, trip, origin, destination, modalTerminalPicker, modalDatePicker } = this.state;
    const departureDate = Moment(this.props.searchBus.departureDate);
    const returnDate = Moment(this.props.searchBus.returnDate);

    // console.log(this.props, 'props BusScreen');
    // console.log(this.state, 'state BusScreen');

    return(
      <MainView>
        <Header title='Pesan Bus' showLeftButton imageRightButton={pesanan} onPressRightButton={() => this.openOrderBooking()} />
        <ChildContainer>
          <CustomScrollView contentContainerStyle={{paddingTop: 20}}>
            <MainBookingView>
              
              <DestinationMainView>
                <DestinationContainer>
                  <HalfDestinationView style={{width: '90%'}}>
                    <LabelText type='medium'>Dari</LabelText>
                    <TouchOption onPress={() => this.openModal('modalTerminalPicker', true)}>
                      <OptionText align='left' type='medium'>{origin.name}{origin.city !== 'null' ? ' ('+origin.city+')' : ''}</OptionText>
                    </TouchOption>
                  </HalfDestinationView>

                  <AbsoluteDestinationContainer>
                    <View style={{width: '100%', borderBottomWidth: 2, borderColor: Color.primary, top: 16}} />
                    <SwapButton onPress={() => this.setState({origin: destination, destination: origin})}>
                      <SwapIcon source={swap} />
                    </SwapButton>
                  </AbsoluteDestinationContainer>

                  <HalfDestinationView>
                    <LabelText type='medium'>Ke</LabelText>
                    <TouchOption onPress={() => this.openModal('modalTerminalPicker', false)}>
                      <OptionText align='left' type='medium'>{destination.name}{destination.city !== 'null' ? ' ('+destination.city+')' : ''}</OptionText>
                    </TouchOption>
                  </HalfDestinationView>
                </DestinationContainer>
              </DestinationMainView>

            </MainBookingView>
            <MainBookingView>

              <OneWayOptionView>
                <OneWayOptionContainer>
                  <OneWayView onPress={() => this.setState({trip: 1})} style={trip === 1 && {backgroundColor: Color.theme}}>
                    <NormalText type='medium' style={trip === 1 && {color: '#FFFFFF'}}>Sekali Jalan</NormalText>
                  </OneWayView>
                  <RoundTripView onPress={() => this.setState({trip: 2})} style={trip === 2 && {backgroundColor: Color.theme}}>
                    <NormalText type='medium' style={trip === 2 && { color: '#FFFFFF' }}>Pulang Pergi</NormalText>
                  </RoundTripView>
                </OneWayOptionContainer>
              </OneWayOptionView>

              <DetailedInformation>
                <PerDetailedInformation style={trip === 1 && {marginBottom: 16}}>
                  <LeftDetailedInformation>
                    <LabelText type='medium'>Tanggal Berangkat</LabelText>
                    <LeftTouch onPress={() => this.openModal('modalDatePicker', true)}>
                      <BigOptionText type='bold'>{this.getShortMonth(departureDate)}, {departureDate.format('D')}<SmallOptionText type='bold'> {departureDate.format('ddd YYYY')}</SmallOptionText></BigOptionText>
                    </LeftTouch>
                  </LeftDetailedInformation>
                  {trip !== 1 && <RightDetailedInformation>
                    <LabelText type='medium'>Tanggal Pulang</LabelText>
                    <RightTouch onPress={() => this.openModal('modalDatePicker', false)}>
                      <BigOptionText type='bold'>{this.getShortMonth(returnDate)}, {returnDate.format('D')}<SmallOptionText type='bold'> {returnDate.format('ddd YYYY')}</SmallOptionText></BigOptionText>
                    </RightTouch>
                  </RightDetailedInformation>}
                </PerDetailedInformation>

              </DetailedInformation>
            </MainBookingView>

            <View style={{alignItems: 'center'}}>
              <SearchButton type='semibold' onPress={() => this.searchBus()}>Cari Bus</SearchButton>
            </View>

          </CustomScrollView>
        </ChildContainer>

        <Modal
          visible={modalTerminalPicker}
          animationType='slide'
          onRequestClose={() => this.closeModal('modalTerminalPicker')}
        >
          <ModalTerminalPicker
            onClose={() => this.closeModal('modalTerminalPicker')}
            onSelectedTerminal={this.onSelectedTerminal}
          />
        </Modal>

        <Modal
          visible={modalDatePicker}
          animationType='slide'
          onRequestClose={() => this.closeModal('modalDatePicker')}
        >
          <CalendarScreen
            onSelectedDate={this.onSelectedDate}
            onClose={() => this.closeModal('modalDatePicker')}
            selectedDate={type ? departureDate : returnDate}
            label={type ? 'Depart' : 'Return'}
            minDate={type ? Moment() : departureDate}
            maxDate={Moment().add(3, 'M')}
          />
        </Modal>
      </MainView>
    )
  }
}

const mapStateToProps = (state) => ({
  searchBus: state.searchBus
});

const mapDispatchToProps = (dispatch, props) => ({
  updateSearchBuses: (params) => dispatch({ type: 'UPDATE_SEARCH_BUSES', params }),
  updateDepartureDateBus: (params) => dispatch({ type: 'UPDATE_DEPARTURE_DATE_BUS', params }),
  updateReturnDateBus: (params) => dispatch({ type: 'UPDATE_RETURN_DATE_BUS', params}),
  resetSelectedBus: () => dispatch({ type: 'RESET_SELECTED_BUS' }),
  resetSearchBuses: () => dispatch({ type: 'RESET_SEARCH_BUSES' }),
  getBusResult: () => dispatch(getBusResult())
})

export default connect(mapStateToProps, mapDispatchToProps)(BusScreen);
