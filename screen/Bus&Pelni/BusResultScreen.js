import React, { Component } from 'react';
import { View, FlatList, Image, ActivityIndicator, Dimensions, Modal } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';
import ModalBox from 'react-native-modalbox';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { connect } from 'react-redux';
import ProgressBar from 'react-native-progress/Bar';

import Text from '../Text';
import Color from '../Color';
import Header from '../Header';
import Button from '../Button';
import BusResult from './BusResult';
import TouchableOpacity from '../Button/TouchableDebounce';
// import ModalFlightSort from '../Modal/FlightSort';
// import ModalBusesFilter from '../Modal/BusesFilter';
import CalendarScreen from '../CalendarScreen';

import { getBusResult } from '../../state/actions/bus/get-bus-result';

const MainView = Styled(View)`
  width: 100%;
  height: 100%;
  backgroundColor: #FAFAFA;
  flexDirection: column;
`;

const CustomHeader = Styled(Header)`
  height: 69;
`;

const TitleHeader = Styled(View)`
  width: 100%;
  height: 50%;
  flexDirection: row;
  alignItems: flex-start;
  justifyContent: center;
`;

const ChildHeader = Styled(TitleHeader)`
  position: absolute;
  bottom: 0;
`;

const TitleText = Styled(Text)`
  lineHeight: 16;
  letterSpacing: 0;
  color: ${Color.white};
`;

const ContentText = Styled(Text)`
  fontSize: 12;
  lineHeight: 16;
  color: ${Color.white};
`;

const ArrowRight = Styled(FontAwesome5)`
  fontSize: 10;
  color: ${Color.primary}
  paddingHorizontal: 8;
`;

const SubtitleView = Styled(View)`
  width: 100%;
  minHeight: 1;
  alignItems: center;
  justifyContent: center;
  padding: 8px 0px 0px 0px;
`;

const SubtitleText = Styled(Text)`
  lineHeight: 24;
  letterSpacing: 1;
`;

const BigText = Styled(SubtitleText)`
  fontSize: 18;
  marginTop: 40;
  marginBottom: 11;
`;

const BusCard = Styled(BusResult)`
  marginBottom: 8;
`;

const ResultView = Styled(View)`
  flex: 1;
  justifyContent: center;
`;

const ConfigureContainer = Styled(View)`
  width: 100%;
  minHeight: 1;
  alignItems: center;
  position: absolute;
  bottom: 12;
`;

const ConfigureView = Styled(View)`
  minWidth: 1;
  height: 40;
  flexDirection: row;
  backgroundColor: ${Color.button};
  borderRadius: 100;
  borderWidth: 1;
  borderColor: #000000;
`;

const ConfigureContent = Styled(TouchableOpacity)`
  width: 79;
  height: 100%;
  justifyContent: center;
  alignItems: center;
  flexDirection: column;
`;

// const MiddleConfigure = Styled(ConfigureContent)`
//   width: 120;
//   borderLeftWidth: 1;
//   borderRightWidth: 1;
//   borderColor: #000000;
// `;

const MiddleConfigure = Styled(ConfigureContent)`
  width: 40%;
`;

const FilterIcon = Styled(View)`
  width: 11.11;
  height: 10;
`;

const DateIcon = Styled(View)`
  width: 9;
  height: 10;
`;

const SortIcon = Styled(View)`
  width: 15;
  height: 10;
`;

const ImageProperty = Styled(Image)`
  width: 100%;
  height: 100%;
`;

const ConfigLabel = Styled(Text)`
  fontSize: 12;
  color: #FFFFFF;
`;

const MiniSubtitleView = Styled(View)`
  width: 100%;
  alignItems: center;
  paddingHorizontal: 12;
  justifyContent: center;
  marginBottom: 50;
`;

const WarningImage = Styled(Image)`
  width: 95.99;
  height: 96;
`;

const WarningButton = Styled(Button)`
  height: 40;
  width: 150;
  marginTop: 16;
`;

const CustomModalBox = Styled(ModalBox)`
  width: 100%;
  height: 405;
  backgroundColor: transparent;
  alignItems: center;
`;

// const sort = require('../../images/sort.png');
// const filter = require('../../images/filter.png');
const calendar = require('../../images/calendar.png');
const warningBus = require('../../images/warning-bus.png');
const pesanan = require('../../images/icon_paperWhite.png');

const { width } = Dimensions.get('window');

const sortOptions = [
  { name: 'lowestPrice', label: 'Lowest Price' },
  { name: 'shortestDuration', label: 'Shortest Duration' },
  { name: 'earliestDeparture', label: 'Earliest Departure' },
  { name: 'latestDeparture', label: 'Latest Departure' },
];

class BusResultScreen extends Component {
  static navigationOptions = { header: null };
  focusListener;

  constructor(props) {
    super(props);
    this.state = {
      type: props.navigation.state.params.type === 'departure',
      ready: props.navigation.state.params.type === 'departure',
      customKey: 0,
      loading: props.busResult.loading,
      progress: 0,
      modalSort: false,
      modalFilter: false,
      modalDatePicker: false,
      optionsPriceFilter: [0, 0],
      optionsStationTransitFilter: [],
      optionsBusesFilter: [],
      selectedSort: sortOptions[0].name,
      selectedFilters: {
        stationTransitFilter: [],
        stopFilter: [],
        departureFilter: [],
        arrivalFilter: [],
        busesFilter: [],
        priceFilter: [0, 0]
      }
    };

    this.focusListener = props.navigation.addListener('willFocus', this.componentWillFocus);
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  resetState(func){
    this.setState({
      ready: true,
      loading: true,
      progress: 0,
      modalSort: false,
      modalFilter: false,
      modalDatePicker: false,
      optionsPriceFilter: [0, 0],
      optionsStationTransitFilter: [],
      optionsBusesFilter: [],
      selectedSort: sortOptions[0].name,
      selectedFilters: {
        stationTransitFilter: [],
        stopFilter: [],
        departureFilter: [],
        arrivalFilter: [],
        busesFilter: [],
        priceFilter: [0, 0]
      }
    }, () => func && func());

    this.animateProgress();
  }

  componentDidMount() {
    let newState = {customKey: this.state.customKey + 1};
    if (this.props.busResult) {
      const currentResult = this.state.type ? this.props.busResult.result.departures : this.props.busResult.result.returns;
      const optionsFilter = this.getMoreFilterOptions(currentResult);
      console.log(optionsFilter, 'did optionsFilter');
      const { selectedFilters } = this.state;
      selectedFilters.priceFilter = optionsFilter.optionsPriceFilter;
      newState = {...newState, ...optionsFilter, currentResult, selectedFilters};
    }
    this.setState(newState, () => {
      if (!this.state.ready) {
        const tempTimeout = setTimeout(() => { clearTimeout(tempTimeout); this.setState({ready: true}) }, 1)
      }
    });

    this.animateProgress();
  }

  componentWillReceiveProps(nextProps) {
    let newState = { customKey: this.state.customKey + 1};
    if (nextProps.busResult) {
      const currentResult = this.state.type ? nextProps.busResult.result.departures : nextProps.busResult.result.returns;
      const optionsFilter = this.getMoreFilterOptions(currentResult);
      console.log(optionsFilter, '1 optionsFilter');
      const { selectedFilters } = this.state;
      selectedFilters.priceFilter = optionsFilter.optionsPriceFilter;
      newState = {...newState, ...optionsFilter, currentResult, selectedFilters};
    }
    this.setState(newState);

    this.setState({ loading: nextProps.busResult.loading });
  }

  componentWillFocus = () => {
    if (this.props.navigation.state.params && this.props.navigation.state.params.reset) {
      this.resetState(
        () => {
          this.props.getBusResult();
          this.props.navigation.setParams({ reset: false });
        }
      );
    }
  }

  animateProgress() {
    let progress = 0;
    let interval = setInterval(() => {
      progress += Math.random() * 17;

      if (!this.state.loading) {
        clearInterval(interval);
        progress = 99;
        let timeout = setTimeout(() => {
          this.setState({ progress: 100 });
          clearTimeout(timeout);
        }, 1000);
      }

      if (progress > 100) progress = 99;

      this.setState({ progress });
    }, 500);
  }

  openModal = (modal) => {
    this.setState({ ready: false, [modal]: true});
  }

  closeModal = (modal) => {
    this.setState({ [modal]: false }, () => {
      const tempTimeout = setTimeout(() => {
        clearTimeout(tempTimeout);
        this.setState({ ready: true });
      }, 1);
    });
  }

  onCloseFilter(selectedFilters) {
    this.setState({ ready: false, modalFilter: false, selectedFilters, customKey: this.state.customKey + 1 }, () => {
      const tempTimeout = setTimeout(() => { clearTimeout(tempTimeout); this.setState({ ready: true }); }, 1);
    });
  }

  getShortMonth(month) {
    return Moment(month, 'M').format('MMMM').substring(0, 3);
  }

  getBusesData(type){
    const { searchBus, busResult } = this.props;
    const { departureDate, returnDate, origin, destination } = searchBus;

    if (type) {
      return {
        currentOrigin: origin,
        currentDestination: destination,
        currentDate: departureDate,
        currentResult: busResult.result.departures
      };
    }else {
      return {
        currentOrigin: origin,
        currentDestination: destination,
        currentDate: returnDate,
        currentResult: busResult.result.returns
      };
    }
  }

  onSelectedBus(data){
    const { type } = this.state;

    if (type) this.props.updateSelectedDepartureBus(data);
    else this.props.updateSelectedReturnBus(data);

    this.props.navigation.navigate({
      routeName: 'SelectBusSeat',
      params: {
        data: data,
        type: type ? 'departure' : 'return'
      },
      key: `SelectBusSeat${type}`
    });
  }

  onSelectedDate(date){
    this.setState({
      modalDatePicker: false
    }, () => {
      const tempTimeout = setTimeout(() => {
        if (this.state.type) {
          this.props.updateDepartureDateBus(date);
          if (Moment(Moment(date).format('YYYY-MM-DD')).isAfter(Moment(this.props.searchBus.returnDate).format('YYYY-MM-DD'))) this.props.updateReturnDateBus(Moment(date).add(3, 'd'))
          this.resetState(this.props.getBusResult());
        }else {
          this.props.updateReturnDateBus(date);
          this.props.navigation.navigate({
            routeName: 'BusResultScreen',
            params: { type: 'departure', reset: true },
            key: 'BusResultScreenDeparture'
          });
        }
        clearTimeout(tempTimeout);
      }, 1);
    });
  }

  onSelectedSort = (selectedSort) => {
    this.setState({ ready: false, modalSort: false, selectedSort, customKey: this.state.customKey + 1 }, () => {
      const tempTimeout = setTimeout(() => { clearTimeout(tempTimeout); this.setState({ ready: true }); }, 1);
    });
  }

  openOrderBooking() {
    this.props.navigation.navigate('OrderBooking', {
      title: 'Bus'
    })
  }

  renderLoadingIndicator() {
    return <ActivityIndicator size='large' color={Color.loading} />;
  }

  switchRender(ready, atLeastOneResult, loading, finalResult){
    if (!ready) return this.renderLoadingIndicator();
    if (finalResult.length > 0) return this.renderResult(finalResult);
    if (loading) return this.renderLoadingIndicator();
    return this.renderWarning(atLeastOneResult);
  }

  keyExtractor = (bus, index) => `${bus.id} - ${index} - ${this.state.customKey}`;

  getMoreFilterOptions(currentResult){
    const optionsStationTransitFilter = [];
    const optionsBusesFilter = [];
    let optionsPriceFilter = [0, 0];
    const allFares = [];
    for(const result of currentResult) {
      allFares.push(result.base_price);
    }
    optionsPriceFilter = [Math.min(...allFares), Math.max(...allFares)];

    return {
      optionsStationTransitFilter: [...new Set(optionsStationTransitFilter)],
      optionsBusesFilter: [...new Set(optionsBusesFilter)],
      optionsPriceFilter
    };
  }

  renderWarning(atLeastOneResult){
    return (
      <MiniSubtitleView>
        <WarningImage source={warningBus} />
        <BigText type='bold'>Bus Tidak Tersedia</BigText>
        {atLeastOneResult && <SubtitleText type='bold'>Reset filter untuk menampilkan semua bus yang tersedia.</SubtitleText>}
        <WarningButton onPress={() => this.props.navigation.pop()}>Kembali</WarningButton>
      </MiniSubtitleView>
    )
  }

  renderResult(result){
    return (
      <FlatList
        contentContainerStyle={{paddingHorizontal: 8, paddingTop: 8, paddingBottom: 80}}
        data={result}
        keyExtractor={this.keyExtractor}
        renderItem={({item}) => <BusCard data={item} type={this.state.type} dataSearch={this.props.searchBus} onSelectedBus={() => this.onSelectedBus(item)} />}
      />
    )
  }

  render(){
    const { type, ready, progress, busAvail, modalSort, modalFilter, modalDatePicker, selectedSort, optionsStationTransitFilter, optionsBusesFilter, optionsPriceFilter, selectedFilters } = this.state;
    const { percentage, loading } = this.props.busResult;
    const { currentOrigin, currentDestination, currentDate, currentResult } = this.getBusesData(type);
    // const finalResult = this.customizeResult(ready, currentResult, selectedSort, selectedFilters);
    const atLeastOneResult = currentResult.length > 0;

    const depDate = Moment(this.props.searchBus.departureDate);
    const retDate = Moment(this.props.searchBus.returnDate);

    console.log(currentOrigin, 'test ah');
    
    // console.log(this.props, 'props BusResultScreen');
    // console.log(this.state, 'state BusResultScreen');

    return(
      <MainView>
        <CustomHeader title={type ? 'Pencarian Pergi' : "Pencarian Pulang"} showLeftButton imageRightButton={pesanan} onPressRightButton={() => this.openOrderBooking()} />
        <View style={{height: 48, backgroundColor: Color.theme}}>
          <TitleHeader>
            <TitleText type='bold'>{currentOrigin.name}  </TitleText>
            <ArrowRight name='arrow-right'/>
            <TitleText type='bold'>  {currentDestination.name}</TitleText>
          </TitleHeader>
          <ChildHeader>
            <ContentText type='medium'>{type ? 'Berangkat:' : 'Pulang:'} {this.getShortMonth(currentDate)} {currentDate.format('D')} {currentDate.format('ddd YYYY')}</ContentText>
          </ChildHeader>
        </View>

        {progress < 100 && <ProgressBar progress={progress} width={width} color={Color.primary} />}

        <ResultView>
          {this.switchRender(ready, atLeastOneResult, loading, currentResult)}
        </ResultView>

        {atLeastOneResult && ready && <ConfigureContainer>
          <ConfigureView>
            {false && <ConfigureContent onPress={() => this.openModal('modalFilter')}>
              <FilterIcon><ImageProperty resizeMode='contain' source={filter}/></FilterIcon>
              <ConfigLabel>Filter</ConfigLabel>
            </ConfigureContent>}
            <MiddleConfigure onPress={() => this.openModal('modalDatePicker')}>
              <DateIcon><ImageProperty resizeMode='contain' source={calendar}/></DateIcon>
              <ConfigLabel>Ubah Tanggal</ConfigLabel>
            </MiddleConfigure>
            {false && <ConfigureContent onPress={() => this.openModal('modalSort')}>
              <DateIcon><ImageProperty resizeMode='contain' source={sort}/></DateIcon>
              <ConfigLabel>Sort</ConfigLabel>
            </ConfigureContent>}
          </ConfigureView>
        </ConfigureContainer>}

        {/*<Modal
          visible={modalFilter}
          animationType='slide'
          onRequestClose={() => this.closeModal('modalFilter')}
        >
          <ModalBusesFilter
            onClose={(filters) => this.onCloseFilter(filters)}
            optionsFilter={{ optionsStationTransitFilter, optionsBusesFilter, optionsPriceFilter }}
            selectedFilters={selectedFilters}
          />
        </Modal>*/}

        <Modal
          visible={modalDatePicker}
          animationType='slide'
          onRequestClose={() => this.closeModal('modalDatePicker')}
        >
          <CalendarScreen
            onSelectedDate={(date) => this.onSelectedDate(date)}
            onClose={() => this.closeModal('modalDatePicker')}
            selectedDate={type ? depDate : retDate}
            label={type ? 'Depart' : 'Return'}
            minDate={type ? Moment() : this.props.searchBus.departureDate}
            maxDate={Moment().add(3, 'M')}
          />
        </Modal>

        {/*<CustomModalBox
          position='bottom'
          isOpen={modalSort}
          onClosed={() => this.closeModal('modalSort')}
          coverScreen
        >
          <ModalFlightSort
            sortOptions={sortOptions}
            selected={selectedSort}
            onSelectedSort={this.onSelectedSort}
          />
        </CustomModalBox>*/}
      </MainView>
    )
  }
}

const mapStateToProps = (state) => ({
  searchBus: state.searchBus,
  busResult: state.busResult
});

const mapDispatchToProps = (dispatch, props) => ({
  updateSelectedDepartureBus: (data) => dispatch({ type: 'SELECTED_DEPARTURE_BUS', data }),
  updateSelectedReturnBus: (data) => dispatch({ type: 'SELECTED_RETURN_BUS', data }),
  updateDepartureDateBus: (params) => dispatch({ type: 'UPDATE_DEPARTURE_DATE_BUS', params }),
  updateReturnDateBus: (params) => dispatch({ type: 'UPDATE_RETURN_DATE_BUS', params}),
  getBusResult: () => dispatch(getBusResult())
})

export default connect(mapStateToProps, mapDispatchToProps)(BusResultScreen);
