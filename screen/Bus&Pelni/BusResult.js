import React, { Component } from 'react';
import { Image, View, Platform } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import TouchableOpacity from '../Button/TouchableDebounce';
import FormatMoney from '../FormatMoney';
import Text from '../Text';
import Color from '../Color';
import { Row, Col } from '../Grid';

const MainView = Styled(TouchableOpacity)`
  width: 100%;
  minHeight: 100;
  paddingHorizontal: 8;
  backgroundColor: #FFFFFF;
  elevation: ${Platform.OS === 'android' ? 3 : 0};
  shadowOpacity: ${Platform.OS === 'ios' ? 0.1 : 0};
`;

const SubView = Styled(View)`
  width: 100%;
  minHeight: 1;
  flexDirection: row;
  padding: 10px 0px 10px 0px;
  alignItems: center;
  justifyContent: space-between;
`;

const InfoView = Styled(SubView)`
  paddingTop: 12;
  paddingBottom: 16;
  borderBottomWidth: 0.5;
  borderColor: #A8A699;
`;

const TimeDetailView = Styled(View)`
  flexDirection: row;
`;

const ColumnView = Styled(View)`
  minWidth: 1;
  minHeight: 1;
  flexDirection: column;
  alignItems: center;
  justifyContent: center;
`;

const PriceTypeView = Styled(View)`
  minWidth: 1;
  minHeight: 1;
  flexDirection: column;
  alignItems: center;
  justifyContent: center;
  marginRight: 10;
  marginLeft: 6;
`;

const AirlineView = Styled(View)`
  minWidth: 1;
  minHeight: 1;
  flexDirection: row;
  alignItems: center;
  justifyContent: center;
`;

const StopView = Styled(View)`
  minWidth: 1;
  minHeight: 1;
  flexDirection: row;
  alignItems: center;
  justifyContent: center;
`;

const StopRoundView = Styled(View)`
  minWidth: 1;
  minHeight: 1;
  alignItems: center;
  justifyContent: center;
  borderWidth: 1;
  borderColor: #A8A699;
  borderRadius: 100;
  padding: 0px 8px 2px 8px;
  marginLeft: 8;
`;

const AirlineImageContainer = Styled(View)`
  width: 40;
  height: 20;
  marginRight: 10;
`;

const NormalText = Styled(Text)`
  fontSize: 12;
`;

const SmallerNormalText = Styled(Text)`
  fontSize: 12;
`;

const GreyText = Styled(Text)`
  fontSize: 12;
  color: #A8A699;
  marginTop: 2;
`;

const TimeText = Styled(Text)`
  fontSize: 11;
  color: #A8A699;
  lineHeight: 24;
`

const AvailableChairText = Styled(Text)`
  fontSize: 10;
  color: #A8A699;
`

const SmallerGreyText = Styled(Text)`
  fontSize: 8;
  color: #A8A699;
`;

const PriceText = Styled(Text)`
  color: #FF425E;
`;

const ArrowRightIcon = Styled(FontAwesome)`
  fontSize: 16;
  color: ${Color.text};
`;

export default class BusResult extends Component {
  constructor(props) {
    super(props);
  }

  isInteger(int) {
    let num;
    if (Number.isInteger(int)) num = true;
    else num = false;
    return num;
  }

  render() {
    const { data, type, dataSearch, ...style } = this.props;
    const depDate = Moment(data.departure_time).format('YYYY-MM-DD');
    const depTime = Moment(data.departure_time);
    const arrTime = Moment(data.arrival_time);
    let duration = arrTime.diff(depTime, 'seconds');
    duration = Moment.duration(duration, 'seconds');
    let diffDay = arrTime.diff(depDate, 'days');

    return (
      <MainView activeOpacity={0.8} onPress={() => this.props.onSelectedBus()} {...style}>
        <InfoView>
          <Row>
            <Col size={10} style={{alignItems: 'flex-start'}}>
              <NormalText type='bold' align='left'>{data.buses_name}</NormalText>
              <NormalText type='medium' style={{marginTop: 2}}>{type ? dataSearch.origin.name : dataSearch.destination.name} - {type ? dataSearch.destination.name : dataSearch.origin.name}</NormalText>
              {false && data.logo !== 'null' &&
                <Image style={{width: 40, height: 20, marginTop: 6}} source={{uri: data.logo}}
              />}
            </Col>
            <Col size={2} style={{alignItems: 'flex-end'}}>
              <NormalText type='medium'>{Moment(data.departure_time).format('hh:mm')}</NormalText>
              <GreyText type='medium'>{Moment(data.arrival_time).format('hh:mm')}</GreyText>
              <SmallerGreyText type='medium'>{diffDay > 0 && `(+${diffDay} hari)`}</SmallerGreyText>
            </Col>
          </Row>
        </InfoView>

        <SubView>
          <AirlineView>
            <TimeText type='medium'>{duration.format('H[j] m[m]')}</TimeText>
            {false && <StopRoundView>
              <AvailableChairText type='medium'>data.seat_avail} kursi</AvailableChairText>
            </StopRoundView>}
          </AirlineView>
          <StopView>
            {(data.base_final_price === 0 || typeof data.base_final_price === 'string') && <NormalText type='medium'>Mulai Dari</NormalText>}
            <PriceTypeView>
              {Number.isInteger(data.base_final_price) && <PriceText type='bold'>{FormatMoney.getFormattedMoney(data.base_final_price)}</PriceText>}
              {typeof data.base_final_price === 'string' && <PriceText type='bold'>Rp 0</PriceText>}
            </PriceTypeView>
            <ArrowRightIcon name='chevron-right' />
          </StopView>
        </SubView>
      </MainView>
    );
  }
}
