import React, { Component } from 'react';
import { View, ScrollView, Image, TextInput, Dimensions } from 'react-native';
import Styled from 'styled-components';
import MultiSlider from '@ptomasroos/react-native-multi-slider';

import Text from '../Text';
import FormatMoney from '../FormatMoney';

const { width } = Dimensions.get('window');

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    flexDirection: column;
`;

const TitleFilter = Styled(View)`
    width: 100%;
    height: 50;
    alignItems: flex-start;
    justifyContent: center;
    paddingHorizontal: 16;
`;

const PriceView = Styled(View)`
  width: 100%;
  minHeight: 1;
  padding: 8px 16px 0px 16px;
  flexDirection: row;
  justifyContent: space-between;
`;

const PriceHalfViewLeft = Styled(View)`
  width: 47%;
  height: 60;
  flexDirection: column;
  alignItems: flex-start;
  justifyContent: space-between;
  borderBottomWidth: 0.5;
  borderColor: #DDDDDD;
`;

const PriceHalfViewRight = Styled(PriceHalfViewLeft)`
  alignItems: flex-end;
  paddingLeft: 0;
`;

const PriceTextInput = Styled(TextInput)`
  width: 80%;
  minHeight: 1;
  fontSize: 14;
  color: #484848;
`;

const PriceSliderView = Styled(View)`
  width: 100%;
  height: 40;
  alignItems: center;
  justifyContent: center;
  marginTop: 15;
`;

const TitleFilterText = Styled(Text)`
    letterSpacing: 1;
    textAlign: left;
`;

const PriceLabel = Styled(Text)`
`;

const Marker = Styled(Image)`
  width: 30;
  height: 30;
`;

const iconTinyCircle = require('../../images/slider-circle.png');

const CustomMarker = () => <Marker source={iconTinyCircle} resizeMode='contain' />;

export default class FlightFilterOthers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      minPriceTextInput: props.selectedFilters.priceFilter[0],
      maxPriceTextInput: props.selectedFilters.priceFilter[1],
      minPrice: props.optionsPriceFilter[0],
      maxPrice: props.optionsPriceFilter[1],
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      minPriceTextInput: nextProps.selectedFilters.priceFilter[0],
      maxPriceTextInput: nextProps.selectedFilters.priceFilter[1],
      minPrice: nextProps.optionsPriceFilter[0],
      maxPrice: nextProps.optionsPriceFilter[1],
    });
  }

  onChangeTextInput(value, type) {
    const tempValue = value.replace(/\D/g, '');
    if (type === 'min') this.setState({ minPriceTextInput: tempValue });
    else this.setState({ maxPriceTextInput: tempValue });
  }

  multiSliderValuesChange = (priceFilter) => {
    let minPriceTextInput = priceFilter[0] > this.state.minPrice ? priceFilter[0] : this.state.minPrice;
    let maxPriceTextInput = priceFilter[1] < this.state.maxPrice ? priceFilter[1] : this.state.maxPrice;
    minPriceTextInput = minPriceTextInput < this.state.maxPrice ? minPriceTextInput : this.state.maxPrice;
    maxPriceTextInput = maxPriceTextInput > this.state.minPrice ? maxPriceTextInput : this.state.minPrice;
    this.props.onChangeFilter([minPriceTextInput, maxPriceTextInput]);
  }

  refreshPrice(type) {
    const { minPrice, maxPrice, minPriceTextInput, maxPriceTextInput } = this.state;
    const { priceFilter } = this.props.selectedFilters;
    let tempValue;
    // if (isNaN(tempValue) && type === 'max') tempValue = maxPrice;

    //min
    if (type === 'min') {
      tempValue = parseInt(minPriceTextInput, 10);
      if (isNaN(tempValue)) tempValue = minPrice;

      if (tempValue > priceFilter[1]) {
        if (tempValue > maxPrice) this.props.onChangeFilter([maxPrice, maxPrice]);
        else this.props.onChangeFilter([tempValue, tempValue]);
      }
      else if (tempValue < minPrice) this.props.onChangeFilter([minPrice, priceFilter[1]]);
      else this.props.onChangeFilter([tempValue, priceFilter[1]]);
    }

    //max
    else {
      tempValue = parseInt(maxPriceTextInput, 10);
      if (isNaN(tempValue)) tempValue = maxPrice;

      if (tempValue < priceFilter[0]) {
        if (tempValue < minPrice) this.props.onChangeFilter([minPrice, minPrice]);
        else this.props.onChangeFilter([tempValue, tempValue]);
      }
      else if (tempValue > maxPrice) this.props.onChangeFilter([priceFilter[0], maxPrice]);
      else this.props.onChangeFilter([priceFilter[0], tempValue]);
    }
  }

  renderPriceSlider() {
    const { minPrice, maxPrice } = this.state;
    const { priceFilter } = this.props.selectedFilters;
    return (
      <PriceSliderView>
        <MultiSlider
          values={[priceFilter[0], priceFilter[1]]}
          sliderLength={width - 60}
          onValuesChange={this.multiSliderValuesChange}
          min={minPrice}
          max={maxPrice}
          allowOverlap
          step={10000}
          selectedStyle={{ backgroundColor: '#231F20' }}
          unselectedStyle={{ backgroundColor: 'silver' }}
          customMarker={CustomMarker}
        />
      </PriceSliderView>
    );
  }

  renderSliderPrice() {
    const { minPriceTextInput, maxPriceTextInput } = this.state;
    return (
      <View>
        <TitleFilter>
          <TitleFilterText type='bold'>PRICE</TitleFilterText>
        </TitleFilter>
        <PriceView>
          <PriceHalfViewLeft>
            <PriceLabel type='medium'>Minimum</PriceLabel>
            <PriceTextInput value={FormatMoney.commafy(minPriceTextInput).toString()} onChangeText={(value) => this.onChangeTextInput(value, 'min')} onBlur={() => this.refreshPrice('min')} keyboardType='numeric' />
          </PriceHalfViewLeft>

          <PriceHalfViewRight>
            <PriceLabel type='medium'>Maksimum</PriceLabel>
            <PriceTextInput value={FormatMoney.commafy(maxPriceTextInput).toString()} onChangeText={(value) => this.onChangeTextInput(value, 'max')} onBlur={() => this.refreshPrice('max')} keyboardType='numeric' textAlign={'right'} />
          </PriceHalfViewRight>
        </PriceView>
        {this.renderPriceSlider()}
      </View>
    );
  }

  render() {
    const { minPrice, maxPrice } = this.state;
    return (
      <MainView>
        {minPrice !== maxPrice && <ScrollView>
          {this.renderSliderPrice()}
        </ScrollView>}
      </MainView>
    );
  }

}
