import React, { useEffect } from 'react';
import { View, Image } from 'react-native';
import Styled from 'styled-components';

import Text from '../Text';
import Color from '../Color';

const ColorBorder = Color.darkGrey;

const MainView = Styled.View`
    width: 100;
`;

const Container = Styled.View`
    flex-direction: row;
    justify-content: center;
    align-items: center;
    background-color: #FFFFFF;
    position: absolute;
    bottom: 0;
`;

const SmallCircle = Styled.View`
    width: 4;
    height: 4;
    border-radius: 2;
    background-color: ${ColorBorder};
`;

const HalfCenter = Styled.View`
    align-self: center;
    position: absolute;
    top: 7;
    width: 100%;
    height: 60;
    border-color: ${ColorBorder};
    border-width: 1;
    border-bottom-width: 0;
    border-top-left-radius: 60;
    border-top-right-radius: 60;
`;

const airplane = require('../../images/airplane.png')

const TransitInfo = ({isTransit, duration}) => {
    return (
        <MainView>
            <View style={{width: '100%', height: 35, overflow: 'hidden'}}>
                <Image style={{width: 14, height: 14, resizeMode:'contain', alignSelf: "center"}} source={airplane} />

                <Text size={8}>{duration}</Text>
                
                <Container>
                    <SmallCircle />
                    <View style={{flex: 1, height: 0.6, backgroundColor: ColorBorder}} />
                    {isTransit && <SmallCircle style={{position: 'absolute', alignSelf: 'center'}} />}
                    <SmallCircle />
                </Container>

                <HalfCenter />
            </View>

            <Text size={8}>{isTransit ? 'Transit' : 'Langsung'}</Text>
            
        </MainView>
    )
}

export default TransitInfo;