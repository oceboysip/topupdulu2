import React, { Component } from 'react';
import { Image, View, TextInput, ScrollView, Modal, Platform } from 'react-native';
import Styled from 'styled-components';
import { connect } from 'react-redux';
import Moment from 'moment';
import Ionicons from 'react-native-vector-icons/Ionicons'
import TouchableOpacity from '../Button/TouchableDebounce';
import Button from '../Button';
import ModalIndicator from '../Modal/ModalIndicator';
import ContactBooking from '../ContactBooking';
import ModalInformation from '../Modal/ModalInformation';
// import ModalFlightDetail from '../Modal/ModalFlightDetail';
import PassangersItem from './PassangersItem';
import { getTitles } from '../../state/actions/get-titles';
import { bookFlight } from '../../state/actions/flight/bookFlight';
import Text from '../Text';
import Color from '../Color';
import Header from '../Header';
import FormatMoney from '../FormatMoney';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const ArrowRightIcon = Styled(FontAwesome)`
  fontSize: 16;
  color: ${Color.text};
`;

const MainView = Styled(View)`
  width: 100%;
  height: 100%;
  alignItems: center;
  backgroundColor: #FFFFFF;
`;

const BaseText = Styled(Text)`
  fontSize: 12px
`;

const WhiteText = Styled(Text)`
  color: #FFFFFF
  lineHeight: 12px;
  fontSize: 11px;
`;

const AbsoluteView = Styled(View)`
  position: absolute;
  top: 30;
  zIndex:5;
  width: 100%;
`;
const HeaderView = Styled(TouchableOpacity)`
  minHeight: 1px;
  backgroundColor: #FFFFFF;
  padding: 10px;
  elevation: 5px;
  margin: 0 16px;
  flexDirection: row;
     borderWidth: ${Platform.OS === 'ios' ? 1 : 0 };
    borderBottomWidth: ${Platform.OS === 'ios' ? 2 : 0 };
    borderColor: #DDDDDD;
`;

const PriceView = Styled(View)`
  backgroundColor: #FFFFFF
  flexDirection: row
  padding: 0px 15px
  height: 62px
  width: 100%
`;

const LeftHeaderView = Styled(View)`
  width: 30%;
  justifyContent: flex-start;
  alignItems: flex-start;
`;

const MidHeaderView = Styled(View)`
  width: 70%;
  justifyContent: flex-start;
  alignItems: flex-start;
`;
const MidSubHeader = Styled(View)`

`;
const MidSubHeaderText = Styled(Text)`
  fontSize: 11px;
`;

const RightHeaderView = Styled(View)`
  width: 100%
  height: 100%
  justifyContent: center
  alignItems: flex-end
`;

const BlackHeaderView = Styled(View)`
  backgroundColor: #000000;
  width: 70px;
  height: 20px;
  justifyContent: center;
`;

const ContentView = Styled(View)`

`;

const WrapContentView = Styled(View)`
  flex: 1;
  height: 65px;
  justifyContent: center;
  alignItems: center;
`;
const IconView = Styled(View)`
  justifyContent: center
`;

const PerContent = Styled(View)`
  flexDirection: row
`;

const ContactView = Styled(View)`
  width: 100%
  backgroundColor: #FFFFFF
  borderBottomColor: #DDDDDD
  borderBottomWidth: 1px
`;
const ContactViewPemesan = Styled(View)`
  width: 100%
  backgroundColor: #FFFFFF
  borderBottomColor: #FAF9F9
  borderBottomWidth: 8px
`;
const SubContactView = Styled(View)`
  margin: 22px 15px
  alignItems: flex-start
`;
const SubtotalView = Styled(View)`
  margin: 22px 15px 10px
  alignItems: flex-start
  flexDirection: row
  elevation: 5px
`;

const TextView = Styled(SubtotalView)`
  margin: 0px 10px 0px 0px
  alignItems: flex-start
  flexDirection: row
`;

const LeftView = Styled(MidHeaderView)`
  alignItems: flex-end
`;

const ButtonText = Styled(Text)`
  fontSize: 14px
  color: #FFFFFF
  lineHeight: 34px
`;

const PriceText = Styled(Text)`
  color: #FF425E
`;

const ButtonRadius = Styled(TouchableOpacity)`
  borderRadius: 30px;
  width: 100%;
  backgroundColor: #231F20;
  height: 100%;
  justifyContent: center;
`;

const NormalText = Styled(Text)`
`;

const CustomButton = Styled(Button)`
    width: 100%;
    height: 100%;
`;
const ButtomView = Styled(View)`
  width: 100%
  backgroundColor: #FFFFFF
  alignItems: center
  justifyContent: center
  flexDirection: row
`;

const PassangerView = Styled(TouchableOpacity)`
  alignItems: flex-start
  backgroundColor: #FFFFFF
  padding: 16px
  width: 100%
  borderBottomColor: #DDDDDD
  borderBottomWidth: 1px
`;
const MainBookingView = Styled(View)`
    width: 100%;
    height: 63;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    backgroundColor: #FFFFFF;
    padding: 8px 16px 8px 16px;
    elevation: 20;
`;

const HalfBookingView = Styled(View)`
    width: 50%;
    height: 100%;
    flexDirection: column;
    alignItems: flex-start;
    justifyContent: center;
`;
const titles = [
  { abbr: 'MR', name: 'Mr', isAdult: true },
  { abbr: 'MRS', name: 'Mrs', isAdult: true },
  { abbr: 'MS', name: 'Ms', isAdult: true }
];

  class FlightReviewBooking extends Component {
    static navigationOptions = { header: null }

    constructor(props) {
      super(props);
      let passengers = [], j = 0;

      for (let i = 0; i < props.searchFlight.adult; i++) {
          passengers.push({
              no: i,
              id: j++,
              type: 'ADULT',
              title: '',
              firstName: '',
              lastName: '',
              birthDate: '',
              countryCode: '',
              nationality: '',
              idNumber: '',
              passportNumber: '',
              passportOrigin: '',
              passportExpire: '',
              errors: {
                  title: null,
                  firstName: null,
                  lastName: null,
                  birthDate: null,
                  nationality: null,
                  idNumber: null,
                  passportNumber: null,
                  passportOrigin: null,
                  passportExpire: null,
              }
          });
      }

      for (let i = 0; i < props.searchFlight.child; i++) {
          passengers.push({
              no: i,
              id: j++,
              type: 'CHILD',
              title: '',
              firstName: '',
              lastName: '',
              birthDate: '',
              countryCode: '',
              nationality: '',
              passportNumber: '',
              passportOrigin: '',
              passportExpire: '',
              errors: {
                  title: null,
                  firstName: null,
                  lastName: null,
                  birthDate: null,
                  nationality: null,
                  passportNumber: null,
                  passportOrigin: null,
                  passportExpire: null,
              }
          });
      }

      for (let i = 0; i < props.searchFlight.infant; i++) {
          passengers.push({
              no: i,
              id: j++,
              type: 'INFANT',
              title: '',
              firstName: '',
              lastName: '',
              birthDate: '',
              countryCode: '',
              nationality: '',
              passportNumber: '',
              passportOrigin: '',
              passportExpire: '',
              errors: {
                  title: null,
                  firstName: null,
                  lastName: null,
                  birthDate: null,
                  nationality: null,
                  passportNumber: null,
                  passportOrigin: null,
                  passportExpire: null,
              }
          });
      }

      const { title, firstName, lastName, countryCode, phoneNumber, email } = this.props.user_siago;

      this.state = {
        ModalPassanger: false,
        modalDetail: false,
        modalFail: false,
        modalFailSameContact: false,
        loading: false,
        passengers,
        messageRes: '',
        contact:{
          title: title || '',
          firstName: firstName || '',
          lastName: lastName || '',
          countryCode: countryCode || '62',
          phone: phoneNumber || '',
          email: email || ''
        },
        errors: {
          title: null,
          firstName: null,
          lastName: null,
          countryCode: null,
          phone: null,
          email: null,
        }
      }
    }

    componentDidMount() {
      this.props.clearBooking();
    }

    openModal = (modal) => {
      this.setState({
        [modal]: true
      })
    }

    closeModal = (modal) => {
      this.setState({
        [modal]: false
      })
    }

    deletePassanger = (id) => {
      this.setState(prevState => {
        return {
          passengers: prevState.passengers.map(pax => {
            if(pax.id === id) {
              return  {
                ...pax,
                title: '',
                firstName: '',
                lastName: '',
                birthDate: '',
              };
            }
            return pax;
          })
        }
      });
    }

    onContactChange = (name, value) => {
      this.setState(prevState => {
        return {
          contact: {
            ...prevState.contact,
            [name]: value
          }
        }
      }, () => {console.log(this.state.contact)})
    }

    modalFailSameContact = (modalFailSameContact) => {
      this.setState({ modalFailSameContact })
    }

    onSameContact = (switchOn) => {
      const { title, firstName, lastName } = this.state.contact;
      let passengers = {};

      if (switchOn) {
        passengers = {
          ...this.state.passengers[0],
          title,
          firstName,
          lastName
        }
      }else {
        passengers = {
          ...this.state.passengers[0],
          title: '',
          firstName: '',
          lastName: '',
          birthDate: ''
        }
      }

      this.setState(prevState => {
        return {
          ...prevState,
          passengers: prevState.passengers.map((pax) => {
            if(pax.id === 0) {
              return {
                ...pax,
                title: passengers.title,
                firstName: passengers.firstName,
                lastName: passengers.lastName,
                birthDate: passengers.birthDate
              }
            }else {
              return pax
            }
          })
        }
      });
    }

    onPassangerChange = (name, id, value) => {
      this.setState(prevState => {
        return {
          ...prevState,
          passengers: prevState.passengers.map((pax) => {
            if(pax.id === id){
              return {
                ...pax,
                [name]: value
              }
            } else {
              return pax
            }
          })
        }
      });
    }

    getTitleLabel = (abbr) => {
      const idx = titles.findIndex(title => title.abbr === abbr);
      if (idx !== -1) return titles[idx].name;
      return '';
    }

    getShortMonth(date) {
      return Moment.parseZone(date).format('MMMM').substring(0, 3);
    }

    // static getDerivedStateFromProps(props, state) {
    //   console.log(props, state, 'get derr');

    //   if (props.fetchingBook !== state.loading) {
    //     return {
    //       loading: props.fetchingBook
    //     }
    //   } else {
    //     return {
    //       loading: props.fetchingBook
    //     }
    //   }

    //   return null;
    // }

    // componentDidUpdate(prevProps, prevState) {
    //   console.log(prevProps, prevState, 'didu');
    //   console.log(this.props, this.state, 'init');


    //   if (this.props.fetchingBook !== prevProps.fetchingBook) {
    //     if (!this.props.error && (typeof this.props.booking.id !== 'undefined')) {
    //       console.log('payment')
    //       this.props.navigation.navigate('PaymentScreen');
    //     }else if (this.props.error) {
    //       this.setState({ modalFail: true }, () => setTimeout(() => {
    //         this.setState({ modalFail: false,
    //         })
    //       }, 1000));
    //       console.log(this.props.error, 'error');
    //     }
    //   }
    // }

    componentWillReceiveProps(nextProps) {
      if (!this.props.fetchingBook && nextProps.fetchingBook) {
        this.openModal('loading');
      }else if (this.props.fetchingBook && !nextProps.fetchingBook) {
        this.closeModal('loading');
        if (nextProps.error === null && typeof nextProps.booking.id !== 'undefined') {
          this.props.navigation.navigate('PaymentScreen');
        }else {
          this.setState({ modalFail: true, messageRes: nextProps.error }, () => setTimeout(() => {
            this.setState({ modalFail: false,
            })
          }, 3000));
          console.log(JSON.stringify(nextProps.error));
        }
      }
    }

    submit() {
      const { contact } = this.state;

      if (contact.firstName.length <= 1) {
        this.setState({ modalFail: true, messageRes: 'Nama depan tidak boleh disingkat' }, () => setTimeout(() => {
          this.setState({ modalFail: false, messageRes: '' });
        }, 3000));

        return;
      } else if (contact.lastName.length <= 1) {
        this.setState({ modalFail: true, messageRes: 'Nama belakang tidak boleh disingkat' }, () => setTimeout(() => {
          this.setState({ modalFail: false, messageRes: '' })
        }, 3000));

        return;
      }

      this.props.onBookFlight(
        this.state.contact,
        this.state.passengers,
        this.props.selectedFlights.departureFlight,
        this.props.selectedFlights.returnFlight
      )
    }

    calculateSubtotal() {
      let subtotal = 0;
      const { departureFlight, departureSeatClass, returnFlight, returnSeatClass } = this.props.selectedFlights;
      for (let i = 0; i < departureFlight.segments.length; i++) if (departureFlight.segments[i].classes.length > 0) subtotal += departureFlight.segments[i].classes[departureSeatClass[i]].classFare;
      if (returnFlight) for (let i = 0; i < returnFlight.segments.length; i++) if (returnFlight.segments[i].classes.length > 0) subtotal += returnFlight.segments[i].classes[returnSeatClass[i]].classFare;
      return subtotal;
    }

    renderPassanger(Passangers){
      return Passangers.map((pax, i) => {
        return (
          <PassangersItem
            key={i}
            index={i}
            titles={titles}
            pax={pax}
            id={pax.id}
            deletePassanger={this.deletePassanger}
            onPassangerChange={this.onPassangerChange}

            onSameContact={this.onSameContact}
            contact={this.state.contact}
            ordering={this.state.passengers[0]}
            modalFailSameContact={this.modalFailSameContact}
          />
        )
      })
    }

    renderDepartureInfo(flight) {
      const firstSegment = flight.segments[0];
      const lastSegment = flight.segments[flight.segments.length - 1];
      const firstSegmentDepartsAt = Moment.parseZone(firstSegment.departsAt);
      const lastSegmentArrivesAt = Moment.parseZone(lastSegment.arrivesAt);
      const duration = Moment.duration(lastSegmentArrivesAt.diff(firstSegmentDepartsAt, 'seconds'), 'seconds');
      return (
        <PerContent style={{ marginBottom: 9 }}>
          <LeftHeaderView>
            <BlackHeaderView>
              <WhiteText>Berangkat</WhiteText>
            </BlackHeaderView>
          </LeftHeaderView>
          <MidHeaderView>
            <MidSubHeader>
              <MidSubHeaderText align='left'>{this.getShortMonth(firstSegmentDepartsAt)} {firstSegmentDepartsAt.format('DD ddd YYYY')} | {duration.format('h[j] m[m]')}</MidSubHeaderText>
              <MidSubHeaderText align='left'>{firstSegment.origin.code} - {lastSegment.destination.code} | {firstSegmentDepartsAt.format('HH:mm')} - {lastSegmentArrivesAt.format('HH:mm')}</MidSubHeaderText>
            </MidSubHeader>
          </MidHeaderView>
        </PerContent>
      );
    }

    renderReturnInfo(flight) {
      const firstSegment = flight.segments[0];
      const lastSegment = flight.segments[flight.segments.length - 1];
      const firstSegmentDepartsAt = Moment.parseZone(firstSegment.departsAt);
      const lastSegmentArrivesAt = Moment.parseZone(lastSegment.arrivesAt);
      const duration = Moment.duration(lastSegmentArrivesAt.diff(firstSegmentDepartsAt, 'seconds'), 'seconds');
      return (
        <PerContent>
          <LeftHeaderView>
            <BlackHeaderView>
              <WhiteText>Pulang</WhiteText>
            </BlackHeaderView>
          </LeftHeaderView>
          <MidHeaderView>
            <MidSubHeader>
              <MidSubHeaderText align='left'>{this.getShortMonth(firstSegmentDepartsAt)} {firstSegmentDepartsAt.format('DD ddd YYYY')} | {duration.format('h[j] m[m]')}</MidSubHeaderText>
              <MidSubHeaderText align='left'>{firstSegment.origin.code} - {lastSegment.destination.code} | {firstSegmentDepartsAt.format('HH:mm')} - {lastSegmentArrivesAt.format('HH:mm')}</MidSubHeaderText>
            </MidSubHeader>
          </MidHeaderView>
        </PerContent>
      );
    }

    render() {
      console.log(this.state, 'state');
      console.log(this.props, 'p');

      return (
        <MainView>
          <Header title='Review & Pesan' style={{width: '100%'}} />
          <ScrollView style={{ minHeight: 1, width: '100%' }}>
            {false && <AbsoluteView>
              <HeaderView onPress={() => this.openModal('modalDetail')}>
                  <WrapContentView>
                    {this.renderDepartureInfo(this.props.selectedFlights.departureFlight)}
                    {this.props.selectedFlights.returnFlight && this.renderReturnInfo(this.props.selectedFlights.returnFlight)}
                  </WrapContentView>
                  <IconView>
                     <Text><ArrowRightIcon name='chevron-right' /></Text>
                  </IconView>
              </HeaderView>
            </AbsoluteView>}
            {false && <PriceView />}

            <ContactViewPemesan>
              <ContactBooking
                titles={titles}
                contact={this.state.contact}
                errors={this.state.errors}
                onContactChange={this.onContactChange}
              />
            </ContactViewPemesan>
            <ContactView>
              <SubContactView>
                <Text type='bold' style={{ letterSpacing: 1 }}>DATA PENUMPANG</Text>
              </SubContactView>
              {this.renderPassanger(this.state.passengers)}
            </ContactView>

          </ScrollView>
            <ButtomView>

              <MainBookingView>
                <HalfBookingView>
                  <NormalText type='medium'>Subtotal</NormalText>
                  <PriceText type='bold'>{FormatMoney.getFormattedMoney(this.calculateSubtotal())}</PriceText>
                </HalfBookingView>
                <HalfBookingView>
                  {/*<CustomButton onPress={() => this.submit()}>Bayar Sekarang</CustomButton>*/}
                  <ButtonRadius onPress={() => this.submit()} delay={1000} >
                    <ButtonText>Bayar Sekarang</ButtonText>
                  </ButtonRadius>
                </HalfBookingView>
              </MainBookingView>
            </ButtomView>

            {false && <Modal
              onRequestClose={() => this.closeModal('modalDetail')}
              animationType='slide'
              transparent
              visible={this.state.modalDetail}
            >
              <ModalFlightDetail onClose={() => this.closeModal('modalDetail')} />
            </Modal>}

            <ModalIndicator
                visible={this.state.loading}
                type="large"
                indicators='skype'
                message="Harap tunggu, kami sedang memproses pesanan Anda"
            />

            <Modal
              onRequestClose={() => {}}
              animationType="fade"
              transparent
              visible={this.state.modalFailSameContact}
            >
              <ModalInformation
                label='Lengkapi Data Pemesan terlebih dahulu'
                warning
              />
            </Modal>

            <Modal
              onRequestClose={() => {}}
              animationType="fade"
              transparent
              visible={this.state.modalFail}
            >
              <ModalInformation
                label={this.state.messageRes}
                error
              />
            </Modal>
        </MainView>
      );
    }
  }


const mapStateToProps = state => {
  // console.log(state, 'statre')
  // const selected = selectedFlights(state),
  //       isRoundTrip = state['flight.search'].isRoundTrip;
  //
  //   let returnFlight = selected.returnFlight,
  //       departureFlight = selected.departureFlight;


    // if(departureFlight.comboKeys.length > 0 && returnFlight.comboKeys.length > 0) {
    //     const index = returnFlight.comboKeys.findIndex(comboKey => comboKey === departureFlight.comboKeys[0]);
    //     const comboClasses = returnFlight.comboClasses.filter((classes, idx) => {
    //         if (idx === index) {
    //             return classes
    //         }
    //     });
    //     comboClasses[0].departure.map((classCode, i) =>
    //         departureFlight.segments.map((segment, index) => {
    //             if(i === index ) {
    //                 return {
    //                     ...segment,
    //                     classCode: classCode
    //                 }
    //             }
    //         })
    //     )
    //     comboClasses[0].return.map((classCode, i) =>
    //         departureFlight.segments.map((segment, index) => {
    //             if(i === index ) {
    //                 return {
    //                     ...segment,
    //                     classCode: classCode
    //                 }
    //             }
    //         })
    //     )
    // }

  return {
    // departures: state.selectedFlights.departureFlight,
    // departureSeatClass: state.selectedFlights.departureSeatClass,
    // returnFlight: state.selectedFlights.returnFlight,
    // returnSeatClass: state.selectedFlights.returnSeatClass,
    searchFlight: state.searchFlight,
    selectedFlights: state.selectedFlights,
    user: state['user.auth'].login.user,
    fetchingBook: state.booking.fetching,
    error: state.booking.error,
    booking: state.booking.booking,
    user_siago: state['user.auth'].user_siago
  };
};

const mapDispatchToProps = (dispatch) => (
  {
    onBookFlight: (contact, passengers, departureFlight, returnFlight) => {
      dispatch(bookFlight(contact, passengers, departureFlight, returnFlight));
    },
    getTitles: () => {
      dispatch(getTitles());
    },
    onItemSelect: (type, flightId) => {
      dispatch({
          type: 'FLIGHT.ADD_SELECTED',
          flightType: type.toLowerCase(),
          flightId,
      })
    },
    clearBooking: () => {
      dispatch({
          type: 'BOOKING.CLEAR_BOOKING'
      })
    },
    clearSelectedFlight: () => {
      dispatch({
          type: 'FLIGHT.CLEAR_SELECTED'
      })
    }
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(FlightReviewBooking);
