import React, { Component } from 'react';
import { View, Platform } from 'react-native';
import Styled from 'styled-components';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
// import { createMaterialTopTabNavigator } from 'react-navigation';
import { connect } from 'react-redux';
import { Tabs, Tab, TabHeading } from 'native-base';

import Text from '../Text';
import Color from '../Color';
import Header from '../Header';
import Button from '../Button';
import FormatMoney from '../FormatMoney';

import FlightClass from './FlightClass';
import FlightDetail from './FlightDetail';
import { ScrollView } from 'react-native-gesture-handler';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    flexDirection: column;
`;

const CustomHeader = Styled(Header)`
    height: 60;
`;

const MainBookingView = Styled(View)`
    width: 100%;
    height: 63;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    backgroundColor: #FFFFFF;
    padding: 8px 16px 8px 16px;
    elevation: 20;
`;

const HalfBookingView = Styled(View)`
    width: 50%;
    height: 100%;
    flexDirection: column;
    alignItems: flex-start;
    justifyContent: center;
`;

const CustomButton = Styled(Button)`
    width: 100%;
    height: 100%;
`;

const NormalText = Styled(Text)`
`;

const PriceText = Styled(Text)`
    color: #FF425E;
`;

const HeaderTabsText = Styled(Text)`
    fontSize: 14;
`;

let navigation;

class FlightClassDetail extends Component {

  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    let selectedFlight, currentOrigin, currentDestination;
    navigation = props.navigation;
    if (navigation.state.params.type === 'departure') {
      selectedFlight = props.selectedFlights.departureFlight;
      currentOrigin = props.searchFlight.origin;
      currentDestination = props.searchFlight.destination;
    }
    else {
      selectedFlight = props.selectedFlights.returnFlight;
      currentOrigin = props.searchFlight.destination;
      currentDestination = props.searchFlight.origin;
    }
    this.state = {
      type: navigation.state.params.type,
      processedData: this.processData(selectedFlight),
      chosenClass: this.defaultChosenClass(selectedFlight),
      selectedFlight,
      currentOrigin,
      currentDestination,
    };
  }

  processData(selectedFlight) {
    const processedData = [];
    for (const segment of selectedFlight.segments) {
      const tempArray = [];
      let row = [];
      for (const cls of segment.classes) {
        row.push(cls);
        if (row.length >= 3) {
          tempArray.push(row);
          row = [];
        }
      }
      if (row.length > 0) tempArray.push(row);
      processedData.push(tempArray);
    }
    return processedData;
  }

  defaultChosenClass(selectedFlight) {
    const tempArray = [];
    selectedFlight.segments.forEach(() => tempArray.push(0));
    return tempArray;
  }

  changeClass = (tempArray) => {
    this.setState({ chosenClass: tempArray });
  }

  calculateSubtotal() {
    const { selectedFlight, chosenClass } = this.state;
    let subtotal = 0;
    for (let i = 0; i < selectedFlight.segments.length; i++) if (selectedFlight.segments[i].classes.length > 0) subtotal += selectedFlight.segments[i].classes[chosenClass[i]].classFare;
    return subtotal;
  }

  onBeforeReviewBooking() {
    if (this.props.user.guest) navigation.navigate('LoginScreen', { loginFrom: 'flight', afterLogin: () => navigation.navigate('FlightReviewBooking') });
    else navigation.navigate('FlightReviewBooking');
  }

  onContinueBooking() {
    if (this.state.type === 'departure') {
      this.props.updateSelectedDepartureSeatClass(this.state.chosenClass);
      if (this.props.searchFlight.trip === 1) {
        this.props.noReturnFlight();
        this.onBeforeReviewBooking();
      }
      else navigation.navigate({ routeName: 'HasilSearch', params: { type: 'return' }, key: 'HasilSearchReturn' });
    }
    else {
      this.props.updateSelectedReturnSeatClass(this.state.chosenClass);
      this.onBeforeReviewBooking();
    }
  }

  render() {
    const { currentOrigin, currentDestination, selectedFlight, processedData, chosenClass } = this.state;
    console.log(this.props, 'props flight class');
    console.log(this.state, 'state flight class');

    let airlineName = '';
    selectedFlight.isMultiAirline ? airlineName = 'Multi Airlines' : airlineName = selectedFlight.segments[0].airlineName;

    let operationName = '';
    selectedFlight.segments.length - 1 === 0 ? operationName = 'Direct' : operationName = (selectedFlight.segments.length - 1) + ' Stop'

    return (
      <MainView>
        <Header title={airlineName + ' - ' + operationName} />
        <ScrollView>
          <FlightClass
            type={this.state.type}
            segments={selectedFlight.segments}
            processedData={processedData}
            chosenClass={chosenClass}
            changeClass={this.changeClass}
          />
        </ScrollView>
        <MainBookingView>
          <HalfBookingView>
            <NormalText type='medium'>Subtotal</NormalText>
            <PriceText type='bold'>{FormatMoney.getFormattedMoney(this.calculateSubtotal())}</PriceText>
          </HalfBookingView>
          <HalfBookingView>
            <CustomButton onPress={() => this.onContinueBooking()}>Pilih</CustomButton>
          </HalfBookingView>
        </MainBookingView>
      </MainView>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state['user.auth'].login.user,
  selectedFlights: state.selectedFlights,
  searchFlight: state.searchFlight,
});

const mapDispatchToProps = (dispatch, props) => ({
  updateSelectedDepartureSeatClass: (data) => dispatch({ type: 'SELECTED_DEPARTURE_SEAT_CLASS', data }),
  updateSelectedReturnSeatClass: (data) => dispatch({ type: 'SELECTED_RETURN_SEAT_CLASS', data }),
  noReturnFlight: () => dispatch({ type: 'NO_RETURN_FLIGHT' }),
});

export default connect(mapStateToProps, mapDispatchToProps)(FlightClassDetail);
