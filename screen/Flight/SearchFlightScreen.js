import React, { Component } from "react";
import { Alert, Platform, StyleSheet, Text, View, Image, TextInput, TouchableOpacity , ImageBackground, ScrollView, Dimensions, Button, Switch, Modal } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import SearchFlightStyle from '../../style/SearchFlightStyle';
import ToggleSwitch from "toggle-switch-react-native";

import ModalAirportPicker from '../SearchAirportScreen';
import ModalPassengerPicker from '../Modal/PassengerPicker';
import CalendarScreen from "../CalendarScreen";
import { connect } from 'react-redux';
import Moment from 'moment';
import ModalBox from 'react-native-modalbox';
import Styled from 'styled-components';
import Color from '../Color';
import BannerSlider from '../BannerSlider';
import Header from '../Header';

import { getFlightResult } from '../../state/actions/flight/get-flight-result';

//rubah height nya 50% untuk lebih tinggi
const CustomModalBox = Styled(ModalBox)`
    width: 100%;
    height: 50%;
    backgroundColor: transparent;
    alignItems: center;
`;

const pesanan = require('../../images/icon_paperWhite.png');

class SearchFlightScreen extends Component {
  focusListener;

  constructor(props){
    super(props)
    this.state = {
      trip: 1,
      type: null,
      passengers: { adult: 1, child: 0, infant: 0 },
      modalAirportPicker: false,
      modalDatePicker: false,
      modalPassengerPicker: false,
      origin: { name: 'Soekarno Hatta', code: 'CGK', cityName: 'Jakarta' },
      destination: { name: 'Ngurah Rai (Bali)', code: 'DPS', cityName: 'Denpasar-bali Island' },
      flightClass: 'ALL'
    };

    this.focusListener = props.navigation.addListener(
      'willFocus', this.componentWillFocus
    );
  }

  componentWillFocus = () => {
    if (Moment(Moment(this.props.searchFlight.departureDate).format('YYYY-MM-DD')).isBefore(Moment().format('YYYY-MM-DD'))) this.props.updateDepartureDateParams(Moment());
    if (Moment(Moment(this.props.searchFlight.returnDate).format('YYYY-MM-DD')).isBefore(Moment().format('YYYY-MM-DD'))) this.props.updateReturnDateParams(Moment().add(3, 'd'));
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  openModal = (modal, type) => {
    this.setState({ [modal]: true, type });
  }

  closeModal = (modal) => {
    this.setState({ [modal]: false });
  }

  onSelectedAirport = (item) => {
    const { lastSearch } = this.props.searchFlight;
    let valid = true;

    if (this.state.type) this.setState({ origin: item });
    else this.setState({ destination: item });

    lastSearch.map((ls, idx) => {
      if (ls.code === item.code) valid = false;
    })
    
    if (valid) this.props.updateLastSearchFlight(item);
  }

  onSelectedDate = (date) => {
    if (this.state.type) {
      this.props.updateDepartureDateParams(date);
      if (Moment(Moment(date).format('YYYY-MM-DD')).isAfter(Moment(this.props.searchFlight.returnDate).format('YYYY-MM-DD'))) this.props.updateReturnDateParams(Moment(date).add(3, 'd'));
    }
    else this.props.updateReturnDateParams(date);
    this.setState({ modalDatePicker: false });
  }

  onSavePassengers = (passengers) => {
    this.setState({ passengers, modalPassengerPicker: false });
  }

  onSubmit() {
    const { trip, origin, destination, passengers, flightClass } = this.state;
    if (origin.code === destination.code) return;
    const { departureDate, returnDate } = this.props.searchFlight;
    const { adult, child, infant } = passengers;

    // this.props.resetSearchParams();
    this.props.resetSelectedFlights();
    this.props.updateSearchParams({ trip, departureDate, returnDate, origin, destination, adult, child, infant, flightClass });
    this.props.getFlightResult();
    
    this.props.navigation.navigate({
      routeName: 'HasilSearch',
      params: { type: 'departure' },
      key: 'FlightSearchDeparture'
    });
  }

  openOrderBooking() {
    this.props.navigation.navigate('OrderBooking', {
      title: 'Pesawat'
    })
  }

  render() {
    console.log(this.props, 'props sfs');
    console.log(this.state, 'state sfs');

    const { origin, destination, type, passengers, trip, modalPassengerPicker, modalAirportPicker, modalDatePicker } = this.state;
    const { adult, child, infant } = passengers;
    const { lastSearch, favoriteSearch } = this.props.searchFlight;

    const departureDate = Moment(this.props.searchFlight.departureDate);
    const returnDate = Moment(this.props.searchFlight.returnDate);

    const dataSearch = lastSearch.concat(favoriteSearch);

    return (
      <View style={styles.container}>
        <Header title='Tiket Pesawat' imageRightButton={pesanan} onPressRightButton={() => this.openOrderBooking()} />
        {/* <View style={styles.navTop}>
          <View style={styles.rowsBeetwen}>
            <View style={{flexDirection: 'row',alignItems:'center',paddingLeft:50}}>
              <TouchableOpacity>
                <Text style={styles.tabsActive}>Domestik</Text>
              </TouchableOpacity>
              <TouchableOpacity disabled>
                <Text style={[styles.tabsMenu, {opacity: 0.5}]}>International</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View> */}
        
        <ScrollView>
          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRows}>
                  <View style={styles.iconForm}>
                    <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../../images/icon_flying.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.smallText}>Dari</Text>
                    <TouchableOpacity onPress={() => this.openModal('modalAirportPicker', true)}>
                      <Text style={styles.BigText}>{origin.cityName} ({origin.code})</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={styles.rows2}>
                  <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}} />
                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => this.setState({ origin: destination, destination: origin })}
                    style={{position: 'absolute', right:0, top:-20, paddingLeft: 10, backgroundColor: "#fff"}}
                  >
                    <Image style={{width:40, height:40, resizeMode:'contain'}} source={require('../../images/icon_refresh_orange.png')}/>
                  </TouchableOpacity>
                </View>

                <View style={styles.flexRowsLast}>
                  <View style={styles.iconForm}>
                    <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../../images/icon_depart.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.smallText}>Ke</Text>
                    <TouchableOpacity onPress={() => this.openModal('modalAirportPicker', false)}>
                      <Text style={styles.BigText}>{destination.cityName} ({destination.code})</Text>
                    </TouchableOpacity>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <View style={styles.iconForm}>
                      <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../../images/icon_date.png')}/>
                    </View>
                    <View style={styles.captionForm}>
                      <Text style={styles.smallText}>Berangkat</Text>
                      <TouchableOpacity onPress={() => this.openModal('modalDatePicker', true)}>
                        <Text style={styles.BigText}>{departureDate.format('ddd, DD MMM YYYY')}</Text>
                      </TouchableOpacity>
                    </View>
                  </View>

                  <View style={{alignItems:'flex-end'}}>
                    <Text style={styles.smallText}>Pulang-pergi?</Text>
                     <Switch
                        onValueChange={(value) => this.setState({ trip: value ? 2 : 1 })}
                        value={trip === 2}
                        thumbColor={trip === 2 ? Color.theme : '#DDDDDD'}
                      />
                  </View>
                </View>

                {trip === 2 && <View style={styles.rows2}>
                  <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}} />
                </View>}

                {trip === 2 && <View style={styles.flexRows} hide={trip == 1}>
                  <View style={styles.iconForm}>
                    <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../../images/icon_date.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.smallText}>Pulang</Text>
                    <TouchableOpacity onPress={() => this.openModal('modalDatePicker', false)}>
                      <Text style={styles.BigText}>{returnDate.format('ddd, DD MMM YYYY')}</Text>
                    </TouchableOpacity>
                  </View>
                </View>}

                <View style={styles.rows2}>
                  <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}} />
                </View>

                <View style={styles.flexRowsLast}>
                  <View style={styles.iconForm}>
                    <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../../images/icon_people.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.smallText}>Penumpang</Text>
                    <TouchableOpacity onPress={() => this.openModal('modalPassengerPicker')}>
                      <Text style={styles.BigText}>{adult} Dewasa{child > 0 && `, ${child} Anak`}{infant > 0 && `, ${infant} Bayi`}</Text>
                    </TouchableOpacity>
                  </View>
                </View>

              </View>
            </View>
          </View>
          <View style={styles.alCenter}>
            <TouchableOpacity style={styles.button} onPress={() => this.onSubmit()}>
              <Text style={styles.buttonText}>CARI</Text>
            </TouchableOpacity>
          </View>

          <BannerSlider {...this.props} />
          <View style={{height: 32}} />
        </ScrollView>

        <Modal
          animationType='slide'
          transparent={false}
          visible={modalAirportPicker}
          onRequestClose={() => this.closeModal('modalAirportPicker')}
        >
          <ModalAirportPicker
            onClose={() => this.closeModal('modalAirportPicker')}
            onSelectedAirport={this.onSelectedAirport}
            label={type ? 'Berangkat' : 'Tujuan'}
            dataSearch={dataSearch}
            isHistory={lastSearch.length > 0 ? true : false}
            removeHistory={() => this.props.resetLastSearchFlight()}
          />
        </Modal>

        <Modal
          animationType='slide'
          transparent={false}
          visible={modalDatePicker}
          onRequestClose={() => this.closeModal('modalDatePicker')}
        >

          {/*untuk berangkat dan pusang set nya true/false */}
          <CalendarScreen
            onSelectedDate={this.onSelectedDate}
            onClose={() => this.closeModal('modalDatePicker')}
            selectedDate={type ? departureDate : returnDate}
            label={type ? 'Berangkat' : 'Pulang'}
            minDate={type ? Moment() : departureDate}
            maxDate={Moment().add(1, 'Y')}
          />
        </Modal>

        <CustomModalBox
          position='bottom' 
          isOpen={modalPassengerPicker} 
          onClosed={() => this.closeModal('modalPassengerPicker')}
          coverScreen
          backdropOpacity={0}
        >
          <ModalPassengerPicker 
            onSave={this.onSavePassengers}
            passengers={passengers}
            onClose={() => this.closeModal('modalPassengerPicker')}
          />
        </CustomModalBox>
      </View>
    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}

const mapStateToProps = (state) => ({
  searchFlight: state.searchFlight,
  allState: state
});

const mapDispatchToProps = (dispatch, props) => ({
  updateSearchParams: (params) => dispatch({ type: 'UPDATE_SEARCH_PARAMS', params }),
  updateDepartureDateParams: (params) => dispatch({ type: 'UPDATE_DEPARTURE_DATE_PARAMS', params }),
  updateReturnDateParams: (params) => dispatch({ type: 'UPDATE_RETURN_DATE_PARAMS', params }),
  resetSelectedFlights: () => dispatch({ type: 'RESET_SELECTED_FLIGHTS' }),
  // resetSearchParams: () => dispatch({ type: 'RESET_SEARCH_PARAMS' }),
  updateLastSearchFlight: (params) => dispatch({ type: 'UPDATE_LAST_SEARCH_FLIGHT', params }),
  resetLastSearchFlight: () => dispatch({ type: 'RESET_LAST_SEARCH_FLIGHT' }),
  getFlightResult: () => dispatch(getFlightResult())
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchFlightScreen);


// STYLES
// ------------------------------------
const styles = StyleSheet.create(SearchFlightStyle);