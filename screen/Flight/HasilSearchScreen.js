import React, { Component } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView, FlatList, Dimensions, ActivityIndicator, Modal } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';
import ModalBox from 'react-native-modalbox';
import { connect } from 'react-redux';
import ProgressBar from 'react-native-progress/Bar';

import HasilSearchStyle from '../../style/HasilSearchStyle';
import FormatMoney from '../FormatMoney';
import Color from '../Color';
import Header from '../Header';
import CalendarScreen from '../CalendarScreen';
import ModalFlightSort from '../Modal/FlightSort';
import ModalFlightFilter from '../Modal/FlightFilter';
import TransitInfo from './TransitInfo';

import { getFlightResult } from '../../state/actions/flight/get-flight-result';

const ConfigureContainer = Styled(View)`
  width: 100%;
  minHeight: 1;
  alignItems: center;
  position: absolute;
  bottom: 12;
`;

const ConfigureView = Styled(View)`
  minWidth: 1;
  height: 40;
  flexDirection: row;
  backgroundColor: ${Color.button};
  borderRadius: 100;
  borderWidth: 1;
  borderColor: #000000;
`;

const ConfigureContent = Styled(TouchableOpacity)`
  width: 79;
  height: 100%;
  justifyContent: center;
  alignItems: center;
  flexDirection: column;
`;

const MiddleConfigure = Styled(ConfigureContent)`
  width: 40%;
`;

const FilterIcon = Styled(View)`
  width: 11.11;
  height: 10;
`;

const DateIcon = Styled(View)`
  width: 9;
  height: 10;
`;

const SortIcon = Styled(View)`
  width: 15;
  height: 10;
`;

const ImageProperty = Styled(Image)`
  width: 100%;
  height: 100%;
`;

const ConfigLabel = Styled(Text)`
  fontSize: 12;
  color: #FFFFFF;
`;

const CustomModalBox = Styled(ModalBox)`
    width: 100%;
    height: 405;
    backgroundColor: transparent;
    alignItems: center;
`;

const ResultView = Styled(View)`
    flex: 1;
    justifyContent: center;
`;

const MiniSubtitleView = Styled(View)`
    width: 100%;
    minHeight: 1;
    align-items: center;
`;

const SubtitleText = Styled(Text)`
    fontSize: 14;
`;

const sortOptions = [
  { name: 'lowestPrice', label: 'Lowest Price' },
  { name: 'shortestDuration', label: 'Shortest Duration' },
  { name: 'earliestDeparture', label: 'Earliest Departure' },
  { name: 'latestDeparture', label: 'Latest Departure' },
];

const sort = require('../../images/sort.png');
const filter = require('../../images/filter.png');
const calendar = require('../../images/calendar.png');
const iconLinePesawat = require('../../images/icon_line_pesawat.png');

const { width, height } = Dimensions.get('window');
const formatTime = 'HH:mm:ss';

class HasilSearchScreen extends Component {
  focuslistener;

  constructor(props) {
    super(props)
    this.state = {
      type: props.navigation.state.params.type === 'departure',
      ready: props.navigation.state.params.type === 'departure',
      customKey: 0,
      modalSort: false,
      modalFilter: false,
      modalDatePicker: false,
      optionsPriceFilter: [0, 0],
      optionsAirportTransitFilter: [],
      optionsAirlineFilter: [],
      selectedSort: sortOptions[0].name,
      selectedFilters: {
        airportTransitFilter: [],
        stopFilter: [],
        departureFilter: [],
        arrivalFilter: [],
        airlineFilter: [],
        priceFilter: [0, 0],
      }
    };

    this.focusListener = props.navigation.addListener('willFocus', this.componentWillFocus);
  }

  componentDidMount() {
    let newState = { customKey: this.state.customKey + 1 };
    if (this.props.flightResult) {
      const currentResult = this.state.type ? this.props.flightResult.result.departures : this.props.flightResult.result.returns;
      const optionsFilter = this.getMoreFilterOptions(currentResult, this.props.flightResult.percentage);
      const { selectedFilters } = this.state;
      selectedFilters.priceFilter = optionsFilter.optionsPriceFilter;
      newState = { ...newState, ...optionsFilter, currentResult, selectedFilters };
    }
    this.setState(newState, () => {
      if (!this.state.ready) {
        const tempTimeout = setTimeout(() => { clearTimeout(tempTimeout); this.setState({ ready: true }); }, 1);
      }
    });
  }

  componentWillFocus = () => {
    if (this.props.navigation.state.params && this.props.navigation.state.params.reset) {
      this.resetState(
        () => {
          this.props.getFlightResult();
          this.props.navigation.setParams({ reset: false });
        }
      );
    }
  }

  componentWillReceiveProps(nextProps) {
    let newState = { customKey: this.state.customKey + 1 };
    if (nextProps.flightResult) {
      const currentResult = this.state.type ? nextProps.flightResult.result.departures : nextProps.flightResult.result.returns;
      const optionsFilter = this.getMoreFilterOptions(currentResult, nextProps.flightResult.percentage);
      const { selectedFilters } = this.state;
      selectedFilters.priceFilter = optionsFilter.optionsPriceFilter;
      newState = { ...newState, ...optionsFilter, currentResult, selectedFilters };
    }
    this.setState(newState);
  }

  componentWillUnmount() {
    this.focusListener.remove();
  }

  resetState(func) {
    this.setState({
      ready: true,
      modalSort: false,
      modalFilter: false,
      modalDatePicker: false,
      optionsPriceFilter: [0, 0],
      optionsAirportTransitFilter: [],
      optionsAirlineFilter: [],
      selectedSort: sortOptions[0].name,
      selectedFilters: {
        airportTransitFilter: [],
        stopFilter: [],
        departureFilter: [],
        arrivalFilter: [],
        airlineFilter: [],
        priceFilter: [0, 0],
      }
    }, () => func && func()
    );
  }

  keyExtractor = (flight, index) => `${flight.flightId} - ${index} - ${this.state.customKey}`;

  getShortMonth(month) {
    return Moment(month, 'M').format('MMMM').substring(0, 3);
  }

  getFlightData(type) {
    const { searchFlight, flightResult } = this.props;
    const { departureDate, returnDate, origin, destination } = searchFlight;
    if (type) return { currentOrigin: origin, currentDestination: destination, currentDate: departureDate, currentResult: flightResult.result.departures };
    return { currentOrigin: destination, currentDestination: origin, currentDate: returnDate, currentResult: flightResult.result.returns };
  }

  openModal = (modal) => {
    this.setState({ ready: false, [modal]: true});
  }

  closeModal = (modal) => {
    this.setState({ [modal]: false }, () => {
      const tempTimeout = setTimeout(() => {
        clearTimeout(tempTimeout);
        this.setState({ ready: true });
      }, 1);
    });
  }

  onCloseFilter(selectedFilters) {
    this.setState({ ready: false, modalFilter: false, selectedFilters, customKey: this.state.customKey + 1 }, () => {
      const tempTimeout = setTimeout(() => { clearTimeout(tempTimeout); this.setState({ ready: true }); }, 1);
    });
  }

  onSelectedSort = (selectedSort) => {
    this.setState({ ready: false, modalSort: false, selectedSort, customKey: this.state.customKey + 1 }, () => {
      const tempTimeout = setTimeout(() => { clearTimeout(tempTimeout); this.setState({ ready: true }); }, 1);
    });
  }

  onSelectedFlight(data) {
    if (this.state.type) this.props.updateSelectedDepartureFlight(data);
    else this.props.updateSelectedReturnFlight(data);

    this.props.navigation.navigate({
      routeName: 'FlightClassDetail',
      params: { type: this.state.type ? 'departure' : 'return' },
      key: `FlightClassDetail${this.state.type}`
    });
  }

  onSelectedDate(date) {
    this.setState({
      modalDatePicker: false
    }, () => {
      const tempTimeout = setTimeout(() => {
        if (this.state.type) {
          this.props.updateDepartureDateParams(date);
          if (Moment(Moment(date).format('YYYY-MM-DD')).isAfter(Moment(this.props.searchFlight.returnDate).format('YYYY-MM-DD'))) this.props.updateReturnDateParams(Moment(date).add(3, 'd'));
          this.resetState(this.props.getFlightResult());
        }
        else {
          this.props.updateReturnDateParams(date);
          this.props.navigation.navigate({
            routeName: 'FlightResultScreen',
            params: { type: 'departure', reset: true },
            key: 'FlightResultScreenDeparture'
          });
        }
        clearTimeout(tempTimeout);
      }, 1);
    });
  }

  filterStop(result, stopFilter) {
    if (stopFilter.length <= 0) return result;
    let tempResult = [];
    for (const stop of stopFilter) tempResult = tempResult.concat(result.filter((flight) => {
      switch (stop) {
        case 'Direct': return flight.segments.length <= 1;
        case '1 Stop': return flight.segments.length === 2;
        default: return flight.segments.length >= 3;
      }
    }));
    return tempResult;
  }

  filterAirportTransit(result, airportTransitFilter) {
    if (airportTransitFilter.length <= 0) return result;
    let tempResult = [];
    for (const airportTransit of airportTransitFilter) tempResult = tempResult.concat(result.filter((flight) => {
      if (flight.segments.length <= 1) return true;
      for (let i = 1; i < flight.segments.length; i++) if (flight.segments[i].origin.cityName === airportTransit) return true;
      return false;
    }));
    return tempResult;
  }

  filterDeparture(result, departureFilter) {
    if (departureFilter.length <= 0) return result;
    let index = [];
    for (const departure of departureFilter) {
      const min = Moment(`${departure.substring(0, 5)}:00`, formatTime);
      const max = Moment(`${departure.substr(departure.length - 5)}:00`, formatTime);
      for (let i = 0; i < result.length; i++) if (Moment(Moment.parseZone(result[i].segments[0].departsAt).format(formatTime), formatTime).isBetween(min, max, null, '[]')) index.push(i);
    }
    index = [...new Set(index)];
    const tempResult = [];
    for (const i of index) tempResult.push(result[i]);
    return tempResult;
  }

  filterArrival(result, arrivalFilter) {
    if (arrivalFilter.length <= 0) return result;
    let index = [];
    for (const departure of arrivalFilter) {
      const min = Moment(`${departure.substring(0, 5)}:00`, formatTime);
      const max = Moment(`${departure.substr(departure.length - 5)}:00`, formatTime);
      for (let i = 0; i < result.length; i++) if (Moment(Moment.parseZone(result[i].segments[result[i].segments.length - 1].arrivesAt).format(formatTime), formatTime).isBetween(min, max, null, '[]')) index.push(i);
    }
    index = [...new Set(index)];
    const tempResult = [];
    for (const i of index) tempResult.push(result[i]);
    return tempResult;
  }

  filterAirline(result, airlineFilter) {
    if (airlineFilter.length <= 0) return result;
    const tempResult = [];
    for (const flight of result) {
      for (let segment = 0; segment < flight.segments.length; segment++) {
        for (let airline = 0; airline < airlineFilter.length; airline++) {
          if (flight.segments[segment].airlineName === airlineFilter[airline]) {
            tempResult.push(flight);
            segment = flight.segments.length;
            airline = airlineFilter.length;
          }
        }
      }
    }
    return tempResult;
  }

  filterPrice(result, priceFilter) {
    if (this.state.optionsPriceFilter[0] === this.state.optionsPriceFilter[1]) return result;
    const tempResult = [];
    for (const flight of result) if (flight.fare >= priceFilter[0] && flight.fare <= priceFilter[1]) tempResult.push(flight);
    return tempResult;
  }

  filterResult(result, selectedFilters) {
    let filteredResult;
    const { stopFilter, airportTransitFilter, departureFilter, arrivalFilter, airlineFilter, priceFilter } = selectedFilters;
    filteredResult = this.filterStop(result, stopFilter);
    filteredResult = this.filterAirportTransit(filteredResult, airportTransitFilter);
    filteredResult = this.filterDeparture(filteredResult, departureFilter);
    filteredResult = this.filterArrival(filteredResult, arrivalFilter);
    filteredResult = this.filterAirline(filteredResult, airlineFilter);
    filteredResult = this.filterPrice(filteredResult, priceFilter);
    return filteredResult;
  }

  customizeResult(ready, result, selectedSort, selectedFilters) {
    if (!ready) return [];
    const customResult = this.filterResult(result, selectedFilters);
    switch (selectedSort) {
      case 'shortestDuration': customResult.sort((a, b) => Moment.duration(Moment(a.segments[a.segments.length - 1].arrivesAt).diff(Moment(a.segments[0].departsAt), 'seconds'), 'seconds') - Moment.duration(Moment(b.segments[b.segments.length - 1].arrivesAt).diff(Moment(b.segments[0].departsAt), 'seconds'), 'seconds')); break;
      case 'earliestDeparture': customResult.sort((a, b) => { return Moment(a.segments[0].departsAt).isBefore(Moment(b.segments[0].departsAt)) ? -1 : 1; }); break;
      case 'latestDeparture': customResult.sort((a, b) => { return Moment(a.segments[0].departsAt).isAfter(Moment(b.segments[0].departsAt)) ? -1 : 1; }); break;
      default: customResult.sort((a, b) => a.fare - b.fare); break;
    }
    return customResult;
  }

  getMoreFilterOptions(currentResult, percentage) {
    const optionsAirportTransitFilter = [];
    const optionsAirlineFilter = [];
    let optionsPriceFilter = [0, 0];
    if (percentage < 100) {
      for (const flight of currentResult) {
        for (let i = 0; i < flight.segments.length; i++) {
          optionsAirlineFilter.push(flight.segments[i].airlineName);
          if (i > 0 && i < flight.segments.length) optionsAirportTransitFilter.push(flight.segments[i].origin.cityName);
        }
      }
    }
    else {
      const allFares = [];
      for (const flight of currentResult) {
        allFares.push(flight.fare);
        for (let i = 0; i < flight.segments.length; i++) {
          optionsAirlineFilter.push(flight.segments[i].airlineName);
          if (i > 0 && i < flight.segments.length) optionsAirportTransitFilter.push(flight.segments[i].origin.cityName);
        }
      }
      optionsPriceFilter = [Math.min(...allFares), Math.max(...allFares)];
    }
    return { optionsAirportTransitFilter: [...new Set(optionsAirportTransitFilter)], optionsAirlineFilter: [...new Set(optionsAirlineFilter)], optionsPriceFilter };
  }

  renderResult(result) {
    return (
      <FlatList
        contentContainerStyle={{ paddingHorizontal: 8, paddingTop: 8, paddingBottom: 60 }}
        keyExtractor={this.keyExtractor}
        data={result}
        renderItem={({ item }) => this.renderFlightResult(item)}
      />
    );
  }

  renderLoadingIndicator() {
    return <ActivityIndicator size='large' color={Color.theme} />;
  }

  switchRender(ready, atLeastOneResult, loading, finalResult) {
    if (!ready) return this.renderLoadingIndicator();
    if (finalResult.length > 0) return this.renderResult(finalResult);
    if (loading) return this.renderLoadingIndicator();
    return <MiniSubtitleView><SubtitleText type='bold'>Penerbangan Tidak Ditemukan{atLeastOneResult && ', silahkan ubah filter Anda'}</SubtitleText></MiniSubtitleView>;
  }

  //Flight result CARD
  getAirlinesLogos(segments) {
    let airlineLogos = [];
    for (const segment of segments) airlineLogos.push(segment.airlineLogo);
    airlineLogos = [...new Set(airlineLogos)];
    return airlineLogos;
  }

  renderAirlinesImage(airlineLogos) {
    return airlineLogos.map((airlineLogo, i) =>
      <View key={i} style={{height: 20, width: 40}}>
        <Image resizeMode='contain' source={{ uri: airlineLogo }} style={{height: '100%', width: '100%'}} />
      </View>
    );
  }

  renderFlightResult(item) {
    const { segments, fare, isMultiAirline } = item;
    const firstSegment = segments[0];
    const lastSegment = segments[segments.length - 1];
    const departsAt = Moment(firstSegment.departsAt);
    const arrivesAt = Moment(lastSegment.arrivesAt);
    let duration = arrivesAt.diff(departsAt, 'seconds');
    duration = Moment.duration(duration, 'seconds');

    return (
      <View>
        <View style={styles.boxShadow}>
          <View style={styles.boxInner_bt}>
            <View style={styles.flexRowsBeetwen}>
              <View style={{flexDirection:'row', alignItems:'center', justifyContent: 'center'}}>
                <View style={[styles.iconForm, {alignItems: 'center', justifyContent: 'space-between'}]}>
                  {this.renderAirlinesImage(this.getAirlinesLogos(segments))}
                  <Text style={{fontSize: 8}}>{isMultiAirline ? 'Multi airlines' : firstSegment.airlineName}</Text>
                </View>
                <View style={styles.captionForm}>
                  {/*<Text style={styles.smallTextStrike}>IDR 851.845</Text>*/}
                  <Text style={styles.BigText}>{FormatMoney.getFormattedMoney(fare, 'IDR')}</Text>
                  <Text style={{fontSize: 12, color:'#1a558f'}}>/org</Text>
                </View>
              </View>
              <View style={{alignItems: 'flex-end'}}>
                <TouchableOpacity onPress={() => this.onSelectedFlight(item)}>
                  <Text style={styles.buttonYellow}>Pilih</Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.rows2}>
              <View style={{flex:1, height:2, backgroundColor:'#fcdf00'}}>
              </View>
            </View>

            <View style={styles.rowsLast}>
              <View style={{flexDirection: 'row', alignItems:'center', paddingVertical: 0}}>
                <View style={styles.testTop2}>
                  <Text style={styles.middleTextGreen}>{firstSegment.origin.code}</Text>
                  <Text style={styles.middleTextGreen}>{Moment.parseZone(departsAt).format('HH:mm')}</Text>
                </View>
                {/* <Image style={{width:72, height:36, resizeMode:'contain'}} source={require('../../images/logo_time.png')}/> */}
                <TransitInfo
                  duration={duration.format('h[j] m[m]')}
                  isTransit={segments.length > 1}
                />
                <View style={styles.testTop2}>
                  <Text style={styles.middleTextGreen}>{lastSegment.destination.code}</Text>
                  <Text style={styles.middleTextGreen}>{Moment.parseZone(arrivesAt).format('HH:mm')}</Text>
                </View>
              </View>
            </View>

          </View>
        </View>
      </View>
    )
  }

  render() {
    const { type, ready, optionsAirportTransitFilter, optionsAirlineFilter, optionsPriceFilter, selectedSort, selectedFilters, modalSort, modalFilter, modalDatePicker } = this.state;
    const { searchFlight, flightResult } = this.props;
    const { adult, child, infant } = searchFlight;
    const { percentage, loading } = flightResult;
    const { currentOrigin, currentDestination, currentDate, currentResult } = this.getFlightData(type);
    const finalResult = this.customizeResult(ready, currentResult, selectedSort, selectedFilters);
    const atLeastOneResult = currentResult.length > 0;

    const departureDate = Moment(searchFlight.departureDate);
    const returnDate = Moment(searchFlight.returnDate);

    return (
      <View style={styles.container}>
        <Header style={{paddingVertical: 6}}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <View style={{alignItems: "center"}}>
              <Text style={styles.borderYellow}>DARI</Text>
              {/*<Text style={styles.biggerText}>{currentOrigin.code}</Text>*/}
            </View>

            <View style={{flex: 1, alignItems: 'center'}}>
              <Image style={{flex: 1, height: 23}} resizeMode='center' source={iconLinePesawat}/>
            </View>

            <View style={{alignItems: "center"}}>
              <Text style={styles.borderYellow}>KE</Text>
              {/*<Text style={styles.biggerText}>{currentDestination.code}</Text>*/}
            </View>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-between', width: '100%', marginTop: 8}}>
            <View style={{width: '30%', alignItems: 'flex-start'}}>
              <Text style={[styles.smallWhite, {textAlign: 'left'}]}>{currentOrigin.cityName}</Text>
            </View>
            <View style={{width: '40%'}}>
              <Text style={[styles.smallWhite]}>{Moment(currentDate).format('ddd, DD MMM YY')}</Text>
            </View>
            <View style={{width: '30%', alignItems: 'flex-end'}}>
              <Text style={[styles.smallWhite, {textAlign: 'right', flex: 1}]}>{currentDestination.cityName}</Text>
            </View>
          </View>
        </Header>
        
        {percentage < 100 && <ProgressBar progress={percentage} width={width} color={Color.primary} />}

        <ResultView>
          {this.switchRender(ready, atLeastOneResult, loading, finalResult)}
        </ResultView>

        {atLeastOneResult && ready && <ConfigureContainer>
          <ConfigureView>
            <ConfigureContent onPress={() => this.openModal('modalFilter')}>
              <FilterIcon><ImageProperty resizeMode='contain' source={filter}/></FilterIcon>
              <ConfigLabel>Filter</ConfigLabel>
            </ConfigureContent>
            <MiddleConfigure onPress={() => this.openModal('modalDatePicker')}>
              <DateIcon><ImageProperty resizeMode='contain' source={calendar}/></DateIcon>
              <ConfigLabel>Ubah Tanggal</ConfigLabel>
            </MiddleConfigure>
            <ConfigureContent onPress={() => this.openModal('modalSort')}>
              <DateIcon><ImageProperty resizeMode='contain' source={sort}/></DateIcon>
              <ConfigLabel>Sort</ConfigLabel>
            </ConfigureContent>
          </ConfigureView>
        </ConfigureContainer>}

        <Modal
          visible={modalFilter}
          animationType='slide'
          onRequestClose={() => this.closeModal('modalFilter')}
        >
          <ModalFlightFilter
            onClose={(filters) => this.onCloseFilter(filters)}
            optionsFilter={{ optionsAirportTransitFilter, optionsAirlineFilter, optionsPriceFilter }}
            selectedFilters={selectedFilters}
          />
        </Modal>

        <Modal
          visible={modalDatePicker}
          animationType='slide'
          onRequestClose={() => this.closeModal('modalDatePicker')}
        >
          <CalendarScreen
            onSelectedDate={(date) => this.onSelectedDate(date)}
            onClose={() => this.closeModal('modalDatePicker')}
            selectedDate={type ? departureDate : returnDate}
            label={type ? 'Depart' : 'Return'}
            minDate={type ? Moment() : departureDate}
            maxDate={Moment().add(1, 'Y')}
          />
        </Modal>

        <CustomModalBox
          position='bottom'
          isOpen={modalSort}
          onClosed={() => this.closeModal('modalSort')}
          coverScreen
        >
          <ModalFlightSort
            sortOptions={sortOptions}
            selected={selectedSort}
            onSelectedSort={this.onSelectedSort}
          />
        </CustomModalBox>
      </View>
    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}

// STYLES
// ------------------------------------
const styles = StyleSheet.create(HasilSearchStyle);

const mapStateToProps = (state) => ({
  searchFlight: state.searchFlight,
  flightResult: state.flightResult
});

const mapDispatchToProps = (dispatch, props) => ({
  updateSelectedDepartureFlight: (data) => dispatch({ type: 'SELECTED_DEPARTURE_FLIGHT', data }),
  updateSelectedReturnFlight: (data) => dispatch({ type: 'SELECTED_RETURN_FLIGHT', data }),
  updateDepartureDateParams: (params) => dispatch({ type: 'UPDATE_DEPARTURE_DATE_PARAMS', params }),
  updateReturnDateParams: (params) => dispatch({ type: 'UPDATE_RETURN_DATE_PARAMS', params }),
  getFlightResult: () => dispatch(getFlightResult()),
});

export default connect(mapStateToProps, mapDispatchToProps)(HasilSearchScreen);
