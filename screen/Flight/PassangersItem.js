import React, { Component } from 'react';
import { Image, View, Modal, Switch } from 'react-native';
import Styled from 'styled-components';
import TouchableOpacity from '../Button/TouchableDebounce';
import ModalPassanger from '../Modal/ModalPassanger';
import ModalPassangerTour from '../Modal/ModalPassangerTour';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Color from '../Color';
import Text from '../Text';

const MainView = Styled(View)`
  minHeight: 1;
  width: 100%;
  flexDirection: column;
`;

const SubtotalView = Styled(View)`
  margin: 22px 15px 10px
  alignItems: flex-start
  flexDirection: row
  elevation: 5px
`;

const TextView = Styled(SubtotalView)`
  margin: 0px 10px 0px 0px
  alignItems: flex-start
  flexDirection: row
`;
const MidHeaderView = Styled(View)`
  width: 70%;
  minHeight: 1;
  alignItems: flex-start;
`;

const SwitchTouch = Styled(TouchableOpacity)`
  width: 30%;
  alignItems: flex-end;
  minHeight: 1;
`;

const LeftView = Styled(View)`
  width: 70%
  justifyContent: flex-start
  alignItems: flex-end
`;

const LeftHeaderView = Styled(View)`
  width: 30%
  justifyContent: flex-start
  alignItems: flex-start
`;

const PassangerView = Styled(TouchableOpacity)`
  alignItems: flex-start
  backgroundColor: #FFFFFF
  padding: 16px
  width: 100%
  borderBottomColor: #DDDDDD
  borderBottomWidth: 1px
`;

const SamaPemesanView = Styled(View)`
  width: 100%;
  minHeight: 1;
  padding: 0px 16px 0px 16px;
  justifyContent: space-between;
  alignItems: center;
  flexDirection: row;
`;

const TitleView = Styled(View)`
  flexDirection: row;
  justifyContent: center;
`;

const ImageAdd = Styled(Image)`
  width: 15;
  height: 15;
  marginTop: 2;
  marginRight: 10;
`;

const IconCreate = Styled(Ionicons)`
  fontSize: 18;
`;

const add = require('../../images/add.png');
const btndisabled = require('../../images/btndisabled.png');
const btnactive = require('../../images/btnactive.png');

const ageTypeView = {
  ADULT: 'Dewasa',
  CHILD: 'Anak',
};

const piceTypeView = {
  SINGLE: 'Single',
  TWIN_SHARING: 'Twin Sharing',
  EXTRA_BED: 'Extra Bed',
  NO_BED: 'No Bed'
};

export default class PassangersItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      switchOn: false,
      modalPassangers: false
    };
  }

  openModal = (modal) => {
    this.setState({
      [modal]: true
    })
  }

  closeModal = (modal, id, name) => {
    if(name !== 'simpan') this.props.deletePassanger(id)
    this.setState({
      [modal]: false
    })

    const { contact, ordering } = this.props;
    const sameTitle = (contact.title === ordering.title);
    const sameFirstName = (contact.firstName === ordering.firstName);
    const sameLastName = (contact.lastName === ordering.lastName);

    if (sameTitle && sameFirstName && sameLastName) {
      this.setState({ switchOn: true });
    }else {
      this.setState({ switchOn: false });
    }
  }

  validateForm() {
    const { title, firstName, lastName } = this.props.contact;
    let result;

    if (title !== '' && firstName !== '' && lastName !== '') result = true;
    else result = false;

    return result;
  }

  onSwitch(result) {
    console.log(result, 'tes result');
    
    if (result) {
      this.setState({ switchOn: !this.state.switchOn }, () => {
        this.props.onSameContact(this.state.switchOn);
        this.openModal('modalPassangers');
      })
    }else {
      this.props.modalFailSameContact(true);
      let timeout = setTimeout(() => {
        clearTimeout(timeout);
        this.props.modalFailSameContact(false);
      }, 3000);
    }
  }

  onPressPassenger(result) {
    if (result) {
      this.openModal('modalPassangers')
    }else {
      this.props.modalFailSameContact(true);
      let timeout = setTimeout(() => {
        clearTimeout(timeout);
        this.props.modalFailSameContact(false);
      }, 3000);
    }
  }

  renderOnSwitch() {
    const result = this.validateForm();
    return (
      <Switch
        onValueChange={() => this.onSwitch(result)}
        value={this.state.switchOn}
        thumbColor={Color.theme}
      />
    )
    // return (
    //   <SwitchTouch activeOpacity={0.5} onPress={() => this.onSwitch(result)}>
    //     <Image source={this.state.switchOn ? btnactive : btndisabled} style={{width: 49, height: 27}} />
    //   </SwitchTouch>
    // )
  }

  renderOnSameCOntact(index, tour) {
    if (index === 0 && tour && this.props.roomId === 0) {
      return (
        <SamaPemesanView style={{backgroundColor: '#FFFFFF', paddingTop: 8}}>
          <MidHeaderView>
            <Text type='medium'>Sama dengan Pemesan</Text>
          </MidHeaderView>
          {this.renderOnSwitch()}
        </SamaPemesanView>
      )
    }

    if (index === 0 && !tour) {
      return (
        <SamaPemesanView>
          <MidHeaderView>
            <Text type='medium'>Sama dengan Pemesan</Text>
          </MidHeaderView>
          {this.renderOnSwitch()}
        </SamaPemesanView>
      )
    }
  }

  render() {
    const { pax, id, tour, index } = this.props;
    const result = this.validateForm();

    return (
      <MainView>
        {this.renderOnSameCOntact(index, tour)}
        <PassangerView onPress={() => this.onPressPassenger(result)} key={pax.id}>
          {pax.firstName === '' && <TitleView>
            <ImageAdd source={add} />
            {!tour && <Text type='medium'>Nama Penumpang {id + 1}</Text>}
            {tour && <Text type='medium'>Wisatawan {id+1} ({ageTypeView[pax.type]})</Text>}
          </TitleView>}

          {pax.firstName !== '' && <TextView>
            <LeftHeaderView style={{width: '90%'}}>
              <Text type='medium'>{pax.firstName + ' ' + pax.lastName}</Text>
            </LeftHeaderView>
            <LeftView style={{width: '10%'}}>
              <Text type='bold' align='right'><IconCreate name='md-create' /></Text>
            </LeftView>
          </TextView>}

          <Modal
            onRequestClose={() => this.closeModal('modalPassangers')}
            animationType="fade"
            transparent
            visible={this.state.modalPassangers}
          >
            {!tour && <ModalPassanger
              closeModal={this.closeModal}
              titles={this.props.titles}
              onPassangerChange={this.props.onPassangerChange}
              pax={pax}
            />}
            {tour && <ModalPassangerTour
              closeModal={this.closeModal}
              titles={this.props.titles}
              onPassangerChange={this.props.onPassangerChange}
              pax={pax}
              id={id}
            />}
          </Modal>
        </PassangerView>
      </MainView>
    );
  }
}
