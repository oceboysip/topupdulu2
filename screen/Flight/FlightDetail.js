import React, { Component } from 'react';
import { Image, View, ScrollView } from 'react-native';
import Moment from 'moment';
import Styled from 'styled-components';
import Accordion from 'react-native-collapsible/Accordion';
import Ionicons from 'react-native-vector-icons/Ionicons';

import Text from '../Text';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    alignItems: center;
    flexDirection: column;
    backgroundColor: #FFFFFF;
`;

const HeaderContainer = Styled(View)`
    width: 100%;
    height: 50;
    alignItems: center;
    flexDirection: row;
    paddingHorizontal: 16;
    backgroundColor: #FFFFFF;
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
`;

const HeaderSectionLeft = Styled(View)`
    width: 90%;
    height: 100%;
    alignItems: flex-start;
    justifyContent: center;
`;

const HeaderSectionRight = Styled(View)`
    width: 10%;
    height: 100%;
    justifyContent: center;
    alignItems: flex-end;
`;

const AccordionSectionContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    paddingHorizontal: 16;
    flexDirection: column;
`;

const FlightDetailView = Styled(View)`
    width: 100%;
    minHeight: 100;
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
    flexDirection: column;
    paddingVertical: 10;
`;

// const RefundView = Styled(View)`
//     width: 100%;
//     height: 50;
//     flexDirection: row;
// `;
//
// const RefundLeftView = Styled(View)`
//     width: 50%;
//     height: 100%;
//     alignItems: flex-start;
//     justifyContent: center;
// `;
//
// const RefundRightView = Styled(RefundLeftView)`
//     alignItems: flex-end;
// `;

const OneFlightContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
`;

const LineView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
`;

const TimeView = Styled(View)`
    width: 50;
    minHeight: 1;
    alignItems: center;
`;

const CodeNameAirportView = Styled(View)`
    flex: 1;
    alignItems: center;
    justifyContent: flex-start;
    flexDirection: row;
    paddingRight: 10;
`;

const AirportTextContainer = Styled(View)`
    flex: 1;
    paddingRight: 10;
`;

const CircleView = Styled(View)`
    width: 15;
    height: 22;
    flexDirection: column;
    marginRight: 8;
`;

const DottedView = Styled(View)`
    width: 8;
    height: 50;
    alignItems: flex-end;
    justifyContent: center;
`;

const DottedImageView = Styled(View)`
    width: 1;
    height: 100%;
`;

const GreyTransitContainer = Styled(View)`
    flex: 1;
    justifyContent: center;
    paddingLeft: 2;
`;

const GreyTransitView = Styled(View)`
    height: 25;
    alignItems: flex-start;
    justifyContent: center;
    backgroundColor: #F4F4F4;
    paddingLeft: 14;
`;

const DepartureTopCircleView = Styled(View)`
    width: 100%;
    height: 57.5%;
    alignItems: center;
    justifyContent: flex-end;
`;

const DepartureBottomCircleView = Styled(DepartureTopCircleView)`
    height: 42.5%;
    justifyContent: flex-start;
`;

const DestinationTopCircleView = Styled(DepartureBottomCircleView)`
    justifyContent: flex-end;
`;

const DestinationBottomCircleView = Styled(DepartureTopCircleView)`
    justifyContent: flex-start;
`;

const FullVerticalLine = Styled(CircleView)`
    height: 100%;
    alignItems: center;
`;

const VerticalLineView = Styled(DepartureTopCircleView)`
    width: 0.5;
    height: 100%;
    backgroundColor: #000000;
`;

const MainAirlineInfo = Styled(View)`
    flex: 1;
    height: 100%;
    alignItems: flex-start;
    justifyContent: flex-start;
    flexDirection: column;
`;

const AirlineInfo = Styled(View)`
    width: 100%;
    minHeight: 1;
    alignItems: center;
    justifyContent: flex-start;
    flexDirection: row;
`;

const BaggageIcon = Styled(View)`
    width: 13;
    height: 10;
`;

const AirlineLogo = Styled(View)`
    width: 40;
    height: 20;
    alignItems: flex-start;
    justifyContent: center;
    margin: 4px 10px 2px 0px;
`;

const MainDurationView = Styled(LineView)`
    height: 60;
`;

const DurationView = Styled(View)`
    width: 50;
    height: 100%;
    justifyContent: center;
    alignItems: center;
`;

const BlackCircle = Styled(View)`
    width: 5;
    height: 5;
    borderRadius: 5;
    backgroundColor: #000000;
`;

const WhiteCircle = Styled(BlackCircle)`
    backgroundColor: #FFFFFF;
    borderWidth: 1;
    borderColor: #000000;
`;

const AccordionTitle = Styled(Text)`
  letterSpacing: 1
`;

// const RefundText = Styled(Text)`
// `;

const TimeText = Styled(Text)`
`;

const CodeText = Styled(Text)`
    fontSize: 12;
    marginRight: 20;
    textAlign: left;
`;

const FlightNumberText = Styled(Text)`
    fontSize: 11;
    textAlign: left;
`;

const AirportText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const DurationText = Styled(Text)`
    fontSize: 11;
    color: #A8A699;
`;

const AirlineInfoText = Styled(Text)`
    fontSize: 11;
    color: #A8A699;
    textAlign: left;
`;

const TransitText = Styled(Text)`
    fontSize: 11;
    color: #A8A699;
    textAlign: left;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

// const UnderlinedRefundText = Styled(RefundText)`
//   textDecorationLine: underline;
// `;

const IconArrow = Styled(Ionicons)`
    color: #333333;
`;

const baggage = require('../../images/baggage.png');
const dotted = require('../../images/dotted-black.png');

export default class FlightDetail extends Component {

  constructor(props) {
    super(props);
    this.state = {
      activeSection: 0,
    };
  }

  onChangeAccordionActive = (activeSection) => {
    this.props.onChangeAccordionActive(activeSection);
  };

  renderFlights(i, segments) {
    const segment = segments[i];
    const departsAt = Moment(segment.departsAt);
    const arrivesAt = Moment(segment.arrivesAt);
    let duration = arrivesAt.diff(departsAt, 'seconds');
    duration = Moment.duration(duration, 'seconds');
    return (
      <OneFlightContainer>
        <LineView>
          <TimeView><TimeText type='medium'>{Moment.parseZone(departsAt).format('HH:mm')}</TimeText></TimeView>
          <CircleView>
            <DepartureTopCircleView>{i === 0 ? <BlackCircle /> : <WhiteCircle />}</DepartureTopCircleView>
            <DepartureBottomCircleView><VerticalLineView /></DepartureBottomCircleView>
          </CircleView>
          <CodeNameAirportView>
            <CodeText type='semibold'>{segment.origin.code}</CodeText>
            <AirportTextContainer><AirportText type='semibold'>{segment.origin.name}</AirportText></AirportTextContainer>
          </CodeNameAirportView>
        </LineView>
        <MainDurationView>
          <DurationView><DurationText type='medium'>{false && duration.format('h[j] m[m]')}</DurationText></DurationView>
          <FullVerticalLine><VerticalLineView /></FullVerticalLine>
          <MainAirlineInfo>
            <AirlineInfo>
              <AirlineLogo><ImageProperty resizeMode='contain' source={{ uri: segment.airlineLogo }} /></AirlineLogo>
              <FlightNumberText type='bold'>{segment.carrierCode} {segment.flightNumber}</FlightNumberText>
            </AirlineInfo>
            {segment.classes.length > 0 && <AirlineInfo>
              <AirlineInfoText type='medium'>{segment.classes[this.props.chosenClass[i]].classType === 'E' ? 'Economy' : 'Business'} - </AirlineInfoText>
              <BaggageIcon><ImageProperty resizeMode='stretch' source={baggage} /></BaggageIcon>
              <AirlineInfoText type='medium'> {segment.classes[this.props.chosenClass[i]].baggage}</AirlineInfoText>
            </AirlineInfo>}
          </MainAirlineInfo>
        </MainDurationView>
        <LineView>
          <TimeView><TimeText type='medium'>{Moment.parseZone(arrivesAt).format('HH:mm')}</TimeText></TimeView>
          <CircleView>
            <DestinationTopCircleView><VerticalLineView /></DestinationTopCircleView>
            <DestinationBottomCircleView>{i === segments.length - 1 ? <BlackCircle /> : <WhiteCircle />}</DestinationBottomCircleView>
          </CircleView>
          <CodeNameAirportView>
            <CodeText type='semibold'>{segment.destination.code}</CodeText>
            <AirportText type='semibold'>{segment.destination.name}</AirportText>
          </CodeNameAirportView>
        </LineView>
      </OneFlightContainer>
    );
  }

  renderTransit(i, segments) {
    const segment = segments[i];
    const nextSegment = segments[i + 1];
    let transitDuration = Moment(nextSegment.departsAt).diff(Moment(segment.arrivesAt), 'seconds');

    return (
      <LineView>
        <TimeView />
        <DottedView>
          <DottedImageView>
            <ImageProperty resizeMode='stretch' source={dotted} />
          </DottedImageView>
        </DottedView>
        <GreyTransitContainer>
          <GreyTransitView>
            <TransitText type='medium'>Transit di {nextSegment.origin.cityName}, {false && Moment.duration(transitDuration, 'seconds').format('h[j] m[m]')}</TransitText>
          </GreyTransitView>
        </GreyTransitContainer>
      </LineView>
    );
  }

  renderAllDetail() {
    const { segments } = this.props.selectedFlight;
    return segments.map((segment, i) =>
      <View key={i}>
        {this.renderFlights(i, segments)}
        {i < segments.length - 1 && this.renderTransit(i, segments)}
      </View>
    );
  }

  render() {
    return (
      <MainView>
        {this.renderAllDetail()}
      </MainView>
    );
  }
}
