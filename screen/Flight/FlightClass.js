import React, { Component } from 'react';
import { Image, View, ScrollView, TouchableOpacity as NativeTouchable, Text as ReactText } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';
import Text from '../Text';
import FormatMoney from '../FormatMoney';
import Color from '../Color';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    backgroundColor: #FFFFFF;
    flexDirection: column;
`;

const MainClass = Styled(View)`
    width: 100%;
    minHeight: 1;
    padding: 0px 16px 0px 16px;
    flexDirection: column;
    marginBottom: 20;
`;

const HeaderClass = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
    padding: 8px 0px 16px 0px;
`;

const HalfHeader = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
`;

const AirlineImageContainer = Styled(View)`
    width: 40;
    height: 20;
    marginRight: 14;
`;

const AllClassContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    borderWidth: 1;
    borderColor: ${Color.theme};
    borderRadius: 3;
`;

const PerRowClassContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    borderBottomWidth: 0.5;
    borderColor: ${Color.theme};
`;

const OneClassContainer = Styled(NativeTouchable)`
    width: 33.33%;
    minHeight: 88;
    justifyContent: center;
    alignItems: center;
    borderRightWidth: 0.5;
    borderColor: ${Color.theme};
`;

const TransitView = Styled(View)`
    width: 100%;
    minHeight: 1;
    alignItems: flex-start;
    padding: 6px 16px 6px 16px;
    backgroundColor: #231F20;
`;

const CheckIcon = Styled(View)`
    width: 16;
    height: 11;
    marginTop: 6;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const HeaderText = Styled(Text)`
    fontSize: 12;
`;

const ClassText = Styled(Text)`
    fontSize: 14;
`;

const PriceClassText = Styled(Text)`
    fontSize: 14;
    color: #FF425E;
`;

const TransitText = Styled(Text)`
    fontSize: 12;
    color: #FFFFFF;
`;

const SmallerHeaderText = Styled(HeaderText)`
    fontSize: 11;
`;

const CustomScrollView = Styled(ScrollView)`
  height: 100%;
  paddingBottom: 8;
`;

//detail
const OneFlightContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
`;

const LineView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    paddingLeft: 16;
    paddingRight: 10;
`;

const TimeView = Styled(View)`
    width: 50;
    minHeight: 1;
    alignItems: center;
`;

const CircleView = Styled(View)`
    width: 15;
    height: 20;
    flexDirection: column;
    marginRight: 8;
`;

const DepartureTopCircleView = Styled(View)`
    width: 100%;
    height: 57.5%;
    alignItems: center;
    justifyContent: flex-end;
`;

const DepartureBottomCircleView = Styled(DepartureTopCircleView)`
    height: 42.5%;
    justifyContent: flex-start;
`;

const DestinationBottomCircleView = Styled(DepartureTopCircleView)`
    justifyContent: flex-start;
`;

const DestinationTopCircleView = Styled(DepartureBottomCircleView)`
    justifyContent: flex-end;
`;

const FullVerticalLine = Styled(CircleView)`
    height: 100%;
    alignItems: center;
`;

const BlackCircle = Styled(View)`
    width: 5;
    height: 5;
    borderRadius: 5;
    backgroundColor: #000000;
`;

const WhiteCircle = Styled(BlackCircle)`
    backgroundColor: #FFFFFF;
    borderWidth: 1;
    borderColor: #000000;
`;

const VerticalLineView = Styled(DepartureTopCircleView)`
    width: 0.5;
    height: 100%;
    backgroundColor: #000000;
`;

const CodeNameAirportView = Styled(View)`
    flex: 1;
    alignItems: center;
    justifyContent: flex-start;
    flexDirection: row;
    paddingRight: 10;
`;

const MainDurationView = Styled(LineView)`
    height: 80;
    justifyContent: center;
`;

const DurationView = Styled(View)`
    width: 50;
    height: 100%;
    justifyContent: center;
    alignItems: center;
`;

const MainAirlineInfo = Styled(View)`
    flex: 1;
    height: 100%;
    alignItems: flex-start;
    justifyContent: center;
    flexDirection: column;
`;

const AirlineInfo = Styled(View)`
    width: 100%;
    minHeight: 1;
    alignItems: center;
    justifyContent: flex-start;
    flexDirection: row;
`;

const BaggageIcon = Styled(View)`
    width: 13;
    height: 10;
`;

const AirlineLogo = Styled(View)`
    width: 40;
    height: 20;
    alignItems: flex-start;
    justifyContent: center;
    margin: 4px 10px 2px 0px;
`;

const DottedView = Styled(View)`
    width: 8;
    height: 50;
    alignItems: flex-end;
    justifyContent: center;
`;

const DottedImageView = Styled(View)`
    width: 1;
    height: 100%;
`;

const GreyTransitContainer = Styled(View)`
    flex: 1;
    justifyContent: center;
`;

const GreyTransitView = Styled(View)`
    height: 25;
    alignItems: flex-start;
    justifyContent: center;
    backgroundColor: #F4F4F4;
    paddingLeft: 14;
`;

const TimeCustomText = Styled(ReactText)`
  fontSize: 15;
  backgroundColor: ${Color.theme};
  borderRadius: 5;
  paddingHorizontal: 3;
  paddingBottom: 1;
  color: #FFFFFF;
`;

const AirportCustomText = Styled(ReactText)`
  fontSize: 15;
  backgroundColor: #DDDDDD;
  borderRadius: 5;
  paddingHorizontal: 3;
  paddingBottom: 1;
  color: #FFFFFF;
  fontWeight: bold;
`;

const check = require('../../images/check.png');
const checkDisabled = require('../../images/check-disabled.png');

const baggage = require('../../images/baggage.png');
const dotted = require('../../images/dotted-black.png');

const flying = require('../../images/icon_flying.png');
const depart = require('../../images/icon_depart.png');

export default class FlightClass extends Component {

  constructor(props) {
    super(props);
    this.state = {
      segments: props.segments,
      processedData: props.processedData,
      chosenClass: props.chosenClass,
    };
  }

  changeClass = (segmentId, chosen) => {
    const tempArray = this.state.chosenClass;
    tempArray[segmentId] = chosen;
    this.setState({ chosenClass: tempArray });
    this.props.changeClass(tempArray);
  }

  renderOneClass(classes, row, segmentId) {
    const { chosenClass } = this.state;
    return classes.map((cls, i) =>
      <OneClassContainer 
        activeOpacity={1}
        key={i} onPress={() => this.changeClass(segmentId, (row * 3) + i)} 
        style={classes.length - 1 === i && classes.length === 3 && {borderRightWidth: 0}}
        style={chosenClass[segmentId] === (row * 3) + i && {backgroundColor: '#F4F4F4'}}
      >
        <ClassText type='medium'>Class {cls.classCode}</ClassText>
        <PriceClassText type='bold'>{FormatMoney.getFormattedMoney(cls.classFare)}</PriceClassText>
        <CheckIcon><ImageProperty resizeMode='stretch' source={chosenClass[segmentId] === (row * 3) + i ? check : checkDisabled} /></CheckIcon>
      </OneClassContainer>
    );
  }

  renderPerRowClass(data, segmentId) {
    return data.map((row, i) =>
      <PerRowClassContainer key={i} style={data.length - 1 === i && { borderBottomWidth: 0 }}>
        {this.renderOneClass(row, i, segmentId)}
      </PerRowClassContainer>
    );
  }

  renderClassPerFlight() {
    const { segments, processedData } = this.state;
    
    return segments.map((segment, i) => {
      const nextSegment = segments[i + 1];
      let durationHours, durationMinutes;
      if (nextSegment) {
        let durationNumber = Moment(nextSegment.departsAt).diff(Moment(segment.arrivesAt), 'seconds');
        durationHours = Moment.duration(durationNumber, 'seconds').hours();
        durationMinutes = Moment.duration(durationNumber, 'seconds').minutes();
      }
      
      return (
        <View key={i}>
          <MainClass>
            <HeaderClass>
              <HalfHeader>
                <AirlineImageContainer>
                  <ImageProperty resizeMode='contain' source={{uri: segment.airlineLogo}} />
                </AirlineImageContainer>
                <SmallerHeaderText type='bold'>{segment.carrierCode} {segment.flightNumber}</SmallerHeaderText>
              </HalfHeader>

              <HalfHeader>
                <HeaderText type='bold'>{segment.origin.code} - {segment.destination.code} | {Moment.parseZone(segment.departsAt).format('HH:mm')} - {Moment.parseZone(segment.arrivesAt).format('HH:mm')}</HeaderText>
              </HalfHeader>
            </HeaderClass>

            {processedData[i].length > 0 && <AllClassContainer>
              {this.renderPerRowClass(processedData[i], i)}
            </AllClassContainer>}
          </MainClass>
          {nextSegment && <TransitView>
            <TransitText type='medium'>Transit di {nextSegment.origin.cityName}, {durationHours + 'j ' + durationMinutes + 'm'}</TransitText>
          </TransitView>}
        </View>
      );
    });
  }

  //detail flight
  renderFlights(i, segments) {
    const segment = segments[i];
    const departsAt = Moment(segment.departsAt);
    const arrivesAt = Moment(segment.arrivesAt);

    let durationNumber = arrivesAt.diff(departsAt, 'seconds');
    let durationHours = Moment.duration(durationNumber, 'seconds').hours();
    let durationMinutes = Moment.duration(durationNumber, 'seconds').minutes();

    return (
      <OneFlightContainer>
        <LineView>
          <CircleView>
            {false && <DepartureTopCircleView>{i === 0 ? <BlackCircle /> : <WhiteCircle />}</DepartureTopCircleView>}
            <Image style={{width: 17, height: 17}} resizeMode='contain' source={flying} />
            <DepartureBottomCircleView><VerticalLineView /></DepartureBottomCircleView>
          </CircleView>
          <CodeNameAirportView>
            <AirportCustomText>{segment.origin.code}</AirportCustomText>
            <Text> {segment.origin.name}</Text>
          </CodeNameAirportView>
          <TimeView>
            <ReactText>{Moment.parseZone(departsAt).format('HH:mm')}</ReactText>
            <ReactText style={{fontSize: 12}}>{Moment.parseZone(departsAt).format('DD MMM')}</ReactText>
          </TimeView>
        </LineView>

        <MainDurationView>
          <FullVerticalLine><VerticalLineView /></FullVerticalLine>
          <MainAirlineInfo>
            <AirlineInfo>
              <AirlineLogo><ImageProperty resizeMode='contain' source={{ uri: segment.airlineLogo }} /></AirlineLogo>
              <Text type='bold'>{segment.carrierCode} {segment.flightNumber}</Text>
            </AirlineInfo>
            {segment.classes.length > 0 && <AirlineInfo>
              <Text type='medium'>{segment.classes[this.props.chosenClass[i]].classType === 'E' ? 'Economy' : 'Business'} - </Text>
              <BaggageIcon><ImageProperty resizeMode='stretch' source={baggage} /></BaggageIcon>
              <Text type='medium'> {segment.classes[this.props.chosenClass[i]].baggage}</Text>
            </AirlineInfo>}
          </MainAirlineInfo>
          <DurationView>
            <TimeCustomText>{durationHours > 0 && durationHours + 'j '}{durationMinutes + 'm'}</TimeCustomText>
          </DurationView>
        </MainDurationView>

        <LineView>
          <CircleView style={{top: -6}}>
            <DestinationTopCircleView><VerticalLineView /></DestinationTopCircleView>
            <Image style={{width: 17, height: 17}} resizeMode='contain' source={depart} />
            {false && <DestinationBottomCircleView>{i === segments.length - 1 ? <BlackCircle /> : <WhiteCircle />}</DestinationBottomCircleView>}
          </CircleView>
          <CodeNameAirportView>
            <AirportCustomText>{segment.destination.code}</AirportCustomText>
            <Text type='semibold'> {segment.destination.name}</Text>
          </CodeNameAirportView>
          <TimeView>
            <ReactText type='medium'>{Moment.parseZone(arrivesAt).format('HH:mm')}</ReactText>
            <ReactText style={{fontSize: 12}}>{Moment.parseZone(arrivesAt).format('DD MMM')}</ReactText>
          </TimeView>
        </LineView>
      </OneFlightContainer>
    );
  }

  renderTransit(i, segments) {
    const segment = segments[i];
    const nextSegment = segments[i + 1];
    let durationHours, durationMinutes;
    if (nextSegment) {
      let durationNumber = Moment(nextSegment.departsAt).diff(Moment(segment.arrivesAt), 'seconds');
      durationHours = Moment.duration(durationNumber, 'seconds').hours();
      durationMinutes = Moment.duration(durationNumber, 'seconds').minutes();
    }

    return (
      <LineView>
        <DottedView>
          <DottedImageView>
            <ImageProperty resizeMode='stretch' source={dotted} />
          </DottedImageView>
        </DottedView>
        <GreyTransitContainer>
          <GreyTransitView>
            <Text type='medium'>Transit di {nextSegment.origin.cityName}</Text>
          </GreyTransitView>
        </GreyTransitContainer>
        <TimeView style={{backgroundColor: '#F4F4F4', height: 25, justifyContent: 'center'}}>
          <Text>{durationHours > 0 && durationHours + 'j '} {durationMinutes + 'm'}</Text>
        </TimeView>
      </LineView>
    );
  }

  renderAllDetail() {
    const { segments } = this.state;
    return segments.map((segment, i) =>
      <View key={i}>
        {this.renderFlights(i, segments)}
        {i < segments.length - 1 && this.renderTransit(i, segments)}
      </View>
    );
  }

  renderHeader(text, type) {
    return (
      <View style={{height: 42, backgroundColor: Color.theme, marginBottom: 8, justifyContent: 'center'}}>
        <Text style={{color: '#FFFFFF'}}>{text} {type === 'departure' ? 'Keberangkatan' : 'Kepulangan'}</Text>
      </View>
    )
  }

  render() {
    const { type } = this.props;
    let isShown = true;
    this.state.segments.map((segment, index) => {
      if (segment.classes.length <= 1) isShown = false;
      else isShown = true;
    })
    console.log(isShown, 'shown');
    
    return (
      <MainView>
        <CustomScrollView>
          {isShown && this.renderHeader('Pilih Kelas', type)}
          {isShown && this.renderClassPerFlight()}
          {this.renderHeader('Rincian', type)}
          {this.renderAllDetail()}
        </CustomScrollView>
      </MainView>
    );
  }
}
