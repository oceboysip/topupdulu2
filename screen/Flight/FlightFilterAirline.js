import React, { Component } from 'react';
import { View, ScrollView, Image, TouchableOpacity as NativeTouchable } from 'react-native';
import Styled from 'styled-components';

import Text from '../Text';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    flexDirection: column;
`;

const OneFilterView = Styled(NativeTouchable)`
    width: 100%;
    height: 50;
    flexDirection: row;
    paddingHorizontal: 16;
    alignItems: center;
    justifyContent: space-between;
    borderColor: #DDDDDD;
    borderBottomWidth: 0.5;
`;

const OneTypeOption = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
`;

const FilterLabelView = Styled(View)`
    flex: 1;
    minHeight: 1;
    paddingRight: 16;
`;

const TitleFilter = Styled(View)`
    width: 100%;
    height: 50;
    alignItems: flex-start;
    justifyContent: center;
    paddingHorizontal: 16;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const Checkbox = Styled(View)`
    width: 20;
    height: 20;
`;

const FilterLabel = Styled(Text)`
    textAlign: left;
`;

const TitleFilterText = Styled(Text)`
    textAlign: left;
`;

const checked = require('../../images/checkbox-selected.png');
const checkoff = require('../../images/checkbox-unselected.png');

const OneFilter = (props) => {
  const { label, active } = props;
  return (
    <OneFilterView {...props}>
      <FilterLabelView><FilterLabel>{label}</FilterLabel></FilterLabelView>
      <Checkbox>
        <ImageProperty source={active ? checked : checkoff} />
      </Checkbox>
    </OneFilterView>
  );
};

export default class FlightFilterAirline extends Component {

  constructor(props) {
    super(props);
    this.state = {
      airline: {
        title: 'AIRLINE',
        label: '',
        type: 'airlineFilter',
      },
    };
  }

  renderOneOption(mode) {
    const tempArray = this.props.selectedFilters[mode.type];
    return mode.options.map((opt, i) =>
      <OneFilter
        key={i}
        activeOpacity={1}
        onPress={() => this.props.onChangeFilter(opt, tempArray.indexOf(opt) === -1, mode.type)}
        label={opt + mode.label}
        active={tempArray.indexOf(opt) !== -1}
      />
    );
  }

  renderOptions(mode) {
    return (
      <OneTypeOption>
        {mode.title && <TitleFilter>
          <TitleFilterText type='bold'>{mode.title}</TitleFilterText>
        </TitleFilter>}
        {this.renderOneOption(mode)}
      </OneTypeOption>
    );
  }

  render() {
    const { airline } = this.state;
    airline.options = this.props.optionsAirlineFilter;
    return (
      <MainView>
        <ScrollView>
          {this.renderOptions(airline)}
        </ScrollView>
      </MainView>
    );
  }

}
