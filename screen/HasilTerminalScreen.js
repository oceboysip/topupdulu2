/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {Alert,Platform, StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions, Button,Switch} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import HasilTerminalStyle from '../style/HasilTerminalStyle';

import ToggleSwitch from "toggle-switch-react-native";

export default class HasilTerminalScreen extends Component<{}> {

  constructor(props){
  super(props)
    this.state = {
      isOnDefaultToggleSwitch: false,
      isOnLargeToggleSwitch: false,
      isOnBlueToggleSwitch: false,
      isHidden: false
    };
  }

  onToggle(isOn) {
    console.log("Changed to " + isOn);
  }
  Dashboard = () => {
    this.props.navigation.navigate('Dashboard');
  }
  CalendarBerangkat = () => {
    this.props.navigation.navigate('Calendar');
  }
  CalendarPulang = () => {
    this.props.navigation.navigate('CalendarPulang');
  }
  BookedTrain = () => {
    this.props.navigation.navigate('BookedTrain');
  }
  render() {
    return (
      <View style={styles.container}>

        <View style={styles.navTop}>
          <View style={styles.rowsBeetwen}>
            <View style={{flexDirection: 'row',alignItems:'center'}}>
              <TouchableOpacity onPress={this.BookedTrain}>
                <Image style={{width:30, height:30,resizeMode:'contain'}} source={require('../images/left-arrow.png')}/>
              </TouchableOpacity>
              <Text style={styles.titlePage}>Pencarian Pergi</Text>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={{flexDirection: 'row',alignItems:'center',paddingLeft:30}}>
              <View style={styles.testTop}>
                <Text style={styles.borderYellow}>DARI</Text>
                <Text style={styles.biggerText}>GMR</Text>
                <Text style={styles.smallWhite}>JAKARTA</Text>
              </View>

              <Image style={{width:150, height:23,resizeMode:'contain'}} source={require('../images/icon_line_train.png')}/>

               <View style={styles.testTop}>
                <Text style={styles.borderYellow}>KE</Text>
                <Text style={styles.biggerText}>BD</Text>
                <Text style={styles.smallWhite}>BANDUNG</Text>
              </View>
              
            </View>

          </View>

        </View>

        
        <ScrollView>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <View style={styles.iconForm}>
                      <Image style={{width:60, height:60, resizeMode:'contain'}}  source={require('../images/logo_kereta.png')}/>
                    </View>
                    <View style={styles.captionForm}>
                      <Text style={styles.smallTextStrike}>IDR 851.845</Text>
                        <Text style={styles.BigText}>IDR 765.987</Text>
                    </View>
                  </View>
                  <View style={{alignItems:'flex-end'}}>
                    <TouchableOpacity onPress={this.SearchFlight}>
                      <Text style={styles.buttonYellow}>Pilih</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={styles.rows2}>
                  <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}}>
                  </View>
                </View>

                <View style={styles.rowsLast}>
                  <View style={{flexDirection: 'row',alignItems:'center'}}>
                    <View style={styles.testTop2}>
                      <Text style={styles.middleTextGreen}>GMR</Text>
                      <Text style={styles.bigTextGreen}>20:20</Text>
                    </View>
                    <Image style={{width:72, height:72,resizeMode:'contain'}} source={require('../images/logo_time_kereta.png')}/>
                    <View style={styles.testTop2}>
                      <Text style={styles.middleTextGreen}>BD</Text>
                      <Text style={styles.bigTextGreen}>20:20</Text>
                    </View>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <View style={styles.iconForm}>
                      <Image style={{width:60, height:60, resizeMode:'contain'}}  source={require('../images/logo_kereta.png')}/>
                    </View>
                    <View style={styles.captionForm}>
                      <Text style={styles.smallTextStrike}>IDR 851.845</Text>
                        <Text style={styles.BigText}>IDR 765.987</Text>
                    </View>
                  </View>
                  <View style={{alignItems:'flex-end'}}>
                    <TouchableOpacity onPress={this.SearchFlight}>
                      <Text style={styles.buttonYellow}>Pilih</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={styles.rows2}>
                  <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}}>
                  </View>
                </View>

                <View style={styles.rowsLast}>
                  <View style={{flexDirection: 'row',alignItems:'center'}}>
                    <View style={styles.testTop2}>
                      <Text style={styles.middleTextGreen}>GMR</Text>
                      <Text style={styles.bigTextGreen}>20:20</Text>
                    </View>
                    <Image style={{width:72, height:72,resizeMode:'contain'}} source={require('../images/logo_time_kereta.png')}/>
                    <View style={styles.testTop2}>
                      <Text style={styles.middleTextGreen}>BD</Text>
                      <Text style={styles.bigTextGreen}>20:20</Text>
                    </View>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <View style={styles.iconForm}>
                      <Image style={{width:60, height:60, resizeMode:'contain'}}  source={require('../images/logo_kereta.png')}/>
                    </View>
                    <View style={styles.captionForm}>
                      <Text style={styles.smallTextStrike}>IDR 851.845</Text>
                        <Text style={styles.BigText}>IDR 765.987</Text>
                    </View>
                  </View>
                  <View style={{alignItems:'flex-end'}}>
                    <TouchableOpacity onPress={this.SearchFlight}>
                      <Text style={styles.buttonYellow}>Pilih</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={styles.rows2}>
                  <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}}>
                  </View>
                </View>

                <View style={styles.rowsLast}>
                  <View style={{flexDirection: 'row',alignItems:'center'}}>
                    <View style={styles.testTop2}>
                      <Text style={styles.middleTextGreen}>GMR</Text>
                      <Text style={styles.bigTextGreen}>20:20</Text>
                    </View>
                    <Image style={{width:72, height:72,resizeMode:'contain'}} source={require('../images/logo_time_kereta.png')}/>
                    <View style={styles.testTop2}>
                      <Text style={styles.middleTextGreen}>BD</Text>
                      <Text style={styles.bigTextGreen}>20:20</Text>
                    </View>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <View style={styles.iconForm}>
                      <Image style={{width:60, height:60, resizeMode:'contain'}}  source={require('../images/logo_kereta.png')}/>
                    </View>
                    <View style={styles.captionForm}>
                      <Text style={styles.smallTextStrike}>IDR 851.845</Text>
                        <Text style={styles.BigText}>IDR 765.987</Text>
                    </View>
                  </View>
                  <View style={{alignItems:'flex-end'}}>
                    <TouchableOpacity onPress={this.SearchFlight}>
                      <Text style={styles.buttonYellow}>Pilih</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={styles.rows2}>
                  <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}}>
                  </View>
                </View>

                <View style={styles.rowsLast}>
                  <View style={{flexDirection: 'row',alignItems:'center'}}>
                    <View style={styles.testTop2}>
                      <Text style={styles.middleTextGreen}>GMR</Text>
                      <Text style={styles.bigTextGreen}>20:20</Text>
                    </View>
                    <Image style={{width:72, height:72,resizeMode:'contain'}} source={require('../images/logo_time_kereta.png')}/>
                    <View style={styles.testTop2}>
                      <Text style={styles.middleTextGreen}>BD</Text>
                      <Text style={styles.bigTextGreen}>20:20</Text>
                    </View>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <View style={styles.iconForm}>
                      <Image style={{width:60, height:60, resizeMode:'contain'}}  source={require('../images/logo_kereta.png')}/>
                    </View>
                    <View style={styles.captionForm}>
                      <Text style={styles.smallTextStrike}>IDR 851.845</Text>
                        <Text style={styles.BigText}>IDR 765.987</Text>
                    </View>
                  </View>
                  <View style={{alignItems:'flex-end'}}>
                    <TouchableOpacity onPress={this.SearchFlight}>
                      <Text style={styles.buttonYellow}>Pilih</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={styles.rows2}>
                  <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}}>
                  </View>
                </View>

                <View style={styles.rowsLast}>
                  <View style={{flexDirection: 'row',alignItems:'center'}}>
                    <View style={styles.testTop2}>
                      <Text style={styles.middleTextGreen}>GMR</Text>
                      <Text style={styles.bigTextGreen}>20:20</Text>
                    </View>
                    <Image style={{width:72, height:72,resizeMode:'contain'}} source={require('../images/logo_time_kereta.png')}/>
                    <View style={styles.testTop2}>
                      <Text style={styles.middleTextGreen}>BD</Text>
                      <Text style={styles.bigTextGreen}>20:20</Text>
                    </View>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <View style={styles.iconForm}>
                      <Image style={{width:60, height:60, resizeMode:'contain'}}  source={require('../images/logo_kereta.png')}/>
                    </View>
                    <View style={styles.captionForm}>
                      <Text style={styles.smallTextStrike}>IDR 851.845</Text>
                        <Text style={styles.BigText}>IDR 765.987</Text>
                    </View>
                  </View>
                  <View style={{alignItems:'flex-end'}}>
                    <TouchableOpacity onPress={this.SearchFlight}>
                      <Text style={styles.buttonYellow}>Pilih</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={styles.rows2}>
                  <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}}>
                  </View>
                </View>

                <View style={styles.rowsLast}>
                  <View style={{flexDirection: 'row',alignItems:'center'}}>
                    <View style={styles.testTop2}>
                      <Text style={styles.middleTextGreen}>GMR</Text>
                      <Text style={styles.bigTextGreen}>20:20</Text>
                    </View>
                    <Image style={{width:72, height:72,resizeMode:'contain'}} source={require('../images/logo_time_kereta.png')}/>
                    <View style={styles.testTop2}>
                      <Text style={styles.middleTextGreen}>BD</Text>
                      <Text style={styles.bigTextGreen}>20:20</Text>
                    </View>
                  </View>
                </View>

              </View>
            </View>
          </View>

        </ScrollView>
      </View>
    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}




// STYLES
// ------------------------------------
const styles = StyleSheet.create(HasilTerminalStyle);