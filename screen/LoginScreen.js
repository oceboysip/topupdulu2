import React, { Component } from 'react';
import { Alert, StyleSheet, Text as ReactText, View, Image, TouchableOpacity, ImageBackground } from 'react-native';
import { Spinner } from 'native-base';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import FloatingLabel from 'react-native-floating-labels';
import { connect } from 'react-redux';
import Styled from 'styled-components';

import LoginStyle from '../style/LoginStyle';
import passShow from '../images/eye_open.png';
import passHide from '../images/eye_close.png';
import Color from './Color';
import ModalIndicator from './Modal/ModalIndicator';
import { login } from '../state/actions/user/auth';
import { NavigationActions, StackActions } from 'react-navigation';
import Text from './Text';
import ModalBanner from './Modal/ModalBanner';

import { constant } from '../constants';

const Checkbox = Styled(TouchableOpacity)`
  width: 14;
  aspectRatio: 1;
  marginRight: 4;
`;

const ImageProperty = Styled(Image)`
  width: 100%;
  height: 100%;
`;

const checked = require('../images/checkbox-selected.png');
const checkoff = require('../images/checkbox-unselected.png');

class LoginScreen extends Component {
	getUserState = false;

	constructor(props) {
		super(props);
		const { params } = props.navigation.state;
		let modalInformation = false;
		let messageInformation = '';

		if (params) {
			if (params.logout) {
				modalInformation = true;
				messageInformation = 'Sesi Anda telah habis';
			}
		}

		this.props.navigation.setParams({ logout: false });

		this.state = {
			username: props.userSavedAccount.username, 
			password: props.userSavedAccount.password,
			showPassword: true,
			saveAccount: false,
			inputValue: '',
			fieldActive: false,
			dirty: false,
			user: null,
			modalInformation,
			messageInformation
		};
	}

	componentDidMount() {
		this.getUserState = true;
		this._loadInitialState().done();

		if (this.getUserState) {
			setTimeout(() => {
				this.setState({ modalInformation: false });
			}, 3000);
		}
	}

	componentWillUnmount() {
		this.getUserState = false;
	}

	_loadInitialState = async () => {
		// CEK SESSION
		var value = await AsyncStorage.getItem('user');
		this.props.screenProps.setLoading(false);

		if (this.props.user_siago !== null) {
			const resetAction = StackActions.reset({
			  index: 0,
			  actions: [NavigationActions.navigate({ routeName: 'Dashboard' })],
		  	});
	
			this.props.navigation.dispatch(resetAction);
			
			if (this.getUserState) this.setState({ user: value });
		}
	}

	login = () => {
		this.props.screenProps.setUser(null);
		this.props.screenProps.setLoading(true);
	
		axios.post(constant.api_url+"/user/auth", {
			email: this.state.username,
			password: this.state.password
		}, {
			headers: {'X-Auth-Token': constant.api_token},
		})
		.then(response => {
			res = response.data;
			this.props.screenProps.setLoading(false);
			console.log(res, 'res login'); 

			if (res.status=="success"){
			  	this.props.screenProps.setUser(null);
				this.props.screenProps.setLoading(false);
				
				AsyncStorage.setItem('user', JSON.stringify(res.data));

				this.props.onLogin(constant.EMAIL_LOGIN, constant.PASSWORD_LOGIN);

				this.props.userSiagoLogin(res.data);

				if (this.state.saveAccount) this.props.onUserSavedAccount({ username: this.state.username, password: this.state.password });

				const resetAction = StackActions.reset({
					index: 0,
					actions: [NavigationActions.navigate({ routeName: 'Dashboard' })],
				});

				this.props.navigation.dispatch(resetAction);
			} else {

				if (this.state.username=="topupdulu" && this.state.password=="top") {
					this.props.screenProps.setLoading(false);
					const resetAction = StackActions.reset({
					index: 0,
					actions: [NavigationActions.navigate({ routeName: 'Dashboard' })],
					});
					this.props.navigation.dispatch(resetAction);
				}
				else {
					this.props.screenProps.setLoading(false);
					this.stateTimeout('messageInformation', res.message);
					this.stateTimeout('modalInformation');
				}
			}
		})
		.catch(function (error) {
			this.props.screenProps.setLoading(false);
			this.stateTimeout('messageInformation', error);
			this.stateTimeout('modalInformation');
		});
	}

	stateTimeout(state, value, delay) {
		this.setState({ [state]: value ? value : true });
		setTimeout(() => {
			this.setState({ [state]: value ? '' : false });
		}, delay ? delay : 2500);
	}
	
	Dashboard = () => {
		this.props.navigation.navigate('Dashboard');
	}

	renderLoginButton() {
		if (!this.props.screenProps.loading) {
			return (
				<TouchableOpacity style={styles.button} onPress={this.login}>
					<Text style={styles.buttonText}>Login</Text>
				</TouchableOpacity>
			)
		} else {
		  	return null;
		}
	}

  	render() {
		let imgSource = this.state.showPassword ? passHide : passShow;
		let type = 'error';

		const { modalInformation, messageInformation, saveAccount } = this.state;
		const { params } = this.props.navigation.state;

		if (params) if (params.logout) type = 'warning';

		if (this.props.user_siago !== null) return <ModalIndicator transparent indicators='wave' size={120} indicatorColor={Color.theme} backDropColor='#FFFFFF' />;

		return (
			<ImageBackground source={require('../images/bg_login.jpg')} style={{width: '100%', height: '100%'}}>
				<View style={styles.container}>
					{/* <View style={styles.navTop}>
						<TouchableOpacity >
						<Image style={{width:40, height:40,resizeMode:'contain'}} source={require('../images/anchor.png')}/>
						</TouchableOpacity>
					</View> */}
					<View style={styles.innerLogin}>
						<View style={styles.insideLogo}>
							<Image style={{width: 250, height: 184, resizeMode: 'contain'}} 
							source={require('../images/logotopup.png')}/>
						</View>
						<View style={styles.boxInner}>
							<View style={styles.groupForm}>
							<FloatingLabel
								clearButtonMode='always'
								returnKeyType="next"
								autoCapitalize="none"
								value={this.state.username}
								onChangeText={(username) => this.setState({ username })}
								labelStyle={styles.labelInput}
								inputStyle={styles.inputBox}
								onBlur={this.onBlur}
							>E-mail atau Username</FloatingLabel>
							</View>

							<View style={styles.groupForm}>

							<FloatingLabel
								clearButtonMode='always'
								returnKeyType="done"
								autoCapitalize="none"
								labelStyle={styles.labelInput}
								inputStyle={styles.inputBox}
								onBlur={this.onBlur}
								secureTextEntry = { this.state.showPassword }
								value={this.state.password}
								onChangeText={ (password) => this.setState({ password }) }
							>Kata Sandi</FloatingLabel>

							<TouchableOpacity style={{position: 'absolute', right: 0, bottom: 5, width: 20, height: 20}} onPress={() => this.setState({ showPassword: !this.state.showPassword })}>
								<Image style={{width: 20, height: 20, resizeMode:'contain'}} source={imgSource} />
							</TouchableOpacity>
							
							</View>

						</View>
						{this.renderLoginButton()}
					
						{this.props.screenProps.loading && <Spinner color={Color.theme} />}

						<View style={{flexDirection: 'row', alignItems: 'center'}}>
							<Checkbox onPress={() => this.setState({ saveAccount: !saveAccount })}>
								<ImageProperty source={saveAccount ? checked : checkoff} />
							</Checkbox>
							<Text>Simpan Username dan Password?</Text>
						</View>

						{/* <Text style={styles.versionText}>Lupa Password</Text> */}

						<View style={{flexDirection: 'row', paddingTop: 16}}>
							<Text>Belum punya akun? </Text>
							<Text onPress={() => this.props.navigation.navigate('RegisterScreen')} style={{color: Color.theme}} type='bold'>Register</Text>
						</View>
					</View>
				</View>

				<ModalBanner
					visible={modalInformation}
					type={type}
					label={messageInformation}
					onClose={() => this.setState({ modalInformation: false })}
				/>
			</ImageBackground>
		);
  	}
  	// HEADER STYLE
  	// --------------------------------------
  	static navigationOptions = {
		header: null,
		gesturesEnabled: false
	};
}

// STYLES
// ------------------------------------
const styles = StyleSheet.create(LoginStyle);

const mapStateToProps = (state) => ({
	allState: state,
	user_siago: state['user.auth'].user_siago,
	userSavedAccount: state['user.auth'].userSavedAccount
});

const mapDispatchToProps = (dispatch, props) => ({
	userSiagoLogin: (data) => dispatch({ type: 'USER.SIAGO_LOGIN', data }),
	onUserSavedAccount: (data) => dispatch({ type: 'USER_SAVED_ACCOUNT', data }),
	onLogin: (email, password) => dispatch(login(email, password))
	// onLogout: () => dispatch({ type: 'USER.LOGOUT' })
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);