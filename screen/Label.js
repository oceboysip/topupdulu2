import React from 'react'
import { View, Platform, Text } from 'react-native'
import PropTypes from 'prop-types'
import Styled from 'styled-components'
// import Text from './Text'

const defaultProps = {
    componentName: 'Label',
    color: 'text',
    align: 'flex-start',
    size: 12
}

const propTypes = {
    children: PropTypes.string
}

const BaseText = Styled(Text)`
    color: #A8A699
    fontSize: 12
`;

const LabelWrapper = Styled(View)`
    flexDirection: column;
    ${props => props.align && `
        justifyContent: ${props.align};
    `
    };
`

const Label = props => {
  const { children, size, color, align, onPress, ...style } = props;

    return (
        <LabelWrapper {...style} align={align} >
            <BaseText size={size} color={color} onPress={onPress}>{ children }</BaseText>
        </LabelWrapper>
    )
}

Label.defaultProps = defaultProps;

Label.propTypes = propTypes;

export default Label;
