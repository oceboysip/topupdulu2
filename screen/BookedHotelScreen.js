import React, { Component } from "react";
import {Alert,Platform, StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions, Button,Switch,TouchableWithoutFeedback} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Dialog, {SlideAnimation, DialogContent } from 'react-native-popup-dialog';
import BottomSheet from 'reanimated-bottom-sheet';
import SlidingUpPanel from 'rn-sliding-up-panel';
import BookedTrainStyle from '../style/BookedHotelStyle';

import ToggleSwitch from "toggle-switch-react-native";

export default class BookedHotelScreen extends Component<{}> {

  constructor(props){
  super(props)
    this.state = {
      isOnDefaultToggleSwitch: false,
      isOnLargeToggleSwitch: false,
      isOnBlueToggleSwitch: false,
      isHidden: false
    };
  }

  onToggle(isOn) {
    console.log("Changed to " + isOn);
  }
  Dashboard = () => {
    this.props.navigation.navigate('Dashboard');
  }
  CalendarCheckin = () => {
    this.props.navigation.navigate('CalendarCheckin');
  }
  SearchHotel = () => {
    this.props.navigation.navigate('SearchHotel');
  }
  ReportHotel = () => {
    this.props.navigation.navigate('ReportHotel');
  }

  HasilSearchHotel=()=>{
    this.props.navigation.navigate('HasilSearchHotel');
  }

  renderInner = () => (
    <View style={styles.panel}>
      <View style={styles.rowPanel}>
        <Image style={{width:70, height:12,resizeMode:'contain',marginBottom:10}} source={require('../images/block_panel.png')} />
        <Text style={styles.BigText2}>Atur Tamu dan Kamar</Text>
      </View>
      <View style={styles.rowPanelDirect}>
        <View style={styles.flexRow}>
          <Text style={styles.greyBig}>Tamu Dewasa</Text>
          <Text style={styles.greySmall}>Jumlah Tamu</Text>
        </View>
        <View style={styles.flexRow}>
          <Text style={styles.BigText2}>1</Text>
        </View>
        <View style={styles.flexRow2}>
          <TouchableOpacity> 
            <Text style={styles.plusMinusbt}>-</Text> 
          </TouchableOpacity>

          <TouchableOpacity> 
            <Text style={styles.plusMinusbt}>+</Text> 
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.rowPanelDirect}>  
        <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}}>
        </View>
      </View>

      <View style={styles.rowPanelDirect}>
        <View style={styles.flexRow}>
          <Text style={styles.greyBig}>Tamu Anak</Text>
          <Text style={styles.greySmall}>Jumlah Tamu</Text>
        </View>
        <View style={styles.flexRow}>
          <Text style={styles.BigText2}>1</Text>
        </View>
        <View style={styles.flexRow2}>
          <TouchableOpacity> 
            <Text style={styles.plusMinusbt}>-</Text> 
          </TouchableOpacity>

          <TouchableOpacity> 
            <Text style={styles.plusMinusbt}>+</Text> 
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.rowPanelDirect}>  
        <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}}>
        </View>
      </View>

      <View style={styles.rowPanelDirect}>
        <View style={styles.flexRow}>
          <Text style={styles.greyBig}>Kamar</Text>
          <Text style={styles.greySmall}>Jumlah Kamar</Text>
        </View>
        <View style={styles.flexRow}>
          <Text style={styles.BigText2}>0</Text>
        </View>
        <View style={styles.flexRow2}>
          <TouchableOpacity> 
            <Text style={styles.plusMinusbt}>-</Text> 
          </TouchableOpacity>

          <TouchableOpacity> 
            <Text style={styles.plusMinusbt}>+</Text> 
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.rowPanelDirect}>
        <TouchableOpacity onPress={() => this.bs.current.snapTo(3)} style={styles.buttonYellow}>
        <Text style={styles.whiteText}>Batal</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.bs.current.snapTo(3)} style={styles.buttonGreen}>
        <Text style={styles.whiteText}>Selesai</Text>
        </TouchableOpacity>
      </View>

    </View>
  )

  bs = React.createRef()


  render() {
    return (
      <View style={styles.container}>
      

        <View style={styles.navTop}>
          <View style={styles.rowsBeetwen}>
            <View style={{flexDirection: 'row',alignItems:'center'}}>
              <TouchableOpacity onPress={this.Dashboard}>
                <Image style={{width:30, height:30,resizeMode:'contain'}} source={require('../images/left-arrow.png')}/>
              </TouchableOpacity>
              <Text style={styles.titlePage}>Cari Hotel</Text>
            </View>

            <View style={{flexDirection:'row'}}>
              <TouchableOpacity onPress={this.ReportHotel}>
                <Image style={{width:30, height:30, resizeMode:'contain'}}  source={require('../images/icon_paperWhite.png')}/>
               </TouchableOpacity>
            </View>
          </View>

        </View>
   

        
        <ScrollView>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRows}>
                  <View style={styles.iconForm}>
                    <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../images/icon_place.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.smallText}>Tujuan</Text>
                    <TouchableOpacity onPress={this.SearchHotel}>
                      <Text style={styles.BigText}> BANDUNG</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={styles.rows2}>
                  <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}}>
                  </View>
                </View>

                <View style={styles.flexRowsLastSpace}>
                  <View style={{flex:1, flexDirection:'row'}}>
                    <View style={styles.iconForm}>
                      <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../images/icon_date.png')}/>
                    </View>
                    <View style={styles.captionForm}>
                      <Text style={styles.smallText}>Tanggal Check-in</Text>
                      <TouchableOpacity onPress={this.CalendarCheckin}>
                        <Text style={styles.BigText}>Fri, 27 Sep 2019</Text>
                        <Text style={styles.VerySmallText}>Check-out sat, 28 Sep 2019</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={{flex:1, alignItems:"flex-end"}}>
                    <Text style={styles.smallText}>Durasi</Text>
                    <Text style={styles.bgGreenText}>1 Malam</Text>
                  </View>
                </View>



              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <View style={styles.iconForm}>
                      <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../images/icon_key.png')}/>
                    </View>
                    <View style={styles.captionForm}>
                      <Text style={styles.smallText}>Total Tamu dan Kamar</Text>
                      <TouchableOpacity onPress={() => this.bs.current.snapTo(0)}>
                        <Text style={styles.BigText}>1 Tamu, 1 Kamar</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>

                <View style={styles.rows2}>
                  <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}}>
                  </View>
                </View>

                <View style={styles.flexRowsLast}>
                  <View style={styles.iconForm}>
                    <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../images/icon_bed.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.smallText}>Tipe Kamar</Text>
                    <TouchableOpacity>
                      <Text style={styles.BigText}>Twin Bed</Text>
                    </TouchableOpacity>
                  </View>
                </View>

              </View>
            </View>
          </View>
          <View style={styles.alCenter}>
            <TouchableOpacity style={styles.button} onPress={this.HasilSearchHotel}>
              <Text style={styles.buttonText}>CARI</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>

        <BottomSheet
            ref={this.bs}
            snapPoints={[350, 200, 100, 0]}
            renderContent={this.renderInner}
            initialSnap={3}
            enabledGestureInteraction
          />
        
      </View>
    )
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  }
}




// STYLES
// ------------------------------------
const styles = StyleSheet.create(BookedHotelStyle);