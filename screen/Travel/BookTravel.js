import React, { Component } from 'react';
import { View, Image, ScrollView, Platform } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Styled from 'styled-components';
import Moment from 'moment';

import Text from '../Text';
import Color from '../Color';
import Header from '../Header';
import TouchableOpacity from '../Button/TouchableDebounce';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    flexDirection: column;
    backgroundColor: #FFFFFF;
`;

const MainContentView = Styled(View)`
    flex: 1;
`;

const AirlineView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    marginVertical: 6;
`;

const AirlineImageView = Styled(View)`
    width: 20;
    height: 20;
    marginRight: 6;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const MainInfoView = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: #FFFFFF;
    marginTop: 20;
    padding: 10px 14px 10px 14px;
    borderRadius: 3;
    elevation: 5;
      borderWidth: ${Platform.OS === 'ios' ? 1 : 0 };
    borderBottomWidth: ${Platform.OS === 'ios' ? 2 : 0 };
    borderColor: #DDDDDD;
`;

const CustomButton = Styled(TouchableOpacity)`
    width: 168;
    height: 45;
    borderRadius: 30;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
    paddingHorizontal: 16;
    backgroundColor: #231F20;
`;

const NormalText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const TitleInfoText = Styled(Text)`
    textAlign: left;
    marginBottom: 6;
`;

const DetailInfoText = Styled(NormalText)`
    textAlign: left;
    marginBottom: 6;
`;

const ButtonLabel = Styled(Text)`
    textAlign: left;
    color: #FFFFFF;
`;

const AntDesignIcon = Styled(AntDesign)`
    fontSize: 15;
    color: #FFFFFF;
`;

export default class BookTravel extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
  }

  renderAirlines(airlines) {
    return airlines.map((airline, i) =>
      <AirlineView key={i}>
        <AirlineImageView><ImageProperty resizeMode='contain' source={{ uri: airline.logoUrl }} /></AirlineImageView>
        <DetailInfoText type='medium'>{airline.name}</DetailInfoText>
      </AirlineView>
    );
  }

  renderMainInfoView() {
    const { tourDetail, departureId, tourId } = this.props.navigation.state.params;
    const { name, departure } = tourDetail;
    const { date, duration, airlines } = departure;
    const departureDate = Moment(date);
    const returnDate = Moment(date).add(duration - 1, 'days');

    return (
      <MainInfoView>
        <TitleInfoText type='bold'>{duration}D {name}</TitleInfoText>
        <DetailInfoText type='medium'>Tanggal berangkat : {departureDate.format('DD MMM YYYY')} - {returnDate.format('DD MMM YYYY')} </DetailInfoText>
        <DetailInfoText type='medium'>Durasi : {duration} Hari</DetailInfoText>
        {this.renderAirlines(airlines)}
        <CustomButton onPress={() => this.props.navigation.navigate('BookRoom', { tourDetail, departureId, tourId })}>
          <AntDesignIcon name='pluscircleo' />
          <ButtonLabel type='semibold'>Jumlah Peserta</ButtonLabel>
        </CustomButton>
      </MainInfoView>
    );
  }

  render() {
    return (
      <MainView>
        <Header title='Pesan Travel' />
        <MainContentView>
          <ScrollView contentContainerStyle={{ paddingHorizontal: 16, paddingBottom: 20 }}>
            {this.renderMainInfoView()}
          </ScrollView>
        </MainContentView>
      </MainView>
    );
  }

}
