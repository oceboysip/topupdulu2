import React, { Component } from 'react';
import { View, Image } from 'react-native';
import Styled from 'styled-components';

import Text from '../Text';
import FormatMoney from '../FormatMoney';
import TouchableOpacity from '../Button/TouchableDebounce';

const MainView = Styled(TouchableOpacity)`
    width: 50%;
    aspectRatio: 1;
`;

const ImageContainer = Styled(View)`
    width: 100%;
    aspectRatio: 1.7;
    marginBottom: 8;
`;

const LabelContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    marginBottom: 8;
`;

const PriceContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    justifyContent: flex-end;
`;

const ImageProperty = Styled(Image)`
    width: 98%;
    height: 98%;
`;

const NormalText = Styled(Text)`
    fontSize: 12;
    color: #333333;
    textAlign: left;
`;

const SmallText = Styled(NormalText)`
    fontSize: 10;
    marginTop: 8;
`;

const GreyText = Styled(Text)`
    fontSize: 10;
    color: #999999;
    textAlign: left;
`;

const PriceText = Styled(Text)`
    fontSize: 12;
    color: #FF425E;
    textAlign: left;
`;

// listView
const ListView = Styled(TouchableOpacity)`
  flexDirection: row;
  marginBottom: 8;
  borderRadius: 3;
  borderWidth: 0.5;
  borderColor: #DDDDDD;
`;

const imgLocation = require('../../images/location.png');

export default class FirstHighlighted extends Component {

  getMinimumDuration(departures) {
    let duration = departures[0].duration;
    for (const departure of departures) if (departure.duration < duration) duration = departure.duration;
    return duration;
  }

  render() {
    const { data, view, ...style } = this.props;
    const { pictures, name, startingPrice, departures } = data;

    if(view === 'listView') {
      return (
        <ListView onPress={() => this.props.onPress()} activeOpacity={0.5}>
          <Image style={{width: '27%', aspectRatio: 1}} source={{ uri: pictures ? pictures[0].thumbnailUrl : data.AttractionsProductsDetail.AttractionConfig[1].value + data.AttractionsProductsDetail.photos[0].path }} />
          <View style={{paddingLeft: 8, paddingTop: 8, width: '73%'}}>
            {departures && <NormalText type='bold'>{this.getMinimumDuration(departures)}D {name || data.title}</NormalText>}
            {data.AttractionsProductsDetail && <NormalText type='bold'>{data.title}</NormalText>}
            {data.AttractionsProductsDetail && <SmallText><Image source={imgLocation} style={{width: 5.73, height: 7}}/> {data.AttractionsProductsDetail.locations[0].city}, {data.AttractionsProductsDetail.locations[0].country}</SmallText>}
            <View style={{marginTop: 6}}>
              <GreyText type='medium'>From <PriceText type='bold'>{FormatMoney.getFormattedMoney(startingPrice ? startingPrice.finalPrice : data.AttractionsProductsDetail.baseprice)}</PriceText></GreyText>
            </View>
          </View>
        </ListView>
      )
    }else {
      return (
        <MainView activeOpacity={0.5} {...style}>
          <ImageContainer>
            <ImageProperty source={{ uri: pictures ? pictures[0].thumbnailUrl : data.AttractionsProductsDetail.AttractionConfig[1].value + data.AttractionsProductsDetail.photos[0].path }} />
          </ImageContainer>

          <LabelContainer>
            {departures && <NormalText type='bold'>{this.getMinimumDuration(departures)}D {name || data.title}</NormalText>}
            {data.AttractionsProductsDetail && <View>
              <NormalText type='bold'>{data.title}</NormalText>
              <SmallText lineHeight={2}><Image source={imgLocation} style={{width: 5.73, height: 7}}/> {data.AttractionsProductsDetail.locations[0].city}, {data.AttractionsProductsDetail.locations[0].country}</SmallText>
            </View>}
          </LabelContainer>

          <PriceContainer>
            <GreyText type='medium'>From <PriceText type='bold'>{FormatMoney.getFormattedMoney(startingPrice ? startingPrice.finalPrice : data.AttractionsProductsDetail.baseprice)}</PriceText></GreyText>
          </PriceContainer>
        </MainView>
      )
    }
  }
}
