import React, { Component } from 'react';
import { View, ScrollView, ActivityIndicator, Modal } from 'react-native';
import Styled from 'styled-components';
import { connect } from 'react-redux';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import gql from 'graphql-tag';

import graphClient from '../../state/apollo';
import Color from '../Color';
import Header from '../Header';
import CardTravel from './CardItemTravel';
import ModalTourSearch from '../Modal/TravelSearch';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    backgroundColor: #FAFAFA;
    flexDirection: column;
`;

const CustomScrollView = Styled(ScrollView)`
    width: 100%;
    height: 100%;
`;

const ResultView = Styled(View)`
    width: 100%;
    minHeight: 1
    padding: 14px 10px 0px 10px;
    flexDirection: column;
`;

const MiniLoadingActivityView = Styled(View)`
    width: 100%;
    flex: 1;
    justifyContent: center;
    alignItems: center;
`;

const CardItemTravel = Styled(CardTravel)`
    marginBottom: 12;
`;

const SearchIcon = Styled(EvilIcons)`
    color: #FFFFFF;
    fontSize: 32;
`;

const getTourAvailabilityQuery = gql`
  query(
    $groupSlugIn: [String!]
  ) {
   tourAvailability(
    groupSlugIn: $groupSlugIn
   ) {
      id name slug
      startingPrice { price discount finalPrice }
      departures {
        id date duration quota reserved
        startingPrice { price discount finalPrice }
        airlines { logoUrl name }
      }
      pictures { url thumbnailUrl }
      countries{
        code
        name
      }
      tags {
        id name
        items { id name }
      }
   }
 }
`;

class RecommendedTravel extends Component {

  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    this.state = {
      title: null,
      modalTourSearch: false,
      tours: [],
      loading: false,
    };
  }

  componentDidMount() {
    const { recommendedTours, navigation } = this.props;
    const { data } = navigation.state.params;
    if (Object.prototype.toString.call(data) === '[object Object]') this.getTourAvailability(data);
    else this.setState({ tours: recommendedTours.result, title: 'GRUP TRAVEL' });
  }

  openModal(modal) {
    this.setState({ [modal]: true });
  }

  closeModal(modal) {
    this.setState({ [modal]: false });
  }

  getTourAvailability(destination) {
    this.setState({ title: destination.name, loading: true }, () => {
      const variables = { groupSlugIn: destination.slug };

      graphClient
        .query({
          query: getTourAvailabilityQuery,
          variables
        })
        .then(res => {
          if (res.data.tourAvailability) {
            this.setState({ tours: res.data.tourAvailability, loading: false });
          }
        }).catch(reject => {
            this.setState({ tours: [], loading: false });
        });
    });
  }

  renderMiniLoadingIndicator() {
    return (
      <MiniLoadingActivityView>
        <ActivityIndicator size='large' color={Color.loading} />
      </MiniLoadingActivityView>
    );
  }

  renderRecommendedTour(tours) {
    return tours.map((tour, i) =>
      <CardItemTravel key={i} data={tour} />
    );
  }

  render() {
    const { modalTourSearch, tours, title, loading } = this.state;

    return (
      <MainView>
        <Header
          title={title}
          showLeftButton
          iconRightButton={<SearchIcon name='search' />}
          onPressRightButton={() => { if (tours.length > 0) this.openModal('modalTourSearch'); }}
        />
        {tours.length > 0 && <CustomScrollView>
          <ResultView>{this.renderRecommendedTour(tours)}</ResultView>
        </CustomScrollView>}

        {loading && this.renderMiniLoadingIndicator()}

        <Modal
          visible={modalTourSearch}
          animationType='fade'
          onRequestClose={() => this.closeModal('modalTourSearch')}
        >
          <ModalTourSearch onClose={() => this.closeModal('modalTourSearch')} tours={tours} />
        </Modal>
      </MainView>
    );
  }
}

const mapStateToProps = (state) => ({
  recommendedTours: state.recommendedTours,
});

export default connect(mapStateToProps)(RecommendedTravel);
