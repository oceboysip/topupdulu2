import React, { Component } from 'react';
import { View, Modal, ScrollView, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import Styled from 'styled-components';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import gql from 'graphql-tag';

// import HighlightedScreen from '../../components/HighlightedScreen';
import graphClient from '../../state/apollo';
import TravelSearch from '../Modal/TravelSearch';
import FirstHighlightedCard from './FirstHighlighted';
import SecondHighlightedCard from './SecondHighlighted';
import Header from '../Header';
import Text from '../Text';
import Color from '../Color';
import TouchableOpacity from '../Button/TouchableDebounce';
import CardItemTravel from '../Travel/CardItemTravel';

const MainView = Styled(View)`
  flex: 1;
`;

const HeaderContent = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: ${Color.theme};
    paddingHorizontal: 16;
`;

const DestinationSearchView = Styled(TouchableOpacity)`
    width: 100%;
    height: 50;
    flexDirection: row;
    alignItems: center;
    backgroundColor: #FFFFFF;
    paddingHorizontal: 6;
    marginBottom: 16;
`;

const SearchIconContainer = Styled(View)`
    minWidth: 20;
    minHeight: 20;
    alignItems: center;
    justifyContent: center;
    margin: 10px 4px 10px 0px;
`;

const SearchIcon = Styled(EvilIcons)`
    color: #A8A699;
    fontSize: 32;
`;

const SearchText = Styled(Text)`
    fontSize: 13.7;
    color: ${Color.placeholder};
    textAlign: left;
`;

const SubMenuContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    marginTop: 16;
    alignItems: center;
`;

const SubHeaderMenuContainer = Styled(View)`
    width: 50%;
    minHeight: 1;
    flexDirection: row;
    justifyContent: center;
    paddingHorizontal: 16;
    marginBottom: 16;
    paddingVertical: 4;
    backgroundColor: ${Color.primary};
`;

const LargerBlackText = Styled(Text)`
    fontSize: 14;
    letterSpacing: 1;
`;

const SubMenu = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    flexWrap: wrap;
    paddingLeft: 4;
`;

const CardTravel = Styled(CardItemTravel)`
    marginBottom: 12;
`;

const getTourAvailabilityQuery = gql`
  query(
    $query: String
  ) {
   tourAvailability(
    query: $query
   ) {
      id name slug
      startingPrice { price discount finalPrice }
      departures {
        id date duration quota reserved
        startingPrice { price discount finalPrice }
        airlines { logoUrl name }
      }
      pictures { url thumbnailUrl }
      countries{
        code
        name
      }
      tags {
        id name
        items { id name }
      }
   }
 }
`;

const pesanan = require('../../images/icon_paperWhite.png');

class TravelScreen extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: [],
      modalSearch: false
    }
  }

  componentDidMount() {
    this.getUmrohTravel();
  }

  getUmrohTravel() {
    graphClient
    .query({
      query: getTourAvailabilityQuery,
      variables: { query: 'umroh' }
    })
    .then(res => {
      this.setState({ data: res.data.tourAvailability, loading: false });
    }).catch(reject => {
      console.log(reject);
      this.setState({ data: [], loading: false });
    });
  }

  openModal(modal) {
    this.setState({ [modal]: true });
  }

  closeModal(modal) {
    this.setState({ [modal]: false });
  }

  openOrderBooking() {
    this.props.navigation.navigate('OrderBooking', {
      title: 'Travel'
    })
  }

  renderMiniLoading() {
    return (
      <View style={{width: '100%', height: 120, justifyContent: 'center'}}>
        <ActivityIndicator size='large' color={Color.theme} />
      </View>
    );
  }

  renderFirstHighlighted(firstHighlightedData) {
    return firstHighlightedData.map((data, i) =>
      <FirstHighlightedCard
        key={i}
        data={data}
        style={{marginBottom: 16}}
        onPress={() => this.props.navigation.navigate('SelectTravelDeparture', { data })}
      />
    );
  }

  renderSecondHighlighted(secondHighlightedData) {
    return secondHighlightedData.map((data, i) =>
      <SecondHighlightedCard
        key={i}
        data={data}
        onPress={() => this.props.navigation.navigate('RecommendedTravel', { data, 'user': this.props.user })}
      />
    );
  }

  renderUmroh(data) {
    if (data.length === 0) {
      return (
        <View style={{width: '100%', justifyContent: 'center', paddingVertical: 32}}>
          <Text>Umroh tidak tersedia</Text>
        </View>
      )
    }

    return data.map((item, idx) =>
      <CardTravel
        key={idx}
        data={item}
        widthImage='36%'
        onPress={() => this.props.navigation.navigate('SelectTravelDeparture', { data: item }) }
      />
    )
  }

  render() {
    const { recommendedTours, popularDestinations } = this.props;
    const { data, loading, modalSearch } = this.state;

    console.log(this.state);

    return (
      <MainView>
        <Header title='Umroh & Travel' imageRightButton={pesanan} onPressRightButton={() => this.openOrderBooking()} />
        <ScrollView>
          <HeaderContent>
            <DestinationSearchView onPress={() => this.openModal('modalSearch')}>
              <SearchIconContainer><SearchIcon name='search' /></SearchIconContainer>
              <SearchText>Cari Umroh atau Travel</SearchText>
            </DestinationSearchView>
          </HeaderContent>

          <SubMenuContainer>
            <SubHeaderMenuContainer>
              <LargerBlackText type='bold'>UMROH</LargerBlackText>
            </SubHeaderMenuContainer>

            <SubMenu>
              {loading ? this.renderMiniLoading() : this.renderUmroh(data)}
            </SubMenu>
          </SubMenuContainer>
          
          {recommendedTours.result && <SubMenuContainer>
            <SubHeaderMenuContainer>
              <LargerBlackText type='bold'>TOP TRAVEL</LargerBlackText>
            </SubHeaderMenuContainer>

            <SubMenu>
              {this.renderFirstHighlighted(recommendedTours.result)}
            </SubMenu>
          </SubMenuContainer>}

          {popularDestinations.result && <SubMenuContainer>
            <SubHeaderMenuContainer>
              <LargerBlackText type='bold'>GROUP TRAVEL</LargerBlackText>
            </SubHeaderMenuContainer>

            <SubMenu style={{justifyContent: 'space-between'}}>
              {this.renderSecondHighlighted(popularDestinations.result)}
            </SubMenu>
          </SubMenuContainer>}
        </ScrollView>

        <Modal
          visible={modalSearch}
          animationType='fade'
          onRequestClose={() => this.closeModal('modalSearch')}
        >
          <TravelSearch onClose={() => this.closeModal('modalSearch')} />
        </Modal>
      </MainView>
    )

    // return (
    //   <HighlightedScreen
    //     title='Tidak ada kata terlambat untuk berlibur!'
    //     textInputPlaceholder='Ketik destinasi Anda'
    //     firstHighlightedLabel='DESTINASI PILIHAN VESTA'
    //     firstHighlightedNavigate='RecommendedTours'
    //     firstHighlightedData={recommendedTours.result}
    //     firstHighlightedOnPress='SelectTourDeparture'
    //     secondHighlightedLabel='DESTINASI POPULER'
    //     secondHighlightedNavigate='PopularDestinations'
    //     secondHighlightedData={popularDestinations.result}
    //     secondHighlightedOnPress='RecommendedTours'
    //     FirstHighlightedCard={FirstHighlightedCard}
    //     SecondHighlightedCard={SecondHighlightedCard}
    //     CustomModal={ModalTourSearch}
    //   />
    // );
  }
}

const mapStateToProps = (state) => {
  return {
    recommendedTours: state.recommendedTours,
    popularDestinations: state.popularDestinations,
  };
};

const mapDispatchToProps = null;

export default connect(mapStateToProps, mapDispatchToProps)(TravelScreen);
