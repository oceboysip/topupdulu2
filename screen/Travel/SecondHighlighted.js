import React, { Component } from 'react';
import { View, Image } from 'react-native';
import Styled from 'styled-components';

import Text from '../Text';
import Color from '../Color';
import TouchableOpacity from '../Button/TouchableDebounce';

const MainView = Styled(TouchableOpacity)`
    width: 50%;
    aspectRatio: 1;
`;

const GradientView = Styled(View)`
    width: 100%;
    height: 50%;
    position: absolute;
    bottom: 0;
    justifyContent: flex-end;
`;

const LabelContainer = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    alignItems: center;
    justifyContent: flex-start;
    position: absolute;
    flexDirection: row;
    top: 42;
    paddingHorizontal: 5;
    flexWrap: wrap;
`;

const TotalContainer = Styled(View)`
    minWidth: 1;
    minHeight: 12;
    backgroundColor: ${Color.theme};
    alignItems: center;
    justifyContent: center;
    paddingHorizontal: 4px;
    borderRadius: 30;
`;

const ImageProperty = Styled(Image)`
    width: 98%;
    height: 98%;
    marginBottom: 2%;
`;

const SmallerNormalText = Styled(Text)`
    fontSize: 10;
    textAlign: left;
`;

const WhiteText = Styled(Text)`
    color: #FFFFFF;
    textAlign: left;
    marginRight: 3;
    lineHeight: 16;
`;

const transparent = require('../../images/gradient-transparent.png');

export default class SecondHighlighted extends Component {

    renderAllWords(string) {
      return string.split(' ').map((str, i) =>
        <WhiteText type='bold' key={i}>{str}</WhiteText>
      );
    }

    render() {
      const { data, ...style } = this.props;
      const { name, pictureUrl } = data;

      return (
        <MainView activeOpacity={1} {...style}>
          <ImageProperty source={{ uri: pictureUrl }} />
          <GradientView>
            <ImageProperty source={transparent} />
            <LabelContainer>
              {this.renderAllWords(name)}
              {false && <TotalContainer><SmallerNormalText type='semibold'>15</SmallerNormalText></TotalContainer>}
            </LabelContainer>
          </GradientView>
        </MainView>
      );
    }
}
