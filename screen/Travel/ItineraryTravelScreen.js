import React, { Component } from 'react';
import { View, Image, ScrollView, ActivityIndicator, Platform, Animated } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';
import { Tabs, Tab, TabHeading } from 'native-base';
import gql from 'graphql-tag';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { Row, Col } from '../Grid';
import graphClient from '../../state/apollo';
import Text from '../Text';
import Color from '../Color';
import Button from '../Button';
import TouchableOpacity from '../Button/TouchableDebounce';
import FormatMoney from '../FormatMoney';
import Header from '../Header';

import Overview from './ItineraryTabs/Overview';
import Itinerary from './ItineraryTabs/Itinerary';
import TravelPrice from './ItineraryTabs/TravelPrice';
import TermsConditions from './ItineraryTabs/TermsConditions';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    backgroundColor: #FFFFFF;
`;

const UpperMainView = Styled(View)`
    flex: 1;
    width: 100%;
`;

const BottomMainView = Styled(View)`
    width: 100%;
    height: 63;
    alignItems: center;
    justifyContent: space-between;
    flexDirection: row;
    padding: 10px 10px 10px 10px;
    backgroundColor: #FFFFFF;
    borderWidth: 0;
    elevation: 5;
    bottom: 0;
`;

const HalfBottomMainView = Styled(View)`
    minWidth: 1;
    height: 100%;
`;

const SubMainView = Styled(View)`
    width: 100%;
    height: 100%;
    paddingTop: 150;
    flexDirection: column;
`;

const RowView = Styled(Row)`
    height: 100%;
`;

const ColumnView = Styled(Col)`
    justifyContent: center;
    alignItems: center;
`;

const SideButton = Styled(TouchableOpacity)`
    flex: 1;
    width: 100%;
    alignItems: center;
    justifyContent: center;
`;

const SideIcon = Styled(Icon)`
    fontSize: 23;
    color: ${Color.text};
`;

const CustomHeader = Styled(Header)`
    height: 60;
`;

const AbsoluteCustomHeader = Styled(Animated.View)`
    width: 100%;
    height: 60;
    alignItems: center;
    flexDirection: row;
    zIndex: 1;
    position: absolute;
`;

const BackgroundImageView = Styled(View)`
    width: 100%;
    height: 260;
    position: absolute;
`;

const MainInfoContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: transparent;
    paddingHorizontal: 16;
    marginBottom: 16;

`;

const MainInfoView = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: #FFFFFF;
    padding: 12px 12px 16px 12px;
    borderRadius: 3;
    elevation: 5;
    flexDirection: column;
        borderWidth: ${Platform.OS === 'ios' ? 1 : 0 };
    borderBottomWidth: ${Platform.OS === 'ios' ? 2 : 0 };
    borderColor: #DDDDDD;
`;

const TitleInfoView = Styled(View)`
    width: 100%;
    minHeight: 1;
    marginBottom: 10;
`;

const DateInfoView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
`;

const LeftDateInfoView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    padding: 4px 8px 4px 8px;
    borderRadius: 50;
    backgroundColor: ${Color.theme};
`;

const RightDateInfoView = Styled(TouchableOpacity)`
    minWidth: 1;
    minHeight: 1;
    padding: 4px 8px 4px 8px;
    borderRadius: 50;
    backgroundColor: #000000;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
`;

const CalendarView = Styled(View)`
    width: 9;
    height: 8;
    marginRight: 4;
`;

const LoadingActivityView = Styled(View)`
    width: 100%;
    flex: 1;
    justifyContent: center;
    alignItems: center;
    backgroundColor: #FFFFFF;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const TitleText = Styled(Text)`
    textAlign: left;
`;

const AnimatedTitleText = Styled(Animated.Text)`
    fontSize: 14;
    textAlign: center;
    textShadowRadius: 0;
`;

const DateText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const ChangeDateText = Styled(Text)`
    fontSize: 12;
    color: #FFFFFF;
    textAlign: left;
`;

const BottomText = Styled(Text)`
    textAlign: left;
`;

const PriceText = Styled(Text)`
    textAlign: left;
    color: #FF425E;
    textAlign: left;
`;

const HeaderTabsText = Styled(Text)`
    fontSize: 12;
`;

const BookingButton = Styled(Button)`
    width: 130;
    height: 45;
`;

const calendar = require('../../images/calendar.png');

const getTourDetailQuery = gql`
    query(
      $tourSlug: String!
      $departureId: Int!
    ) {
      tour (
        tourSlug: $tourSlug
      ) {
        name description
        pictures { url thumbnailUrl }
        startingPrice { finalPrice }
        countries { code name }
        visas { managed country { name code }}
        departure (
          departureId: $departureId
        ) {
          date duration quota reserved
          itineraries {
            title dayNumber description
            meal { dinner lunch breakfast }
            flights {
              departsAt arrivesAt
              airline { logoUrl code name }
              origin { name code cityName utcOffset }
              destination { name code cityName utcOffset }
            }
            places {
              id name
              geoPoint { latitude longitude }
            }
            pictures { url }
          }
          price {
            airportTaxAndFuelPrice visaPrice vatPercent downPaymentPrice
            adultTwinSharingPrice { price discount finalPrice }
            adultSinglePrice { price discount finalPrice }
            childTwinSharingPrice { price discount finalPrice }
            childExtraBedPrice { price discount finalPrice }
            childNoBedPrice { price discount finalPrice }
          }
          airlines { name code logoUrl }
        }
      }
    }
`;

export default class ItineraryTravelScreen extends Component {

  static navigationOptions = { header: null };
  animatedValue;

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      tourDetail: null,
      activeTab: 0,
      duration: null,
      departureDate: null,
      returnDate: null,
      tabs: null,
    };
  }

  componentDidMount() {
    this.getTourDetail();
    this.setAnimatedHeader();
  }

  setAnimatedHeader() {
    this.animatedValue = new Animated.Value(0);
    this.props.navigation.setParams({
      animatedValue: this.animatedValue.interpolate({
        inputRange: [0, 50],
        outputRange: ['transparent', Color.theme],
        extrapolate: 'clamp'
      }),
      animatedText: this.animatedValue.interpolate({
        inputRange: [0, 50],
        outputRange: ['transparent', Color.text],
        extrapolate: 'clamp'
      })
    });
  }

  getTourDetail() {
    const { departureId, tourSlug } = this.props.navigation.state.params;
    const variables = { departureId, tourSlug };

    graphClient
      .query({
        query: getTourDetailQuery,
        variables
      })
      .then(res => {
        if (res.data.tour) {
          const tourDetail = res.data.tour;
          const { description, departure, visas, countries } = tourDetail;
          const { duration, itineraries, quota, reserved, date, price, airlines } = departure;
          const departureDate = Moment(date).format('DD MMM YYYY');
          const returnDate = Moment(date).add(duration - 1, 'days').format('DD MMM YYYY');
          const availableSeats = quota - reserved;
          const customItineraries = this.getItineraries(departure);
          const airlinesString = this.getAirlines(airlines);
          const tabs = [
            { title: 'Ringkasan', children: <Overview data={{ description, visas, countries, duration, itineraries, airlinesString, availableSeats }} /> },
            { title: 'Itinerary', children: <Itinerary data={{ itineraries: customItineraries, date }} /> },
            { title: 'Harga', children: <TravelPrice data={{ tourPrice: price, visas }} /> },
            { title: 'Ketentuan', children: <TermsConditions /> },
          ];
          this.setState({ tourDetail, duration, departureDate, returnDate, tabs, loading: false });
        }
      }).catch(reject => {
        this.setState({ loading: false });
      });
  }

  onlyUnique(array) {
    return Array.from(new Set(array));
  }

  getItineraries(departure) {
    const itineraries = [];
    for (const itinerary of departure.itineraries) {
      const { dayNumber, title, meal, flights, places, pictures, description } = itinerary;
      const mealString = this.getMeal(meal);
      itineraries.push({ dayNumber, title, mealString, flights, places, pictures, description, coordinates: this.getCoordinates(places) });
    }
    return itineraries;
  }

  getCoordinates(places) {
    const coordinates = [];
    for (const place of places) {
      const { name, geoPoint } = place;
      let { latitude, longitude } = geoPoint;
      if (latitude && longitude) {
        latitude = parseFloat(latitude);
        longitude = parseFloat(longitude);
        coordinates.push({ name, latitude, longitude });
      }
    }
    return coordinates;
  }

  getMeal(meal) {
    let mealString = '';
    if (meal.breakfast) mealString += 'Pagi';
    if (meal.lunch) {
      if (mealString) mealString += ', ';
      mealString += 'Siang';
    }
    if (meal.dinner) {
      if (mealString) mealString += ', ';
      mealString += 'Malam';
    }
    return mealString;
  }

  getAirlines(paramAirlines) {
    const airlines = this.onlyUnique(paramAirlines);
    if (airlines.length === 1) return `All ${airlines[0].name}`;
    let airlinesString = '';
    airlines.forEach((airline, i) => {
      if (i !== 0) airlinesString += ', ';
      airlinesString += airline.name;
    });
    return airlinesString;
  }

  renderLoadingIndicator() {
    return (
      <LoadingActivityView>
        <ActivityIndicator size='large' color={Color.loading} />
      </LoadingActivityView>
    );
  }

  renderBackgroundImage(tourDetail) {
    return (
      <BackgroundImageView>
        <ImageProperty source={{ uri: tourDetail.pictures[0].url }} />
      </BackgroundImageView>
    );
  }

  renderTabHeading(tab, i, activeTab) {
    return (
      <TabHeading style={{ backgroundColor: '#F4F4F4' }}>
        <HeaderTabsText type={activeTab !== i ? 'medium' : 'bold'}>{tab.title}</HeaderTabsText>
      </TabHeading>
    );
  }

  renderAllTabs() {
    const { activeTab, tabs } = this.state;
    return tabs.map((tab, i) =>
      <Tab
        key={i}
        heading={this.renderTabHeading(tab, i, activeTab)}
        tabStyle={{ backgroundColor: '#F4F4F4', elevation: 0 }}
        activeTabStyle={{ backgroundColor: '#F4F4F4' }}
      >
        {i === activeTab && tab.children}
      </Tab>
    );
  }

  renderMainTabs() {
    return (
      <Tabs
        prerenderingSiblingsNumber={Infinity}
        tabStyle={{ backgroundColor: '#F4F4F4' }}
        tabContainerStyle={{ elevation: 0 }}
        tabBarUnderlineStyle={{ backgroundColor: '#000000', height: 2 }}
        onChangeTab={({ i }) => this.setState({ activeTab: i })}
        locked={Platform.OS === 'android'}
      >
        {this.renderAllTabs()}
      </Tabs>
    );
  }

  renderAbsoluteCustomHeader(tourDetail, animatedValue, animatedText) {
    return (
      <AbsoluteCustomHeader style={{ backgroundColor: animatedValue || 'transparent' }}>
        <RowView>
          <ColumnView size={2}>
            <SideButton onPress={() => this.props.navigation.pop()}>
              <SideIcon name='arrow-back' />
            </SideButton>
          </ColumnView>

          <ColumnView size={7.8}>
            <AnimatedTitleText numberOfLines={2} type='bold' style={animatedText && { color: animatedText }}>{tourDetail.name}</AnimatedTitleText>
          </ColumnView>

          <ColumnView size={2.2}><View /></ColumnView>
        </RowView>
      </AbsoluteCustomHeader>
    );
  }

  render() {
    const { tourDetail, loading, duration, departureDate, returnDate } = this.state;
    const { departureId, tourId, animatedValue, animatedText } = this.props.navigation.state.params;
    return (
      <MainView>
        {tourDetail && this.renderAbsoluteCustomHeader(tourDetail, animatedValue, animatedText)}
        <UpperMainView>
          <ScrollView contentContainerStyle={{ width: '100%', minHeight: '100%' }} onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: this.animatedValue } } }])}>
            {tourDetail && this.renderBackgroundImage(tourDetail)}
            {/*<CustomHeader transparentMode showLeftButton showIconLeftButton={false} />*/}
            {loading && this.renderLoadingIndicator()}
            {tourDetail && <SubMainView>
              <MainInfoContainer>
                <MainInfoView>
                  <TitleInfoView><TitleText type='bold'>{duration}D {tourDetail.name}</TitleText></TitleInfoView>
                  <DateInfoView>
                    <LeftDateInfoView><DateText type='medium'>{departureDate} - {returnDate}</DateText></LeftDateInfoView>
                    <RightDateInfoView onPress={() => this.props.navigation.pop()}>
                      <CalendarView><ImageProperty resizeMode='stretch' source={calendar} /></CalendarView>
                      <ChangeDateText type='semibold'>Ubah</ChangeDateText>
                    </RightDateInfoView>
                  </DateInfoView>
                </MainInfoView>
              </MainInfoContainer>
              {this.renderMainTabs()}
            </SubMainView>}
          </ScrollView>
        </UpperMainView>
        {tourDetail && <BottomMainView>
          <HalfBottomMainView>
            <BottomText>Mulai dari</BottomText>
            <BottomText><PriceText type='bold'>{FormatMoney.getFormattedMoney(tourDetail.startingPrice.finalPrice)}</PriceText> per org</BottomText>
          </HalfBottomMainView>
          <BookingButton onPress={() => this.props.navigation.navigate('BookTravel', { tourDetail, tourId, departureId })}>Pesan</BookingButton>
        </BottomMainView>}
      </MainView>
    );
  }
}
