import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import Styled from 'styled-components';

import Header from '../Header';
import TourDepartureCard from './CardTourDeparture';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    backgroundColor: #DDDDDD;
`;

const CustomScrollView = Styled(ScrollView)`
    width: 100%;
    height: 100%;
`;

const CustomTourDeparture = Styled(TourDepartureCard)`
    marginBottom: 8;
`;

export default class SelectTravelDeparture extends Component {

  static navigationOptions = { header: null };

  renderDepartures() {
    const { data } = this.props.navigation.state.params;
    return data.departures.map((departure, j) =>
      <CustomTourDeparture data={{ departure, tourId: data.id, departureId: departure.id, tourSlug: data.slug }} key={j} />
    );
  }

  render() {
    return (
      <MainView>
        <Header title='Pilih Tanggal Keberangkatan' />
        <CustomScrollView>
         {this.renderDepartures()}
        </CustomScrollView>
      </MainView>
    );
  }
}
