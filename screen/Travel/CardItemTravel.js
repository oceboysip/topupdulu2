import React, { Component } from 'react';
import { View, Image } from 'react-native';
import Styled from 'styled-components';
import { withNavigation } from 'react-navigation';

import Text from '../Text';
import Color from '../Color';
import FormatMoney from '../FormatMoney';
import TouchableOpacity from '../Button/TouchableDebounce';

const MainView = Styled(TouchableOpacity)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    backgroundColor: #FFFFFF;
    elevation: 1;
`;

const MainImageView = Styled(View)`
    width: 120;
    height: 100%;
`;

const MainImageLimiter = Styled(View)`
    flex: 1;
`;

const MainAllInfo = Styled(View)`
    flex: 1;
    minHeight: 1;
    flexDirection: column;
    paddingHorizontal: 10;
    paddingVertical: 14;
`;

const TopInfo = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
`;

const TagLabelContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    margin: 8px 0px 2px 0px;
`;

const TagLabelView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    borderRadius: 30;
    backgroundColor: ${Color.theme};
    padding: 0px 8px 2px 8px;
    marginRight: 5;
`;

const BottomInfo = Styled(TopInfo)`
`;

const DottedView = Styled(View)`
    width: 100%;
    height: 1;
    marginVertical: 14;
`;

const DottedViewLimiter = Styled(View)`
    flex: 1;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const TitleText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const TagLabelText = Styled(Text)`
    fontSize: 10;
    textAlign: left;
`;

const SmallInfoText = Styled(Text)`
    color: #231F20;
    fontSize: 10;
    textAlign: left;
`;

const PriceText = Styled(Text)`
    color: #FF425E;
    textAlign: left;
`;

const dotted = require('../../images/line-dotted.png');

const SUPER_SALE = { id: 11 };
const ALL_IN = { id: 13 };

class CardItemTravel extends Component {

    isAllIn(tags) {
      for (const tag of tags)
        for (const item of tag.items) if (item.id === ALL_IN.id) return true;
      return false;
    }

    isSuperSale(tags) {
      for (const tag of tags)
        for (const item of tag.items) if (item.id === SUPER_SALE.id) return true;
      return false;
    }

    render() {
      const { data, navigation, onPress, widthImage, ...style } = this.props;
      const { name, pictures, tags, startingPrice } = data;
      const allIn = tags ? this.isAllIn(tags) : null;
      const superSale = tags ? this.isAllIn(tags) : null;

      return (
        <MainView {...style} activeOpacity={1} onPress={() => { if (onPress) onPress(); else navigation.navigate('SelectTravelDeparture', { data }); }}>
          <MainImageView style={widthImage && {width: widthImage, aspectRatio: 1}}>
            <MainImageLimiter><ImageProperty source={{ uri: pictures ? pictures[0].thumbnailUrl : data.AttractionsProductsDetail.AttractionConfig[1].value + data.AttractionsProductsDetail.photos[0].path }} /></MainImageLimiter>
          </MainImageView>

          <MainAllInfo>
            <TopInfo>
              <TitleText lineHeight={2} type='bold'>{name || data.title}</TitleText>
              {data.AttractionsProductsDetail && <TagLabelContainer><TagLabelView><TagLabelText>{data.AttractionsProductsDetail.typename}</TagLabelText></TagLabelView></TagLabelContainer>}
              {(allIn || superSale) && <TagLabelContainer>
                {allIn && <TagLabelView><TagLabelText>All in</TagLabelText></TagLabelView>}
                {superSale && <TagLabelView><TagLabelText>Supersale</TagLabelText></TagLabelView>}
              </TagLabelContainer>}
            </TopInfo>

            <DottedView>
              <DottedViewLimiter>
                <ImageProperty source={dotted} />
              </DottedViewLimiter>
            </DottedView>

            <BottomInfo>
              <SmallInfoText type='medium'>mulai dari</SmallInfoText>
              <SmallInfoText type='medium'>From <PriceText type='medium'>{FormatMoney.getFormattedMoney(startingPrice ? startingPrice.finalPrice : data.AttractionsProductsDetail.baseprice)}</PriceText></SmallInfoText>
            </BottomInfo>
          </MainAllInfo>
        </MainView>
      );
    }
}

export default withNavigation(CardItemTravel);
