import React, { Component } from 'react';
import { View, Image, TouchableOpacity as NativeTouchable } from 'react-native';
import Styled from 'styled-components';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import Text from '../Text';
import TouchableOpacity from '../Button/TouchableDebounce';
import Color from '../Color';

const MainView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    backgroundColor: #FFFFFF;
`;

const LineView = Styled(View)`
    width: 100%;
    height: 60;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
    padding: 12px 16px 12px 16px;
`;

const HeaderLineView = Styled(LineView)`
    height: 40;
    padding: 12px 0px 12px 16px;
`;

const LineViewWithBorder = Styled(LineView)`
    borderBottomWidth: 1;
    borderColor: #DDDDDD;
`;

const OperationalView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
`;

const RectangleView = Styled(NativeTouchable)`
    width: 35;
    height: 35;
    backgroundColor: ${Color.theme};
    alignItems: center;
    justifyContent: center;
    borderRadius: 3;
    borderWidth: 1;
    borderColor: #231F20;
`;

const NumberView = Styled(View)`
    width: 30;
    minHeight: 1;
    alignItems: center;
    justifyContent: center;
`;

const RemoveView = Styled(TouchableOpacity)`
    width: 55;
    height: 35;
    paddingRight: 10;
    alignItems: flex-end;
    justifyContent: center;
`;

const ExtraBedView = Styled(NativeTouchable)`
    minWidth: 1;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
`;

const CustomButton = Styled(TouchableOpacity)`
    width: 168;
    height: 45;
    borderRadius: 30;
    flexDirection: row;
    alignItems: center;
    paddingHorizontal: 16;
    justifyContent: space-between;
    backgroundColor: #231F20;
`;

const CheckboxImage = Styled(Image)`
    width: 20;
    height: 20;
`;

const TitleText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
    letterSpacing: 1;
`;

const NumberText = Styled(Text)`
    fontSize: 16;
    textAlign: left;
`;

const LabelText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const BlueLabelText = Styled(LabelText)`
    color: #5887FB;
`;

const ButtonLabel = Styled(Text)`
    textAlign: left;
    color: #FFFFFF;
`;

const OperationalIcon = Styled(AntDesign)`
    fontSize: 20;
    color: #231F20;
`;

const RemoveIcon = Styled(FontAwesome)`
    fontSize: 14;
    color: #000000;
`;

const AntDesignIcon = Styled(AntDesign)`
    fontSize: 15;
    color: #FFFFFF;
`;

const options = [
  { label: 'Dewasa', type: 'adult' },
  { label: 'Anak (2-12 thn)', type: 'child' },
];

const checked = require('../../images/checkbox-selected.png');
const checkedDisabled = require('../../images/checkbox-disabled.png');
const checkoff = require('../../images/checkbox-unselected.png');
const checkoffDisabled = require('../../images/checkbox-line-disabled.png');

const max = {
  adult: {
    withExtraBed: [3, 2, 2, 1],
    noExtraBed: [3, 2, 1]
  },
  child: {
    withExtraBed: { 1: 3, 2: 2, 3: 0 },
    noExtraBed: { 1: 2, 2: 1, 3: 0 }
  }
};

const min = { adult: 1, child: 0 };

const optionalExtraBed = { 1: 2, 2: 1 };
const mustExtraBed = { 1: 3, 2: 2 };

export default class CardBookRoom extends Component {

  tryIncrement(type) {
    const { data, extraBedAvailable } = this.props;
    const { guests } = data;
    const counterType = type === 'adult' ? 'child' : 'adult';
    const extraBed = extraBedAvailable ? 'withExtraBed' : 'noExtraBed';

    return guests[type] < max[type][extraBed][guests[counterType]];
  }

  tryDecrement(type) {
    return this.props.data.guests[type] > min[type];
  }

  decrement(type) {
    const { data, update, index, extraBedAvailable, toogleExtraBed, ruleExtraBed } = this.props;
    const { guests } = data;
    if (extraBedAvailable) {
      let onOptionalExtraBed;
      if (type === 'child') onOptionalExtraBed = guests.child - 1 === optionalExtraBed[guests.adult];
      else onOptionalExtraBed = guests.child === optionalExtraBed[guests.adult - 1];

      if (onOptionalExtraBed) ruleExtraBed(index, false);
      else { ruleExtraBed(index, true); toogleExtraBed(index, false); }
    }
    update(type, -1, index);
  }

  increment(type) {
    const { data, update, index, extraBedAvailable, toogleExtraBed, ruleExtraBed } = this.props;
    const { guests } = data;
    if (extraBedAvailable) {
      let onMustExtraBed;
      let onOptionalExtraBed;

      if (type === 'child') {
        onMustExtraBed = guests.child + 1 === mustExtraBed[guests.adult];
        onOptionalExtraBed = guests.child + 1 === optionalExtraBed[guests.adult];
      }
      else {
        onMustExtraBed = guests.child === mustExtraBed[guests.adult + 1];
        onOptionalExtraBed = guests.child === optionalExtraBed[guests.adult + 1];
      }

      if (onMustExtraBed) { ruleExtraBed(index, true); toogleExtraBed(index, true); }
      else if (onOptionalExtraBed) ruleExtraBed(index, false);
      else { ruleExtraBed(index, true); toogleExtraBed(index, false); }
    }
    update(type, 1, index);
  }

  renderOptions() {
    const { guests } = this.props.data;

    return options.map((opt, i) => {
      const canDecrement = this.tryDecrement(opt.type);
      const canIncrement = this.tryIncrement(opt.type);
      return (
        <LineViewWithBorder key={i}>
          <LabelText type='medium'>{opt.label}</LabelText>
          <OperationalView>
            <RectangleView activeOpacity={1} onPress={() => canDecrement && this.decrement(opt.type)} style={!canDecrement && { backgroundColor: Color.disabled, borderColor: '#C9C6BE' }}>
              <OperationalIcon name='minus' style={!canDecrement && { color: '#C9C6BE' }} />
            </RectangleView>
            <NumberView>
              <NumberText type='medium'>{guests[opt.type]}</NumberText>
            </NumberView>
            <RectangleView activeOpacity={1} onPress={() => canIncrement && this.increment(opt.type)} style={!canIncrement && { backgroundColor: Color.disabled, borderColor: '#C9C6BE' }}>
              <OperationalIcon name='plus' style={!canIncrement && { color: '#C9C6BE' }} />
            </RectangleView>
          </OperationalView>
        </LineViewWithBorder>
      );
    });
  }

  render() {
    const { index, data, removable, canAddRoom, toogleExtraBed, addRoom, remove, extraBedAvailable, ...style } = this.props;
    const { extraBed, lockExtraBed } = data;
    let checkImage = lockExtraBed ? { checked: checkedDisabled, checkoff: checkoffDisabled } : { checked, checkoff };
    checkImage = extraBed ? checkImage.checked : checkImage.checkoff;

    return (
      <MainView {...style}>
        <HeaderLineView>
          <TitleText type='bold'>Kamar {index + 1}</TitleText>
          {removable && <RemoveView onPress={() => remove(index)}><RemoveIcon name='remove' /></RemoveView>}
        </HeaderLineView>
        {this.renderOptions()}
        <LineView>
          {extraBedAvailable ?
            <ExtraBedView activeOpacity={1} onPress={() => !lockExtraBed && toogleExtraBed(index, !extraBed, true)}>
              <CheckboxImage resizeMode='contain' source={checkImage} />
              <LabelText type='medium' style={lockExtraBed && { color: '#BBBBBB' }}>  Extra bed</LabelText>
            </ExtraBedView>
            :
            <BlueLabelText type='medium'>Extra bed tidak tersedia</BlueLabelText>
          }
          {canAddRoom && <CustomButton onPress={() => addRoom()}>
            <AntDesignIcon name='pluscircleo' />
            <ButtonLabel type='semibold'>Tambah Kamar</ButtonLabel>
          </CustomButton>}
        </LineView>
      </MainView>
    );
  }
}
