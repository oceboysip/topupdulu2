import React, { Component } from 'react';
import { View, Image } from 'react-native';
import Styled from 'styled-components';
import { withNavigation } from 'react-navigation';
import Moment from 'moment';

import Text from '../Text';
import Button from '../Button';
import FormatMoney from '../FormatMoney';
import Header from '../Header';

const MainView = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: #FFFFFF;
    padding: 8px 8px 8px 8px;
    flexDirection: row;
`;

const LeftView = Styled(View)`
    width: 60%;
    minHeight: 1;
    alignItems: flex-start;
    justifyContent: flex-start;
    flexDirection: column;
`;

const RightView = Styled(LeftView)`
    width: 40%;
    alignItems: center;
`;

const AirlineView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    marginVertical: 6;
`;

const AirlineImageView = Styled(View)`
    width: 20;
    height: 20;
    marginRight: 6;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const DateText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const AirlineText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const SeatText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const FinalPriceText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
    color: #FF425E;
`;

const SelectButton = Styled(Button)`
    width: 120.5;
    height: 25;
    marginTop: 10;
`;

const UnavailableButton = Styled(SelectButton)`
    backgroundColor: #DDDDDD;
`;

const OldPriceText = Styled(FinalPriceText)`
    color: #A8A699;
    textDecorationLine: line-through;
`;

class TourDeparture extends Component {

  static navigationOptions = ({ navigation, screenProps }) => ({
    headerTitle: <Header title='Pilih Tanggal Keberangkatan' showLeftButton {...screenProps} />,
    headerLeft: null,
    headerRight: null
  });

  renderAirlines(airlines) {
    return airlines.map((airline, i) =>
      <AirlineView key={i}>
        <AirlineImageView><ImageProperty resizeMode='contain' source={{ uri: airline.logoUrl }} /></AirlineImageView>
        <AirlineText type='medium'>{airline.name}</AirlineText>
      </AirlineView>
    );
  }

  render() {
    const { data, ...style } = this.props;
    const { departure, departureId, tourSlug, tourId } = data;
    const { date, duration, airlines, quota, reserved, startingPrice } = departure;
    const departureDate = Moment(date);
    const returnDate = Moment(date).add(duration - 1, 'days');
    const oldPrice = startingPrice.price;
    const finalPrice = startingPrice.finalPrice;
    const availableSeats = quota - reserved;

    return (
      <MainView {...style}>
        <LeftView>
          <DateText type='bold'>{departureDate.format('DD MMM YYYY')} - {returnDate.format('DD MMM YYYY')}</DateText>
          {this.renderAirlines(airlines)}
          <SeatText type='medium'>{availableSeats > 9 ? 'Pemesanan Online' : availableSeats > 0 ? `Sisa: ${availableSeats} Kursi` : 'Tempat tidak tersedia'}</SeatText>
        </LeftView>

        <RightView>
          {oldPrice !== finalPrice && <OldPriceText type='bold'>{FormatMoney.getFormattedMoney(oldPrice)}</OldPriceText>}
          <FinalPriceText type='bold'>{FormatMoney.getFormattedMoney(finalPrice)}/Pax</FinalPriceText>
          {availableSeats ? <SelectButton fontSize={12} onPress={() => this.props.navigation.navigate('ItineraryTravelScreen', { departureId, tourSlug, tourId })}>Pilih Tanggal</SelectButton>
          :
          <UnavailableButton fontSize={12}>Terjual Habis</UnavailableButton>}
        </RightView>
      </MainView>
    );
  }
}

export default withNavigation(TourDeparture);
