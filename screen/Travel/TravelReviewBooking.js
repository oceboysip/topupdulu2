import React, { Component } from 'react';
import { View, ScrollView, Modal, Platform} from 'react-native';
import Styled from 'styled-components';
import { connect } from 'react-redux';
import moment from 'moment';
import Icon from 'react-native-vector-icons/Ionicons';
import Accordion from 'react-native-collapsible/Accordion';

import TouchableOpacity from '../Button/TouchableDebounce';
import FormatMoney from '../FormatMoney';
import validate from '../ValidateAuth';
import ModalIndicator from '../Modal/ModalIndicator';
import ContactBooking from '../ContactBooking';
import ModalInformation from '../Modal/ModalInformation';
// import ModalFlightDetail from '../Modal/ModalFlightDetail';
import PassangersItem from '../Flight/PassangersItem';
import { getTitles } from '../../state/actions/get-titles';
import { bookTour } from '../../state/actions/travel/bookTour';
import Text from '../Text';
import Header from '../Header';
import Color from '../Color';

const MainView = Styled(View)`
  width: 100%;
  height: 100%;
  alignItems: center;
  backgroundColor: #FAF9F9;
`;

const BaseText = Styled(Text)`
  fontSize: 14px
  textAlign: left
`;

const SmallText = Styled(BaseText)`
  fontSize: 12px
`;

const WhiteText = Styled(Text)`
  color: #FFFFFF
  lineHeight: 12px
`;

const AbsoluteView = Styled(View)`
  position: absolute
  top: 30
  width: 100%
  zIndex: 1
`;

const HeaderView = Styled(TouchableOpacity)`
  minHeight: 1px
  backgroundColor: #FFFFFF
  margin: 0px 15px
  elevation: 5px
  justifyContent: center
  alignItems: flex-start
  borderWidth: ${Platform.OS === 'ios' ? 1 : 0 };
  borderBottomWidth: ${Platform.OS === 'ios' ? 2 : 0 };
  borderColor: #DDDDDD;
`;

const PriceView = Styled(View)`
  backgroundColor: #FFFFFF
  flexDirection: row
  padding: 0px 15px
  height: 62px
  width: 100%
`;

const LeftHeaderView = Styled(View)`
  minWidth: 1;
  justifyContent: flex-start
  alignItems: flex-start
`;

const MidHeaderView = Styled(View)`
  width: 70%
  justifyContent: flex-start
  alignItems: flex-start

`;
const MidSubHeader = Styled(View)`
`;

const RightHeaderView = Styled(View)`
  width: 100%
  height: 100%
  justifyContent: center
  alignItems: flex-end
`;

const ContentView = Styled(View)`
  margin: 10px 15px 10px 11px
`;

const WrapContentView = Styled(View)`
  width: 90%
`;
const IconView = Styled(View)`
  width: 10%
`;

const PerContent = Styled(View)`
  flexDirection: row
`;

const ContactView = Styled(View)`
  width: 100%
  backgroundColor: #FFFFFF
  borderBottomColor: #FAF9F9
  borderBottomWidth: 8px
`;

const SubContactView = Styled(View)`
  margin: 22px 15px
  alignItems: flex-start
`;
const SubtotalView = Styled(View)`
  margin: 22px 15px 10px
  alignItems: flex-start
  flexDirection: row
  elevation: 5px
`;

const TextView = Styled(SubtotalView)`
  margin: 0px 10px 0px 0px
  alignItems: flex-start
  flexDirection: row
`;
const MainBookingView = Styled(View)`
  width: 100%;
  height: 63;
  flexDirection: row;
  justifyContent: space-between;
  alignItems: center;
  backgroundColor: #FFFFFF;
  padding: 8px 16px 8px 16px;
  elevation: 20;
`;

const LeftView = Styled(MidHeaderView)`
  alignItems: flex-end
`;

const ButtonText = Styled(Text)`
  fontSize: 14px
  color: #FFFFFF
  lineHeight: 34px
`;

const PriceText = Styled(Text)`
  color: #FF425E
`;

const ButtonRadius = Styled(TouchableOpacity)`
  borderRadius: 30px;
  width: 100%
  backgroundColor: #231F20;
  height: 100%
  justifyContent: center
`;
const ButtomView = Styled(View)`
  width: 100%
  backgroundColor: #FFFFFF
  alignItems: center
  justifyContent: center
  flexDirection: row
`;
const HalfBookingView = Styled(View)`
  width: 50%;
  height: 100%;
  flexDirection: column;
  alignItems: flex-start;
  justifyContent: center;
`;

const NormalText = Styled(Text)`
`;

const HeaderContent = Styled(View)`
  width: 100%;
  minHeight: 50;
  backgroundColor: #FFFFFF;
  flexDirection: row;
  padding: 8px 16px 8px 16px;
  alignItems: center;
  borderTopWidth: 0.5
  borderTopColor: #F4F4F4;
`;

const HeaderContentLeft = Styled(View)`
   width: 90%;
   minHeight: 1;
   alignItems: flex-start;
`;

const HeaderContentRight = Styled(HeaderContentLeft)`
   width: 10%;
   alignItems: flex-end;
`;

const IconArrow = Styled(Icon)`
    fontSize: 14;
    color: #231F20;
`;

const titles = [
  { abbr: 'MR', name: 'Mr', isAdult: true },
  { abbr: 'MRS', name: 'Mrs', isAdult: true },
  { abbr: 'MS', name: 'Ms', isAdult: true }
];

const ageTypeView = {
  ADULT: 'Dewasa',
  CHILD: 'Anak',
};

const piceTypeView = {
  SINGLE: 'Single',
  TWIN_SHARING: 'Twin Sharing',
  EXTRA_BED: 'Extra Bed',
  NO_BED: 'No Bed'
};

const penumpang = { adult: 1, child: 0, infant: 0 };

const objValue = {
  title: '',
  firstName: '',
  lastName: '',
  birthDate: '',
  errors: {
    title: null,
    firstName: null,
    lastName: null,
    birthDate: null
  }
}

  class TravelReviewVBooking extends Component {
    static navigationOptions = { header: null };

    constructor(props){
      super(props);
      let objTravel = null;
      let passengers = 0;

      const rooms = props.navigation.state.params.bookTourData.rooms.map((room, roomId) => {
        const travellers = room.travellers.map((traveller, travellerId) => ({
          ...traveller,
          ...objValue,
          id: travellerId,
          roomId
        }));
        passengers += room.travellers.length;
        return {
          ...room,
          travellers,
        }
      })

      const bookedTour = {
        departureId: props.navigation.state.params.bookTourData.departureId,
        isDp: props.navigation.state.params.bookTourData.isDp,
        isIncludeVisa: props.navigation.state.params.bookTourData.isIncludeVisa,
        tourId: props.navigation.state.params.bookTourData.tourId,
        rooms,
      };

      const tempUsedUserPaxes = [];
      for (let i = 0; i < passengers; i++) tempUsedUserPaxes.push('');

      const { title, firstName, lastName, countryCode, phoneNumber, email } = this.props.user_siago;

      this.state = {
        ModalPassanger: false,
        modalDetail: false,
        modalFail: false,
        modalFailSameContact: false,
        loading: false,
        bookedTour,
        contact:{
          title: title || '',
          firstName: firstName || '',
          lastName: lastName || '',
          countryCode: countryCode || '62',
          phone: phoneNumber || '',
          email: email || ''
        },
        errors: {
          title: null,
          firstName: null,
          lastName: null,
          countryCode: null,
          phone: null,
          email: null,
        }
      }

      this.props.getTitles();
    }

    openModal = (modal) => {
      this.setState({
        [modal]: true
      })
    }

    closeModal = (modal) => {
      this.setState({
        [modal]: false
      })
    }

    deletePassanger = (id) => {

    }

    componentWillReceiveProps(nextProps) {
      if (!this.props.fetchingBook && nextProps.fetchingBook) {
        this.openModal('loading');
      }else if (this.props.fetchingBook && !nextProps.fetchingBook) {
        this.closeModal('loading');
        if (nextProps.error === null) {
          this.props.navigation.navigate('PaymentScreen');
        }else {
          this.setState({ modalFail: true }, () => setTimeout(() => {
            this.setState({ modalFail: false })
          }, 1000));
        }
      }
    }

    onContactChange = (name, value) => {
      this.setState(prevState => {
        return {
          contact: {
            ...prevState.contact,
            [name]: value
          }
        }
      }, () => {})
    }

    modalFailSameContact = (modalFailSameContact) => {
      this.setState({ modalFailSameContact })
    }

    onSameContact = (switchOn) => {
      const { title, firstName, lastName } = this.state.contact;
      let passengers = {};

      if (switchOn) {
        passengers = {
          ...this.state.bookedTour.rooms[0].travellers[0],
          title,
          firstName,
          lastName
        };
      }else {
        passengers = {
          ...this.state.bookedTour.rooms[0].travellers[0],
          title: '',
          firstName: '',
          lastName: '',
          birthDate: ''
        };
      }

      this.setState(prevState => {
        return {
          ...prevState,
          bookedTour: {
            ...prevState.bookedTour,
            rooms: prevState.bookedTour.rooms.map((room, roomId) => {
              return {
                ...room,
                travellers: room.travellers.map((traveller, travellerId) => {
                  if (travellerId === 0 && roomId === 0) {
                    return {
                      ...traveller,
                      title: passengers.title,
                      firstName: passengers.firstName,
                      lastName: passengers.lastName,
                      birthDate: passengers.birthDate
                    }
                  }else {
                    return traveller;
                  }
                })
              }
            })
          }
        }
      });
    }

    onPassengerChange = (id, selectedRoomId, name, value) => {
      this.setState(prevState => {
        return {
          ...prevState,
          bookedTour: {
            ...prevState.bookedTour,
            rooms: prevState.bookedTour.rooms.map((room, roomId) => {
              return {
                ...room,
                travellers: room.travellers.map((traveller, travellerId) => {
                  if (travellerId === id && roomId === selectedRoomId)
                  return {
                    ...traveller,
                    [name]: value
                  }

                  return traveller;
                })
              }
            })
          }
        }
      });
    }

    onValidatePassenger = (id, selectedRoomId, name) => {
      let valid = false;
      let error = null
      this.state.bookedTour.rooms.map((room, roomId) => {
        room.travellers.map((traveller, travellerId) => {
          if(travellerId === id && roomId === selectedRoomId) {
            error = validate(name, traveller[name]);
          }
        })
      });
      this.setState(prevState => {
        return {
          ...prevState,
          bookedTour: {
            ...prevState.bookedTour,
            rooms: prevState.bookedTour.rooms.map((room, roomId) => {
              return {
                ...room,
                travellers: room.travellers.map((traveller, travellerId) => {
                  if (travellerId === id && roomId === selectedRoomId)
                  return {
                  ...traveller,
                  error: {
                      ...traveller.error,
                      [name]: error
                    }
                  };

                  return traveller;
                })
              }
            })
          }
        }
      })
    }

    getTitleLabel = (abbr) => {
      const idx = titles.findIndex(title => title.abbr === abbr);
      if(idx !== -1) {
          return titles[idx].name;
      } else {
          return ''
      }
    }

    submit(finalAmount) {
      const { contact, bookedTour } = this.state;
      this.props.onBookTour(contact, bookedTour, finalAmount);
    }

    renderAccordionHeader = (item, index, isActive) => {
      let iconArrow = 'ios-arrow-forward';
      if (isActive) iconArrow = 'ios-arrow-down';

      return (
        <HeaderContent>
          <HeaderContentLeft>
            <BaseText size={16} type='medium'>Kamar {index + 1}</BaseText>
          </HeaderContentLeft>
          {/* <HeaderContentRight>
            <IconArrow name={iconArrow} />
          </HeaderContentRight> */}
        </HeaderContent>
      )
    }

    renderAccordionContent = (item, i) => {
      return this.renderPassenger(item, i);
    }

    renderPassenger(room, roomId) {
      const { params } = this.props.navigation.state;
      const duration = params.duration - 1;
      const returnDateTour = moment(params.date).add(duration, 'days');

      return room.travellers.map((val, i) =>
        <PassangersItem
          key={i}
          index={i}
          titles={titles}
          pax={val}
          id={i}
          tour={true}
          deletePassanger={this.deletePassanger}
          onPassangerChange={this.onPassengerChange}
          roomId={roomId}
          onSameContact={this.onSameContact}
          contact={this.state.contact}
          ordering={this.state.bookedTour.rooms[0].travellers[0]}
          modalFailSameContact={this.modalFailSameContact}
        />
      );
    }

    renderRoom() {
      //accordion
      // return (
      //   <Accordion
      //     touchableProps={{activeOpacity: 1}}
      //     sections={this.state.bookedTour.rooms}
      //     renderHeader={this.renderAccordionHeader}
      //     renderContent={this.renderAccordionContent}
      //   />
      // )
      return this.state.bookedTour.rooms.map((item, idx) =>
        <View key={idx}>
          {this.renderAccordionHeader(item, idx)}
          {this.renderAccordionContent(item, idx)}
        </View>
      )
    }

    render() {
      const { params } = this.props.navigation.state;

      return (
        <MainView>
          <Header title='Review & Pesan' style={{width: '100%'}} />
          <ScrollView style={{ minHeight: 1, width: '100%' }}>
            <View style={{height: 76, backgroundColor: Color.theme, width: '100%'}} />
            <AbsoluteView>
              <HeaderView>
                <ContentView>
                  <BaseText type='bold'>{params.tourName}</BaseText>
                  <BaseText type='medium'>Tanggal Keberangkatan: {params.date}</BaseText>
                  <BaseText type='medium'>Durasi: {params.duration} Hari</BaseText>
                  <BaseText type='medium'>{params.airline.length > 1 ? 'Multi Airlines' : params.airline[0].name}</BaseText>
                </ContentView>
              </HeaderView>
            </AbsoluteView>
            <PriceView />
            <ContactView>
              <ContactBooking
                titles={this.props.titles}
                contact={this.state.contact}
                errors={this.state.errors}
                onContactChange={this.onContactChange}
              />
            </ContactView>
            <ContactView>
              <SubContactView>
                <Text type='bold'>DATA WISATAWAN</Text>
              </SubContactView>
            </ContactView>
            {this.renderRoom()}
            <PriceView />
          </ScrollView>
          <ButtomView>
            <MainBookingView>
              <HalfBookingView>
                <NormalText type='medium'>Subtotal</NormalText>
                <PriceText type='bold' color='button'>{FormatMoney.getFormattedMoney(params.payAmount)}</PriceText>
              </HalfBookingView>
              <HalfBookingView>
                <ButtonRadius onPress={() => this.submit(params.payAmount)} delay={1000} >
                  <ButtonText>Bayar Sekarang</ButtonText>
                </ButtonRadius>
              </HalfBookingView>
            </MainBookingView>
          </ButtomView>

          {/*<Modal
            onRequestClose={() => this.closeModal('modalDetail')}
            animationType="fade"
            transparent
            visible={this.state.modalDetail}
          >
            <ModalFlightDetail
              closeModal={this.closeModal}
            />
          </Modal>*/}

          <Modal
            onRequestClose={() => {}}
            animationType="fade"
            transparent
            visible={this.state.modalFailSameContact}
          >
            <ModalInformation
              label='Lengkapi Data Pemesan terlebih dahulu'
              warning
            />
          </Modal>

          <ModalIndicator
            visible={this.state.loading}
            type="large"
            message="Harap tunggu, kami sedang memproses pesanan Anda"
          />

          <Modal
            onRequestClose={() => {}}
            animationType="fade"
            transparent
            visible={this.state.modalFail}
          >
            <ModalInformation
              label='Booking Anda Gagal'
              error
            />
          </Modal>
        </MainView>
      );
    }
  }

const mapStateToProps = state => {
  return {
    user: state['user.auth'].login.user,
    fetchingBook: state['booking'].fetching,
    error: state['booking'].error,
    titles: state['titles'].titles,
    user_siago: state['user.auth'].user_siago
  };
};

const mapDispatchToProps = (dispatch) => (
  {
    onBookTour: (contact, tour, finalAmount) => {
      dispatch(bookTour(contact, tour, finalAmount));
    },
    getTitles: () => {
      dispatch(getTitles());
    },
    clearBooking: () => {
      dispatch({
        type: 'BOOKING.CLEAR_BOOKING'
      })
    },
    clearSelectedFlight: () => {
      dispatch({
        type: 'FLIGHT.CLEAR_SELECTED'
      })
    }
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(TravelReviewVBooking);
