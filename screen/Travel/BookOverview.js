import React, { Component } from 'react';
import { View, Image, ScrollView, TouchableOpacity as NativeTouchable, Platform, Modal } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Styled from 'styled-components';
import Moment from 'moment';
import { connect } from 'react-redux';

import Text from '../Text';
import Color from '../Color';
import Header from '../Header';
import Button from '../Button';
import FormatMoney from '../FormatMoney';
import TouchableOpacity from '../Button/TouchableDebounce';
import ModalDetailBookTour from '../Modal/ModalDetailBookTour';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    flexDirection: column;
    backgroundColor: #FFFFFF;
`;

const MiniBackgroundView = Styled(View)`
    width: 100%;
    height: 50;
    backgroundColor: ${Color.theme};
    position: absolute;
`;

const MainContentView = Styled(View)`
    flex: 1;
`;

const AirlineView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    marginVertical: 6;
`;

const AirlineImageView = Styled(View)`
    width: 20;
    height: 20;
    marginRight: 6;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const ContentContainerView = Styled(View)`
    width: 100%;
    minHeight: 1;
    padding: 0px 16px 20px 16px;
`;

const MainInfoView = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: #FFFFFF;
    marginTop: 20;
    padding: 10px 14px 10px 14px;
    borderRadius: 3;
    elevation: 5;
      borderWidth: ${Platform.OS === 'ios' ? 1 : 0 };
    borderBottomWidth: ${Platform.OS === 'ios' ? 2 : 0 };
    borderColor: #DDDDDD;
`;

const CustomButton = Styled(TouchableOpacity)`
    width: 168;
    height: 45;
    borderRadius: 30;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
    paddingHorizontal: 20;
    backgroundColor: #231F20;
    marginTop: 8;
`;

const BottomContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    backgroundColor: #FFFFFF;
    borderWidth: 0;
    elevation: 5;
`;

const VerticalView = Styled(View)`
    flexDirection: column;
`;

const LineView = Styled(View)`
    height: 55;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
`;

const OptionTouch = Styled(NativeTouchable)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
    paddingHorizontal: 16;
`;

const BottomBookView = Styled(LineView)`
    width: 100%;
    paddingHorizontal: 16;
`;

const NormalText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const BiggerNormalText = Styled(Text)`
    textAlign: left;
`;

const UnderlinedBiggerNormalText = Styled(BiggerNormalText)`
    textDecorationLine: underline;
`;

const TitleInfoText = Styled(Text)`
    textAlign: left;
    marginBottom: 6;
`;

const DetailInfoText = Styled(NormalText)`
    textAlign: left;
    marginBottom: 6;
`;

const ButtonLabel = Styled(Text)`
    textAlign: left;
    color: #FFFFFF;
`;

const GrandTotalText = Styled(Text)`
    textAlign: left;
`;

const RedPriceText = Styled(GrandTotalText)`
    color: #FF425E;
`;

const MaterialIconsIcon = Styled(MaterialIcons)`
    fontSize: 18;
    color: #FFFFFF;
`;

const BookButton = Styled(Button)`
    width: 140;
    height: 45;
`;

const CheckboxImage = Styled(Image)`
    width: 20;
    height: 20;
    marginRight: 8;
`;

const typeBeds = [
  { code: 'adultSingle', object: { type: 'ADULT', priceType: 'SINGLE' } },
  { code: 'adultTwinSharing', object: { type: 'ADULT', priceType: 'TWIN_SHARING' } },
  { code: 'childTwinSharing', object: { type: 'CHILD', priceType: 'TWIN_SHARING' } },
  { code: 'childExtraBed', object: { type: 'CHILD', priceType: 'EXTRA_BED' } },
  { code: 'childNoBed', object: { type: 'CHILD', priceType: 'NO_BED' } },
];

const checked = require('../../images/checkbox-selected.png');
const checkoff = require('../../images/checkbox-unselected.png');

let navigation;

class BookOverview extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    navigation = this.props.navigation;
    this.state = {
      isDownPayment: false,
      modalVisible: false,
    };
  }

  closeModal() {
    this.setState({ modalVisible: false });
  }

  openModal() {
    this.setState({ modalVisible: true });
  }

  getSumAllProps(obj) {
    return Object.values(obj).reduce((a, b) => a + b);
  }

  renderAirlines(airlines) {
    return airlines.map((airline, i) =>
      <AirlineView key={i}>
        <AirlineImageView><ImageProperty resizeMode='contain' source={{ uri: airline.logoUrl }} /></AirlineImageView>
        <DetailInfoText type='medium'>{airline.name}</DetailInfoText>
      </AirlineView>
    );
  }

  renderMainInfoView(tourDetail, name, date, duration, airlines, guests, sumBeds) {
    const departureDate = Moment(date);
    const returnDate = Moment(date).add(duration - 1, 'days');

    return (
      <MainInfoView>
        <TitleInfoText type='bold'>{duration}D {name}</TitleInfoText>
        <DetailInfoText type='medium'>Tanggal berangkat : {departureDate.format('DD MMM YYYY')} - {returnDate.format('DD MMM YYYY')} </DetailInfoText>
        <DetailInfoText type='medium'>Durasi : {duration} Hari</DetailInfoText>
        {this.renderAirlines(airlines)}
        <DetailInfoText type='medium'>{sumBeds} Kamar, {guests.adult} Dewasa{guests.child > 0 && `, ${guests.child} Anak`}</DetailInfoText>
        <CustomButton onPress={() => navigation.pop()}>
          <MaterialIconsIcon name='edit' />
          <ButtonLabel type='semibold'>Ubah Peserta</ButtonLabel>
        </CustomButton>
      </MainInfoView>
    );
  }

  // renderOptions(tourDetail, grandTotal, isDownPayment) {
  //   const options = [
  //     { label: 'Uang Muka', state: true, value: tourDetail.departure.price.downPaymentPrice },
  //     { label: 'Bayar Lunas', state: false, value: grandTotal }
  //   ];

  //   return options.map((opt, i) =>
  //     <OptionTouch key={i} activeOpacity={1} onPress={() => this.setState({ isDownPayment: opt.state })}>
  //       <LineView>
  //         <CheckboxImage resizeMode='contain' source={isDownPayment === opt.state ? checked : checkoff} />
  //         <NormalText type='medium'>{opt.label}:</NormalText>
  //       </LineView>
  //       <NormalText type='medium'>{FormatMoney.getFormattedMoney(opt.value)}</NormalText>
  //     </OptionTouch>
  //   );
  // }

  bookTour() {
    const { isDownPayment } = this.state;
    const { tourDetail, departureId, tourId, includeVisa, rooms, grandTotal } = navigation.state.params;
    const payAmount = isDownPayment ? tourDetail.departure.price.downPaymentPrice : grandTotal;
    const newRooms = rooms.map((room) => {
      let travellers = [];
      const beds = Object.keys(room.beds);
      for (const bed of beds) {
        for (const type of typeBeds) if (bed === type.code) travellers = travellers.concat(Array(room.beds[bed]).fill(type.object));
      }
      return { extraBed: room.extraBed, travellers };
    });
    const allData = {
      date: tourDetail.departure.date,
      duration: tourDetail.departure.duration,
      tourName: tourDetail.name,
      airline: tourDetail.departure.airlines,
      payAmount,
      bookTourData: {
        departureId,
        tourId,
        isDp: isDownPayment,
        isIncludeVisa: includeVisa,
        grandTotal,
        rooms: newRooms
      }
    };
    if (this.props.user.guest) navigation.navigate('LoginScreen', {
      loginFrom: 'tour',
      afterLogin: () => this.props.navigation.navigate('TravelReviewBooking', { ...allData })
    });
    else this.props.navigation.navigate('TravelReviewBooking', { ...allData });
  }

  render() {
    const { isDownPayment, modalVisible } = this.state;
    const { tourDetail, totalBeds, guests, grandTotal, allBedsPrice, totalVat, totalTaxFuel, totalVisaPrice, includeVisa, rooms } = navigation.state.params;
    const { name, departure } = tourDetail;
    const { date, duration, airlines, price } = departure;
    return (
      <MainView>
        <Header title='Pesan Travel' />
        <MainContentView>
          <ScrollView>
            <MiniBackgroundView />
            <ContentContainerView>
              {this.renderMainInfoView(tourDetail, name, date, duration, airlines, guests, rooms.length)}
              <LineView>
                <BiggerNormalText type='medium'>Rincian Bayar</BiggerNormalText>
                <UnderlinedBiggerNormalText type='medium' onPress={() => this.openModal()}>Detail</UnderlinedBiggerNormalText>
              </LineView>
            </ContentContainerView>
          </ScrollView>
        </MainContentView>
        <BottomContainer>
          {/* {this.renderOptions(tourDetail, grandTotal, isDownPayment)} */}
          <BottomBookView>
            <VerticalView>
              {/* <GrandTotalText type='medium'>{isDownPayment ? 'Uang Muka' : 'Bayar Lunas'}:</GrandTotalText> */}
              <RedPriceText type='bold'>{FormatMoney.getFormattedMoney(isDownPayment ? price.downPaymentPrice : grandTotal)}</RedPriceText>
            </VerticalView>
            <BookButton onPress={() => this.bookTour()}>Pesan Travel</BookButton>
          </BottomBookView>
        </BottomContainer>
        <Modal
          visible={modalVisible}
          animationType='slide'
          onRequestClose={() => this.closeModal()}
        >
          <ModalDetailBookTour data={{ tourDetail, allBedsPrice, totalVat, totalTaxFuel, totalVisaPrice, totalBeds, guests, includeVisa }} onClose={() => this.closeModal()} />
        </Modal>
      </MainView>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state['user.auth'].login.user,
});

export default connect(mapStateToProps)(BookOverview);
