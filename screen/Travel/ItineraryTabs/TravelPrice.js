import React, { Component } from 'react';
import { View } from 'react-native';
import Styled from 'styled-components';

import Text from '../../Text';
import FormatMoney from '../../FormatMoney';

const MainView = Styled(View)`
    alignItems: center;
    backgroundColor: #FFFFFF;
`;

const CardMainView = Styled(View)`
    width: 94%;
    minHeight: 1;
    flexDirection: column;
    paddingTop: 10;
    paddingBottom: 10;
    borderBottomWidth: 1;
    borderColor: #F4F4F4;
`;

const CardPerRowView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    paddingTop: 2;
    paddingBottom: 2;
`;

const CardHalfLeftView = Styled(View)`
    width: 50%;
    minHeight: 1;
    alignItems: flex-start;
    justifyContent: center;
`;

const CardHalfRightView = Styled(CardHalfLeftView)`
    alignItems: flex-end;
`;

const NormalText = Styled(Text)`
    fontSize: 12;
`;

export default class TravelPrice extends Component {

  constructor(props) {
    super(props);
    const { tourPrice, visas } = this.props.data;
    const isManaged = visas.length > 0 ? visas.every(visa => visa.managed === true) : false;
    this.state = {
      details: [
        {
          title: 'Dewasa',
          info: [
            { label: 'Twin Sharing', value: FormatMoney.getFormattedMoney(tourPrice.adultTwinSharingPrice.finalPrice) },
            { label: 'Single', value: FormatMoney.getFormattedMoney(tourPrice.adultSinglePrice.finalPrice) }
          ]
        },
        {
          title: 'Anak-anak',
          info: [
            { label: 'Twin Sharing', value: FormatMoney.getFormattedMoney(tourPrice.childTwinSharingPrice.finalPrice) },
            { label: 'Extra Bed', value: tourPrice.childExtraBedPrice.finalPrice !== 0 ? FormatMoney.getFormattedMoney(tourPrice.childExtraBedPrice.finalPrice) : 'Tidak tersedia' },
            { label: 'No Bed', value: FormatMoney.getFormattedMoney(tourPrice.childNoBedPrice.finalPrice) },
          ]
        },
        {
          title: 'Biaya lainnya',
          info: [
            { label: 'Airport Tax & Fuel', value: tourPrice.airportTaxAndFuelPrice !== 0 ? FormatMoney.getFormattedMoney(tourPrice.airportTaxAndFuelPrice) : 'Sudah termasuk' },
            { label: 'Visa', value: tourPrice.visaPrice !== 0 ? FormatMoney.getFormattedMoney(tourPrice.visaPrice) : 'Sudah termasuk', isManaged },
            { label: 'Ppn', value: tourPrice.vatPercent !== 0 ? `${tourPrice.vatPercent} %` : 'Sudah termasuk' },
          ]
        },
      ]
    };
  }

  renderSubInfo(info) {
    return info.map((inf, i) =>
      (inf.isManaged === undefined || inf.isManaged) && <CardPerRowView key={i}>
        <CardHalfLeftView><NormalText type='medium'>- {inf.label}</NormalText></CardHalfLeftView>
        <CardHalfRightView><NormalText type='medium'>{inf.value}</NormalText></CardHalfRightView>
      </CardPerRowView>
    );
  }

  renderMainInfo() {
    return this.state.details.map((detail, i) =>
      <CardMainView key={i}>
        <CardPerRowView>
          <NormalText type='medium'>{detail.title}</NormalText>
        </CardPerRowView>
        {this.renderSubInfo(detail.info)}
      </CardMainView>
    );
  }

  render() {
    return (
      <MainView>
        {this.renderMainInfo()}
      </MainView>
    );
  }
}
