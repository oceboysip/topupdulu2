import React, { Component } from 'react';
import { View, Modal } from 'react-native';
import Styled from 'styled-components';

import Text from '../../Text';
import TouchableOpacity from '../../Button/TouchableDebounce';
import ModalTravelTermsConditions from '../../Modal/ModalTravelTermsConditions';

const MainView = Styled(View)`
    alignItems: center;
    backgroundColor: #FFFFFF;
    paddingHorizontal: 16;
`;

const LineView = Styled(TouchableOpacity)`
    width: 100%;
    minHeight: 1;
    paddingVertical: 12;
    justifyContent: center;
    borderBottomWidth: 0.5;
    borderColor: #DDDDDD;
`;

const NormalText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const TitleText = Styled(NormalText)`
    marginBottom: 4;
`;

export default class TermsConditions extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      informations: [
        {
          title: 'Syarat dan Ketentuan',
          desc: 'Syarat-Syarat dan Ketentuan Perjalanan ini',
          modal: <ModalTravelTermsConditions onClose={() => this.closeModal()} />
        }
      ],
    };
  }

  closeModal() {
    this.setState({ modalVisible: false });
  }

  openModal() {
    this.setState({ modalVisible: true });
  }

  renderInformations() {
    return this.state.informations.map((info, i) =>
      <LineView key={i} onPress={() => this.openModal()}>
        <TitleText type='semibold'>{info.title}</TitleText>
        <NormalText type='medium'>{info.desc}</NormalText>
        <Modal visible={this.state.modalVisible} animationType='slide' onRequestClose={() => this.closeModal()}>
          {info.modal}
        </Modal>
      </LineView>
    );
  }

  render() {
    return (
      <MainView>
        {this.renderInformations()}
      </MainView>
    );
  }
}
