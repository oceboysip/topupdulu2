import React, { Component } from 'react';
import { View, Image, StyleSheet } from 'react-native';
import HTMLView from 'react-native-htmlview';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Styled from 'styled-components';

import Text from '../../Text';
import Color from '../../Color';

const MainView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    padding: 8px 16px 0px 16px;
`;

const SubView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    marginTop: 30;
    marginBottom: 10;
`;

const HTMLContainer = Styled(View)`
    width: 100%;
    marginBottom: 10;
`;

const IconAndTextView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    justifyContent: flex-start;
    marginBottom: 8;
`;

const DateIconView = Styled(View)`
    width: 16;
    height: 16;
    marginRight: 8;
`;

const LineView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
`;

const LeftLineView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    marginHorizontal: 12;
    alignItems: center;
`;

const RightLineView = Styled(LeftLineView)`
    flex: 1;
    alignItems: flex-start;
    paddingTop: 0;
    marginHorizontal: 0;
`;

const Limiter = Styled(View)`
    flex: 1;
    justifyContent: center;
`;

const LimiterCenter = Styled(Limiter)`
    justifyContent: center;
    alignItems: center;
`;

const CountriesAndVisaView = Styled(View)`
    width: 100%;
    minHeight: 10;
    padding: 10px 15px 10px 0px;
    borderColor: #A8A699;
    borderWidth: 0.5;
    flexDirection: column;
`;

const CountriesAndVisaSubView = Styled(View)`
    width: 100%;
    minHeight: 1;
    alignItems: center;
    flexDirection: row;
    flexWrap: wrap;
`;

const NeedVisaView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    backgroundColor: ${Color.theme};
    alignItems: center;
    justifyContent: center;
    padding: 2px 8px 2px 8px;
    borderRadius: 50;
    marginLeft: 4px;
`;

const ManagedVisaView = Styled(View)`
    width: 100%;
    minHeight: 30;
    marginTop: 6;
    alignItems: flex-start;
    justifyContent: flex-start;
    flexWrap: wrap;
`;

const VerticalLine = Styled(View)`
    flex: 1;
    width: 1;
    backgroundColor: #231F20;
`;

const UpperVerticalLine = Styled(VerticalLine)`
    flex: 0;
    height: 2;
`;

const YellowCircleView = Styled(View)`
    width: 9;
    height: 9;
    borderRadius: 50;
    backgroundColor: ${Color.theme};
    borderColor: #231F20;
    borderWidth: 0.5;
    marginVertical: 2;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const NormalText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const SubTitleText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
    marginBottom: 16;
`;

const DaysText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const HTMLText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
    marginBottom: 12;
`;

const CountriesAndVisaText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const PlaneIcon = Styled(FontAwesome5)`
    fontSize: 14;
    color: ${Color.text};
    marginRight: 9;
`;

const calendarBlack = require('../../../images/calendar-black.png');

const styles = StyleSheet.create({});

function renderNode(node, index, siblings, parent, defaultRenderer) {
    if (node.type === 'tag') {
      const specialStyle = node.attribs.style;
      let style = {};

      if (specialStyle !== undefined) {
        const newStr = specialStyle.split(':');
        for (let i = 0; i < newStr.length; i++) style = { [newStr[0]]: newStr[1] };
      }

      if (node.children.length > 0) {
        return (
          <HTMLText key={index} style={style} type='medium'>
            {defaultRenderer(node.children, parent)}
          </HTMLText>
        );
      }
      return null;
    }
    if (node.type === 'text' && node.parent === null) return null;
    if (node.type === 'text' && /&nbsp;/g.test(node.data)) return null;
}

export default class Overview extends Component {

  renderDays(itineraries) {
    return itineraries.map((itinerary, i) =>
      <DaysText key={i} type='medium'>Hari {itinerary.dayNumber}: {itinerary.title}</DaysText>
    );
  }

  renderAllWords(string) {
    return string.split(' ').map((str, i) =>
      <CountriesAndVisaText type='medium' key={i}>{str} </CountriesAndVisaText>
    );
  }

  renderAllCountries(countriesAndVisa) {
    const firstIndex = 0;
    const lastIndex = countriesAndVisa.length - 1;
    return countriesAndVisa.map((country, i) =>
      <LineView key={i}>
        <LeftLineView>
          <LimiterCenter>
            <UpperVerticalLine style={i === firstIndex && { backgroundColor: 'transparent' }} />
            <YellowCircleView style={country.needVisa && { marginVertical: 4 }} />
            <VerticalLine style={i === lastIndex && { backgroundColor: 'transparent' }} />
          </LimiterCenter>
        </LeftLineView>
        <RightLineView>
          <CountriesAndVisaSubView style={country.needVisa && { marginBottom: 4 }}>
            {this.renderAllWords(country.name)}
            {country.needVisa && <NeedVisaView><CountriesAndVisaText type='medium'>Butuh visa</CountriesAndVisaText></NeedVisaView>}
          </CountriesAndVisaSubView>
          <ManagedVisaView style={i === lastIndex && { minHeight: 0, marginTop: 0 }}>
            {country.needVisa && <CountriesAndVisaText>Golden Rama{!country.managed && ' tidak'} membantu pengurusan visa</CountriesAndVisaText>}
          </ManagedVisaView>
        </RightLineView>
      </LineView>
    );
  }

  renderCountriesAndVisa(countries, visas) {
    const countriesAndVisa = [];
    for (const country of countries) {
      const name = country.name;
      let needVisa = false;
      let managed = false;

      for (const visa of visas) {
        if (visa.country.code === country.code) {
          needVisa = true;
          managed = visa.managed;
          break;
        }
      }
      countriesAndVisa.push({ name, needVisa, managed });
    }
    return (
      <CountriesAndVisaView>
        {this.renderAllCountries(countriesAndVisa)}
      </CountriesAndVisaView>
    );
  }

  render() {
    // const { description, departure } = this.props.data;
    const { description, visas, countries, duration, itineraries, airlinesString, availableSeats } = this.props.data;
    // const countries = [{ code: 'ID', name: 'Indonesia' }, { code: 'AM', name: 'America Serikat' }, { code: 'CN', name: 'China' }];
    // const visas = [{ country: { code: 'CN', name: 'China' }, managed: true }, { country: { code: 'AM', name: 'America' }, managed: false }];
    return (
      <MainView>
        {description && <HTMLContainer><HTMLView value={description} stylesheet={styles} addLineBreaks={false} renderNode={renderNode} /></HTMLContainer>}
        <IconAndTextView>
          <PlaneIcon name='plane' />
          <NormalText type='medium'>{airlinesString}</NormalText>
        </IconAndTextView>
        <IconAndTextView>
          <DateIconView><ImageProperty resizeMode='contain' source={calendarBlack} /></DateIconView>
          <NormalText type='medium'>{duration} Hari</NormalText>
        </IconAndTextView>
        <NormalText type='medium'>{availableSeats > 9 ? 'Pemesanan Online' : availableSeats <= 0 ? 'Tempat tidak tersedia' : `Sisa: ${availableSeats} Kursi`}</NormalText>
        <SubView>
          <SubTitleText letterSpacing={1} type='bold'>RUTE PERJALANAN</SubTitleText>
          {this.renderDays(itineraries)}
        </SubView>
        <SubView>
          <SubTitleText letterSpacing={1} type='bold'>NEGARA</SubTitleText>
          {this.renderCountriesAndVisa(countries, visas)}
        </SubView>
      </MainView>
    );
  }

}
