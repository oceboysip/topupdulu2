import React, { Component } from 'react';
import { View, Image, Modal } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';

import Text from '../../Text';
import Color from '../../Color';
import TouchableOpacity from '../../Button/TouchableDebounce';
import ModalTourFlight from '../../Modal/ModalTravelFlight';
import ModalTourPlace from '../../Modal/ModalTravelPlace';
import ModalPhotoViewer from '../../Modal/ModalPhotoViewer';

const MainView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    padding: 14px 0px 0px 16px;
    backgroundColor: white;
`;

const YellowCircleView = Styled(View)`
    width: 9;
    height: 9;
    borderRadius: 50;
    backgroundColor: ${Color.theme};
    borderColor: #231F20;
    borderWidth: 0.5;
    marginVertical: 2;
`;

const LineView = Styled(View)`
    width: 100%;
    minHeight: 1
    paddingRight: 15;
    flexDirection: row;
`;

const LineViewCenter = Styled(LineView)`
    alignItems: center;
`;

const LeftLineView = Styled(View)`
    width: 16;
    minHeight: 1;
    flexDirection: column;
    alignItems: center;
    marginRight: 16;
`;

const Limiter = Styled(View)`
    flex: 1;
    justifyContent: center;
`;

const LimiterCenter = Styled(Limiter)`
    justifyContent: center;
    alignItems: center;
`;

const VerticalLine = Styled(View)`
    flex: 1;
    width: 1;
    backgroundColor: #231F20;
`;

const RightLineView = Styled(LeftLineView)`
    flex: 1;
    minHeight: 1;
    alignItems: flex-start;
    justifyContent: flex-start;
`;

const IconView = Styled(View)`
    width: 10;
    height: 10;
    marginVertical: 2;
    justifyContent: center;
`;

const ModalTouchable = Styled(TouchableOpacity)`
    minWidth: 1;
    minHeight: 1;
    alignItems: flex-start;
    justifyContent: flex-start;
    flexDirection: column;
    padding: 4px 8px 10px 8px;
    margin: 0px 8px 16px 0px;
    borderWidth: 0.5;
    borderColor: #231F20;
    borderRadius: 3;
`;

const DescriptionView = Styled(View)`
    width: 100%;
    minHeight: 1;
    alignItems: flex-start;
    justifyContent: flex-start;
    padding: 4px 8px 4px 8px;
    marginBottom: 16;
    backgroundColor: #FFFF;
    borderRadius: 3;
    elevation: 3;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const DayText = Styled(Text)`
    fontSize: 12;
    letterSpacing: 1;
`;

const NormalText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const ModalTouchableText = Styled(NormalText)`
`;

const GreyModalTouchableText = Styled(ModalTouchableText)`
    color: #A8A699;
`;

const SubTitleText = Styled(NormalText)`
`;

const DescriptionText = Styled(NormalText)`
    lineHeight: 18;
`;

const DateText = Styled(NormalText)`
    marginBottom: 8;
`;

const MealText = Styled(NormalText)`
    marginBottom: 16;
`;

const food = require('../../../images/food.png');
const airplane = require('../../../images/airplane.png');
const map = require('../../../images/map.png');

export default class Itinerary extends Component {

  constructor(props) {
    super(props);
    this.state = {
      flights: null,
      modalData: null,
      modalVisible: null,
    };
  }

  openModal = (modal, modalData) => {
    this.setState({ modalData, modalVisible: modal });
  }

  closeModal = () => {
    this.setState({ modalVisible: null });
  }

  renderModal() {
    const { modalVisible, modalData } = this.state;
    const onClose = () => this.closeModal();

    switch (modalVisible) {
      case 'ModalTourFlight':
        return <ModalTourFlight flights={modalData} onClose={onClose} />;
      case 'ModalTourPlace':
        return <ModalTourPlace data={modalData} onClose={onClose} />;
      case 'ModalPhotoViewer':
        return <ModalPhotoViewer photos={modalData} onClose={onClose} />;
    }
  }

  renderModalTouchable(title, number, subtitle, onPress) {
    return (
      <ModalTouchable onPress={onPress}>
        <ModalTouchableText type='semibold'>{title}</ModalTouchableText>
        <GreyModalTouchableText>{number} {subtitle}</GreyModalTouchableText>
      </ModalTouchable>
    );
  }

  renderHeaderDays(firstIndex, dayNumber) {
    return (
      <LineViewCenter>
        <LeftLineView>
          <LimiterCenter>
            <VerticalLine style={firstIndex && { backgroundColor: 'transparent' }} />
            <YellowCircleView />
            <VerticalLine />
          </LimiterCenter>
        </LeftLineView>
        <RightLineView>
          <DayText type='bold'>HARI {dayNumber}</DayText>
        </RightLineView>
      </LineViewCenter>
    );
  }

  renderSubHeaderDays(title, date) {
    return (
      <LineView>
        <LeftLineView>
          <LimiterCenter>
            <VerticalLine />
          </LimiterCenter>
        </LeftLineView>
        <RightLineView>
          <SubTitleText type='medium'>{title}</SubTitleText>
          <DateText type='medium'>{Moment(date).format('ddd, MMM DD YYYY')}</DateText>
        </RightLineView>
      </LineView>
    );
  }

  renderMeal(mealString) {
    if (mealString) return (
      <LineViewCenter>
        <LeftLineView>
          <LimiterCenter>
            <IconView><ImageProperty resizeMode='contain' source={food} /></IconView>
            <VerticalLine />
          </LimiterCenter>
        </LeftLineView>
        <RightLineView>
          <MealText type='medium'>Makan {mealString}</MealText>
        </RightLineView>
      </LineViewCenter>
    );
  }

  renderFlights(flights) {
    return (
      <LineView>
        <LeftLineView>
          <LimiterCenter>
            <IconView><ImageProperty resizeMode='contain' source={airplane} /></IconView>
            <VerticalLine />
          </LimiterCenter>
        </LeftLineView>
        <RightLineView>
          {this.renderModalTouchable('Penerbangan', flights.length, 'Maskapai', () => this.openModal('ModalTourFlight', flights))}
        </RightLineView>
      </LineView>
    );
  }

  renderPlaces(coordinates, places, dayNumber, pictures) {
    return (
      <LineView>
        <LeftLineView>
          <LimiterCenter>
            <IconView><ImageProperty resizeMode='contain' source={map} /></IconView>
            <VerticalLine />
          </LimiterCenter>
        </LeftLineView>
        <RightLineView>
          <LineView style={{ flexWrap: 'wrap' }}>
            {places.length > 0 && this.renderModalTouchable('Destinasi', places.length, 'Tempat wisata', () => this.openModal('ModalTourPlace', { coordinates, places, dayNumber }))}
            {pictures.length > 0 && this.renderModalTouchable('Galeri', pictures.length, 'Foto', () => this.openModal('ModalPhotoViewer', pictures))}
          </LineView>
        </RightLineView>
      </LineView>
    );
  }

  renderDescription(description) {
    if (description) return (
      <LineView>
        <LeftLineView>
          <LimiterCenter>
            <VerticalLine />
          </LimiterCenter>
        </LeftLineView>
        <RightLineView>
          <DescriptionView>
            <DescriptionText>{description}</DescriptionText>
          </DescriptionView>
        </RightLineView>
      </LineView>
    );
  }

  renderDays() {
    const { itineraries, date } = this.props.data;
    const firstIndex = 0;

    return itineraries.map((itinerary, i) => {
      const { dayNumber, title, mealString, flights, coordinates, places, pictures, description } = itinerary;
      return (
        <View key={i}>
          {this.renderHeaderDays(i === firstIndex, dayNumber)}
          {this.renderSubHeaderDays(title, Moment(date).add(i, 'days'))}
          {this.renderMeal(mealString)}
          {flights.length > 0 && this.renderFlights(flights)}
          {(places.length > 0 || pictures.length > 0) && this.renderPlaces(coordinates, places, dayNumber, pictures)}
          {this.renderDescription(description)}
        </View>
      );
    });
  }

  render() {
    const { modalVisible } = this.state;
    return (
      <MainView>
        {this.renderDays()}
        <Modal visible={modalVisible !== null} animationType='slide' onRequestClose={() => this.closeModal()}>
          {this.renderModal()}
        </Modal>
      </MainView>
    );
  }

}
