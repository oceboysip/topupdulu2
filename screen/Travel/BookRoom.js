import React, { Component } from 'react';
import { View, ScrollView, Image, TouchableOpacity as NativeTouchable } from 'react-native';
import Styled from 'styled-components';

import Text from '../Text';
import Button from '../Button';
import Header from '../Header';
import FormatMoney from '../FormatMoney';
import BookRoomCard from './CardBookRoom';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    flexDirection: column;
    backgroundColor: #F4F4F4;
`;

const CardBookRoom = Styled(BookRoomCard)`
    marginBottom: 12;
`;

const DetailInfoMainView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    padding: 12px 16px 12px 16px;
    backgroundColor: #FFFFFF;
`;

const LowerDetailInfoView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    padding: 8px 0px 0px 0px;
`;

const BottomOrderView = Styled(View)`
    width: 100%;
    height: 63;
    alignItems: center;
    justifyContent: space-between;
    flexDirection: row;
    paddingHorizontal: 16;
    borderWidth: 0;
    elevation: 5;
    backgroundColor: #FFFFFF;
`;

const UpperDetailInfoView = Styled(LowerDetailInfoView)`
    borderBottomWidth: 0.5;
    borderColor: #231F20;
    padding: 0px 0px 8px 0px;
`;

const IncludeVisaView = Styled(NativeTouchable)`
    minWidth: 1;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
`;

const LineView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
`;

const VerticalView = Styled(View)`
    flexDirection: column;
`;

const NormalText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
    lineHeight: 28;
`;

const GrandTotalText = Styled(Text)`
    textAlign: left;
`;

const RedPriceText = Styled(GrandTotalText)`
    color: #FF425E;
`;

const CheckboxImage = Styled(Image)`
    width: 20;
    height: 20;
    marginRight: 4;
`;

const CustomButton = Styled(Button)`
    width: 140;
    height: 45;
`;

const checked = require('../../images/checkbox-selected.png');
const checkoff = require('../../images/checkbox-unselected.png');

const typeBeds = [
  { code: 'adultSingle', label: 'Adult (Single)' },
  { code: 'adultTwinSharing', label: 'Adult (Twin Sharing)' },
  { code: 'childTwinSharing', label: 'Child (Twin Sharing)' },
  { code: 'childExtraBed', label: 'Child (Extra Bed)' },
  { code: 'childNoBed', label: 'Child (No Bed)' }
];

export default class BookRoom extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    this.state = {
      anyManagedVisa: this.getAnyManagedVisa(),
      extraBedAvailable: props.navigation.state.params.tourDetail.departure.price.childExtraBedPrice.finalPrice !== 0,
      guests: { adult: 1, child: 0 },
      totalBeds: { adultSingle: 1 },
      totalGuests: 1,
      includeVisa: false,
      rooms: [{
        lockExtraBed: true,
        extraBed: false,
        guests: { adult: 1, child: 0 },
        beds: { adultSingle: 1 }
      }]
    };
  }

  onBookTourOverview(tourDetail, departureId, tourId, totalBeds, guests, grandTotal, allBedsPrice, totalVat, totalTaxFuel, totalVisaPrice, includeVisa) {
    this.props.navigation.navigate('BookOverview', { tourDetail, departureId, tourId, totalBeds, guests, grandTotal, allBedsPrice, totalVat, totalTaxFuel, totalVisaPrice, includeVisa, rooms: this.state.rooms });
  }

  getAnyManagedVisa() {
    for (const visa of this.props.navigation.state.params.tourDetail.visas) if (visa.managed) return true;
    return false;
  }

  getSumFromKey(rooms, key) {
    const tempArray = [];
    for (const room of rooms) tempArray.push(room[key]);
    return tempArray;
  }

  getSumAllProps(obj) {
    return Object.values(obj).reduce((a, b) => a + b);
  }

  sumObjectsByKey(objs) {
    return objs.reduce((obj1, obj2) => {
      const newObj = obj1;
      Object.keys(obj2).forEach(key => { if (Object.prototype.hasOwnProperty.call(obj2, key)) newObj[key] = (obj1[key] || 0) + obj2[key]; });
      return newObj;
    }, {});
  }

  calculateAllUpdates(rooms) {
    const totalBeds = this.sumObjectsByKey(this.getSumFromKey(rooms, 'beds'));
    const guests = this.sumObjectsByKey(this.getSumFromKey(rooms, 'guests'));
    const totalGuests = this.getSumAllProps(guests);
    this.setState({ totalBeds, totalGuests, rooms, guests });
  }

  calculateBeds(room) {
    const { extraBedAvailable } = this.state;
    const { guests, extraBed, lockExtraBed } = room;
    const { adult, child } = guests;
    const tempBedsObject = { adultTwinSharing: adult };
    if (adult === 1) {
      if (child === 0) return { adultSingle: 1 };
      tempBedsObject.childTwinSharing = 1;
    }
    if (adult !== 3) {
      if (extraBed) {
        tempBedsObject.childExtraBed = 1;
        if (lockExtraBed) tempBedsObject.childNoBed = 1;
      }
      else if (!lockExtraBed || (!extraBedAvailable && adult + child === 3)) tempBedsObject.childNoBed = 1;
    }
    return tempBedsObject;
  }

  toogleExtraBed(i, bool, calculate) {
    const { rooms } = this.state;
    rooms[i].extraBed = bool;
    if (calculate) {
      rooms[i].beds = this.calculateBeds(rooms[i]);
      this.calculateAllUpdates(rooms);
    }
    else this.setState({ rooms });
  }

  ruleExtraBed(i, bool) {
    const { rooms } = this.state;
    rooms[i].lockExtraBed = bool;
    this.setState({ rooms });
  }

  updateRoom(type, n, i) {
    const { rooms } = this.state;
    rooms[i].guests[type] += n;
    rooms[i].beds = this.calculateBeds(rooms[i]);
    this.calculateAllUpdates(rooms);
  }

  addRoom() {
    const { rooms } = this.state;
    rooms.push({
      lockExtraBed: true,
      extraBed: false,
      guests: { adult: 1, child: 0 },
      beds: { adultSingle: 1 }
    });
    this.calculateAllUpdates(rooms);
  }

  removeRoom(i) {
    const { rooms } = this.state;
    rooms.splice(i, 1);
    this.calculateAllUpdates(rooms);
  }

  renderTourRooms() {
    const { rooms, extraBedAvailable } = this.state;
    const lastIndex = rooms.length - 1;
    return rooms.map((room, i) =>
      <CardBookRoom
        key={i}
        index={i}
        data={room}
        extraBedAvailable={extraBedAvailable}
        removable={rooms.length > 1}
        canAddRoom={i === lastIndex}
        remove={(x) => this.removeRoom(x)}
        update={(type, n, x) => this.updateRoom(type, n, x)}
        ruleExtraBed={(x, bool) => this.ruleExtraBed(x, bool)}
        toogleExtraBed={(x, bool, calculate) => this.toogleExtraBed(x, bool, calculate)}
        addRoom={() => this.addRoom()}
      />
    );
  }

  renderUpperDetails(allBedsPrice, price) {
    const { totalBeds } = this.state;
    return typeBeds.map((type, i) => {
      if (allBedsPrice[type.code] >= 0) {
        return (
          <View key={i}>
            <LineView>
              <NormalText type='medium'>{type.label}:</NormalText>
              <NormalText type='medium'>{FormatMoney.getFormattedMoney(allBedsPrice[type.code])}</NormalText>
            </LineView>
            {totalBeds[type.code] > 1 && <LineView>
              <NormalText type='medium'>{totalBeds[type.code]} @ {FormatMoney.getFormattedMoney(price[`${type.code}Price`].finalPrice)}</NormalText>
            </LineView>}
          </View>
        );
      }
      return null;
    });
  }

  renderLowerDetail(subtotal, totalGuests, totalVat, totalTaxFuel) {
    const { vatPercent, airportTaxAndFuelPrice } = this.props.navigation.state.params.tourDetail.departure.price;
    const subDetails = [
      { label: 'Subtotal', value: FormatMoney.getFormattedMoney(subtotal) },
      { label: `Ppn${vatPercent !== 0 ? ` ${vatPercent} %` : ''}`, value: vatPercent !== 0 ? FormatMoney.getFormattedMoney(totalVat) : 'Sudah Termasuk' },
      { label: `Airport Tax & Fuel x ${totalGuests}`, value: airportTaxAndFuelPrice !== 0 ? FormatMoney.getFormattedMoney(totalTaxFuel) : 'Sudah Termasuk' },
    ];
    return subDetails.map((detail, i) =>
      <LineView key={i}>
        <NormalText type='medium'>{detail.label}:</NormalText>
        <NormalText type='medium'>{detail.value}</NormalText>
      </LineView>
    );
  }

  renderDetails(allBedsPrice, totalBeds, totalGuests, price, subtotal, visas, visaPrice, includeVisa, totalVat, totalTaxFuel, totalVisaPrice) {
    return (
      <DetailInfoMainView>
        <UpperDetailInfoView>
          {this.renderUpperDetails(allBedsPrice, price)}
        </UpperDetailInfoView>
        <LowerDetailInfoView>
          {this.renderLowerDetail(subtotal, totalGuests, totalVat, totalTaxFuel)}
          {this.state.anyManagedVisa && <LineView>
            <IncludeVisaView activeOpacity={1} onPress={() => this.setState({ includeVisa: !includeVisa })}>
              <CheckboxImage resizeMode='contain' source={includeVisa ? checked : checkoff} />
              <NormalText type='medium'>Termasuk Visa x {totalGuests}:</NormalText>
            </IncludeVisaView>
            <NormalText type='medium'>{FormatMoney.getFormattedMoney(totalVisaPrice)}</NormalText>
          </LineView>}
        </LowerDetailInfoView>
      </DetailInfoMainView>
    );
  }

  render() {
    const { tourDetail, departureId, tourId } = this.props.navigation.state.params;
    const { includeVisa, totalBeds, guests, totalGuests } = this.state;
    const { visas, departure } = tourDetail;
    const { price } = departure;
    const { visaPrice, vatPercent, airportTaxAndFuelPrice } = price;
    const allBedsPrice = {};
    for (const type of typeBeds) if (totalBeds[type.code]) allBedsPrice[type.code] = totalBeds[type.code] * price[`${type.code}Price`].finalPrice;
    const subtotal = this.getSumAllProps(allBedsPrice);
    const totalVat = (subtotal * vatPercent) / 100;
    const totalTaxFuel = totalGuests * airportTaxAndFuelPrice;
    const totalVisaPrice = totalGuests * visaPrice;
    const grandTotal = subtotal + totalVat + totalTaxFuel + (includeVisa && totalVisaPrice);

    return (
      <MainView>
        <Header title='Jumlah Peserta' />
        <ScrollView>
          {this.renderTourRooms()}
          {this.renderDetails(allBedsPrice, totalBeds, totalGuests, price, subtotal, visas, visaPrice, includeVisa, totalVat, totalTaxFuel, totalVisaPrice)}
        </ScrollView>
        <BottomOrderView>
          <VerticalView>
            <GrandTotalText type='bold'>Grand Total</GrandTotalText>
            <RedPriceText type='bold'>{FormatMoney.getFormattedMoney(grandTotal)}</RedPriceText>
          </VerticalView>
          <CustomButton onPress={() => this.onBookTourOverview(tourDetail, departureId, tourId, totalBeds, guests, grandTotal, allBedsPrice, totalVat, totalTaxFuel, totalVisaPrice, includeVisa)}>Pesan Travel</CustomButton>
        </BottomOrderView>
      </MainView>
    );
  }
}
