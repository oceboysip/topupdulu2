import React, { Component } from 'react';
import { View, SafeAreaView, Modal, FlatList, TextInput } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { TextInputMask } from 'react-native-masked-text';
import gql from 'graphql-tag';
import Styled from 'styled-components';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { connect } from 'react-redux';

import Text from '../Text';
import Button from '../Button';
import Color from '../Color';
import Header from '../Header';
import TouchableOpacity from '../Button/TouchableDebounce';
import Client from '../../state/apollo';
import FormatMoney from '../FormatMoney';
import ModalIndicator from '../Modal/ModalIndicator';
import ModalInformation from '../Modal/ModalInformation';
import { pdamPascaBayar } from '../../state/actions/PPOB/pdam';
import ModalPdamList from '../Modal/PdamList';

const TextWhite = Styled(Text)`
  color: #FFFFFF;
`;

const TextTheme = Styled(Text)`
  color: ${Color.theme};
`;

const queryInquiryPascaBayarPDAM = gql `
  query(
    $area_pelayanan: PDAM_AREA_CODE
    $nomor_pelanggan: String!
  ){
    Inquiry_PascaBayar_PDAM(
      area_pelayanan: $area_pelayanan
      nomor_pelanggan: $nomor_pelanggan
    ) {
      tr_id
      code
      hp
      tr_name
      period
      nominal
      admin
      ref_id
      response_code
      message
      price
      selling_price
      desc{
        bill_quantity
        address
        biller_admin
        pdam_name
        stamp_dut
        due_date
        kode_tarif
        bill{
          detail{
            period
            first_meter
            last_meter
            penalty
            bill_amount
            misc_amount
            stand
          }
        }
      }
    }
  }
`;

const queryGetListPdamArea = gql`
  {
    GET_LIST_PDAM_AREA
  }
`;

const iconReport = require('../../images/icon_paperWhite.png');

class PdamScreen extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    this.state = {
      inquiryPascabayar: null,
      serviceArea: '',
      pdamNumber: '',
      listPdamArea: [],
      loadingListPdam: true,
      modalPdamArea: false,
      loadingPascabayar: false,
      message: '',
      messageRes: '',
      loading: false,
      modalFail: false
    };
  };

  componentDidMount() {
    this.getListPdamArea();
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.fetching && nextProps.fetching) {
      this.openModal('loading');
    }else if (this.props.fetching && !nextProps.fetching) {
      this.closeModal('loading');
      if (nextProps.error === null) {
        this.props.navigation.navigate('PaymentScreen');
      }else {
        this.setState({ modalFail: true }, () => setTimeout(() => {
          this.setState({ modalFail: false })
        }, 3000));
        console.log(nextProps.error);
      }
    }
  }

  getListPdamArea() {
    Client.query({
      query: queryGetListPdamArea
    })
    .then(res => {
      console.log(res, 'res pdam list area');
      
      if (res.data.GET_LIST_PDAM_AREA) this.setState({ listPdamArea: res.data.GET_LIST_PDAM_AREA });
      else this.setState({ listPdamArea: [] });

      this.setState({ loadingListPdam: false });
    })
    .catch(err => {
      console.log(err, 'err');
      this.setState({ listPdamArea: [], loadingListPdam: false });
    })
  }

  getInquiryPdamPascabayar(nomor_pelanggan) {
    if (nomor_pelanggan.length > 5 && this.state.serviceArea != '') {
      this.setState({ loadingPascabayar: true });

      let variables = {
        area_pelayanan: this.state.serviceArea,
        nomor_pelanggan
      }
  
      Client.query({
        query: queryInquiryPascaBayarPDAM,
        variables
      })
      .then(res => {
        let data = res.data.Inquiry_PascaBayar_PDAM;

        if(data) {
          if (data.tr_id) this.setState({ inquiryPascabayar: data, message: '' });
          else this.setState({ inquiryPascabayar: null, message: data.message });
        }
      })
      .catch(err => {
        console.log(err, 'err');
        this.setState({ inquiryPascabayar: null });
      })

      this.setState({ loadingPascabayar: false });
    } else {
      this.setState({ inquiryPascabayar: null });
    }
  }

  timeoutModal(modal) {
    this.setState({
      [modal]: true
    }, () => {
      let timeout = setTimeout(() => {
        clearTimeout(timeout);
        this.setState({ [modal]: false });
      }, 2000);
    })
  }

  timeoutMessage(state, value) {
    this.setState({
      [state]: value
    }, () => {
      let timeout = setTimeout(() => {
        clearTimeout(timeout);
        this.setState({ [state]: '' });
      }, 2000);
    })
  }

  openModal(modal) {
      this.setState({ [modal]: true });
  }

  closeModal(modal) {
      this.setState({ [modal]: false });
  }

  submit(NomorPelanggan, Kode_Area) {
    const { inquiryPascabayar } = this.state;

    if (NomorPelanggan === ''){
      this.timeoutModal('modalFail');
      this.timeoutMessage('messageRes', 'Mohon isi terlebih dahulu nomor pelanggan');
      return;
    } else if (Kode_Area === '') {
      this.timeoutModal('modalFail');
      this.timeoutMessage('messageRes', 'Mohon pilih kode area PDAM');
      return;
    } else if (inquiryPascabayar === null) {
      this.timeoutModal('modalFail');
      this.timeoutMessage('messageRes', 'Silakan masukan data dengan benar');
      return;
    }

    const body = {
      NomorPelanggan,
      Kode_Area,
      finalAmount: inquiryPascabayar.price
    };

    this.props.orderPdamPascaBayar(body);
  }

  openOrderBooking(title) {
    this.props.navigation.navigate('OrderBooking', { title });
  }

  renderBillDetail(inquiryPascabayar) {
    if (inquiryPascabayar) return (
      <View style={{backgroundColor: Color.theme, marginTop: 16, paddingHorizontal: 16, paddingVertical: 16}}>
        <TextWhite align= 'left' type='bold'>Informasi Tagihan</TextWhite>
        
        <View style={{flexDirection: 'row', marginTop: 16, borderTopWidth: 0.5, borderTopColor: '#0000000D', flexDirection: 'row', justifyContent: 'space-between'}}>
          <View style={{ width: '50%'}}>
            <TextWhite align={'left'} type='medium'>Nama Pelanggan</TextWhite>
            <TextWhite align={'left'} type='medium' style={{marginTop: 3}}>Periode</TextWhite>
            <TextWhite align={'left'} type='medium' style={{marginTop: 3}}>Biaya Admin</TextWhite>
            <TextWhite align={'left'} type='medium' style={{marginTop: 3}}>Harga</TextWhite>
          </View>

          <View style={{width: '50%', alignItems: 'flex-end'}}>
            <TextWhite align={'left'} type='medium'>{inquiryPascabayar.tr_name}</TextWhite>
            <TextWhite align={'left'} type='medium' style={{marginTop: 3}}>{inquiryPascabayar.desc.due_date}</TextWhite>
            <TextWhite align={'left'} type='medium' style={{marginTop: 3}}>{FormatMoney.getFormattedMoney(inquiryPascabayar.admin)}</TextWhite>
            <TextWhite align={'left'} type='medium' style={{marginTop: 3}}>{FormatMoney.getFormattedMoney(inquiryPascabayar.nominal)}</TextWhite>
          </View>
        </View>
      </View>
    )
    else return (
      <View style={{flexDirection: 'row', marginTop: 13, borderTopWidth: 0.5, borderTopColor: '#0000000D', flexDirection: 'row', justifyContent: 'center'}}>
        <Text>{this.state.message}</Text>
      </View>
    );
  }

  render() {
    const { pdamNumber, serviceArea, inquiryPascabayar, modalPdamArea, listPdamArea } = this.state;

    console.log(this.state, 'state pdam');

    return(
      <View style={{width: '100%', height: '100%', backgroundColor: '#FAFAFA'}}>
        <Header title='PDAM' imageRightButton={iconReport} onPressRightButton={() => this.openOrderBooking('PdamPascabayar')} />
        <View style={{paddingHorizontal: 16, marginTop: 16}}>
          <TextTheme type='medium' align='left'>Area</TextTheme>
          <TouchableOpacity
            activeOpacity={0.5}
            onPress={() => this.openModal('modalPdamArea')}
            style={{flexDirection: 'row', alignItems: 'center', width: '100%', justifyContent: 'space-between', marginBottom: 20, marginTop: 16, paddingBottom: 4, borderBottomWidth: 2, borderBottomColor: Color.primary}}
          >
            <TextTheme type='bold'>{serviceArea == '' ? 'Pilih Area' : serviceArea}</TextTheme>
            <MaterialIcons style={{fontSize: 20, color: Color.primary}} name='keyboard-arrow-right' />
          </TouchableOpacity>

          <TextTheme align='left'>ID Pelanggan</TextTheme>
          <TextInputMask
            name="text"
            placeholder='Masukan ID Pelanggan'
            returnKeyType="done"
            returnKeyLabel="Done"
            type={'only-numbers'}
            value={this.state.pdamNumber}
            onChangeText={(text) => this.setState({ pdamNumber: text }, () => this.getInquiryPdamPascabayar(text) )}
            blurOnSubmit={false}
            error={null}
            style={{width: '100%', height: 45, alignContent: 'flex-start', letterSpacing: 0.36, borderBottomWidth: 2, borderColor: Color.primary, marginTop: 8, fontWeight: 'bold', color: Color.theme}}
          />
          
          {this.renderBillDetail(inquiryPascabayar)}
        </View>

        {inquiryPascabayar && <View style={{flexDirection: 'row', width: '100%', height: 63, position: 'absolute',bottom: 0, backgroundColor: '#FFFFFF'}}>
          <View style={{ width: '50%'}}>
            <TextTheme align={'left'} type='medium' style={{paddingHorizontal: 16, marginTop: 8}}>Total Tagihan</TextTheme>
            <Text align={'left'} type='bold' style={{color: '#FF425E', paddingHorizontal: 16, marginTop: 3}}>{inquiryPascabayar ? FormatMoney.getFormattedMoney(inquiryPascabayar.price) : 'Rp 0'}</Text>
          </View>

          <View style={{ width: '50%', alignItems: 'flex-end', justifyContent: 'center', paddingHorizontal: 16}}>
            <Button style={{width: '100%', height: 45}} onPress={() => this.submit(pdamNumber, serviceArea)}>
              <Text style={{color: '#FFFFFF'}}>Bayar</Text>
            </Button>
          </View>
        </View>}

        <Modal
          onRequestClose={() => this.closeModal('modalPdamArea')}
          animationType="fade"
          visible={modalPdamArea}
        >
          <ModalPdamList listPdamArea={listPdamArea} onSelectedArea={(serviceArea) => this.setState({ serviceArea, modalPdamArea: false })} closeModal={() => this.closeModal('modalPdamArea')} />
        </Modal>

        <ModalIndicator
          visible={this.state.loading}
          type="large"
          message="Harap tunggu, kami sedang memproses pesanan Anda"
        />

        <ModalIndicator
          visible={this.state.loadingListPdam}
          type="large"
          message="Harap tunggu"
        />

        <Modal
          onRequestClose={() => {}}
          animationType="fade"
          transparent={true}
          visible={this.state.modalFail}
        >
          <ModalInformation
            label={this.state.messageRes}
            error={false}
          />
        </Modal>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user: state['user.auth'].login.user,
    fetching: state['booking'].fetching,
    error: state['booking'].error
  }
}

const mapDispatchToProps = (dispatch) => (
  {
    orderPdamPascaBayar: (data) => {
      dispatch(pdamPascaBayar(data))
    }
  }
)

export default connect(mapStateToProps, mapDispatchToProps)(PdamScreen)