import React, { Component } from 'react';
import { View, Image, Platform } from 'react-native';
import Styled from  'styled-components';

import Text from '../Text';
import Color from '../Color';
import Header from '../Header';
import TouchableOpacity from '../Button/TouchableDebounce';

const PaymentButton = Styled(TouchableOpacity)`
  width: 33.33%;
  height: 95;
  backgroundColor: #FFFFFF;
  alignItems: center;
  justifyContent : center;
`;

const PaymentImage = Styled(Image)`
  height: 100%;
  width: 45%;
  marginBottom: 6;
`;

const pulsa = require('../../images/PPOB_PULSA.png');
const paketData = require('../../images/PPOB_PAKET_DATA.png');
const voucherGame = require('../../images/PPOB_VOUCHER_GAME.png');
const plnPra = require('../../images/PPOB_LISTRIK_PRABAYAR.png');
const plnPas = require('../../images/PPOB_LISTRIK_PASCABAYAR.png');
const bpjs = require('../../images/PPOB_BPJS.png');
const pdam = require('../../images/PPOB_PDAM.png');
const telepon = require('../../images/PPOB_TELKOM_TELPON.png');
const multifinance = require('../../images/PPOB_MULTIFINANCE.png');
const inet = require('../../images/PPOB_INTERNET.png');
const tvSubscriber = require('../../images/PPOB_TV_BERLANGGANAN.png');

const MainMenu = [
  { label : 'Pulsa', image: pulsa, nav: 'PulsaScreen' },
  { label : 'Paket Data', image: paketData, nav: 'PaketDataScreen' },
  { label : 'Voucher Game', image: voucherGame, nav: 'VoucherGameScreen' },
  { label : 'Listrik Prabayar', image: plnPra, nav: 'PlnScreen', params: { activeTab: 0 } },
  { label : 'Listrik Pascabayar', image: plnPas, nav: 'PlnScreen', params: { activeTab: 1 } },
  { label : 'PDAM', image: pdam, nav: 'PdamScreen' },
  { label : 'Telepon', image: telepon, nav: 'TeleponScreen' },
  { label : 'BPJS', image: bpjs, nav: 'BpjsScreen', comingsoon: true },
  { label : 'Multifinance', image: multifinance, nav: '', comingsoon: true },
  { label : 'Internet', image: inet, nav: '', comingsoon: true },
  { label : 'TV Berlangganan', image: tvSubscriber, nav: '', comingsoon: true }
];

export default class PPOBScreen extends Component {
  static navigationOptions = { header: null };

  renderCardContentIOS(m, idx) {
    if (!m.comingsoon) return this.renderCardContent(m, idx);
  }

  navigate(nav, params) {
    this.props.navigation.navigate(nav, params);
  }

  renderCardContent(m, idx) {
    return (
      <PaymentButton
        key={idx}
        activeOpacity={1}
        disabled={m.comingsoon}
        onPress={() => this.navigate(m.nav, m.params)}
      >
        <View style={{width: '100%', height: '45%', justifyContent: 'center', alignItems: 'center'}}>
          <PaymentImage style={m.comingsoon && {opacity: 0.2}} resizeMode='contain' source={m.image} />

          <View style={{position: 'absolute'}}>
            {m.comingsoon && <Text size={8} style={{backgroundColor: '#000000', borderColor: '#FFFFFF', color: '#FFFFFF', borderWidth: 0.5, paddingHorizontal: 8}}>COMING SOON</Text>}
          </View>
        </View>
        <Text size={12} type='medium' style={[m.comingsoon && {opacity: 0.2}, {color: Color.theme}]}>{m.label}</Text>
      </PaymentButton>
    )
  }

  render() {
    return(
      <View style= {{width: '100%', height: '100%', backgroundColor: '#FAFAFA'}}>
        <Header title='PPOB' />
        <View style={{width: '100%', paddingHorizontal: 16, paddingTop: 16, backgroundColor: '#F9F9F9'}}>
          <View style={{borderWidth: 0.5, borderColor: '#DDDDDD', flexDirection: 'row', flexWrap: 'wrap', backgroundColor: '#FFFFFF', paddingVertical: 16, borderRadius: 20, shadowOpacity: 0.1}}>
            {MainMenu.map((menu, idx) =>
              Platform.OS === 'ios' ? this.renderCardContentIOS(menu, idx) : this.renderCardContent(menu, idx)
            )}
          </View>
        </View>
      </View>
    )
  }
}
