import React, { Component } from "react";
import { StyleSheet, View, Image, TouchableOpacity, ScrollView, SafeAreaView, Modal, Dimensions } from 'react-native';
import RadioGroup from 'react-native-radio-buttons-group';
import gql from 'graphql-tag';
import { TextInputMask } from 'react-native-masked-text';
import Styled from 'styled-components';

import PulsaStyle from '../../style/PulsaStyle';
import Client from '../../state/apollo';
import FormatMoney from '../FormatMoney';
import Header from '../Header';
import Color from '../Color';
import Text from '../Text';

import ModalPhoneContact from '../Modal/ModalPhoneContact';

const TextInput = Styled(TextInputMask)`
  width: 100%;
  height: 45;
  alignContent: flex-start;
  letterSpacing: 0.36;
  marginTop: 5;
  paddingHorizontal: 10;
`;

const getQueryPulsaList = gql`
  query(
      $mobileNumber: String
  ){
    mobilePulsaCheckOperator(
      mobileNumber: $mobileNumber
    )
  }
`;

const getQuerymobilePulsaPrepaidPriceList = gql`
  query(
      $param: RequestServiceType!
  ){
    mobilePulsaPrepaidPriceList(
      param: $param
    ){
      pulsa_code
      pulsa_op
      pulsa_nominal
      pulsa_price
      pulsa_type
      masaaktif
      status
    }
  }
`;

const opTsel = require('../../images/operator_tsel.png');
const opIsat = require('../../images/operator_indosat.png');
const opThree = require('../../images/operator_three.png');
const opXl = require('../../images/operator_xl.png');
const opSmart = require('../../images/operator_smart.png');
const opAxis = require('../../images/operator_axis.png');

export default class PulsaScreen extends Component {
  constructor(props){
    super(props)
    this.state = {
      radioButtons: [
        {
          label: 'Pulsa',
          color: Color.theme,
          value: 'pulsa'
        },
        {
          label: 'Paket Data',
          color: Color.theme,
          value: 'data'
        }
      ],
      selectedRadio: 0,

      modalPulsa: false,
      modalContact: false,
      mobilePulsaCheckOperator: '',
      mobilePulsaPrepaidPriceList: [],
      pulsa_nominal: '',
      pulsa_price: 0,
      pulsa_code: '',
      loading: false,
      modalFail: false,
      phoneNumber: '',
    };

    this.renderInner = this.renderInner.bind(this);
  }

  openModal(modal){
    this.setState({ [modal]: true });
  };

  closeModal(modal){
    this.setState({ [modal]: false });
  };

  openOrderBooking() {
    this.props.navigation.navigate('OrderBooking', {
      title: 'Pulsa'
    })
  }

  getQueryPulsaList(mobileNumber) {
    if (mobileNumber.length > 8) {
      let variables = {
        mobileNumber
      }

      Client.query({
        query: getQueryPulsaList,
        variables
      }).then(res => {
        let data = res.data.mobilePulsaCheckOperator;
        if (data) {
          this.setState({ mobilePulsaCheckOperator: data });
          this.getQuerymobilePulsaPrepaidPriceList();
        }else {
          this.setState({ mobilePulsaCheckOperator: '' });
        }
      }).catch((err) => {
        console.log('Error', err);
      })
    }else {
      this.setState({
        mobilePulsaPrepaidPriceList: [],
        mobilePulsaCheckOperator: '',
        pulsa_nominal: '',
        pulsa_price: 0,
        pulsa_code: ''
      });
    }
  };

  getQuerymobilePulsaPrepaidPriceList() {
    let variables = {
      param: {
        SegmentType: 'PULSA_HP',
        ServiceItem: {
          OperatorItemProvided: {
            Operator_Pulsa: this.state.mobilePulsaCheckOperator
          }
        }
      }
    }

    Client.query({
      query: getQuerymobilePulsaPrepaidPriceList,
      variables
    }).then(res => {
      let data = res.data.mobilePulsaPrepaidPriceList;
      console.log(data, 'pulsa list');
      let mobilePulsaPrepaidPriceList = []

      if (data.length > 0) {
        for (var i = 0; i < data.length; i++) {
          if (data[i] !== null) mobilePulsaPrepaidPriceList.push(data[i])
        }
      }

      this.setState({ mobilePulsaPrepaidPriceList: mobilePulsaPrepaidPriceList.sort(this.compare) });
    })
  }

  compare(a, b) {
    let nameA = a.pulsa_price, nameB = b.pulsa_price;
  
    let comparison = 0;

    if (nameA > nameB) {
      comparison = 1;
    } else if (nameA < nameB) {
      comparison = -1;
    }
    return comparison;
  }

  ReviewPulsa = () => {
    const { phoneNumber, pulsa_price, mobilePulsaCheckOperator, pulsa_nominal, pulsa_code } = this.state;
    if (phoneNumber === '') {
      alert('Nomor telepon harus diisi!');
      return;
    }
    if (pulsa_price === 0) {
      alert('Silakan pilih nominal pulsa terlebih dulu!');
      return;
    }
    if (mobilePulsaCheckOperator === '' || mobilePulsaCheckOperator === 'unkown prefix') {
      alert('Operator tidak dikenal');
      return;
    }
    this.props.navigation.navigate('ReviewPulsa', {
      number: phoneNumber,
      price: pulsa_price,
      nominal: pulsa_nominal,
      pulsa_code: pulsa_code,
      operator: mobilePulsaCheckOperator,
      product: 'Pulsa'
    });
  }

  onSelectRadioButton(radioButtons) {
    let selectedRadio = 0;
    radioButtons.map((item, idx) => {
      console.log(item);
      
      if (item.selected) selectedRadio = idx;
    })

    this.setState({ radioButtons, selectedRadio });
  }

  onSelectPress(pulsa_nominal,pulsa_price, pulsa_code) {
    this.setState({ pulsa_nominal, pulsa_price, pulsa_code }, () => {
      this.closeModal('modalPulsa');
    });
  }

  selectedPhoneNumber(n) {
    let phoneNumber = n.replace('62', '0');

    this.setState({ phoneNumber });
    this.closeModal('modalContact');
    this.getQueryPulsaList(phoneNumber);
  }

  nominal(num) {
    const nominal = FormatMoney.getFormattedMoney(parseInt(num));
    return nominal.substring(2, nominal.length);
  }

  renderInner(getHeight) {
    const { mobilePulsaPrepaidPriceList, mobilePulsaCheckOperator } = this.state;
    let height = getHeight - 154;
    
    return (
      <SafeAreaView style={{backgroundColor: Color.theme, flex: 1}}>
        <Header title='Pilih Nominal Pulsa' onPressLeftButton={() => this.closeModal('modalPulsa')} />
        <ScrollView style={{height, paddingHorizontal: 16, paddingTop: 16, backgroundColor: '#FFFFFF'}}>
          {mobilePulsaPrepaidPriceList.length > 0 ?
            <View>
              {mobilePulsaPrepaidPriceList.map((item, idx) => 
                <TouchableOpacity key={idx} onPress={() => this.onSelectPress(item.pulsa_nominal, item.pulsa_price, item.pulsa_code)}>
                  <View style={styles.rowPanelDirect}>
                    <View style={[styles.flexRow, {alignItems: 'flex-start'}]}>
                      <Text style={styles.greyBig}>{mobilePulsaCheckOperator} {this.nominal(item.pulsa_nominal)}</Text>
                      <Text style={[styles.greySmall, {color: 'red'}]}>{FormatMoney.getFormattedMoney(item.pulsa_price)}</Text>
                    </View>
                    <View>
                      <Text size={12} type='bold'>Masa aktif</Text>
                      <Text size={12}>{item.masaaktif} hari</Text>
                    </View>
                  </View>
                  <View style={styles.rowPanelDirect}>  
                    <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}} />
                  </View>
                </TouchableOpacity>
              )}
            </View>
          :
            <View style={{height, justifyContent: 'center', alignItems: 'center'}}>
              <Text>Pulsa tidak tersedia</Text>
              <TouchableOpacity onPress={() => this.closeModal('modalPulsa')} style={{height: 40, backgroundColor: Color.theme, marginTop: 8, justifyContent: 'center', paddingHorizontal: 16, borderRadius: 20}}>
                <Text style={styles.whiteText}>Kembali</Text>
              </TouchableOpacity>
            </View>
          }

        </ScrollView>
      </SafeAreaView>
    )
  }

  render() {
    const { height } = Dimensions.get('window');
    const { phoneNumber, mobilePulsaCheckOperator, pulsa_nominal, modalPulsa, modalContact, radioButtons, selectedRadio } = this.state;
    console.log(this.state, 'satee');

    let imageOperator = null;
    let resizeMode = 'contain';
    let width = 60;

    switch(mobilePulsaCheckOperator) {
      case 'TELKOMSEL': imageOperator = opTsel; break;
      case 'XL/AXIATA': imageOperator = opXl; resizeMode = 'stretch'; width = 20; break;
      case 'AXIS': imageOperator = opAxis; resizeMode = 'stretch'; width = 40; break;
      case 'SMARTFREN': imageOperator = opSmart; break;
      case 'THREE': imageOperator = opThree; resizeMode = 'stretch'; width = 20; break;
      case 'INDOSAT': imageOperator = opIsat; resizeMode = 'stretch'; width = 40; break;
    }
    
    return (
      <View style={styles.container}>
        <View style={styles.navTop}>
          <View style={styles.rowsBeetwen}>
            <View style={{flexDirection: 'row', alignItems:'center'}}>
              <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                <Image style={{width:30, height:30}} resizeMode='contain' source={require('../../images/left-arrow.png')}/>
              </TouchableOpacity>
              <Text style={styles.titlePage}>Pulsa</Text>
            </View>

            <View style={{flexDirection:'row'}}>
              <TouchableOpacity onPress={() => this.openOrderBooking()}>
                <Image style={{width:30, height:30}} resizeMode={'contain'} source={require('../../images/icon_paperWhite.png')}/>
               </TouchableOpacity>
            </View>
          </View>
        </View>

        <ScrollView>
          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                
                {/* <View style={styles.flexRows}>
                  <RadioGroup radioButtons={radioButtons} onPress={(data) => this.onSelectRadioButton(data)} flexDirection='row' />
                </View> */}

                {selectedRadio === 0 && <View>
                  <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingTop: 8}}>
                    <Text style={styles.smallText}>Nomor Ponsel</Text>
                    {mobilePulsaCheckOperator !== '' && <Image style={{width, height: 20}} resizeMode={resizeMode} source={imageOperator} />}
                  </View>
                  <View style={styles.flexRows}>
                    <View style={{width: '90%', height: 45}}>
                      <TextInput
                        name='phoneNumber'
                        placeholder="Isi Nomor Telepon"
                        value={phoneNumber}
                        onChangeText={(phoneNumber) => this.setState({ phoneNumber }, () => this.getQueryPulsaList(phoneNumber) )}
                        returnKeyType="done"
                        returnKeyLabel="Done"
                        type={'only-numbers'}
                        maxLength={13}
                        style={styles.inputText}
                      />
                    </View>
                    <View style={{width: '10%', height: 45, alignItems: 'flex-end'}}>
                      <TouchableOpacity onPress={() => this.openModal('modalContact')}>
                        <Image style={{width:25, resizeMode:'contain'}} source={require('../../images/icon_book.png')}/>
                      </TouchableOpacity>
                    </View>
                  </View>

                  <Text style={styles.smallText} align='left'>Nominal</Text>
                  <View style={[styles.flexRows, {marginTop: 10}]}>
                    <TouchableOpacity style={{justifyContent: 'space-between', flexDirection: 'row', width: '100%'}} onPress={() => this.openModal('modalPulsa')}>
                      <View style={{width: '90%', height: '100%'}}>
                        <Text style={styles.BigTextUnder} align='left'>{pulsa_nominal.length > 0 ? FormatMoney.getFormattedMoney(parseInt(pulsa_nominal)) : 'Pilih jumlah nominal'}</Text>
                      </View>
                      <View style={{width: '10%', height: '100%', alignItems: 'flex-end'}}>
                        <Image style={{width:25,resizeMode:'contain'}} source={require('../../images/arrow_down.png')}/>
                      </View>
                    </TouchableOpacity>
                  </View>

                </View>}

                {/* {selectedRadio === 1 && <View>
                  <Text>Coming soon</Text>
                </View>} */}

              </View>
            </View>
          </View>

          {selectedRadio === 0 ?
            <View style={styles.alCenter}>
              <TouchableOpacity style={styles.button} onPress={this.ReviewPulsa}>
                <Text style={styles.buttonText}>Lanjutkan Transaksi</Text>
              </TouchableOpacity>
            </View>
          :
            <View style={styles.alCenter}>
              {false && <TouchableOpacity style={styles.button} onPress={() => alert('coming soon!')}>
                <Text style={styles.buttonText}>Lanjutkan Transaksi</Text>
              </TouchableOpacity>}
            </View>
          }
        </ScrollView>

        <Modal
          visible={modalPulsa}
          onRequestClose={() => this.closeModal('modalPulsa')}
          animationType='slide'
        >
          {this.renderInner(height)}
        </Modal>

        <Modal
          visible={modalContact}
          onRequestClose={() => this.closeModal('modalContact')}
          animationType='slide'
        >
          <ModalPhoneContact
            onClose={() => this.closeModal('modalContact')}
            selectedPhoneNumber={(n) => this.selectedPhoneNumber(n)}
          />
        </Modal>
        
      </View>
    )
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  }
}




// STYLES
// ------------------------------------
const styles = StyleSheet.create(PulsaStyle);
