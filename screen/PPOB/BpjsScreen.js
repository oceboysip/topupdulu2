import React, { Component } from 'react';
import { View, Image, ScrollView } from 'react-native';
import Text from '../Text';
import Button from '../Button';
import Color from '../Color';
import Header from '../Header';
import TouchableOpacity from '../Button/TouchableDebounce';
import { TextInputMask } from 'react-native-masked-text';
import Ionicons from 'react-native-vector-icons/Ionicons';

const MainMenuArray = [
  [
    {id: 0, label : 'Sep', label2 : '2019'},
    {id: 1, label : 'Okt', label2 : '2019'},
    {id: 2, label : 'Nov', label2 : '2019'}
  ],
  [
    {id: 3, label : 'Des', label2 : '2019'},
    {id: 4, label : 'Jan', label2 : '2020'},
    {id: 5, label : 'Feb', label2 : '2020'}
  ],
  [
    {id: 6, label : 'Mar', label2 : '2020'},
    {id: 7, label : 'Apr', label2 : '2020'},
    {id: 8, label : 'Mei', label2 : '2020'}
  ],
  [
    {id: 9, label : 'Jun', label2 : '2020'},
    {id: 10, label : 'Jul', label2 : '2020'},
    {id: 11, label : 'Ags', label2 : '2020'}
  ],
];


export default class BpjsScreen extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    this.state = {
      selectedPrice: []
    };
  };

  onSelectPrice(m) {
    let selectedPrice = [m]
    this.setState({ selectedPrice });
  }


render(){
  console.log(this.state,'kkk')
  return(
    <View style= {{width: '100%', height: '100%', backgroundColor: '#FAFAFA'}}>
      <Header style={{ height: 60}} title= 'BPJS' showLeftButton></Header>
      <ScrollView style={{marginBottom: 90}}>
        <Text align= 'left' style={{color: '#A8A699', fontSize: 12, marginTop: 13, paddingHorizontal: 16}}> No. VA Keluarga </Text>
              <TextInputMask
                name="text"
                returnKeyType="done"
                returnKeyLabel="Done"
                type={'only-numbers'}
                value={this.state.value}
                onChangeText={text => {
                this.setState({
                  value:text
                    })
                  }
                }
                blurOnSubmit={false}
                error={null}
                style={{marginLeft: 16, marginBottom:7, width: '90%', alignContent: 'flex-start', letterSpacing: 0.36, borderBottomWidth: 0.5, borderColor: '#0000000D'}}
            />
                <Text align= 'left' type='bold' style={{fontSize: 14, paddingHorizontal: 16, paddingBottom: 16}}>Bayar Hingga</Text>
                <View style={{width: '100%', paddingHorizontal: 16, elevation: 2, marginBottom: 15}}>
                  {MainMenuArray.map((menu, i) =>
                    <View style={{flexDirection: 'row', width: '65%', backgroundColor: '#FFFFFF', justifyContent: 'center', alignItems: 'center', marginLeft: 65}} key={i}>
                      {menu.map((m, idx) =>
                        <TouchableOpacity
                          activeOpacity={0.5} onPress={() => this.onSelectPrice(m)}
                          style={[this.state.selectedPrice.filter(e => e.id == m.id).length> 0 ? {backgroundColor: Color.theme} : {backgroundColor: '#FFFFFF'}, {width: '50%', height: 95, alignItems: 'center', justifyContent : 'center', borderRightWidth: 0.5, borderTopWidth: 0.5, borderColor: '#DDDDDD', elevation: 2, paddingVertical: 3, paddingHorizontal: 3}]}
                        >
                          {this.state.selectedPrice.filter(e => e.id == m.id).length> 0 && <Ionicons name='md-checkmark' style={{right: 5, position: 'absolute', top: 3, zIndex:2}}/>}
                          <View style={{ backgroundColor: '#FFFFFF', height: '100%', width: '100%', borderTopRightRadius: 50}}>
                            <Text type='bold' style={{fontSize: 11, marginTop: 6, fontSize: 16}}>{m.label}</Text>
                            <Text style={{fontSize: 11, marginTop: 6, color: '#FF425E', fontSize: 14}}>{m.label2}</Text>
                          </View>
                        </TouchableOpacity>)
                      }
                    </View>
                  )}
                </View>
                <View style={{backgroundColor: '#0000000D', width: '100%', height: 10, marginTop: 10}}></View>
                <Text align= 'left' type='bold' style={{marginTop:10, paddingHorizontal: 16}}>Informasi Tagihan</Text>
                  <View style={{flexDirection: 'row', marginTop: 13, borderTopWidth: 0.5, borderTopColor: '#0000000D',flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={{ width: '50%'}}>
                      <Text align={'left'} type='medium' style={{fontSize:12, paddingHorizontal: 16, marginTop: 8}}>No. VA Keluarga</Text>
                      <Text align={'left'} type='medium' style={{fontSize:12, paddingHorizontal: 16, marginTop: 3}}>Nama lengkap</Text>
                      <Text align={'left'} type='medium' style={{fontSize:12, paddingHorizontal: 16,  marginTop: 3}}>Nama Cabang</Text>
                      <Text align={'left'} type='medium' style={{fontSize:12, paddingHorizontal: 16,  marginTop: 3}}>Jumlah Peserta</Text>
                      <Text align={'left'} type='medium' style={{fontSize:12, paddingHorizontal: 16,  marginTop: 3}}>Bayar Hingga</Text>
                      <Text align={'left'} type='medium' style={{fontSize:12, paddingHorizontal: 16,  marginTop: 3}}>Biaya Admin</Text>
                      <Text align={'left'} type='medium' style={{fontSize:12, paddingHorizontal: 16,  marginTop: 3}}>Iuran</Text>
                    </View>

                    <View style={{width: '50%', alignItems: 'flex-end'}}>
                    <Text align={'left'} type='medium' style={{fontSize:12, paddingHorizontal: 16, marginTop: 8}}>888888442713889</Text>
                    <Text align={'left'} type='medium' style={{fontSize:12, paddingHorizontal: 16,  marginTop: 3}}>Sepatan</Text>
                    <Text align={'left'} type='medium' style={{fontSize:12, paddingHorizontal: 16, marginTop: 3}}>Ronald Brow</Text>
                    <Text align={'left'} type='medium' style={{fontSize:12, paddingHorizontal: 16,  marginTop: 3}}>Rp 2500</Text>
                    <Text align={'left'} type='medium' style={{fontSize:12, paddingHorizontal: 16,  marginTop: 3}}>1</Text>
                    <Text align={'left'} type='medium' style={{fontSize:12, paddingHorizontal: 16,  marginTop: 3}}>Nov 2019</Text>
                    <Text align={'left'} type='medium' style={{fontSize:12, paddingHorizontal: 16,  marginTop: 3}}>Rp 167.500</Text>
                    </View>
                  </View>
              </ScrollView>
              <View style={{flexDirection: 'row', width: '100%', height: 63, backgroundColor: '#FFFFFF', position: 'absolute', bottom: 0}}>
                  <View style={{ width: '50%'}}>
                      <Text align={'left'} type='medium' style={{paddingHorizontal: 16, marginTop: 8}}>Total Tagihan</Text>
                      <Text align={'left'} type='bold' style={{color: '#FF425E', paddingHorizontal: 16, marginTop: 3}}>Rp 167.500</Text>
                  </View>

                  <View style={{ width: '50%', alignItems: 'flex-end', justifyContent: 'center', paddingHorizontal: 16}}>
                      <Button style={{width: 173, height: 45, marginLeft: 150}} onPress={() => alert ('Bayar')}>
                        <Text style={{color: '#FFFFFF'}}>Bayar</Text>
                      </Button>
                  </View>
              </View>
    </View>
  )
 }
}
