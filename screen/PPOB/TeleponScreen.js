import React, { Component } from 'react';
import { View, Modal, SafeAreaView, Image } from 'react-native';
import Styled from 'styled-components';
import gql from 'graphql-tag';
import { TextInputMask } from 'react-native-masked-text';
import { connect } from 'react-redux';

import Text from '../Text';
import Button from '../Button';
import Color from '../Color';
import Header from '../Header';
import TouchableOpacity from '../Button/TouchableDebounce';
import Input from '../Input/Input';
import Client from '../../state/apollo';
import FormatMoney from '../FormatMoney';
import ModalIndicator from '../Modal/ModalIndicator';
import ModalInformation from '../Modal/ModalInformation';
import { teleponPascaBayar } from '../../state/actions/PPOB/telepon';

const TextInputSubcriber = Styled(TextInputMask)`
  width: 100%;
  height: 45;
  alignContent: flex-start;
  letterSpacing: 0.36;
  borderBottomWidth: 2;
  borderColor: ${Color.primary};
  marginTop: 4;
  color: ${Color.theme};
  fontWeight: bold;
`;

const TextWhite = Styled(Text)`
  color: #FFFFFF;
`;

const TextTheme = Styled(Text)`
  color: ${Color.theme};
`;

const queryInquiryPascaBayarTelepon = gql`
  query(
    $operator_pascabayar: EnumPascaBayarTelpon!
    $nomor_pelanggan: String!
  ){
    Inquiry_PascaBayar_TELEPON(
      operator_pascabayar: $operator_pascabayar
      nomor_pelanggan: $nomor_pelanggan
    ){
      tr_id
      code
      hp
      tr_name
      period
      nominal
      admin
      ref_id
      response_code
      message
      price
      selling_price
      desc{
        kode_area
        divre
        datel
        jumlah_tagihan
        tagihan{
          detail{
            periode
            nilai_tagihan
            admin
            total
          }
        }
      }
    }
  }
`;

const telkom = require('../../images/telkom.png');
const three = require('../../images/telepon-three.png');
const iconReport = require('../../images/icon_paperWhite.png');

class TeleponScreen extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    this.state = {
      operator: '',
      nomor_pelanggan: '',
      inquiryPascaBayar: null,
      loadingPascaBayar: false,
      modalOperator: false,
      loading: false,
      modalFail: false,
      messageRes: 'Gagal melakukan pembayaran telepon, Harap ulangi kembali'
    };
  };

  componentWillReceiveProps(nextProps) {
    if (!this.props.fetching && nextProps.fetching) {
      this.openModal('loading');
    }else if (this.props.fetching && !nextProps.fetching) {
      this.closeModal('loading');
      if (nextProps.error === null) {
        this.props.navigation.navigate('PaymentScreen');
      }else {
        this.setState({ modalFail: true }, () => setTimeout(() => {
          this.setState({ modalFail: false })
        }, 3000));
        console.log(nextProps.error);
      }
    }
  }

  getInquiryPascaBayarTelepon(nomor_pelanggan) {
    if (nomor_pelanggan.length > 9 && this.state.operator != '') {
      this.setState({ loadingPascaBayar: true });

      let variables = {
        operator_pascabayar: this.state.operator,
        nomor_pelanggan
      }

      console.log(variables);
      

      Client.query({
        query: queryInquiryPascaBayarTelepon,
        variables
      })
      .then(res => {
        this.setState({ inquiryPascaBayar: res.data.Inquiry_PascaBayar_TELEPON })
      })
      .catch(err => {
        this.setState({ inquiryPascaBayar: null });
        console.log(err, 'err')
      })

      this.setState({ loadingPascaBayar: false });
    }else {
      this.setState({ inquiryPascaBayar: null });
    }
  }

  timeoutModal(modal) {
    this.setState({
      [modal]: true
    }, () => {
      let timeout = setTimeout(() => {
        clearTimeout(timeout);
        this.setState({ [modal]: false });
      }, 2000);
    })
  }

  timeoutMessage(state, value) {
    this.setState({
      [state]: value
    }, () => {
      let timeout = setTimeout(() => {
        clearTimeout(timeout);
        this.setState({ [state]: 'Gagal melakukan pembayaran telepon, Harap ulangi kembali' });
      }, 2000);
    })
  }

  openModal(modal) {
    // let timeout = setTimeout(() => {
      // clearTimeout(timeout);
      this.setState({ [modal]: true });
    // }, 500)
  }

  closeModal(modal) {
    let timeout = setTimeout(() => {
      clearTimeout(timeout);
      this.setState({ [modal]: false });
    }, 500);
  }

  openOrderBooking(title) {
    this.props.navigation.navigate('OrderBooking', { title });
  }

  submit(OperatorType, NomorPelanggan) {
    const { inquiryPascaBayar } = this.state;

    if (OperatorType === ''){
      this.timeoutModal('modalFail');
      this.timeoutMessage('messageRes', 'Mohon isi terlebih dahulu operator telepon');
      return;
    } else if (NomorPelanggan === '') {
      this.timeoutModal('modalFail');
      this.timeoutMessage('messageRes', 'Mohon isi nomor pelanggan');
      return;
    } else if (inquiryPascaBayar === null) {
      this.timeoutModal('modalFail');
      this.timeoutMessage('messageRes', 'Mohon isi data dengan benar');
      return;
    }

    const body = {
      OperatorType,
      NomorPelanggan,
      finalAmount: inquiryPascaBayar.price
    };
    
    this.props.orderTeleponPascaBayar(body);
  }

  renderModalOperator() {
    const operator = [
      { operator_pascabayar: 'TELKOMPSTN', operator_logo: telkom, size: {height: 22, width: 36} },
      { operator_pascabayar: 'HPTHREE', operator_logo: three, size: {height: 22, width: 22} }
    ];

    return (
      <SafeAreaView backgroundColor={Color.theme}>
        <Header style={{height: 70}} title='Pilih Operator' showLeftButton onPressLeftButton={() => this.closeModal('modalOperator')} />
        {operator.map((op, idx) =>
          <TouchableOpacity
            key={idx}
            onPress={() => this.setState({ operator: op.operator_pascabayar, modalOperator: false })}
            style={{paddingBottom: 8, backgroundColor: '#FFFFFF', paddingHorizontal: 16, alignItems: 'center', flexDirection: 'row', justifyContent: 'flex-start'}}
          >
            <View style={{borderBottomColor: '#DDDDDD', borderBottomWidth: 0.5, flexDirection: 'row', width: '100%', alignItems: 'center', paddingVertical: 8}}>
              <View style={{width: '12%'}}>
                <Image source={op.operator_logo} resizeMode='contain' style={op.size} />
              </View>
              <Text>{op.operator_pascabayar}</Text>
            </View>
          </TouchableOpacity>)}
      </SafeAreaView>
    )
  }

  render() {
    const { operator, nomor_pelanggan, inquiryPascaBayar, modalOperator } = this.state;
    console.log(this.state)

    return (
      <View style= {{width: '100%', height: '100%', backgroundColor: '#FAFAFA'}}>
        <Header title= 'Telepon' imageRightButton={iconReport} onPressRightButton={() => this.openOrderBooking('TeleponPascabayar')} />
        <View style={{paddingHorizontal: 16, marginTop: 10}}>
          <Input
            componentName='select'
            label='Operator'
            color={Color.theme}
            borderColor={Color.primary}
            borderBottomWidth={2}
            type='bold'
            value={operator == '' ? 'Pilih Operator' : operator}
            onSelect={() => this.setState({ modalOperator: true })}
          />

          <View style={{marginTop: 8}}>
            <TextTheme align='left'>Nomor Telepon</TextTheme>
            <TextInputSubcriber
              name="text"
              placeholder='Masukan nomor telepon'
              returnKeyType="done"
              returnKeyLabel="Done"
              type={'only-numbers'}
              value={nomor_pelanggan}
              onChangeText={(nomor_pelanggan) => this.setState({ nomor_pelanggan }, () => this.getInquiryPascaBayarTelepon(nomor_pelanggan))}
              blurOnSubmit={false}
              error={null}
            />
          </View>
          
          {inquiryPascaBayar && <View style={{marginTop: 16, paddingHorizontal: 10, paddingVertical: 10, backgroundColor: Color.theme}}>
            <TextWhite type='bold' align='left'>Informasi Tagihan</TextWhite>
            <View style={{flexDirection: 'row', flexDirection: 'row', justifyContent: 'space-between', marginTop: 12}}>
              <View style={{width: '50%'}}>
                <TextWhite align={'left'} type='medium'>Kode Area</TextWhite>
                <TextWhite align={'left'} type='medium' style={{marginTop: 3}}>Nama Pelanggan</TextWhite>
                <TextWhite align={'left'} type='medium' style={{marginTop: 3}}>Bayar Hingga</TextWhite>
                <TextWhite align={'left'} type='medium' style={{marginTop: 3}}>Biaya Admin</TextWhite>
                <TextWhite align={'left'} type='medium' style={{marginTop: 3}}>Harga</TextWhite>
              </View>

              <View style={{width: '50%', alignItems: 'flex-end'}}>
                <TextWhite align={'left'} type='medium'>{inquiryPascaBayar.desc.kode_area}</TextWhite>
                <TextWhite align={'left'} type='medium' style={{marginTop: 3}}>{inquiryPascaBayar.tr_name}</TextWhite>
                <TextWhite align={'left'} type='medium' style={{marginTop: 3}}>{inquiryPascaBayar.desc.tagihan.detail[inquiryPascaBayar.desc.tagihan.detail.length - 1].periode}</TextWhite>
                <TextWhite align={'left'} type='medium' style={{marginTop: 3}}>{FormatMoney.getFormattedMoney(inquiryPascaBayar.admin)}</TextWhite>
                <TextWhite align={'left'} type='medium' style={{marginTop: 3}}>{FormatMoney.getFormattedMoney(inquiryPascaBayar.nominal)}</TextWhite>
              </View>
            </View>
          </View>}
        </View>

        {inquiryPascaBayar && <View style={{flexDirection: 'row', width: '100%', height: 63, backgroundColor: '#FFFFFF', position: 'absolute', bottom: 0}}>
          <View style={{ width: '50%'}}>
            <TextTheme align={'left'} type='medium' style={{paddingHorizontal: 16, marginTop: 8}}>Total Tagihan</TextTheme>
            <Text align={'left'} type='bold' style={{color: '#FF425E', paddingHorizontal: 16, marginTop: 3}}>{inquiryPascaBayar ? FormatMoney.getFormattedMoney(inquiryPascaBayar.price) : 'Rp 0'}</Text>
          </View>

          <View style={{ width: '50%', alignItems: 'flex-end', justifyContent: 'center', paddingHorizontal: 16}}>
            <Button style={{width: '100%', height: 45}} onPress={() => this.submit(operator, nomor_pelanggan)}>
              <Text style={{color: '#FFFFFF'}}>Bayar</Text>
            </Button>
          </View>
        </View>}

        <Modal
            onRequestClose={() => this.setState({ modalOperator: false })}
            animationType="fade"
            visible={modalOperator}
        >
          {this.renderModalOperator()}
        </Modal>

        <ModalIndicator
          visible={this.state.loading}
          type="large"
          message="Harap tunggu, kami sedang memproses pesanan Anda"
        />

        <Modal
          onRequestClose={() => {}}
          animationType="fade"
          transparent={true}
          visible={this.state.modalFail}
        >
          <ModalInformation
            label={this.state.messageRes}
            error={false}
          />
        </Modal>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user: state['user.auth'].login.user,
    fetching: state['booking'].fetching,
    error: state['booking'].error
  }
}

const mapDispatchToProps = (dispatch) => (
  {
    orderTeleponPascaBayar: (data) => {
      dispatch(teleponPascaBayar(data))
    }
  }
)

export default connect(mapStateToProps, mapDispatchToProps)(TeleponScreen)