import React, { Component } from "react";
import { StyleSheet, View, Image, TextInput, TouchableOpacity, ScrollView, Modal } from 'react-native';
import BottomSheet from 'reanimated-bottom-sheet';
import { connect } from 'react-redux';

import ReviewPulsaStyle from '../../style/ReviewPulsaStyle';
import FormatMoney from '../FormatMoney';
import ModalIndicator from '../Modal/ModalIndicator';
import ModalInformation from '../Modal/ModalInformation';
import Text from '../Text';

import { pulsaPrabayar } from '../../state/actions/PPOB/pulsa';
import { paketData } from '../../state/actions/PPOB/paket-data';
import { VoucherGame, voucherGame } from '../../state/actions/PPOB/voucher-game';

class ReviewPulsaScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      password: '',
      secureText: true,
      modalFail: false,
      loading: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.fetching && nextProps.fetching) {
      this.openModal('loading');
    }else if (this.props.fetching && !nextProps.fetching) {
      this.closeModal('loading');
      if (nextProps.error === null) {
        this.props.navigation.navigate('PaymentScreen');
      }else {
        this.setState({ modalFail: true, messageRes: nextProps.error }, () => setTimeout(() => {
          this.setState({ modalFail: false,
          })
        }, 3000));
        console.log(JSON.stringify(nextProps.error));
      }
    }
  }

  openModal = (modal) => {
    this.setState({
      [modal]: true
    })
  }

  closeModal = (modal) => {
    this.setState({
      [modal]: false
    })
  }

  onSubmit(number, price, pulsa_code) {
    const { product } = this.props.navigation.state.params;
    
    if (product === 'Pulsa') this.props.onTopUpPulsa({ number, price, pulsa_code });
    else if (product === 'PaketData') this.props.onTopUpPaketData({ number, price, pulsa_code });
    else if (product === 'VoucherGame') this.props.onTopUpVoucherGame({ number, price, pulsa_code });
  }

  render() {
    console.log(this.props, 'props review');
    const { number, nominal, price, pulsa_code, operator, product, type } = this.props.navigation.state.params;
    const { password, secureText } = this.state;
    
    return (
      <View style={styles.container}>
        <View style={styles.navTop}>
          <View style={styles.rowsBeetwen}>
            <View style={{flexDirection: 'row',alignItems:'center'}}>
              <TouchableOpacity onPress={() => this.props.navigation.pop()}>
                <Image style={{width:30, height:30,resizeMode:'contain'}} source={require('../../images/left-arrow.png')}/>
              </TouchableOpacity>
              <Text style={styles.titlePage}>Konfirmasi Pembelian</Text>
            </View>

            {/* <View style={{flexDirection:'row'}}>
              <TouchableOpacity onPress={this.ReportKereta}>
                <Image style={{width:30, height:30, resizeMode:'contain'}}  source={require('../../images/icon_paperWhite.png')}/>
               </TouchableOpacity>
            </View> */}
          </View>
        </View>

        <ScrollView>
          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                
                <View style={styles.flexRowsCenter}>
                  <Text style={styles.smallTextCenter}>Mohon Konfirmasi detail dibawah ini untuk memastikan kebenaran transaksi.</Text>
                </View>
                
                <View style={styles.flexRows}>
                    <View style={{flex:1}}>
                      <Text align='left' style={styles.BigText}>Nama Produk</Text>
                      {product === 'Pulsa' && <Text align='left' style={styles.smallText}>{operator} {FormatMoney.getFormattedMoney(nominal)},-</Text>}
                      {product === 'PaketData' && <Text align='left' style={styles.smallText}>{operator} {nominal}</Text>}
                      {product === 'VoucherGame' && <Text align='left' style={styles.smallText}>{type} {nominal}</Text>}
                    </View>
                </View>

                <View style={styles.flexRows}>
                    <View style={{flex:1}}>
                      <Text align='left' style={styles.BigText}>Nomor Tujuan</Text>
                      <Text align='left' style={styles.smallText}>{number}</Text>
                    </View>
                </View>

                <View style={styles.flexRows}>
                    <View style={{flex:1}}>
                      <Text align='left' style={styles.BigText}>Harga</Text>
                      <Text align='left' style={styles.smallText}>{FormatMoney.getFormattedMoney(price)},-</Text>
                    </View>
                </View>

                {/* <View style={styles.rows2}>
                  <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}}>
                  </View>
                  <Text style={styles.smallTextGreenAbs}>Komisi Member</Text>
                  <Text style={styles.smallTextYellowAbs}>IDR 400</Text>
                </View> */}

                 {/* <View style={styles.flexRows}>
                    <View style={{flex:1}}>
                      <TextInput
                        returnKeyType="done"
                        autoCapitalize="none"
                        placeholder="Password"
                        style={styles.inputText}
                        value={password}
                        secureText={secureText}
                        onChangeText={(password) => this.setState({ password })}
                      />
                      <Text style={styles.smallText}>Masukan password untuk konfirmasi</Text>
                    </View>
                </View> */}

              </View>
            </View>
          </View>

          <View style={styles.alCenter}>
            <TouchableOpacity style={styles.button} onPress={() => this.onSubmit(number, price, pulsa_code)}>
              <Text style={styles.buttonText}>Beli</Text>
            </TouchableOpacity>
                      {/* <Text style={styles.BigTextGrey}>ATAU</Text>
            <TouchableOpacity style={styles.button}>
              <Text style={styles.buttonText}>Gunakan Fingerprint</Text>
            </TouchableOpacity>
            <Text style={styles.smallTextCenter}>Jika gagal dengan fingerprint anda harus menggunakan password dan klik ‘BELI’</Text> */}
          </View>
        </ScrollView>

        <ModalIndicator
          visible={this.state.loading}
          type="large"
          indicators='skype'
          message="Harap tunggu, kami sedang memproses pesanan Anda"
        />
        
        <Modal
            onRequestClose={() => {}}
            animationType="fade"
            transparent
            visible={this.state.modalFail}
          >
            <ModalInformation
              label='Booking Anda Gagal'
              error
            />
          </Modal>
      </View>
    )
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  }
}

// STYLES
// ------------------------------------
const styles = StyleSheet.create(ReviewPulsaStyle);

const mapStateToProps = (state) => {
  return {
    user: state['user.auth'].login.user,
    fetching: state['booking'].fetching,
    error: state['booking'].error,
    booking: state['booking'].booking
  }
}

const mapDispatchToProps = (dispatch) => (
  {
    onTopUpPulsa: (data) => {
      dispatch(pulsaPrabayar(data));
    },
    onTopUpPaketData: (data) => {
      dispatch(paketData(data));
    },
    onTopUpVoucherGame: (data) => {
      dispatch(voucherGame(data));
    }
  }
)

export default connect(mapStateToProps, mapDispatchToProps)(ReviewPulsaScreen)