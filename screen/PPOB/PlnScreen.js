import React, { Component } from 'react';
import { Image, ScrollView, TouchableOpacity, View, Modal, ActivityIndicator, Platform, SafeAreaView } from 'react-native';
import { Tab, Tabs, TabHeading } from 'native-base';
import Styled from 'styled-components';
import Moment from 'moment';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { TextInputMask } from 'react-native-masked-text';
import gql from 'graphql-tag';
import { connect } from 'react-redux';

import Color from '../Color';
import Button from '../Button';
import Text from '../Text';
import Header from '../Header';
import Client from '../../state/apollo';
import FormatMoney from '../FormatMoney';
import ModalIndicator from '../Modal/ModalIndicator';
import ModalInformation from '../Modal/ModalInformation';
import { plnPraBayar, plnPascaBayar } from '../../state/actions/PPOB/pln';

const TextInputSubsriber = Styled(TextInputMask)`
  width: 100%;
  height: 45;
  alignContent: flex-start;
  letterSpacing: 0.36;
  borderBottomWidth: 2;
  borderColor: ${Color.primary};
  color: ${Color.theme};
  fontWeight: bold
  marginTop: 4;
`;

const PrepaidInfoView = Styled(View)`
  flexDirection: row;
  marginTop: 16;
  paddingHorizontal: 16;
`;

const PrepaidInfoLeftView = Styled(View)`
  width: 50%;
  paddingHorizontal: 10;
  alignItems: flex-start;
  backgroundColor: ${Color.theme};
  paddingVertical: 10;
`;

const PrepaidInfoRightView = Styled(PrepaidInfoLeftView)`
  alignItems: flex-end;
`;

const PrepaidInfoText = Styled(Text)`
  color: #FFFFFF;
  lineHeight: 24;
`;

const BottomView = Styled(View)`
  flexDirection: row;
  width: 100%;
  height: 60;
  backgroundColor: #FFFFFF;
  alignItems: center;
  paddingHorizontal: 16;
`;

const BottomLeftView = Styled(View)`
  width: 50%;
`;

const BottomRightView = Styled(BottomLeftView)`
  alignItems: flex-end;
  justifyContent: center;
`;

const ButtonPrepaidList = Styled(TouchableOpacity)`
  width: 50%;
  backgroundColor: #FFFFFF;
  aspectRatio: 2;
  alignItems: center;
  justifyContent : center;
  borderColor: #DDDDDD;
  borderTopWidth: 0.5;
  borderRightWidth: 0.5;
  paddingVertical: 3;
  paddingHorizontal: 3;
`;

const TextWhite = Styled(Text)`
  color: #FFFFFF;
`;

const TextTheme = Styled(Text)`
  color: ${Color.theme};
`;

const getQueryPlnPrabayar = gql`
  query(
    $customerID: String
  ){
    mobilePulsaCheck_PLN_Prabayar_Subcriber(
      customerID: $customerID
    ){
      status
      hp
      meter_no
      subscriber_id
      name
      segment_power
      message
      rc
    }
  }
`;

const getQueryPlnPrice = gql`
  query(
    $param: RequestServiceType!
  ){
    mobilePulsaPrepaidPriceList(
      param:$param
    )
    {
     pulsa_code
      pulsa_op
      pulsa_nominal
      pulsa_price
      pulsa_type
      masaaktif
      status
    }
  }`
;

const queryInquiryPascaBayarPLN = gql`
  query(
    $nomor_pelanggan: String!
  ){
    Inquiry_PascaBayar_PLN(
      nomor_pelanggan: $nomor_pelanggan
    ) {
      tr_id
      code
      hp
      tr_name
      period
      nominal
      admin
      ref_id
      response_code
      message
      price
      selling_price
      desc{
        tarif
        daya
        lembar_tagihan
        tagihan{
          detail{
            periode
            nilai_tagihan
            admin
            denda
            total
          }
        }
      }
    }
  }
`;

const iconReport = require('../../images/icon_paperWhite.png');

class PlnScreen extends Component {
  static navigationOptions= ({ navigation, screenProps }) => ({
      header: null
  });

  constructor(props) {
    super(props);
    this.state = {
      activeTab: props.navigation.state.params.activeTab,
      selectedPrice: null,
      plnPriceList: [],
      name: '',
      meter_no: '',
      hp: '',
      loading: false,
      modalFail: false,

      inquiryPascabayar: null,
      loadingPascaBayar: false
    };
  };

  componentDidMount() {
    this.getQuerymobilePulsaPrepaidPriceList();
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.fetching && nextProps.fetching) {
      this.openModal('loading');
    }else if (this.props.fetching && !nextProps.fetching) {
      this.closeModal('loading');
      if (nextProps.error === null) {
        this.props.navigation.navigate('PaymentScreen');
      }else {
        this.setState({ modalFail: true }, () => setTimeout(() => {
          this.setState({ modalFail: false })
        }, 1000));
        console.log(nextProps.error);
      }
    }
  }

  openModal(modal) {
    let timeout = setTimeout(() => {
      clearTimeout(timeout);
      this.setState({ [modal]: true });
    }, 500)
  }

  closeModal(modal) {
    let timeout = setTimeout(() => {
      clearTimeout(timeout);
      this.setState({ [modal]: false });
    }, 500);
  }

  onSelectPrice(selectedPrice) {
    // let selectedPrice = this.state.selectedPrice;
    // let index = selectedPrice.findIndex(item => item == m);
    //
    // if (index === -1) {
    //   selectedPrice.push(m);
    // }
    // else {
    //   selectedPrice.splice(index, 1)
    // }
    this.setState({ selectedPrice });
  }

  getPlnPrabayar(customerID) {
    if (customerID.length > 9) {
      let variables = {
        customerID
      }

      Client.query({
        query: getQueryPlnPrabayar,
        variables
      }).then(res => {
        let data = res.data.mobilePulsaCheck_PLN_Prabayar_Subcriber;
        console.log(data, 'hgr');
        if (data) {
          this.setState({
            hp: data.hp ? data.hp : '',
            name: data.name ? data.name : '',
            meter_no: data.meter_no ? data.meter_no : ''
          });
          // this.getQueryPlnPrice();
        }
      }).catch((err) => {
        console.log('Error', err);
      })
    }else {
      this.setState({
        name: '',
        meter_no:'',
        hp: ''
      })
    }
  }

  getQuerymobilePulsaPrepaidPriceList(){
    let variables = {
      param: {
        SegmentType: 'TOKEN_LISTRIK',
        ServiceItem: {
          OperatorItemProvided: {
            Provider_Token_Listrik: 'pln'
          }
        }
      }
    }

    console.log(variables);

    Client.query({
      query: getQueryPlnPrice,
      variables
    }).then(res => {
      let data = res.data.mobilePulsaPrepaidPriceList;
      console.log(res, 'pulsa list');
      let plnPriceList = []

      if (data.length > 0) {
        for (var i = 0; i < data.length; i++) {
          if (data[i] !== null) plnPriceList.push(data[i])
        }
      }

      this.setState({ plnPriceList });
    })
    .catch(err => console.log(err, 'err'))
  }

  getQueryPascaBayarPLN(nopel) {
    if(nopel.length > 9) {
      this.setState({ loadingPascaBayar: true })
      let variables = {
        nomor_pelanggan: nopel
      }
  
      Client.query({
        query: queryInquiryPascaBayarPLN,
        variables
      })
      .then(res => {
        let data = res.data.Inquiry_PascaBayar_PLN;
        this.setState({ inquiryPascabayar: data });
      })
      .catch(err => {
        console.log(err, 'err')
        this.setState({ inquiryPascabayar: null });
      })

      this.setState({ loadingPascaBayar: false });
    }else {
      this.setState({ inquiryPascabayar: null });
    }
  }

  compare(a, b) {
    let nameA = a.pulsa_price, nameB = b.pulsa_price;
  
    let comparison = 0;

    if (nameA > nameB) {
      comparison = 1;
    } else if (nameA < nameB) {
      comparison = -1;
    }
    return comparison;
  }

  onSubmitPrepaid() {
    const { selectedPrice, meter_no } = this.state;

    if (selectedPrice === null || meter_no === '') return;

    let body = {
      finalAmount: selectedPrice.pulsa_price,
      phoneNumber: meter_no,
      pulsaCode: selectedPrice.pulsa_code,
      autoDetect: 'false',
      pulsaPrice: selectedPrice.pulsa_price.toString(),
      category: 'TOKEN_LISTRIK'
    }

    this.props.orderPlnPraBayar(body);
  }

  onSubmitPostpaid() {
    const { pascabayarText, inquiryPascabayar } = this.state;

    if (pascabayarText === '' || inquiryPascabayar === null) return;

    const body = {
      NomorPelanggan: pascabayarText,
      finalAmount: inquiryPascabayar.price
    };

    this.props.orderPlnPascaBayar(body);
  }

  getNominalComma(str) {
    let nominal = FormatMoney.getFormattedMoney(parseInt(str));
    return nominal.split(" ")[1];
  }

  openOrderBooking(title) {
    this.props.navigation.navigate('OrderBooking', { title });
  }

  renderTabHeading(label, index, activeTab) {
    return (
      <TabHeading style={{ backgroundColor: Color.theme }}>
        <Text type={activeTab !== index ? 'medium' : 'bold'} style={{color: '#FFFFFF'}}>{label}</Text>
      </TabHeading>
    );
  }

  renderPrabayar() {
    const { plnPriceList, selectedPrice, meter_no, name } = this.state;

    return (
      <View style={{height: '100%'}}>
        <ScrollView>
          <View style={{paddingHorizontal: 16, marginTop: 22}}>
            <TextTheme type='medium' align='left'>ID Pelanggan</TextTheme>
            <TextInputSubsriber
              name="text"
              placeholder='Masukan ID Pelanggan'
              returnKeyType="done"
              returnKeyLabel="Done"
              type={'only-numbers'}
              blurOnSubmit={false}
              error={null}
              value={this.state.value}
              onChangeText={(text) => this.setState({ value: text }, () => this.getPlnPrabayar(text.toString()) )}
            />
          </View>
          
          <PrepaidInfoView>
            <PrepaidInfoLeftView>
              <PrepaidInfoText type='medium'>No. Meter</PrepaidInfoText>
              <PrepaidInfoText type='medium'>Nama</PrepaidInfoText>
            </PrepaidInfoLeftView>

            <PrepaidInfoRightView>
              <PrepaidInfoText type='medium'>{meter_no !== '' ? meter_no : '-'}</PrepaidInfoText>
              <PrepaidInfoText type='medium'>{name !== '' ? name : '-'}</PrepaidInfoText>
            </PrepaidInfoRightView>
          </PrepaidInfoView>

          <TextTheme align={'left'} type='medium' style={{marginVertical: 16, paddingHorizontal: 16}}>Nominal</TextTheme>
          <View style={{paddingHorizontal: 16}}>
            {plnPriceList.length > 0 ? <View style={{flexDirection: 'row', borderBottomWidth: 0.5, borderLeftWidth: 0.5, borderColor: '#DDDDDD', flexWrap: 'wrap', width: '100%'}}>
              {this.state.plnPriceList.sort(this.compare).map((menu, i) => 
                <ButtonPrepaidList
                  key={i}
                  activeOpacity={1} onPress={() => this.onSelectPrice(menu)}
                  style={selectedPrice && selectedPrice.pulsa_code === menu.pulsa_code && {backgroundColor: Color.primary}}
                >
                  <View style={[{backgroundColor: '#FFFFFF', height: '100%', width: '100%', justifyContent: 'center'}, selectedPrice && selectedPrice.pulsa_code === menu.pulsa_code && {opacity: 0.7}]}>
                    <TextTheme type='bold' size={12} style={{marginTop: 6, fontSize: 16}}>{this.getNominalComma(menu.pulsa_nominal)}</TextTheme>
                    <TextTheme>Harga <Text style={{marginTop: 6, color: '#FF425E'}}>{FormatMoney.getFormattedMoney(menu.pulsa_price)}</Text></TextTheme>
                  </View>
                </ButtonPrepaidList>
              )}
            </View>
          :
            <ActivityIndicator color={Color.text} size='large' style={{marginTop: 80}} />
          }
          </View>
        </ScrollView>

        {selectedPrice && this.state.meter_no !== '' && <BottomView>
          <BottomLeftView>
            <TextTheme align={'left'} type='medium'>Total Tagihan</TextTheme>
            <Text align={'left'} type='bold' style={{color: '#FF425E', lineHeight: 24}}>{FormatMoney.getFormattedMoney(selectedPrice.pulsa_price)}</Text>
          </BottomLeftView>
          <BottomRightView>
            <Button style={{width: '100%', height: 45}} onPress={() => this.onSubmitPrepaid()}>
              Bayar
            </Button>
          </BottomRightView>
        </BottomView>}
      </View>
    )
  }

  renderPascaBayar() {
    const { inquiryPascabayar, loadingPascabayar } = this.state;

    return (
      <View style={{height: '100%', backgroundCOlor: '#FFFFFF'}}>
        <ScrollView>
          <View style= {{width: '100%', height: '100%'}}>
            <View style={{paddingHorizontal: 16, marginTop: 22}}>
              <TextTheme align= 'left' type='medium'>ID Pelanggan</TextTheme>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <TextInputSubsriber
                name="text"
                placeholder='Masukan ID Pelanggan'
                returnKeyType="done"
                returnKeyLabel="Done"
                type={'only-numbers'}
                blurOnSubmit={false}
                error={null}
                value={this.state.pascabayarText}
                onChangeText={(text) => this.setState({ pascabayarText: text }, () => this.getQueryPascaBayarPLN(text) )}
              />
              {loadingPascabayar && <ActivityIndicator color={Color.text} style={{right: 16, top: 24, position: 'absolute'}} />}
              </View>
            </View>

            {inquiryPascabayar && <View style={{marginTop: 16, marginHorizontal: 16}}>
              <View style={{backgroundColor: Color.theme, paddingVertical: 10, paddingHorizontal: 10}}>
                <Text align='left' type='bold' style={{color: '#FFFFFF'}}>Informasi Tagihan</Text>
                <View style={{flexDirection: 'row', marginTop: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
                  <View style={{width: '50%'}}>
                    <Text align={'left'} type='medium' style={{color: '#FFFFFF'}}>Harga</Text>
                    <Text align={'left'} type='medium' style={{marginTop: 3, color: '#FFFFFF'}}>Periode</Text>
                    <Text align={'left'} type='medium' style={{marginTop: 3, color: '#FFFFFF'}}>Biaya Admin</Text>
                  </View>

                  <View style={{width: '50%', alignItems: 'flex-end'}}>
                    <Text align={'left'} type='medium' style={{color: '#FFFFFF'}}>{FormatMoney.getFormattedMoney(inquiryPascabayar.nominal)}</Text>
                    <Text align={'left'} type='medium' style={{marginTop: 3, color: '#FFFFFF'}}>{Moment("2016-08").format('MMM-YYYY')}</Text>
                    <Text align={'left'} type='medium' style={{marginTop: 3, color: '#FFFFFF'}}>{FormatMoney.getFormattedMoney(inquiryPascabayar.admin)}</Text>
                  </View>
                </View>
              </View>
            </View>}
          </View>
        </ScrollView>
        {inquiryPascabayar && <BottomView>
          <BottomLeftView>
            <TextTheme align={'left'} type='medium'>Total Tagihan</TextTheme>
            <Text align={'left'} type='bold' style={{color: '#FF425E', lineHeight: 24}}>{inquiryPascabayar ? FormatMoney.getFormattedMoney(inquiryPascabayar.price) : 'Rp 0'}</Text>
          </BottomLeftView>
          <BottomRightView>
            <Button style={{width: '100%', height: 45}} onPress={() => this.onSubmitPostpaid()}>
              Bayar
            </Button>
          </BottomRightView>
        </BottomView>}
      </View>
    )
  }

  renderAllTabs(tab) {
    const { activeTab } = this.state;
    return tab.map((item, index) =>
      <Tab
        key={index}
        heading={this.renderTabHeading(item.label, index, activeTab)}
        tabStyle={{backgroundColor: Color.theme}}
        activeTabStyle={{backgroundColor: Color.theme}}
      >
        {index === activeTab && item.children}
      </Tab>
    )
  }

  render() {
    console.log(this.state, 'stt');
    console.log(this.props, 'prr');

    const { activeTab } = this.state;
    
    const tab = [
      { label: 'PRABAYAR', children: this.renderPrabayar() },
      { label: 'PASCABAYAR', children: this.renderPascaBayar() }
    ];

    return (
      <SafeAreaView style={{flex: 1}}>
        <Header title='PLN' imageRightButton={iconReport} onPressRightButton={() => this.openOrderBooking(activeTab === 0 ? 'PlnPrabayar' : 'PlnPascabayar')} />
        <Tabs
          initialPage={activeTab}
          page={activeTab}
          prerenderingSiblingsNumber={Infinity}
          tabStyle={{backgroundColor: '#F4F4F4'}}
          tabContainerStyle={{elevation: 0}}
          tabBarUnderlineStyle={{backgroundColor: '#FFFFFF', height: 3}}
          onChangeTab={({ i }) => this.setState({ activeTab: i })}
          locked
        >
          {this.renderAllTabs(tab)}
        </Tabs>

        <ModalIndicator
          visible={this.state.loading}
          type="large"
          message="Harap tunggu, kami sedang memproses pesanan Anda"
        />

        <Modal
          onRequestClose={() => {}}
          animationType="fade"
          transparent={true}
          visible={this.state.modalFail}
        >
          <ModalInformation
            label='Gagal melakukan Pembelian Token Listrik, Harap ulangi kembali'
            error={false}
          />
        </Modal>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state['user.auth'].login.user,
    fetching: state['booking'].fetching,
    error: state['booking'].error
  }
}

const mapDispatchToProps = (dispatch) => (
  {
    orderPlnPraBayar: (body) => {
      dispatch(plnPraBayar(body))
    },
    orderPlnPascaBayar: (body) => {
      dispatch(plnPascaBayar(body))
    }
  }
)

export default connect(mapStateToProps, mapDispatchToProps)(PlnScreen)
