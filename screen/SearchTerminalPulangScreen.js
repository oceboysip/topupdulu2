/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {Alert,Platform, StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions, Button} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import SearchTerminalStyle from '../style/SearchTerminalStyle';


export default class SearchTerminalPulangScreen extends Component<{}> {
  BookedTrain = () => {
    this.props.navigation.navigate('BookedTrain');
  }
  render() {
    return (
      <View style={styles.container}>

        <View style={styles.navTop}>
          <View style={styles.rowsBeetwen}>
            <View style={{flexDirection: 'row',alignItems:'center'}}>
              <TouchableOpacity onPress={this.BookedTrain}>
                <Image style={{width:30, height:30,resizeMode:'contain'}} source={require('../images/left-arrow.png')}/>
              </TouchableOpacity>
              <Text style={styles.titlePage}>Pilih Stasiun Pulang</Text>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.searchRow}>
              <TextInput placeholder="Cari Terminal" style={styles.searchInput} />
              <Image style={{width:30, height:30,resizeMode:'contain',position:'absolute', left:40,top:4}} source={require('../images/icon_search.png')}/>
            </View>
          </View>

        </View>

        
        <ScrollView>
          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  
                  <View style={styles.captionForm}>
                    <TouchableOpacity onPress={this.BookedTrain}>
                      <Text style={styles.BigText}>Bandung</Text>
                    </TouchableOpacity>
                    <Text style={styles.smallText}>BANDUNG</Text>
                  </View>

                  <View style={styles.iconForm}>
                    <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../images/icon_train.png')}/>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  
                  <View style={styles.captionForm}>
                    <TouchableOpacity onPress={this.BookedTrain}>
                      <Text style={styles.BigText}>Klaracondong</Text>
                    </TouchableOpacity>
                    <Text style={styles.smallText}>BANDUNG</Text>
                  </View>

                  <View style={styles.iconForm}>
                    <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../images/icon_train.png')}/>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  
                  <View style={styles.captionForm}>
                    <TouchableOpacity onPress={this.BookedTrain}>
                      <Text style={styles.BigText}>Cirebon</Text>
                    </TouchableOpacity>
                    <Text style={styles.smallText}>CIREBON</Text>
                  </View>

                  <View style={styles.iconForm}>
                    <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../images/icon_train.png')}/>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  
                  <View style={styles.captionForm}>
                    <TouchableOpacity onPress={this.BookedTrain}>
                      <Text style={styles.BigText}>Bandung</Text>
                    </TouchableOpacity>
                    <Text style={styles.smallText}>BANDUNG</Text>
                  </View>

                  <View style={styles.iconForm}>
                    <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../images/icon_train.png')}/>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  
                  <View style={styles.captionForm}>
                    <TouchableOpacity onPress={this.BookedTrain}>
                      <Text style={styles.BigText}>Klaracondong</Text>
                    </TouchableOpacity>
                    <Text style={styles.smallText}>BANDUNG</Text>
                  </View>

                  <View style={styles.iconForm}>
                    <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../images/icon_train.png')}/>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  
                  <View style={styles.captionForm}>
                    <TouchableOpacity onPress={this.BookedTrain}>
                      <Text style={styles.BigText}>Cirebon</Text>
                    </TouchableOpacity>
                    <Text style={styles.smallText}>CIREBON</Text>
                  </View>

                  <View style={styles.iconForm}>
                    <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../images/icon_train.png')}/>
                  </View>
                </View>

              </View>
            </View>
          </View>

          
          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  
                  <View style={styles.captionForm}>
                    <TouchableOpacity onPress={this.BookedTrain}>
                      <Text style={styles.BigText}>Cirebon</Text>
                    </TouchableOpacity>
                    <Text style={styles.smallText}>CIREBON</Text>
                  </View>

                  <View style={styles.iconForm}>
                    <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../images/icon_train.png')}/>
                  </View>
                </View>

              </View>
            </View>
          </View>

        </ScrollView>
      </View>
    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}




// STYLES
// ------------------------------------
const styles = StyleSheet.create(SearchTerminalStyle);