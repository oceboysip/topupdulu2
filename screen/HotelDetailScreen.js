/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {Alert,Platform, StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions, Button,Switch,TouchableWithoutFeedback} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Dialog, {SlideAnimation, DialogContent } from 'react-native-popup-dialog';
import BottomSheet from 'reanimated-bottom-sheet'
import SlidingUpPanel from 'rn-sliding-up-panel';
import HotelDetailStyle from '../style/HotelDetailStyle';
import Carousel from 'react-native-banner-carousel';

const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 200;
 
const images = [
     "http://mfebriansyah.com/images/img_hotel1.jpg",
    "http://mfebriansyah.com/images/img_hotel2.jpg"
];


export default class HotelDetailScreen extends Component<{}> {
  renderPage(image, index) {
      return (
          <View key={index}>
              <Image style={{ width: BannerWidth, height: BannerHeight, resizeMode:'contain' }} source={{ uri: image }} />
          </View>
      );
  }

  constructor(props){
  super(props)
    this.state = {
      isOnDefaultToggleSwitch: false,
      isOnLargeToggleSwitch: false,
      isOnBlueToggleSwitch: false,
      isHidden: false
    };
  }

  onToggle(isOn) {
    console.log("Changed to " + isOn);
  }
  HasilSearchHotel = () => {
    this.props.navigation.navigate('HasilSearchHotel');
  }
  PilihKamar = () => {
    this.props.navigation.navigate('PilihKamar');
  }
  ReviewBooking = () => {
    this.props.navigation.navigate('ReviewBooking');
  }

  render() {
    return (
      <View style={styles.container}>
      

        <View style={styles.navTop}>
          <View style={styles.rowsBeetwen}>
            <View style={{flexDirection: 'row',alignItems:'center'}}>
              <TouchableOpacity onPress={this.HasilSearchHotel}>
                <Image style={{width:30, height:30,resizeMode:'contain'}} source={require('../images/left-arrow.png')}/>
              </TouchableOpacity>
              <Text style={styles.titlePage}>Hotel Detail</Text>
            </View>
            <View style={{flexDirection:'row'}}>
              <TouchableOpacity onPress={this.ReportHotel}>
                <Image style={{width:30, height:30, resizeMode:'contain'}}  source={require('../images/icon_direction.png')}/>
               </TouchableOpacity>
            </View>
          </View>

        </View>
   

        
        <ScrollView>
          <View style={styles.rowBanner}>
              <Carousel
                  autoplay
                  autoplayTimeout={5000}
                  loop
                  index={0}
                  pageSize={BannerWidth}
              >
                  {images.map((image, index) => this.renderPage(image, index))}
              </Carousel>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row',alignSelf:'stretch',flex:1}}>
                    <View style={styles.captionForm}>

                        <View style={{flexDirection:'row',alignSelf:'stretch',flex:1,marginTop:5, alignItems:'center',marginBottom:10}}>
                          <Image style={{width:127, height:20, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_stars.png')}/>
                        </View>
                        <Text style={styles.BigText}>Lorin Sentul</Text>

                        <View style={{flexDirection:'row',alignSelf:'stretch',flex:1,marginTop:5, alignItems:'center'}}>
                          <Image style={{width:15, height:21, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_place.png')}/>
                          <Text style={styles.smallText}>Sentul Circuit (Exit Toll KM. 32)</Text>
                        </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <Text style={styles.BigText2}>Fasilitas</Text>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                <View>
                  <View style={{flexDirection:'row',marginBottom:15}}>
                    <View style={styles.rowButton}>
                      <Image style={{width:20, height:16, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_checklist.png')}/>
                        <Text style={styles.smallText}>24 Hrs. Room Service</Text>
                    </View>
                    <View style={styles.rowButton}>
                      <Image style={{width:20, height:16, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_checklist.png')}/>
                        <Text style={styles.smallText}>Air-Condition</Text>
                    </View>
                    <View style={styles.rowButton}>
                      <Image style={{width:20, height:16, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_checklist.png')}/>
                        <Text style={styles.smallText}>Kids Activities</Text>
                    </View>
                  </View>

                  <View style={{flexDirection:'row',marginBottom:15}}>
                    <View style={styles.rowButton}>
                      <Image style={{width:20, height:16, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_checklist.png')}/>
                        <Text style={styles.smallText}>24 Hrs. Room Service</Text>
                    </View>
                    <View style={styles.rowButton}>
                      <Image style={{width:20, height:16, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_checklist.png')}/>
                        <Text style={styles.smallText}>Air-Condition</Text>
                    </View>
                    <View style={styles.rowButton}>
                      <Image style={{width:20, height:16, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_checklist.png')}/>
                        <Text style={styles.smallText}>Kids Activities</Text>
                    </View>
                  </View>

                  <View style={{flexDirection:'row',marginBottom:15}}>
                    <View style={styles.rowButton}>
                      <Image style={{width:20, height:16, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_checklist.png')}/>
                        <Text style={styles.smallText}>24 Hrs. Room Service</Text>
                    </View>
                    <View style={styles.rowButton}>
                      <Image style={{width:20, height:16, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_checklist.png')}/>
                        <Text style={styles.smallText}>Air-Condition</Text>
                    </View>
                    <View style={styles.rowButton}>
                      <Image style={{width:20, height:16, resizeMode:'contain',marginRight:10}}  source={require('../images/icon_checklist.png')}/>
                        <Text style={styles.smallText}>Kids Activities</Text>
                    </View>
                  </View>

                </View>
              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <Text style={styles.BigText2}>Pilih Tipe Kamar</Text>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row',alignSelf:'stretch',flex:1}}>
                    <View style={styles.captionForm}>
                      <Text style={styles.BigText}>Superior</Text>
                      <Text style={styles.smallText}>Single</Text>
                    </View>
                  </View>

                  <View style={{flex:1, alignItems:"flex-end", flexDirection:"row",paddingRight:10}}>
                    <Text style={styles.BigText}>IDR 170.000</Text>
                    <TouchableOpacity onPress={this.PilihKamar}>
                      <Image style={{width:30, height:30, resizeMode:'contain',marginLeft:10}}  source={require('../images/arrow_next.png')}/>
                    </TouchableOpacity>
                  </View>

                </View>
              </View>
            </View>
          </View>

          <View style={styles.alCenter}>
            <TouchableOpacity style={styles.button} onPress={this.ReviewBooking}>
              <Text style={styles.buttonText}>BOOKING</Text>
            </TouchableOpacity>
          </View>

        </ScrollView>
        
      </View>
    )
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  }
}




// STYLES
// ------------------------------------
const styles = StyleSheet.create(HotelDetailStyle);