import React, { Component } from 'react';
import { Image, View, ScrollView, BackHandler, ActivityIndicator, TextInput } from 'react-native';
import Styled from 'styled-components';
import { connect } from 'react-redux';
import gql from 'graphql-tag';
import Accordion from 'react-native-collapsible/Accordion';
import Icon from 'react-native-vector-icons/Ionicons';

import Client from '../state/apollo';
import TouchableOpacity from './Button/TouchableDebounce';
import Text from './Text';
import FormatMoney from './FormatMoney';
import Color from './Color';
import Header from './Header';
import fetchApi from '../state/fetchApi';

const getPaymentMethodsQuery = gql`
  query(
    $bookingId: Int!
  ) {
   paymentMethods(
     bookingId: $bookingId
   ) {
      id name items { id class name installment minPaymentAmount logo discount transactionFee finalAmount }
   }
  }
`;

const queryBookingDetail = gql`
  query(
    $bookingId: Int!
  ) {
    bookingDetail(
      bookingId: $bookingId
    ){
      id
      userId
      invoiceNumber
      createdAt
      expiresAt
      amount
      discount
      vat
      finalAmount
      contact {
        title { name abbr }
        firstName
        lastName
        email
        countryCode
        phone
        address
        postalCode
      }
      bookingStatus { id name }
      payments {
        type { id name }
        amount
      }
      flights {
        departure {
          bookingCode
          fareDetail
          total
          journeys {
            origin { code name cityName countryCode }
            destination { code name cityName countryCode }
            airline { name code logoUrl }
            departsAt
            arrivesAt
            flightNumber
            seatClass
          }
        }
        return {
          bookingCode
          fareDetail
          total
          journeys {
            origin { code name cityName countryCode }
            destination { code name cityName countryCode }
            airline { name code logoUrl }
            departsAt
            arrivesAt
            flightNumber
            seatClass
          }
        }
        passengers {
          type
          title { name abbr }
          firstName
          lastName
          birthDate
          nationality
          idNumber
          phone
          passport { number expirationDate issuingCountry }
        }
      }
      tours {
        price { subTotal vat airportTaxAndFuel visa discount total }
        paymentAmount
        priceDetail {
          specs { type price count total }
          dp
          isDp
          isIncludeVisa
        }
        tourSnapshot {
          id
          name
          slug
          departure {
            date
            duration
            airlines { name logoUrl code }
          }
        }
        rooms {
          travellers { type titleAbbr priceType firstName lastName birthDate }
        }
      }
      vestabalance {
        id amount
      }
      attractions {
        id
        paxPrice { adults children seniors }
        timeslottxt
        message
        adults
        children
        seniors
        arrivaldate
        attraction_name
        confirm_date
        status
        discount
        ppn
        final_amount
      }
      buses {
        booking_code
        booking_id
        order_id
        seats {
          busesCode
          busesName
          departureCode
          arrivalCode
          departureName
          arrivalName
          departureDate
          arrivalDate
          seatPosition
          rateCode
          date
          type
          boardingPoint{
            pointId
            pointName
            pointTime
            pointAddress
            pointLandmark
            pointContact
          }
          droppingPoint{
            pointId
            pointName
            pointTime
            pointAddress
            pointLandmark
            pointContact
          }
          passenger{
            title
            fullname
            name
            surname
            birthdate
            address
            city
            country
            postal_code
            phone_number
            age
            email
            id_card
          }
        }
      }
      hotels {
        HotelConfig { name value }
        id
        bookingId
        bookingDate
        checkInDate
        checkOutDate
        hotelName
        hotelBoardName
        hotelCategory
        cancellationAmount
        rateClass
        rateClassName
        hotelImage
        hotelCustomerRemark
        hotelAdult
        hotelChild
        hotelInfant
        hotelRoomCodeName
        hotelAddress
        amount
        markup
        discount
        additionalFee
        transactionFee
        ppn
        finalAmount
      }
      prepaidTransaction {
        id
        orderId
        refId
        status
        code
        destinationNumber
        price
        message
        balance
        tr_id
        rc
        category
        order_status
        request_data
        response_data
        lastUpdated
      }
      postpaidTransaction{
        id
        orderId
        paymentRequestData
        paymentResponseData
        transactionId
        productCode
        customerSubscribeNumber
        customerName
        billPeriod
        nominal
        adminFee
        inquiryResponseCode
        inquiryResponseMessage
        price
        sellingPrice
        refId
        paymentResponseCode
        paymentResponseMessage
        paymentResponseDescription
        lastUpdated
      }
    }
  }`
;

const MainView = Styled(View)`
  width: 100%;
  backgroundColor: #FAF9F9
  flexDirection: column
  height: 100%;
`;

const LeftHeaderView = Styled(View)`
  alignItems: flex-start
  width: 70%;
`;

const RightHeaderView = Styled(View)`
  alignItems: flex-end
  width: 30%;
`;

const HeaderView = Styled(View)`
  backgroundColor: ${Color.theme};
  width: 100%;
  flexDirection: column;
`;

const ContentView = Styled(ScrollView)`
`;

const SubHeaderView = Styled(View)`
  flexDirection: row
`;

const Container = Styled(View)`
  padding: 20px 15px 15px 16px;
`;

const BaseText = Styled(Text)`
  color: #231F20;
  lineHeight: 18px;
  fontSize: 14px;
`;

const SmallText = Styled(BaseText)`
  fontSize: 12px
  textAlign: left
`;

const RedText = Styled(SmallText)`
  color: #FF425E;
`;

const SectionView = Styled(TouchableOpacity)`
  minHeight: 1px;
  backgroundColor: #FFFFFF
  marginBottom: 10px
`;

const SectionContainer = Styled(View)`
  paddingTop:15px;
  flexDirection: row
`;

const SectionContainerWithBorder = Styled(View)`
  borderBottomColor: #DDDDDD
  flexDirection: row
  borderBottomWidth: 1
  paddingBottom:15px;
`;

const ImageProperty = Styled(Image)`
    width: 46px;
    height: 15px;
`;

const ImageDetail = Styled(Image)`
    height: 30px;
    width: 60px;
`;

const ImageHeaderView = Styled(View)`
    marginRight: 10px
    justifyContent: flex-end
    minHeight: 1px
`;

const ContainerOvo = Styled(View)`
  width: 100%;
  alignItems: flex-end
`;

const TextFieldOVOPhone = Styled(View)`
  width: 70%;
  flexDirection: row;
  paddingHorizontal: 10;
  borderColor: #999999;
  borderWidth: 0.5;
  height: 45;
`;

const OVOTextInput = Styled(TextInput)`
  fontSize: 12;
  flexBasis: 90%;
  color: #484848;
`;

const ButtonView = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: #FFFFFF;
    alignItems: center;
    justifyContent: center;
    padding: 10px 15px 0px 15px;
    borderTopWidth: 0.5;
    borderTopColor: #DDDDDD;
`;

const ButtonRadius = Styled(TouchableOpacity)`
    borderRadius: 30px;
    width: 80%
    backgroundColor: #231F20;
    height: 50px
    marginVertical: 15px
    justifyContent: center
`;

const ButtonText = Styled(Text)`
    fontSize: 14px
    color: #FFFFFF
    lineHeight: 34px
`;

const logoSiago = require('../images/logo_siago.png');

class PaymentScreen extends Component {
  static navigationOptions = { header: null };

  constructor(props) {
    super(props);
    this.state = {
      siagoAmount: 0,
      loadingAmount: true,
      paymentMethods: [],
      selectedPayment: null,
      readyPaymentMethods: false,
      booking: null,
      readyBooking: false,
      subTitle: '',
      bookingType: '',
      textOVOPhone: '',
      activeSections: [0]
    };
  }

  componentDidMount() {
    if (this.props.navigation.state.params === undefined) {
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    if (this.props.user_siago.role === 'member') this.getSaldo();
    this.getPaymentMethods(this.props.booking.id);
    this.getBookingDetail(this.props.booking.id);
  }

  componentWillUnmount() {
    if (this.props.navigation.state.params === undefined) {
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    }
  }

  handleBackPress = () => {
    this.props.navigation.popToTop();
    return true;
  }

  getSaldo() {
    fetchApi.get('user/check-saldo')
    .then(res => {
      this.setState({ siagoAmount: res.data.data.saldo, loadingAmount: false })
    })
    .catch(err => {
      console.log(err, 'error get saldo');
      this.setState({ loadingAmount: false })
    });
  }

  getPaymentMethods(bookingId) {
    const variables = { bookingId };
    const { user_siago } = this.props;

    Client
      .query({
        query: getPaymentMethodsQuery,
        variables
      })
      .then(res => {
        console.log(res, 'res payment');
        
        if(res.data.paymentMethods) {
          let paymentMethods = [];

          res.data.paymentMethods.map((item) => {
            if (user_siago.role === 'member') {
              if (item.id === 8) paymentMethods.push(item);
            } else {
              if (item.id !== 8) paymentMethods.push(item);
            }
          })
          
          this.setState({
            paymentMethods,
            selectedPayment: paymentMethods[0].items[0],
            readyPaymentMethods: true
          });
        }
      })
      .catch(reject => {
        let errorMessage = 'Terjadi kesalahan server';
        if (reject.graphQLErrors[0].code.indexOf('CLIENT_') === 0) errorMessage = reject.graphQLErrors[0].message;
        this.setState({ readyPaymentMethods: true });
        console.log('reject', reject);
        
      });
    }

    titleBank(item) {
      let merge = '';
      for (const value of item.items) {
         merge = merge + value.name + ', '
      }

      return merge
  }

  getBookingDetail(bookingId) {
    Client
    .query({
        query: queryBookingDetail,
        variables: { bookingId }
    })
    .then(res => {
      if(res.data.bookingDetail) {
        const { flights, tours, attractions, buses, hotels, vestabalance, prepaidTransaction, postpaidTransaction } = res.data.bookingDetail;

        let bookingType = attractions ? 'attractions' : buses ? 'buses' : hotels ? 'hotels' : vestabalance ? 'vestabalance' : null;

        if (flights) if (flights.length > 0) bookingType = 'flights';
        if (tours) if (tours.length > 0) bookingType = 'tours';
        if (prepaidTransaction) bookingType = prepaidTransaction.category;
        if (postpaidTransaction) bookingType = postpaidTransaction.productCode;

        this.setState({ booking: res.data.bookingDetail, bookingType, readyBooking: true });
      }
    }).catch(error => {
      console.log(error, 'error detail booking');
      this.setState({ readyBooking: true });
    });
  }

    submit(pay) {
      const headertitle = this.titleBank(pay);

      this.props.navigation.navigate('PaymentPerBank', {
        paymentMethods: pay.items,
        booking: this.state.booking,
        subTitle: headertitle,
        pickedTitle: pay.name,
        bookingType: this.state.bookingType
      });
    }

    paymentDetail(pay) {
      console.log(pay, 'payyyyy');
      
      this.setState({ selectedPayment: pay });
      this.props.navigation.navigate('PaymentDetail', {
        payment: pay,
        ovoPhone: this.state.textOVOPhone,
        booking: this.state.booking,
        bookingType: this.state.bookingType
      })
    }

    renderLogoBank(item) {
      return item.map((payMethod, payMethodId) =>
        <ImageHeaderView key={payMethodId}>
          <ImageProperty resizeMode={'contain'} source={{ uri: payMethod.logo }} />
        </ImageHeaderView>
      )
    }

    renderPaymentMethod(payment) {
      const iconArrow = 'ios-arrow-forward';
      let _reMap = payment ;
      let _reMapTakeoutVesta = [];
      payment.map((pay, id) => {
        _reMap[id].isVestaBalance = false;
        _reMap[id].enabled = true;
          pay.items.map((payMethod, payMethodId) => {
              if( payMethod.class == 'vestaPoint'  ) {
                    _reMap[id].isVestaBalance = true;
              }
          });
      });

      _reMap.map((pay, id) => {
        if(!this.props.booking.vestabalance) {
          _reMapTakeoutVesta.push(pay);
        }else {
          if(!pay.isVestaBalance) {
            _reMapTakeoutVesta.push(pay);
          }
        }
      });

      return _reMapTakeoutVesta.map((pay, id) => {
        return (
          <SectionView onPress={() => { if(pay.enabled) this.submit(pay); } } key={id}>
              <Container>
                <SectionContainerWithBorder>
                  <LeftHeaderView>
                    <BaseText type='semibold'>{pay.name}</BaseText>
                  </LeftHeaderView>
                  <RightHeaderView>
                    <BaseText><Icon name={iconArrow} /></BaseText>
                  </RightHeaderView>
                </SectionContainerWithBorder>
                <SectionContainer>
                  {this.renderLogoBank(pay.items)}
                </SectionContainer>
              </Container>
            </SectionView>
          );
        }
      )
    }

  onChangeSectionsActive(index, method, i) {
    let activeSections = this.state.activeSections;

    activeSections[i] = index;
    this.setState({ activeSections });
  }

  renderAccordionHeader = (method, index, isActive) => {
    return (
      <View style={{backgroundColor: '#FFFFFF'}}>
        <Container>
          <SectionContainerWithBorder>
            <LeftHeaderView><Text type='bold'>{method.id === 8 ? 'Siago Pay' : method.name}</Text></LeftHeaderView>
            <RightHeaderView><Icon size={18} name={isActive ? 'ios-arrow-back' : 'ios-arrow-forward'} /></RightHeaderView>
          </SectionContainerWithBorder>
        </Container>
      </View>
    );
  }

  renderAccordionContent = (section) => {
    console.log(section, 'item');
    const { siagoAmount, booking } = this.state;
    const amountTotal = booking ? booking.amount : 0;
    
    return section.items.map((pay, id) =>
      <View key={id}>
        <SectionView activeOpacity={0.7} onPress={() => this.setState({ selectedPayment: pay })}>
          {pay.class === 'vestaPoint' && <View style={{paddingHorizontal: 16, marginBottom: 16}}>
            <SmallText>Total saldo <RedText>{FormatMoney.getFormattedMoney(siagoAmount)}</RedText></SmallText>
            {amountTotal > siagoAmount && <RedText>Saldo Anda tidak mencukupi untuk membayar transaksi ini.</RedText> }
          </View>}

          <View style={{paddingHorizontal: 16, flexDirection: 'row', alignItems: 'center'}}>
            <View style={{width: 20}}>
              {this.state.selectedPayment.id === pay.id && <Icon size={20} name='ios-checkmark' />}
            </View>
            <ImageDetail resizeMode='contain' source={pay.class === 'vestaPoint' ? logoSiago : {uri: pay.logo}} />
          </View>
        </SectionView>
        {pay.class === 'OVO' && <ContainerOvo>
          <TextFieldOVOPhone>
            <OVOTextInput
              numberOfLines={1}
              value={this.state.textOVOPhone}
              underlineColorAndroid="transparent"
              autoCorrect={false}
              placeholder='masukkan no telepon OVO / OVO ID'
              onChangeText={(code) => { this.setState({ textOVOPhone: code }, () => console.log(this.state.textOVOPhone)) }}
            />
          </TextFieldOVOPhone>
        </ContainerOvo>}
      </View>
    )
  }

  render() {
    console.log(this.state, 'state payment screen');
    console.log(this.props, 'props payment screen');
    
    const { navigation } = this.props;
    const { readyPaymentMethods, paymentMethods, selectedPayment, activeSections, readyBooking, siagoAmount, booking } = this.state;
    const amountTotal = booking ? booking.amount : 0;

    let paymentData = [];
    paymentMethods.map((item) => {
      paymentData.push([item]);
    })

    let isVestaPayment = false;
    if (selectedPayment) if (selectedPayment.class === 'vestaPoint') isVestaPayment = true;
    
    return (
      <MainView>
        <Header title='Metode Pembayaran' showLeftButton onPressLeftButton={() => {
          if(navigation.state.params === 'order_screen') navigation.pop();
          else navigation.popToTop();
        }} />

        {/* {paymentMethods.length > 0 && this.renderPaymentMethod(paymentMethods)} */}

        <ContentView>
          {paymentMethods.length > 0 && paymentData.map((item, idx) => 
            <Accordion
              key={idx}
              activeSection={activeSections[idx]}
              sections={item}
              touchableProps={{activeOpacity: 1}}
              renderHeader={this.renderAccordionHeader}
              renderContent={this.renderAccordionContent}
              onChange={(index) => this.onChangeSectionsActive(index, item, idx)}
            />
          )}
        </ContentView>

        {selectedPayment && <ButtonView>
          <ButtonRadius disabled={isVestaPayment ? (amountTotal > siagoAmount ? true : false) : false} onPress={() => this.paymentDetail(selectedPayment)}>
            <ButtonText>Berikutnya</ButtonText>
          </ButtonRadius>
        </ButtonView>}

        {/* <ModalIndicator
          visible={!readyPaymentMethods || !readyBooking}
          type="large"
          indicators='skype'
          message="Harap tunggu"
        /> */}
      </MainView>
    );
  }
}

const mapStateToProps = state => {
  return {
    booking: state['booking'].booking,
    user_siago: state['user.auth'].user_siago
  };
};

const mapDispatchToProps = (dispatch) => (
  {

  }
);

export default connect(mapStateToProps, mapDispatchToProps)(PaymentScreen);
