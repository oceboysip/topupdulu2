import React, { Component } from 'react';
import { View, TextInput, ScrollView, Modal, Platform } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';
import { connect } from 'react-redux';
import gql from 'graphql-tag';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import FormatMoney from './FormatMoney';
import TouchableOpacity from './Button/TouchableDebounce';
import ModalIndicator from './Modal/ModalIndicator';
import graphClient from '../state/apollo';
import Button from './Button';
import WebViewScreen from './WebViewScreen';
import Text from './Text';
import ModalInformation from './Modal/ModalInformation';
import Color from './Color';
import Header from './Header';

import BookingDetailFlight from './Order/BookingDetailFlight';
import BookingDetailTravel from './Order/BookingDetailTravel';
import BookingDetailAttraction from './Order/BookingDetailAttraction';
import BookingDetailHotel from './Order/BookingDetailHotel';
import BookingDetailBus from './Order/BookingDetailBus';
import BookingDetailPulsa from './Order/BookingDetailPulsa';
import BookingDetailPaketData from './Order/BookingDetailPaketData';
import BookingDetailVoucherGame from './Order/BookingDetailVoucherGame';
import BookingDetailPlnPrabayar from './Order/BookingDetailPlnPrabayar';
import BookingDetailPlnPascabayar from './Order/BookingDetailPlnPascabayar';
import BookingDetailPdamPascabayar from './Order/BookingDetailPdamPascabayar';
import BookingDetailTeleponPascabayar from './Order/BookingDetailTeleponPascabayar';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
`;

const ModalVoucherView = Styled(TouchableOpacity)`
    width: 340;
    minHeight: 1;
    flexDirection: column;
    justifyContent: center;
    alignItems: center;
    paddingHorizontal: 20;
    backgroundColor: ${Color.theme};
    borderRadius: 3;
`;

const BackgroundModalVoucherView = Styled(TouchableOpacity)`
    width: 100%;
    height: 100%;
    justifyContent: center;
    alignItems: center;
    backgroundColor: rgba(0, 0, 0, 0.4);
`;

const AbsoluteView = Styled(View)`
    position: absolute
    top: 33
    width: 100%;
    zIndex: 1
`;
const HeaderView = Styled(View)`
    height: 92px
    backgroundColor: #FFFFFF
    margin: 0px 15px
    elevation: 5px
    justifyContent: center
    borderWidth: ${Platform.OS === 'ios' ? 1 : 0 };
    borderColor: #DDDDDD;
    borderBottomWidth: ${Platform.OS === 'ios' ? 2 : 0 };

`;
const PriceView = Styled(View)`
    backgroundColor: #FFFFFF
    flexDirection: row
    padding: 0px 15px
    height: 73px
    width: 100%
`;

const PriceViewWithBackground = Styled(PriceView)`
    flexDirection: column;
    alignItems: flex-start;
    height: 60%;
`;

const AgreementView = Styled(View)`
    width: 100%;
    minHeight: 1;
`;

const VoucherBackground = Styled(View)`
    backgroundColor: #FAF9F9;
    flexDirection: row;
    alignItems: flex-start;
`;

const LeftHeaderView = Styled(View)`
  minWidth: 1;
  minHeight: 1;
  alignItems: flex-start
`;

const RightHeaderView = Styled(TouchableOpacity)`
  flex: 1;
  minHeight: 1;
  flexDirection: row;
  alignItems: flex-start;
  justifyContent: flex-end;
`;

const DetailPriceView = Styled(View)`
  flexDirection: row;
  margin: 16px 0px 0px 0px;
`;

const BaseText = Styled(Text)`
  color: #231F20
  lineHeight: 18px
  fontSize: 14px
`;

const RedText = Styled(BaseText)`
  color: #FF425E
`;

const ButtonView = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: #FFFFFF;
    alignItems: center;
    justifyContent: center;
    padding: 10px 15px 0px 15px;
    borderTopWidth: 0.5;
    borderTopColor: #DDDDDD;
`;

const ModalView = Styled(View)`
    minHeight: 1px;
    width: 90%;
    backgroundColor: ${Color.theme};
`;

const ModalButtonView = Styled(View)`
    width: 100%
    alignItems: center
    marginBottom: 20px
`;

const ButtonRadius = Styled(TouchableOpacity)`
    borderRadius: 30px;
    width: 80%
    backgroundColor: #231F20;
    height: 50px
    marginVertical: 15px
    justifyContent: center
`;

const VoucherRoundedView = Styled(View)`
    width: 300;
    height: 50;
    flexDirection: row;
    alignItems: center;
    justifyContent: flex-start;
    marginTop: 10;
    padding: 0px 15px 0px 15px;
    backgroundColor: white;
`;
const HeaderModalVoucher = Styled(View)`
    minWidth: 1;
    height: 50;
    justifyContent: center;
    alignItems: center;
`;

const ErrorView = Styled(View)`
    width: 100%;
    minHeight: 1;
    paddingVertical: 6;
    alignItems: flex-start;
`;

const CloseModalVoucherView = Styled(TouchableOpacity)`
    width: 50;
    height: 50;
    justifyContent: center;
    alignItems: center;
    position: absolute;
    top: 0;
    right: 0;
`;

const VoucherView = Styled(TouchableOpacity)`
    minWidth: 30;
    minHeight: 1;
    padding: 15px 0px;
    paddingLeft: 15;
    paddingRight: 15;
    justifyContent: flex-start;
    alignItems: flex-end;
`;

const CustomTextInput = Styled(TextInput)`
    width: 100%;
    height: 100%;
    color: ${Color.text};
`;

const ResetTextVoucher = Styled(TouchableOpacity)`
    width: 12%;
    height: 100%;
    justifyContent: center;
    alignItems: flex-end;
`;

const NormalText = Styled(Text)`
`;

const ButtonText = Styled(Text)`
    fontSize: 14px
    color: #FFFFFF
    lineHeight: 34px
`;

const ErrorMessage = Styled(Text)`
   fontSize: 12px
   color: red;
   textAlign: left;
`;

const UnderlineText = Styled(BaseText)`
    textAlign: left;
    textDecorationLine: underline
`;

const VoucherButton = Styled(Button)`
    width: 163;
    height: 50;
    marginVertical: 20;
`;

const CloseIcon = Styled(MaterialIcons)`
  fontSize: 23;
  color: #000000;
`;

const SmallerCloseIcon = Styled(CloseIcon)`
  fontSize: 18;
`;

const bookingStatusQuery = gql`
  query(
    $bookingId: Int!
  ) {
   bookingDetail(
     bookingId: $bookingId
   ) {
      id
      bookingStatus { id name }
   }
 }
`;

const voucherCheckQuery = gql`
  query(
    $voucherCode: String!
    $bookingId: Int!
    $paymentId: Int!
  ) {
   voucherCheck(
     voucherCode: $voucherCode
     bookingId: $bookingId
     paymentId: $paymentId
   ) {
      discountAmount transactionFee finalAmount
   }
 }
`;

const payQuery = gql`
    mutation pay(
      $bookingId: Int!
      $paymentId: Int!
      $voucherCode: String
      $paymentPhoneNumber: String
    ){
      pay(
        bookingId: $bookingId
        paymentId: $paymentId
        voucherCode: $voucherCode
        paymentPhoneNumber: $paymentPhoneNumber
      ) {
        redirectUrl transferAccountNumber paymentAmount
      }
    }
`;

class PaymentDetail extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      modalVoucher: false,
      modalTemporary: false,
      appliedVoucher: null,
      voucherFailed: false,
      voucherCheckId: 0,
      kdVoucher: '',
      expiresAt: props.navigation.state.params.booking.expiresAt,
      codeVoucherApplied: null,
      messageVoucherCode: null,
      countdown: null,
      modalWebView: false,
      sourceURL: null,
      loading: false,
      modalFailedInformation: false,
      messageRes: 'Transaksi Gagal'
    };
  }

  openModal = (modal) => {
    this.setState({ [modal]: true })
  }

  componentDidMount() {
    this.startCountdown()
  }

  openModalTimer(name) {
    this.setState({ [name]: true }, () => setTimeout(() => {
      this.setState({ [name]: false,
      })
    }, 3000));
  }

  startCountdown() {
    const countdown = setInterval(() => {
      this.isBookingExpired(this.state.expiresAt)
    }, 1000);

    this.setState({ countdown });
  }

  isPaid() {
    const variables = {
      bookingId: this.props.booking.id
    };
    graphClient
        .query({
            query: bookingStatusQuery,
            variables
        })
        .then(res => {
            if (res.data.bookingDetail) {
                this.setState({ textVoucherCode: null, codeVoucherApplied: null, messageVoucherCode: null });
                if (res.data.bookingDetail.bookingStatus.id === 4) {
                  this.props.navigation.navigate('PaymentSucceed', { bookingId: this.props.booking.id });
                }
                else this.props.navigation.popToTop();
            }
        }).catch(reject => {
          this.props.navigation.popToTop();
    });
  }

  isBookingExpired = (expiresAt) => {
    if (Moment(expiresAt).diff(Moment(), 'seconds') <= 0) {
      this.setState({ kdVoucher: '', codeVoucherApplied: null, messageVoucherCode: null, expiresAt });
      this.clearCountdown();
      this.props.navigation.popToTop();
      return true;
    }
    this.setState({ expiresAt });
    return false;
  }

  clearCountdown() {
    if (this.state.countdown !== null) clearInterval(this.state.countdown);
  }

  voucherCheck() {
    if (!this.state.kdVoucher) {
      this.setState({ voucherFailed: 'Masukkan kode voucher' });
      return;
    }
    if (this.state.voucherFailed) return;
    const voucherCheckId = this.state.voucherCheckId + 1;
    this.setState({  voucherCheckId }, () => {
      const voucherCode = this.state.kdVoucher.toUpperCase();
      graphClient.query({
        query: voucherCheckQuery,
        variables: {
          voucherCode,
          bookingId: this.props.booking.id,
          paymentId: this.props.navigation.state.params.payment.id
        }
      })
      .then(res => {
         if (res.data.voucherCheck && this.state.voucherCheckId === voucherCheckId) {
           this.setState({  modalVoucher: false,voucherFailed: false, appliedVoucher: { ...res.data.voucherCheck, voucherCode } });
         }else{
           this.setState({  modalVoucher: false, voucherFailed: true, appliedVoucher: null });
         }
      })
      .catch(reject => {

        this.setState({  voucherFailed: reject.graphQLErrors[0].message || 'Voucher Tidak Valid', appliedVoucher: null });
      });
    });
  }

  submit() {
    const { booking, payment } = this.props.navigation.state.params;
    this.openModal('loading');

    const variables = {
      bookingId: booking.id,
      paymentId: payment.id
    };
    
    if (this.state.appliedVoucher) variables.voucherCode = this.state.appliedVoucher.voucherCode;

    console.log(variables, 'variables ============================================================');

    graphClient
    .mutate({
      mutation: payQuery,
      variables
    })
    .then(res => {
      if (res.data.pay.redirectUrl === 'CONFIRMED') {
        this.setState({ loading: false }, () => this.props.navigation.navigate('PaymentSucceed', { bookingId: this.props.booking.id }));
      }
      else {
        this.setState({ loading: false, sourceURL: res.data.pay.redirectUrl }, () => {
          if (res.data.pay.transferAccountNumber) this.props.navigation.navigate('PaymentInstruction', { booking: this.props.booking, payment, payInfo: res.data.pay });
          else this.openModal('modalWebView');
        });
      }
    })
    .catch(reject => {
      console.log(JSON.stringify(reject.graphQLErrors[0].message), 'err pay');
      this.setState({ loading: false, messageRes: reject.graphQLErrors[0].message }, () => {
        setTimeout(() => {
          this.openModalTimer('modalFailedInformation');
        }, 500)
      });
    });
  }

  closeModalWebView = (status) => {
    this.setState({ modalWebView: false });
    if (status === 'paymentPaid') this.isPaid();
    else this.props.navigation.popToTop();
  }

  closeModalVoucher() {
    this.setState({ modalVoucher: false, voucherFailed: false });
  }

  renderModalVoucher() {
    return (
      <BackgroundModalVoucherView onPress={() => this.closeModalVoucher()}>
        <ModalVoucherView>
          <CloseModalVoucherView onPress={() => this.closeModalVoucher()}>
            <CloseIcon name='clear' />
          </CloseModalVoucherView>
          <HeaderModalVoucher>
            <NormalText type='bold'>Tambahkan Kode Voucher</NormalText>
          </HeaderModalVoucher>
          <VoucherRoundedView>
            <CustomTextInput
              // ref={(ref) => { this.VoucherTextInput = ref; }}
              style={{ width: '88%' }}
              placeholder='Kode voucher'
              placeholderTextColor='#DDDDDD'
              underlineColorAndroid='transparent'
              autoCorrect={false}
              value={this.state.kdVoucher}
              onChangeText={(text) => this.setState({ kdVoucher: text, voucherFailed: false })}
              selectionColor={Color.text}
            />
            {this.state.kdVoucher.length > 0 && <ResetTextVoucher onPress={() => this.setState({ voucherFailed: false, kdVoucher: '' })}>
              <CloseIcon name='clear' />
            </ResetTextVoucher>}
          </VoucherRoundedView>
          {!!this.state.voucherFailed && <ErrorView><ErrorMessage type='medium'>{this.state.voucherFailed}</ErrorMessage></ErrorView>}
          <VoucherButton onPress={() => this.voucherCheck()}>Gunakan</VoucherButton>
        </ModalVoucherView>
      </BackgroundModalVoucherView>
    );
  }

  renderSwitch(type, booking) {
    const property = { ...this.props, isComponent: true, booking };
    console.log(type);
    

    switch (type) {
      case 'flights': return <BookingDetailFlight { ...property } />;
      case 'tours': return <BookingDetailTravel { ...property } />;
      case 'attractions': return <BookingDetailAttraction { ...property } />;
      case 'buses': return <BookingDetailBus { ...property } />;
      case 'hotels': return <BookingDetailHotel { ...property } />;
      case 'PULSA_HP': return <BookingDetailPulsa { ...property } />;
      case 'PAKET_DATA': return <BookingDetailPaketData { ...property } />;
      case 'ONLINE_GAME': return <BookingDetailVoucherGame { ...property } />;
      case 'TOKEN_LISTRIK': return <BookingDetailPlnPrabayar { ...property } />;
      case 'PLNPOSTPAID': return <BookingDetailPlnPascabayar { ...property } />;
      case 'TELKOMPSTN': return <BookingDetailTeleponPascabayar { ...property } />;
      // case 'unknown': return this.renderPlnPrabayar();
      default: return <BookingDetailPdamPascabayar { ...property } />;    //////default pdam
    }
  }

  render() {
    console.log(this.props, 'props payment detail', this.state);
    
    const { navigation } = this.props;
    const { booking, bookingType } = navigation.state.params;
    const { expiresAt, appliedVoucher } = this.state;

    const upperAppliedVoucher = appliedVoucher ? appliedVoucher.voucherCode.toUpperCase() : '';
    const discount = -Math.abs(appliedVoucher ? appliedVoucher.discountAmount : booking.discount);
    const finalAmount = appliedVoucher ? appliedVoucher.finalAmount : booking.finalAmount;

    let durationNumber = Moment(expiresAt).diff(Moment(), 'seconds');
        hours = Moment.duration(durationNumber, 'seconds').hours();
        minutes = Moment.duration(durationNumber, 'seconds').minutes();
        seconds = Moment.duration(durationNumber, 'seconds').seconds();

    return (
      <MainView>
        <Header title='Review Pembayaran' />
        <ScrollView>
          <View style={{height: 80, backgroundColor: Color.theme, width: '100%'}}>
            <BaseText type='bold' style={{textAlign: 'left', marginLeft: 16, color: '#FFFFFF'}}>ORDER ID : {booking.invoiceNumber}</BaseText>
          </View>
          <AbsoluteView>
            <HeaderView>
              <BaseText type='bold'>Selesaikan pembayaran sebelum</BaseText>
              <BaseText type='bold'>{Moment(expiresAt).format('h[:]mm A DD MMM YYYY')}{'\n'}</BaseText>
              <BaseText type='bold'>{hours > 0 ? hours + ' Jam : ' : ''}{minutes > 0 ? minutes + ' Menit : ' : ''}{seconds + ' Detik'}</BaseText>
            </HeaderView>
          </AbsoluteView>
          <PriceView />

          <VoucherBackground>
            <VoucherView>
              <BaseText type='bold'>VOUCHER</BaseText>
            </VoucherView>
            <RightHeaderView>
              <VoucherView style={appliedVoucher && { paddingRight: 0 }} onPress={() => { this.setState({ kdVoucher: appliedVoucher ? upperAppliedVoucher : '' }, () => this.openModal('modalVoucher')); }}><BaseText type='bold' style={{ textDecorationLine: 'underline' }}>{appliedVoucher ? upperAppliedVoucher : 'GUNAKAN'}</BaseText></VoucherView>
              {appliedVoucher && <VoucherView onPress={() => this.setState({ appliedVoucher: null, kdVoucher: '' })}><SmallerCloseIcon name='clear' /></VoucherView>}
            </RightHeaderView>
          </VoucherBackground>

          {this.renderSwitch(bookingType, booking)}

        {/* <PriceViewWithBackground>
            <DetailPriceView>
              <BaseText type='bold'>RINCIAN HARGA</BaseText>
            </DetailPriceView>
            <DetailPriceView>
              <LeftHeaderView>
                <BaseText type='medium'>Sub Total</BaseText>
              </LeftHeaderView>
              <RightHeaderView>
                <BaseText type='medium'>{FormatMoney.getFormattedMoney(booking.amount)}</BaseText>
              </RightHeaderView>
            </DetailPriceView>
            <DetailPriceView>
              <LeftHeaderView>
                <BaseText type='medium'>Diskon</BaseText>
              </LeftHeaderView>
              <RightHeaderView>
                <BaseText type='medium'>{FormatMoney.getFormattedMoney(discount)}</BaseText>
              </RightHeaderView>
            </DetailPriceView>
            <DetailPriceView>
              <LeftHeaderView>
                <BaseText type='medium'>Ppn</BaseText>
              </LeftHeaderView>
              <RightHeaderView>
                <BaseText type='medium'>{FormatMoney.getFormattedMoney(booking.vat)}</BaseText>
              </RightHeaderView>
            </DetailPriceView>
            <DetailPriceView style={{borderTopWidth: 0.5, borderTopColor: '#DDDDDD', paddingTop: 16}}>
              <LeftHeaderView>
                <BaseText type='bold'>Total</BaseText>
              </LeftHeaderView>
              <RightHeaderView>
                <RedText type='bold'>{FormatMoney.getFormattedMoney(finalAmount)}</RedText>
              </RightHeaderView>
            </DetailPriceView>
          </PriceViewWithBackground> */}
        </ScrollView>

        <ButtonView>
          {/* <AgreementView>
            <BaseText align='left' type='medium'>By tapping the Button, you have agreed to {"Topupdulu's"} <UnderlineText type='medium'>Terms & Conditions</UnderlineText> and <UnderlineText type='medium'>Privacy Policy</UnderlineText></BaseText>
          </AgreementView> */}
          <ButtonRadius onPress={() => this.submit()}>
            <ButtonText>Berikutnya</ButtonText>
          </ButtonRadius>
        </ButtonView>

        <Modal transparent animationType='fade' visible={!!this.state.modalVoucher} onRequestClose={() => this.closeModalVoucher()}>
          {this.renderModalVoucher()}
        </Modal>

        {this.state.modalFailedInformation && (<Modal
          onRequestClose={() => {}}
          animationType="fade"
          transparent={true}
          visible={this.state.modalFailedInformation}
        >
          <ModalInformation
            label={this.state.messageRes}
            warning
          />
        </Modal>)}

        {this.state.loading && (<ModalIndicator
          visible={this.state.loading}
          type='large'
          message='Mohon Tunggu Sebentar'
          indicators='skype'
        />)}

        {this.state.modalWebView && (<Modal
          animationType="fade"
          transparent={true}
          onRequestClose={this.closeModalWebView}
          visible={this.state.modalWebView}>

          <WebViewScreen url={this.state.sourceURL} onClose={this.closeModalWebView} />
        </Modal>)}
      </MainView>
    );
  }
}

const mapStateToProps = state => {
  return {
    booking: state['booking'].booking,
  };
};

export default connect(mapStateToProps, null)(PaymentDetail);
