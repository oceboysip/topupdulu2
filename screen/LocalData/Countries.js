const Countries = [
  {
    id: 1,
    name: "South Africa's",
    isoCode2: 'ZA'
  },
  {
    id: 2,
    name: 'China',
    isoCode2: 'CN'
  },
  {
    id: 4,
    name: 'South Korea',
    isoCode2: 'KR'
  },
  {
    id: 5,
    name: 'Thailand',
    isoCode2: 'TH'
  },
  {
    id: 6,
    name: 'Malaysia',
    isoCode2: 'MY'
  },
  {
    id: 7,
    name: 'Singapore',
    isoCode2: 'SG'
  },
  {
    id: 8,
    name: 'Taiwan',
    isoCode2: 'TW'
  },
  {
    id: 9,
    name: 'Cambodia',
    isoCode2: 'KH'
  },
  {
    id: 10,
    name: 'Vietnam',
    isoCode2: 'VN'
  },
  {
    id: 11,
    name: 'Indonesia',
    isoCode2: 'ID'
  },
  {
    id: 12,
    name: 'Australia',
    isoCode2: 'AU'
  },
  {
    id: 13,
    name: 'New Zealand',
    isoCode2: 'NZ'
  },
  {
    id: 14,
    name: 'United States',
    isoCode2: 'US'
  },
  {
    id: 15,
    name: 'Canada',
    isoCode2: 'CA'
  },
  {
    id: 16,
    name: 'Austria',
    isoCode2: 'AT'
  },
  {
    id: 17,
    name: 'Czech Republic',
    isoCode2: 'CZ'
  },
  {
    id: 18,
    name: 'Denmark',
    isoCode2: 'DK'
  },
  {
    id: 19,
    name: 'France',
    isoCode2: 'FR'
  },
  {
    id: 20,
    name: 'Germany',
    isoCode2: 'DE'
  },
  {
    id: 21,
    name: 'Greece',
    isoCode2: 'GR'
  },
  {
    id: 22,
    name: 'Hungary',
    isoCode2: 'HU'
  },
  {
    id: 23,
    name: 'Italy',
    isoCode2: 'IT'
  },
  {
    id: 24,
    name: 'Netherlands',
    isoCode2: 'NL'
  },
  {
    id: 25,
    name: 'Norway',
    isoCode2: 'NO'
  },
  {
    id: 26,
    name: 'Portugal',
    isoCode2: 'PT'
  },
  {
    id: 27,
    name: 'Romania',
    isoCode2: 'RO'
  },
  {
    id: 28,
    name: 'Russian Federation',
    isoCode2: 'RU'
  },
  {
    id: 29,
    name: 'Spain',
    isoCode2: 'ES'
  },
  {
    id: 30,
    name: 'Switzerland',
    isoCode2: 'CH'
  },
  {
    id: 31,
    name: 'Sweden',
    isoCode2: 'SE'
  },
  {
    id: 32,
    name: 'Turkey',
    isoCode2: 'TR'
  },
  {
    id: 33,
    name: 'UK / Great Britain',
    isoCode2: 'GB'
  },
  {
    id: 34,
    name: 'Egypt',
    isoCode2: 'EG'
  },
  {
    id: 35,
    name: 'Uni Arab Emirates',
    isoCode2: 'AE'
  },
  {
    id: 36,
    name: 'Mexico',
    isoCode2: 'MX'
  },
  {
    id: 37,
    name: 'Argentina',
    isoCode2: 'AR'
  },
  {
    id: 38,
    name: 'Brazil',
    isoCode2: 'BR'
  },
  {
    id: 39,
    name: 'Belgium',
    isoCode2: 'BE'
  },
  {
    id: 40,
    name: 'Monaco',
    isoCode2: 'MC'
  },
  {
    id: 41,
    name: 'Morocco',
    isoCode2: 'MA'
  },
  {
    id: 42,
    name: 'Afghanistan',
    isoCode2: 'AF'
  },
  {
    id: 43,
    name: 'Algeria',
    isoCode2: 'DZ'
  },
  {
    id: 44,
    name: 'Bangladesh',
    isoCode2: 'BD'
  },
  {
    id: 45,
    name: 'Brunei Darussalam',
    isoCode2: 'BN'
  },
  {
    id: 46,
    name: 'Bulgaria',
    isoCode2: 'BG'
  },
  {
    id: 47,
    name: 'Chile',
    isoCode2: 'CL'
  },
  {
    id: 48,
    name: 'Colombia',
    isoCode2: 'CO'
  },
  {
    id: 49,
    name: 'Croatia',
    isoCode2: 'HR'
  },
  {
    id: 50,
    name: 'Cuba',
    isoCode2: 'CU'
  },
  {
    id: 51,
    name: 'East Timor',
    isoCode2: 'TM'
  },
  {
    id: 52,
    name: 'Finland',
    isoCode2: 'FI'
  },
  {
    id: 53,
    name: 'Serbia',
    isoCode2: 'RS'
  },
  {
    id: 54,
    name: 'Schengen',
    isoCode2: 'XX'
  },
  {
    id: 55,
    name: 'Apostolic Nuncio Vatican',
    isoCode2: 'VV'
  },
  {
    id: 56,
    name: 'Palestine',
    isoCode2: 'PL'
  },
  {
    id: 57,
    name: 'India',
    isoCode2: 'IN'
  },
  {
    id: 58,
    name: 'Iran',
    isoCode2: 'IR'
  },
  {
    id: 59,
    name: 'Iraq',
    isoCode2: 'IQ'
  },
  {
    id: 60,
    name: 'Jordan',
    isoCode2: 'JO'
  },
  {
    id: 61,
    name: 'North Korea',
    isoCode2: 'KP'
  },
  {
    id: 62,
    name: 'Kuwait',
    isoCode2: 'KW'
  },
  {
    id: 63,
    name: 'Lebanon',
    isoCode2: 'LB'
  },
  {
    id: 64,
    name: 'Mozambique',
    isoCode2: 'MZ'
  },
  {
    id: 65,
    name: 'Myanmar',
    isoCode2: 'MM'
  },
  {
    id: 66,
    name: 'Nigeria',
    isoCode2: 'NG'
  },
  {
    id: 67,
    name: 'Pakistan',
    isoCode2: 'PK'
  },
  {
    id: 68,
    name: 'Panama',
    isoCode2: 'PA'
  },
  {
    id: 69,
    name: 'Papua New Guinea',
    isoCode2: 'PG'
  },
  {
    id: 70,
    name: 'Peru',
    isoCode2: 'PE'
  },
  {
    id: 71,
    name: 'Philippines',
    isoCode2: 'PH'
  },
  {
    id: 72,
    name: 'Qatar',
    isoCode2: 'QA'
  },
  {
    id: 73,
    name: 'Saudi Arabia',
    isoCode2: 'SA'
  },
  {
    id: 74,
    name: 'Slovakia',
    isoCode2: 'SK'
  },
  {
    id: 75,
    name: 'Sri Lanka',
    isoCode2: 'LK'
  },
  {
    id: 76,
    name: 'Sudan',
    isoCode2: 'SD'
  },
  {
    id: 77,
    name: 'Syria',
    isoCode2: 'SY'
  },
  {
    id: 78,
    name: 'Tunisia',
    isoCode2: 'TN'
  },
  {
    id: 79,
    name: 'Ukraine',
    isoCode2: 'UA'
  },
  {
    id: 80,
    name: 'Uzbekistan',
    isoCode2: 'UZ'
  },
  {
    id: 81,
    name: 'Venezuela',
    isoCode2: 'VE'
  },
  {
    id: 82,
    name: 'Yemen',
    isoCode2: 'YE'
  },
  {
    id: 83,
    name: 'Zimbabwe',
    isoCode2: 'ZW'
  },
  {
    id: 84,
    name: 'Libya',
    isoCode2: 'LY'
  },
  {
    id: 85,
    name: 'Bosnia Herzegovina',
    isoCode2: 'BA'
  },
  {
    id: 86,
    name: 'Laos',
    isoCode2: 'LA'
  },
  {
    id: 87,
    name: 'Caribbean',
    isoCode2: 'CR'
  },
  {
    id: 88,
    name: 'Nepal',
    isoCode2: 'NP'
  },
  {
    id: 89,
    name: 'Bhutan',
    isoCode2: 'BT'
  },
  {
    id: 90,
    name: 'Ireland',
    isoCode2: 'IE'
  },
  {
    id: 91,
    name: 'Andorra',
    isoCode2: 'AD'
  },
  {
    id: 92,
    name: 'Antigua and Barbuda',
    isoCode2: 'AG'
  },
  {
    id: 93,
    name: 'Albania',
    isoCode2: 'AL'
  },
  {
    id: 94,
    name: 'Armenia',
    isoCode2: 'AM'
  },
  {
    id: 95,
    name: 'Netherlands Antilles',
    isoCode2: 'AN'
  },
  {
    id: 96,
    name: 'Angola',
    isoCode2: 'AO'
  },
  {
    id: 97,
    name: 'American Samoa',
    isoCode2: 'AS'
  },
  {
    id: 98,
    name: 'Aruba',
    isoCode2: 'AW'
  },
  {
    id: 99,
    name: 'Azerbaijan',
    isoCode2: 'AZ'
  },
  {
    id: 100,
    name: 'Barbados',
    isoCode2: 'BB'
  },
  {
    id: 101,
    name: 'Burkina Faso',
    isoCode2: 'BF'
  },
  {
    id: 102,
    name: 'Bahrain',
    isoCode2: 'BH'
  },
  {
    id: 103,
    name: 'Burundi',
    isoCode2: 'BI'
  },
  {
    id: 104,
    name: 'Benin',
    isoCode2: 'BJ'
  },
  {
    id: 105,
    name: 'Bonaire',
    isoCode2: 'BQ'
  },
  {
    id: 106,
    name: 'Bermuda',
    isoCode2: 'BM'
  },
  {
    id: 107,
    name: 'Bolivia',
    isoCode2: 'BO'
  },
  {
    id: 108,
    name: 'Bahamas',
    isoCode2: 'BS'
  },
  {
    id: 109,
    name: 'Botswana',
    isoCode2: 'BW'
  },
  {
    id: 110,
    name: 'Belarus',
    isoCode2: 'BY'
  },
  {
    id: 111,
    name: 'Belize',
    isoCode2: 'BZ'
  },
  {
    id: 112,
    name: 'Democratic Republic of the Congo',
    isoCode2: 'CD'
  },
  {
    id: 113,
    name: 'Cook Islands',
    isoCode2: 'CK'
  },
  {
    id: 114,
    name: 'Cameroon',
    isoCode2: 'CM'
  },
  {
    id: 115,
    name: 'Cape Verde',
    isoCode2: 'CV'
  },
  {
    id: 116,
    name: 'Cyprus',
    isoCode2: 'CY'
  },
  {
    id: 117,
    name: 'Dominica',
    isoCode2: 'DM'
  },
  {
    id: 118,
    name: 'Dominican Republic',
    isoCode2: 'DO'
  },
  {
    id: 119,
    name: 'Ecuador',
    isoCode2: 'EC'
  },
  {
    id: 120,
    name: 'Estonia',
    isoCode2: 'EE'
  },
  {
    id: 121,
    name: 'Eritrea',
    isoCode2: 'ER'
  },
  {
    id: 122,
    name: 'Ethiopia',
    isoCode2: 'ET'
  },
  {
    id: 123,
    name: 'Fiji',
    isoCode2: 'FJ'
  },
  {
    id: 124,
    name: 'Micronesia',
    isoCode2: 'FM'
  },
  {
    id: 125,
    name: 'Faroe Islands',
    isoCode2: 'FO'
  },
  {
    id: 126,
    name: 'Gabon',
    isoCode2: 'GA'
  },
  {
    id: 127,
    name: 'Grenada',
    isoCode2: 'GD'
  },
  {
    id: 128,
    name: 'Georgia',
    isoCode2: 'GE'
  },
  {
    id: 129,
    name: 'French Guiana',
    isoCode2: 'GF'
  },
  {
    id: 130,
    name: 'Ghana',
    isoCode2: 'GH'
  },
  {
    id: 131,
    name: 'Gibraltar',
    isoCode2: 'GI'
  },
  {
    id: 132,
    name: 'Greenland',
    isoCode2: 'GL'
  },
  {
    id: 133,
    name: 'Gambia',
    isoCode2: 'GM'
  },
  {
    id: 134,
    name: 'Guinea',
    isoCode2: 'GN'
  },
  {
    id: 135,
    name: 'Guadeloupe',
    isoCode2: 'GP'
  },
  {
    id: 136,
    name: 'Guatemala',
    isoCode2: 'GT'
  },
  {
    id: 137,
    name: 'Guam',
    isoCode2: 'GU'
  },
  {
    id: 138,
    name: 'Guinea-Bissau',
    isoCode2: 'GW'
  },
  {
    id: 139,
    name: 'Guyana',
    isoCode2: 'GY'
  },
  {
    id: 140,
    name: 'Hong Kong',
    isoCode2: 'HK'
  },
  {
    id: 141,
    name: 'Honduras',
    isoCode2: 'HN'
  },
  {
    id: 142,
    name: 'Haiti',
    isoCode2: 'HT'
  },
  {
    id: 143,
    name: 'Israel',
    isoCode2: 'IL'
  },
  {
    id: 144,
    name: 'Iceland',
    isoCode2: 'IS'
  },
  {
    id: 145,
    name: 'Jamaica',
    isoCode2: 'JM'
  },
  {
    id: 146,
    name: 'Kenya',
    isoCode2: 'KE'
  },
  {
    id: 147,
    name: 'Kyrgyzstan',
    isoCode2: 'KG'
  },
  {
    id: 148,
    name: 'Comoros',
    isoCode2: 'KM'
  },
  {
    id: 149,
    name: 'Saint Kitts and Nevis',
    isoCode2: 'KN'
  },
  {
    id: 150,
    name: 'Cayman Islands',
    isoCode2: 'KY'
  },
  {
    id: 151,
    name: 'Kazakhstan',
    isoCode2: 'KZ'
  },
  {
    id: 152,
    name: 'Saint Lucia',
    isoCode2: 'LC'
  },
  {
    id: 153,
    name: 'Liechtenstein',
    isoCode2: 'LI'
  },
  {
    id: 154,
    name: 'Liberia',
    isoCode2: 'LR'
  },
  {
    id: 155,
    name: 'Lithuania',
    isoCode2: 'LT'
  },
  {
    id: 156,
    name: 'Luxembourg',
    isoCode2: 'LU'
  },
  {
    id: 157,
    name: 'Latvia',
    isoCode2: 'LV'
  },
  {
    id: 158,
    name: 'Moldova',
    isoCode2: 'MD'
  },
  {
    id: 159,
    name: 'Montenegro',
    isoCode2: 'ME'
  },
  {
    id: 160,
    name: 'Madagascar',
    isoCode2: 'MG'
  },
  {
    id: 161,
    name: 'Macedonia',
    isoCode2: 'MK'
  },
  {
    id: 162,
    name: 'Mali',
    isoCode2: 'ML'
  },
  {
    id: 163,
    name: 'Mongolia',
    isoCode2: 'MN'
  },
  {
    id: 164,
    name: 'Macau',
    isoCode2: 'MO'
  },
  {
    id: 165,
    name: 'Saipan',
    isoCode2: 'MP'
  },
  {
    id: 166,
    name: 'Mauritania',
    isoCode2: 'MR'
  },
  {
    id: 167,
    name: 'Malta',
    isoCode2: 'MT'
  },
  {
    id: 168,
    name: 'Mauritius',
    isoCode2: 'MU'
  },
  {
    id: 169,
    name: 'Maldives',
    isoCode2: 'MV'
  },
  {
    id: 170,
    name: 'Malawi',
    isoCode2: 'MW'
  },
  {
    id: 171,
    name: 'Namibia',
    isoCode2: 'NA'
  },
  {
    id: 172,
    name: 'New Caledonia',
    isoCode2: 'NC'
  },
  {
    id: 173,
    name: 'Niger',
    isoCode2: 'NE'
  },
  {
    id: 174,
    name: 'Norfolk Island',
    isoCode2: 'NF'
  },
  {
    id: 175,
    name: 'Nicaragua',
    isoCode2: 'NI'
  },
  {
    id: 176,
    name: 'Niue',
    isoCode2: 'NU'
  },
  {
    id: 177,
    name: 'Oman',
    isoCode2: 'OM'
  },
  {
    id: 178,
    name: 'French Polynesia',
    isoCode2: 'PF'
  },
  {
    id: 179,
    name: 'Puerto Rico',
    isoCode2: 'PR'
  },
  {
    id: 180,
    name: 'Palau',
    isoCode2: 'PW'
  },
  {
    id: 181,
    name: 'Paraguay',
    isoCode2: 'PY'
  },
  {
    id: 182,
    name: 'Reunion',
    isoCode2: 'RE'
  },
  {
    id: 183,
    name: 'Rwanda',
    isoCode2: 'RW'
  },
  {
    id: 184,
    name: 'Solomon Islands',
    isoCode2: 'SB'
  },
  {
    id: 185,
    name: 'Seychelles',
    isoCode2: 'SC'
  },
  {
    id: 186,
    name: 'Slovenia',
    isoCode2: 'SI'
  },
  {
    id: 187,
    name: 'Sierra Leone',
    isoCode2: 'SL'
  },
  {
    id: 188,
    name: 'San Marino',
    isoCode2: 'SM'
  },
  {
    id: 189,
    name: 'Senegal',
    isoCode2: 'SN'
  },
  {
    id: 190,
    name: 'Sao Tome and Principe',
    isoCode2: 'ST'
  },
  {
    id: 191,
    name: 'El Salvador',
    isoCode2: 'SV'
  },
  {
    id: 192,
    name: 'Swaziland',
    isoCode2: 'SZ'
  },
  {
    id: 193,
    name: 'Turks and Caicos Islands',
    isoCode2: 'TC'
  },
  {
    id: 194,
    name: 'Chad',
    isoCode2: 'TD'
  },
  {
    id: 195,
    name: 'Togo',
    isoCode2: 'TG'
  },
  {
    id: 196,
    name: 'Tajikistan',
    isoCode2: 'TJ'
  },
  {
    id: 197,
    name: 'Tonga',
    isoCode2: 'TO'
  },
  {
    id: 198,
    name: 'Trinidad and Tobago',
    isoCode2: 'TT'
  },
  {
    id: 199,
    name: 'Tanzania',
    isoCode2: 'TZ'
  },
  {
    id: 200,
    name: 'Uganda',
    isoCode2: 'UG'
  },
  {
    id: 201,
    name: 'Uruguay',
    isoCode2: 'UY'
  },
  {
    id: 202,
    name: 'Saint Vincent and the Grenadines',
    isoCode2: 'VC'
  },
  {
    id: 203,
    name: 'Virgin Islands, UK',
    isoCode2: 'VG'
  },
  {
    id: 204,
    name: 'Virgin Islands, US',
    isoCode2: 'VI'
  },
  {
    id: 205,
    name: 'Vanuatu',
    isoCode2: 'VU'
  },
  {
    id: 206,
    name: 'Samoa',
    isoCode2: 'WS'
  },
  {
    id: 207,
    name: 'Zambia',
    isoCode2: 'ZM'
  },
  {
    id: 208,
    name: 'Western Sahara',
    isoCode2: 'EH'
  },
  {
    id: 209,
    name: 'Falkland Islands',
    isoCode2: 'FK'
  },
  {
    id: 210,
    name: 'Guernsey',
    isoCode2: 'GG'
  },
  {
    id: 211,
    name: 'Isle of Man',
    isoCode2: 'IM'
  },
  {
    id: 212,
    name: 'Jersey',
    isoCode2: 'JE'
  },
  {
    id: 213,
    name: 'Kiribati',
    isoCode2: 'KI'
  },
  {
    id: 214,
    name: 'Lesotho',
    isoCode2: 'LS'
  },
  {
    id: 215,
    name: 'Marshall Islands',
    isoCode2: 'MH'
  },
  {
    id: 216,
    name: 'Montserrat',
    isoCode2: 'MS'
  },
  {
    id: 217,
    name: 'Martinique',
    isoCode2: 'MQ'
  },
  {
    id: 218,
    name: 'Mayotte',
    isoCode2: 'YT'
  },
  {
    id: 219,
    name: 'Nauru',
    isoCode2: 'NR'
  },
  {
    id: 220,
    name: 'Palestine',
    isoCode2: 'PS'
  },
  {
    id: 221,
    name: 'South Georgia and the Islands',
    isoCode2: 'GS'
  },
  {
    id: 222,
    name: 'Saint Helena',
    isoCode2: 'SH'
  },
  {
    id: 223,
    name: 'Svalbard',
    isoCode2: 'SJ'
  },
  {
    id: 224,
    name: 'Somalia',
    isoCode2: 'SO'
  },
  {
    id: 225,
    name: 'Saint Pierre and Miquelon',
    isoCode2: 'PM'
  },
  {
    id: 226,
    name: 'Suriname',
    isoCode2: 'SR'
  },
  {
    id: 227,
    name: 'Turkmenistan',
    isoCode2: 'TM'
  },
  {
    id: 228,
    name: 'Tuvalu',
    isoCode2: 'TV'
  },
  {
    id: 229,
    name: 'United States Minor Outlying Islands',
    isoCode2: 'UM'
  },
  {
    id: 230,
    name: 'Wallis and Futuna',
    isoCode2: 'WF'
  },
  {
    id: 231,
    name: 'Anguilla',
    isoCode2: 'AI'
  },
  {
    id: 232,
    name: 'Antarctica ',
    isoCode2: 'AQ'
  },
  {
    id: 233,
    name: 'Cocos (Keeling) Islands',
    isoCode2: 'CC'
  },
  {
    id: 234,
    name: 'Central African Republic',
    isoCode2: 'CF'
  },
  {
    id: 235,
    name: 'Congo (Brazzaville)',
    isoCode2: 'CG'
  },
  {
    id: 236,
    name: "Cote d'Ivoire",
    isoCode2: 'CI'
  },
  {
    id: 237,
    name: 'Christmas Island',
    isoCode2: 'CX'
  },
  {
    id: 238,
    name: 'Djibouti',
    isoCode2: 'DJ'
  },
  {
    id: 239,
    name: 'Equatorial Guinea',
    isoCode2: 'GQ'
  },
  {
    id: 240,
    name: 'British Indian Ocean Territory',
    isoCode2: 'IO'
  },
  {
    id: 242,
    name: 'South Sudan',
    isoCode2: 'SS'
  },
  {
    id: 243,
    name: 'French Southern Territories',
    isoCode2: 'TF'
  },
  {
    id: 244,
    name: 'Tokelau ',
    isoCode2: 'TK'
  },
  {
    id: 3,
    name: 'Japan',
    isoCode2: 'JP'
  }
];

export default Countries;
