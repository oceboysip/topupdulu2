const Airports =  [
  {
    "name": "Aalborg",
    "cityName": "Aalborg",
    "code": "AAL"
  },
  {
    "name": "Albuquerque Sunport",
    "cityName": "Albuquerque",
    "code": "ABQ"
  },
  {
    "name": "Haliwen Airport",
    "cityName": "Atambua",
    "code": "ABU"
  },
  {
    "name": "Nnamdi Azikiwe",
    "cityName": "Abuja",
    "code": "ABV"
  },
  {
    "name": "Aberdeen Dyce",
    "cityName": "Aberdeen",
    "code": "ABZ"
  },
  {
    "name": "General Juan N Alvarez",
    "cityName": "Acapulco",
    "code": "ACA"
  },
  {
    "name": "Kotoka",
    "cityName": "Accra",
    "code": "ACC"
  },
  {
    "name": "Adana",
    "cityName": "Adana",
    "code": "ADA"
  },
  {
    "name": "Adnan Menderes",
    "cityName": "Äzmir",
    "code": "ADB"
  },
  {
    "name": "Addis Ababa Bole",
    "cityName": "Addis Ababa",
    "code": "ADD"
  },
  {
    "name": "Adelaide International Airport",
    "cityName": "Adelaide",
    "code": "ADL"
  },
  {
    "name": "Andrews Air Force Base",
    "cityName": "Camp Springs",
    "code": "ADW"
  },
  {
    "name": "Aek Godang Airport",
    "cityName": "Padang Sidempuan",
    "code": "AEG"
  },
  {
    "name": "Sochi",
    "cityName": "Sochi",
    "code": "AER"
  },
  {
    "name": "Fort Worth Alliance",
    "cityName": "Fort Worth",
    "code": "AFW"
  },
  {
    "name": "Malaga",
    "cityName": "Malaga",
    "code": "AGP"
  },
  {
    "name": "Augusta Regional At Bush Field",
    "cityName": "Augusta",
    "code": "AGS"
  },
  {
    "name": "Amahai Airport",
    "cityName": "Amahai",
    "code": "AHI"
  },
  {
    "name": "Auckland International Airport",
    "cityName": "Auckland",
    "code": "AKL"
  },
  {
    "name": "Almaty",
    "cityName": "Almaty",
    "code": "ALA"
  },
  {
    "name": "Alicante",
    "cityName": "Alicante",
    "code": "ALC"
  },
  {
    "name": "Houari Boumediene",
    "cityName": "Algiers",
    "code": "ALG"
  },
  {
    "name": "Aleppo",
    "cityName": "Aleppo",
    "code": "ALP"
  },
  {
    "name": "Rick Husband Amarillo",
    "cityName": "Amarillo",
    "code": "AMA"
  },
  {
    "name": "Selaparang Airport",
    "cityName": "Mataram",
    "code": "AMI"
  },
  {
    "name": "Queen Alia",
    "cityName": "Amman",
    "code": "AMM"
  },
  {
    "name": "Pattimura Airport",
    "cityName": "Ambon",
    "code": "AMQ"
  },
  {
    "name": "Amsterdam Schiphol",
    "cityName": "Amsterdam",
    "code": "AMS"
  },
  {
    "name": "Ted Stevens Anchorage",
    "cityName": "Anchorage",
    "code": "ANC"
  },
  {
    "name": "Mali Airport",
    "cityName": "Alor",
    "code": "ARD"
  },
  {
    "name": "Stockholm-Arlanda",
    "cityName": "Stockholm",
    "code": "ARN"
  },
  {
    "name": "Eleftherios Venizelos",
    "cityName": "Athens",
    "code": "ATH"
  },
  {
    "name": "Hartsfield Jackson Atlanta",
    "cityName": "Atlanta",
    "code": "ATL"
  },
  {
    "name": "Sri Guru Ram Dass Jee",
    "cityName": "Amritsar",
    "code": "ATQ"
  },
  {
    "name": "Abu Dhabi",
    "cityName": "Abu Dhabi",
    "code": "AUH"
  },
  {
    "name": "Austin Bergstrom",
    "cityName": "Austin",
    "code": "AUS"
  },
  {
    "name": "Asheville Regional",
    "cityName": "Asheville",
    "code": "AVL"
  },
  {
    "name": "Antalya",
    "cityName": "Antalya",
    "code": "AYT"
  },
  {
    "name": "Beale Air Force Base",
    "cityName": "Marysville",
    "code": "BAB"
  },
  {
    "name": "Barksdale Air Force Base",
    "cityName": "Bossier City",
    "code": "BAD"
  },
  {
    "name": "Bahrain",
    "cityName": "Manama",
    "code": "BAH"
  },
  {
    "name": "Barcelona",
    "cityName": "Barcelona",
    "code": "BCN"
  },
  {
    "name": "Syamsudin Noor Airport",
    "cityName": "Banjarmasin",
    "code": "BDJ"
  },
  {
    "name": "Bradley",
    "cityName": "Hartford",
    "code": "BDL"
  },
  {
    "name": "Husein Sastranegara International Airport",
    "cityName": "Bandung",
    "code": "BDO"
  },
  {
    "name": "Belgrade Nikola Tesla",
    "cityName": "Belgrade",
    "code": "BEG"
  },
  {
    "name": "Kalimarau-Hero Domestic Airport",
    "cityName": "Berau",
    "code": "BEJ"
  },
  {
    "name": "Val de Cans",
    "cityName": "Belém",
    "code": "BEL"
  },
  {
    "name": "Beirut Rafic Hariri",
    "cityName": "Beirut",
    "code": "BEY"
  },
  {
    "name": "Boeing Field King County",
    "cityName": "Seattle",
    "code": "BFI"
  },
  {
    "name": "Belfast",
    "cityName": "Belfast",
    "code": "BFS"
  },
  {
    "name": "Bergen, Flesland",
    "cityName": "Bergen",
    "code": "BGO"
  },
  {
    "name": "Bangor",
    "cityName": "Bangor",
    "code": "BGR"
  },
  {
    "name": "Baghdad",
    "cityName": "Baghdad",
    "code": "BGW"
  },
  {
    "name": "Il Caravaggio",
    "cityName": "Bergamo",
    "code": "BGY"
  },
  {
    "name": "George Best Belfast City",
    "cityName": "Belfast",
    "code": "BHD"
  },
  {
    "name": "Birmingham-Shuttlesworth",
    "cityName": "Birmingham",
    "code": "BHM"
  },
  {
    "name": "Birmingham",
    "cityName": "Birmingham",
    "code": "BHX"
  },
  {
    "name": "Frans Kaisiepo Airport",
    "cityName": "Biak",
    "code": "BIK"
  },
  {
    "name": "Billings Logan",
    "cityName": "Billings",
    "code": "BIL"
  },
  {
    "name": "Benjina Airport",
    "cityName": "Benjina",
    "code": "BJK"
  },
  {
    "name": "Milas Bodrum",
    "cityName": "Bodrum",
    "code": "BJV"
  },
  {
    "name": "Bajawa Soa Airport",
    "cityName": "Bajawa",
    "code": "BJW"
  },
  {
    "name": "Kota Kinabalu International Airport",
    "cityName": "Kota Kinabalu",
    "code": "BKI"
  },
  {
    "name": "Bangkok Suvarnabhumi Airport",
    "cityName": "Bangkok Suvarnabhumi",
    "code": "BKK"
  },
  {
    "name": "Fatmawati Soekarno Airport",
    "cityName": "Bengkulu",
    "code": "BKS"
  },
  {
    "name": "Billund",
    "cityName": "Billund",
    "code": "BLL"
  },
  {
    "name": "Bologna Guglielmo Marconi",
    "cityName": "Bologna",
    "code": "BLQ"
  },
  {
    "name": "Bengaluru",
    "cityName": "Bangalore",
    "code": "BLR"
  },
  {
    "name": "Scott AFB/Midamerica",
    "cityName": "Belleville",
    "code": "BLV"
  },
  {
    "name": "Central Illinois Regional at Bloomington-Normal",
    "cityName": "Bloomington/normal",
    "code": "BMI"
  },
  {
    "name": "Sultan M. Salahuddin Airport",
    "cityName": "Bima",
    "code": "BMU"
  },
  {
    "name": "Nashville",
    "cityName": "Nashville",
    "code": "BNA"
  },
  {
    "name": "Brisbane",
    "cityName": "Brisbane",
    "code": "BNE"
  },
  {
    "name": "Bordeaux-Merignac",
    "cityName": "Bordeaux/merignac",
    "code": "BOD"
  },
  {
    "name": "El Dorado",
    "cityName": "Bogota",
    "code": "BOG"
  },
  {
    "name": "Bournemouth",
    "cityName": "Bournemouth",
    "code": "BOH"
  },
  {
    "name": "Boise Air Terminal/Gowen field",
    "cityName": "Boise",
    "code": "BOI"
  },
  {
    "name": "Burgas",
    "cityName": "Burgas",
    "code": "BOJ"
  },
  {
    "name": "Chhatrapati Shivaji International Airport",
    "cityName": "Mumbai",
    "code": "BOM"
  },
  {
    "name": "Bodø",
    "cityName": "Bodø",
    "code": "BOO"
  },
  {
    "name": "General Edward Lawrence Logan",
    "cityName": "Boston",
    "code": "BOS"
  },
  {
    "name": "Sepingan Airport",
    "cityName": "Balikpapan",
    "code": "BPN"
  },
  {
    "name": "Bremen",
    "cityName": "Bremen",
    "code": "BRE"
  },
  {
    "name": "Bari Karol Wojtyla",
    "cityName": "Bari",
    "code": "BRI"
  },
  {
    "name": "Bristol",
    "cityName": "Bristol",
    "code": "BRS"
  },
  {
    "name": "Brussels",
    "cityName": "Brussels",
    "code": "BRU"
  },
  {
    "name": "Presidente Juscelino Kubistschek",
    "cityName": "Brasilia",
    "code": "BSB"
  },
  {
    "name": "EuroAirport Basel-Mulhouse-Freiburg",
    "cityName": "Basel",
    "code": "BSL"
  },
  {
    "name": "Basrah",
    "cityName": "Basrah",
    "code": "BSR"
  },
  {
    "name": "Hang Nadim Airport",
    "cityName": "Batam",
    "code": "BTH"
  },
  {
    "name": "Sultan Lskandarmuda Airport",
    "cityName": "Banda Aceh",
    "code": "BTJ"
  },
  {
    "name": "Baton Rouge Metropolitan, Ryan Field",
    "cityName": "Baton Rouge",
    "code": "BTR"
  },
  {
    "name": "Bratislava",
    "cityName": "Bratislava",
    "code": "BTS"
  },
  {
    "name": "Budapest Ferenc Liszt",
    "cityName": "Budapest",
    "code": "BUD"
  },
  {
    "name": "Buffalo Niagara",
    "cityName": "Buffalo",
    "code": "BUF"
  },
  {
    "name": "Betoambari Airport",
    "cityName": "Baubau",
    "code": "BUW"
  },
  {
    "name": "Baltimore/Washington Thurgood Marshall",
    "cityName": "Baltimore",
    "code": "BWI"
  },
  {
    "name": "Brunei",
    "cityName": "Bandar Seri Begawan",
    "code": "BWN"
  },
  {
    "name": "Blimbingsari Airport",
    "cityName": "Banyuwangi",
    "code": "BWX"
  },
  {
    "name": "Bontang Airport",
    "cityName": "Bontang",
    "code": "BXT"
  },
  {
    "name": "Philip S. W. Goldson",
    "cityName": "Belize City",
    "code": "BZE"
  },
  {
    "name": "RAF Brize Norton",
    "cityName": "Brize Norton",
    "code": "BZZ"
  },
  {
    "name": "Columbia Metropolitan",
    "cityName": "Columbia",
    "code": "CAE"
  },
  {
    "name": "Cagliari Elmas",
    "cityName": "Cagliari",
    "code": "CAG"
  },
  {
    "name": "Cairo",
    "cityName": "Cairo",
    "code": "CAI"
  },
  {
    "name": "Guangzhou Baiyun",
    "cityName": "Guangzhou",
    "code": "CAN"
  },
  {
    "name": "Columbus Air Force Base",
    "cityName": "Columbus",
    "code": "CBM"
  },
  {
    "name": "Cakrabhuwana Airport",
    "cityName": "Cirebon",
    "code": "CBN"
  },
  {
    "name": "Canberra",
    "cityName": "Canberra",
    "code": "CBR"
  },
  {
    "name": "Calicut",
    "cityName": "Calicut",
    "code": "CCJ"
  },
  {
    "name": "Netaji Subhash Chandra Bose",
    "cityName": "Kolkata",
    "code": "CCU"
  },
  {
    "name": "Charles de Gaulle",
    "cityName": "Paris",
    "code": "CDG"
  },
  {
    "name": "Mactan Cebu",
    "cityName": "Lapu-lapu City",
    "code": "CEB"
  },
  {
    "name": "Congonhas",
    "cityName": "São Paulo",
    "code": "CGH"
  },
  {
    "name": "Soekarno Hatta",
    "cityName": "Jakarta",
    "code": "CGK"
  },
  {
    "name": "Cologne Bonn",
    "cityName": "Cologne",
    "code": "CGN"
  },
  {
    "name": "Zhengzhou Xinzheng",
    "cityName": "Zhengzhou",
    "code": "CGO"
  },
  {
    "name": "Laguindingan",
    "cityName": "Cagayan De Oro City",
    "code": "CGY"
  },
  {
    "name": "Lovell Field",
    "cityName": "Chattanooga",
    "code": "CHA"
  },
  {
    "name": "Christchurch International Airport",
    "cityName": "Christchurch",
    "code": "CHC"
  },
  {
    "name": "Charleston Air Force Base-International",
    "cityName": "Charleston",
    "code": "CHS"
  },
  {
    "name": "Ciampino-G. B. Pastine",
    "cityName": "Roma",
    "code": "CIA"
  },
  {
    "name": "The Eastern Iowa",
    "cityName": "Cedar Rapids",
    "code": "CID"
  },
  {
    "name": "Cheongju",
    "cityName": "Cheongju",
    "code": "CJJ"
  },
  {
    "name": "Jeju",
    "cityName": "Jeju City",
    "code": "CJU"
  },
  {
    "name": "Chongqing Jiangbei",
    "cityName": "Chongqing",
    "code": "CKG"
  },
  {
    "name": "Chkalovskiy",
    "cityName": "Moscow",
    "code": "CKL"
  },
  {
    "name": "Cleveland Hopkins",
    "cityName": "Cleveland",
    "code": "CLE"
  },
  {
    "name": "Charlotte Douglas",
    "cityName": "Charlotte",
    "code": "CLT"
  },
  {
    "name": "Colombo",
    "cityName": "Colombo",
    "code": "CMB"
  },
  {
    "name": "Port Columbus",
    "cityName": "Columbus",
    "code": "CMH"
  },
  {
    "name": "Mohammed V",
    "cityName": "Casablanca",
    "code": "CMN"
  },
  {
    "name": "Tancredo Neves",
    "cityName": "Belo Horizonte",
    "code": "CNF"
  },
  {
    "name": "Chiang Mai",
    "cityName": "Chiang Mai",
    "code": "CNX"
  },
  {
    "name": "Cochin",
    "cityName": "Cochin",
    "code": "COK"
  },
  {
    "name": "City of Colorado Springs Municipal",
    "cityName": "Colorado Springs",
    "code": "COS"
  },
  {
    "name": "Copenhagen Kastrup",
    "cityName": "Copenhagen",
    "code": "CPH"
  },
  {
    "name": "Casper-Natrona County",
    "cityName": "Casper",
    "code": "CPR"
  },
  {
    "name": "Cape Town",
    "cityName": "Cape Town",
    "code": "CPT"
  },
  {
    "name": "Clark",
    "cityName": "Angeles City",
    "code": "CRK"
  },
  {
    "name": "Brussels South Charleroi",
    "cityName": "Brussels",
    "code": "CRL"
  },
  {
    "name": "Corpus Christi",
    "cityName": "Corpus Christi",
    "code": "CRP"
  },
  {
    "name": "Yeager",
    "cityName": "Charleston",
    "code": "CRW"
  },
  {
    "name": "Changsha Huanghua",
    "cityName": "Changsha",
    "code": "CSX"
  },
  {
    "name": "Catania-Fontanarossa",
    "cityName": "Catania",
    "code": "CTA"
  },
  {
    "name": "New Chitose",
    "cityName": "Chitose / Tomakomai",
    "code": "CTS"
  },
  {
    "name": "Chengdu Shuangliu",
    "cityName": "Chengdu",
    "code": "CTU"
  },
  {
    "name": "Cuneo",
    "cityName": "Cuneo",
    "code": "CUF"
  },
  {
    "name": "Cancún",
    "cityName": "Cancún",
    "code": "CUN"
  },
  {
    "name": "Alejandro Velasco Astete",
    "cityName": "Cusco",
    "code": "CUZ"
  },
  {
    "name": "Cincinnati Northern Kentucky",
    "cityName": "Cincinnati",
    "code": "CVG"
  },
  {
    "name": "Cannon Air Force Base",
    "cityName": "Clovis",
    "code": "CVS"
  },
  {
    "name": "Afonso Pena",
    "cityName": "Curitiba",
    "code": "CWB"
  },
  {
    "name": "Cardiff",
    "cityName": "Cardiff",
    "code": "CWL"
  },
  {
    "name": "Tunggul Wulung Airport",
    "cityName": "Cilacap",
    "code": "CXP"
  },
  {
    "name": "Cam Ranh International Airport",
    "cityName": "Cam Ranh",
    "code": "CXR"
  },
  {
    "name": "Da Nang",
    "cityName": "Da Nang",
    "code": "DAD"
  },
  {
    "name": "Dallas Love Field",
    "cityName": "Dallas",
    "code": "DAL"
  },
  {
    "name": "Damascus",
    "cityName": "Damascus",
    "code": "DAM"
  },
  {
    "name": "James M Cox Dayton",
    "cityName": "Dayton",
    "code": "DAY"
  },
  {
    "name": "Dubuque Regional",
    "cityName": "Dubuque",
    "code": "DBQ"
  },
  {
    "name": "Ronald Reagan Washington National",
    "cityName": "Washington",
    "code": "DCA"
  },
  {
    "name": "Indira Gandhi",
    "cityName": "New Delhi",
    "code": "DEL"
  },
  {
    "name": "Denver",
    "cityName": "Denver",
    "code": "DEN"
  },
  {
    "name": "Dallas Fort Worth",
    "cityName": "Dallas-fort Worth",
    "code": "DFW"
  },
  {
    "name": "King Abdulaziz Air Base",
    "cityName": "Dhahran",
    "code": "DHA"
  },
  {
    "name": "Presidente Nicolau Lobato International Airport",
    "cityName": "Timor Leste",
    "code": "DIL"
  },
  {
    "name": "Sultan Taha Syarifudn Airport",
    "cityName": "Jambi",
    "code": "DJB"
  },
  {
    "name": "Sentani Airport",
    "cityName": "Jayapura",
    "code": "DJJ"
  },
  {
    "name": "Léopold Sédar Senghor",
    "cityName": "Dakar",
    "code": "DKR"
  },
  {
    "name": "Zhoushuizi",
    "cityName": "Dalian",
    "code": "DLC"
  },
  {
    "name": "Laughlin Air Force Base",
    "cityName": "Del Rio",
    "code": "DLF"
  },
  {
    "name": "Duluth",
    "cityName": "Duluth",
    "code": "DLH"
  },
  {
    "name": "Dalaman",
    "cityName": "Dalaman",
    "code": "DLM"
  },
  {
    "name": "Domodedovo",
    "cityName": "Moscow",
    "code": "DME"
  },
  {
    "name": "Don Mueang",
    "cityName": "Bangkok",
    "code": "DMK"
  },
  {
    "name": "King Fahd",
    "cityName": "Ad Dammam",
    "code": "DMM"
  },
  {
    "name": "Rar Gwamar Airport",
    "cityName": "Dobo",
    "code": "DOB"
  },
  {
    "name": "Hamad",
    "cityName": "Doha",
    "code": "DOH"
  },
  {
    "name": "Donetsk",
    "cityName": "Donetsk",
    "code": "DOK"
  },
  {
    "name": "Dover Air Force Base",
    "cityName": "Dover",
    "code": "DOV"
  },
  {
    "name": "Ngurah Rai (Bali)",
    "cityName": "Denpasar-bali Island",
    "code": "DPS"
  },
  {
    "name": "Dresden",
    "cityName": "Dresden",
    "code": "DRS"
  },
  {
    "name": "Darwin International Airport",
    "cityName": "Darwin",
    "code": "DRW"
  },
  {
    "name": "Robin Hood Doncaster Sheffield",
    "cityName": "Doncaster",
    "code": "DSA"
  },
  {
    "name": "Des Moines",
    "cityName": "Des Moines",
    "code": "DSM"
  },
  {
    "name": "Silangit Airport",
    "cityName": "Silangit",
    "code": "DTB"
  },
  {
    "name": "Datadawai Airport",
    "cityName": "Datadawai",
    "code": "DTD"
  },
  {
    "name": "Dortmund",
    "cityName": "Dortmund",
    "code": "DTM"
  },
  {
    "name": "Detroit Metropolitan Wayne County",
    "cityName": "Detroit",
    "code": "DTW"
  },
  {
    "name": "Dublin",
    "cityName": "Dublin",
    "code": "DUB"
  },
  {
    "name": "King Shaka",
    "cityName": "Durban",
    "code": "DUR"
  },
  {
    "name": "Düsseldorf",
    "cityName": "Düsseldorf",
    "code": "DUS"
  },
  {
    "name": "Francisco Bangoy",
    "cityName": "Davao City",
    "code": "DVO"
  },
  {
    "name": "Al Maktoum",
    "cityName": "Jebel Ali",
    "code": "DWC"
  },
  {
    "name": "Dubai",
    "cityName": "Dubai",
    "code": "DXB"
  },
  {
    "name": "Dyess Air Force Base",
    "cityName": "Abilene",
    "code": "DYS"
  },
  {
    "name": "Edinburgh",
    "cityName": "Edinburgh",
    "code": "EDI"
  },
  {
    "name": "Eindhoven",
    "cityName": "Eindhoven",
    "code": "EIN"
  },
  {
    "name": "East Midlands",
    "cityName": "Nottingham",
    "code": "EMA"
  },
  {
    "name": "Vance Air Force Base",
    "cityName": "Enid",
    "code": "END"
  },
  {
    "name": "H.Hasan Aroeboesman Airport",
    "cityName": "Ende",
    "code": "ENE"
  },
  {
    "name": "Erie Tom Ridge Field",
    "cityName": "Erie",
    "code": "ERI"
  },
  {
    "name": "Erzurum",
    "cityName": "Erzurum",
    "code": "ERZ"
  },
  {
    "name": "Esenboga",
    "cityName": "Ankara",
    "code": "ESB"
  },
  {
    "name": "Zvartnots",
    "cityName": "Yerevan",
    "code": "EVN"
  },
  {
    "name": "Newark Liberty",
    "cityName": "Newark",
    "code": "EWR"
  },
  {
    "name": "Exeter",
    "cityName": "Exeter",
    "code": "EXT"
  },
  {
    "name": "Ministro Pistarini",
    "cityName": "Buenos Aires",
    "code": "EZE"
  },
  {
    "name": "Fairbanks",
    "cityName": "Fairbanks",
    "code": "FAI"
  },
  {
    "name": "Faro",
    "cityName": "Faro",
    "code": "FAO"
  },
  {
    "name": "Leonardo da Vinci-Fiumicino",
    "cityName": "Rome",
    "code": "FCO"
  },
  {
    "name": "RAF Fairford",
    "cityName": "Fairford",
    "code": "FFD"
  },
  {
    "name": "Wright-Patterson Air Force Base",
    "cityName": "Dayton",
    "code": "FFO"
  },
  {
    "name": "Karlsruhe Baden-Baden",
    "cityName": "Baden-baden",
    "code": "FKB"
  },
  {
    "name": "Torea Airport",
    "cityName": "Fakfak",
    "code": "FKQ"
  },
  {
    "name": "Fort Lauderdale Hollywood",
    "cityName": "Fort Lauderdale",
    "code": "FLL"
  },
  {
    "name": "Hercílio  Luz",
    "cityName": "Florianópolis",
    "code": "FLN"
  },
  {
    "name": "Ferdinand Lumban Tobing",
    "cityName": "Sibolga",
    "code": "FLZ"
  },
  {
    "name": "Münster Osnabrück",
    "cityName": "Münster",
    "code": "FMO"
  },
  {
    "name": "Fuzhou Changle",
    "cityName": "Fuzhou",
    "code": "FOC"
  },
  {
    "name": "Numfoor Airport",
    "cityName": "Numfoor",
    "code": "FOO"
  },
  {
    "name": "Frankfurt am Main",
    "cityName": "Frankfurt-am-main",
    "code": "FRA"
  },
  {
    "name": "Manas",
    "cityName": "Bishkek",
    "code": "FRU"
  },
  {
    "name": "Fort Smith Regional",
    "cityName": "Fort Smith",
    "code": "FSM"
  },
  {
    "name": "Mt. Fuji Shizuoka",
    "cityName": "Makinohara / Shimada",
    "code": "FSZ"
  },
  {
    "name": "Fukuoka",
    "cityName": "Fukuoka",
    "code": "FUK"
  },
  {
    "name": "Fort Wayne",
    "cityName": "Fort Wayne",
    "code": "FWA"
  },
  {
    "name": "Owen Roberts",
    "cityName": "Georgetown",
    "code": "GCM"
  },
  {
    "name": "Don Miguel Hidalgo Y Costilla",
    "cityName": "Guadalajara",
    "code": "GDL"
  },
  {
    "name": "Gdansk Lech Walesa",
    "cityName": "Gdansk",
    "code": "GDN"
  },
  {
    "name": "Spokane",
    "cityName": "Spokane",
    "code": "GEG"
  },
  {
    "name": "Rio de Janeiro–Galeão",
    "cityName": "Rio De Janeiro",
    "code": "GIG"
  },
  {
    "name": "Glasgow",
    "cityName": "Glasgow",
    "code": "GLA"
  },
  {
    "name": "Gamar Malamo Airport",
    "cityName": "Galela",
    "code": "GLX"
  },
  {
    "name": "Gimpo",
    "cityName": "Seoul",
    "code": "GMP"
  },
  {
    "name": "Binaka Airport",
    "cityName": "Gunung Sitoli",
    "code": "GNS"
  },
  {
    "name": "Genoa Cristoforo Colombo",
    "cityName": "Genova",
    "code": "GOA"
  },
  {
    "name": "Dabolim",
    "cityName": "Vasco Da Gama",
    "code": "GOI"
  },
  {
    "name": "Gothenburg-Landvetter",
    "cityName": "Gothenburg",
    "code": "GOT"
  },
  {
    "name": "Gulfport Biloxi",
    "cityName": "Gulfport",
    "code": "GPT"
  },
  {
    "name": "Austin Straubel",
    "cityName": "Green Bay",
    "code": "GRB"
  },
  {
    "name": "George",
    "cityName": "George",
    "code": "GRJ"
  },
  {
    "name": "São Paulo–Guarulhos",
    "cityName": "São Paulo",
    "code": "GRU"
  },
  {
    "name": "Grozny North",
    "cityName": "Grozny",
    "code": "GRV"
  },
  {
    "name": "Seymour Johnson Air Force Base",
    "cityName": "Goldsboro",
    "code": "GSB"
  },
  {
    "name": "Piedmont Triad",
    "cityName": "Greensboro",
    "code": "GSO"
  },
  {
    "name": "Greenville Spartanburg",
    "cityName": "Greenville",
    "code": "GSP"
  },
  {
    "name": "Djalaluddin Airport",
    "cityName": "Gorontalo",
    "code": "GTO"
  },
  {
    "name": "Antonio B. Won Pat",
    "cityName": "Guam",
    "code": "GUM"
  },
  {
    "name": "Grissom Air Reserve Base",
    "cityName": "Peru",
    "code": "GUS"
  },
  {
    "name": "Geneva Cointrin",
    "cityName": "Geneva",
    "code": "GVA"
  },
  {
    "name": "Heydar Aliyev",
    "cityName": "Baku",
    "code": "GYD"
  },
  {
    "name": "Gaziantep",
    "cityName": "Gaziantep",
    "code": "GZT"
  },
  {
    "name": "Hannover",
    "cityName": "Hannover",
    "code": "HAJ"
  },
  {
    "name": "Haikou Meilan International Airport",
    "cityName": "Haikou",
    "code": "HAK"
  },
  {
    "name": "Hamburg",
    "cityName": "Hamburg",
    "code": "HAM"
  },
  {
    "name": "Noi Bai International",
    "cityName": "Hanoi",
    "code": "HAN"
  },
  {
    "name": "José Martí",
    "cityName": "Havana",
    "code": "HAV"
  },
  {
    "name": "Hobart International Airport",
    "cityName": "Hobart",
    "code": "HBA"
  },
  {
    "name": "Helsinki Vantaa",
    "cityName": "Helsinki",
    "code": "HEL"
  },
  {
    "name": "Heraklion Nikos Kazantzakis",
    "cityName": "Heraklion",
    "code": "HER"
  },
  {
    "name": "Luogang",
    "cityName": "Hefei",
    "code": "HFE"
  },
  {
    "name": "Hangzhou Xiaoshan International Airport",
    "cityName": "Hangzhou",
    "code": "HGH"
  },
  {
    "name": "Range Regional",
    "cityName": "Hibbing",
    "code": "HIB"
  },
  {
    "name": "Hong Kong International",
    "cityName": "Hong Kong",
    "code": "HKG"
  },
  {
    "name": "Phuket International Airport",
    "cityName": "Phuket",
    "code": "HKT"
  },
  {
    "name": "Halim Perdana Kusuma",
    "cityName": "Jakarta Halim",
    "code": "HLP"
  },
  {
    "name": "Holloman Air Force Base",
    "cityName": "Alamogordo",
    "code": "HMN"
  },
  {
    "name": "General Ignacio P. Garcia",
    "cityName": "Hermosillo",
    "code": "HMO"
  },
  {
    "name": "Haneda Airport",
    "cityName": "Tokyo",
    "code": "HND"
  },
  {
    "name": "Honolulu International Airport",
    "cityName": "Honolulu",
    "code": "HNL"
  },
  {
    "name": "William P Hobby",
    "cityName": "Houston",
    "code": "HOU"
  },
  {
    "name": "Taiping",
    "cityName": "Harbin",
    "code": "HRB"
  },
  {
    "name": "Hurghada",
    "cityName": "Hurghada",
    "code": "HRG"
  },
  {
    "name": "Mattala Rajapaksa",
    "cityName": "Mattala",
    "code": "HRI"
  },
  {
    "name": "Kharkiv",
    "cityName": "Kharkiv",
    "code": "HRK"
  },
  {
    "name": "Zhoushan Airport",
    "cityName": "Zhoushan",
    "code": "HSN"
  },
  {
    "name": "Huntsville Carl T Jones Field",
    "cityName": "Huntsville",
    "code": "HSV"
  },
  {
    "name": "Tri-State/Milton J. Ferguson Field",
    "cityName": "Huntington",
    "code": "HTS"
  },
  {
    "name": "Rajiv Gandhi",
    "cityName": "Hyderabad",
    "code": "HYD"
  },
  {
    "name": "Washington Dulles",
    "cityName": "Washington",
    "code": "IAD"
  },
  {
    "name": "George Bush Intercontinental Houston",
    "cityName": "Houston",
    "code": "IAH"
  },
  {
    "name": "Incheon",
    "cityName": "Seoul",
    "code": "ICN"
  },
  {
    "name": "Wichita Mid Continent",
    "cityName": "Wichita",
    "code": "ICT"
  },
  {
    "name": "Imam Khomeini",
    "cityName": "Tehran",
    "code": "IKA"
  },
  {
    "name": "Indianapolis",
    "cityName": "Indianapolis",
    "code": "IND"
  },
  {
    "name": "Sultan Azlan Shah Airport",
    "cityName": "Ipoh",
    "code": "IPH"
  },
  {
    "name": "Benazir Bhutto",
    "cityName": "Islamabad",
    "code": "ISB"
  },
  {
    "name": "Atatürk",
    "cityName": "Istanbul",
    "code": "IST"
  },
  {
    "name": "Osaka",
    "cityName": "Osaka",
    "code": "ITM"
  },
  {
    "name": "Jackson-Medgar Wiley Evers",
    "cityName": "Jackson",
    "code": "JAN"
  },
  {
    "name": "Jacksonville",
    "cityName": "Jacksonville",
    "code": "JAX"
  },
  {
    "name": "Notohadinegoro Airport",
    "cityName": "Jember",
    "code": "JBB"
  },
  {
    "name": "King Abdulaziz",
    "cityName": "Jeddah",
    "code": "JED"
  },
  {
    "name": "John F Kennedy",
    "cityName": "New York",
    "code": "JFK"
  },
  {
    "name": "Joplin Regional",
    "cityName": "Joplin",
    "code": "JLN"
  },
  {
    "name": "OR Tambo",
    "cityName": "Johannesburg",
    "code": "JNB"
  },
  {
    "name": "Duqm Jaaluni",
    "cityName": "Duqm",
    "code": "JNJ"
  },
  {
    "name": "Adisutjipto Airport",
    "cityName": "Yogyakarta",
    "code": "JOG"
  },
  {
    "name": "Kau Airport",
    "cityName": "Tobelo",
    "code": "KAZ"
  },
  {
    "name": "Boryspil",
    "cityName": "Kiev",
    "code": "KBP"
  },
  {
    "name": "Gusti Sjamsir Alam Airport",
    "cityName": "Kotabaru",
    "code": "KBU"
  },
  {
    "name": "Halu Oleo Airport",
    "cityName": "Kendari",
    "code": "KDI"
  },
  {
    "name": "Keflavik",
    "cityName": "Reykjavik",
    "code": "KEF"
  },
  {
    "name": "Sary-Arka",
    "cityName": "Karaganda",
    "code": "KGF"
  },
  {
    "name": "Kaohsiung",
    "cityName": "Kaohsiung City",
    "code": "KHH"
  },
  {
    "name": "Norman Manley",
    "cityName": "Kingston",
    "code": "KIN"
  },
  {
    "name": "Kansai International Airport",
    "cityName": "Kansai Osaka",
    "code": "KIX"
  },
  {
    "name": "Yemelyanovo",
    "cityName": "Krasnoyarsk",
    "code": "KJA"
  },
  {
    "name": "Kunming Wujiaba",
    "cityName": "Kunming",
    "code": "KMG"
  },
  {
    "name": "Kaimana Airport",
    "cityName": "Kaimana",
    "code": "KNG"
  },
  {
    "name": "Kinmen",
    "cityName": "Shang-i",
    "code": "KNH"
  },
  {
    "name": "Kuala Namu Airport",
    "cityName": "Medan Kuala Namu",
    "code": "KNO"
  },
  {
    "name": "Eltari Airport",
    "cityName": "Kupang",
    "code": "KOE"
  },
  {
    "name": "Kagoshima",
    "cityName": "Kagoshima",
    "code": "KOJ"
  },
  {
    "name": "John Paul II",
    "cityName": "Krakow",
    "code": "KRK"
  },
  {
    "name": "Khartoum",
    "cityName": "Khartoum",
    "code": "KRT"
  },
  {
    "name": "H. Aroeppala Airport",
    "cityName": "Kepulauan Selayar",
    "code": "KSR"
  },
  {
    "name": "Rahadi Oesman Airport",
    "cityName": "Ketapang",
    "code": "KTG"
  },
  {
    "name": "Katowice",
    "cityName": "Katowice",
    "code": "KTW"
  },
  {
    "name": "Kurumoch",
    "cityName": "Samara",
    "code": "KUF"
  },
  {
    "name": "Kuala Lumpur International Airport",
    "cityName": "Kuala Lumpur",
    "code": "KUL"
  },
  {
    "name": "Kunsan Air Base",
    "cityName": "Kunsan",
    "code": "KUV"
  },
  {
    "name": "Longdongbao Airport",
    "cityName": "Guiyang",
    "code": "KWE"
  },
  {
    "name": "Kuwait",
    "cityName": "Kuwait City",
    "code": "KWI"
  },
  {
    "name": "Guilin Liangjiang",
    "cityName": "Guilin City",
    "code": "KWL"
  },
  {
    "name": "Labuha Airport",
    "cityName": "Labuha",
    "code": "LAH"
  },
  {
    "name": "McCarran",
    "cityName": "Las Vegas",
    "code": "LAS"
  },
  {
    "name": "Los Angeles",
    "cityName": "Los Angeles",
    "code": "LAX"
  },
  {
    "name": "Leeds Bradford",
    "cityName": "Leeds",
    "code": "LBA"
  },
  {
    "name": "Lubbock Preston Smith",
    "cityName": "Lubbock",
    "code": "LBB"
  },
  {
    "name": "Mutiara Airport",
    "cityName": "Labuan Bajo",
    "code": "LBJ"
  },
  {
    "name": "Juvai Semaring Airport",
    "cityName": "Long Bawan",
    "code": "LBW"
  },
  {
    "name": "Larnarca",
    "cityName": "Larnarca",
    "code": "LCA"
  },
  {
    "name": "Rickenbacker",
    "cityName": "Columbus",
    "code": "LCK"
  },
  {
    "name": "London City",
    "cityName": "London",
    "code": "LCY"
  },
  {
    "name": "Pulkovo",
    "cityName": "St. Petersburg",
    "code": "LED"
  },
  {
    "name": "Leipzig Halle",
    "cityName": "Leipzig",
    "code": "LEJ"
  },
  {
    "name": "Blue Grass",
    "cityName": "Lexington",
    "code": "LEX"
  },
  {
    "name": "Langley Air Force Base",
    "cityName": "Hampton",
    "code": "LFI"
  },
  {
    "name": "Lafayette Regional",
    "cityName": "Lafayette",
    "code": "LFT"
  },
  {
    "name": "La Guardia",
    "cityName": "New York",
    "code": "LGA"
  },
  {
    "name": "Liege",
    "cityName": "Liege",
    "code": "LGG"
  },
  {
    "name": "London Gatwick",
    "cityName": "London",
    "code": "LGW"
  },
  {
    "name": "London Heathrow",
    "cityName": "London",
    "code": "LHR"
  },
  {
    "name": "Jorge Chávez",
    "cityName": "Lima",
    "code": "LIM"
  },
  {
    "name": "Linate",
    "cityName": "Milan",
    "code": "LIN"
  },
  {
    "name": "Lisbon Portela",
    "cityName": "Lisbon",
    "code": "LIS"
  },
  {
    "name": "Bill & Hillary Clinton National/Adams Field",
    "cityName": "Little Rock",
    "code": "LIT"
  },
  {
    "name": "Ljubljana Jože Pucnik",
    "cityName": "Ljubljana",
    "code": "LJU"
  },
  {
    "name": "Gewayantana Airport",
    "cityName": "Larantuka Gewayantana",
    "code": "LKA"
  },
  {
    "name": "RAF Lakenheath",
    "cityName": "Lakenheath",
    "code": "LKZ"
  },
  {
    "name": "Silampari Airport",
    "cityName": "Lubuk Linggau",
    "code": "LLG"
  },
  {
    "name": "Lombok International Airport",
    "cityName": "Lombok, Mataram",
    "code": "LOP"
  },
  {
    "name": "Murtala Muhammed",
    "cityName": "Lagos",
    "code": "LOS"
  },
  {
    "name": "Gran Canaria",
    "cityName": "Gran Canaria Island",
    "code": "LPA"
  },
  {
    "name": "Liverpool John Lennon",
    "cityName": "Liverpool",
    "code": "LPL"
  },
  {
    "name": "Long Apung Airport",
    "cityName": "Long Apung",
    "code": "LPU"
  },
  {
    "name": "Lasondre Airport",
    "cityName": "Batu Islands",
    "code": "LSE"
  },
  {
    "name": "Launceston Airport",
    "cityName": "Launceston",
    "code": "LST"
  },
  {
    "name": "Lhoksumawe Airport",
    "cityName": "Lhokseumawe",
    "code": "LSW"
  },
  {
    "name": "Bassel Al-Assad",
    "cityName": "Latakia",
    "code": "LTK"
  },
  {
    "name": "London Luton",
    "cityName": "London",
    "code": "LTN"
  },
  {
    "name": "Altus Air Force Base",
    "cityName": "Altus",
    "code": "LTS"
  },
  {
    "name": "Cotopaxi",
    "cityName": "Latacunga",
    "code": "LTX"
  },
  {
    "name": "Luke Air Force Base",
    "cityName": "Glendale",
    "code": "LUF"
  },
  {
    "name": "Dumatubun Airport",
    "cityName": "Tual",
    "code": "LUV"
  },
  {
    "name": "Luwuk Airport",
    "cityName": "Luwuk",
    "code": "LUW"
  },
  {
    "name": "Luxembourg-Findel",
    "cityName": "Luxembourg",
    "code": "LUX"
  },
  {
    "name": "Chennai",
    "cityName": "Chennai",
    "code": "MAA"
  },
  {
    "name": "Adolfo Suarez Madrid-Barajas",
    "cityName": "Madrid",
    "code": "MAD"
  },
  {
    "name": "Manchester",
    "cityName": "Manchester",
    "code": "MAN"
  },
  {
    "name": "Mombasa Moi",
    "cityName": "Mombasa",
    "code": "MBA"
  },
  {
    "name": "MBS",
    "cityName": "Saginaw",
    "code": "MBS"
  },
  {
    "name": "Mac Dill Air Force Base",
    "cityName": "Tampa",
    "code": "MCF"
  },
  {
    "name": "Kansas City",
    "cityName": "Kansas City",
    "code": "MCI"
  },
  {
    "name": "Orlando",
    "cityName": "Orlando",
    "code": "MCO"
  },
  {
    "name": "Muscat",
    "cityName": "Muscat",
    "code": "MCT"
  },
  {
    "name": "Samratulangi Airport",
    "cityName": "Manado",
    "code": "MDC"
  },
  {
    "name": "Mandalay",
    "cityName": "Mandalay",
    "code": "MDL"
  },
  {
    "name": "Chicago Midway",
    "cityName": "Chicago",
    "code": "MDW"
  },
  {
    "name": "Prince Mohammad Bin Abdulaziz",
    "cityName": "Medina",
    "code": "MED"
  },
  {
    "name": "Melbourne Airport",
    "cityName": "Melbourne",
    "code": "MEL"
  },
  {
    "name": "Memphis",
    "cityName": "Memphis",
    "code": "MEM"
  },
  {
    "name": "Seunagan Airport",
    "cityName": "Meulaboh",
    "code": "MEQ"
  },
  {
    "name": "Licenciado Benito Juarez",
    "cityName": "Mexico City",
    "code": "MEX"
  },
  {
    "name": "Macau International Airport",
    "cityName": "Macau",
    "code": "MFM"
  },
  {
    "name": "Dobbins Air Reserve Base",
    "cityName": "Marietta",
    "code": "MGE"
  },
  {
    "name": "Montgomery Regional (Dannelly Field)",
    "cityName": "Montgomery",
    "code": "MGM"
  },
  {
    "name": "Mashhad",
    "cityName": "Mashhad",
    "code": "MHD"
  },
  {
    "name": "Manchester",
    "cityName": "Manchester",
    "code": "MHT"
  },
  {
    "name": "RAF Mildenhall",
    "cityName": "Mildenhall",
    "code": "MHZ"
  },
  {
    "name": "Miami",
    "cityName": "Miami",
    "code": "MIA"
  },
  {
    "name": "Mamuju Airport",
    "cityName": "Mamuju",
    "code": "MJU"
  },
  {
    "name": "General Mitchell",
    "cityName": "Milwaukee",
    "code": "MKE"
  },
  {
    "name": "Mopah Airport",
    "cityName": "Merauke",
    "code": "MKQ"
  },
  {
    "name": "Rendani Airport",
    "cityName": "Manokwari",
    "code": "MKW"
  },
  {
    "name": "Malta",
    "cityName": "Luqa",
    "code": "MLA"
  },
  {
    "name": "Abdul Rahman Saleh Airport",
    "cityName": "Malang",
    "code": "MLG"
  },
  {
    "name": "Quad City",
    "cityName": "Moline",
    "code": "MLI"
  },
  {
    "name": "Melalan",
    "cityName": "Melak",
    "code": "MLK"
  },
  {
    "name": "Monroe Regional",
    "cityName": "Monroe",
    "code": "MLU"
  },
  {
    "name": "Melangguane Airport",
    "cityName": "Melanguane",
    "code": "MNA"
  },
  {
    "name": "Ninoy Aquino Intl Airport",
    "cityName": "Manila",
    "code": "MNL"
  },
  {
    "name": "Mobile Regional",
    "cityName": "Mobile",
    "code": "MOB"
  },
  {
    "name": "Frans Seda Airport",
    "cityName": "Maumere",
    "code": "MOF"
  },
  {
    "name": "Muara Bungo Airport",
    "cityName": "Muara Bungo",
    "code": "MRB"
  },
  {
    "name": "Marseille Provence",
    "cityName": "Marseille",
    "code": "MRS"
  },
  {
    "name": "Dane County Regional Truax Field",
    "cityName": "Madison",
    "code": "MSN"
  },
  {
    "name": "Minneapolis-St Paul/Wold-Chamberlain",
    "cityName": "Minneapolis",
    "code": "MSP"
  },
  {
    "name": "Minsk",
    "cityName": "Minsk",
    "code": "MSQ"
  },
  {
    "name": "Maastricht Aachen",
    "cityName": "Maastricht",
    "code": "MST"
  },
  {
    "name": "Louis Armstrong New Orleans",
    "cityName": "New Orleans",
    "code": "MSY"
  },
  {
    "name": "General Mariano Escobedo",
    "cityName": "Monterrey",
    "code": "MTY"
  },
  {
    "name": "Munich",
    "cityName": "Munich",
    "code": "MUC"
  },
  {
    "name": "Carrasco /General C L Berisso",
    "cityName": "Montevideo",
    "code": "MVD"
  },
  {
    "name": "Muan",
    "cityName": "Muan",
    "code": "MWX"
  },
  {
    "name": "Malpensa",
    "cityName": "Milan",
    "code": "MXP"
  },
  {
    "name": "Naha Airport",
    "cityName": "Tahuna",
    "code": "NAH"
  },
  {
    "name": "Namlea Airport",
    "cityName": "Namlea",
    "code": "NAM"
  },
  {
    "name": "Naples",
    "cityName": "Napoli",
    "code": "NAP"
  },
  {
    "name": "Lynden Pindling",
    "cityName": "Nassau",
    "code": "NAS"
  },
  {
    "name": "Nevsehir Kapadokya",
    "cityName": "Nevsehir",
    "code": "NAV"
  },
  {
    "name": "Beijing Nanyuan",
    "cityName": "Beijing",
    "code": "NAY"
  },
  {
    "name": "Enfidha - Hammamet",
    "cityName": "Enfidha",
    "code": "NBE"
  },
  {
    "name": "Jomo Kenyatta",
    "cityName": "Nairobi",
    "code": "NBO"
  },
  {
    "name": "Nabire Airport",
    "cityName": "Nabire",
    "code": "NBX"
  },
  {
    "name": "Nice Côte d'Azur",
    "cityName": "Nice",
    "code": "NCE"
  },
  {
    "name": "Newcastle",
    "cityName": "Newcastle",
    "code": "NCL"
  },
  {
    "name": "Bandanaira Airport",
    "cityName": "Banda",
    "code": "NDA"
  },
  {
    "name": "Ningbo Lishe",
    "cityName": "Ningbo",
    "code": "NGB"
  },
  {
    "name": "Chubu Centrair",
    "cityName": "Tokoname",
    "code": "NGO"
  },
  {
    "name": "Nanjing Lukou",
    "cityName": "Nanjing",
    "code": "NKG"
  },
  {
    "name": "Nanning Wuxu",
    "cityName": "Nanning",
    "code": "NNG"
  },
  {
    "name": "Nunukan Airport",
    "cityName": "Nunukan",
    "code": "NNX"
  },
  {
    "name": "Namrole Airport",
    "cityName": "Namrole",
    "code": "NRE"
  },
  {
    "name": "Narita International Airport",
    "cityName": "Tokyo Narita",
    "code": "NRT"
  },
  {
    "name": "Natuna Ranai Airport",
    "cityName": "Natuna Ranai",
    "code": "NTX"
  },
  {
    "name": "Nuremberg",
    "cityName": "Nuremberg",
    "code": "NUE"
  },
  {
    "name": "Norwich",
    "cityName": "Norwich",
    "code": "NWI"
  },
  {
    "name": "Metropolitan Oakland",
    "cityName": "Oakland",
    "code": "OAK"
  },
  {
    "name": "Odessa",
    "cityName": "Odessa",
    "code": "ODS"
  },
  {
    "name": "Naha",
    "cityName": "Naha",
    "code": "OKA"
  },
  {
    "name": "Will Rogers World",
    "cityName": "Oklahoma City",
    "code": "OKC"
  },
  {
    "name": "Sapporo Okadama Airport",
    "cityName": "Sapporo Okadama",
    "code": "OKD"
  },
  {
    "name": "Yokota Air Base",
    "cityName": "Fussa",
    "code": "OKO"
  },
  {
    "name": "Ontario",
    "cityName": "Ontario",
    "code": "ONT"
  },
  {
    "name": "Gold Coast Airport",
    "cityName": "Gold Coast",
    "code": "OOL"
  },
  {
    "name": "Francisco de Sá Carneiro",
    "cityName": "Porto",
    "code": "OPO"
  },
  {
    "name": "Chicago O'Hare",
    "cityName": "Chicago",
    "code": "ORD"
  },
  {
    "name": "Norfolk",
    "cityName": "Norfolk",
    "code": "ORF"
  },
  {
    "name": "Cork",
    "cityName": "Cork",
    "code": "ORK"
  },
  {
    "name": "Paris-Orly",
    "cityName": "Paris",
    "code": "ORY"
  },
  {
    "name": "Oslo Gardermoen",
    "cityName": "Oslo",
    "code": "OSL"
  },
  {
    "name": "Pitu Airport",
    "cityName": "Morotai Island",
    "code": "OTI"
  },
  {
    "name": "Henri Coanda",
    "cityName": "Bucharest",
    "code": "OTP"
  },
  {
    "name": "Tyndall Air Force Base",
    "cityName": "Panama City",
    "code": "PAM"
  },
  {
    "name": "Palm Beach",
    "cityName": "West Palm Beach",
    "code": "PBI"
  },
  {
    "name": "Pondok Cabe Airport",
    "cityName": "Tangerang",
    "code": "PCB"
  },
  {
    "name": "Minangkabau International Airport",
    "cityName": "Padang",
    "code": "PDG"
  },
  {
    "name": "João Paulo II",
    "cityName": "Ponta Delgada",
    "code": "PDL"
  },
  {
    "name": "Plovdiv",
    "cityName": "Plovdiv",
    "code": "PDV"
  },
  {
    "name": "Portland",
    "cityName": "Portland",
    "code": "PDX"
  },
  {
    "name": "Beijing Capital",
    "cityName": "Beijing",
    "code": "PEK"
  },
  {
    "name": "Penang International",
    "cityName": "Penang",
    "code": "PEN"
  },
  {
    "name": "Perth Airport",
    "cityName": "Perth",
    "code": "PER"
  },
  {
    "name": "Paphos",
    "cityName": "Paphos",
    "code": "PFO"
  },
  {
    "name": "Depati Amir Airport",
    "cityName": "Pangkal Pinang",
    "code": "PGK"
  },
  {
    "name": "Newport News Williamsburg",
    "cityName": "Newport News",
    "code": "PHF"
  },
  {
    "name": "Philadelphia",
    "cityName": "Philadelphia",
    "code": "PHL"
  },
  {
    "name": "Phoenix Sky Harbor",
    "cityName": "Phoenix",
    "code": "PHX"
  },
  {
    "name": "General Wayne A. Downing Peoria",
    "cityName": "Peoria",
    "code": "PIA"
  },
  {
    "name": "Pittsburgh",
    "cityName": "Pittsburgh",
    "code": "PIT"
  },
  {
    "name": "Iskandar Airport",
    "cityName": "Pangkalanbun",
    "code": "PKN"
  },
  {
    "name": "Sultan Syarif Kasim II  Airport",
    "cityName": "Pekanbaru",
    "code": "PKU"
  },
  {
    "name": "Tjilik Riwut Airport",
    "cityName": "Palangkaraya",
    "code": "PKY"
  },
  {
    "name": "Mahmud Badaruddin II Airport",
    "cityName": "Palembang",
    "code": "PLM"
  },
  {
    "name": "Mutiara Airport",
    "cityName": "Palu",
    "code": "PLW"
  },
  {
    "name": "Palma De Mallorca",
    "cityName": "Palma De Mallorca",
    "code": "PMI"
  },
  {
    "name": "Falcone-Borsellino",
    "cityName": "Palermo",
    "code": "PMO"
  },
  {
    "name": "Phnom Penh International Airport",
    "cityName": "Phnom Penh",
    "code": "PNH"
  },
  {
    "name": "Supadio Airport",
    "cityName": "Pontianak",
    "code": "PNK"
  },
  {
    "name": "Port Moresby Jacksons",
    "cityName": "Port Moresby",
    "code": "POM"
  },
  {
    "name": "Poznan-Lawica",
    "cityName": "Poznan",
    "code": "POZ"
  },
  {
    "name": "Václav Havel",
    "cityName": "Prague",
    "code": "PRG"
  },
  {
    "name": "Pisa",
    "cityName": "Pisa",
    "code": "PSA"
  },
  {
    "name": "Kasiguncu Airport",
    "cityName": "Poso",
    "code": "PSJ"
  },
  {
    "name": "Pangsuma Airport",
    "cityName": "Putussibau",
    "code": "PSU"
  },
  {
    "name": "Tocumen",
    "cityName": "Tocumen",
    "code": "PTY"
  },
  {
    "name": "Punta Cana",
    "cityName": "Punta Cana",
    "code": "PUJ"
  },
  {
    "name": "Pomala Airport",
    "cityName": "Kolaka",
    "code": "PUM"
  },
  {
    "name": "Gimhae",
    "cityName": "Busan",
    "code": "PUS"
  },
  {
    "name": "Shanghai Pudong",
    "cityName": "Shanghai",
    "code": "PVG"
  },
  {
    "name": "Licenciado Gustavo Díaz Ordaz",
    "cityName": "Puerto Vallarta",
    "code": "PVR"
  },
  {
    "name": "Portland Jetport",
    "cityName": "Portland",
    "code": "PWM"
  },
  {
    "name": "Akwa Ibom",
    "cityName": "Uyo",
    "code": "QUO"
  },
  {
    "name": "Rarotonga",
    "cityName": "Avarua",
    "code": "RAR"
  },
  {
    "name": "Raleigh Durham",
    "cityName": "Raleigh/durham",
    "code": "RDU"
  },
  {
    "name": "Siem Reap International Airport",
    "cityName": "Siem Reap",
    "code": "REP"
  },
  {
    "name": "Chicago Rockford",
    "cityName": "Chicago/rockford",
    "code": "RFD"
  },
  {
    "name": "Yangon Airport",
    "cityName": "Yangon",
    "code": "RGN"
  },
  {
    "name": "Richmond",
    "cityName": "Richmond",
    "code": "RIC"
  },
  {
    "name": "Riga",
    "cityName": "Riga",
    "code": "RIX"
  },
  {
    "name": "Ramstein Air Base",
    "cityName": "Ramstein",
    "code": "RMS"
  },
  {
    "name": "Reno Tahoe",
    "cityName": "Reno",
    "code": "RNO"
  },
  {
    "name": "Roanokeâ€“Blacksburg Regional",
    "cityName": "Roanoke",
    "code": "ROA"
  },
  {
    "name": "Greater Rochester",
    "cityName": "Rochester",
    "code": "ROC"
  },
  {
    "name": "Rochester",
    "cityName": "Rochester",
    "code": "RST"
  },
  {
    "name": "Southwest Florida",
    "cityName": "Fort Myers",
    "code": "RSW"
  },
  {
    "name": "King Khaled",
    "cityName": "Riyadh",
    "code": "RUH"
  },
  {
    "name": "San Diego",
    "cityName": "San Diego",
    "code": "SAN"
  },
  {
    "name": "San Antonio",
    "cityName": "San Antonio",
    "code": "SAT"
  },
  {
    "name": "Savannah Hilton Head",
    "cityName": "Savannah",
    "code": "SAV"
  },
  {
    "name": "Sabiha Gökçen",
    "cityName": "Istanbul",
    "code": "SAW"
  },
  {
    "name": "South Bend Regional",
    "cityName": "South Bend",
    "code": "SBN"
  },
  {
    "name": "Santiago de Compostela",
    "cityName": "Santiago De Compostela",
    "code": "SCQ"
  },
  {
    "name": "Louisville Standiford Field",
    "cityName": "Louisville",
    "code": "SDF"
  },
  {
    "name": "Las Américas",
    "cityName": "Santo Domingo",
    "code": "SDQ"
  },
  {
    "name": "Seattle Tacoma",
    "cityName": "Seattle",
    "code": "SEA"
  },
  {
    "name": "Orlando Sanford",
    "cityName": "Orlando",
    "code": "SFB"
  },
  {
    "name": "San Francisco",
    "cityName": "San Francisco",
    "code": "SFO"
  },
  {
    "name": "Springfield Branson National",
    "cityName": "Springfield",
    "code": "SGF"
  },
  {
    "name": "Tan Son Nhat International Airport",
    "cityName": "Ho Chi Minh City",
    "code": "SGN"
  },
  {
    "name": "Shanghai Hongqiao",
    "cityName": "Shanghai",
    "code": "SHA"
  },
  {
    "name": "Taoxian",
    "cityName": "Shenyang",
    "code": "SHE"
  },
  {
    "name": "Sharjah",
    "cityName": "Sharjah",
    "code": "SHJ"
  },
  {
    "name": "Shreveport Regional",
    "cityName": "Shreveport",
    "code": "SHV"
  },
  {
    "name": "Changi Airport",
    "cityName": "Singapore",
    "code": "SIN"
  },
  {
    "name": "Simferopol",
    "cityName": "Simferopol",
    "code": "SIP"
  },
  {
    "name": "Sibisa Airport",
    "cityName": "Parapat / Toba Samosir",
    "code": "SIW"
  },
  {
    "name": "Norman Y. Mineta San Jose",
    "cityName": "San Jose",
    "code": "SJC"
  },
  {
    "name": "Sarajevo",
    "cityName": "Sarajevo",
    "code": "SJJ"
  },
  {
    "name": "Luis Munoz Marin",
    "cityName": "San Juan",
    "code": "SJU"
  },
  {
    "name": "Fairchild Air Force Base",
    "cityName": "Spokane",
    "code": "SKA"
  },
  {
    "name": "Thessaloniki Macedonia",
    "cityName": "Thessaloniki",
    "code": "SKG"
  },
  {
    "name": "Sialkot",
    "cityName": "Sialkot",
    "code": "SKT"
  },
  {
    "name": "Salt Lake City",
    "cityName": "Salt Lake City",
    "code": "SLC"
  },
  {
    "name": "Sacramento",
    "cityName": "Sacramento",
    "code": "SMF"
  },
  {
    "name": "Sampit Airport",
    "cityName": "Sampit",
    "code": "SMQ"
  },
  {
    "name": "John Wayne-Orange County",
    "cityName": "Santa Ana",
    "code": "SNA"
  },
  {
    "name": "Shannon",
    "cityName": "Shannon",
    "code": "SNN"
  },
  {
    "name": "Adi Sumarmo Airport",
    "cityName": "Solo",
    "code": "SOC"
  },
  {
    "name": "Sofia",
    "cityName": "Sofia",
    "code": "SOF"
  },
  {
    "name": "Domine Eduard Osok Airport",
    "cityName": "Sorong",
    "code": "SOQ"
  },
  {
    "name": "Southampton",
    "cityName": "Southampton",
    "code": "SOU"
  },
  {
    "name": "Abraham Lincoln Capital",
    "cityName": "Springfield",
    "code": "SPI"
  },
  {
    "name": "Sheppard Air Force Base-Wichita Falls Municipal",
    "cityName": "Wichita Falls",
    "code": "SPS"
  },
  {
    "name": "Sintang Airport",
    "cityName": "Sintang",
    "code": "SQG"
  },
  {
    "name": "Achmad Yani Airport",
    "cityName": "Semarang",
    "code": "SRG"
  },
  {
    "name": "Temindung Airport",
    "cityName": "Samarinda",
    "code": "SRI"
  },
  {
    "name": "Sarasota Bradenton",
    "cityName": "Sarasota/bradenton",
    "code": "SRQ"
  },
  {
    "name": "Deputado Luís Eduardo MagalhãesInternational",
    "cityName": "Salvador",
    "code": "SSA"
  },
  {
    "name": "Lambert St Louis",
    "cityName": "St Louis",
    "code": "STL"
  },
  {
    "name": "London Stansted",
    "cityName": "London",
    "code": "STN"
  },
  {
    "name": "Stuttgart",
    "cityName": "Stuttgart",
    "code": "STR"
  },
  {
    "name": "Juanda Airport",
    "cityName": "Surabaya",
    "code": "SUB"
  },
  {
    "name": "Sioux Gateway Col. Bud Day Field",
    "cityName": "Sioux City",
    "code": "SUX"
  },
  {
    "name": "Stavanger, Sola",
    "cityName": "Stavanger",
    "code": "SVG"
  },
  {
    "name": "Sheremetyevo",
    "cityName": "Moscow",
    "code": "SVO"
  },
  {
    "name": "Koltsovo",
    "cityName": "Yekaterinburg",
    "code": "SVX"
  },
  {
    "name": "Jieyang Chaoshan Airport",
    "cityName": "Shantou/jieyang",
    "code": "SWA"
  },
  {
    "name": "Brang Bidji Airport",
    "cityName": "Sumbawa",
    "code": "SWQ"
  },
  {
    "name": "Berlin Schönefeld",
    "cityName": "Berlin",
    "code": "SXF"
  },
  {
    "name": "Saumlaki Olilit Airport",
    "cityName": "Saumlaki",
    "code": "SXK"
  },
  {
    "name": "Sydney Airport",
    "cityName": "Sydney",
    "code": "SYD"
  },
  {
    "name": "Syracuse Hancock",
    "cityName": "Syracuse",
    "code": "SYR"
  },
  {
    "name": "Sanya Phoenix",
    "cityName": "Sanya",
    "code": "SYX"
  },
  {
    "name": "Shiraz Shahid Dastghaib",
    "cityName": "Shiraz",
    "code": "SYZ"
  },
  {
    "name": "Shenzhen Bao'an",
    "cityName": "Shenzhen",
    "code": "SZX"
  },
  {
    "name": "Tashkent",
    "cityName": "Tashkent",
    "code": "TAS"
  },
  {
    "name": "Tbilisi",
    "cityName": "Tbilisi",
    "code": "TBS"
  },
  {
    "name": "Tabriz",
    "cityName": "Tabriz",
    "code": "TBZ"
  },
  {
    "name": "McChord Air Force Base",
    "cityName": "Tacoma",
    "code": "TCM"
  },
  {
    "name": "Lajes Field",
    "cityName": "Lajes",
    "code": "TER"
  },
  {
    "name": "Tenerife Norte",
    "cityName": "Tenerife Island",
    "code": "TFN"
  },
  {
    "name": "Tenerife South",
    "cityName": "Tenerife Island",
    "code": "TFS"
  },
  {
    "name": "Podgorica",
    "cityName": "Podgorica",
    "code": "TGD"
  },
  {
    "name": "Mehrabad",
    "cityName": "Tehran",
    "code": "THR"
  },
  {
    "name": "Tirana Mother Teresa",
    "cityName": "Tirana",
    "code": "TIA"
  },
  {
    "name": "General Abelardo L. Rodriguez",
    "cityName": "Tijuana",
    "code": "TIJ"
  },
  {
    "name": "Moses Kilangin Airport",
    "cityName": "Timika",
    "code": "TIM"
  },
  {
    "name": "Tripoli",
    "cityName": "Tripoli",
    "code": "TIP"
  },
  {
    "name": "H.A.S. Hanandjoeddin Airport",
    "cityName": "Tanjung Pandan",
    "code": "TJQ"
  },
  {
    "name": "Tanjung Harapan Airport",
    "cityName": "Tanjung Selor",
    "code": "TJS"
  },
  {
    "name": "Branti Airport",
    "cityName": "Bandar Lampung",
    "code": "TKG"
  },
  {
    "name": "Tallahassee Regional",
    "cityName": "Tallahassee",
    "code": "TLH"
  },
  {
    "name": "LALOS AIRPORT",
    "cityName": "Tolitoli",
    "code": "TLI"
  },
  {
    "name": "Tallinn",
    "cityName": "Tallinn",
    "code": "TLL"
  },
  {
    "name": "Toulouse-Blagnac",
    "cityName": "Toulouse/blagnac",
    "code": "TLS"
  },
  {
    "name": "Ben Gurion",
    "cityName": "Tel Aviv",
    "code": "TLV"
  },
  {
    "name": "Waikabubak Airport",
    "cityName": "Tambolaka",
    "code": "TMC"
  },
  {
    "name": "Yaoqiang",
    "cityName": "Jinan",
    "code": "TNA"
  },
  {
    "name": "Kidjang Airport",
    "cityName": "Tanjung Pinang",
    "code": "TNJ"
  },
  {
    "name": "Toledo Express",
    "cityName": "Toledo",
    "code": "TOL"
  },
  {
    "name": "Tromsø",
    "cityName": "Tromsø",
    "code": "TOS"
  },
  {
    "name": "Tampa",
    "cityName": "Tampa",
    "code": "TPA"
  },
  {
    "name": "Taiwan Taoyuan International Airport",
    "cityName": "Taipei",
    "code": "TPE"
  },
  {
    "name": "Trondheim",
    "cityName": "Trondheim",
    "code": "TRD"
  },
  {
    "name": "Tri Cities Regional Tn Va",
    "cityName": "Bristol/johnson/kingsport",
    "code": "TRI"
  },
  {
    "name": "Juwata Airport",
    "cityName": "Tarakan",
    "code": "TRK"
  },
  {
    "name": "Turin",
    "cityName": "Torino",
    "code": "TRN"
  },
  {
    "name": "Trivandrum",
    "cityName": "Trivandrum",
    "code": "TRV"
  },
  {
    "name": "Sungshan",
    "cityName": "Taipei",
    "code": "TSA"
  },
  {
    "name": "Astana",
    "cityName": "Astana",
    "code": "TSE"
  },
  {
    "name": "Treviso-Sant'Angelo",
    "cityName": "Treviso",
    "code": "TSF"
  },
  {
    "name": "Tianjin Binhai",
    "cityName": "Tianjin",
    "code": "TSN"
  },
  {
    "name": "Babullah Airport",
    "cityName": "Ternate",
    "code": "TTE"
  },
  {
    "name": "Tulsa",
    "cityName": "Tulsa",
    "code": "TUL"
  },
  {
    "name": "Tunis Carthage",
    "cityName": "Tunis",
    "code": "TUN"
  },
  {
    "name": "Tucson",
    "cityName": "Tucson",
    "code": "TUS"
  },
  {
    "name": "Berlin-Tegel",
    "cityName": "Berlin",
    "code": "TXL"
  },
  {
    "name": "Taiyuan Wusu",
    "cityName": "Taiyuan",
    "code": "TYN"
  },
  {
    "name": "McGhee Tyson",
    "cityName": "Knoxville",
    "code": "TYS"
  },
  {
    "name": "Trabzon",
    "cityName": "Trabzon",
    "code": "TZX"
  },
  {
    "name": "Ufa",
    "cityName": "Ufa",
    "code": "UFA"
  },
  {
    "name": "Mariscal Sucre",
    "cityName": "Quito",
    "code": "UIO"
  },
  {
    "name": "Pogogul",
    "cityName": "Buol",
    "code": "UOL"
  },
  {
    "name": "Hasanudin Airport",
    "cityName": "Makassar",
    "code": "UPG"
  },
  {
    "name": "Samui",
    "cityName": "Ko Samui",
    "code": "USM"
  },
  {
    "name": "Varna",
    "cityName": "Varna",
    "code": "VAR"
  },
  {
    "name": "Vandenberg Air Force Base",
    "cityName": "Lompoc",
    "code": "VBG"
  },
  {
    "name": "Venice Marco Polo",
    "cityName": "Venice",
    "code": "VCE"
  },
  {
    "name": "Ovda",
    "cityName": "Eilat",
    "code": "VDA"
  },
  {
    "name": "Vienna",
    "cityName": "Vienna",
    "code": "VIE"
  },
  {
    "name": "Vnukovo",
    "cityName": "Moscow",
    "code": "VKO"
  },
  {
    "name": "Vilnius",
    "cityName": "Vilnius",
    "code": "VNO"
  },
  {
    "name": "Eglin Air Force Base",
    "cityName": "Valparaiso",
    "code": "VPS"
  },
  {
    "name": "Juan Gualberto Gomez",
    "cityName": "Varadero",
    "code": "VRA"
  },
  {
    "name": "Verona Villafranca",
    "cityName": "Verona",
    "code": "VRN"
  },
  {
    "name": "Warsaw Chopin",
    "cityName": "Warsaw",
    "code": "WAW"
  },
  {
    "name": "Umbu Mehang Kunda Airport",
    "cityName": "Waingapu",
    "code": "WGP"
  },
  {
    "name": "Wellington International Airport",
    "cityName": "Wellington",
    "code": "WLG"
  },
  {
    "name": "Modlin",
    "cityName": "Warsaw",
    "code": "WMI"
  },
  {
    "name": "Matahora Airport",
    "cityName": "Wakatobi",
    "code": "WNI"
  },
  {
    "name": "Wenzhou Yongqiang",
    "cityName": "Wenzhou",
    "code": "WNZ"
  },
  {
    "name": "Robins Air Force Base",
    "cityName": "Warner Robins",
    "code": "WRB"
  },
  {
    "name": "Buli Airport",
    "cityName": "Buli",
    "code": "WUB"
  },
  {
    "name": "Wuhan Tianhe",
    "cityName": "Wuhan",
    "code": "WUH"
  },
  {
    "name": "Xi'an Xianyang",
    "cityName": "Xianyang",
    "code": "XIY"
  },
  {
    "name": "Xiamen Gaoqi",
    "cityName": "Xiamen",
    "code": "XMN"
  },
  {
    "name": "Edmonton",
    "cityName": "Edmonton",
    "code": "YEG"
  },
  {
    "name": "Halifax / Stanfield",
    "cityName": "Halifax",
    "code": "YHZ"
  },
  {
    "name": "Yantai Penglai Int'l Airport",
    "cityName": "Yantai",
    "code": "YNT"
  },
  {
    "name": "Ottawa Macdonald-Cartier",
    "cityName": "Ottawa",
    "code": "YOW"
  },
  {
    "name": "Quebec Jean Lesage",
    "cityName": "Quebec",
    "code": "YQB"
  },
  {
    "name": "Montreal / Pierre Elliott Trudeau",
    "cityName": "Montreal",
    "code": "YUL"
  },
  {
    "name": "Vancouver",
    "cityName": "Vancouver",
    "code": "YVR"
  },
  {
    "name": "Winnipeg / James Armstrong Richardson",
    "cityName": "Winnipeg",
    "code": "YWG"
  },
  {
    "name": "London",
    "cityName": "London",
    "code": "YXU"
  },
  {
    "name": "Calgary",
    "cityName": "Calgary",
    "code": "YYC"
  },
  {
    "name": "Victoria",
    "cityName": "Victoria",
    "code": "YYJ"
  },
  {
    "name": "St. John's",
    "cityName": "St. John's",
    "code": "YYT"
  },
  {
    "name": "Lester B. Pearson",
    "cityName": "Toronto",
    "code": "YYZ"
  },
  {
    "name": "Zagreb",
    "cityName": "Zagreb",
    "code": "ZAG"
  },
  {
    "name": "Queenstown Airport",
    "cityName": "Queenstown",
    "code": "ZQN"
  },
  {
    "name": "Zurich",
    "cityName": "Zurich",
    "code": "ZRH"
  },
  {
    "name": "Kuching International Airport",
    "cityName": "Kuching",
    "code": "KCH"
  },
  {
    "name": "Langkawi Intl. Airport",
    "cityName": "Lengkawi",
    "code": "LGK"
  }
];

export default Airports;
