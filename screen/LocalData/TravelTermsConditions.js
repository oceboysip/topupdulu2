const TravelTermsConditions = [
  [{
    title: 'Biaya Tour Telah Termasuk',
    label: [
      {
        desc: [
          'Tiket pesawat internasional kelas ekonomi (non-refundable, non-endorsable, non-reroutable berdasarkan harga tiket group atau harga promosi lainnya).',
          'Akomodasi, menginap di hotel dengan ketentuan 2 (dua) orang dalam satu kamar (Twin Sharing). Jika menginginkan satu kamar sendiri akan dikenakan biaya tambahan (Single Supplement).',
          'Bagasi cuma-cuma 1 potong dengan berat maksimum 20kg atau sesuai dengan peraturan penerbangan yang digunakan dan 1 handbag kecil untuk dibawa ke kabin pesawat.',
          'Makan (MP-Makan Pagi; MS-Makan Siang; MM-Makan Malam), transportasi dan acara tour sesuai dengan jadwal acara tour.',
          'Tas Golden Rama (kecuali untuk Tour Super Sale)',
          'Tour Leader yang berpengalaman.'
        ]
      },
    ],
  }],
  [{
    title: 'Biaya Tour Tidak Termasuk',
    label: [
      {
        desc: [
          'PPn 1%, Airport Tax Internasional, Fuel Surcharge, Asuransi Perjalanan, Dokumen Perjalanan (passport, visa).',
          'Pengeluaran pribadi seperti: mini bar, room service, telepon, laundry, tambahan makanan dan minuman di restoran, dll.',
          'Tour tambahan (optional) yang mungkin diadakan selama perjalanan.',
          'Biaya kelebihan berat barang-barang bawaan di atas 20kg (excess baggage) atau sesuai dengan peraturan penerbangan yang digunakan.',
          'Bea masuk bagi barang-barang yang dikenakan oleh bea & cukai di Jakarta maupun di negara-negara yang dikunjungi.',
          'Tip untuk Pengemudi, Porter, Local Guide dan Tour Leader.',
          'Biaya Single Supplement bagi Peserta yang menempati 1 kamar sendiri.'
        ]
      }
    ],
    type: true
  }],
  [{
    title: 'Pendaftaran & Pembayaran',
    label: [
      {
        desc: [
          'Uang muka pendaftaran yang dibayarkan kepada PT. Golden Rama Express (“Golden Rama Tours & Travel”) tidak dapat dikembalikan (down payment non-refundable) dengan pembayaran IDR 7.000.000,-/per Peserta untuk tujuan Korea, Jepang, Asia Tenggara, China & Taiwan dan pembayaran IDR 10.000.000,-/per Peserta untuk tujuan Eropa, Amerika & Kanada, Australia, Selandia Baru, Afrika dan Asia Tengah.',
          'Peserta bersedia memenuhi kelengkapan persyaratan dokumen sesuai jadwal dan ketentuan  dari pihak Kedutaan Negara yang dituju.  Biaya visa tetap harus dibayarkan walaupun visa tidak disetujui oleh Kedutaan, demikian juga jika terdapat biaya lain seperti pembatalan  hotel, kereta dan atau  tiket pesawat yang terjadi karena adanya tenggat waktu yang belum tentu sesuai dengan waktu penyelesaian proses visa dari Kedutaan, dan juga biaya tour lainnya maka akan dibebankan kepada Peserta tour.',
          'Pendaftaran tanpa disertai deposit bersifat tidak mengikat dan dapat dibatalkan tanpa pemberitahuan terlebih dahulu kepada Peserta.',
          'Pelunasan biaya tour dilakukan 14 hari sebelum tanggal keberangkatan.',
          'Pendaftaran yang dilakukan kurang dari 15 hari sebelum tanggal keberangkatan harus langsung dibayar lunas.',
          'Bagi pendaftar yang berusia di atas 70 tahun atau memiliki keterbatasan fungsi anggota tubuh atau indera atau keterbatasan secara mental, wajib  didampingi oleh anggota keluarga, teman atau saudara yang akan bertanggung jawab selama perjalanan tour.',
        ]
      }
    ],
  }],
  [{
    title: 'Deviasi',
    label: [
      {
        header: '(Deviasi; Perubahan, perpanjangan, penambahan/penyimpangan rute perjalanan di luar rute perjalanan yang telah dijadwalkan oleh Golden Rama Tours & Travel)',
        desc: [
          'Deviasi dapat diproses apabila sudah melakukan deposit dan melampirkan fotokopi paspor.',
          'Deviasi dapat dilakukan apabila jumlah Peserta yang berangkat dan yang pulang telah memenuhi kuota dari ketentuan maskapai penerbangan.',
          'Apabila deviasi sudah disetujui maka akan dikenakan biaya sesuai dengan ketentuan maskapai penerbangan dan tidak dapat kembali ke jadwal semula.',
          'Golden Rama Tours & Travel tidak menjamin konfirmasi pesawat, hotel dan sebagainya bila Peserta menghendaki perpanjangan jadwal paket tour. Apabila permintaan deviasi tidak dapat disetujui oleh pihak maskapai penerbangan maka Peserta secara otomatis akan kembali ke jadwal semula.',
          'Deviasi yang akan mempersingkat jadwal paket tour, tidak diberikan pengurangan biaya dari biaya paket standar semula.'
        ]
      }
    ],
  }],
  [{
    title: 'Pembatalan',
    label: [
      {
        header: 'Jika terjadi pembatalan acara tour oleh Peserta sebelum tanggal keberangkatan maka biaya pembatalan adalah sebagai berikut:',
        desc: [
          'Setelah pendaftaran:\nUang muka pendaftaran (Non Refundable)',
          '30-15 hari kalender sebelum tanggal keberangkatan:\n50% dari biaya tour',
          '14-07 hari kalender sebelum tanggal keberangkatan:\n75% dari biaya tour',
          '06-00 hari kalender sebelum tanggal keberangkatan:\n100% dari biaya tour'
        ]
      },
      {
        header: 'Biaya pembatalan di atas juga berlaku bagi:',
        desc: [
          'Peserta yang mengganti tanggal keberangkatan atau mengganti paket/jenis tour.',
          'Peserta yang terlambat memberikan persyaratan visa dari batas waktu yang telah di tentukan Golden Rama Tours & Travel dan mengakibatkan Peserta tidak dapat berangkat tepat pada waktunya karena permohonan visa nya masih diperoses oleh Kedutaan.',
          'Pembatalan yang dilakukan oleh salah satu pihak (Peserta atau Golden Rama Tours & Travel) karena bencana alam, perang, wabah penyakit, aksi teroris atau keadaan ‘Force Majeure’ lainnya, maka ketentuan-ketentuan di atas dapat berubah sewaktu-waktu tanpa pemberitahuan terlebih dahulu, tergantung dari kebijakan pihak airlines, hotel dan agen di luar negeri.\n(Force Majeure; Suatu kejadian yang terjadi di luar kemampuan manusia dan tidak dapat dihindarkan sehingga suatu kegiatan tidak dapat dilaksanakan sebagaimana mestinya).',
          'Golden Rama Tours & Travel berhak membatalkan pendaftaran Peserta tour yang belum membayar uang muka atau pelunasan sesuai batas waktu yang telah ditentukan Golden Rama Tours & Travel.',
          'Bila permohonan visa ditolak, sedangkan tiket sudah diterbitkan sebelum permohonan visa disetujui, karena keharusan sehubungan dengan tenggat waktu yang ditentukan perusahaan penerbangan (airlines), maka biaya visa tidak dapat dikembalikan dan Peserta tetap dikenakan denda pembatalan dan administrasi sesuai dengan kondisi terkait pihak airlines, hotel dan agen di luar negeri.',
          'Uang muka pendaftaran Peserta tidak dapat dikembalikan bila Peserta melakukan pembatalan secara sepihak.'
        ]
      }
    ],
  }],
  [{
    title: 'Pengembalian Uang',
    label: [
      {
        desc: [
          'Tiket pesawat udara, kereta api, dan transportasi lainnya serta akomodasi yang tidak terpakai tidak dapat diuangkan kembali (non-refundable).',
          'Bila calon Peserta tour berhalangan/sakit sebelum tanggal keberangkatan yang dijadwalkan maka pengembalian uang/biaya pembatalan, akan mengacu kepada pasal pembatalan.',
          'Bila ada pelayanan dalam paket yang tidak digunakan oleh para Peserta dikarenakan berhalangan atau sakit selama perjalanan, para Peserta tidak berhak menuntut uang kembali.',
          'Bagi Peserta tour yang tidak diijinkan masuk atau dikenakan tindakan deportasi oleh pihak imigrasi Negara setempat (walaupun sudah memiliki visa), atau yang ditolak oleh perusahaan penerbangan, atau dalam perjalanan menderita sakit, atau ada kelainan jiwa, atau dalam perjalanan mengalami kecelakaan, yang terpaksa harus kembali atau menyimpang dari perjalanan yang telah ditentukan dalam acara tour, atau terpaksa membatalkan sebagian/hampir seluruh perjalanan setelah keberangkatan, tidak berhak atas pengembalian uang atau bentuk pengembalian lain apapun atas jasa-jasa yang belum atau tidak digunakan.'
        ]
      }
    ],
  }],
  [{
    title: 'Pihak Golden Rama Tours & Travel dan Seluruh Agen - Agen Tidak Bertanggung Jawab Dan Tidak Bisa Dituntut Atas:',
    label: [
      {
        desc: [
          'Kecelakaan, kehilangan koper dan keterlambatan tibanya koper akibat tindakan pihak maskapai penerbangan atau alat pengangkutan lainnya, maka standard penggantian didasarkan pada ketentuan maskapai penerbangan internasional atau penyedia jasa pengangkutan yang digunakan.',
          'Kehilangan barang pribadi, koper, titipan barang di airport, hotel dan tindakan kriminal yang menimpa Peserta tour selama perjalanan.',
          'Keterlambatan atau pembatalan jadwal penerbangan, dan seluruh kejadian yang terjadi di luar kuasa pihak Golden Rama Tours & Travel.',
          'Perubahan atau berkurangnya acara perjalanan akibat dari bencana alam, kerusuhan dan lain sebagainya yang bersifat ‘Force Majeure’.',
          'Meninggalkan Peserta akibat sakit yang diderita atau kecelakaan selama dalam tour.'
        ]
      }
    ],
  }],
  [{
    title: 'Pihak Golden Rama Tours & Travel Dan Seluruh Agen-Agen Berhak:',
    label: [
      {
        desc: [
          'Demi kenyamanan dan kelancaran perencanaan perjalanan tour, Golden Rama Tours & Travel berhak untuk menerbitkan tiket pesawat, kereta api dan transportasi lainnya, akomodasi, tiket masuk objek wisata tanpa melakukan konfirmasi lisan maupun tertulis kepada pendaftar yang telah melakukan deposit.',
          'Membatalkan tanggal keberangkatan tour, apabila jumlah Peserta kurang dari 15 Peserta dewasa, dan biaya tour yang telah dibayarkan akan dikembalikan seluruhnya.',
          'Demi kenyamanan dan kelancaran perencanaan perjalanan tour, Golden Rama Tours & Travel berhak meminta Peserta tour untuk keluar dari rombongan apabila Peserta tour yang bersangkutan mencoba membuat kerusuhan, mengacaukan acara tour, meminta dengan paksa, dan memberikan informasi yang tidak benar mengenai acara tour, dll.',
          'Mengganti hotel-hotel yang akan digunakan berhubung hotel tersebut sudah penuh dan mengganti dengan hotel lain yang setaraf sesuai dengan pertimbangan dan konfirmasi. Apabila dalam periode tour di kota-kota yang dikunjungi sedang berlangsung pameran/konferensi maka akan diganti dengan hotel-hotel lain yang setaraf di kota-kota terdekat.',
          'Jadwal tour dapat berubah sewaktu-waktu mengikuti kondisi yang memungkinkan dengan tanpa mengurangi isi dalam acara tour tersebut.',
          'Pihak Golden Rama Tours & Travel berhak menagih selisih harga tour dan lain-lainnya (jika terjadi kenaikan harga tour, airport tax, dll) kepada calon Peserta.'
        ]
      }
    ],
  }],
];

export default TravelTermsConditions;
