const Titles = [
  {
    id: 1,
    name: 'Mr.',
    abbr: 'MR',
    longName: 'Tuan',
    isAdult: true
  },
  {
    id: 2,
    name: 'Mrs.',
    abbr: 'MRS',
    longName: 'Nyonya',
    isAdult: true
  },
  {
    id: 3,
    name: 'Ms.',
    abbr: 'MS',
    longName: 'Nona',
    isAdult: true
  },
  {
    id: 4,
    name: 'Mstr.',
    abbr: 'MSTR',
    longName: 'Tuan',
    isAdult: false
  },
  {
    id: 5,
    name: 'Miss.',
    abbr: 'MISS',
    longName: 'Nona',
    isAdult: false
  }
];

  export default Titles;
