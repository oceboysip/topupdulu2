import React, { Component } from "react";
import {Alert,Platform, StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions, Button} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import CalendarStyle from '../style/CalendarStyle';
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';

import Color from './Color';

export default class CalendarCheckinScreen extends Component<{}> {
  constructor(props){
  super(props)
    this.state = {
    };
    this.onDayPress = this.onDayPress.bind(this);
  }

  BookedHotel = () => {
    this.props.navigation.navigate('BookedHotel');
  }
  render() {
    return (
      <View style={styles.container}>

        <View style={styles.navTop}>
          <View style={styles.rowsBeetwen}>
            <View style={{flexDirection: 'row',alignItems:'center'}}>
              <TouchableOpacity onPress={this.BookedHotel}>
                <Image style={{width:30, height:30,resizeMode:'contain'}} source={require('../images/left-arrow.png')}/>
              </TouchableOpacity>
              <Text style={styles.titlePage}>Tanggal Check-in</Text>
            </View>
          </View>
        </View>

        
        <ScrollView>
          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <Calendar
                onDayPress={this.onDayPress}
                style={styles.calendar}
                hideExtraDays
                markedDates={{[this.state.selected]: {selected: true, disableTouchEvent: true, selectedDotColor: '#fcdf00'}}}
                theme={{
                  monthTextColor: '#fcdf00',
                  dayTextColor: Color.theme,
                  arrowColor: Color.theme,
                  textSectionTitleColor: Color.theme,
                  selectedDayBackgroundColor: '#fcdf00',
                  selectedDayTextColor: '#ffffff',

                }}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
  onDayPress(day) {
    this.setState({
      selected: day.dateString
    });
  }
}




// STYLES
// ------------------------------------
const styles = StyleSheet.create(CalendarStyle);