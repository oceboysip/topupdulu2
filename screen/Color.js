const colors = {
    theme: '#1A558F',
    primary: '#fcdf00',
    disabled: '#CFE7FF',
    placeholder: '#A8A699',
    text: '#231F20',
    button: '#231F20',
    loading: '#231F20',
    darkGrey: '#696969',
    border: '#DDDDDD',
    white: '#FFFFFF',
    semuGray: '#A8A699',
    success: '#2D8B57',
    info: '#5887FB',
    error: '#FF0000',
    warning: '#F1940C'
};
//theme: #145c94
export default colors;
