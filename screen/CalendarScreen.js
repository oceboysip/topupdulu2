import React, { Component } from "react";
import { SafeAreaView, StyleSheet, Text, View, Image, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { Calendar } from 'react-native-calendars';
import Moment from 'moment';

import CalendarStyle from '../style/CalendarStyle';
import Color from './Color';
import Header from './Header';
import BannerSlider from './BannerSlider';

export default class CalendarScreen extends Component {
  constructor(props){
  super(props)
    this.state = {
      selectedDate: Moment(props.selectedDate).format('YYYY-MM-DD')
    };
  }

  Dashboard = () => {
    this.props.navigation.navigate('Dashboard');
  }

  onDayPress(day) {
    //ini ambil YYYY-MM-DD
    this.setState({ selectedDate: day.dateString });
    this.props.onSelectedDate(Moment(day.dateString));
    this.props.onClose();
  }

  render() {
    console.log(this.props, 'calendar');
    console.log(this.state);
    const { minDate, maxDate, label } = this.props;
    return (
      <View style={styles.container}>
        <SafeAreaView style={{backgroundColor: Color.theme}}>
          <Header title={label} onPressLeftButton={() => this.props.onClose()} />

          <ScrollView style={{backgroundColor: '#F5F6F7', paddingTop: 16}}>
            <View style={styles.rows}>
              <View style={styles.boxShadow}>
                <Calendar
                  minDate={Moment(minDate).format('YYYY-MM-DD')}
                  maxDate={Moment(maxDate).format('YYYY-MM-DD')}
                  current={Moment().format('YYYY-MM-DD')}
                  onDayPress={(day) => this.onDayPress(day)}
                  style={styles.calendar}
                  hideExtraDays
                  markedDates={{[this.state.selectedDate]: {
                    selected: true,
                    disableTouchEvent: true,
                    selectedDotColor: '#fcdf00'
                  }}}
                  theme={{
                    monthTextColor: Color.theme,
                    dayTextColor: Color.theme,
                    arrowColor: Color.theme,
                    textSectionTitleColor: Color.theme,
                    selectedDayBackgroundColor: Color.theme,
                    selectedDayTextColor: '#FFFFFF',
                    todayTextColor: Color.primary
                  }}
                />
              </View>
            </View>

            <BannerSlider {...this.props} />
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}

// STYLES
// ------------------------------------
const styles = StyleSheet.create(CalendarStyle);
