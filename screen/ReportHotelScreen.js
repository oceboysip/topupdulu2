/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {Alert,Platform, StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions, Button,Switch} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import ReportKeretaStyle from '../style/ReportKeretaStyle';

import ToggleSwitch from "toggle-switch-react-native";

export default class ReportHotelScreen extends Component<{}> {

  constructor(props){
  super(props)
    this.state = {
      isOnDefaultToggleSwitch: false,
      isOnLargeToggleSwitch: false,
      isOnBlueToggleSwitch: false,
      isHidden: false
    };
  }

  onToggle(isOn) {
    console.log("Changed to " + isOn);
  }
  BookedTrainScreen = () => {
    this.props.navigation.navigate('BookedTrainScreen');
  }
  
  render() {
    return (
      <View style={styles.container}>

        <View style={styles.navTop}>
          <View style={styles.rowsBeetwen}>
            <View style={{flexDirection: 'row',alignItems:'center'}}>
              <TouchableOpacity onPress={this.BookedTrainScreen}>
                <Image style={{width:30, height:30,resizeMode:'contain'}} source={require('../images/left-arrow.png')}/>
              </TouchableOpacity>
              <Text style={styles.titlePage}>Laporan Transaksi Hotel</Text>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={{flexDirection: 'row',alignItems:'center',paddingLeft:0}}>
              <View style={styles.testTop2}>
                <Text style={styles.borderYellow}>DARI</Text>
                <Text style={styles.biglWhite}>09-10-2019</Text>
              </View>

               <View style={styles.testTop2}>
                <Text style={styles.borderYellow}>SAMPAI</Text>
                <Text style={styles.biglWhite}>12-10-2019</Text>
              </View>
              
            </View>

          </View>

        </View>

        
        <ScrollView>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRows}>
                  <View style={styles.iconForm}>
                    <Image style={{width:30, height:30, resizeMode:'contain',marginVertical:10}}  source={require('../images/icon_hotel.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.textSuccess}>Success</Text>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRows}>
                  <View style={styles.iconForm}>
                    <Image style={{width:30, height:30, resizeMode:'contain',marginVertical:10}}  source={require('../images/icon_hotel.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.textSuccess}>Failed</Text>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRows}>
                  <View style={styles.iconForm}>
                    <Image style={{width:30, height:30, resizeMode:'contain',marginVertical:10}}  source={require('../images/icon_hotel.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.textSuccess}>Success</Text>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRows}>
                  <View style={styles.iconForm}>
                    <Image style={{width:30, height:30, resizeMode:'contain',marginVertical:10}}  source={require('../images/icon_hotel.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.textSuccess}>Failed</Text>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRows}>
                  <View style={styles.iconForm}>
                    <Image style={{width:30, height:30, resizeMode:'contain',marginVertical:10}}  source={require('../images/icon_hotel.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.textSuccess}>Success</Text>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRows}>
                  <View style={styles.iconForm}>
                    <Image style={{width:30, height:30, resizeMode:'contain',marginVertical:10}}  source={require('../images/icon_hotel.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.textSuccess}>Failed</Text>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRows}>
                  <View style={styles.iconForm}>
                    <Image style={{width:30, height:30, resizeMode:'contain',marginVertical:10}}  source={require('../images/icon_hotel.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.textSuccess}>Success</Text>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRows}>
                  <View style={styles.iconForm}>
                    <Image style={{width:30, height:30, resizeMode:'contain',marginVertical:10}}  source={require('../images/icon_hotel.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.textSuccess}>Failed</Text>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRows}>
                  <View style={styles.iconForm}>
                    <Image style={{width:30, height:30, resizeMode:'contain',marginVertical:10}}  source={require('../images/icon_hotel.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.textSuccess}>Success</Text>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRows}>
                  <View style={styles.iconForm}>
                    <Image style={{width:30, height:30, resizeMode:'contain', paddingHorizontal:10}}  source={require('../images/icon_hotel.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.textSuccess}>Failed</Text>
                  </View>
                </View>

              </View>
            </View>
          </View>

          
        </ScrollView>
      </View>
    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}




// STYLES
// ------------------------------------
const styles = StyleSheet.create(ReportKeretaStyle);