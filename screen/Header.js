import React, { Component } from 'react';
import { View, Image, Text, Dimensions } from 'react-native';
import Styled from 'styled-components';
import { withNavigation } from 'react-navigation';

import Icon from 'react-native-vector-icons/MaterialIcons';
// import Text from '../screen/Text';
import Color from '../screen/Color';
import TouchableOpacity from '../screen/Button/TouchableDebounce';

const TransparentMainView = Styled(View)`
    backgroundColor: transparent;
    paddingHorizontal: 16;
    paddingVertical: 10;
    width: 100%;
`;

const TransparentBackView = Styled(View)`
    width: 100%;
    aspectRatio: 1;
    justifyContent: center;
    alignItems: center;
    backgroundColor: #FFFFFF;
    borderRadius: 35;
`;

const SideButton = Styled(TouchableOpacity)`
    flex: 1;
    width: 100%;
    alignItems: center;
    justifyContent: center;
`;

const SideIcon = Styled(Icon)`
    fontSize: 23;
    color: #000000;
`;

const BackIcon = Styled(Icon)`
    fontSize: 30;
    color: ${Color.theme};
`;

const TitleText = Styled(Text)`
    fontSize: 14;
`;

const MainViewFixMode = Styled(View)`
    paddingHorizontal: 16;
    backgroundColor: ${Color.theme};
    shadowColor: #000;
    paddingVertical: 10;
    width: 100%;
`;

const ContainerOptionView = Styled(View)`
    flexDirection: row;
    paddingVertical: 10;
    justifyContent: space-between;
    alignSelf: stretch;
    alignItems: center;
`;

const TextFixMode = Styled(Text)`
    fontSize: 17;
    borderRadius: 15;
    fontWeight: bold;
    paddingVertical: 5;
    paddingHorizontal: 10;
    backgroundColor: #FFFFFF;
    textTransform: uppercase;
`;

const defaultProps = {
    numberOfLines: 2,
    color: Color.theme,
    showLeftButton: false,
    showIconLeftButton: true,
    iconLeftButton: 'arrow-back',
};

const { width } = Dimensions.get('window');

class Header extends Component {

  onPressRightButton = () => {
    if (this.props.onPressRightButton) this.props.onPressRightButton();
  }

  onPressLeftButton = () => {
    if (this.props.onPressLeftButton) this.props.onPressLeftButton();
    else this.props.navigation.pop();
  }

  renderTransparentMode() {
    const { showLeftButton, showIconLeftButton, iconLeftButton, imageRightButton, children, ...style } = this.props;
    return (
      <TransparentMainView {...style}>
        <ContainerOptionView>
          <View style={{alignItems: 'flex-start', width: '10%'}}>
            {/* {showLeftButton ? <SideButton onPress={this.onPressLeftButton}>
              <TransparentBackView>
                {showIconLeftButton && <BackIcon name={iconLeftButton || 'md-arrow-round-back'} />}
              </TransparentBackView>
            </SideButton> : <View />} */}
          </View>

          <View style={[{paddingLeft: 8, width: '90%', alignItems: 'flex-start', justifyContent: 'center'}, imageRightButton && {width: '80%'}]}>
            {children || <View />}
          </View>

          {imageRightButton && <View style={{width: '10%', alignItems: 'flex-end'}}>
            <TouchableOpacity onPress={this.onPressRightButton}>
              <Image style={{width:30, height:30, resizeMode: 'contain'}} source={imageRightButton}/>
            </TouchableOpacity>
          </View>}
        </ContainerOptionView>
      </TransparentMainView>
    );
  }

  renderFixMode() {
    const { showLeftButton, showIconLeftButton, iconLeftButton, iconRightButton, imageRightButton, titleRight, color, title, children, numberOfLines, ...style } = this.props;
    return (
      <MainViewFixMode {...style}>
        <ContainerOptionView>
          <View style={{alignItems: 'flex-start', width: '10%'}}>
            <TouchableOpacity onPress={this.onPressLeftButton}>
              <Image style={{width:30, height:30, resizeMode: 'contain'}} source={require('../images/left-arrow.png')}/>
            </TouchableOpacity>
          </View>

          <View style={[{paddingLeft: 8, width: '90%', alignItems: 'flex-start', justifyContent: 'center'}, (imageRightButton || iconRightButton) && {width: '80%'}]}>
            {children || <TextFixMode style={{color}}>{title}</TextFixMode>}
          </View>

          {imageRightButton && <View style={{width: '10%', alignItems: 'flex-end'}}>
            <TouchableOpacity onPress={this.onPressRightButton}>
              <Image style={{width: 30, height: 30, resizeMode: 'contain'}} source={imageRightButton}/>
            </TouchableOpacity>
          </View>}

          {iconRightButton && <View style={{width: '10%', alignItems: 'flex-end'}}>
            <TouchableOpacity onPress={this.onPressRightButton}>
              {iconRightButton}
            </TouchableOpacity>
          </View>}
        </ContainerOptionView>
      </MainViewFixMode>
    )
  }

  render() {
    // return this.renderFixMode();
    const { transparentMode } = this.props;
    if (!transparentMode) return this.renderFixMode();
    return this.renderTransparentMode();
  }
}

Header.defaultProps = defaultProps;

export default withNavigation(Header);
