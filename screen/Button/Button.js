import React, { Component } from 'react';
import { Image } from 'react-native';
import Styled from 'styled-components';

import Text from '../Text';
import Color from '../Color';
import TouchableOpacity from './TouchableDebounce';

const CustomButton = Styled(TouchableOpacity)`
    backgroundColor: ${props => props.disabled ? '#E7CE3E' : props.color || Color.theme};
    width: 100%;
    borderRadius: 100;
    alignItems: center;
    justifyContent: center;
    flexDirection: row;
`;

const CustomText = Styled(Text)`
    color: ${props => props.disabled ? '#D2BD3F' : props.fontColor || 'white'};
    textAlign: center;
    fontSize: ${props => props.fontSize || 14};
`;

const CustomImage = Styled(Image)`
  width: 10;
  height: 10;
  marginRight: 3;
`;

class Button extends Component {
  render() {
    const { fontColor, fontSize, color, onPress, children, disabled, type, source, ...style } = this.props;
    return (
      <CustomButton {...style} {...this.props} color={color} onPress={!disabled && onPress}>
        {source && <CustomImage source={source} />}
        <CustomText type={type || 'semibold'} disabled={disabled} fontColor={fontColor} fontSize={fontSize}>{children}</CustomText>
      </CustomButton>
    );
  }
}

export default Button;
