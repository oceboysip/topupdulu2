import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';

let ready = true;

class TouchableDebounce extends Component {

  debounce(onPress, wait, context = this) {
    let cooldown = null;
    const promise = () => { ready = true; };

    return function () {
      if (!ready) return;
      ready = false;
      onPress.apply(context, arguments);
      if (cooldown) clearTimeout(cooldown);
      cooldown = setTimeout(promise, wait);
    };
  }

  render() {
    const { disabled, style, activeOpacity, onPress, children, delay } = this.props;
    return (
      <TouchableOpacity
        disabled={disabled}
        style={style}
        activeOpacity={activeOpacity || 1}
        onPress={this.debounce(() => onPress && onPress(), delay ? delay : 200 )}
      >
        {children}
      </TouchableOpacity>
    );
  }
}

export default TouchableDebounce;
