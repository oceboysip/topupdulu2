import React, { Component } from 'react';
import { Text as ReactText } from 'react-native';
import Styled from 'styled-components';

import Color from './Color';

// const fontFamily = {
//   regular: 'Montserrat-Regular',
//   medium: 'Montserrat-Medium',
//   semibold: 'Montserrat-SemiBold',
//   bold: 'Montserrat-Bold',
//   light: 'Montserrat-Light',
// };
// fontFamily: ${props => fontFamily[props.type] || fontFamily.regular};

const BaseText = Styled(ReactText)`
  fontSize: ${props => props.size || '15'};  
  textAlign: ${props => props.align || 'center'};
  color: ${Color.text};
  textShadowRadius: 0;
  fontWeight: ${props => props.type == 'bold' ? 'bold' : 'normal'};
  lineHeight: 18;
`;

class Text extends Component {
  render() {
    const { align, children, size, type, ...style } = this.props;
    return (
      <BaseText {...style} align={align} type={type} size={size} allowFontScaling={false}>
        {children}
      </BaseText>
    );
  }
}

export default Text;
