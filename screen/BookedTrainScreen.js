/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {Alert,Platform, StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions, Button,Switch,TouchableWithoutFeedback} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Dialog, {SlideAnimation, DialogContent } from 'react-native-popup-dialog';
import BottomSheet from 'reanimated-bottom-sheet'
import SlidingUpPanel from 'rn-sliding-up-panel';
import BookedTrainStyle from '../style/BookedTrainStyle';



import ToggleSwitch from "toggle-switch-react-native";

export default class BookedTrainScreen extends Component {

  constructor(props){
  super(props)
    this.state = {
      isOnDefaultToggleSwitch: false,
      isOnLargeToggleSwitch: false,
      isOnBlueToggleSwitch: false,
      isHidden: false
    };
  }

  onToggle(isOn) {
    console.log("Changed to " + isOn);
  }
  Dashboard = () => {
    this.props.navigation.navigate('Dashboard');
  }
  CalendarBerangkat = () => {
    this.props.navigation.navigate('Calendar');
  }
  CalendarPulang = () => {
    this.props.navigation.navigate('CalendarPulang');
  }
  SearchTerminal = () => {
    this.props.navigation.navigate('SearchTerminal');
  }
  SearchTerminalPulang = () => {
    this.props.navigation.navigate('SearchTerminalPulang');
  }
  HasilSearch = () => {
    this.props.navigation.navigate('HasilSearch');
  }
  ReportKereta = () => {
    this.props.navigation.navigate('ReportKereta');
  }

  renderInner = () => (
    <View style={styles.panel}>
      <View style={styles.rowPanel}>
        <Image style={{width:70, height:12,resizeMode:'contain',marginBottom:10}} source={require('../images/block_panel.png')} />
        <Text style={styles.BigText2}>Tambah Penumpang</Text>
      </View>
      <View style={styles.rowPanelDirect}>
        <View style={styles.flexRow}>
          <Text style={styles.greyBig}>Dewasa</Text>
          <Text style={styles.greySmall}>Diatas 12 tahun</Text>
        </View>
        <View style={styles.flexRow}>
          <Text style={styles.BigText2}>1</Text>
        </View>
        <View style={styles.flexRow2}>
          <TouchableOpacity> 
            <Text style={styles.plusMinusbt}>-</Text> 
          </TouchableOpacity>

          <TouchableOpacity> 
            <Text style={styles.plusMinusbt}>+</Text> 
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.rowPanelDirect}>  
        <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}}>
        </View>
      </View>

      <View style={styles.rowPanelDirect}>
        <View style={styles.flexRow}>
          <Text style={styles.greyBig}>Anak-anak</Text>
          <Text style={styles.greySmall}>2-11 tahun</Text>
        </View>
        <View style={styles.flexRow}>
          <Text style={styles.BigText2}>1</Text>
        </View>
        <View style={styles.flexRow2}>
          <TouchableOpacity> 
            <Text style={styles.plusMinusbt}>-</Text> 
          </TouchableOpacity>

          <TouchableOpacity> 
            <Text style={styles.plusMinusbt}>+</Text> 
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.rowPanelDirect}>  
        <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}}>
        </View>
      </View>

      <View style={styles.rowPanelDirect}>
        <View style={styles.flexRow}>
          <Text style={styles.greyBig}>Bayi</Text>
          <Text style={styles.greySmall}>Di bawah 2 tahun</Text>
        </View>
        <View style={styles.flexRow}>
          <Text style={styles.BigText2}>0</Text>
        </View>
        <View style={styles.flexRow2}>
          <TouchableOpacity> 
            <Text style={styles.plusMinusbt}>-</Text> 
          </TouchableOpacity>

          <TouchableOpacity> 
            <Text style={styles.plusMinusbt}>+</Text> 
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.rowPanelDirect}>
        <TouchableOpacity onPress={() => this.bs.current.snapTo(3)} style={styles.buttonYellow}>
        <Text style={styles.whiteText}>Batal</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.bs.current.snapTo(3)} style={styles.buttonGreen}>
        <Text style={styles.whiteText}>Selesai</Text>
        </TouchableOpacity>
      </View>

    </View>
  )

  bs = React.createRef()


  render() {
    return (
      <View style={styles.container}>
      

        <View style={styles.navTop}>
          <View style={styles.rowsBeetwen}>
            <View style={{flexDirection: 'row',alignItems:'center'}}>
              <TouchableOpacity onPress={this.Dashboard}>
                <Image style={{width:30, height:30,resizeMode:'contain'}} source={require('../images/left-arrow.png')}/>
              </TouchableOpacity>
              <Text style={styles.titlePage}>Tiket Kereta</Text>
            </View>

            <View style={{flexDirection:'row'}}>
              <TouchableOpacity onPress={this.ReportKereta}>
                <Image style={{width:30, height:30, resizeMode:'contain'}}  source={require('../images/icon_paperWhite.png')}/>
               </TouchableOpacity>
            </View>
          </View>

        </View>
   

        
        <ScrollView>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRows}>
                  <View style={styles.iconForm}>
                    <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../images/icon_train.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.smallText}>Dari</Text>
                    <TouchableOpacity onPress={this.SearchTerminal}>
                      <Text style={styles.BigText}> GAMBIR (GMR)</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={styles.rows2}>
                  <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}}>
                  </View>
                  <TouchableOpacity style={{position:'absolute',right:0,top:-20,paddingLeft:10,backgroundColor:"#fff"}}>
                    <Image style={{width:40, height:40, resizeMode:'contain'}}  source={require('../images/icon_refresh_orange.png')}/>
                  </TouchableOpacity>
                </View>

                <View style={styles.flexRowsLast}>
                  <View style={styles.iconForm}>
                    <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../images/icon_train.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.smallText}>Ke</Text>
                    <TouchableOpacity onPress={this.SearchTerminalPulang}>
                      <Text style={styles.BigText}>BANDUNG (BD)</Text>
                    </TouchableOpacity>
                  </View>
                </View>

              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>

                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                    <View style={styles.iconForm}>
                      <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../images/icon_date.png')}/>
                    </View>
                    <View style={styles.captionForm}>
                      <Text style={styles.smallText}>Berangkat</Text>
                      <TouchableOpacity onPress={this.CalendarBerangkat}>
                        <Text style={styles.BigText}>Tue, 10 Sep 2019</Text>
                      </TouchableOpacity>
                    </View>
                  </View>

                  <View style={{alignItems:'flex-end'}}>
                    <Text style={styles.smallText}>Pulang-pergi?</Text>
                     <Switch
                        onValueChange={value => this.setState({ isHidden: value })}
                        value={this.state.isHidden}
                      />
                  </View>
                </View>

                <View style={styles.rows2}>
               
                  <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}}>
                  </View>
                </View>

                <View style={styles.flexRows} hide={this.state.isHidden}>
                  <View style={styles.iconForm}>
                    <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../images/icon_date.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.smallText}>Pulang</Text>
                    <TouchableOpacity onPress={this.CalendarPulang}>
                      <Text style={styles.BigText}>Mon, 16 Sep 2019</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View style={styles.rows2}>
                  <View style={{flex:1,height:2,backgroundColor:'#fcdf00'}}>
                  </View>
                </View>

                <View style={styles.flexRowsLast}>
                  <View style={styles.iconForm}>
                    <Image style={{width:40, height:34, resizeMode:'contain'}}  source={require('../images/icon_people.png')}/>
                  </View>
                  <View style={styles.captionForm}>
                    <Text style={styles.smallText}>Penumpang</Text>
                    <TouchableOpacity onPress={() => this.bs.current.snapTo(0)}>
                      <Text style={styles.BigText}>1 Dewasa</Text>
                    </TouchableOpacity>
                  </View>
                </View>

              </View>
            </View>
          </View>
          <View style={styles.alCenter}>
            <TouchableOpacity style={styles.button} onPress={this.HasilSearch}>
              <Text style={styles.buttonText}>CARI</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>

        <BottomSheet
            ref={this.bs}
            snapPoints={[350, 200, 100, 0]}
            renderContent={this.renderInner}
            initialSnap={3}
            enabledGestureInteraction={false}
          />
        
      </View>
    )
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  }
}




// STYLES
// ------------------------------------
const styles = StyleSheet.create(BookedTrainStyle);