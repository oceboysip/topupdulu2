import React, { Component } from 'react';
import { Alert, StyleSheet, View, Image, TouchableOpacity, Dimensions, ActivityIndicator } from 'react-native';
import Carousel from 'react-native-banner-carousel';
import axios from 'axios';

import { constant } from '../constants';
import DashboardStyle from '../style/DashboardStyle';


export default class NewsListDepan extends Component {
    onMounting = true;

    constructor(props) {
        super(props)
        this.state = {
            loading: true,
            tempContentBerita: []
        };
    }

    componentDidMount() {
        this._loadBerita();
    }

    componentWillUnmount() {
        this.onMounting = false;
    }

    _loadBerita = () => {
        axios.get("http://ataprumah.com/api/headline", {headers: {'CARPAN-TOKEN-KEY': '6?spik&6es@g5Cr-p1od'}})
        .then(response => {
          res = response.data;
          console.log('berita'.res);
          if(res.status){
           
            this.setState({tempContentBerita:res.data})
            this.props.screenProps.setLoading(false);
          }
        })
        .catch(function (error) {
          console.log(error);
        });
      }

    rendertempContentNews(image, index, tipe, val){
        let dimensions = Dimensions.get("window");
        let imageHeight = Math.round((dimensions.width * 9) / 16);
        let imageWidth = dimensions.width;  
        return (
            <View key={index}>
               <View style={styles.rowNews}>
                <View style={styles.bigGallery}>
                  <Image style={{ height: imageHeight, width: imageWidth }} source={require('../images/dummy_thumb.jpg')}/>
                  <View style={styles.absOverflow}></View>
                </View>
                <View style={styles.absTitle}>
                  <Text style={styles.titleBig}>Empat Prioritas Kemendikbud Pada RPJMN 2020-2024</Text>
                  <Text style={styles.smallpara}>Lorem ipsum dolorsit amet bla...bla...bla...bla...</Text>
                </View>
              </View>
            </View>
        );
    }

    renderNews() {
        const { tempContentBerita, loading } = this.state;

        if (tempContentBerita.length > 0 && !loading) {
            return tempContentBerita.map((item, key) =>
                this.rendertempContentNews(item.file, key, item.tipe, item.value)
            )
        } else {
            return (
                <View style={{height: 155,  backgroundColor: '#DDDDDD', justifyContent: 'center'}}>
                    <ActivityIndicator />
                </View>
            )
        }
      }
    
    render() {
        return (
            <View style={styles.boxInner_bt}>
               
                    {this.renderNews()}
               
            </View>
        )
    }
}

const styles = StyleSheet.create(DashboardStyle);