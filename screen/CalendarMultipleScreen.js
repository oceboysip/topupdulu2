import React, { Component } from "react";
import { SafeAreaView, StyleSheet, Text, View, ScrollView, Modal, TouchableOpacity } from 'react-native';
import { Calendar } from 'react-native-calendars';
import Moment from 'moment';
import Styled from 'styled-components';

import CalendarStyle from '../style/CalendarStyle';
import Color from './Color';
import Header from './Header';
import BannerSlider from './BannerSlider';
import ModalInformation from './Modal/ModalInformation';

const ButtonSelectType = Styled(TouchableOpacity)`
  height: 30;
  width: 95%;
  borderRadius: 15;
  backgroundColor: ${Color.theme};
  justifyContent: center;
  alignItems: center;
  marginTop: 8;
`;

const dateFormat = 'YYYY-MM-DD';

export default class CalendarMultipleScreen extends Component {
  constructor(props){
    super(props)
    let startDate = Moment(props.startDate).format(dateFormat);
    let endDate = Moment(props.endDate).format(dateFormat);

    let markingDate = this.addMarkingDate(startDate, endDate);

    this.state = {
      activeStartDate: props.activeStartDate ? true : false, //if true select data start to end, else select just end date
      startDate,
      endDate,
      selectedDate: {
        [startDate]: { selected: false, startingDay: true, color: Color.theme, textColor: '#FFFFFF' },
        ...markingDate,
        [endDate]: { selected: false, endingDay: true, color: Color.theme, textColor: '#FFFFFF' }
      },
      messageInfo: ''
    };
  }

  Dashboard = () => {
    this.props.navigation.navigate('Dashboard');
  }

  addMarkingDate(startDate, endDate) {
    let diff = Moment(endDate).diff(startDate, 'days');
    let markingDate = {};

    for (let i = 1; i < diff; i++) {
      let marking = Moment(startDate).add(i, 'day').format(dateFormat);
      markingDate[marking] = { selected: false, color: Color.theme, textColor: '#FFFFFF' }
    }

    return markingDate;
  }

  setMessageInfo(messageInfo) {
    this.setState({ messageInfo });
    let timeout = setTimeout(() => {
      clearTimeout(timeout);
      this.setState({ messageInfo: '' });
    }, 3000);
  }

  onDayPress(day) {
    //ini ambil YYYY-MM-DD
    const { activeStartDate } = this.state;
    let date = Moment(day.dateString).format(dateFormat);
    let startDate = this.state.startDate;
    let endDate = this.state.endDate;

    let selected = true;

    if (this.state.selectedDate[date]) {
      selected = !this.state.selectedDate[date].selected;
    }

    let selectedDate = {};

    if (activeStartDate) {
      startDate = date;
      endDate = null;
      selectedDate = { [date]: { selected, startingDay: true, color: Color.theme, textColor: '#FFFFFF' }}
    } else {
      endDate = date;

      if (!Moment(endDate).isAfter(startDate)) {
        this.setMessageInfo('Durasi menginap minimal 1 malam')
        return;
      }

      let markingDate = this.addMarkingDate(startDate, endDate);
      
      selectedDate = {...this.state.selectedDate, ...{ ...markingDate, [date]: { selected, endingDay: true, color: Color.theme, textColor: '#FFFFFF' } } }
    
      this.props.onSelectedDate(Moment(startDate), Moment(endDate));
      this.props.onClose();
    }

    this.setState({ selectedDate, activeStartDate: !activeStartDate, startDate, endDate });
  }

  render() {
    console.log(this.props, 'props calendar');
    console.log(this.state, 'state calendar');
    const { minDate, maxDate, startDateLabel, endDateLabel } = this.props;
    const { activeStartDate, startDate, endDate } = this.state;

    return (
        <SafeAreaView style={{backgroundColor: Color.theme}}>
          <Header title='Pilih Tanggal' onPressLeftButton={() => this.props.onClose()} />

          <ScrollView style={{backgroundColor: '#F5F6F7', paddingTop: 8, height: '100%'}}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20, marginBottom: 20}}>
              <View style={{width: '50%', alignItems: 'center'}}>
                <Text style={{color: Color.theme}}>{startDateLabel}</Text>
                <ButtonSelectType onPress={() => this.setState({ activeStartDate: true })} style={[activeStartDate && {backgroundColor: Color.primary}, {alignSelf: 'flex-start'}]}>
                  <Text style={activeStartDate ? {color: Color.theme} : {color: '#FFFFFF'}}>{Moment(startDate).format('ddd, DD MMM YYYY')}</Text>
                </ButtonSelectType>
              </View>
              <View style={{width: '50%', alignItems: 'center'}}>
                <Text style={{color: Color.theme}}>{endDateLabel}</Text>
                <ButtonSelectType onPress={() => this.setState({ activeStartDate: false })} style={[!activeStartDate && {backgroundColor: Color.primary}, {alignSelf: 'flex-end'}]}>
                  <Text style={!activeStartDate ? {color: Color.theme} : {color: '#FFFFFF'}}>{endDate ? Moment(endDate).format('ddd, DD MMM YYYY') : 'Pilih Tanggal'}</Text>
                </ButtonSelectType>
              </View>
            </View>

            <View style={styles.rows}>
              <View style={styles.boxShadow}>
                <Calendar
                  minDate={Moment(minDate).format('YYYY-MM-DD')}
                  maxDate={Moment(maxDate).format('YYYY-MM-DD')}
                  current={Moment().format('YYYY-MM-DD')}
                  onDayPress={(day) => this.onDayPress(day)}
                  style={styles.calendar}
                  hideExtraDays
                  markedDates={
                    this.state.selectedDate
                  }
                  markingType='period'
                  theme={{
                    monthTextColor: Color.theme,
                    dayTextColor: Color.theme,
                    arrowColor: Color.theme,
                    textSectionTitleColor: Color.theme,
                    selectedDayBackgroundColor: Color.primary,
                    selectedDayTextColor: Color.primary,
                    todayTextColor: Color.theme
                  }}
                />
              </View>
            </View>

            <BannerSlider {...this.props} />
          </ScrollView>

          <Modal
              onRequestClose={() => {}}
              animationType="fade"
              transparent
              visible={this.state.messageInfo !== ''}
            >
              <ModalInformation
                label={this.state.messageInfo}
                error
              />
            </Modal>
        </SafeAreaView>
    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}

// STYLES
// ------------------------------------
const styles = StyleSheet.create(CalendarStyle);
