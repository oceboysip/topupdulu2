import React, { Component } from 'react'
import { Image, View, ScrollView, Dimensions, TouchableOpacity as NativeTouchable, SafeAreaView } from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import Styled from 'styled-components';
import Icon from 'react-native-vector-icons/MaterialIcons';

import Text from '../Text';
import Color from '../Color';
import TouchableOpacity from '../Button/TouchableDebounce';
import { Row, Col } from '../Grid';
import FormatMoney from '../FormatMoney';

const ContainerView = Styled(View)`
  flex: 1;
`;

const HeaderView = Styled(View)`
  width: 100%;
  height: 60;
  backgroundColor: ${Color.theme};
`;

const RowView = Styled(Row)`
  height: 100%;
`;

const ColumnView = Styled(Col)`
  justifyContent: center;
  alignItems: center;
  height: 100%;
`;

const BackView = Styled(TouchableOpacity)`
  flex: 1;
  width: 100%;
  alignItems: center;
  justifyContent: center;
`;

const BackIcon = Styled(Icon)`
  fontSize: 23;
  color: #000000;
`;

const TitleText = Styled(Text)`
`;

const HeaderTabsText = Styled(Text)`
  fontSize: 12;
`;

const Marker = Styled(Image)`
  width: 25;
  aspectRatio: 1;
`;

const SmallImage = Styled(Image)`
  width: 14;
  aspectRatio: 1;
  marginRight: 2;
`;

const OneFilterView = Styled(NativeTouchable)`
  width: 100%;
  height: 50;
  flexDirection: row;
  paddingHorizontal: 16;
  alignItems: center;
  justifyContent: space-between;
  borderColor: #DDDDDD;
  borderBottomWidth: 0.5;
`;

const FilterLabelView = Styled(View)`
  flex: 1;
  minHeight: 1;
  paddingRight: 16;
`;

const TitleFilter = Styled(View)`
  width: 100%;
  height: 50;
  alignItems: flex-start;
  justifyContent: center;
  paddingHorizontal: 16;
`;

const ImageProperty = Styled(Image)`
  width: 100%;
  height: 100%;
`;

const Checkbox = Styled(View)`
  width: 14;
  aspectRatio: 1;
`;

const TitleFilterView = Styled(View)`
  backgroundColor: #F4F4F4;
  paddingHorizontal: 10;
  paddingVertical: 20
`;

const MinPriceView = Styled(View)`
  paddingBottom: 8;
  borderColor: #DDDDDD;
  borderBottomWidth: 0.5;
  alignItems: flex-start;
`;

const MaxPriceView = Styled(MinPriceView)`
  alignItems: flex-end;
`;

const AbsoluteButton = Styled(View)`
  position: absolute;
  bottom: 12;
  alignItems: center;
  width: 100%;
  height: 45;
`;

const TouchableButton = Styled(TouchableOpacity)`
  backgroundColor: ${Color.button};
  height: 100%;
  width: 50%;
  justifyContent: center;
  borderRadius: 45
`;

const { width } = Dimensions.get('window');
const filterStar = [1, 2, 3, 4, 5];
const facilities = [
  { name: 'Wifi', code: 'wifi' },
  { name: 'Kolam Renang', code: 'pool' },
  { name: 'Gym', code: 'gym' },
  { name: 'Restoran', code: 'resto' }
];

const bulletBlack = require('../../images/bullet-black.png');
const starIcon = require('../../images/star@4x-8.png');
const checked = require('../../images/checkbox-selected.png');
const checkoff = require('../../images/checkbox-unselected.png');

class CustomMarker extends Component {
  render() {
    return (
      <Marker source={bulletBlack} resizeMode='contain' />
    );
  }
}

class HotelFilter extends Component {
  constructor(props) {
    const { star, facilities, price, minPrice, maxPrice } = props.data;
    super(props);
    this.state = {
      star,
      facilities,
      minPrice,
      maxPrice,
      minPriceTextInput: price[0] < minPrice || price[0] >= price[1] ? minPrice : price[0],
      maxPriceTextInput: price[1] > maxPrice || price[1] <= price[0] ? maxPrice : price[1]
    };
  }

  renderHeader() {
    return (
      <HeaderView>
        <RowView>
          <ColumnView size={2}>
            <BackView onPress={() => this.props.onClose()}>
              <BackIcon name='clear' />
            </BackView>
          </ColumnView>

          <ColumnView size={8}>
            <TitleText type='bold'>Filter</TitleText>
          </ColumnView>

          <ColumnView size={2}>
            <BackView onPress={this.reset}>
              <TitleText type='medium'>Reset</TitleText>
            </BackView>
          </ColumnView>
        </RowView>
      </HeaderView>
    );
  }

  reset = () => {
    const { minPrice, maxPrice } = this.props.data;
    this.setState({ minPriceTextInput: minPrice, maxPriceTextInput: maxPrice, star: [], facilities: [] });
  }

  multiSliderValuesChange = (priceFilter) => {
    const { minPrice, maxPrice } = this.state;
    let minPriceTextInput = priceFilter[0] > minPrice ? priceFilter[0] : minPrice;
    let maxPriceTextInput = priceFilter[1] < maxPrice ? priceFilter[1] : maxPrice;
    minPriceTextInput = minPriceTextInput < maxPrice ? minPriceTextInput : maxPrice;
    maxPriceTextInput = maxPriceTextInput > minPrice ? maxPriceTextInput : minPrice;
    this.setState({ minPriceTextInput, maxPriceTextInput });
  }

  onAplyFilter(props) {
    const { star, facilities, minPriceTextInput, maxPriceTextInput } = this.state;
    const filter = { star, facilities, minPriceTextInput, maxPriceTextInput };

    let load = true;
    if (JSON.stringify(star) == JSON.stringify(props.star) && JSON.stringify(facilities) == JSON.stringify(props.facilities) && minPriceTextInput == props.price[0] && maxPriceTextInput == props.price[1]) load = false;
    // if (!load) Toast.info('Tidak ada perubahan filter', 3, null, false);

    this.props.onSelectedFilter(filter, load);
  }

  renderPricing() {
    const { minPriceTextInput, maxPriceTextInput, maxPrice } = this.state;

    return (
      <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 10, paddingVertical: 10}}>
        <MinPriceView>
          <Text size={12} type='semibold' style={{lineHeight: 24}}>Harga minimum</Text>
          <Text size={12} type='medium'>{FormatMoney.getFormattedMoney(minPriceTextInput)}</Text>
        </MinPriceView>
        <MaxPriceView>
          <Text size={12} type='semibold' style={{lineHeight: 24}}>Harga maksimum</Text>
          <Text size={12} type='medium'>{FormatMoney.getFormattedMoney(maxPriceTextInput)}{maxPriceTextInput == maxPrice && '+'}</Text>
        </MaxPriceView>
      </View>
    )
  }

  renderPriceSlider() {
    const { minPrice, maxPrice, minPriceTextInput, maxPriceTextInput } = this.state;

    return (
      <View style={{width, paddingHorizontal: 30}}>
        {minPrice && maxPrice && <MultiSlider
          values={[minPriceTextInput, maxPriceTextInput]}
          sliderLength={width - 60}
          onValuesChange={this.multiSliderValuesChange}
          min={minPrice}
          max={maxPrice}
          allowOverlap
          step={100000}
          selectedStyle={{backgroundColor: '#345E6D'}}
          unselectedStyle={{backgroundColor: '#DDDDDD'}}
          customMarker={CustomMarker}
        />}
      </View>
    );
  }

  renderHotelStar(total) {
    const array = [];
    for (let i = 0; i < total; i++) array.push('');
    return array.map((item, i) =>
      <SmallImage source={starIcon} key={i} />
    );
  }

  onSelectedFilter(value, check, type) {
    let tempArray = [].concat(this.state[type]);
    if (check) tempArray.push(value);
    else {
      const index = tempArray.indexOf(value);
      if (index !== -1) tempArray.splice(index, 1);
    }

    this.setState({ [type]: tempArray });
  }

  renderFacilities() {
    let f = this.state.facilities;
    return facilities.map((value, i) =>
      <OneFilterView key={i} activeOpacity={0.5} onPress={() => this.onSelectedFilter(value.code, f.indexOf(value.code) === -1, 'facilities')}>
        <FilterLabelView>
          <Text size={12} align='left' type='medium'>{value.name}</Text>
        </FilterLabelView>
        <Checkbox>
          <ImageProperty source={f.indexOf(value.code) !== -1 ? checked : checkoff} />
        </Checkbox>
      </OneFilterView>
    )
  }

  renderStar() {
    let s = this.state.star;
    return filterStar.map((value, i) =>
      <OneFilterView key={i} activeOpacity={0.5} onPress={(e) => this.onSelectedFilter(value, s.indexOf(value) === -1, 'star')}>
        <FilterLabelView style={{flexDirection: 'row'}}>
          {this.renderHotelStar(value)}
        </FilterLabelView>
        <Checkbox>
          <ImageProperty source={s.indexOf(value) !== -1 ? checked : checkoff} />
        </Checkbox>
      </OneFilterView>
    )
  }

  render() {
    return (
      <SafeAreaView style={{backgroundColor: Color.theme, flex: 1}}>
        <View style={{backgroundColor: '#FFFFFF'}}>
          {this.renderHeader()}
          <ScrollView>
            <TitleFilterView>
              <Text align='left' type='bold'>Harga</Text>
            </TitleFilterView>
            {this.renderPricing()}
            {this.renderPriceSlider()}

            <TitleFilterView>
              <Text align='left' type='bold'>Fasilitas</Text>
            </TitleFilterView>
            {this.renderFacilities()}

            <TitleFilterView>
              <Text align='left' type='bold'>Bintang</Text>
            </TitleFilterView>
            {this.renderStar()}
            <View style={{marginBottom: 72}} />
          </ScrollView>
          <AbsoluteButton>
            <TouchableButton activeOpacity={0.5} onPress={() => this.onAplyFilter(this.props.data)}>
              <Text type='medium' style={{color:'#FFFFFF'}}>Terapkan</Text>
            </TouchableButton>
          </AbsoluteButton>
        </View>
      </SafeAreaView>
    );
  }
}

export default HotelFilter;
