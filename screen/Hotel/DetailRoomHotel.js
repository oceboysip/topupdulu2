import React, { Component } from 'react';
import { View, Image, ScrollView, ActivityIndicator, Platform, Animated, Dimensions } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';
import Carousel from 'react-native-banner-carousel';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';

import { Row, Col } from '../Grid';
import Text from '../Text';
import Color from '../Color';
import Button from '../Button';
import TouchableOpacity from '../Button/TouchableDebounce';
import FormatMoney from '../FormatMoney';
import Header from '../Header';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    backgroundColor: #FFFFFF;
`;

const UpperMainView = Styled(View)`
    flex: 1;
    width: 100%;
`;

const BottomMainView = Styled(View)`
    width: 100%;
    height: 78;
    alignItems: center;
    justifyContent: space-between;
    flexDirection: row;
    padding: 13px 30px 13px 30px;
    backgroundColor: #FFFFFF;
    borderWidth: 0;
    elevation: 5;
    bottom: 0;
`;

const HalfBottomMainView = Styled(View)`
    minWidth: 1;
    height: 100%;
`;

const SubMainView = Styled(View)`
    width: 100%;
    height: 100%;
    paddingTop: 150;
    flexDirection: column;
`;

const RowView = Styled(Row)`
    height: 100%;
`;

const ColumnView = Styled(Col)`
    justifyContent: center;
    alignItems: center;
`;

const SideButton = Styled(TouchableOpacity)`
    flex: 1;
    width: 100%;
    alignItems: center;
    justifyContent: center;
`;

const SideIcon = Styled(Icon)`
    fontSize: 23;
    color: ${Color.text};
`;

const CustomHeader = Styled(Header)`
    height: 60;
`;

const AbsoluteCustomHeader = Styled(Animated.View)`
    width: 100%;
    height: 60;
    alignItems: center;
    flexDirection: row;
    zIndex: 1;
    position: absolute;
`;

const BackgroundImageView = Styled(View)`
    width: 100%;
    height: 260;
    position: absolute;
`;

const MainInfoContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: transparent;
    paddingHorizontal: 16;
    marginBottom: 16;

`;

const MainInfoView = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: #FFFFFF;
    padding: 12px 12px 16px 12px;
    borderRadius: 3;
    elevation: 5;
    flexDirection: column;
    borderWidth: ${Platform.OS === 'ios' ? 1 : 0 };
    borderBottomWidth: ${Platform.OS === 'ios' ? 2 : 0 };
    borderColor: #DDDDDD;
`;

const TitleInfoView = Styled(View)`
    width: 100%;
    minHeight: 1;
    marginBottom: 10;
`;

const DateInfoView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    justifyContent: flex-start;
`;

const LeftDateInfoView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    padding: 2px 8px 2px 8px;
    borderRadius: 50;
    backgroundColor: ${Color.theme};
    marginRight: 3;
`;

const RightDateInfoView = Styled(TouchableOpacity)`
    minWidth: 1;
    minHeight: 1;
    padding: 4px 8px 4px 8px;
    borderRadius: 50;
    flexDirection: row;
    alignItems: flex-end;
    justifyContent: flex-end;
`;

const CalendarView = Styled(View)`
    width: 9;
    height: 8;
    marginRight: 4;
`;

const LoadingActivityView = Styled(View)`
    width: 100%;
    flex: 1;
    justifyContent: center;
    alignItems: center;
    backgroundColor: #FFFFFF;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const TitleText = Styled(Text)`
    textAlign: left;
`;

const DescriptionText = Styled(Text)`
    textAlign: left;
    fontSize: 12;
    color: #231F20;
`;

const AnimatedTitleText = Styled(Animated.Text)`
    fontSize: 14;
    textAlign: center;
    textShadowRadius: 0;
`;

const DateText = Styled(Text)`
    fontSize: 10;
`;

const ChangeDateText = Styled(Text)`
    fontSize: 12;
    color: #FFFFFF;
    textAlign: left;
`;

const BottomText = Styled(Text)`
    textAlign: left;
`;

const PriceText = Styled(Text)`
    textAlign: left;
    color: #FF425E;
    textAlign: left;
`;

const HeaderTabsText = Styled(Text)`
    fontSize: 12;
`;

const BookingButton = Styled(Button)`
    width: 100%;
    height: 50;
`;

const guestIcon = require('../../images/users-group.png');
const noPict = require('../../images/no-pict.png');

const { width } = Dimensions.get('window');

class DetailRoomHotel extends Component {
    static navigationOptions = { header: null };
    animatedValue;

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
        };
    }

    componentDidMount() {
        this.setAnimatedHeader();
    }

    setAnimatedHeader() {
        this.animatedValue = new Animated.Value(0);
        this.props.navigation.setParams({
            animatedValue: this.animatedValue.interpolate({
                inputRange: [0, 50],
                outputRange: ['transparent', Color.theme],
                extrapolate: 'clamp'
            }),
            animatedText: this.animatedValue.interpolate({
                inputRange: [0, 50],
                outputRange: ['transparent', Color.text],
                extrapolate: 'clamp'
            })
        });
    }

    onBeforeReviewBooking(roomName, rate) {
      const { hotelName, HotelFacilitiesHighlight, checkInDate, checkOutDate,currency, optionBedGuest } = this.props.navigation.state.params;
      const night = Moment(checkOutDate).diff(Moment(checkInDate), 'days');
      const { user, navigation } = this.props;

      if (user.guest) {
        navigation.navigate('LoginScreen', {
          loginFrom: 'bus', afterLogin: () => {
            navigation.navigate('HotelReviewBooking', {
              roomName,
              boardName: rate.boardName,
              checkOutDate,
              checkInDate,
              night,
              guest: optionBedGuest.guest,
              hotelName,
              rateEnc: rate.rateEnc,
              ratePrices: rate.net,
              cancellationPolicies: rate.cancellationPolicies.amount
            })
          }
        });
      }else {
        navigation.navigate('HotelReviewBooking', {
          roomName,
          boardName: rate.boardName,
          checkOutDate,
          checkInDate,
          night,
          guest: optionBedGuest.guest,
          hotelName,
          rateEnc: rate.rateEnc,
          ratePrices: rate.net,
          cancellationPolicies: rate.cancellationPolicies.amount
        })
      }
    }

    renderLoadingIndicator() {
      return (
        <LoadingActivityView>
          <ActivityIndicator size='large' color={Color.loading} />
        </LoadingActivityView>
      );
    }

    renderAbsoluteCustomHeader(animatedValue, animatedText) {
        return (
          <AbsoluteCustomHeader style={{ backgroundColor: animatedValue || 'transparent' }}>
            <RowView>
              <ColumnView size={2}>
                <SideButton onPress={() => this.props.navigation.pop()}>
                  <SideIcon name='arrow-back' />
                </SideButton>
              </ColumnView>

              <ColumnView size={7.8}>
                <AnimatedTitleText numberOfLines={2} type='bold' style={animatedText && { color: animatedText }}>Nama Hotel</AnimatedTitleText>
              </ColumnView>

              <ColumnView size={2.2}><View /></ColumnView>
            </RowView>
          </AbsoluteCustomHeader>
        );
    }

    renderBackgroundImage() {
        const { rate, hotelConfig } = this.props.navigation.state.params;

        return (
          <BackgroundImageView>
            {rate.images.length > 0 ? <Carousel
              autoplay
              autoplayTimeout={5000}
              loop
              index={0}
              pageSize={width}
            >
              {rate.images.map((image, i) => <ImageProperty key={i} source={{uri: hotelConfig[0].value+image.path}} resizeMode='stretch' />)}
            </Carousel>
            :
            <ImageProperty source={noPict} resizeMode='stretch' />}
          </BackgroundImageView>
        );
      }

      renderThreeMainFacilities(hotelFacilitiesHighlight) {
        return hotelFacilitiesHighlight.map((facility, i) =>
          <View key={i}>
            {/* <FacilitiesImage source={{ uri: facility.image }} /> */}
            <LeftDateInfoView>
              <DateText type='medium'>{facility.active ? facility.name : '-'}</DateText>
            </LeftDateInfoView>
          </View>
        );
      }

    render() {
        const { hotelName, checkInDate, checkOutDate, roomName, boardName, night, HotelFacilitiesHighlight, price, hotelConfig, rate } = this.props.navigation.state.params;
        const { cancellationPolicies } = rate;
        let roomFacilities = [];//from rroms;roomFacilities;facilityName
        console.log(this.props.navigation.state.params, 'params');

        return (
          <MainView>
            {/* {this.renderAbsoluteCustomHeader(animatedValue, animatedText)} */}
            <UpperMainView>
              <ScrollView>
                {/* <ScrollView contentContainerStyle={{ width: '100%', minHeight: '100%' }} onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: this.animatedValue } } }])}> */}
                {this.renderBackgroundImage()}
                <CustomHeader transparentMode showLeftButton showIconLeftButton={true} />
                {/* {this.state.loading && this.renderLoadingIndicator()} */}
                <SubMainView>
                  <MainInfoContainer>
                    <MainInfoView>
                      <TitleInfoView>
                        <TitleText type='bold'>{roomName} - {boardName}</TitleText>
                        <DateInfoView style={{marginTop: 5}}>
                          <CalendarView><ImageProperty resizeMode='stretch' source={guestIcon} /></CalendarView>
                          <TitleText size={10}>{`maks ${rate.adults} orang`}</TitleText>
                        </DateInfoView>
                      </TitleInfoView>
                      <DateInfoView style={{ paddingBottom: 5 }}>
                        {this.renderThreeMainFacilities(HotelFacilitiesHighlight)}
                      </DateInfoView>
                      <TitleText size={12} type='medium'>Harga untuk {night} malam</TitleText>
                      <DateInfoView>
                        <View style={{width: '50%'}}>
                          <TitleText size={12} type='medium'>({Moment(checkInDate).format('DD MMM')} - {Moment(checkOutDate).format('DD MMM')})</TitleText>
                        </View>
                        <View style={{width: '50%', alignItems: 'flex-end'}}>
                          <PriceText type='bold'>{FormatMoney.getFormattedMoney(price)}</PriceText>
                        </View>
                      </DateInfoView>
                    </MainInfoView>
                    {/* <TitleText type='bold' style={{ paddingTop: 35 }}>RINGKASAN KAMAR</TitleText>
                  <DescriptionText alignItems='flex-start' type='medium' >A spacious private villa completed with pool, Jacuzzi, spa, gym, rooftop deck, outdoor pavilion surrounded by the pool and lush lavish garden perfected for your inspired holiday and wedding celebration.</DescriptionText>*/}
                    {/*<TitleText type='bold' size={12} style={{paddingTop: 35, letterSpacing: 1.2, marginBottom: 13}}>FASILITAS KAMAR</TitleText>
                    {roomFacilities.map((item, idx) =>
                      <TitleText type='medium' size={12}>• {item.facilityName}</TitleText>
                    )}*/}

                    <TitleText type='bold' size={12} style={{paddingTop: 35, letterSpacing: 1.2}}>KEBIJAKAN PEMBATALAN</TitleText>
                      {cancellationPolicies.length > 0 ? <DescriptionText style={{marginTop: 13, lineHeight: 18}} type='medium'>
                        Pembatalan akan dikenai biaya penuh sebesar {FormatMoney.getFormattedMoney(cancellationPolicies[0].amount)}. Waktu menginap dan tipe kamar tidak dapat diubah. Waktu yang tertera berdasarkan waktu lokal hotel. Jika tidak datang, kartu Anda akan dikenai biaya sebesar {FormatMoney.getFormattedMoney(cancellationPolicies[0].amount)}.
                        Maksimal pembatalan tanggal {Moment(cancellationPolicies[0].from).format('DD-MM-YYYY')}.
                      </DescriptionText>
                      :
                      <DescriptionText>-</DescriptionText>}
                  </MainInfoContainer>
                </SubMainView>
              </ScrollView>
            </UpperMainView>
            <BottomMainView>
              <BookingButton onPress={() => this.onBeforeReviewBooking(roomName, rate)}>Pesan Kamar</BookingButton>
            </BottomMainView>
          </MainView>
        )
    }
}

const mapStateToProps = (state) => {
  return {
    user: state['user.auth'].login.user
  }
}

const mapDispatchToProps = null;

export default connect(mapStateToProps, mapDispatchToProps)(DetailRoomHotel);
