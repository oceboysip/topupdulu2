import React, { Component } from 'react';
import { View, Image } from 'react-native';
import Styled from 'styled-components';

import Text from '../Text';
import TouchableOpacity from '../Button/TouchableDebounce';

const MainView = Styled(View)`
    width: 100%;
    height: 100%
    borderTopLeftRadius: 5;
    borderTopRightRadius: 5;
    backgroundColor: #FFFFFF;
    justifyContent: flex-end;
`;

const OptionTouch = Styled(TouchableOpacity)`
    width: 100%;
    height: 20%;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    borderTopWidth: 1;
    borderColor: #DDDDDD;
    paddingHorizontal: 16;
`;

const DragContainer = Styled(View)`
    width: 100%;
    justifyContent: flex-start;
    alignItems: center;
`;

const DragView = Styled(View)`
    width: 40;
    height: 6;
    borderRadius: 3;
    backgroundColor: #DDDDDD;
`;

const TitleView = Styled(View)`
    width: 100%;
    height: 18%;
    justifyContent: center;
    alignItems: center;
`;

const CheckIcon = Styled(View)`
    width: 20;
    height: 15;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const TitleText = Styled(Text)`
    letterSpacing: 1;
`;

const NormalText = Styled(Text)`
`;

const check = require('../../images/check.png');

export default class HotelSort extends Component {

  onSelected(selected) {
    this.props.onSelectedSort(selected);
  }

  renderSortOptions() {
    const { sortOptions, selected } = this.props;
    return sortOptions.map((option, i) => (
      <OptionTouch key={i} onPress={() => this.onSelected(option)}>
        <NormalText size={12} type={!(option.value === selected[option.sort]) ? 'medium' : 'bold'}>{option.label}</NormalText>
        {option.value === selected[option.sort] && <CheckIcon><ImageProperty resizeMode='stretch' source={check} /></CheckIcon>}
      </OptionTouch>
    ));
  }

  render() {
    console.log(this.props);
    return (
      <MainView>
        <View style={{height: '98%'}}>
          <DragContainer>
            <DragView />
          </DragContainer>
          <TitleView>
            <TitleText type='bold'>URUTKAN BERDASARKAN</TitleText>
          </TitleView>
          {this.renderSortOptions()}
        </View>
      </MainView>
    );
  }
}
