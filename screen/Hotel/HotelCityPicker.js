import React, { Component } from 'react';
import { View, FlatList, TextInput, Platform, ScrollView, ActivityIndicator, Dimensions, SafeAreaView, PermissionsAndroid as PA } from 'react-native';
import Styled from 'styled-components';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import gql from 'graphql-tag';
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';

import Client from '../../state/apollo';
import Text from '../Text';
import Color from '../Color';
import TouchableOpacity from '../Button/TouchableDebounce';
import Header from '../Header';

const { width, height } = Dimensions.get('window');

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    alignItems: center;
    flexDirection: column;
    backgroundColor: #FAF9F9;
`;

const ChildMainView = Styled(View)`
    width: 100%;
    height: 100%;
    alignItems: stretch;
    flexDirection: column;
`;

const BackgroundSearchBar = Styled(View)`
    width: 100%;
    alignItems: center;
    justifyContent: flex-start;
    backgroundColor: ${Color.theme};
    paddingHorizontal: 16;
`;

const SearchBar = Styled(View)`
    width: 100%;
    height: 45;
    flexDirection: row;
    backgroundColor: #FFFFFF;
    borderRadius: 22.5;
    alignItems: center;
`;

const HeaderResultContainer = Styled(View)`
    paddingVertical: 12;
    paddingHorizontal: 16;
    backgroundColor: ${Color.theme};
    alignItems: flex-start;
    maxHeight: 40;
    marginTop: 8;
    flexDirection: row;
    justifyContent: space-between;
`;

const CustomTextInput = Styled(TextInput)`
    width: 100%;
    height: 100%;
    color: ${Color.theme};
    fontSize: 16;
    letterSpacing: 0.5
`;

const CustomFlatList = Styled(FlatList)`
    width: 100%;
    paddingHorizontal: 16;

`;

const RowView = Styled(TouchableOpacity)`
    width: 100%;
    minHeight: 1;
    paddingVertical: 10;
    paddingHorizontal: 16;
    alignItems: flex-start;
    marginTop: 10;
    borderRadius: 10;
    backgroundColor: #FFFFFF;
`;

const SearchIconContainer = Styled(View)`
    width: 32;
    height: 32;
    borderRadius: 16;
    alignItems: center;
    justifyContent: center;
    marginHorizontal: 8;
    backgroundColor: ${Color.primary};
`;

const RowText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
    color: ${Color.theme};
`;

const SearchIcon = Styled(EvilIcons)`
    color: #FFFFFF;
    fontSize: 32;
`;

const ScrollViewOfferPromo = Styled(View)`
  minWidth: 100%;
  height: ${width / 2 * 0.53};
  marginTop: 5;
  marginBottom: 10;
  flexDirection: row;
  justifyContent: center;
  alignItems: center;
`;

export default class HotelCityPicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hotels: [],
      cities: [],
      searchText: '',
      idRequest: 0,
      noResult: false,
      searching: false,
      hasPermission: false
    };
  }

  componentDidMount() {
    // this.findCoordinates();
  }

  findCoordinates() {
    console.log('woi');
    
    Geolocation.getCurrentPosition(res => {
      console.log(res, 'res');
      
      // if (res.coords) {
      //   Geocoder.init("AIzaSyAvSstWIaNpLZLceUmUwqao53Ko77BC0pg", { language : "id" });
      //   Geocoder.from(res.coords.latitude, res.coords.longitude)
      //   .then(json => {
      //     console.log(json, 'json');
      //   })
      //   .catch(err => {
      //     console.log(err, 'error');
      //   })
      // } else {
      //   console.log('err');
      // }
    })
    // Geolocation.getCurrentPosition(
    //   position => {
    //     const location = JSON.stringify(position);
    //     console.log(position, 'position');
        
    //     this.setState({ location });
    //   },
    //   error => console.log(error, 'error'),
    //   { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    // );
  }

  async requestLocationPermission() {
    try {
      const granted = await PA.request(PA.PERMISSIONS.ACCESS_FINE_LOCATION)

      if (Platform.Version >= 23) {
        if (granted === PA.RESULTS.GRANTED) {
          this.setState({ hasPermission: true });
        }else {
          this.setState({ hasPermission: false });
        }
      }else {
        this.setState({ hasPermission: true });
      }

      if (this.state.hasPermission) {
        this.findCoordinates();
      }
    }catch (err) {
      console.warn(err, 'error');
    }
  }

  searchFilter(searchText) {
    const tempIdRequest = this.state.idRequest;
    if (searchText.length >= 3) {
      this.searchHotelDestination(tempIdRequest + 1, searchText);
      this.setState({ idRequest: tempIdRequest + 1, searchText, searching: true });
    }
    else this.setState({ searchText, cities: [], hotels: [], searching: false });
}

  onClose = () => {
    this.props.onClose();
  }

  onSelectedItem = (type, item) => {
    this.props.onSelected(type, item);
    this.props.onClose();
  }

  searchHotelDestination = (idRequest, keyword) => {
    const variables = {
      param: { query: keyword }
    };
    const searchQuery = gql `
      query(
        $param: paramHotelDestination
      ) {
        hotelDestination(
          param: $param
        ) {
          cities { code name country city }
          hotels { code name country city }
        }
      }
    `;
    Client
      .query({
        query: searchQuery,
        variables
      })
      .then(res => {
        if(res.data.hotelDestination) {
          if (this.state.searchText.length >= 3 && idRequest === this.state.idRequest)
            //console.log(res.data.hotelDestination.cities)
              this.setState({ cities: res.data.hotelDestination.cities, hotels: res.data.hotelDestination.hotels, searching: false});
        }
      }).catch(reject => {
          //console.log(reject)
          let errorMessage = 'Terjadi kesalahan server / tidak ada koneksi internet';
          this.setState({ cities: [], hotels: [], searching: false });
          alert(errorMessage);
      });
  }

  getHighlightedKeyword(name) {
    if (!name) return;
    const regString = '(' + this.state.searchText + ')';
    const reg = new RegExp(regString, 'i');
    const result = name.split(reg);

    return result.map((font, i) => {
      if (font.toLowerCase() === this.state.searchText.toLowerCase())
        return <Text size={14} key={i} style={{color: Color.primary}}>{font}</Text>;
        return <Text size={14} key={i} style={{color: Color.theme}}>{font}</Text>
    });
  }

  onChangeText(searchText) {
    this.setState({ searchText });
    // this.getHighlightedKeyword(searchText);
  }

  renderLoadingIndicator() {
    return (
      <ScrollViewOfferPromo>
          <ActivityIndicator size="large" color={Color.text} />
      </ScrollViewOfferPromo>
    );
  }

  renderHeader = (name) => {
    const { isHistory, removeHistory } = this.props;

    if (name === 'Pencarian Hotel Terakhir') return <View>
      <HeaderResultContainer>
         <Text size={16} type="bold" style={{color: '#FFFFFF'}}>{name}</Text>
         {isHistory && <Text size={10} style={{color: '#FFFFFF'}} onPress={() => removeHistory()}>Hapus Riwayat</Text>}
      </HeaderResultContainer>
      {!isHistory && <View style={{alignItems: 'center', paddingVertical: 16}}>
        <Text>Anda belum memiliki riwayat pencarian</Text>
      </View>}
    </View>

    return <HeaderResultContainer>
         <Text size={16} type="bold" style={{color: '#FFFFFF'}}>{name}</Text>
      </HeaderResultContainer>
  }

  renderCities(rowData) {
    return (
      <RowView onPress={() => this.onSelectedItem('city', rowData.item)}>
          <RowText>{this.getHighlightedKeyword(rowData.item.city)}</RowText>
          <RowText>{rowData.item.country}</RowText>
      </RowView>
    )
  }

  renderHotels(rowData) {
    return (
      <RowView onPress={() => this.onSelectedItem('hotel', rowData.item)}>
        <RowText>{this.getHighlightedKeyword(rowData.item.name)}</RowText>
        <RowText>{rowData.item.city}</RowText>
      </RowView>
    )
  }

  render() {
    const { onClose, lastSearch, favoriteSearch, isHistory } = this.props;
    const { cities, hotels, searchText } = this.state;

    const validSearch = (searchText.length >= 3);

    console.log(this.state, 'state city picker', Geocoder);
    

    return (
      <SafeAreaView style={{backgroundColor: Color.theme, flex: 1}}>
        <MainView>
          <Header onPressLeftButton={onClose}>
            <BackgroundSearchBar>
              <SearchBar>
                <SearchIconContainer><SearchIcon name='search' /></SearchIconContainer>
                <CustomTextInput
                  ref={(input) => this.searchBarHotel = input}
                  placeholder='Cari Hotel atau Kota'
                  placeholderTextColor='#DDDDDD'
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  autoFocus
                  maxLength={25}
                  onChangeText={(text) => this.searchFilter(text)}
                  selectionColor={Color.text}
                />
              </SearchBar>
            </BackgroundSearchBar>
          </Header>

          <ChildMainView>
            <ScrollView keyboardShouldPersistTaps='always'>
              {this.state.searching && this.renderLoadingIndicator()}
              {/*<Text onPress={() => Platform.OS === 'android' ? this.requestLocationPermission() : this.findCoordinates()}>Gunakan Lokasi Saat ini</Text>*/}

              {!this.state.searching && <CustomFlatList
                keyboardShouldPersistTaps='always'
                ListHeaderComponent={this.renderHeader(validSearch ? 'Kota' : 'Pencarian Hotel Terakhir')}
                data={validSearch ? cities : lastSearch}
                keyExtractor={(item, index) => `${item.id}.${index}`}
                renderItem={(rowData) => validSearch ? this.renderCities(rowData) : this.renderHotels(rowData)}
                enableEmptySections={true}
              />}

              {!this.state.searching && <CustomFlatList
                keyboardShouldPersistTaps='always'
                ListHeaderComponent={this.renderHeader(validSearch ? 'Hotel' : 'Kota Favorit')}
                data={validSearch ? hotels : favoriteSearch}
                keyExtractor={(item, index) => `${item.id}.${index}`}
                renderItem={(rowData) => validSearch ? this.renderHotels(rowData) : this.renderCities(rowData)}
                enableEmptySections={true}
              />}
            </ScrollView>
          </ChildMainView>
        </MainView>
      </SafeAreaView>
    );
  }
}