import React, { Component } from 'react';
import { View, Image, FlatList, Modal, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import Styled from 'styled-components';
import Moment from 'moment';
import gql from 'graphql-tag';
import ModalIndicator from '../Modal/ModalIndicator';
import ModalBox from 'react-native-modalbox';

import Text from '../Text';
import Color from '../Color';
import Button from '../Button/Button';
import TouchableOpacity from '../Button/TouchableDebounce';
import Header from '../Header';
import FormatMoney from '../FormatMoney';
import Client from '../../state/apollo';
import HotelFilter from './HotelFilter';
import HotelSort from './HotelSort';

import { updateInputUser } from '../../state/actions/hotel/update-input-user';
import { clearAll } from '../../state/actions/hotel/clear-all';
import { updateHotelCode } from '../../state/actions/hotel/update-hotel-code';
import { updateHotels } from '../../state/actions/hotel/update-hotel';
import { clearHotels } from '../../state/actions/hotel/clear-hotel';

const MainView = Styled(View)`
  width: 100%;
  height: 100%;
  backgroundColor: #FFFFFF;
`;

const PureView = Styled(View)`
  paddingHorizontal: 8;
`;

const ContentView = Styled(TouchableOpacity)`
  width: 100%;
  minHeight: 1;
  flexDirection: row;
  backgroundColor: #FFFFFF;
  elevation: 1;
  marginBottom: 12;
  borderRadius: 3;
  borderWidth: 0.5;
  borderColor: #0000001A;
`;

const MainImageView = Styled(View)`
  width: 120;
  height: 100%;
`;

const MainImageLimiter = Styled(View)`
  flex: 1;
`;

const MainAllInfo = Styled(View)`
  flex: 1;
  minHeight: 1;
  flexDirection: column;
  paddingLeft: 10;
  paddingRight: 8;
  paddingVertical: 14;
`;

const TopInfo = Styled(View)`
  width: 100%;
  minHeight: 1;
  flexDirection: column;
`;

const BottomInfo = Styled(TopInfo)`
  flexDirection: row;
`;

const BottomInfoLeft = Styled(TopInfo)`
  width: 60%;
`;

const BottomInfoRight = Styled(TopInfo)`
  width: 40%;
  alignItems: flex-end;
  justifyContent: center;
`;

const DottedView = Styled(View)`
  width: 100%;
  height: 1;
  marginVertical: 14;
`;

const DottedViewLimiter = Styled(View)`
  flex: 1;
`;

const ImageProperty = Styled(Image)`
  width: 100%;
  height: 100%;
`;

const TitleText = Styled(Text)`
  fontSize: 12;
  textAlign: left;
`;

const SmallInfoText = Styled(Text)`
  color: #231F20;
  fontSize: 10;
  textAlign: left;
`;

const PriceText = Styled(Text)`
  color: #FF425E;
  textAlign: left;
`;

const ConfigureContainer = Styled(View)`
  width: 100%;
  minHeight: 1;
  alignItems: center;
  position: absolute;
  bottom: 12;
`;

const ConfigureView = Styled(View)`
  minWidth: 1;
  height: 40;
  flexDirection: row;
  backgroundColor: ${Color.theme};
  borderRadius: 100;
`;

const LeftConfigure = Styled(TouchableOpacity)`
  width: 79;
  height: 100%;
  justifyContent: center;
  alignItems: center;
  flexDirection: column;
  borderColor: #FFFFFF;
  borderRightWidth: 0.25;
`;

const RightConfigure = Styled(TouchableOpacity)`
  width: 79;
  height: 100%;
  justifyContent: center;
  alignItems: center;
  flexDirection: column;
  borderColor: #FFFFFF;
  borderLeftWidth: 0.25;
`;

const FilterIcon = Styled(View)`
  width: 11.11;
  height: 10;
`;

const SortIcon = Styled(View)`
  width: 15;
  height: 10;
`;

const ConfigLabel = Styled(Text)`
  fontSize: 12;
  color: #FFFFFF;
`;

const TopView = Styled(View)`
  backgroundColor: #FFFFFF;
  width: 100%;
  height: 60;
  flexDirection: row;
  alignItems: center;
`;

const SelectButton = Styled(Button)`
  maxWidth: 76;
  width: 100%;
  height: 24;
  backgroundColor: ${Color.theme};
`;

const TopLeftView = Styled(View)`
  width: 74%;
  flexDirection: row;
  justifyContent: flex-start;
  alignItems: center;
`;

const TopCenterView = Styled(View)`
  width: 2%;
`;

const TopRightView = Styled(View)`
  width: 24%;
  alignItems: flex-end;
  justifyContent: center;
`;

const GuestInfoView = Styled(View)`
  backgroundColor: ${Color.theme};
  paddingHorizontal: 8;
  paddingVertical: 4;
  borderRadius: 16;
  maxWidth: 114;
  maxHeight: 24;
`;

const CustomModalBox = Styled(ModalBox)`
  width: 100%;
  height: 50%;
  backgroundColor: transparent;
  alignItems: center;
`;

const dotted = require('../../images/line-dotted.png');
const calendar = require('../../images/calendar.png');
const loc = require('../../images/location.png');
const star = require('../../images/star@4x-8.png');
const sort = require('../../images/sort.png');
const filter = require('../../images/filter.png');
const noPict = require('../../images/no-pict.png');
const hotelWarning = require('../../images/warning-hotel.png');

const getHotelResult = gql`
  query(
    $param: paramHotel
  ) {
    hotels(
      param: $param
    ) {
      HotelProducts {
        id code name city image
        HotelCategory { simpleCode }
        HotelPricing { id hotelCode currency price }
      }
      HotelConfig { name value }
    }
  }`
;

const sortOptions = [
  {sort: 'price', value: 'asc', label: 'Harga Terendah' },
  {sort: 'price', value: 'desc', label: 'Harga Tertinggi' },
  {sort: 'star', value: 'asc', label: 'Bintang Terendah' },
  {sort: 'star', value: 'desc', label: 'Bintang Tertinggi' }
];

class HotelResultScreen extends Component {
    static navigationOptions = {
      header: null
    };

    constructor(props) {
      super(props);
      this.state = {
        idRequest: 0,
        modalFilter: false,
        modalSort: false,
        hotelConfig: null,
        page: 1,
        star: [],
        facilities: [],
        price: [0, 0],
        minPrice: null,
        maxPrice: null,
        selectedSort: {
          price: "",
          star: ""
        },
        loadingHotel: true,
        setPricingConfig: true,
        hotelsLength: 0,
        hotelThisCity: 0
      };
    }

    componentDidMount() {
      this.getHotelResult(this.state.idRequest);
    }

    openModal(modal) {
      this.setState({ [modal]: true });
    }

    closeModal(modal) {
      this.setState({ [modal]: false });
    }

    onSelectedFilter(filter) {
      const { star, facilities, minPriceTextInput, maxPriceTextInput } = filter;
      const price = [minPriceTextInput, maxPriceTextInput];
      console.log(price, 'prince');

      this.props.clearHotels();
      this.setState({ modalFilter: false, star, facilities, price, page: 1, loadingHotel: true }, () => {
        this.getHotelResult(this.state.idRequest);
      });
    }

    onSelectedSort = (selectedSort) => {
      let unselected = '';
      if (selectedSort.sort == 'price') unselected = 'star';
      else unselected = 'price';

      this.props.clearHotels();
      this.setState({
        modalSort: false,
        selectedSort: { [selectedSort.sort]: selectedSort.value, [unselected]: "" },
        page: 1,
        loadingHotel: true
      }, () => this.getHotelResult(this.state.idRequest) );
    }

    getMoreHotelResult() {
      const { page, hotelsLength, idRequest } = this.state;
      if (hotelsLength < this.props.itemPerPage) return;
      this.setState({ idRequest: idRequest + 1, page: page + 1, loadingHotel: true }, () => {
        this.getHotelResult(idRequest + 1);
      });
    }

    getHotelResult(idReq) {
      const { type, itemPerPage, checkInDate, checkOutDate, guest, room, hotel, hotels } = this.props;
      const { idRequest, page, star, facilities, price, selectedSort, setPricingConfig } = this.state;

      let variables = {};
      let parameters = {
        page,
        itemPerPage,
        checkInDate: Moment(checkInDate).format('YYYY-MM-DD'),
        checkOutDate: Moment(checkOutDate).format('YYYY-MM-DD'),
        adult: guest,
        room,
        filter: {
          star,
          facilities,
          minPrice: price[0]
        },
        sort: {...selectedSort}
      };

      if (type === 'city') parameters = Object.assign({destination: hotel.code}, parameters);
      else parameters = Object.assign({code: hotel.code}, parameters);

      if (price[1] >= price[0]) parameters.filter = { ...parameters.filter, maxPrice: price[1] };
      if (price[1] < price[0]) parameters.filter = { star, facilities, minPrice: price[0] };

      variables = Object.assign({ param: parameters }, variables);

      console.log(variables, 'variables');

      Client
      .query({
        query: getHotelResult,
        variables
      })
      .then(res => {
        let data = res.data.hotels;
        let minimalPrice = 0, maximalPrice = 0;
        if (idReq !== idRequest) return;

        if (data) {
          let currentStateResult = hotels;
          currentStateResult = currentStateResult.concat(data.HotelProducts);

          this.props.updateHotels(currentStateResult);

          for(const config of data.HotelConfig) {
            if(config.name == 'min-price') minimalPrice = parseInt(config.value);
            if(config.name == 'max-price') maximalPrice = parseInt(config.value);
          }

          if (setPricingConfig) {
            this.setState({
              hotelConfig: data.HotelConfig,
              hotelThisCity: data.HotelProducts.length,
              minPrice: minimalPrice,
              maxPrice: maximalPrice,
              setPricingConfig: false
            })
          }

          this.setState({ loadingHotel: false, hotelsLength: data.HotelProducts.length });
        }
      }).catch(reject => {
        let errorMessage = 'Terjadi kesalahan server';
        if (reject.graphQLErrors[0].extensions.code.indexOf('CLIENT_') === 0) errorMessage = reject.graphQLErrors[0].message;
        console.log(errorMessage, 'errorMessage');
      });
    }

    onSelectedHotel(item) {
      const params = {
        hotelCode: item.code,
        hotelName: item.name,
        hotelCity: item.city,
        checkInDate: this.props.checkInDate,
        checkOutDate: this.props.checkOutDate,
        guest: this.props.guest,
        room: this.props.room,
        image: item.image
      }

      paramsUpdate = {
        code: item.code,
        name: item.name,
        city: item.city
      }

      this.props.navigation.navigate('DetailHotelScreen', params)

      this.props.updateLastSearchHotel(paramsUpdate);
    }

    keyExtractor = (item, index) => index.toString();

    renderInfoBox(props) {
      const { checkInDate, checkOutDate, guest, room, navigation } = props;

      return (
        <TopView>
          <TopLeftView>
            <GuestInfoView style={{marginRight: 5}}>
              <Text size={12} style={{color: '#FFFFFF'}} type='medium'>{Moment(checkInDate).format('DD MMM')} - {Moment(checkOutDate).format('DD MMM')}</Text>
            </GuestInfoView>
            <GuestInfoView>
              <Text size={12} style={{color: '#FFFFFF'}} type='medium'>{guest} Tamu {room} Kamar</Text>
            </GuestInfoView>
          </TopLeftView>
          <TopCenterView />
          <TopRightView>
            <SelectButton fontSize={12} onPress={() => navigation.navigate('HotelScreen', { title: 'Ubah Pencarian' })} source={calendar}>Ubah</SelectButton>
          </TopRightView>
        </TopView>
      )
    }

    renderStar(total) {
      const array = [];
      for (let i = 0; i < total; i++) array.push('');

      return array.map((item, i) =>
        <ImageProperty key={i} resizeMode='stretch' source={star} style={{width: 10, height: 10, marginTop: 10, marginBottom: 10}} />
      );
    }

    renderListHotel = ({ item, i }) => (
      <ContentView key={i}>
        <MainImageView>
          <MainImageLimiter>
            {this.state.hotelConfig ? <ImageProperty source={{uri: this.state.hotelConfig[0].value + item.image}} /> : <ImageProperty style={{backgroundColor: '#DDDDDD'}} />}
          </MainImageLimiter>
        </MainImageView>

        <MainAllInfo>
          <TopInfo>
            <TitleText type='bold'>{item.name}</TitleText>
            <View style={{flexDirection: 'row'}}>
              {this.renderStar(item.HotelCategory ? item.HotelCategory.simpleCode : 0)}
            </View>
            <BottomInfo style={{alignItems: 'center'}}>
              <ImageProperty resizeMode='stretch' source={loc} style={{width: 10, height: 10, marginRight: 4}} />
              <Text size={10}>{item.city}</Text>
            </BottomInfo>
          </TopInfo>

          <DottedView>
            <DottedViewLimiter>
              <ImageProperty source={dotted} />
            </DottedViewLimiter>
          </DottedView>

          <BottomInfo>
            <BottomInfoLeft>
              <PriceText type='bold'>{FormatMoney.getFormattedMoney(item.HotelPricing ? item.HotelPricing.price : 0)}</PriceText>
              <SmallInfoText type='medium'>per kamar per malam </SmallInfoText>
            </BottomInfoLeft>

            <BottomInfoRight>
              <SelectButton style={{backgroundColor: Color.text}} fontSize={12} onPress={() => this.onSelectedHotel(item)}>Pesan</SelectButton>
            </BottomInfoRight>
          </BottomInfo>
        </MainAllInfo>
      </ContentView>
    )

    renderNoHotelAvailability() {
      return (
        <View style={{borderTopColor: '#FAF9F9', borderTopWidth: 8, alignItems: 'center'}}>
          <Image resizeMode='contain' source={hotelWarning} style={{height: 95, width: 75, marginTop: 60}} />
          <Text size={18} type='bold' style={{marginTop: 40, letterSpacing: 0.54}}>Hotel Tidak Tersedia</Text>
          {this.state.hotelThisCity > 0 && <Text style={{marginTop: 11}}>Reset filter untuk menampilkan semua hotel yang tersedia.</Text>}
        </View>
      )
    }

    renderHotelResult(hotels) {
      return (
        <FlatList
          contentContainerStyle={{paddingBottom: 50}}
          keyExtractor={this.keyExtractor}
          data={hotels}
          renderItem={this.renderListHotel}
          onEndReached={() => this.getMoreHotelResult()}
          onEndReachedThreshold={0.1}
        />
      )
    }

    render() {
      const { navigation, checkInDate, checkOutDate, guest, room, hotels } = this.props;
      const { loadingHotel, star, facilities, price, minPrice, maxPrice, selectedSort, modalFilter, modalSort, hotelThisCity } = this.state;

      console.log(this.state, 's');
      console.log(this.props, 'p');

      return (
        <MainView>
          <Header title={navigation.state.params.hotel.name + ', ' + navigation.state.params.hotel.country} />
          <PureView>
            {this.renderInfoBox(this.props)}
            {hotels.length > 0 && this.renderHotelResult(hotels)}
            {hotels.length === 0 && !loadingHotel && this.renderNoHotelAvailability()}
          </PureView>
          {!loadingHotel && hotelThisCity > 0 && <ConfigureContainer>
            <ConfigureView>
              <LeftConfigure activeOpacity={0.5} onPress={() => this.openModal('modalFilter')}>
                <FilterIcon><ImageProperty resizeMode='contain' source={filter} /></FilterIcon>
                <ConfigLabel type='semibold'>Filter</ConfigLabel>
              </LeftConfigure>
              <RightConfigure activeOpacity={0.5} onPress={() => this.openModal('modalSort')}>
                <SortIcon><ImageProperty resizeMode='contain' source={sort} /></SortIcon>
                <ConfigLabel type='semibold'>Urutkan</ConfigLabel>
              </RightConfigure>
            </ConfigureView>
          </ConfigureContainer>}

          {hotels.length > 0 && loadingHotel && <ConfigureContainer>
            <ActivityIndicator size='large' color={Color.text} />
          </ConfigureContainer>}

          <ModalIndicator
            visible={hotels.length === 0 && loadingHotel}
            type="large"
            message="Memuat ketersedian hotel"
          />

          <Modal
            visible={modalFilter}
            onRequestClose={() => this.closeModal('modalFilter')}
            animationType='slide'
          >
            <HotelFilter
              data={{star, facilities, price, minPrice, maxPrice}}
              onClose={() => this.closeModal('modalFilter')}
              onSelectedFilter={(filter, load) => load ? this.onSelectedFilter(filter) : this.closeModal('modalFilter')}
            />
          </Modal>

          <CustomModalBox
            coverScreen
            position='bottom'
            isOpen={modalSort}
            onClosed={() => this.closeModal('modalSort')}
          >
            <HotelSort
              sortOptions={sortOptions}
              selected={selectedSort}
              onSelectedSort={this.onSelectedSort} />
          </CustomModalBox>
        </MainView>
      )
    }
}

const mapStateToProps = state => {
  return {
    type: state['hotels'].type,
    hotel: state['hotels'].hotel,
    itemPerPage: state['hotels'].itemPerPage,
    checkInDate: state['hotels'].checkInDate,
    checkOutDate: state['hotels'].checkOutDate,
    guest: state['hotels'].guest,
    room: state['hotels'].room,
    hotels: state['hotels'].hotels
  };
};

const mapDispatchToProps = (dispatch, props) => (
  {
    updateInputUser: (data) => {
      dispatch(updateInputUser(data));
    },
    updateHotels: (data) => {
      dispatch(updateHotels(data));
    },
    updateHotelCode: (data) => {
      dispatch(updateHotelCode(data));
    },
    clearAll: () => {
      dispatch(clearAll());
    },
    clearHotels: () => {
      dispatch(clearHotels());
    },
    updateLastSearchHotel: (params) => dispatch({ type: 'UPDATE_LAST_SEARCH_HOTEL', params })
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(HotelResultScreen);
