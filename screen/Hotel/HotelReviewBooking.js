import React, { Component } from 'react';
import { View, ScrollView, Modal, Platform } from 'react-native';
import Styled from 'styled-components';
import { connect } from 'react-redux';
import Moment from 'moment';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import TouchableOpacity from '../Button/TouchableDebounce';
import Button from '../Button';
import ContactBooking from '../ContactBooking';
import ModalIndicator from '../Modal/ModalIndicator';
import ModalInformation from '../Modal/ModalInformation';
import { getTitles } from '../../state/actions/get-titles';
import { bookHotel } from '../../state/actions/hotel/book-hotel';
import Text from '../Text';
import Color from '../Color';
import Header from '../Header';
import FormatMoney from '../FormatMoney';

const ArrowRightIcon = Styled(FontAwesome)`
    fontSize: 16;
    color: ${Color.text};
`;

const MainView = Styled(View)`
  width: 100%;
  height: 100%;
  alignItems:   center;
  backgroundColor: #FFFFFF;
`;

const BaseText = Styled(Text)`
  fontSize: 12px
`;

const WhiteText = Styled(Text)`
  color: #FFFFFF
  lineHeight: 12px;
  fontSize: 11px;
`;

const AbsoluteView = Styled(View)`
  position: absolute;
  top: 30;
  zIndex:5;
  width: 100%;
`;
const HeaderView = Styled(TouchableOpacity)`
  minHeight: 1px;
  backgroundColor: #FFFFFF;
  padding: 10px;
  elevation: 5px;
  margin: 0 16px;
  flexDirection: row;
     borderWidth: ${Platform.OS === 'ios' ? 1 : 0 };
    borderBottomWidth: ${Platform.OS === 'ios' ? 2 : 0 };
    borderColor: #DDDDDD;
`;

const PriceView = Styled(View)`
  backgroundColor: #FFFFFF
  flexDirection: row
  padding: 0px 15px
  height: 62px
  width: 100%
`;

const LeftHeaderView = Styled(View)`
  width: 30%;
  justifyContent: flex-start;
  alignItems: flex-start;
`;

const MidHeaderView = Styled(View)`
  width: 70%;
  justifyContent: flex-start;
  alignItems: flex-start;
`;
const MidSubHeader = Styled(View)`

`;
const MidSubHeaderText = Styled(Text)`
  fontSize:11px;
`;

const RightHeaderView = Styled(View)`
  width: 100%
  height: 100%
  justifyContent: center
  alignItems: flex-end
`;

const BlackHeaderView = Styled(View)`
  backgroundColor: #000000;
  width: 70px;
  height: 20px;
  justifyContent: center;
`;

const ContentView = Styled(View)`

`;

const WrapContentView = Styled(View)`
  flex: 1;
  height: 65px;
  justifyContent: center;
  alignItems: flex-start;
`;
const IconView = Styled(View)`
  justifyContent: center
`;

const PerContent = Styled(View)`
  flexDirection: row
`;

const ContactView = Styled(View)`
  width: 100%
  backgroundColor: #FFFFFF
  borderBottomColor: #DDDDDD
  borderBottomWidth: 1px
`;
const ContactViewPemesan = Styled(View)`
  width: 100%
  backgroundColor: #FFFFFF
  borderBottomColor: #FAF9F9
  borderBottomWidth: 8px
`;
const SubContactView = Styled(View)`
  margin: 22px 15px
  alignItems: flex-start
`;
const SubtotalView = Styled(View)`
  margin: 22px 15px 10px
  alignItems: flex-start
  flexDirection: row
  elevation: 5px
`;

const TextView = Styled(SubtotalView)`
  margin: 0px 10px 0px 0px
  alignItems: flex-start
  flexDirection: row
`;

const LeftView = Styled(MidHeaderView)`
  alignItems: flex-end
`;

const ButtonText = Styled(Text)`
  fontSize: 14px
  color: #FFFFFF
  lineHeight: 34px
`;

const PriceText = Styled(Text)`
  color: #FF425E
`;

const ButtonRadius = Styled(TouchableOpacity)`
  borderRadius: 30px;
  width: 100%;
  backgroundColor: #231F20;
  height: 100%;
  justifyContent: center;
`;

const NormalText = Styled(Text)`
`;

const CustomButton = Styled(Button)`
    width: 100%;
    height: 100%;
`;
const ButtomView = Styled(View)`
  width: 100%
  backgroundColor: #FFFFFF
  alignItems: center
  justifyContent: center
  flexDirection: row
`;

const PassangerView = Styled(TouchableOpacity)`
  alignItems: flex-start
  backgroundColor: #FFFFFF
  padding: 16px
  width: 100%
  borderBottomColor: #DDDDDD
  borderBottomWidth: 1px
`;
const MainBookingView = Styled(View)`
    width: 100%;
    height: 63;
    flexDirection: row;
    justifyContent: space-between;
    alignItems: center;
    backgroundColor: #FFFFFF;
    padding: 8px 16px 8px 16px;
    elevation: 20;
`;

const HalfBookingView = Styled(View)`
    width: 50%;
    height: 100%;
    flexDirection: column;
    alignItems: flex-start;
    justifyContent: center;
`;
const titles = [
  { abbr: 'MR', name: 'Mr', isAdult: true },
  { abbr: 'MRS', name: 'Mrs', isAdult: true },
  { abbr: 'MS', name: 'Ms', isAdult: true }
];

class HotelReviewBooking extends Component {
    static navigationOptions = { header: null };

    constructor(props){
        super(props);

        const { title, firstName, lastName, countryCode, phoneNumber, email } = this.props.user_siago;
        
        this.state = {
          modalFail: false,
          loading: false,
            contact: {
              title: title || '',
              firstName: firstName || '',
              lastName: lastName || '',
              countryCode: countryCode || '62',
              phone: phoneNumber || '',
              email: email || ''
            },
            errors: {
                title: null,
                firstName: null,
                lastName: null,
                countryCode: null,
                phone: null,
                email: null,
            }
        }
    }

    componentWillReceiveProps(nextProps) {
      if (!this.props.loadingBooking && nextProps.loadingBooking) {
        this.openModal('loading');
      }else if (this.props.loadingBooking && !nextProps.loadingBooking) {
        this.closeModal('loading');
        if (nextProps.errorBooking === null) {
          this.props.navigation.navigate('PaymentScreen');
        }else {
          this.setState({ modalFail: true }, () => setTimeout(() => {
            this.setState({ modalFail: false,
            })
          }, 1000));
          console.log(nextProps.errorBooking);
        }
      }
    }

    onContactChange = (name, value) => {
      this.setState(prevState => {
        return {
          contact: {
            ...prevState.contact,
            [name]: value
          }
        }
      }, () => {console.log(this.state.contact)})
    }

    getTitleLabel = (abbr) => {
      const idx = titles.findIndex(title => title.abbr === abbr);
      if (idx !== -1) return titles[idx].name;
      return '';
    }

    openModal = (modal) => {
      this.setState({
        [modal]: true
      })
    }

    closeModal = (modal) => {
      this.setState({
        [modal]: false
      })
    }

    submit() {
      const { rateEnc, ratePrices } = this.props.navigation.state.params;
      this.props.onBookHotel(this.state.contact, rateEnc, ratePrices);
    }

    render() {
        const { hotelName, roomName, boardName, checkOutDate, checkInDate, night, guest, rateEnc, ratePrices } = this.props.navigation.state.params;
        // console.log(this.props);
        // console.log(this.state);

        return (
          <MainView>
            <Header title='Review & Pesan' />
            <ScrollView style={{minHeight: 1, width: '100%'}}>
              <View style={{height: 76, backgroundColor: Color.theme, width: '100%'}} />
              <AbsoluteView>
                <HeaderView>
                  <WrapContentView>
                    <Text type='bold'>{hotelName}</Text>
                    <Text size='12' type='medium'>{Moment(checkInDate).format('DD MMM')} - {Moment(checkOutDate).format('DD MMM YYYY')}, {night} Malam, {guest} Tamu</Text>
                    <Text size='12' type='medium'>{roomName}</Text>
                  </WrapContentView>
                  {/*<IconView>
                     <Text><ArrowRightIcon name='chevron-right' /></Text>
                  </IconView>*/}
                </HeaderView>
              </AbsoluteView>
              <PriceView />

              <ContactViewPemesan>
                <ContactBooking
                  titles={titles}
                  contact={this.state.contact}
                  errors={this.state.errors}
                  onContactChange={this.onContactChange}
                />
              </ContactViewPemesan>
              </ScrollView>

              <ButtomView>
                <MainBookingView>
                  <HalfBookingView>
                    <NormalText type='medium'>Subtotal</NormalText>
                    <PriceText type='bold'>{FormatMoney.getFormattedMoney(ratePrices)}</PriceText>
                  </HalfBookingView>
                  <HalfBookingView>
                    {/*<CustomButton onPress={() => this.submit()}>Bayar Sekarang</CustomButton>*/}
                    <ButtonRadius delay={1000} onPress={() => this.submit()}>
                      <ButtonText type='semibold'>Bayar Sekarang</ButtonText>
                    </ButtonRadius>
                  </HalfBookingView>
                </MainBookingView>
              </ButtomView>

              <ModalIndicator
                  visible={this.state.loading}
                  type="large"
                  message="Harap tunggu, kami sedang memproses pesanan Anda"
              />
              <Modal
                onRequestClose={() => {}}
                animationType="fade"
                transparent
                visible={this.state.modalFail}
              >
                <ModalInformation
                  label='Booking Anda Gagal'
                  error
                />
              </Modal>
          </MainView>
        )
    }

}

const MapStateToProps = state => {
  return {
      hotel: state['hotels'].hotel,
      loadingTitle: state['titles'].fetching,
      loadingBooking: state['booking'].fetching,
      user: state['user.auth'].login.user,
      errorBooking: state['booking'].error,
      user_siago: state['user.auth'].user_siago
  };
};

const mapDispatchToProps = (dispatch, props) => (
  {
      getTitles: () => {
          dispatch(getTitles());
      },
      onBookHotel: (contact, hotels, finalAmount) => {
          dispatch(bookHotel(contact, hotels, finalAmount))
      }
  }
);

export default connect(MapStateToProps, mapDispatchToProps)(HotelReviewBooking);
