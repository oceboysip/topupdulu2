import React, { Component } from 'react';
import { View, Image, ScrollView, ActivityIndicator, Platform, Animated, Dimensions } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';
import Carousel from 'react-native-banner-carousel';
import gql from 'graphql-tag';
import Icon from 'react-native-vector-icons/MaterialIcons';
// import MapView, { Marker, Polyline, PROVIDER_GOOGLE } from 'react-native-maps';

import { Row, Col } from '../Grid';
import Client from '../../state/apollo';
import Text from '../Text';
import Color from '../Color';
import Button from '../Button';
import TouchableOpacity from '../Button/TouchableDebounce';
import FormatMoney from '../FormatMoney';
import Header from '../Header';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    backgroundColor: #FFFFFF;
`;

const UpperMainView = Styled(View)`
    flex: 1;
    width: 100%;
`;

const BottomMainView = Styled(View)`
    width: 100%;
    height: 83;
    alignItems: center;
    justifyContent: space-between;
    flexDirection: row;
    paddingHorizontal: 16;
    backgroundColor: #FFFFFF;
    borderWidth: 0;
    elevation: 5;
`;

const HalfBottomMainView = Styled(View)`
    minWidth: 1;
    height: 100%;
    justifyContent: center;
`;

const SubMainView = Styled(View)`
    width: 100%;
    height: 100%;
    paddingTop: 150;
    flexDirection: column;
`;

const RowView = Styled(Row)`
    height: 100%;
`;

const ColumnView = Styled(Col)`
    justifyContent: center;
    alignItems: center;
`;

const SideButton = Styled(TouchableOpacity)`
    flex: 1;
    width: 100%;
    alignItems: center;
    justifyContent: center;
`;

const SideIcon = Styled(Icon)`
    fontSize: 23;
    color: ${Color.text};
`;

const CustomHeader = Styled(Header)`
    height: 60;
`;

const AbsoluteCustomHeader = Styled(Animated.View)`
    width: 100%;
    height: 60;
    alignItems: center;
    flexDirection: row;
    zIndex: 1;
    position: absolute;
`;

const BackgroundImageView = Styled(View)`
    width: 100%;
    height: 260;
    position: absolute;
`;

const MainInfoContainer = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: transparent;
    marginBottom: 16;
`;

const MainInfoView = Styled(View)`
    width: 100%;
    minHeight: 1;
    backgroundColor: #FFFFFF;
    padding: 14px 8px 16px 8px;
    borderRadius: 3;
    elevation: 5;
    flexDirection: column;
    borderWidth: ${Platform.OS === 'ios' ? 1 : 0 };
    borderBottomWidth: ${Platform.OS === 'ios' ? 2 : 0 };
    borderColor: #DDDDDD;
`;

const TitleInfoView = Styled(View)`
    width: 100%;
    minHeight: 1;
`;

const DateInfoView = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    justifyContent: flex-start;
`;

const LeftDateInfoView = Styled(View)`
    padding: 4px 8px 4px 8px;
    borderRadius: 30;
    backgroundColor: ${Color.theme};
`;

const CalendarView = Styled(View)`
    width: 9;
    height: 8;
    marginRight: 4;
`;

const LoadingActivityView = Styled(View)`
    width: 100%;
    flex: 1;
    justifyContent: center;
    alignItems: center;
    backgroundColor: #FFFFFF;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const TitleText = Styled(Text)`
    textAlign: left;
`;

const DescriptionText = Styled(Text)`
    textAlign: left;
    fontSize: 12;
`;

const AnimatedTitleText = Styled(Animated.Text)`
    fontSize: 14;
    textAlign: center;
    textShadowRadius: 0;
`;

const DateText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const BottomText = Styled(Text)`
    textAlign: left;
`;

const PriceText = Styled(Text)`
    textAlign: left;
    color: #FF425E;
    textAlign: left;
`;

const HeaderTabsText = Styled(Text)`
    fontSize: 12;
`;

const BookingButton = Styled(Button)`
    width: 100%;
    height: 60%;
`;

const TopView = Styled(View)`
  backgroundColor: #FFFFFF;
  width: 100%;
  flexDirection: row;
  alignItems: center;
`;

const SelectButton = Styled(Button)`
  maxWidth: 76;
  width: 100%;
  height: 24;
`;

const TopLeftView = Styled(View)`
  width: 74%;
  flexDirection: row;
  justifyContent: flex-start;
  alignItems: center;
`;

const TopCenterView = Styled(View)`
  width: 2%;
`;

const TopRightView = Styled(View)`
  width: 24%;
  alignItems: flex-end;
  justifyContent: center;
`;

const GuestInfoView = Styled(View)`
  backgroundColor: ${Color.theme};
  paddingHorizontal: 8;
  paddingVertical: 4;
  borderRadius: 16;
  maxWidth: 114;
  maxHeight: 24;
`;

const { width } = Dimensions.get('window');

const maxBannerShowing = 5;
const calendar = require('../../images/calendar.png');
const star = require('../../images/star@4x-8.png');
const loc = require('../../images/location.png');
const noPict = require('../../images/no-pict.png');
const warnHotel = require('../../images/warning-hotel.png');

const getHotelDetail = gql`
  query(
    $param: paramHotel
  ) {
    hotelDetail(
      param: $param
    ) {
      name address description
      rooms{
        roomFacilities{
          facilityName
        }
      }
      facilities{ groupCode groupName facilities { facilityName } }
      HotelFacilitiesHighlight { name active image }
      HotelConfig { name value }
      HotelCategory { simpleCode }
      HotelPricing{ price currency }
      images { path }
      coordinates { latitude longitude }
      nearby { poiName distance }
    }
  }`
;

export default class DetailHotelScreen extends Component {

    static navigationOptions = { header: null };
    // animatedValue;

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            hotelDetail: {},
            photosBanner: [],
            showingPhotosBanner: null,
            photoModal: null,
            selectedPhotoModal: 0,
            modalPhotoViewer: false,
            modalHotelFacilities: false,
            modalHotelDescription: false,
            readyPhotosBanner: false,
            readyPhotosModal: false,
            readyHotelDetail: false,
            openMaps: false,
        }
    }

    componentWillMount() {
        // this.props.navigation.setParams({
        //   animatedValue: this._animatedValue.interpolate({
        //     inputRange: [0, 1],
        //     outputRange: ['transparent', '#FFFFFF'],
        //     extrapolate: 'clamp'
        //   }),
        //   animatedText : this._animatedValue.interpolate({
        //     inputRange: [0, 210],
        //     outputRange: ['transparent', '#345E6D'],
        //     extrapolate: 'clamp'
        //   })
        // });
      }

    componentDidMount() {
        this.getHotelDetail();
        // this.setAnimatedHeader();
    }

    getHotelDetail() {
        const variables = {
          param: {
            code: this.props.navigation.state.params.hotelCode
          }
        };

        Client
            .query({
                query: getHotelDetail,
                variables
            })
            .then(res => {
                if (res.data.hotelDetail) {
                  this.setState({ hotelDetail: res.data.hotelDetail, readyHotelDetail: true });
                  console.log(this.props.navigation.state.params.hotelCode, 'code')
                  console.log(res.data.hotelDetail, 'detail')

                  this.preparePhotosBanner();
                  this.prepareShowingPhotosBanner();
                //   this.preparePhotoModal();
                }
            }).catch(reject => {
                let errorMessage = 'Terjadi kesalahan server';
                if (reject.graphQLErrors[0].extensions.code.indexOf('CLIENT_') === 0) errorMessage = reject.graphQLErrors[0].message;
                this.setState({ hotelDetail: {}, readyHotelDetail: true, readyPhotosBanner: true });
                console.log(reject, 'reject')
            });
      }

      searchImageConfig() {
        for (const config of this.state.hotelDetail.HotelConfig)
          if (config.name === 'image-url-ori') return config.value;
      }

    // setAnimatedHeader() {
        // this.animatedValue = new Animated.Value(0);
        // this.props.navigation.setParams({
        //     animatedValue: this.animatedValue.interpolate({
        //         inputRange: [0, 50],
        //         outputRange: ['transparent', Color.theme],
        //         extrapolate: 'clamp'
        //     }),
        //     animatedText: this.animatedValue.interpolate({
        //         inputRange: [0, 50],
        //         outputRange: ['transparent', Color.text],
        //         extrapolate: 'clamp'
        //     })
        // });
    // }

    renderLoadingIndicator() {
      return (
        <LoadingActivityView>
          <ActivityIndicator size='large' color={Color.loading} />
        </LoadingActivityView>
      );
    }

    renderAbsoluteCustomHeader(animatedValue, animatedText) {
      return (
        <AbsoluteCustomHeader style={{ backgroundColor: animatedValue || 'transparent' }}>
          <RowView>
            <ColumnView size={2}>
              <SideButton onPress={() => this.props.navigation.pop()}>
                <SideIcon name='arrow-back' />
              </SideButton>
            </ColumnView>

            <ColumnView size={7.8}>
              <AnimatedTitleText numberOfLines={2} type='bold' style={animatedText && { color: animatedText }}>Nama Hotel</AnimatedTitleText>
            </ColumnView>

            <ColumnView size={2.2}><View /></ColumnView>
          </RowView>
        </AbsoluteCustomHeader>
      );
    }

    renderBackgroundImage(image) {
      image.map((img, i) => (
        <ImageProperty source={img} />
      ));
    }

    preparePhotosBanner() {
      const tempArray = [];
      const imageConfig = this.searchImageConfig();
      for (const image of this.state.hotelDetail.images) tempArray.push(imageConfig + image.path);
      this.setState({ photosBanner: tempArray });
    }

    prepareShowingPhotosBanner() {
      let tempArray = [];
      if (this.state.photosBanner.length > maxBannerShowing)
        for (let i = 0; i < maxBannerShowing; i++) tempArray.push(this.state.photosBanner[i]);
      else tempArray = this.state.photosBanner;
      this.setState({ showingPhotosBanner: tempArray, readyPhotosBanner: true });
    }

    renderStar(total) {
      const array = [];
      for (let i = 0; i < total; i++) array.push('');
      // return array.map((item, i) =>
      //   <SmallImage source={starImage} key={i} />
      // );

      return array.map((item, i) =>
      <ImageProperty key={i} resizeMode='stretch' source={star} style={{ width: 10, height: 10, marginTop: 10, marginBottom: 10 }} />
      );
    }

    renderInfoBox(params, paddingHorizontal, height) {
      const { checkInDate, checkOutDate, guest, room } = params;

      return (
        <TopView style={{paddingHorizontal, height}}>
          <TopLeftView>
            <GuestInfoView style={{marginRight: 5}}>
              <Text size={12} type='medium'>{Moment(checkInDate).format('DD MMM')} - {Moment(checkOutDate).format('DD MMM')}</Text>
            </GuestInfoView>
            <GuestInfoView>
              <Text size={12} type='medium'>{guest} Tamu {room} Kamar</Text>
            </GuestInfoView>
          </TopLeftView>
          <TopCenterView />
          <TopRightView>
            <SelectButton fontSize={12} onPress={() => this.props.navigation.navigate('HotelScreen', { title: 'Ubah Pencarian' })} source={calendar}>Ubah</SelectButton>
          </TopRightView>
        </TopView>
      );
    }

    renderThreeMainFacilities(hotelFacilitiesHighlight) {
      return hotelFacilitiesHighlight.map((facility, i) =>
        <View key={i} style={{marginRight: 5}}>
          {/* <FacilitiesImage source={{ uri: facility.image }} /> */}
          <LeftDateInfoView>
            <DateText type='medium'>{facility.active ? facility.name : '-'}</DateText>
          </LeftDateInfoView>
        </View>
      );
    }

    renderNearbyArea(nearbyArea) {
      return nearbyArea.map((nearby, i) =>
        nearby ?
          <View style={{alignItems:'flex-start', flexDirection: 'row', width: '100%'}} key={i}>
            <View style={{width: '4%', alignItems: 'flex-start'}}>
              <Text size={12} type='medium'>{i+1}.</Text>
            </View>
            <View style={{alignItems: 'flex-start'}}>
              <Text size={12} type='medium'>{nearby.poiName}</Text>
              <Text size={12} type='medium'>({nearby.distance/1000} km)</Text>
            </View>
          </View>
        :
          '-'
      );
    }

    renderEmptyHotel(params) {
      const { hotelName, hotelCity } = params;

      return (
        <MainView>
          <CustomHeader
            showLeftButton
            showIconLeftButton={true}
            title={`${hotelName}, ${hotelCity}`}
          />
          {this.renderInfoBox(params, 8, 60)}
          <View style={{alignItems: 'center', paddingTop: 76, paddingHorizontal: 8, borderTopWidth: 8, borderColor: '#FAF9F9'}}>
            <Image style={{width: 72, height: 103, marginBottom: 32}} source={warnHotel} resizeMode='contain' />
            <Text type='bold' size={18} style={{letterSpacing: 0.54}}>Hotel Tidak Tersedia</Text>
          </View>
        </MainView>
      )
    }

    render() {
      console.log(this.state, 's');
      console.log(this.props, 'p');
      const { hotelDetail, readyHotelDetail, readyPhotosBanner, showingPhotosBanner } = this.state;
      const { params } = this.props.navigation.state;

      if (!readyHotelDetail || !readyPhotosBanner) {
        return this.renderLoadingIndicator();
      }

      if (Object.getOwnPropertyNames(hotelDetail).length === 0) {
        return this.renderEmptyHotel(params);
      }

      const { name, address, phones, facilities, description, HotelFacilitiesHighlight, HotelCategory, HotelPricing, coordinates, nearby, images } = hotelDetail;
      const coor = { latitude: parseFloat(coordinates.latitude), longitude: parseFloat(coordinates.longitude) };

      return (
        <MainView>
          {/* {this.renderAbsoluteCustomHeader(animatedValue, animatedText)} */}
          <UpperMainView>
            <ScrollView>
              {/* <ScrollView contentContainerStyle={{ width: '100%', minHeight: '100%' }} onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: this.animatedValue } } }])}> */}
              <BackgroundImageView>
                {showingPhotosBanner.length > 0 && readyPhotosBanner && <Carousel
                  autoplay
                  autoplayTimeout={5000}
                  loop
                  index={0}
                  pageSize={width}
                >
                  <ImageProperty resizeMode={'stretch'} source={{ uri: "https://photos.hotelbeds.com/giata/" + params.image }} />
                  {showingPhotosBanner.map((banner, i) => <ImageProperty key={i} resizeMode='stretch' source={{uri: banner}} /> )}
                </Carousel>}

                {showingPhotosBanner.length === 0 && <ImageProperty resizeMode={'stretch'} source={noPict} /> }
              </BackgroundImageView>

              <CustomHeader transparentMode showLeftButton showIconLeftButton={true} />

              <SubMainView>
                <MainInfoContainer>
                  <View style={{width: '100%', paddingHorizontal: 16}}>
                    <MainInfoView>
                      <TitleInfoView>
                        <TitleText type='bold'>{name}</TitleText>
                        <View style={{flexDirection: 'row'}}>
                          {this.renderStar(HotelCategory.simpleCode)}
                        </View>
                        <DateInfoView>
                          <CalendarView>
                            <ImageProperty resizeMode='stretch' source={loc} />
                          </CalendarView>
                          <TitleText size={10}>{address}</TitleText>
                        </DateInfoView>
                        <TitleText size={10}>{phones}</TitleText>
                      </TitleInfoView>
                      {this.renderInfoBox(params, 0, 30)}
                    </MainInfoView>
                    <TitleText type='bold' style={{ paddingTop: 35, letterSpacing: 1.2, paddingBottom: 13 }}>DESKRIPSI</TitleText>
                    <DescriptionText alignItems='flex-start' type='medium'>{description}</DescriptionText>
                    <TitleText type='bold' style={{ paddingTop: 35, letterSpacing: 1.2, paddingBottom: 13 }}>FASILITAS</TitleText>
                    <DateInfoView>
                      {this.renderThreeMainFacilities(HotelFacilitiesHighlight)}
                    </DateInfoView>
                    <TitleText type='bold' style={{ paddingTop: 35, paddingBottom: 13 }}>LOKASI</TitleText>
                  </View>
                  {/*<MapView
                    ref={(ref) => { this.mapRef = ref; }}
                    // onPress={()}
                    style={{ width: '100%', aspectRatio: 1.5 }}
                    provider={PROVIDER_GOOGLE}
                    draggable={false}
                    mapType='standard'
                    showsScale
                    showsCompass
                    showsPointsOfInterest
                    showsBuildings
                    initialRegion={{
                      latitude: parseFloat(coor.latitude),
                      longitude: parseFloat(coor.longitude),
                      latitudeDelta: 0.0543,
                      longitudeDelta: 0.0534,
                    }}>
                    <MapView.Marker coordinate={coor} />
                  </MapView>*/}
                  <View style={{width: '100%', paddingHorizontal: 16}}>
                    <TitleText type='bold' style={{ letterSpacing: 1.2, paddingTop: 13, paddingBottom: 10 }}>AREA SEKITARNYA</TitleText>
                    {this.renderNearbyArea(nearby)}
                  </View>
                </MainInfoContainer>
              </SubMainView>
            </ScrollView>
          </UpperMainView>
          <BottomMainView>
            <HalfBottomMainView style={{width: '60%'}}>
              <BottomText type='medium'>Per kamar mulai dari</BottomText>
              <View style={{flexDirection: 'row'}}>
                <PriceText type='bold'>{FormatMoney.getFormattedMoney(HotelPricing.price)}</PriceText>
                <BottomText type='medium'> per malam</BottomText>
              </View>
            </HalfBottomMainView>
            <View style={{width: '40%', paddingLeft: 5}}>
              <BookingButton onPress={() => this.props.navigation.navigate('SelectHotelRoom', {
                hotelCode: this.props.navigation.state.params.hotelCode,
                facilities: this.state.hotelDetail.facilities,
                hotelName: name,
                hotelCity: params.hotelCity,
                HotelFacilitiesHighlight,
                checkInDate: this.props.navigation.state.params.checkInDate,
                checkOutDate: this.props.navigation.state.params.checkOutDate,
                guest: this.props.navigation.state.params.guest,
                room: this.props.navigation.state.params.room
              })}>Pilih Kamar</BookingButton>
            </View>
          </BottomMainView>
        </MainView>
      )
    }
}
