import React, { Component } from 'react';
import { View, ScrollView, Platform, Modal, SafeAreaView } from 'react-native';
import Styled from 'styled-components';
import Moment from 'moment';
import { connect } from 'react-redux';
import ModalBox from 'react-native-modalbox';

import Text from '../Text';
import Color from '../Color';
import Button from '../Button';
import Header from '../Header';
import TouchableOpacity from '../Button/TouchableDebounce';
import ModalHotelCityPicker from './HotelCityPicker';
import CalendarMultipleScreen from "../CalendarMultipleScreen";
import ModalGuestRoomPicker from './GuestRoomPicker';
import { updateInputUser } from '../../state/actions/hotel/update-input-user';
import { clearAll } from '../../state/actions/hotel/clear-all';
import BannerSlider from '../BannerSlider';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    flexDirection: column;
    backgroundColor: #F4F4F4;
`;

const CustomScrollView = Styled(ScrollView)`
    width: 100%;
    paddingTop: 20;
`;

const ChildContainer = Styled(View)`
    flex; 1;
    width: 100%;
    alignItems: center;
`;

const BackgroundView = Styled(View)`
    width: 100%;
    height: 130;
    backgroundColor: ${Color.theme};
    position: absolute;
    top: 0;
    left: 0;
`;

const MainBookingView = Styled(View)`
    width: 100%;
    borderRadius: 20;
    alignItems: center;
    marginBottom: 15;
    borderWidth: 0.5;
    borderColor: #DDDDDD;
    backgroundColor: #FFFFFF;
`;

const DetailedInformation = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
    padding: 0px 16px 0px 16px;
`;

const PerDetailedInformation = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    paddingBottom: 8;
    marginBottom: 10;
    borderBottomWidth: 2;
    borderColor: ${Color.primary};
    alignItems: flex-end;
    justifyContent: flex-start;
`;

const LeftDetailedInformation = Styled(View)`
    width: 50%;
    minHeight: 1;
    alignItems: flex-start;
    justifyContent: flex-end;
`;

const FullDetailedInformation = Styled(LeftDetailedInformation)`
    width: 100%;
`;

const RightDetailedInformation = Styled(LeftDetailedInformation)`
    alignItems: flex-end;
`;

const LeftTouch = Styled(TouchableOpacity)`
    width: 100%;
    minHeight: 1;
    alignItems: flex-start;
    justifyContent: flex-end;
`;

const RightTouch = Styled(LeftTouch)`
    alignItems: flex-end;
`;

const SearchButton = Styled(Button)`
    marginBottom: 28;
    height: 45;
    width: 90%;
    marginTop: 10;
    backgroundColor: ${Color.theme};
`;

const NormalText = Styled(Text)`
    fontSize: 12;
`;

const LabelInfo = Styled(NormalText)`
    color: ${Color.theme};
    marginBottom: 8;
`;

const BlackInfo = Styled(Text)`
  fontWeight: bold;
  color: ${Color.theme};
`;

const SmallerBlackInfo = Styled(BlackInfo)`
    fontSize: 12;
    fontWeight: normal;
    textAlign: left;
`;

const ListNightDurationView = Styled(TouchableOpacity)`
    height: 50;
    flexDirection: row;
    paddingTop: 10;
    paddingBottom: 10;
    borderColor: #DDDDDD;
    borderBottomWidth: 0.5;
`;

const RightNightDurationView = Styled(View)`
    width: 65%;
    justifyContent: center;
    alignItems: flex-end;
`;

const LeftNightDurationView = Styled(RightNightDurationView)`
    width: 35%;
    justifyContent: center;
    alignItems: flex-start;
`;

const BlueText = Styled(Text)`
    color: #417689;
`;

const MainScrollView = Styled(ScrollView)`
    backgroundColor: #FFFFFF;
    flex: 1;
    paddingHorizontal: 16;
    paddingVertical: 10;
`;

const WheelHeaderView = Styled(View)`
   padding-top: 8px;
   padding-bottom: 8px;
   background-color: ${Color.theme};
   justify-content: center;
   align-items: center;
`;

const WheelHeaderText = Styled(Text)`
   fontSize: 14;
`;

const ContainerList = Styled(View)`
    backgroundColor: #FFFFFF;
    paddingVertical: 8px;
`;

// const List = Styled(Flex)`
//     paddingHorizontal: 16px;
//     marginBottom: 16px;
// `;

// const Item = Styled(Flex.Item)`
//    justifyContent: center;
// `;

const WheelButtonView = Styled(View)`
    paddingHorizontal: 16;
    paddingBottom: 10;
`;

const CustomModalBox = Styled(ModalBox)`
    width: 100%;
    height: 50%;
    backgroundColor: transparent;
    alignItems: center;
`;

const pesanan = require('../../images/icon_paperWhite.png');

class HotelScreen extends Component {
  static navigationOptions = { header : null };

  constructor(props) {
    super(props);
    this.state = {
      modalHotelPicker: false,
      modalDatePicker: false,
      modalNightDuration: false,
      modalValueBedGuest: false,
      checkInDate: Moment(),
      checkOutDate: Moment().add(1, 'day'),
      type: null,
      hotel: null,
      pickedDate: null,
      valueBedGuest: {
        guest: props.guest,
        room: props.room,
      },
      optionBedGuest: {
        guest: 2,
        room: 1
      },
    };
  }

  onSaveGuestRoom = (optionBedGuest) => {
    this.setState({ optionBedGuest, modalValueBedGuest: false });
  }

  onSelectedDate = (checkInDate, checkOutDate) => {
    this.setState({ checkInDate, checkOutDate, modalDatePicker: false });
  }

  onSelectedHotel = (type, item) => {
    this.setState({ type, hotel: item });

    if (type === 'hotel') {
      const { lastSearch } = this.props.searchHotel;
      let valid = true;

      lastSearch.map((ls, idx) => {
        if (ls.code === item.code) valid = false;
      })
      
      if (valid) this.props.updateLastSearchHotel(item);
    }
  }

  onChangeBedGuest = (name, value) => {
    this.setState(prevState => {
      return {
        ...prevState,
        valueBedGuest: {
          ...prevState.valueBedGuest,
          [name]: value
        }
      }
    });
  }

  getShortMonth(month) {
    return Moment(month, 'M').format('MMMM').substring(0, 3);
  }

  openModal = (modal) => {
    this.setState({ [modal]: true });
  }

  closeModal = (modal) => {
    this.setState({ [modal]: false });
  }

  // createListItem = () => {
  //   const passengers = [];
  //     for (const name in this.state.optionBedGuest) {
  //       passengers.push(<List justify="between" key={name}>
  //         <Item>
  //           <Text type="semiBold" lineHeight={18} style={{textTransform: 'capitalize'}}>{name}</Text>
  //           {this.state.optionBedGuest[name].subTitle !== '' && <Text size={12} lineHeight={18}>{this.state.optionBedGuest[name].subTitle}</Text>}
  //         </Item>
  //         <Item>
  //           <Stepper
  //             name={name}
  //             min={this.state.optionBedGuest[name].min}
  //             max={this.state.optionBedGuest[name].max}
  //             defaultValue={this.state.valueBedGuest[name]}
  //             onChange={this.onChangeBedGuest}
  //           />
  //         </Item>
  //       </List>
  //     );
  //   }
  //   return passengers;
  // }

  renderListModalNightDuration() {
    const tempArray = [];
    for (let i = 0; i < 30; i++) tempArray.push('');

    return tempArray.map((day, i) =>
      <ListNightDurationView key={i} activeOpacity={1.0} onPress={() => {this.onSelectedDate(false, new Date(Moment(this.state.checkInDate).add(i + 1, 'days').format('YYYY-MM-DD'))); this.closeModal('modalNightDuration'); }}>
        <LeftNightDurationView>
          {Moment(this.state.checkInDate).add(i + 1, 'days').isSame(Moment(this.state.checkOutDate)) ? 
            <BlueText style={{backgroundColor: Color.theme}}>{i + 1} Malam</BlueText>
          :
            <NormalText>{i + 1} Malam</NormalText>
          }
        </LeftNightDurationView>
        <RightNightDurationView>
          {Moment(this.state.checkInDate).add(i + 1, 'days').isSame(Moment(this.state.checkOutDate)) ?
            <BlueText>Check out: {Moment(this.state.checkInDate).add(i + 1, 'days').format('DD MMM YYYY')}</BlueText>
          :
            <NormalText>Check out: {Moment(this.state.checkInDate).add(i + 1, 'days').format('DD MMM YYYY')}</NormalText>
          }
        </RightNightDurationView>
      </ListNightDurationView>
    );
  }

  renderModalNightDuration() {
    return (
      <SafeAreaView>
      <BackgroundView>
        <Header showLeftButton={true} onPressLeftButton={() => this.closeModal('modalNightDuration')} title="Pilih Durasi" style={{ borderBottomWidth: 0.5, borderBottomColor: '#DDDDDD'}} />
        <MainScrollView>
          {this.renderListModalNightDuration()}
        </MainScrollView>
      </BackgroundView>
      </SafeAreaView>
    );
  }

  searchHotel() {
    const resultCheckInputUser = this.checkInputUser();
    if (resultCheckInputUser !== null) { 
      alert(resultCheckInputUser);
      return;
    }
    this.props.clearAll();
    this.props.updateInputUser({ type: this.state.type, hotel: this.state.hotel, checkIn: this.state.checkInDate, checkOut: this.state.checkOutDate, guest: this.state.optionBedGuest.guest, room: this.state.optionBedGuest.room });

    switch(this.state.type) {
      case 'city':
        this.props.navigation.navigate('HotelResultScreen', {
          hotel: this.state.hotel,
          checkInDate: this.state.checkInDate,
          checkOutDate: this.state.checkOutDate,
          guest: this.state.optionBedGuest.guest,
          room: this.state.optionBedGuest.room
        });
        break;
      case 'hotel':
        this.props.navigation.navigate('DetailHotelScreen', {
          hotelCode: this.state.hotel.code,
          hotelName: this.state.hotel.name,
          hotelCity: this.state.hotel.city,
          checkInDate: this.state.checkInDate,
          checkOutDate: this.state.checkOutDate,
          guest: this.state.optionBedGuest.guest,
          room: this.state.optionBedGuest.room
        });
        break;
    }
  }

  checkInputUser() {
    const { hotel, optionBedGuest } = this.state;
    if (hotel === null) return 'Silahkan pilih kota atau hotel';
    else if (optionBedGuest.room > optionBedGuest.guest) return 'Jumlah kamar tidak boleh melebihi jumlah tamu';
    return null;
  }

  openOrderBooking() {
    this.props.navigation.navigate('OrderBooking', {
      title: 'Hotel'
    })
  }

  render() {
    const { params } = this.props.navigation.state;
    const { checkInDate, checkOutDate, type, hotel, modalHotelPicker, modalDatePicker, modalNightDuration, modalValueBedGuest, optionBedGuest } = this.state;
    const { lastSearch, favoriteSearch } = this.props.searchHotel;
    let hotelText = 'Cari Hotel';

    if (hotel) {
      hotelText = hotel.name;
      if (type === 'hotel') hotelText += ', ' + hotel.city;
      else hotelText += ', ' + hotel.country;
    }

    const checkInDateText = Moment(checkInDate).format('ddd, DD MMM YYYY');
    const checkOutDateText = Moment(checkOutDate).format('ddd, DD MMM YYYY');
    const nightDuration = Moment(checkOutDate).diff(Moment(checkInDate), 'days');

    console.log(this.state, 'state hotel');
    console.log(this.props, 'props hotel');
    
    return (
      <MainView>
        <Header title={params ? params.title : 'Booking Hotel'} imageRightButton={pesanan} onPressRightButton={() => this.openOrderBooking()} />
        <ChildContainer>
          <CustomScrollView>
            <View style={{paddingHorizontal: 16}}>
              <MainBookingView>
                <DetailedInformation>

                    <PerDetailedInformation style={{paddingBottom: 10}}>
                      <FullDetailedInformation>
                        <LabelInfo type='medium' style={{paddingTop: 16}}>Tujuan</LabelInfo>
                        <LeftTouch onPress={() => this.openModal('modalHotelPicker')}>
                          <BlackInfo align='left' type='medium'>{hotelText}</BlackInfo>
                        </LeftTouch>
                      </FullDetailedInformation>
                    </PerDetailedInformation>

                    <PerDetailedInformation style={{borderBottomWidth: 0}}>
                      <LeftDetailedInformation style={{width: '70%'}}>
                        <LabelInfo type='medium'>Tanggal Check-in</LabelInfo>
                        <LeftTouch onPress={() => this.openModal('modalDatePicker', true)}>
                          <BlackInfo type='medium'>{checkInDateText}</BlackInfo>
                          <SmallerBlackInfo>Check-out {checkOutDateText}</SmallerBlackInfo>  
                        </LeftTouch>
                      </LeftDetailedInformation>

                      <RightDetailedInformation style={{width: '30%'}}>
                        <LabelInfo type='medium'>Durasi</LabelInfo>
                        <RightTouch onPress={() => this.openModal('modalDatePicker', false)}>
                          <Text type='medium' style={{fontWeight: 'normal', fontSize: 12, paddingVertical: 4, paddingHorizontal: 8, borderRadius: 13, backgroundColor: Color.theme, color: '#FFFFFF'}}>{nightDuration} Malam</Text>
                        </RightTouch>
                      </RightDetailedInformation>
                    </PerDetailedInformation>
                  
                </DetailedInformation>
              </MainBookingView>
              <MainBookingView>

                <DetailedInformation style={{paddingBottom: 10}}>

                  <PerDetailedInformation style={{marginVertical: 20}}>
                    <LeftDetailedInformation>
                      <LabelInfo type='medium'>Total Tamu dan Kamar</LabelInfo>
                      <LeftTouch style={{flexDirection: 'row', justifyContent: 'flex-start'}} onPress={() => this.openModal('modalValueBedGuest')}>
                        <BlackInfo type='medium'>{optionBedGuest.guest} Tamu, </BlackInfo>
                        <BlackInfo type='medium'>{optionBedGuest.room} Kamar</BlackInfo>
                      </LeftTouch>
                    </LeftDetailedInformation>

                    <RightDetailedInformation />
                  </PerDetailedInformation>
                  
                </DetailedInformation>
              </MainBookingView>

              <View style={{alignItems: 'center', width: '100%'}}>
                <SearchButton type='semibold' onPress={() => this.searchHotel()}>CARI</SearchButton>
              </View>
            </View>

            <BannerSlider {...this.props} />
          </CustomScrollView>
        </ChildContainer>

        <Modal
          animationType='slide'
          transparent={false}
          visible={modalHotelPicker}
          onRequestClose={() => this.closeModal('modalHotelPicker')}
        >
          <ModalHotelCityPicker
            onSelected={this.onSelectedHotel}
            onClose={() => this.closeModal('modalHotelPicker')}
            lastSearch={lastSearch}
            favoriteSearch={favoriteSearch}
            isHistory={lastSearch.length > 0 ? true : false}
            removeHistory={() => this.props.resetLastSearchHotel()}
          />
        </Modal>

        <Modal
          visible={modalDatePicker}
          animationType='slide'
          transparent={false}
          onRequestClose={() => this.closeModal('modalDatePicker')}
        >
          <CalendarMultipleScreen
            activeStartDate={true} //if true select data start to end, else select just end date
            startDate={checkInDate}
            endDate={checkOutDate}
            onSelectedDate={this.onSelectedDate}
            onClose={() => this.closeModal('modalDatePicker')}
            startDateLabel='Check-in'
            endDateLabel='Check-out'
            minDate={Moment()}
            maxDate={Moment().add(1, 'Y')}
          />
        </Modal>

        <Modal
          visible={modalNightDuration}
          onRequestClose={() => this.closeModal('modalNightDuration')}
          animationType='fade'
        >
          {this.renderModalNightDuration()}
        </Modal>

        <CustomModalBox
          coverScreen
          position='bottom'
          isOpen={modalValueBedGuest}
          onClosed={() => this.closeModal('modalValueBedGuest')}
        >
          <ModalGuestRoomPicker
            onSave={this.onSaveGuestRoom}
            onClose={() => this.closeModal('modalValueBedGuest')}
            optionBedGuest={optionBedGuest}
          />
        </CustomModalBox>
      </MainView>
    );
  }
}

const mapStateToProps = state => {
  return {
      type: state['hotels'].type,
      hotel: state['hotels'].hotel,
      checkInDate: state['hotels'].checkInDate,
      checkOutDate: state['hotels'].checkOutDate,
      guest: state['hotels'].guest,
      room: state['hotels'].room,
      hotels: state['hotels'].hotels,
      loadingHotel: state['hotels'].loading,
      searchHotel: state.searchHotel
  };
};

 const mapDispatchToProps = (dispatch, props) => (
   {
    updateInputUser: (data) => {
       dispatch(updateInputUser(data));
    },
    clearAll: () => {
       dispatch(clearAll());
    },
    updateLastSearchHotel: (params) => dispatch({ type: 'UPDATE_LAST_SEARCH_HOTEL', params }),
    resetLastSearchHotel: () => dispatch({ type: 'RESET_LAST_SEARCH_HOTEL' }),
   }
 );

export default connect(mapStateToProps, mapDispatchToProps)(HotelScreen);
