import React, { Component } from 'react';
import { View, Image, ScrollView, Platform, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import Styled from 'styled-components';
import Moment from 'moment';
import gql from 'graphql-tag';

import Text from '../Text';
import Color from '../Color';
import Button from '../Button/Button';
import TouchableOpacity from '../Button/TouchableDebounce';
import Header from '../Header';
import FormatMoney from '../FormatMoney';
import { updateInputUser } from '../../state/actions/hotel/update-input-user';
import Client from '../../state/apollo';

const TopView = Styled(View)`
  backgroundColor: #FFFFFF;
  width: 100%;
  height: 60;
  flexDirection: row;
  alignItems: center;
`;

const SelectButton = Styled(Button)`
  maxWidth: 76;
  width: 100%;
  height: 24;
`;

const TopLeftView = Styled(View)`
  width: 74%;
  flexDirection: row;
  justifyContent: flex-start;
  alignItems: center;
`;

const TopCenterView = Styled(View)`
  width: 2%;
`;

const TopRightView = Styled(View)`
  width: 24%;
  alignItems: flex-end;
  justifyContent: center;
`;

const GuestInfoView = Styled(View)`
  backgroundColor: ${Color.theme};
  paddingHorizontal: 8;
  paddingVertical: 4;
  borderRadius: 16;
  maxWidth: 114;
  maxHeight: 24;
`;

const ContentView = Styled(TouchableOpacity)`
    width: 100%;
    minHeight: 1;
    flexDirection: row;
    backgroundColor: #FFFFFF;
`;

const MainImageView = Styled(View)`
    width: 120;
    height: 100%;
`;

const MainImageLimiter = Styled(View)`
    flex: 1;
`;

const MainAllInfo = Styled(View)`
    flex: 1;
    minHeight: 1;
    flexDirection: column;
    paddingHorizontal: 8;
    paddingVertical: 10;
`;

const TopInfo = Styled(View)`
    width: 100%;
    minHeight: 1;
    flexDirection: column;
`;

const AbsoluteView = Styled(View)`
  position: absolute;
  top: 30;
  zIndex:5;
  width: 100%;
`;

const HeaderView = Styled(TouchableOpacity)`
  minHeight: 1px;
  backgroundColor: #FFFFFF;
  paddingTop: 9;
  paddingHorizontal: 8;
  elevation: 5px;
  margin: 0 16px;
  flexDirection: row;
  borderWidth: ${Platform.OS === 'ios' ? 1 : 0};
  borderBottomWidth: ${Platform.OS === 'ios' ? 2 : 0};
  borderColor: #DDDDDD;
`;

const WrapContentView = Styled(View)`
  flex: 1;
  justifyContent: center;
  alignItems: flex-start;
`;

const TagDescription = Styled(View)`
    flexDirection: row;
    justifyContent: flex-start;
    alignItems: flex-start;
`;

const BottomInfo = Styled(TopInfo)`
    flexDirection: row;
    alignItems: center;
`;

const BottomInfoLeft = Styled(TopInfo)`
    width: 60%;
`;

const BottomInfoRight = Styled(TopInfo)`
    width: 40%;
    alignItems: flex-end;
    paddingLeft: 16;
`;

const DottedView = Styled(View)`
    width: 100%;
    height: 1;
    marginTop: 12;
    marginBottom: 10;
`;

const DottedViewLimiter = Styled(View)`
    flex: 1;
`;

const ImageProperty = Styled(Image)`
    width: 100%;
    height: 100%;
`;

const TitleText = Styled(Text)`
    fontSize: 12;
    textAlign: left;
`;

const TagLabelText = Styled(Text)`
    fontSize: 10;
    textAlign: left;
`;

const SmallInfoText = Styled(Text)`
    color: #231F20;
    fontSize: 10;
    textAlign: left;
`;

const PriceText = Styled(Text)`
    color: #FF425E;
    textAlign: left;
`;

const ContactViewPemesan = Styled(View)`
  width: 100%
  backgroundColor: #FFFFFF
  borderBottomColor: #FAF9F9
  borderBottomWidth: 8px
`;

const LoadingActivityView = Styled(View)`
    width: 100%;
    flex: 1;
    justifyContent: center;
    alignItems: center;
    backgroundColor: #FFFFFF;
`;

const NoResultView = Styled(View)`
    width: 100%;
    height: 100%;
    backgroundColor: #FFFFFF;
    flex: 1;
    justifyContent: center;
    alignItems: center;
    paddingTop: 30%;
`;

const DateInfoView = Styled(ScrollView)`
    width: 100%;
    minHeight: 1;
`;

const LeftDateInfoView = Styled(View)`
    minWidth: 1;
    minHeight: 1;
    padding: 2px 4px 2px 4px;
    borderRadius: 50;
    backgroundColor: ${Color.theme};
`;

const DateText = Styled(Text)`
    fontSize: 10;
`;

const dotted = require('../../images/line-dotted.png');
const calendar = require('../../images/calendar.png');
const guestIcon = require('../../images/users-group.png');
const noPict = require('../../images/no-pict.png');
const noRoom = require('../../images/warning-hotel.png');

const getHotelAvailability = gql`
query(
  $param: paramHotel
) {
 hotelAvailability(
   param: $param
 ) {
    HotelConfig{
      name
      value
    }
    hotels {
      hotels {
        name
        currency
        rooms {
          name
          rates {
            adults
            rateClass
            rateClassName
            rateEnc
            images{
              path
            }
            boardName
            net
            cancellationPolicies{
              amount
              from
            }
          }
        }
      }
    }
 }
}`;

class SelectHotelRoom extends Component {
    static navigationOptions = { header: null };

    constructor(props) {
        super(props);
        this.state = {
            readyRooms: false,
            hotels: {},
            modalDetail: false,
            modalBedGuest: false,
            modalNightDuration: false,
            checkInDate: props.checkInDate,
            checkOutDate: props.checkOutDate,
            valueBedGuest: {
                guest: props.guest,
                room: props.room,
            },
            optionBedGuest: {
                guest: 2,
                room: 1
            },
            itemDetail: {
                room: null,
                rate: null,
                currency: null,
            }
        }
    }

    componentDidMount() {
        this.getHotelAvailability();
    }

    onBeforeReviewBooking(room, rate) {
      const { currency } = this.state.hotels.hotels.hotels[0];
      const { hotelName, HotelFacilitiesHighlight, checkInDate, checkOutDate } = this.props.navigation.state.params;
      const night = Moment(this.state.checkOutDate).diff(Moment(this.state.checkInDate), 'days');
      const { user, navigation } = this.props;

      if (user.guest) {
        navigation.navigate('LoginScreen', {
          loginFrom: 'bus', afterLogin: () => {
            navigation.navigate('HotelReviewBooking', {
              roomName: room.name,
              boardName: rate.boardName,
              checkOutDate,
              checkInDate,
              night,
              guest: this.state.optionBedGuest.guest,
              hotelName,
              rateEnc: rate.rateEnc,
              ratePrices: rate.net,
              cancellationPolicies: rate.cancellationPolicies.amount
            })
          }
        });
      }else {
        navigation.navigate('HotelReviewBooking', {
          roomName: room.name,
          boardName: rate.boardName,
          checkOutDate,
          checkInDate,
          night,
          guest: this.state.optionBedGuest.guest,
          hotelName,
          rateEnc: rate.rateEnc,
          ratePrices: rate.net,
          cancellationPolicies: rate.cancellationPolicies.amount
        })
      }
    }

    getHotelAvailability() {
      const { hotelCode } = this.props.navigation.state.params;
      const variables = {
        param: {
          // code: '131825',
          code: hotelCode,
          checkInDate: Moment(this.state.checkInDate).format('YYYY-MM-DD'),
          checkOutDate: Moment(this.state.checkOutDate).format('YYYY-MM-DD'),
          adult: this.state.optionBedGuest.guest,
          child: 0,
          room: this.state.optionBedGuest.room,
      } };

      console.log(variables, 'variables')

      Client
      .query({
        query: getHotelAvailability,
        variables
      })
      .then(res => {
        if (res.data.hotelAvailability) {
          this.setState({ hotels: res.data.hotelAvailability, readyRooms: true })
          console.log(res.data.hotelAvailability, 'respon');
        }
      }).catch(reject => {
        let errorMessage = 'Terjadi kesalahan server';
        if (reject.graphQLErrors[0].extensions.code.indexOf('CLIENT_') === 0) errorMessage = reject.graphQLErrors[0].message;
        alert(errorMessage);
      });
    }

    renderPerRoom() {
      return this.state.hotels.hotels.hotels.map(hotel => {
        return hotel.rooms.map(room =>
          this.renderCard(room)
        );
      });
    }

    renderThreeMainFacilities(hotelFacilitiesHighlight) {
      return hotelFacilitiesHighlight.map((facility, i) =>
        <View key={i} style={{marginRight: 4}}>
          {/* <FacilitiesImage source={{ uri: facility.image }} /> */}
          <LeftDateInfoView>
            <DateText type='medium'>{facility.active ? facility.name : '-'}</DateText>
          </LeftDateInfoView>
        </View>
      );
    }

    renderCard(room) {
        const { currency } = this.state.hotels.hotels.hotels[0];
        const { hotelName, HotelFacilitiesHighlight, checkInDate, checkOutDate } = this.props.navigation.state.params;
        const night = Moment(this.state.checkOutDate).diff(Moment(this.state.checkInDate), 'days');

        return room.rates.map((rate, i) =>
          {
            const url = rate.images.length > 0 ? { uri: this.state.hotels.HotelConfig[0].value + rate.images[0].path } : noPict;
            return (
              <ContentView key={i} style={{marginBottom: 10, borderWidth: 0.5, borderRadius: 3, elevation: 1, borderColor: '#0000001A'}}>
                <MainImageView>
                  <MainImageLimiter><ImageProperty source={url} /></MainImageLimiter>
                </MainImageView>

                <MainAllInfo>
                  <TopInfo>
                    <TitleText type='bold'>{room.name} - {rate.boardName}</TitleText>
                    <TagDescription style={{marginTop: 4, alignItems: 'center'}}>
                      <Image source={guestIcon} style={{width: 8, height: 8, marginRight: 3}} />
                      <TitleText type='medium'>{`maks ${rate.adults} tamu`}</TitleText>
                    </TagDescription>
                    <TitleText style={{marginTop: 4}} type='medium'>{rate.rateClass === "NOR" ? "Normal" : "Tidak bisa refund"}</TitleText>
                    <TagDescription style={{marginVertical: 4, width: '100%'}}>
                      <DateInfoView horizontal showsHorizontalScrollIndicator={false}>
                        {this.renderThreeMainFacilities(HotelFacilitiesHighlight)}
                      </DateInfoView>
                    </TagDescription>
                    <TitleText type='medium' onPress={() => this.props.navigation.navigate('DetailRoomHotel', {
                      optionBedGuest: this.state.optionBedGuest,
                      currency,
                      hotelName,
                      roomName: room.name,
                      boardName: rate.boardName,
                      checkInDate,
                      checkOutDate,
                      night,
                      HotelFacilitiesHighlight,
                      paddingRight: 8,
                      price: rate.net,
                      rate: rate,
                      hotelConfig: this.state.hotels.HotelConfig
                    })} style={{textDecorationLine: 'underline'}}>Detail</TitleText>
                  </TopInfo>

                  <DottedView>
                    <DottedViewLimiter>
                      <ImageProperty source={dotted} />
                    </DottedViewLimiter>
                  </DottedView>

                  <BottomInfo>
                    <BottomInfoLeft>
                      <PriceText type='bold'>{FormatMoney.getFormattedMoney(rate.net)}</PriceText>
                      <SmallInfoText type='medium'>per kamar per malam</SmallInfoText>
                    </BottomInfoLeft>

                    <BottomInfoRight>
                      <SelectButton fontSize={12} onPress={() => this.onBeforeReviewBooking(room, rate)}>Pesan</SelectButton>
                    </BottomInfoRight>
                  </BottomInfo>
                </MainAllInfo>
              </ContentView>
            )
          }
        );
      }

    renderLoadingIndicator() {
      return (
        <LoadingActivityView>
          <ActivityIndicator size='large' color={Color.loading} />
        </LoadingActivityView>
      );
    }

    renderInfoBox(params) {
      const { checkInDate, checkOutDate, guest, room } = params;

      return (
        <TopView>
          <TopLeftView>
            <GuestInfoView style={{marginRight: 5}}>
              <Text size={12} type='medium'>{Moment(checkInDate).format('DD MMM')} - {Moment(checkOutDate).format('DD MMM')}</Text>
            </GuestInfoView>
            <GuestInfoView>
              <Text size={12} type='medium'>{guest} Tamu {room} Kamar</Text>
            </GuestInfoView>
          </TopLeftView>
          <TopCenterView />
          <TopRightView>
            <SelectButton fontSize={12} onPress={() => this.props.navigation.navigate('HotelScreen', { title: 'Ubah Pencarian' })} source={calendar}>Ubah</SelectButton>
          </TopRightView>
        </TopView>
      )
    }

    render() {
      const { params } = this.props.navigation.state;
      const { readyRooms, hotels } = this.state;
      const { checkInDate, checkOutDate, guest, room, hotelCity, hotelName, facilities } = params;

      if (!readyRooms) {
        return this.renderLoadingIndicator()
      }

      return (
        <View>
          <Header title='Pilih Kamar' />
          <ScrollView style={{backgroundColor: '#FFFFFF'}}>
            <View style={{height: 76, backgroundColor: Color.theme, width: '100%'}} />
            <AbsoluteView>
              <HeaderView>
                <WrapContentView>
                  <TitleText type='bold'>{hotelName}</TitleText>
                  {this.renderInfoBox(params)}
                </WrapContentView>
              </HeaderView>
            </AbsoluteView>
            <ContactViewPemesan>
            {readyRooms && hotels.hotels.hotels &&
              <View style={{paddingTop: 56, paddingHorizontal: 8}}>
                {this.renderPerRoom()}
              </View>}
            </ContactViewPemesan>
            {readyRooms && (!hotels.hotels.hotels || hotels.hotels.hotels[0].rooms.length <= 0) &&
              <NoResultView>
                <ImageProperty source={noRoom} style={{width: 80, height: 120, marginBottom: 32}}/>
                <Text size={18} type='bold' style={{lineHeight: 24, letterSpacing: 0.54}}>Kamar Tidak Tersedia</Text>
              </NoResultView>}
          </ScrollView>
        </View>
      )
    }
}

const mapStateToProps = state => {
  return {
    user: state['user.auth'].login.user,
    type: state['hotels'].type,
    hotel: state['hotels'].hotel,
    hotelCode: state['hotels'].hotelCode,
    name: state['hotels'].name,
    checkInDate: state['hotels'].checkInDate,
    checkOutDate: state['hotels'].checkOutDate,
    guest: state['hotels'].guest,
    room: state['hotels'].room,
    hotels: state['hotels'].hotels,
    loadingHotel: state['hotels'].loading,
  };
};

const mapDispatchToProps = (dispatch, props) => (
  {
    updateInputUser: (data) => {
      dispatch(updateInputUser(data));
    },
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(SelectHotelRoom);
