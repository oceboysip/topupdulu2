import React, { Component } from 'react';
import { View, TouchableOpacity as NativeTouchable } from 'react-native';
import Styled from 'styled-components';
import AntDesign from 'react-native-vector-icons/AntDesign';

import Text from '../Text';
import Button from '../Button';
import Color from '../Color';
import { Col } from '../Grid';

const MainView = Styled(View)`
    width: 100%;
    height: 100%;
    borderTopLeftRadius: 5;
    borderTopRightRadius: 5;
    backgroundColor: #FFFFFF;
    justifyContent: flex-end;
`;

const DragContainer = Styled(View)`
    width: 100%;
    height: 2%;
    justifyContent: flex-start;
    alignItems: center;
`;

const DragView = Styled(View)`
    width: 40;
    height: 6;
    borderRadius: 3;
    backgroundColor: #DDDDDD;
`;

const TitleView = Styled(View)`
    width: 100%;
    height: 20%;
    justifyContent: center;
    alignItems: center;
`;

const PerLineView = Styled(View)`
    width: 100%;
    height: 26%;
    flexDirection: row;
    alignItems: center;
    justifyContent: space-between;
    paddingHorizontal: 16;
    borderBottomWidth: 1;
    borderColor: #E8E3E3;
`;

const RectangleView = Styled(NativeTouchable)`
    width: 45;
    height: 35;
    backgroundColor: ${Color.darkGrey};
    alignItems: center;
    justifyContent: center;
    borderRadius: 17.5;
`;

const OperationalView = Styled(View)`
    width: 50%;
    minHeight: 1;
    flexDirection: row;
    alignItems: center;
    justifyContent: flex-end;
`;

const PassengerNumberView = Styled(View)`
    alignItems: flex-start;
    justifyContent: center;
    marginLeft: 4;
`;

const TitleText = Styled(Text)`
    letterSpacing: 1;
`;

const OptionText = Styled(Text)`
    fontSize: 12;
`;

const PassengerNumber = Styled(Text)`
    fontSize: 16;
    color: ${Color.theme};
    fontWeight: bold;
`;

const ButtonContainer = Styled(View)`
    width: 100%;
    height: 26%;
    justifyContent: space-between;
    paddingHorizontal: 16;
    flexDirection: row;
    alignItems: center;
`;

const CustomButton = Styled(Button)`
    height: 50;
    width: 48%;
    backgroundColor: ${Color.theme};
`;

const CancelButton = Styled(CustomButton)`
    backgroundColor: ${Color.primary};
`;

const OperationalIcon = Styled(AntDesign)`
    fontSize: 14;
    color: #FFFFFF;
`;

const options = [
  { header: 'Tamu', label: 'Jumlah Tamu', type: 'guest' },
  { header: 'Kamar', label: 'Jumlah Kamar', type: 'room' },
];

export default class GuestRoomPicker extends Component {

  constructor(props) {
    super(props);
    this.state = {
      guest: props.optionBedGuest.guest,
      room: props.optionBedGuest.room,
      max: props.max || { guest: 16, room: 6 },
      min: props.min || { guest: 1, room: 1 },
    };
  }

  tryDecrement(type) {
    return this.state[type] > this.state.min[type];
  }

  tryIncrement(type) {
    return this.state[type] < this.state.max[type];
  }

  decrement(type) {
    this.setState({ [type]: this.state[type] - 1 });
  }

  increment(type) {
    this.setState({ [type]: this.state[type] + 1 });
  }

  renderOptions() {
    return options.map((opt, i) => {
      const canDecrement = this.tryDecrement(opt.type);
      const canIncrement = this.tryIncrement(opt.type);
      return (
        <PerLineView key={i}>
          <View style={{flexDirection: 'row', width: '50%'}}>
            <View style={{alignItems: 'flex-start', width: '60%'}}>
              <Text type='bold'>{opt.header}</Text>
              <OptionText type='medium'>{opt.label}</OptionText>
            </View>
            <PassengerNumberView style={{width: '40%'}}>
              <PassengerNumber>{this.state[opt.type]}</PassengerNumber>
            </PassengerNumberView>
          </View>
          <OperationalView>
            <RectangleView activeOpacity={1} onPress={() => canDecrement && this.decrement(opt.type)} style={!canDecrement && { backgroundColor: Color.border}}>
              <OperationalIcon name='minus' style={!canDecrement && { color: '#FFFFFF' }} />
            </RectangleView>
            <View style={{width: 8}} />
            <RectangleView activeOpacity={1} onPress={() => canIncrement && this.increment(opt.type)} style={!canIncrement && { backgroundColor: Color.border}}>
              <OperationalIcon name='plus' style={!canIncrement && { color: '#FFFFFF' }} />
            </RectangleView>
          </OperationalView>
        </PerLineView>
      );
    });
  }

  render() {
    const { guest, room } = this.state;
    return (
      <MainView>
        <View style={{height: '98%'}}>
          <DragContainer>
            <DragView />
          </DragContainer>
          <TitleView>
            <TitleText style={{color: Color.theme}} type='bold'>ATUR TAMU DAN KAMAR</TitleText>
          </TitleView>
          {this.renderOptions()}
          <ButtonContainer>
            <CancelButton onPress={() => this.props.onClose()} fontSize={14}>BATAL</CancelButton>
            <CustomButton onPress={() => this.props.onSave({guest, room})} fontSize={14}>SELESAI</CustomButton>
          </ButtonContainer>
        </View>
      </MainView>
    );
  }
}
