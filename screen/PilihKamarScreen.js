/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {Alert,Platform, StyleSheet, Text, View,Image,TextInput,TouchableOpacity,ImageBackground,ScrollView,Dimensions, Button,Switch,TouchableWithoutFeedback} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Dialog, {SlideAnimation, DialogContent } from 'react-native-popup-dialog';
import BottomSheet from 'reanimated-bottom-sheet'
import SlidingUpPanel from 'rn-sliding-up-panel';
import PilihKamarStyle from '../style/PilihKamarStyle';


export default class PilihKamarScreen extends Component<{}> {
  constructor(props){
  super(props)
    this.state = {
      isOnDefaultToggleSwitch: false,
      isOnLargeToggleSwitch: false,
      isOnBlueToggleSwitch: false,
      isHidden: false
    };
  }

  onToggle(isOn) {
    console.log("Changed to " + isOn);
  }
  HotelDetail = () => {
    this.props.navigation.navigate('HotelDetail');
  }

  render() {
    return (
      <View style={styles.container}>
      

        <View style={styles.navTop}>
          <View style={styles.rowsBeetwen}>
            <View style={{flexDirection: 'row',alignItems:'center'}}>
              <TouchableOpacity onPress={this.HotelDetail}>
                <Image style={{width:30, height:30,resizeMode:'contain'}} source={require('../images/left-arrow.png')}/>
              </TouchableOpacity>
              <Text style={styles.titlePage}>Pilih Tipe Kamar</Text>
            </View>
          </View>

        </View>
   

        
        <ScrollView>
          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row',alignSelf:'stretch',flex:1}}>
                    <View style={styles.captionForm}>
                      <Text style={styles.BigText}>Superior</Text>
                      <Text style={styles.smallText}>Single - Room Only</Text>
                    </View>
                  </View>

                  <View style={{flex:1, alignItems:"flex-end",paddingRight:10}}>
                    <Text style={styles.BigText}>IDR 170.000</Text>
                    <TouchableOpacity style={styles.bgGreenTextAbs} onPress={this.HotelDetail}>
                      <Text style={{color:"#fff",fontSize:13,fontWeight:'bold',PaddingHorizontal:20}}>PILIH</Text>
                    </TouchableOpacity>
                  </View>

                </View>
              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row',alignSelf:'stretch',flex:1}}>
                    <View style={styles.captionForm}>
                      <Text style={styles.BigText}>Superior</Text>
                      <Text style={styles.smallText}>Single - Room Only</Text>
                    </View>
                  </View>

                  <View style={{flex:1, alignItems:"flex-end",paddingRight:10}}>
                    <Text style={styles.BigText}>IDR 170.000</Text>
                    <TouchableOpacity style={styles.bgGreenTextAbs} onPress={this.HotelDetail}>
                      <Text style={{color:"#fff",fontSize:13,fontWeight:'bold',PaddingHorizontal:20}}>PILIH</Text>
                    </TouchableOpacity>
                  </View>

                </View>
              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row',alignSelf:'stretch',flex:1}}>
                    <View style={styles.captionForm}>
                      <Text style={styles.BigText}>Superior</Text>
                      <Text style={styles.smallText}>Single - Room Only</Text>
                    </View>
                  </View>

                  <View style={{flex:1, alignItems:"flex-end",paddingRight:10}}>
                    <Text style={styles.BigText}>IDR 170.000</Text>
                    <TouchableOpacity style={styles.bgGreenTextAbs} onPress={this.HotelDetail}>
                      <Text style={{color:"#fff",fontSize:13,fontWeight:'bold',PaddingHorizontal:20}}>PILIH</Text>
                    </TouchableOpacity>
                  </View>

                </View>
              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row',alignSelf:'stretch',flex:1}}>
                    <View style={styles.captionForm}>
                      <Text style={styles.BigText}>Superior</Text>
                      <Text style={styles.smallText}>Single - Room Only</Text>
                    </View>
                  </View>

                  <View style={{flex:1, alignItems:"flex-end",paddingRight:10}}>
                    <Text style={styles.BigText}>IDR 170.000</Text>
                    <TouchableOpacity style={styles.bgGreenTextAbs} onPress={this.HotelDetail}>
                      <Text style={{color:"#fff",fontSize:13,fontWeight:'bold',PaddingHorizontal:20}}>PILIH</Text>
                    </TouchableOpacity>
                  </View>

                </View>
              </View>
            </View>
          </View>

          <View style={styles.rows}>
            <View style={styles.boxShadow}>
              <View style={styles.boxInner_bt}>
                <View style={styles.flexRowsBeetwen}>
                  <View style={{flexDirection:'row',alignSelf:'stretch',flex:1}}>
                    <View style={styles.captionForm}>
                      <Text style={styles.BigText}>Superior</Text>
                      <Text style={styles.smallText}>Single - Room Only</Text>
                    </View>
                  </View>

                  <View style={{flex:1, alignItems:"flex-end",paddingRight:10}}>
                    <Text style={styles.BigText}>IDR 170.000</Text>
                    <TouchableOpacity style={styles.bgGreenTextAbs} onPress={this.HotelDetail}>
                      <Text style={{color:"#fff",fontSize:13,fontWeight:'bold',PaddingHorizontal:20}}>PILIH</Text>
                    </TouchableOpacity>
                  </View>

                </View>
              </View>
            </View>
          </View>
        </ScrollView>
        
      </View>
    )
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  }
}




// STYLES
// ------------------------------------
const styles = StyleSheet.create(PilihKamarStyle);