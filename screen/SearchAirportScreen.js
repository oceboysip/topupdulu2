import React, { Component } from "react";
import { SafeAreaView, FlatList, StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';

import SearchAirportStyle from '../style/SearchAirportStyle';
import Airports from './LocalData/Airports';
import Color from './Color';
import Header from './Header';

const imgSearch = require('../images/icon_search.png');

export default class SearchAirportScreen extends Component {
  constructor(props) {
    super(props);
    // airports: Airports,
    this.state = {
      airports: [],
      searchText: ''
    }
  }

  searchFilter(searchText) {
    let airports = Airports;
    const text = searchText.toLowerCase();
    //filter  -> javascript fungsi langsung 
    airports = airports.filter((item) => (
      item.code.toLowerCase().indexOf(text) > -1
          || item.name.toLowerCase().indexOf(text) > -1
          || item.cityName.toLowerCase().indexOf(text) > -1
      )
    );
    this.setState({ airports });
  }

  onChangeText(searchText) {
    this.setState({ searchText });
    this.searchFilter(searchText);
  }

  onSelectedAirport({ item }) {
    this.props.onSelectedAirport(item);
    this.props.onClose();
  }

  renderAirports(data) {
    return (
      <TouchableOpacity style={[styles.rows, {backgroundColor: '#FFFFFF'}]} onPress={() => this.onSelectedAirport(data)} activeOpacity={0.9}>
        <View style={styles.boxShadow}>
          <View style={styles.boxInner_bt}>
            <View style={styles.flexRowsBeetwen}>
              <View style={styles.captionForm}>
                <View>
                  <Text style={styles.BigText}>{data.item.code}</Text>
                </View>
                <Text style={styles.smallText}>{data.item.name} - {data.item.cityName}</Text>
              </View>
              <View style={styles.iconForm}>
                <Image
                  style={{width: 40, height: 34, resizeMode: 'contain'}}
                  source={this.props.label === 'Berangkat' ? require('../images/icon_flying.png') : require('../images/icon_depart.png')}
                />
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  renderHeader(text, isHistory) {
    return (
      <View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 8, paddingVertical: 8, alignItems: 'center'}}>
          <Text>{text}</Text>
          {text === 'Pencarian Terakhir' && isHistory && <Text style={{fontSize: 10}} onPress={() => this.props.removeHistory()}>Hapus Riwayat</Text>}
        </View>
        {text === 'Pencarian Terakhir' && !isHistory && <View style={{alignItems: 'center', paddingVertical: 16}}>
          <Text>Anda belum memiliki riwayat pencarian</Text>
        </View>}
      </View>
    )
  }

  render() {
    console.log(this.props, 'aipo');
    const { airports, searchText } = this.state;
    const { dataSearch, isHistory } = this.props;

    return (
      <SafeAreaView style={{backgroundColor: Color.theme, flex: 1}}>
        <Header onPressLeftButton={() => this.props.onClose()}>
          <View style={{flexDirection: 'row', backgroundColor: '#FFFFFF', width: '90%', height: 45, paddingHorizontal: 8, alignItems: 'center', borderRadius: 22.5}}>
            <Image 
              style={{width: 30, height: 30, resizeMode: 'contain'}}
              source={imgSearch}
            />
            <TextInput
              placeholder={'Pilih Bandara ' + this.props.label}
              style={{width: '100%', paddingLeft: 8}}
              placeholderTextColor='#DDDDDD'
              underlineColorAndroid='transparent'
              autoCorrect={false}
              onChangeText={(text) => this.onChangeText(text)}
              selectionColor='#000000'
              maxLength={30}
            />
          </View>
        </Header>
      
        <FlatList
          style={{backgroundColor: '#F5F6F7'}}
          keyboardShouldPersistTaps='always'
          data={searchText !== '' ? airports : dataSearch}
          keyExtractor={(item, index) => `${item.id}.${index}`}
          renderItem={(rowData) => rowData.item.header ? this.renderHeader(rowData.item.header, isHistory) : this.renderAirports(rowData)}
          ListHeaderComponent={() => searchText === '' && this.renderHeader('Pencarian Terakhir', isHistory)}
        />
      </SafeAreaView>
    );
  }
  // HEADER STYLE
  // --------------------------------------
  static navigationOptions = {
    header: null,
    gesturesEnabled: false
  };
}

// STYLES
// ------------------------------------
const styles = StyleSheet.create(SearchAirportStyle);
